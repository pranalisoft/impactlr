﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Configuration;



namespace AppCommon
{
   
       public class ImpersonateUser
       {
           public const int LOGON32_LOGON_INTERACTIVE = 2;
           public const int LOGON32_PROVIDER_DEFAULT = 0;

           WindowsImpersonationContext impersonationContext;

           [DllImport("advapi32.dll")]
           public static extern int LogonUserA(String lpszUserName,
               String lpszDomain,
               String lpszPassword,
               int dwLogonType,
               int dwLogonProvider,
               ref IntPtr phToken);
           [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
           public static extern int DuplicateToken(IntPtr hToken,
               int impersonationLevel,
               ref IntPtr hNewToken);

           [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
           public static extern bool RevertToSelf();

           [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
           public static extern bool CloseHandle(IntPtr handle);

           public bool ImpersonateValidUser(string username , string domainname,string pwd)
           {
               

               WindowsIdentity tempWindowsIdentity;
               IntPtr token = IntPtr.Zero;
               IntPtr tokenDuplicate = IntPtr.Zero;
               String userName = username;
               String domain = domainname;
               String password = pwd; 

               if (RevertToSelf())
               {
                   if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                       LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                   {
                       if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                       {
                           tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                           impersonationContext = tempWindowsIdentity.Impersonate();
                           if (impersonationContext != null)
                           {
                               CloseHandle(token);
                               CloseHandle(tokenDuplicate);
                               return true;
                           }
                       }
                   }
               }
               if (token != IntPtr.Zero)
                   CloseHandle(token);
               if (tokenDuplicate != IntPtr.Zero)
                   CloseHandle(tokenDuplicate);
               return false;
           }

           public bool ImpersonateValidUser()
           {
               WindowsIdentity tempWindowsIdentity;
               IntPtr token = IntPtr.Zero;
               IntPtr tokenDuplicate = IntPtr.Zero;
               String userName = ConfigurationSettings.AppSettings["Imp_user"].ToString();
               String domain = ConfigurationSettings.AppSettings["Imp_Domain"].ToString();
               String password = ConfigurationSettings.AppSettings["Imp_pwd"].ToString();

               if (RevertToSelf())
               {
                   if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                       LOGON32_PROVIDER_DEFAULT, ref token) != 0)
                   {
                       if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                       {
                           tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                           impersonationContext = tempWindowsIdentity.Impersonate();
                           if (impersonationContext != null)
                           {
                               CloseHandle(token);
                               CloseHandle(tokenDuplicate);
                               return true;
                           }
                       }
                   }
               }
               if (token != IntPtr.Zero)
                   CloseHandle(token);
               if (tokenDuplicate != IntPtr.Zero)
                   CloseHandle(tokenDuplicate);
               return false;
           }


           public void UndoImpersonation()
           {
               impersonationContext.Undo();
           }

       }
    
}
