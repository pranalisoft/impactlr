﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;

namespace Logger
{
    public class LogWriter
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(LogWriter));

        public LogWriter()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public void WriteLogInfo(string msg)
        {
            log.Info(msg);
        }

        public void WriteLogInfo(string msg,string level)
        {
            log.Info(msg);
        }

        public void WriteLogError(string msg,Exception excp)
        {
            log.Error(msg, excp);
        }

        public void LoadConfiguration()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
