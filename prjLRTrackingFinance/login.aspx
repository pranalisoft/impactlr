﻿<%@ Page Title="LR Tracking Login" Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs"
    Inherits="prjLRTrackerFinanceAuto.Pages.login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <link href="Include/CSS/login.css" rel="stylesheet" type="text/css" />
    <script src="Include/js/layout/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="Include/CSS/animate.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
         
    </script>
    <title>LR Tracking Login</title>
</head>
<body style="font-family: Verdana; margin: 0; background-image: Url(Include/Common/images/BG.jpg);
    background-repeat: no-repeat, repeat; background-size: cover;">
    <form id="form1" runat="server">
    <ajax:ToolkitScriptManager runat="server" AsyncPostBackTimeout="600">
    </ajax:ToolkitScriptManager>
    <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
    <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
    <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Width="500px" Height="200px"
        Style="display: none; border-radius: 12px;">
        <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 16px;
            height: 40px; font-weight: bold; border-radius: 5px; text-align: left;">
            Select Line Of Business
        </div>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="popPlant" runat="server" DynamicServicePath="" Enabled="True"
        TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <%-- <div class="header">
        <table style="width: 100%">
            <tr>               
                <td style="vertical-align: middle; padding-left: 10px; padding-top: 10px">
                    <asp:Label ID="lblApplicationName" runat="server" Text="LR Tracking Application"
                        Style="font-size: 20px; font-weight: bolder" ForeColor="#3A66AF"></asp:Label>
                </td>
            </tr>
        </table>
    </div>--%>
    <%--<div style="top: 0; border-bottom: solid 2px #247BFF; height: auto; background-color: #3A66AF;">
        <table>
            <tr>
                <td width="40%">
                    <asp:Label ID="lblApplicationName" runat="server" Text="Jobwork Application" Style="font-size: 20px;
                        font-weight: bolder" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </div>--%>
    <div style="width: 100%; height: 500px; vertical-align: bottom;">
        <asp:Panel ID="pnlmain" runat="server" DefaultButton="btnlogin">
            <table width="99%">
                <tr>
                    <td style="width: 30%; vertical-align: top">
                        <asp:Panel ID="Panel1" runat="server" Style="text-align: center; background-color: transparent">
                            <img id="Img1" alt="SignIn" runat="server" src="Include/Common/images/Ash.png" height="150"
                                width="300" />
                            <%--style="box-shadow: 0 0 20px 7px rgba(0,0,0,0.5);"--%>
                        </asp:Panel>
                    </td>
                    <td style="width: 40%">
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <asp:Panel ID="Panel3" runat="server" Style="text-align: center; background-color: transparent">
                            <img id="Img4" alt="SignIn" runat="server" src="Include/Common/images/Impact.png"
                                height="150" width="300" />
                            <%--style="box-shadow: 0 0 20px 7px rgba(0,0,0,0.5);"--%>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <table width="99%" style="margin-top: -40px">
                <tr>
                    <td style="width: 23%;">
                    </td>
                    <td style="width: 56%">
                        <table align="center" style="margin-top: 50px">
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <div class="box_border_outernew" style="margin-left: -20px">
                            <table align="center" style="margin: -7px 0px 0px -7px; padding-bottom: -5px; width: 100%;">
                                <tr>
                                    <td style="width: 60%; height: 520px; background-color: #45889B; border-radius: 40px;">
                                        <table align="center" style="margin: -5px 0px 0px 1px; padding-bottom: -5px; width: 100%">
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="Label3" runat="server" Text="Transport Management System" Style="font-family: Cambria;
                                                        font-size: 30px; font-weight: bold" ForeColor="White"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="Label4" runat="server" Text="(TMS)" Visible="true" Style="font-family: Cambria;
                                                        font-size: 30px; font-weight: bold" ForeColor="White"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <img id="Img7" alt="SignIn" runat="server" src="Include/Common/images/Plane.jpg"
                                                        height="160" width="250" style="margin-left: -160px; margin-top: -50px" />
                                                </td>
                                                <td align="center">
                                                    <img id="Img6" alt="SignIn" runat="server" src="Include/Common/images/TMS.jpg" height="390"
                                                        width="500" style="margin-left: -100px; margin-top: 20px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 50%; height: 520px; vertical-align: top">
                                        <div id="dvLogin" runat="server" class="" style="margin-top: 22px">
                                            <asp:Label ID="lblMessage" runat="server" Text="Invalid UserName or Password" Visible="false"
                                                ForeColor="Red"></asp:Label>
                                            <table class="login_box" cellpadding="0" cellspacing="1" style="width: 100%">
                                                <tr>
                                                    <td colspan="2" style="text-align: center">
                                                        <asp:Panel ID="Panel2" runat="server" Style="text-align: center">
                                                            <img id="Img3" alt="SignIn" runat="server" src="Include/Common/images/Abhi_Group.png"
                                                                height="160" width="250" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center; border-bottom: 5px solid #45889B;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <%--<tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>--%>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center">
                                                        <asp:Label ID="Label2" runat="server" Text="USER LOGIN" ForeColor="Navy" Font-Size="18px"
                                                            Font-Names="Verdana"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_row" style="background: #1886C3; border-radius: 5px 0px 0px 5px">
                                                    <td width="80px" align="center" style="border-radius: 5px 0px 0px 5px">
                                                        <img id="Img2" alt="SignIn" runat="server" src="Include/Common/images/userlogin1.png"
                                                            height="35" width="40" />
                                                    </td>
                                                    <td style="border-radius: 0px 5px 5px 0px">
                                                        <asp:TextBox ID="txtlogin" CssClass="logintextbox" runat="server" Style="background: #1886C3"
                                                            Font-Size="17px" Width="90%" TabIndex="2" Height="35px" ForeColor="White"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="reqFldValUserID" runat="server" ErrorMessage="Please enter the userid."
                                                            ControlToValidate="txtlogin" Display="None" SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                                        <ajax:ValidatorCalloutExtender ID="reqFldValUserID_ValidatorCalloutExtender" runat="server"
                                                            Enabled="True" TargetControlID="reqFldValUserID">
                                                        </ajax:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_row" style="background: #1886C3; border-radius: 5px 0px 0px 5px">
                                                    <td align="center" style="border-radius: 5px 0px 0px 5px">
                                                        <img id="Img5" alt="SignIn" runat="server" src="Include/Common/images/password.png"
                                                            height="35" width="40" />
                                                    </td>
                                                    <td style="border-radius: 0px 5px 5px 0px">
                                                        <asp:TextBox ID="txtPwd" CssClass="logintextbox" TextMode="Password" ForeColor="White"
                                                            Font-Size="17px" runat="server" Width="90%" TabIndex="3" Height="35px" Style="background: #1886C3"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="reqFldValPwd" runat="server" ErrorMessage="Please enter the password."
                                                            ControlToValidate="txtPwd" Display="None" SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                                        <ajax:ValidatorCalloutExtender ID="reqFldValPwd_ValidatorCalloutExtender" runat="server"
                                                            Enabled="True" TargetControlID="reqFldValPwd">
                                                        </ajax:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_row" style="background: #1886C3; border-radius: 5px 0px 0px 5px">
                                                    <td width="80px" align="center" style="border-radius: 5px 0px 0px 5px">
                                                        <img id="Img9" alt="SignIn" runat="server" src="Include/Common/images/Otp.png" height="35"
                                                            width="40" />
                                                    </td>
                                                    <td style="border-radius: 0px 5px 5px 0px">
                                                        <asp:TextBox ID="txtOtp" CssClass="logintextbox" TextMode="Password" runat="server"
                                                            Style="background: #1886C3" Font-Size="27px" Width="90%" TabIndex="4" Height="35px"
                                                            ForeColor="White"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvOtp" runat="server" ErrorMessage="Please enter otp"
                                                            ControlToValidate="txtOtp" Display="None" SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                                        <ajax:ValidatorCalloutExtender ID="rfvOtpCall" runat="server" Enabled="True" TargetControlID="rfvOtp">
                                                        </ajax:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td>
                                                        <asp:CheckBox ID="chkboxKeepSignedIn" runat="server" Text=" keep me signed in for two weeks"
                                                            CssClass="keep_signin" TabIndex="8" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="login_box_header">
                                                    <td colspan="2" style="text-align: center;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="center">
                                                        <div class="link_panel">
                                                            <%--<asp:LinkButton ID="lnkBtnLostPwd" Visible="false" runat="server" CssClass="lost_link"
                                                                OnClick="lnkBtnLostPwd_Click" TabIndex="5">Forgot Password?</asp:LinkButton>--%>
                                                        </div>
                                                        <div class="button_panel">
                                                            <asp:Button ID="btnlogin" runat="server" Text=" LOGIN " CssClass="button" ValidationGroup="login"
                                                                Font-Size="17px" OnClick="btnlogin_Click" TabIndex="5" Width="100px" />
                                                            &nbsp;&nbsp;
                                                            <asp:Button runat="server" ID="btnOtpGenerate" CssClass="button" Text="Generate OTP"
                                                                OnClick="btnOtpGenerate_Click" TabIndex="6" Width="120px"  />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="dvBranch" runat="server" class="" visible="false" style="margin-top: 22px">
                                            <div align="center" style="padding: 5px">
                                                <table class="login_box" cellpadding="0" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td colspan="2" style="text-align: center">
                                                            <asp:Panel ID="Panel4" runat="server" Style="text-align: center">
                                                                <img id="Img8" alt="SignIn" runat="server" src="Include/Common/images/Abhi_Group.png"
                                                                    height="160" width="250" />
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td colspan="2" style="text-align: center;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td colspan="2" style="text-align: center; border-bottom: 5px solid #45889B;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td colspan="2" style="text-align: center;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td colspan="2" style="text-align: center">
                                                            <asp:Label ID="Label1" runat="server" Text="Line Of Business" ForeColor="#3A66AF"
                                                                Font-Size="18px" Font-Bold="true" Font-Names="Verdana"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cmbPlant" runat="server" CssClass="mydropdownlist1" Width="96%"
                                                                Height="35px">
                                                            </asp:DropDownList>
                                                            <asp:CustomValidator ID="reqFVCust" runat="server" ControlToValidate="cmbPlant" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select plant from the list'
                                                                ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                                ValidationGroup="selectPlant" SetFocusOnError="true"></asp:CustomValidator>
                                                            <asp:Label ID="lblUserSrl" runat="server" Text="Plant" Font-Bold="True" Font-Size="10pt"
                                                                Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr class="login_box_header">
                                                        <td style="width: 50%;">
                                                            <asp:Button ID="btnYes" runat="server" Text="Ok" Width="96%" CssClass="button" Font-Size="17px"
                                                                CommandName="yes" OnCommand="btnConfirm_Command" ValidationGroup="selectPlant" />
                                                        </td>
                                                        <td style="width: 50%;">
                                                            <asp:Button ID="btnNo" runat="server" Text="Cancel" Width="96%" CssClass="button"
                                                                Font-Size="17px" CommandName="no" OnCommand="btnConfirm_Command" Style="margin-left: -4px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%-- <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 14px; font-family: Arial; color: White">
                                </td>
                            </tr>
                        </table>
                        <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 14px; font-family: Arial; color: White">
                                </td>
                            </tr>
                        </table>
                        <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 14px; font-family: Arial; color: White">
                                    <b>Sign in into </b>
                                </td>
                            </tr>
                        </table>
                        <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 18px; font-family: Arial; color: White">
                                    <b>LR Tracking Application</b>
                                </td>
                            </tr>
                        </table>
                        <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 14px; font-family: Arial; color: White">
                                </td>
                            </tr>
                        </table>
                        <table align="center" bgcolor="#3A66AF" style="width: 100%">
                            <tr>
                                <td align="center" style="font-size: 14px; font-family: Arial; color: White">
                                </td>
                            </tr>
                        </table>
                        <table border="0" bgcolor="white" align="center" style="border: solid 3px #3A66AF;
                            width: 100%" cellspacing="20px" cellpadding="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:TextBox ID="txtlogin" runat="server" Width="99%" CssClass="NormalTextBold" TabIndex="1"></asp:TextBox>
                                    <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server" TargetControlID="txtlogin"
                                        WatermarkText="Enter User Id" WatermarkCssClass="watermarked" />
                                    <asp:RequiredFieldValidator ID="rfvUserId" ControlToValidate="txtlogin" SetFocusOnError="true"
                                        ValidationGroup="login" runat="server" ErrorMessage="<br/>Please enter userid"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%">
                                    <asp:TextBox ID="txtPwdText" runat="server" Width="99%" CssClass="NormalTextBold"
                                        TextMode="SingleLine" TabIndex="1"></asp:TextBox>
                                    <asp:TextBox ID="txtPwd" runat="server" Width="99%" CssClass="NormalTextBold" TextMode="Password"
                                        TabIndex="2"></asp:TextBox>
                                    <ajax:TextBoxWatermarkExtender ID="TBWE3" runat="server" TargetControlID="txtPwdText"
                                        WatermarkText="Enter Password" WatermarkCssClass="watermarked" />
                                    <asp:RequiredFieldValidator ID="rfvPwd" ControlToValidate="txtPwd" SetFocusOnError="true"
                                        ValidationGroup="login" runat="server" ErrorMessage="<br/>Please enter password"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Button ID="btnlogin" runat="server" ValidationGroup="login" OnClick="btnlogin_Click"
                                        CssClass="button" Text="Login" Width="70px" TabIndex="3" />&nbsp;&nbsp;
                                    <asp:Label runat="server" ID="lblIP"></asp:Label>
                                </td>
                            </tr>
                        </table>--%>
                    </td>
                    <td style="width: 23%">
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
