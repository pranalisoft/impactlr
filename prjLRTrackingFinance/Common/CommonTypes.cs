﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;

namespace prjLRTrackerFinanceAuto.Common
{
    public static class CommonTypes
    {
        #region Common Types

        public enum EntryFormCommand
        {
            Add, Delete, Save, Submit, Clear, None,SaveP
        }       

        public enum ModalPopupCommand
        {
            Ok, Yes, No
        }

        public enum ModalTypes
        {
            Alert, Confirm, Error
        }

        #endregion

        #region Common Functions

        public static T StringToEnum<T>(string command)
        {
            if (Enum.IsDefined(typeof(T), command))
            {
                T tCommand = (T)Enum.Parse(typeof(T), command, true);
                return tCommand;
            }
            else
            {
                return default(T);
            }
        }

        public static DataTable CreateDataTable(string colData, string dtName)
        {
            DataTable dt = new DataTable(dtName);
            string[] columnnames = colData.Split(",".ToCharArray());
            foreach (var item in columnnames)
            {
                dt.Columns.Add(item, typeof(string));
            }
            return dt;
        }

        public static DataTable CreateDataTable(string colData, string dtName, string colDataType)
        {
            DataTable dt = new DataTable(dtName);
            string[] columnnames = colData.Split(",".ToCharArray());
            string[] columntype = colDataType.Split(",".ToCharArray());
            for (int i = 0; i < columnnames.Length; i++)
            {
                switch (columntype[i])
                {
                    case "int":
                        dt.Columns.Add(columnnames[i], typeof(int));
                        break;
                    case "long":
                        dt.Columns.Add(columnnames[i], typeof(long));
                        break;
                    case "string":
                        dt.Columns.Add(columnnames[i], typeof(string));
                        break;
                    case "decimal":
                        dt.Columns.Add(columnnames[i], typeof(decimal));
                        break;
                    case "dateTime":
                        dt.Columns.Add(columnnames[i], typeof(DateTime));
                        break;
                    default:
                        break;
                }

            }
            return dt;
        }

        public static DataTable GetDataTable(string dtName)
        {
            DataTable dt = null;
            if (HttpContext.Current.Session[dtName] != null)
            {
                dt = (DataTable)HttpContext.Current.Session[dtName];
            }
            return dt;
        }

        public static void Insertdata(DataTable dt)
        {
            HttpContext.Current.Session[dt.TableName] = dt;
        }

        public static Boolean Addrow(DataTable dt, DataRow dr)
        {
            try
            {
                dt.Rows.Add(dr);
                Insertdata(dt);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static void DeleteRow(string dtName, int rowind)
        {
            DataTable dt = GetDataTable(dtName);
            dt.Rows.RemoveAt(rowind);
            dt.AcceptChanges();
            HttpContext.Current.Session[dtName] = dt;
        }

        public static void DeleteDataTable(string dtName)
        {
            if (HttpContext.Current.Session[dtName] != null)
            {
                HttpContext.Current.Session.Remove(dtName);
            }
        }

        public static string GetMailCotent(string FileType)
        {
            string FileName = "~/MailFormats/";
            switch (FileType)
            {
                case "L":
                    FileName = FileName + "LR.txt";
                    break;
                case "V":
                    FileName = FileName + "VC.txt";
                    break;   
                default:
                    break;
            }
            using (StreamReader streamReader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath(FileName)))
            {
                return streamReader.ReadToEnd();
            }
        }

        #endregion
    }
}
