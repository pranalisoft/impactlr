﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Net.Configuration;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Common
{
    public class MailHandler
    {
        private delegate void SendEmailDelegate(MailMessage m);

        public string SenderMail { get; set; }
        public string Password { get; set; }

        public MailHandler()
        {

        }

        public MailHandler(string senderMail, string password)
        {
            SenderMail = senderMail;
            Password = password;
        }

        public MailMessage CreateMessage(string toAddresses, string ccAddresses, string subject, string body, bool isBodyHtml, string Filepaths)
        {
            MailMessage msg = new MailMessage();

            string[] addresses = toAddresses.Split(',');

            string[] ccaddresses = ccAddresses.Split(',');

            for (int i = 0; i < addresses.Length; i++)
            {
                if (string.IsNullOrEmpty(addresses[i]))
                    continue;
                if (addresses[i].ToString().Trim().Contains("@"))
                    msg.To.Add(new MailAddress(addresses[i]));
            }

            for (int i = 0; i < ccaddresses.Length; i++)
            {
                if (string.IsNullOrEmpty(ccaddresses[i]))
                    continue;
                if (ccaddresses[i].ToString().Trim().Contains("@"))
                msg.CC.Add(new MailAddress(ccaddresses[i]));
            }

            msg.Subject = subject;
            msg.IsBodyHtml = isBodyHtml;
            msg.Body = body;
            if (Filepaths != string.Empty)
            {
                string[] Filepaths1 = Filepaths.Split(',');

                for (int i = 0; i < Filepaths1.Length; i++)
                {
                    if (string.IsNullOrEmpty(Filepaths1[i]))
                        continue;

                    Attachment data = new Attachment(Filepaths1[i]);
                    msg.Attachments.Add(data);
                }

            }
            return msg;
        }

        public bool SendEmail(MailMessage m, Boolean Async, Boolean enableSSL, Boolean useDefaultCredentials)
        {
            try
            {
                string smtphost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                string SenderMail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string Password = ConfigurationManager.AppSettings["Pwd"].ToString(); 
                int port = int.Parse(ConfigurationManager.AppSettings["PortNo"].ToString());                

                var client = new SmtpClient(smtphost, port)
                {
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(SenderMail, Password),
                    EnableSsl = true
                };
                if (Async)
                {
                    SendEmailDelegate sd = new SendEmailDelegate(client.Send);
                    AsyncCallback cb = new AsyncCallback(SendEmailResponse);
                    sd.BeginInvoke(m, cb, sd);

                }
                else
                {
                    client.Send(m);
                }
                
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private void SendEmailResponse(IAsyncResult ar)
        {
            SendEmailDelegate sd = (SendEmailDelegate)(ar.AsyncState);
            try
            {
                if (ar.IsCompleted == true)
                {

                }

                sd.EndInvoke(ar);
            }
            catch (Exception e)
            {

            }
        }
    }
}