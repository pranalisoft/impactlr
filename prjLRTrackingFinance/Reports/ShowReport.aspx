﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowReport.aspx.cs" 
Inherits="prjLRTrackerFinanceAuto.Reports.ShowReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="height:1500px">    
    <div style="overflow: auto; width: 100%; height: 100%;">
    <asp:HiddenField ID="HFReportType" runat="server" />
        <table width="100%">
            <tr style="width:100%">
                <td align="center">
                    <CR:CrystalReportViewer ID="CrptView" runat="server" AutoDataBind="true" ReuseParameterValuesOnRefresh="True"
                        HasRefreshButton="True" Width="100%" Height="100%" 
                        HasToggleGroupTreeButton="false" HasPrintButton="False" PageZoomFactor="100"
                        ToolPanelView="None" onunload="CrptView_Unload" />
                </td>
            </tr>
        </table>
        <%--<CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />--%>
    </div>
    </form>
</body>
</html>
