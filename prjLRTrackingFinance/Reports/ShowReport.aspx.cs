﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using CrystalDecisions;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Reports
{
    public partial class ShowReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                HFReportType.Value = Session["ReportType"].ToString();
            }
            InitializeReport();
        }

        private void InitializeReport()
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            CommonBO comBO = new CommonBO();
            DataTable dt = null;
            string indexReport = HFReportType.Value;

            if (indexReport == "LRDoc")
            {
                prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
                OptBO.ID = long.Parse(Session["LRID"].ToString());
                dt = OptBO.LR_Document();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                RptDoc.SetParameterValue(0, "N");
                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "InvDoc")
            {
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvRefNo = Session["InvRefNo"].ToString();
                OptBO.BillingCompanySrl = long.Parse(Session["BillingCompSrlDoc"].ToString());
                dt = OptBO.Invoice_Doument();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                if (Session["ForGdrej"] == null) Session["ForGdrej"] = "N";

                if (Session["ForGdrej"].ToString().Trim() == "G")
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentGodrej.rpt"));
                else if (Session["ForGdrej"].ToString().Trim() == "W")
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentFreightWhirlpool.rpt"));
                else if (Session["ForGdrej"].ToString().Trim() == "L")
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentLG.rpt"));
                else if (Session["ForGdrej"].ToString().Trim() == "M")
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocument_Mahindra.rpt"));
                else
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentTallyFormat.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "InvItems")
            {

                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvRefNo = Session["InvRefNo"].ToString();
                OptBO.BillingCompanySrl = long.Parse(Session["BillingCompSrlDoc"].ToString());
                dt = OptBO.Invoice_Doument();
                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                if (Session["PlntName"].ToString().Trim().ToLower().Contains("posco import"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexurePosco.rpt"));
                else if (Session["PlntName"].ToString().Trim().ToLower().Contains("mahindra"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexure_Mahindra.rpt"));
                else
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexure.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);

                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "Hamali")
            {
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvSrls = Session["InvSrls"].ToString();
                OptBO.InvYear = Session["InvYear"] ==null?0:  int.Parse(Session["InvYear"].ToString());
                OptBO.BillingCompanySrl = long.Parse(Session["BillingCompSrlDoc"].ToString());
                dt = OptBO.Invoice_Document_Multiple();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentHamaliWhirlpool.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "HamaliAnn")
            {
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvSrls = Session["InvSrls"].ToString();
                OptBO.BillingCompanySrl = long.Parse(Session["BillingCompSrlDoc"].ToString());
                OptBO.InvYear = Session["InvYear"] == null ? 0 : int.Parse(Session["InvYear"].ToString());
                dt = OptBO.Invoice_Document_Multiple();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                RptDoc.Load(Server.MapPath("~/Reports/MultipleInvoiceHamaliAnnexure.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "InvAnn")
            {
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvSrls = Session["InvSrls"].ToString();
                OptBO.BillingCompanySrl = long.Parse(Session["BillingCompSrlDoc"].ToString());
                OptBO.InvYear = 0;
                dt = OptBO.Invoice_Document_Multiple();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                RptDoc.Load(Server.MapPath("~/Reports/MultipleInvoiceItemAnnexure.rpt"));
                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                CrptView.ReportSource = RptDoc;
            }
            else if (indexReport == "CustomerLedger")
            {
                string SelType = "Customer Ledger For ' " + Session["CustomerName"].ToString() + " ' From " + Session["FromDate"].ToString() + " To " + Session["ToDate"].ToString();
                decimal Opening = 0;
                prjLRTrackerFinanceAuto.Datasets.DsCustomerLedger ds = new prjLRTrackerFinanceAuto.Datasets.DsCustomerLedger();
                OptBO.ID = long.Parse(Session["CustomerID"].ToString());
                OptBO.FromDate = Session["FromDate"].ToString();
                OptBO.ToDate = Session["ToDate"].ToString();
                Opening = OptBO.Customer_Ledger_Opening();
                dt = OptBO.Customer_Ledger();

                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);


                RptDoc.Load(Server.MapPath("~/Reports/CustomerLedger.rpt"));

                condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);
                RptDoc.SetParameterValue(0, Opening);
                RptDoc.SetParameterValue(1, SelType);
                RptDoc.SetParameterValue(2, Session["FromDate"].ToString());
                CrptView.ReportSource = RptDoc;
            }
            Session["RptDoc"] = RptDoc;
            //RptDoc.Close();
            //RptDoc.Dispose();
            //GC.Collect();
        }

        private static void condbsLogon(ReportDocument RptDoc)
        {
            string datasource = "";
            string dbname = "LRTracking";
            string username = "";
            string password = "";

            string connSetting = ConfigurationManager.AppSettings["ConnectionString"].ToString();

            string[] tokens = connSetting.Split(";".ToCharArray());
            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i].ToLower().Contains("data source"))
                {
                    datasource = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("user id"))
                {
                    username = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("password"))
                {
                    password = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("initial catalog"))
                {
                    dbname = tokens[i].Split("=".ToCharArray())[1];
                }
            }
            //datasource = tokens[0].Split("=".ToCharArray())[1];
            //username = tokens[3].Split("=".ToCharArray())[1];
            //password = tokens[4].Split("=".ToCharArray())[1];
            //dbname = tokens[1].Split("=".ToCharArray())[1];

            RptDoc.SetDatabaseLogon(username, password, datasource, dbname);

        }

        protected void CrptView_Unload(object sender, EventArgs e)
        {
            if (Session["RptDoc"] != null)
            {
                ReportDocument RptDoc = (ReportDocument)Session["RptDoc"];
                RptDoc.Close();
                RptDoc.Dispose();
            }
            GC.Collect();
        }
    }
}