<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="menu">
    <table width="100%" cellpadding="4" cellspacing="0" border="0" class="menuTable">
      <xsl:apply-templates/>
      <tr>
        <td class="menu_border" valign="top"></td>
      </tr>
    </table>
  </xsl:template>
  
  <xsl:template match="section">
    <tr>
      <td class="menuSection">
        <img src="{@image_url}" border="0" valign="bottom" style="padding-right:2px;"/>
        <span class="menuSctionText">
          <xsl:value-of select="@text" />
        </span>
      </td>
    </tr>
    <xsl:apply-templates select="items/item"/>
  </xsl:template>
  
  <xsl:template match="item">
    <tr>
      <td class="menuItem">
        <img src="{@image_url}" border="0" valign="baseline" />
        <a class="lnkMenuItems" href="{@url}">
          <xsl:value-of select="@text"/>
        </a>
      </td>
    </tr>
  </xsl:template>
  
</xsl:stylesheet>