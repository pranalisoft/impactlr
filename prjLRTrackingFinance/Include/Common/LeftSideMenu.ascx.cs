﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using Logger;
using System.Data;

namespace prjLRTrackerFinanceAuto.Common
{
    public partial class LeftSideMenu : System.Web.UI.UserControl
    {
        LogWriter logger;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["menuxml"] == null)
                {
                    DataTable dtMenu = new CommonDAO().GetMenu(long.Parse(Session["EID"].ToString()));
                    //DataSet ds=new DataSet();
                    //ds.Tables.Add(dtMenu);                   
                    string xmlstr = "<menu>";
                    string sectionsrl = "";
                    int sectioncnt = 0;
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        if (sectionsrl == "")
                        {
                            sectioncnt = 1;
                            sectionsrl = dtMenu.Rows[i].ItemArray[8].ToString();
                            xmlstr = xmlstr + @"<section image_url=""" + dtMenu.Rows[i].ItemArray[10].ToString() + @""" text=""" + dtMenu.Rows[i].ItemArray[9].ToString() + @""">";
                        }
                        else if (sectionsrl != dtMenu.Rows[i].ItemArray[8].ToString())
                        {
                            sectioncnt = 1;
                            sectionsrl = dtMenu.Rows[i].ItemArray[8].ToString();
                            xmlstr = xmlstr + @"</items></section><section image_url=""" + dtMenu.Rows[i].ItemArray[10].ToString() + @""" text=""" + dtMenu.Rows[i].ItemArray[9].ToString() + @""">";
                        }
                        if (sectioncnt == 1)
                            xmlstr = xmlstr + @"<items><item image_url=""" + dtMenu.Rows[i].ItemArray[6].ToString() + @""" url=""" + dtMenu.Rows[i].ItemArray[7].ToString() + @""" text=""" + dtMenu.Rows[i].ItemArray[5].ToString() + @"""/>";
                        else
                            xmlstr = xmlstr + @"<item image_url=""" + dtMenu.Rows[i].ItemArray[6].ToString() + @""" url=""" + dtMenu.Rows[i].ItemArray[7].ToString() + @""" text=""" + dtMenu.Rows[i].ItemArray[5].ToString() + @"""/>";

                        sectioncnt = sectioncnt + 1;
                    }
                    xmlstr = xmlstr + "</items></section></menu>";
                    System.Xml.XmlDocument menuXmlDocument = new System.Xml.XmlDocument();
                    menuXmlDocument.LoadXml(xmlstr);
                    leftSideMenuXml.Document = menuXmlDocument;
                    Session["menuxml"] = menuXmlDocument;
                }
                else
                {
                    leftSideMenuXml.Document = (System.Xml.XmlDocument)Session["menuxml"];
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Menu Generation", exp);
            }
        }
    }
}