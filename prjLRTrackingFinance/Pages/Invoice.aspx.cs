﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class Invoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                txtBidding.Text = "";
                if (Session["PlntName"].ToString().Trim().Contains("LG"))
                {                    
                    txtBidding.Enabled = true;
                }
                else
                {
                    txtBidding.Enabled = false;
                }
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void LoadGridView()
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            DataTable dt = optBO.FillFinalInvoiceGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;
        }

        private void FillPendingCustomers()
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            InvoiceBO optbo = new InvoiceBO();
            optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            DataTable dtCust = optbo.FillPendingCustomers();
            cmbCustomer.DataSource = dtCust;
            cmbCustomer.DataTextField = "CustomerName";
            cmbCustomer.DataValueField = "CustomerID";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        protected void txtBasicAmount_TextChanged(object sender, EventArgs e)
        {

        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            InvoiceBO optbo = new InvoiceBO();
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
            optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            DataTable dt = optbo.FillPendingLRSNew();
            grdPendingLRs.DataSource = dt;
            grdPendingLRs.DataBind();

            //for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            //{
            //    DisableGridAmounts(i);
            //}
            txtBRefNo.Focus();

        }

        //private void DisableGridAmounts(int rowindex)
        //{
        //    if (rowindex == -1)
        //    {
        //        for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
        //        {
        //            GridViewRow grdrow = grdPendingLRs.Rows[i];
        //            TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //            TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //            TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //            TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //            TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //            TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //            TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //            TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");
        //            TextBox txtDetloadingDays = (TextBox)grdrow.FindControl("txtDetloadingDays");
        //            TextBox txtDetUnloadingDays = (TextBox)grdrow.FindControl("txtDetUnloadingDays");
        //            TextBox txtTransitPenaltyDays = (TextBox)grdrow.FindControl("txtTransitPenaltyDays");
        //            TextBox txtLRRemarks = (TextBox)grdrow.FindControl("txtLRRemarks");

        //            txtFreight.Enabled = false;
        //            txtDetention.Enabled = false;
        //            txtDetention.Text = string.Empty;
        //            txtWarai.Enabled = false;
        //            txtWarai.Text = string.Empty;
        //            txtOtherCharges.Enabled = false;
        //            txtOtherCharges.Text = string.Empty;
        //            txtTwoPoint.Enabled = false;
        //            txtTwoPoint.Text = string.Empty;
        //            txtTotalCharges.Enabled = false;
        //            txtTotalCharges.Text = string.Empty;
        //            txtTransitPenaltyCharges.Enabled = false;
        //            txtTransitPenaltyCharges.Text = string.Empty;
        //            txtTransitPenaltyDays.Enabled = false;
        //            txtTransitPenaltyDays.Text = string.Empty;
        //            txtDetloadingDays.Enabled = false;
        //            txtDetloadingDays.Text = string.Empty;
        //            txtDetUnloadingCharges.Enabled = false;
        //            txtDetUnloadingCharges.Text = string.Empty;
        //            txtDetUnloadingDays.Enabled = false;
        //            txtDetUnloadingDays.Text = string.Empty;
        //            txtLRRemarks.Enabled = false;
        //            txtLRRemarks.Text = string.Empty;
        //            CalculateTotalAmount();
        //        }
        //    }
        //    else
        //    {
        //        GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
        //        TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //        TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //        TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //        TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //        TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //        TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //        TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //        TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");
        //        TextBox txtDetloadingDays = (TextBox)grdrow.FindControl("txtDetloadingDays");
        //        TextBox txtDetUnloadingDays = (TextBox)grdrow.FindControl("txtDetUnloadingDays");
        //        TextBox txtTransitPenaltyDays = (TextBox)grdrow.FindControl("txtTransitPenaltyDays");
        //        TextBox txtLRRemarks = (TextBox)grdrow.FindControl("txtLRRemarks");

        //        txtFreight.Enabled = false;
        //        txtDetention.Enabled = false;
        //        txtDetention.Text = string.Empty;
        //        txtWarai.Enabled = false;
        //        txtWarai.Text = string.Empty;
        //        txtOtherCharges.Enabled = false;
        //        txtOtherCharges.Text = string.Empty;
        //        txtTwoPoint.Enabled = false;
        //        txtTwoPoint.Text = string.Empty;
        //        txtTotalCharges.Enabled = false;
        //        txtTotalCharges.Text = string.Empty;
        //        txtTransitPenaltyCharges.Enabled = false;
        //        txtTransitPenaltyCharges.Text = string.Empty;
        //        txtTransitPenaltyDays.Enabled = false;
        //        txtTransitPenaltyDays.Text = string.Empty;
        //        txtDetloadingDays.Enabled = false;
        //        txtDetloadingDays.Text = string.Empty;
        //        txtDetUnloadingCharges.Enabled = false;
        //        txtDetUnloadingCharges.Text = string.Empty;
        //        txtDetUnloadingDays.Enabled = false;
        //        txtDetUnloadingDays.Text = string.Empty;
        //        txtLRRemarks.Enabled = false;
        //        txtLRRemarks.Text = string.Empty;
        //        DataKey keys = grdPendingLRs.DataKeys[rowindex];
        //        txtFreight.Text = keys["TotalFrieght"].ToString();

        //        CalculateTotalAmount();
        //    }
        //}

        //private void EnableGridAmounts(int rowindex)
        //{
        //    if (rowindex == -1)
        //    {
        //        for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
        //        {
        //            GridViewRow grdrow = grdPendingLRs.Rows[i];
        //            TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //            TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //            TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //            TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //            TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //            TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //            TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //            TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");
        //            TextBox txtDetloadingDays = (TextBox)grdrow.FindControl("txtDetloadingDays");
        //            TextBox txtDetUnloadingDays = (TextBox)grdrow.FindControl("txtDetUnloadingDays");
        //            TextBox txtTransitPenaltyDays = (TextBox)grdrow.FindControl("txtTransitPenaltyDays");
        //            TextBox txtLRRemarks = (TextBox)grdrow.FindControl("txtLRRemarks");
        //            txtFreight.Enabled = true;
        //            txtDetention.Enabled = true;
        //            txtWarai.Enabled = true;
        //            txtOtherCharges.Enabled = true;
        //            txtTwoPoint.Enabled = true;
        //            txtTransitPenaltyCharges.Enabled = true;
        //            txtTransitPenaltyDays.Enabled = true;
        //            txtDetloadingDays.Enabled = true;
        //            txtDetUnloadingCharges.Enabled = true;
        //            txtDetUnloadingDays.Enabled = true;
        //            txtLRRemarks.Enabled = true;
        //            txtTotalCharges.Text = txtFreight.Text;
        //            CalculateTotalAmount();
        //            txtFreight.Focus();

        //        }
        //    }
        //    else
        //    {
        //        GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
        //        TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //        TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //        TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //        TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //        TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //        TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //        TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //        TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");
        //        TextBox txtDetloadingDays = (TextBox)grdrow.FindControl("txtDetloadingDays");
        //        TextBox txtDetUnloadingDays = (TextBox)grdrow.FindControl("txtDetUnloadingDays");
        //        TextBox txtTransitPenaltyDays = (TextBox)grdrow.FindControl("txtTransitPenaltyDays");
        //        TextBox txtLRRemarks = (TextBox)grdrow.FindControl("txtLRRemarks");

        //        txtFreight.Enabled = true;
        //        txtDetention.Enabled = true;
        //        txtWarai.Enabled = true;
        //        txtOtherCharges.Enabled = true;
        //        txtTwoPoint.Enabled = true;
        //        txtTransitPenaltyCharges.Enabled = true;
        //        txtTransitPenaltyDays.Enabled = true;
        //        txtDetloadingDays.Enabled = true;
        //        txtDetUnloadingCharges.Enabled = true;
        //        txtDetUnloadingDays.Enabled = true;
        //        txtLRRemarks.Enabled = true;
        //        txtTotalCharges.Enabled = false;
        //        txtTotalCharges.Text = txtFreight.Text;
        //        CalculateTotalAmount();
        //        txtFreight.Focus();
        //    }
        //}

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            InvoiceBO optbo = new InvoiceBO();
            NumberToWords wordBO = new NumberToWords();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillDropDowns();
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Srl = 0;
                    optbo.BasicAmount = decimal.Parse(txtBasicAmount.Text);
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.EnteredBy = long.Parse(Session["EID"].ToString());
                    optbo.ItemXML = GetItemXML();
                    optbo.PaymentTerms = txtPaymentTerms.Text.Trim();
                    optbo.Particulars = txtParticulars.Text.Trim();
                    optbo.AmountInWords = wordBO.Num2WordConverter(txtBasicAmount.Text).ToString();
                    optbo.BuyerRefNo = txtBRefNo.Text.Trim();
                    optbo.BuyerRefDate = txtBdate.Text.Trim();
                    optbo.InvoiceDate = txtDate.Text.Trim();
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.BiddingCharges = txtBidding.Text.Trim() == string.Empty ? 0 : decimal.Parse(txtBidding.Text);
                    optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
                    long cnt = optbo.InsertFinalInvoiceNew();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Invoice details saved Successfully. Now you can Print the Invoice!!", true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Invoice Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private string GetItemXML()
        {
            string strxml = String.Empty;
            string LRID = String.Empty;
            string InvoiceID = String.Empty;

            for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                TextBox txtFreight = (TextBox)grdPendingLRs.Rows[i].FindControl("txtFrieght");
                TextBox txtDetention = (TextBox)grdPendingLRs.Rows[i].FindControl("txtDetention");
                TextBox txtWarai = (TextBox)grdPendingLRs.Rows[i].FindControl("txtWarai");
                TextBox txtTwoPoint = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTwoPoint");
                TextBox txtOtherCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtOtherCharges");
                TextBox txtTotalCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTotalCharges");
                TextBox txtDetUnloadingCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtDetUnloadingCharges");
                TextBox txtTransitPenaltyCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTransitPenaltyCharges");
                TextBox txtDetloadingDays = (TextBox)grdPendingLRs.Rows[i].FindControl("txtDetloadingDays");
                TextBox txtDetUnloadingDays = (TextBox)grdPendingLRs.Rows[i].FindControl("txtDetUnloadingDays");
                TextBox txtTransitPenaltyDays = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTransitPenaltyDays");
                TextBox txtLRRemarks = (TextBox)grdPendingLRs.Rows[i].FindControl("txtLRRemarks");
                TextBox txtCoilNo = (TextBox)grdPendingLRs.Rows[i].FindControl("txtCoilNo");

                if (txtDetention.Text == string.Empty) txtDetention.Text = "0";
                if (txtWarai.Text == string.Empty) txtWarai.Text = "0";
                if (txtTwoPoint.Text == string.Empty) txtTwoPoint.Text = "0";
                if (txtOtherCharges.Text == string.Empty) txtOtherCharges.Text = "0";
                if (txtTotalCharges.Text == string.Empty) txtTotalCharges.Text = "0";
                if (txtDetUnloadingCharges.Text == string.Empty) txtDetUnloadingCharges.Text = "0";
                if (txtTransitPenaltyCharges.Text == string.Empty) txtTransitPenaltyCharges.Text = "0";
                if (txtDetloadingDays.Text == string.Empty) txtDetloadingDays.Text = "0";
                if (txtDetUnloadingDays.Text == string.Empty) txtDetUnloadingDays.Text = "0";
                if (txtTransitPenaltyDays.Text == string.Empty) txtTransitPenaltyDays.Text = "0";

                DataKey key = grdPendingLRs.DataKeys[i];
                if (chk.Checked)
                {
                    LRID = key["LRId"].ToString();
                    InvoiceID = "0";
                    strxml = strxml + "<dtItem><LRId>" + LRID + "</LRId><TransportationCharges>" + txtFreight.Text.Trim() + "</TransportationCharges><DetentionCharges>" + txtDetention.Text.Trim() + "</DetentionCharges><WaraiCharges>" + txtWarai.Text.Trim() + "</WaraiCharges><TwoPointCharges>" + txtTwoPoint.Text.Trim() + "</TwoPointCharges><OtherCharges>" + txtOtherCharges.Text + "</OtherCharges><TotalCharges>" + txtTotalCharges.Text + "</TotalCharges><DetentionCharges_Unload>" + txtDetUnloadingCharges.Text + "</DetentionCharges_Unload><TransitPenalty>" + txtTransitPenaltyCharges.Text + "</TransitPenalty><DetentionDaysLoading>" + txtDetloadingDays.Text + "</DetentionDaysLoading><DetentionDaysUnLoading>" + txtDetUnloadingDays.Text + "</DetentionDaysUnLoading><TransitPenaltyDays>" + txtTransitPenaltyDays.Text + "</TransitPenaltyDays><Remarks>" + txtLRRemarks.Text.Trim() + "</Remarks><Srl>" + InvoiceID + "</Srl><CoilNo>" + txtCoilNo.Text.Trim() + "</CoilNo></dtItem>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<NewDataSet>" + strxml + "</NewDataSet>";
            }

            return strxml;
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        //protected void txtFrieght_TextChanged(object sender, EventArgs e)
        //{
        //    TextBox txt = (TextBox)sender;
        //    GridViewRow grdrow = (GridViewRow)txt.Parent.Parent;
        //    int rowIndex = 0;
        //    rowIndex = grdrow.RowIndex;
        //    CalculateRowAmount(rowIndex);

        //    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //    TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //    TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //    TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //    TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //    TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //    TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //    TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");
        //    TextBox txtDetloadingDays = (TextBox)grdrow.FindControl("txtDetloadingDays");
        //    TextBox txtDetUnloadingDays = (TextBox)grdrow.FindControl("txtDetUnloadingDays");
        //    TextBox txtTransitPenaltyDays = (TextBox)grdrow.FindControl("txtTransitPenaltyDays");
        //    TextBox txtLRRemarks = (TextBox)grdrow.FindControl("txtLRRemarks");

        //    switch (txt.ID)
        //    {
        //        case "txtFrieght":
        //            txtDetention.Focus();
        //            break;
        //        case "txtDetention":
        //            txtWarai.Focus();
        //            break;
        //        case "txtWarai":
        //            txtTwoPoint.Focus();
        //            break;
        //        case "txtTwoPoint":
        //            txtDetloadingDays.Focus();
        //            break;
        //        case "txtDetloadingDays":
        //            txtDetUnloadingDays.Focus();
        //            break;
        //        case "txtDetUnloadingDays":
        //            txtDetUnloadingCharges.Focus();
        //            break;
        //        case "txtDetUnloadingCharges":
        //            txtTransitPenaltyDays.Focus();
        //            break;
        //        case "txtTransitPenaltyDays":
        //            txtTransitPenaltyCharges.Focus();
        //            break;
        //        case "txtTransitPenaltyCharges":
        //            txtOtherCharges.Focus();
        //            break;
        //        case "txtOtherCharges":
        //            txtLRRemarks.Focus();
        //            break;
        //        case "txtLRRemarks":
        //            if (rowIndex < grdPendingLRs.Rows.Count - 1)
        //            {
        //                CheckBox chkLR = (CheckBox)grdPendingLRs.Rows[rowIndex + 1].FindControl("chkLR");
        //                chkLR.Focus();
        //            }
        //            else
        //            {
        //                txtPaymentTerms.Focus();
        //            }
        //            break;

        //    }

        //    //txt.Focus();
        //}

        //private void CalculateRowAmount(int rowIndex)
        //{
        //    GridViewRow grdrow = grdPendingLRs.Rows[rowIndex];
        //    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
        //    TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
        //    TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
        //    TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
        //    TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
        //    TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
        //    TextBox txtDetUnloadingCharges = (TextBox)grdrow.FindControl("txtDetUnloadingCharges");
        //    TextBox txtTransitPenaltyCharges = (TextBox)grdrow.FindControl("txtTransitPenaltyCharges");


        //    decimal freightCharges = txtFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtFreight.Text.Trim());
        //    decimal DetentionCharges = txtDetention.Text.Trim() == "" ? 0 : decimal.Parse(txtDetention.Text.Trim());
        //    decimal WaraiCharges = txtWarai.Text.Trim() == "" ? 0 : decimal.Parse(txtWarai.Text.Trim());
        //    decimal twoPointCharges = txtTwoPoint.Text.Trim() == "" ? 0 : decimal.Parse(txtTwoPoint.Text.Trim());
        //    decimal OtherCharges = txtOtherCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtOtherCharges.Text.Trim());
        //    decimal DetentionUnloadingCharges = txtDetUnloadingCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtDetUnloadingCharges.Text.Trim());
        //    decimal TransitPenalty = txtTransitPenaltyCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtTransitPenaltyCharges.Text.Trim());
        //    decimal TotalCharges = freightCharges + DetentionCharges + WaraiCharges + twoPointCharges + OtherCharges + DetentionUnloadingCharges - TransitPenalty;
        //    txtTotalCharges.Text = TotalCharges.ToString("F2");


        //    CalculateTotalAmount();
        //}

        //private void CalculateTotalAmount()
        //{
        //    decimal Total = 0;
        //    for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
        //    {
        //        CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
        //        if (chk.Checked)
        //        {
        //            TextBox txttotal = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTotalCharges");
        //            Total += txttotal.Text == "" ? 0 : decimal.Parse(txttotal.Text.Trim());
        //        }
        //    }
        //    txtBasicAmount.Text = Total.ToString("F2");
        //}

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    popDelPacklist.Hide();
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void ClearView()
        {
            txtDate.Text = string.Empty;
            txtParticulars.Text = string.Empty;
            txtPaymentTerms.Text = string.Empty;           
            txtBasicAmount.Text = string.Empty;            
            SetPanelMsg("", false, 0);
            if (cmbBillingCompany.Items.Count > 0) cmbBillingCompany.SelectedIndex = 0;
            if (cmbCustomer.Items.Count > 0) cmbCustomer.SelectedIndex = 0;
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            cmbBillingCompany.Focus();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For Invoice No.:" + keys["InvRefNo"].ToString();
                LoadGridViewDetails(keys["InvRefNo"].ToString(), long.Parse(keys["BillingCompany_Srl"].ToString()));
            }
            if (e.CommandName == "UpdateBillSubmission")
            {
                if (keys["BillSubmissionDate"].ToString() == "Update Submission Date")
                {
                    hdfldBillInvRef.Value = keys["InvRefNo"].ToString();
                    popBillSubmission.Show();
                }
               
            }
            if (e.CommandName == "ViewReport")
            {
                {
                    Session["InvRefNo"] = keys["InvRefNo"].ToString();
                    Session["BillingCompSrlDoc"] = keys["BillingCompany_Srl"].ToString();
                    Session["ReportType"] = "InvDoc";

                    if (Session["PlntName"].ToString().Trim().Contains("Godrej"))
                        Session["ForGdrej"] = "G";
                    else if (Session["PlntName"].ToString().Trim().Contains("Whirlpool"))
                        Session["ForGdrej"] = "W";
                    else if (Session["PlntName"].ToString().Trim().Contains("LG"))
                        Session["ForGdrej"] = "L";
                    else if (Session["PlntName"].ToString().Trim().Contains("Mahindra"))
                        Session["ForGdrej"] = "M";
                    else
                        Session["ForGdrej"] = "N";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
            if (e.CommandName.ToLower()=="savepdf")
            {
                ReportDocument RptDoc = new ReportDocument();
                ReportBO OptBO = new ReportBO();
                CommonBO comBO = new CommonBO();
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvRefNo = keys["InvRefNo"].ToString();
                OptBO.BillingCompanySrl = long.Parse(keys["BillingCompany_Srl"].ToString());
                DataTable dt = OptBO.Invoice_Doument();

                string fileName = Server.MapPath("~\\Downloads\\In_" + keys["InvRefNo"].ToString().Replace("/","_") + ".pdf");
                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                if (Session["ForGdrej"] == null) Session["ForGdrej"] = "N";

                if ((Session["PlntName"].ToString().Trim().Contains("Godrej")))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentGodrej.rpt"));
                else if (Session["PlntName"].ToString().Trim().Contains("Whirlpool"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentFreightWhirlpool.rpt"));
                else if (Session["PlntName"].ToString().Trim().Contains("LG"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentLG.rpt"));
                else if (Session["PlntName"].ToString().Trim().Contains("Mahindra"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocument_Mahindra.rpt"));
                else
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceDocumentTallyFormat.rpt"));

                RptDoc.SetDataSource(ds);
                //RptDoc.SetParameterValue(0, "N");
                RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                //return fileName;
                Session["FilePath"] = fileName;
                RptDoc.Close();
                RptDoc.Dispose();
                GC.Collect();
                //Response.Redirect("ViewFile.aspx", false);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);

            }

            if (e.CommandName.ToLower()=="annexurepdf")
            {
                ReportDocument RptDoc = new ReportDocument();
                ReportBO OptBO = new ReportBO();
                CommonBO comBO = new CommonBO();
                prjLRTrackerFinanceAuto.Datasets.DsInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsInvoice();
                OptBO.InvRefNo = keys["InvRefNo"].ToString();
                OptBO.BillingCompanySrl = long.Parse(keys["BillingCompany_Srl"].ToString());

                string fileName = Server.MapPath("~\\Downloads\\An_" + keys["InvRefNo"].ToString().Replace("/", "_") + ".pdf");

                DataTable dt = OptBO.Invoice_Doument();
                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                if (Session["PlntName"].ToString().Trim().ToLower().Contains("posco import"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexurePosco.rpt"));
                else if (Session["PlntName"].ToString().Trim().ToLower().Contains("mahindra"))
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexure_Mahindra.rpt"));
                else
                    RptDoc.Load(Server.MapPath("~/Reports/InvoiceItemAnnexure.rpt"));
                 
                RptDoc.SetDataSource(ds);
                
                RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                //return fileName;
                Session["FilePath"] = fileName;
                RptDoc.Close();
                RptDoc.Dispose();
                GC.Collect();
                //Response.Redirect("ViewFile.aspx", false);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
            }

            if (e.CommandName == "ItemReport")
            {
                {
                    Session["InvRefNo"] = keys["InvRefNo"].ToString();
                    Session["BillingCompSrlDoc"] = keys["BillingCompany_Srl"].ToString();
                    Session["ReportType"] = "InvItems";
                    if (keys["Name"].ToString().Trim().Contains("Godrej"))
                        Session["ForGdrej"] = "Y";
                    else
                        Session["ForGdrej"] = "N";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
        }

        //private void SavePdfLRForDownLoad(string LRNo)
        //{
        //    ReportDocument RptDoc = new ReportDocument();
        //    ReportBO OptBO = new ReportBO();
        //    prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
        //    OptBO.ID = long.Parse(LRNo);
        //    DataTable dt = OptBO.LR_Document();
        //    string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
        //    ds.Tables.RemoveAt(0);
        //    ds.Tables.Add(dt);
        //    RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
        //    //condbsLogon(RptDoc);
        //    RptDoc.SetDataSource(ds);
        //    RptDoc.SetParameterValue(0, "N");
        //    RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
        //    //return fileName;
        //    Session["FilePath"] = fileName;
        //    RptDoc.Close();
        //    RptDoc.Dispose();
        //    GC.Collect();
        //    //Response.Redirect("ViewFile.aspx", false);
        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        //}

        private void LoadGridViewDetails(string InvRefNo, long BillingCompanySrl)
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.InvRefNo = InvRefNo;
            optBO.BillingCompanySrl = BillingCompanySrl;  
            DataTable dt = optBO.FillMainInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        //protected void chkLR_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBox chk = (CheckBox)sender;
        //    GridViewRow grdrow = (GridViewRow)chk.Parent.Parent;
        //    int rowIndex = 0;
        //    rowIndex = grdrow.RowIndex;

        //    if (chk.Checked)
        //    {
        //        EnableGridAmounts(rowIndex);
        //    }
        //    else
        //    {
        //        DisableGridAmounts(rowIndex);
        //    }
        //}

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            FillPendingCustomers();
            cmbCustomer.Focus();
        }

        protected void grdPendingLRs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               
                string lsDataKeyValue = grdPendingLRs.DataKeys[e.Row.RowIndex]["PODPhysicalDate"].ToString();
                string podFile = "";
                if (grdPendingLRs.DataKeys[e.Row.RowIndex]["PODFile"] != null)
                    podFile = grdPendingLRs.DataKeys[e.Row.RowIndex]["PODFile"].ToString();
                if (!string.IsNullOrEmpty(lsDataKeyValue))
                {
                    e.Row.ForeColor = System.Drawing.Color.Green;
                }
                if (string.IsNullOrEmpty(podFile))
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    e.Row.Font.Bold = true;
                }
            }
        }

        protected void btnUpdateBillSubmission_Click(object sender, EventArgs e)
        {
            InvoiceBO optbo = new InvoiceBO();
            optbo.MasterInvref = hdfldBillInvRef.Value;
            optbo.BillSubmissionDate = txtBillSubmissionDate.Text.Trim();
            long cnt = optbo.UpdateBillSubmissionDate();
            if (cnt>0)
            {
                LoadGridView();
                popBillSubmission.Hide();
            }
        }

        protected void btnCancelBillSubmission_Click(object sender, EventArgs e)
        {
            popBillSubmission.Hide();
        }

        //protected void cmbInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    grdPendingLRs.DataSource = null;
        //    grdPendingLRs.DataBind();
        //    InvoiceBO optbo = new InvoiceBO();
        //    optbo.Srl = long.Parse(cmbInvoiceNo.SelectedValue);
        //    DataTable dt = optbo.FillFinalInvoicePendingLRS();
        //    grdPendingLRs.DataSource = dt;
        //    grdPendingLRs.DataBind();
        //    if (dt.Rows.Count > 0)
        //    {
        //        txtDate.Text = DateTime.Parse(dt.Rows[0]["InvoiceDate"].ToString()).ToString("dd/MM/yyyy");
        //    }
        //    DisableGridAmounts(-1);
        //}
    }
}