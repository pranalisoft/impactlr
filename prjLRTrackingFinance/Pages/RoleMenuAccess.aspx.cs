﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class RoleMenuAccess : System.Web.UI.Page
    {
        enum ddlFillType { roles };
        LogWriter logger;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ddlRoles.Focus();
                Fillddl(ddlFillType.roles, ddlRoles);
            }
        }

        private void Fillddl(ddlFillType ddtype, DropDownList ddlname)
        {
            try
            {
                RoleMenuAccessDAO roleMenuAccess = new RoleMenuAccessDAO();
                ddlname.DataSource = null;
                ddlname.DataBind();
                switch (ddtype)
                {
                    case ddlFillType.roles:
                        ddlRoles.Items.Clear();
                        ddlRoles.Items.Add(new ListItem("Select Roles", "-1"));
                        ddlname.DataSource = roleMenuAccess.GetRoles();
                        ddlname.DataTextField = "CodeName";
                        ddlname.DataValueField = "srl";
                        ddlname.DataBind();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-Fill DDL Function", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelMsg("", false, 0);
                long ans = 0;
                RoleMenuAccessDAO roleDAo = new RoleMenuAccessDAO();
                string strmenuxml = "";
                string strGridxml = "";
                int defaultCnt = 0;
                if (ddlRoles.SelectedIndex > 0)
                {
                    strmenuxml = "<DocumentElement><Roles>";
                    strGridxml = "<DocumentElement><DashboardGrids>";
                    for (int i = 0; i < grdEmpRoleMst.Rows.Count; i++)
                    {
                        CheckBox chkBx = (CheckBox)grdEmpRoleMst.Rows[i].FindControl("chkSelect");
                        if (chkBx.Checked)
                        {
                            strmenuxml = strmenuxml + "<Role><MenuItem>" + grdEmpRoleMst.DataKeys[i].Values["Srl"].ToString() + "</MenuItem>";
                            strmenuxml = strmenuxml + "<RoleSrl>" + ddlRoles.SelectedValue.ToString() + "</RoleSrl></Role>";
                        }
                    }
                    for (int i = 0; i < grdViewDashboardGrids.Rows.Count; i++)
                    {
                        CheckBox chkBxGrid = (CheckBox)grdViewDashboardGrids.Rows[i].FindControl("chkSelectGrid");
                        CheckBox chkSelDefault = (CheckBox)grdViewDashboardGrids.Rows[i].FindControl("chkSelDefault");
                        if (chkBxGrid.Checked)
                        {
                            string rdbChecked = "0";
                            if (chkSelDefault.Checked)
                            {
                                defaultCnt = defaultCnt + 1;
                                rdbChecked = "1";
                            }
                            strGridxml = strGridxml + "<Grid><GridId>" + grdViewDashboardGrids.DataKeys[i].Values["Srl"].ToString() + "</GridId>";
                            strGridxml = strGridxml + "<IsDefault>" + rdbChecked + "</IsDefault>";
                            strGridxml = strGridxml + "<RoleSrl>" + ddlRoles.SelectedValue.ToString() + "</RoleSrl></Grid>";
                        }
                    }
                    strmenuxml = strmenuxml + "</Roles></DocumentElement>";
                    strGridxml = strGridxml + "</DashboardGrids></DocumentElement>";
                }

                if (defaultCnt != 1)
                {
                    SetPanelMsg("Please Select Default Grid To Load.", true, 0);
                    return;
                }

                ans = roleDAo.InsertRoleMenuAccess(long.Parse(ddlRoles.SelectedValue.ToString()), ddlRoles.Text.ToString(), strmenuxml, strGridxml);
                if (ans > 0)
                {
                    SetPanelMsg("Menu Assigned Successfully", true, 1);                    
                }
                else
                {
                    SetPanelMsg("Operation Failed. Can Not Insert Record.", true, 0);
                }
                FillGridDashboard();
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-Save Button Click.", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        private void ClearView(View view)
        {
            foreach (Control control in view.Controls)
            {
                if (control.Controls.Count == 0)
                {
                    if (control is Label || control is LiteralControl || control is Button)
                    {
                        continue;
                    }
                    if (control is TextBox)
                    {
                        ((TextBox)control).Text = string.Empty;
                    }
                    else if (control is CheckBox)
                    {
                        if (((CheckBox)control).ToolTip.Equals("Default Active"))
                            continue;
                        ((CheckBox)control).Checked = false;
                    }
                    else if (control is DropDownList)
                    {
                        ((DropDownList)control).SelectedIndex = -1;
                    }
                    else if (control is RadioButtonList)
                    {
                        ((RadioButtonList)control).ClearSelection();
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ClearView(viewEntry);
                SetPanelMsg("", false, 0);
                ClearView(viewEntry);
                Response.Redirect("LRMain.aspx", false);
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access - Cancel Click.", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        private void FillGrid()
        {
            try
            {
                grdEmpRoleMst.DataSource = new RoleMenuAccessDAO().ShowMenuAssigned(long.Parse(ddlRoles.SelectedValue.ToString()));
                grdEmpRoleMst.DataBind();
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-Confirm_Command Click.", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        private void FillGridDashboard()
        {
            try
            {
                grdViewDashboardGrids.DataSource = new RoleMenuAccessDAO().ShowGridAssigned(long.Parse(ddlRoles.SelectedValue.ToString()));
                grdViewDashboardGrids.DataBind();

                for (int i = 0; i < grdViewDashboardGrids.Rows.Count; i++)
                {
                    CheckBox chkBxGrid = (CheckBox)grdViewDashboardGrids.Rows[i].FindControl("chkSelectGrid");
                    CheckBox chkSelDefault = (CheckBox)grdViewDashboardGrids.Rows[i].FindControl("chkSelDefault");

                    if (chkBxGrid.Checked)
                    {
                        chkSelDefault.Enabled = true;
                    }
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-Confirm_Command Click.", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "yes")
                {
                    //Response.Redirect("JobWorkMain.aspx", false);
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-Confirm_Command Click.", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }

        }

        protected void ddlRoles_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                if (ddlRoles.SelectedIndex > 0)
                {
                    FillGrid();
                    FillGridDashboard();
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Role Menu Access-ddlRoles Selected Index Change.", exp);
            }
        }

        protected void grdEmpRoleMst_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk1 = (CheckBox)e.Row.FindControl("chkSelect");
                if (long.Parse(grdEmpRoleMst.DataKeys[e.Row.RowIndex].Values["Sel"].ToString()) > 0)
                    chk1.Checked = true;
                else
                    chk1.Checked = false;

            }
        }
    }
}
