﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using AjaxControlToolkit;
using System.Net.Mail;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PTLLREntry : System.Web.UI.Page
    {
        enum DropDownTypes
        {
            Consignee, Consigner, Origin, Destination, VehicleNo, PackingType, Description
        }

        #region "Procedure"
        //private void SetPanelMsg(string msg, bool visible, int code)
        //{
        //    if (visible)
        //    {
        //        lblMsg.Text = msg;
        //        pnlMsg.Visible = true;
        //        if (code == 1)
        //        {
        //            pnlMsg.Style.Add("border", "solid 1px #336600");
        //            pnlMsg.Style.Add("color", "black");
        //            pnlMsg.Style.Add("background-color", "#9EDC7F");
        //        }
        //        else
        //        {
        //            pnlMsg.Style.Add("border", "solid 1px #CE180E");
        //            pnlMsg.Style.Add("color", "white");
        //            pnlMsg.Style.Add("background-color", "#D20000");
        //        }
        //    }
        //    else
        //    {
        //        lblMsg.Text = string.Empty;
        //        pnlMsg.Visible = false;
        //    }
        //}

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void LoadGridView()
        {
            LRBO optBO = new LRBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.IsPending = false;
            optBO.IsPTL = true;
            DataTable dt = optBO.FillLRGridPTL();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDetails(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataSet dt = optBO.FillStatusAndItemForLR();
            if (dt.Tables.Count > 0)
            {
                grdViewItemDetls.DataSource = dt.Tables[0];
                grdViewItemDetls.DataBind();
                lblStatusCnt.Text = dt.Tables[0].Rows.Count.ToString();

                grdViewItemDetlsPTL.DataSource = dt.Tables[1];
                grdViewItemDetlsPTL.DataBind();
                lblItemCount.Text = dt.Tables[1].Rows.Count.ToString();
            }
        }

        private void LoadGridViewDetailsEditMode(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataSet dt = optBO.FillStatusAndItemForLR();
            if (dt.Tables.Count > 0)
            {
                grdItems.DataSource = dt.Tables[1];
                grdItems.DataBind();
                Session["dtItem"] = dt.Tables[1];
                lblItemCount.Text = dt.Tables[1].Rows.Count.ToString();
            }
        }

        private void ClearView()
        {
            txtVehicleNo.Enabled = true;
            txtLRNo.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtRefNo.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            txtInvoiceValue.Text = string.Empty;
            txtInvoiceDate.Text = string.Empty;
            cmbDescription.SelectedIndex = -1;

            txtDriverDetails.Text = string.Empty;

            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //CalendarExtender3.EndDate = DateTime.Parse(txtDate.Text);
            ddlHH.Text = DateTime.Now.ToString("HH");
            ddlmm.Text = DateTime.Now.ToString("mm");
            cmbBillingCompany.SelectedIndex = -1;
            txtConsigneeAddress.Text = string.Empty;
            txtConsigneeContact.Text = string.Empty;
            txtConsigneeEmail.Text = string.Empty;
            cmbFrom.SelectedIndex = -1;
            cmbTo.SelectedIndex = -1;
            txtDriverNumber.Text = string.Empty;
            txtvehicleSealNo.Text = string.Empty;
            txtVehicleNo.Text = string.Empty;
            txtConsignorGSTNo.Text = string.Empty;
            txtConsigneeGSTNo.Text = string.Empty;
            txtSuplBuying.Text = string.Empty;
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
        }

        private void FillDropdown(DropDownTypes cmbtype)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            switch (cmbtype)
            {
                case DropDownTypes.Consignee:
                    //OptBO.FillType = "S";
                    //cmbConsignee.DataSource = null;
                    //cmbConsignee.Items.Clear();
                    //cmbConsignee.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsignee.DataSource = dt;
                    //cmbConsignee.DataTextField = "Consignee";
                    //cmbConsignee.DataValueField = "Consignee";
                    //cmbConsignee.DataBind();
                    //cmbConsignee.SelectedIndex = 0;
                    break;
                case DropDownTypes.Consigner:
                    //OptBO.FillType = "C";
                    //cmbConsigner.DataSource = null;
                    //cmbConsigner.Items.Clear();
                    //cmbConsigner.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsigner.DataSource = dt;
                    //cmbConsigner.DataTextField = "Consigner";
                    //cmbConsigner.DataValueField = "Consigner";
                    //cmbConsigner.DataBind();
                    //cmbConsigner.SelectedIndex = 0;
                    break;
                case DropDownTypes.Origin:
                    OptBO.FillType = "O";
                    cmbFrom.DataSource = null;
                    cmbFrom.Items.Clear();
                    cmbFrom.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRCombo();
                    cmbFrom.DataSource = dt;
                    cmbFrom.DataTextField = "FromLoc";
                    cmbFrom.DataValueField = "FromLoc";
                    cmbFrom.DataBind();
                    cmbFrom.SelectedIndex = 0;
                    break;
                case DropDownTypes.Destination:
                    //OptBO.FillType = "D";
                    //cmbTo.DataSource = null;
                    //cmbTo.Items.Clear();
                    //cmbTo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbTo.DataSource = dt;
                    //cmbTo.DataTextField = "ToLoc";
                    //cmbTo.DataValueField = "ToLoc";
                    //cmbTo.DataBind();
                    //cmbTo.SelectedIndex = 0;
                    break;
                case DropDownTypes.VehicleNo:
                    OptBO.FillType = "V";
                    //cmbVehicleNo.DataSource = null;
                    //cmbVehicleNo.Items.Clear();
                    //cmbVehicleNo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbVehicleNo.DataSource = dt;
                    //cmbVehicleNo.DataTextField = "VehicleNo";
                    //cmbVehicleNo.DataValueField = "VehicleNo";
                    //cmbVehicleNo.DataBind();
                    //cmbVehicleNo.SelectedIndex = 0;
                    break;
                case DropDownTypes.Description:
                    OptBO.SearchText = string.Empty;
                    cmbDescription.DataSource = null;
                    cmbDescription.Items.Clear();
                    cmbDescription.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRDescriptionCombo();
                    cmbDescription.DataSource = dt;
                    cmbDescription.DataTextField = "Name";
                    cmbDescription.DataValueField = "Name";
                    cmbDescription.DataBind();
                    cmbDescription.SelectedIndex = 0;
                    break;
                case DropDownTypes.PackingType:
                    OptBO.SearchText = string.Empty;
                    cmbType.DataSource = null;
                    cmbType.Items.Clear();
                    cmbType.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillPackingType();
                    cmbType.DataSource = dt;
                    cmbType.DataTextField = "Name";
                    cmbType.DataValueField = "Name";
                    cmbType.DataBind();
                    cmbType.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
        }

        private void FillDropDownDevice(long DeviceID = 0)
        {
            //LRBO OptBO = new LRBO();
            //DataTable dt;
            //cmbDevice.DataSource = null;
            //cmbDevice.Items.Clear();
            //cmbDevice.Items.Add(new ListItem("", "-1"));
            //OptBO.DeviceID = DeviceID;
            //dt = OptBO.FillDropDownDevice();
            //cmbDevice.DataSource = dt;
            //cmbDevice.DataTextField = "Name";
            //cmbDevice.DataValueField = "Srl";
            //cmbDevice.DataBind();
            //cmbDevice.SelectedIndex = 0;
        }

        private void FillDropDownVehicleType()
        {
            VehicleTypeBO OptBO = new VehicleTypeBO();
            DataTable dt;
            cmbVehicleType.DataSource = null;
            cmbVehicleType.Items.Clear();
            cmbVehicleType.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetVehicleTypedetails();
            cmbVehicleType.DataSource = dt;
            cmbVehicleType.DataTextField = "Name";
            cmbVehicleType.DataValueField = "Srl";
            cmbVehicleType.DataBind();
            cmbVehicleType.SelectedIndex = 0;
        }

        private void FillDropDownCustomer()
        {
            CustomerBO OptBO = new CustomerBO();
            DataTable dt;
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            OptBO.ShowAll = "Y";
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            dt = OptBO.GetCustomerDetailsUserWise();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        private void FillDropDownSupplier()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void FillDropDownBillingCompany()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 1;
        }

        private void FillDropDownFrom()
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbFrom.DataSource = null;
            cmbFrom.Items.Clear();
            cmbFrom.Items.Add(new ListItem("", "-1"));
            OptBO.FromOrTo = "F";
            dt = OptBO.GetFromOrToLocationsPTL();
            cmbFrom.DataSource = dt;
            cmbFrom.DataTextField = "FromLoc";
            cmbFrom.DataValueField = "FromLoc";
            cmbFrom.DataBind();
            cmbFrom.SelectedIndex = 0;
        }

        private void FillDropDownDestination(string FromLoc)
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbTo.DataSource = null;
            cmbTo.Items.Clear();
            cmbTo.Items.Add(new ListItem("", "-1"));
            OptBO.FromLoc = FromLoc;
            dt = OptBO.GetToLocationsPTL();
            cmbTo.DataSource = dt;
            cmbTo.DataTextField = "ToLoc";
            cmbTo.DataValueField = "ToLoc";
            cmbTo.DataBind();
            cmbTo.SelectedIndex = 0;
        }

        private void FillDropDownConsignor()
        {
            //ConsignorBO OptBO = new ConsignorBO();
            //DataTable dt;
            //cmbConsigner.DataSource = null;
            //cmbConsigner.Items.Clear();
            //cmbConsigner.Items.Add(new ListItem("", "-1"));
            //OptBO.SearchText = string.Empty;
            //dt = OptBO.GetConsignordetails();
            //cmbConsigner.DataSource = dt;
            //cmbConsigner.DataTextField = "Name";
            //cmbConsigner.DataValueField = "Srl";
            //cmbConsigner.DataBind();
            //cmbConsigner.SelectedIndex = 0;
        }

        private void FillPreviousDetails()
        {
            LRBO optBO = new LRBO();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = optBO.FillLRPreviousDetails();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["CName"] != null) setCombobox(cmbBillingCompany, dt.Rows[0]["CName"].ToString());
                    if (dt.Rows[0]["Consigner"] != null) setCombobox(cmbConsigner, dt.Rows[0]["Consigner"].ToString());
                    txtConsignerAddress.Text = dt.Rows[0]["ConsignerAddress"].ToString();
                    txtConsignorGSTNo.Text = dt.Rows[0]["ConsignorGSTNo"].ToString();
                    cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    if (dt.Rows[0]["FromLoc"] != null) setCombobox(cmbFrom, dt.Rows[0]["FromLoc"].ToString());
                    cmbFrom_SelectedIndexChanged(cmbFrom, EventArgs.Empty);
                }
            }
        }

        private void setCombobox(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.Text = lst.Value;
            else
            {
                lst = new ListItem(FieldName, FieldName);
                cmb.Items.Add(lst);
                cmb.Text = FieldName;
            }
        }

        private void setComboboxValue(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.SelectedValue = lst.Value;
            else
            {
                cmb.SelectedIndex = 0;
            }
        }

        private void SendMail()
        {
            //        string IsMail = ConfigurationManager.AppSettings["IsMail"].ToString();

            //        if (IsMail == "Y")
            //        {
            //            string expectedDateOfReporting = txtHiddenExpDate.Value;
            //            string emailid = txtConsigneeEmail.Text;
            //            string subject = "Intimation of Dispatch";
            //            string body = CommonTypes.GetMailCotent("L");
            //            string filepath = "";



            //            string fromId = ConfigurationManager.AppSettings["FromId"].ToString();
            //            string CCId = ""; // ConfigurationManager.AppSettings["CCId"].ToString();
            //            MailHandler mailhandler = new MailHandler();

            //            string FilePath = SavePdfLRForEmail(txtLRNo.Text.Trim().Replace("/", ""));

            //            body = string.Format(body, cmbDescription.Text, txtDate.Text, cmbConsigner.SelectedItem.Text, cmbFrom.Text, txtLRNo.Text, txtDate.Text, txtVehicleNo.Text, expectedDateOfReporting, txtRefNo.Text);

            //            body = "<html><head><style>body {font-family:arial;font-size:10pt;} td {font-family:arial;font-size:9pt;}</style></head><body bgcolor=#ffffff><table width=100%><tr bgcolor=#336699><td align=left style=font-weight:bold><font color=white> " +
            //                       " LR Tracker Tool</font></td></tr></table> <table> <tr> <td>" + body;

            //            body = body + "</td></tr></table><br/><br/><br/>" +
            //"<hr size=2 width=100% align=center><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td>NOTE: This is a system generated notification, please <font color=red>DO NOT REPLY</font> to this mail.</td></tr></table><br><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td  valign=top style={font-family:verdana;font-size:60%;color:#888888}>This" +
            //"e-mail, and any attachments thereto, are intended only for use by the addressee(s) named herein. If you are not the intended recipient of this e-mail, you are hereby notified that any dissemination, distribution or copying which amounts to misappropriation of this e-mail and any attachments thereto, is strictly prohibited. If you have received this e-mail in error, please immediately notify me and permanently delete the original and any copy of any e-mail and " +
            //"any printout thereof.</td></tr></table></body></html>";

            //            MailMessage msg = mailhandler.CreateMessage(emailid, CCId, subject, body, true, FilePath);
            //            bool result = mailhandler.SendEmail(msg, true, true, false);
            //        }

        }

        private void SendMailTemp()
        {
            string smtpAddress = "smtp.bizmail.yahoo.com";
            int portNumber = 465;
            bool enableSSL = true;

            string emailFrom = "Transport.lr@abhigroup.co.in";
            string password = "TLR@1234";
            string emailTo = "onkarmedhekar@gmail.com";
            string subject = "Hello";
            string body = "Hello, Test Mail from lrtracker auto";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Can set to false, if you are sending pure text.

                //mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));
                //mail.Attachments.Add(new Attachment("C:\\SomeZip.zip"));

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        private string SavePdfLRForEmail(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(txtHiddenId.Value);
            DataTable dt = OptBO.LR_Document();
            string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, "Y");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            return fileName;
        }

        private void SavePdfLRForDownLoad(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(LRNo);
            DataTable dt = OptBO.LR_Document();

            prjLRTrackerFinanceAuto.Datasets.DSPTLAnnexure dsAnn = new prjLRTrackerFinanceAuto.Datasets.DSPTLAnnexure();
            OptBO.ID = long.Parse(LRNo);
            DataTable dtAnn = OptBO.LR_Annexure();

            string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);

            dsAnn.Tables.RemoveAt(0);
            dsAnn.Tables.Add(dtAnn);
            RptDoc.Load(Server.MapPath("~/Reports/LRPTL.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.Subreports[0].SetDataSource(dsAnn);
            RptDoc.SetParameterValue(0, "N");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            //return fileName;
            Session["FilePath"] = fileName;
            RptDoc.Close();
            RptDoc.Dispose();
            GC.Collect();
            //Response.Redirect("ViewFile.aspx", false);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        }

        private void FillDropDownItem(long CustomerId, long VehicleTypeId, string FromLoc, string ToLoc)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            cmbItem.DataSource = null;
            cmbItem.Items.Clear();
            cmbItem.Items.Add(new ListItem("", "-1"));
            OptBO.CustomerID = CustomerId;
            OptBO.VehicleTypeID = VehicleTypeId;
            OptBO.FromLoc = FromLoc;
            OptBO.ToLoc = ToLoc;
            dt = OptBO.FillItem();
            cmbItem.DataSource = dt;
            cmbItem.DataTextField = "Item";
            cmbItem.DataValueField = "Item";
            cmbItem.DataBind();
            cmbItem.SelectedIndex = 0;
        }

        private void FillDropDownItemZoneType(long CustomerId, long VehicleTypeId, string Item, string FromLoc, string ToLoc)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            cmbZoneType.DataSource = null;
            cmbZoneType.Items.Clear();
            cmbZoneType.Items.Add(new ListItem("", "-1"));
            OptBO.CustomerID = CustomerId;
            OptBO.VehicleTypeID = VehicleTypeId;
            OptBO.Item = Item;
            OptBO.FromLoc = FromLoc;
            OptBO.ToLoc = ToLoc;
            dt = OptBO.FillItemZoneType();
            cmbZoneType.DataSource = dt;
            cmbZoneType.DataTextField = "ZoneType";
            cmbZoneType.DataValueField = "ZoneTypeId";
            cmbZoneType.DataBind();
            cmbZoneType.SelectedIndex = 0;
        }

        private void FillDropDownItemZoneTypeSlab(long CustomerId, long VehicleTypeId, string Item, long ZoneTypeId, string FromLoc, string ToLoc)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            cmbSlab.DataSource = null;
            cmbSlab.DataBind();
            cmbSlab.Items.Clear();
            ListItem lst = new ListItem("", "-1");
            cmbSlab.Items.Add(lst);

            OptBO.CustomerID = CustomerId;
            OptBO.VehicleTypeID = VehicleTypeId;
            OptBO.Item = Item;
            OptBO.ZoneTypeID = ZoneTypeId;
            OptBO.FromLoc = FromLoc;
            OptBO.ToLoc = ToLoc;
            dt = OptBO.FillItemZoneTypeSlab();
            cmbSlab.DataTextField = "Slab";
            cmbSlab.DataValueField = "Slab";
            cmbSlab.DataSource = dt;
            cmbSlab.DataBind();
            cmbSlab.SelectedIndex = 0;
        }

        private void FillItemDetails(long CustomerId, long VehicleTypeId, string Item, long ZoneTypeId, string Slab, string FromLoc, string ToLoc)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            OptBO.CustomerID = CustomerId;
            OptBO.VehicleTypeID = VehicleTypeId;
            OptBO.Item = Item;
            OptBO.ZoneTypeID = ZoneTypeId;
            OptBO.Slab = Slab;
            OptBO.FromLoc = FromLoc;
            OptBO.ToLoc = ToLoc;
            dt = OptBO.FillItemDetails();
            RatePerQtyBuying.Value = "0";
            RatePerKgBuying.Value = "0";
            RatePerQtySelling.Value = "0";
            RatePerKgSelling.Value = "0";
            FixedRateSelling.Value = "0";
            FixedRateBuying.Value = "0";
            MOQ.Value = "0";
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    RatePerQtyBuying.Value = dt.Rows[0]["RatePerQtyBuying"].ToString();
                    RatePerKgBuying.Value = dt.Rows[0]["RatePerKgBuying"].ToString();
                    RatePerQtySelling.Value = dt.Rows[0]["RatePerQtySelling"].ToString();
                    RatePerKgSelling.Value = dt.Rows[0]["RatePerKgSelling"].ToString();
                    MOQ.Value = dt.Rows[0]["MOQ"].ToString();
                    FixedRateSelling.Value = dt.Rows[0]["IsFixedRateSelling"].ToString();
                    FixedRateBuying.Value = dt.Rows[0]["IsFixedRateBuying"].ToString();

                    if (decimal.Parse(RatePerQtyBuying.Value) > 0)
                    {
                        lblQtyWt.Text = "Qty";
                        //grdItems.Columns[3].Visible = true;
                        //grdItems.Columns[4].Visible = false;
                    }
                    else
                    {
                        lblQtyWt.Text = "Wt";
                        //grdItems.Columns[3].Visible = false;
                        //grdItems.Columns[4].Visible = true;
                    }
                }
            }
        }

        private void clearItems()
        {
            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtItem") == null)
            {
                dt = CommonTypes.CreateDataTable("Item,ZoneTypeId,ZoneType,Slab,Qty,ChargeableWt,RatePerQtySelling,RatePerKgSelling,RatePerQtyBuying,RatePerKgBuying,MOQ,IsFixedRateSelling,IsFixedRateBuying", "dtItem", "string,long,string,string,decimal,decimal,decimal,decimal,decimal,decimal,decimal,int,int");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtItem");
            }
            dt.Rows.Clear();
            dt.AcceptChanges();
            grdItems.DataSource = dt;
            grdItems.DataBind();
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                if (int.Parse(Session["BranchId"].ToString())==169)
                {
                    lblSupplierBuying.Visible = true;
                    txtSuplBuying.Visible = true;
                    reqFVSupplierBuying.Visible = true;
                }
                else
                {
                    lblSupplierBuying.Visible = false;
                    txtSuplBuying.Visible = false;
                    reqFVSupplierBuying.Visible = false;
                }
                txtSearch.Focus();
            }
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                    //    LoadGridView();
                    //    MsgPanel.Message = "Record(s) deleted successfully.";
                    //    MsgPanel.DispCode = 1;
                    //    txtFromDate.Focus();
                    //}
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            LRBO optbo = new LRBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //txtLRNo.Enabled = true;
                    //FillDropdown(DropDownTypes.Consignee);
                    //FillDropdown(DropDownTypes.Consigner);
                    //FillDropdown(DropDownTypes.Destination);
                    //FillDropdown(DropDownTypes.Origin);
                    FillDropdown(DropDownTypes.VehicleNo);
                    FillDropdown(DropDownTypes.PackingType);
                    FillDropdown(DropDownTypes.Description);
                    FillDropDownDevice();
                    FillDropDownVehicleType();
                    FillDropDownCustomer();
                    FillDropDownSupplier();
                    FillDropDownFrom();
                    //FillDropDownDestination();
                    FillDropDownConsignor();
                    ClearView();
                    txtHiddenId.Value = "0";
                    FillDropDownBillingCompany();
                    cmbSupplier.Enabled = true;
                    cmbCustomer.Enabled = true;
                    cmbVehicleType.Enabled = true;
                    txtVehicleNo.Enabled = true;

                    cmbFrom.Enabled = true;
                    cmbTo.Enabled = true;
                    cmbBillingCompany.Enabled = true;
                    clearItems();
                    FillPreviousDetails();
                    btnSave.Visible = false;
                    btnSavePacklist.Visible = false;
                    ShowViewByIndex(0);
                    cmbBillingCompany.SelectedValue = Session["BillingCompanySrl"].ToString();
                    txtRefNo.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    optbo.LRNo = txtLRNo.Text.Trim();
                    optbo.ID = long.Parse(txtHiddenId.Value);
                    if (txtLRNo.Text.Trim() != string.Empty && optbo.LRDuplicateCount() > 0)
                    {
                        MsgPopUp.ShowModal("Duplicate LR No.", CommonTypes.ModalTypes.Error);
                        cmbBillingCompany.Focus();
                        return;
                    }

                    DateTime lrdate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //DateTime placementdate = DateTime.ParseExact(txtVehiclePlacementDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);                   

                    if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
                    {
                        MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Destination Master!!", CommonTypes.ModalTypes.Error);
                        return;
                    }
                    DataTable dt = null;
                    if (CommonTypes.GetDataTable("dtItem") == null)
                    {
                        MsgPopUp.ShowModal("Please enter atleast single line item to proceed", CommonTypes.ModalTypes.Error);
                        cmbItem.Focus();
                        return;
                    }
                    else
                    {
                        dt = CommonTypes.GetDataTable("dtItem");
                    }
                    string itemXML = "";
                    using (var stringWriter = new System.IO.StringWriter())
                    {
                        dt.WriteXml(stringWriter, true);
                        itemXML = stringWriter.ToString();
                    }

                    if (itemXML == string.Empty)
                    {
                        MsgPopUp.ShowModal("Please enter atleast single line item to proceed", CommonTypes.ModalTypes.Error);
                        cmbItem.Focus();
                        return;
                    }

                    optbo.LRDatetime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text);
                    optbo.Consignee = cmbConsignee.SelectedItem.Text.Trim();
                    optbo.Consigner = cmbConsigner.SelectedItem.Text.Trim();
                    optbo.Origin = cmbFrom.Text.Trim();
                    optbo.Destination = cmbTo.SelectedItem.Text.Trim();
                    optbo.VehicleNo = txtVehicleNo.Text.Trim();
                    optbo.Weight = txtWeight.Text.Trim() == "" ? 0 : decimal.Parse(txtWeight.Text.Trim());
                    optbo.ChargeableWt = txtChWeight.Text.Trim() == "" ? 0 : decimal.Parse(txtChWeight.Text.Trim());
                    optbo.TotPackage = long.Parse(txttotPackage.Text);
                    optbo.InvoiceNo = txtInvoiceNo.Text.Trim();
                    optbo.InvoiceValue = decimal.Parse(txtInvoiceValue.Text);
                    optbo.LoadType = cmbType.Text.Trim().Substring(0, 1);
                    optbo.Remarks = cmbDescription.SelectedItem.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Status = "P";
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.DriverDtls = txtDriverDetails.Text.Trim();
                    optbo.DeviceID = 0;

                    //if (cmbDevice.Items.Count <= 0)
                    //    optbo.DeviceID = 0;
                    //else if (cmbDevice.SelectedValue == null)
                    //    optbo.DeviceID = 0;
                    //else
                    //    optbo.DeviceID = long.Parse(cmbDevice.SelectedValue);

                    optbo.RefNo = txtRefNo.Text.Trim();
                    optbo.SupplierID = int.Parse(cmbSupplier.SelectedValue);
                    optbo.SubSupplierID = 0;
                    optbo.CustomerID = long.Parse(cmbCustomer.SelectedValue);
                    optbo.TotalFrieght = 0;
                    optbo.SupplierAmount = txtSuplBuying.Text.Trim() == "" ? 0 : decimal.Parse(txtSuplBuying.Text);
                    optbo.AdvanceToSupplier = 0;
                    optbo.VehicleTypeID = long.Parse(cmbVehicleType.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.Rate = 0;
                    optbo.ConsignorAddress = txtConsignerAddress.Text.Trim();
                    optbo.ConsigneeAddress = txtConsigneeAddress.Text.Trim();
                    optbo.ConsigneeEmail = txtConsigneeEmail.Text.Trim();
                    optbo.ConsigneeContact = txtConsigneeContact.Text.Trim();
                    optbo.VehicleSealNo = txtvehicleSealNo.Text.Trim();
                    optbo.PackingType = cmbType.SelectedItem.Text;
                    optbo.VehiclePlacementDate = DateTime.Now.ToString("dd/MM/yyyy");
                    optbo.ExpectedDeliveryDays = int.Parse(HFExpectedDeliveryDays.Value);
                    optbo.DriverNumber = txtDriverNumber.Text.Trim();
                    optbo.FreightType = cmbFreight.SelectedItem.Text;
                    optbo.ConsignorGSTNo = txtConsignorGSTNo.Text.Trim();
                    optbo.ConsigneeGSTNo = txtConsigneeGSTNo.Text.Trim();
                    optbo.RefNo_Invoice = "";
                    optbo.InvoiceDate = txtInvoiceDate.Text.Trim();
                    optbo.EWayBillNo = txtEwayBillNo.Text.Trim();
                    optbo.ShipmentDocNo = "";
                    optbo.InternalBillingDocNo = "";
                    optbo.PetrolPumpId = 0;
                    optbo.PlyQty = 0;
                    optbo.ConsignorEmail = txtConsignorEmail.Text.Trim();
                    optbo.ConsignorTelNo = txtConsignorContact.Text.Trim();
                    optbo.ConsignorPincode = txtConsignerPIN.Text.Trim();
                    optbo.ConsigneePincode = txtConsigneePIN.Text.Trim();
                    optbo.EwayBillDate = txtEwayBillDate.Text.Trim();
                    optbo.PTLItemXml = itemXML;
                    long cnt = optbo.InsertUpdateLRPTL();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {

                            string LRNo = string.Empty;
                            optbo.ID = cnt;
                            LRNo = optbo.GetLRNo();


                            //SendMail();
                            MsgPanel.Message = "Record Saved successfully with LR No. : " + LRNo;
                            MsgPanel.DispCode = 1;
                            //Session["LRID"] = cnt.ToString();
                            //Session["ReportType"] = "LRDoc";
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                        }
                        else
                        {
                            //SendMail();
                            MsgPanel.Message = "Record Updated successfully with LR No. : " + txtLRNo.Text.Trim();
                            MsgPanel.DispCode = 1;
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;

                case CommonTypes.EntryFormCommand.SaveP:
                    optbo.LRNo = txtLRNo.Text.Trim();
                    optbo.ID = long.Parse(txtHiddenId.Value);
                    if (txtLRNo.Text.Trim() != string.Empty && optbo.LRDuplicateCount() > 0)
                    {
                        MsgPopUp.ShowModal("Duplicate LR No.", CommonTypes.ModalTypes.Error);
                        cmbBillingCompany.Focus();
                        return;
                    }

                    DateTime lrdate1 = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //DateTime placementdate1 = DateTime.ParseExact(txtVehiclePlacementDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);   

                    if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
                    {
                        MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Destination Master!!", CommonTypes.ModalTypes.Error);
                        return;
                    }
                    DataTable dt1 = null;
                    if (CommonTypes.GetDataTable("dtItem") == null)
                    {
                        MsgPopUp.ShowModal("Please enter atleast single line item to proceed", CommonTypes.ModalTypes.Error);
                        cmbItem.Focus();
                        return;
                    }
                    else
                    {
                        dt1 = CommonTypes.GetDataTable("dtItem");
                    }
                    dt1.TableName = "dtItem";
                    dt1.AcceptChanges();
                    string itemXML1 = "";
                    using (var stringWriter = new System.IO.StringWriter())
                    {
                        dt1.WriteXml(stringWriter, true);
                        itemXML1 = stringWriter.ToString();
                    }

                    if (itemXML1 == string.Empty)
                    {
                        MsgPopUp.ShowModal("Please enter atleast single line item to proceed", CommonTypes.ModalTypes.Error);
                        cmbItem.Focus();
                        return;
                    }
                    optbo.LRDatetime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text);
                    optbo.Consignee = cmbConsignee.SelectedItem.Text.Trim();
                    optbo.Consigner = cmbConsigner.SelectedItem.Text.Trim();
                    optbo.Origin = cmbFrom.Text.Trim();
                    optbo.Destination = cmbTo.SelectedItem.Text.Trim();
                    optbo.VehicleNo = txtVehicleNo.Text.Trim();
                    optbo.Weight = txtWeight.Text.Trim() == "" ? 0 : decimal.Parse(txtWeight.Text.Trim());
                    optbo.ChargeableWt = txtChWeight.Text.Trim() == "" ? 0 : decimal.Parse(txtChWeight.Text.Trim());
                    optbo.TotPackage = long.Parse(txttotPackage.Text);
                    optbo.InvoiceNo = txtInvoiceNo.Text.Trim();
                    optbo.InvoiceValue = decimal.Parse(txtInvoiceValue.Text);
                    optbo.LoadType = cmbType.Text.Trim().Substring(0, 1);
                    optbo.Remarks = cmbDescription.SelectedItem.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Status = "P";
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.DriverDtls = txtDriverDetails.Text.Trim();
                    optbo.DeviceID = 0;

                    //if (cmbDevice.Items.Count <= 0)
                    //    optbo.DeviceID = 0;
                    //else if (cmbDevice.SelectedValue == null)
                    //    optbo.DeviceID = 0;
                    //else
                    //    optbo.DeviceID = long.Parse(cmbDevice.SelectedValue);

                    optbo.RefNo = txtRefNo.Text.Trim();
                    optbo.SupplierID = long.Parse(cmbSupplier.SelectedValue);
                    optbo.SubSupplierID = 0;
                    optbo.CustomerID = long.Parse(cmbCustomer.SelectedValue);
                    optbo.TotalFrieght = 0;
                    optbo.SupplierAmount = txtSuplBuying.Text.Trim() == "" ? 0 : decimal.Parse(txtSuplBuying.Text);
                    optbo.AdvanceToSupplier = 0;
                    optbo.VehicleTypeID = long.Parse(cmbVehicleType.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.Rate = 0;
                    optbo.ConsignorAddress = txtConsignerAddress.Text.Trim();
                    optbo.ConsigneeAddress = txtConsigneeAddress.Text.Trim();
                    optbo.ConsigneeEmail = txtConsigneeEmail.Text.Trim();
                    optbo.ConsigneeContact = txtConsigneeContact.Text.Trim();
                    optbo.VehicleSealNo = txtvehicleSealNo.Text.Trim();
                    optbo.PackingType = cmbType.SelectedItem.Text;
                    optbo.VehiclePlacementDate = DateTime.Now.ToString("dd/MM/yyyy");
                    optbo.ExpectedDeliveryDays = int.Parse(HFExpectedDeliveryDays.Value);
                    optbo.DriverNumber = txtDriverNumber.Text.Trim();
                    optbo.FreightType = cmbFreight.SelectedItem.Text;
                    optbo.ConsignorGSTNo = txtConsignorGSTNo.Text.Trim();
                    optbo.ConsigneeGSTNo = txtConsigneeGSTNo.Text.Trim();
                    optbo.RefNo_Invoice = "";
                    optbo.InvoiceDate = txtInvoiceDate.Text.Trim();
                    optbo.EWayBillNo = txtEwayBillNo.Text.Trim();
                    optbo.Multipoint = false;
                    optbo.MainLRNo = "";
                    optbo.ShipmentDocNo = "";
                    optbo.InternalBillingDocNo = "";
                    optbo.placementId = 0;
                    optbo.PlyQty = 0;
                    optbo.ConsignorEmail = txtConsignorEmail.Text.Trim();
                    optbo.ConsignorTelNo = txtConsignorContact.Text.Trim();
                    optbo.ConsignorPincode = txtConsignerPIN.Text.Trim();
                    optbo.ConsigneePincode = txtConsigneePIN.Text.Trim();
                    optbo.EwayBillDate = txtEwayBillDate.Text;
                    optbo.PTLItemXml = itemXML1;

                    long cnt1 = optbo.InsertUpdateLRPTL();
                    if (cnt1 > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {

                            string LRNo = string.Empty;
                            optbo.ID = cnt1;
                            LRNo = optbo.GetLRNo();

                            if (!string.IsNullOrEmpty(hdTripId.Value))
                            {
                                optbo.TripId = long.Parse(hdTripId.Value);
                                optbo.UpdateTripId();
                            }


                            //SendMail();
                            MsgPanel.Message = "Record Saved successfully.";
                            MsgPanel.DispCode = 1;
                            Session["LRID"] = cnt1.ToString();
                            SavePdfLRForDownLoad(cnt1.ToString());
                            //Session["ReportType"] = "LRDoc";
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                        }
                        else
                        {
                            //SendMail();
                            SavePdfLRForDownLoad(txtHiddenId.Value);
                            MsgPanel.Message = "Record Updated successfully.";
                            MsgPanel.DispCode = 1;
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }
                    break;

                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                if (keys["Status"].ToString() == "Delivered" && Session["ROLEID"].ToString() != "1" && Session["ROLEID"].ToString() != "8")
                {
                    MsgPopUp.ShowModal("LR Status is Delivered Hence Cannot Modify.", CommonTypes.ModalTypes.Error);
                    return;
                }
                //if (decimal.Parse(keys["AdvanceToSupplier"].ToString()) > -1)
                //{
                //    MsgPopUp.ShowModal("LR Backend Updation Is Done. For Any Changes Please Contact Admin.", CommonTypes.ModalTypes.Error);
                //    return;
                //}
                txtLRNo.Enabled = false;
                txtSuplBuying.Text = "";
                //FillDropdown(DropDownTypes.Consignee);
                //FillDropdown(DropDownTypes.Consigner);
                FillDropdown(DropDownTypes.Destination);
                FillDropdown(DropDownTypes.Origin);
                FillDropdown(DropDownTypes.VehicleNo);
                FillDropdown(DropDownTypes.PackingType);
                FillDropdown(DropDownTypes.Description);
                FillDropDownVehicleType();
                FillDropDownCustomer();
                //cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                FillDropDownSupplier();
                FillDropDownFrom();
                //FillDropDownDestination();
                FillDropDownConsignor();
                FillDropDownBillingCompany();
                if (keys["DeviceID"] != null)
                {
                    if (keys["DeviceID"].ToString() != string.Empty)
                        FillDropDownDevice(long.Parse(keys["DeviceID"].ToString()));
                    else
                        FillDropDownDevice();
                }
                else
                    FillDropDownDevice();

                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                cmbBillingCompany.Enabled = false;
                txtHiddenId.Value = keys["ID"].ToString();
                txtHiddenSuplId.Value = keys["SupplierID"].ToString();
                txtLRNo.Text = keys["LRNo"].ToString();
                txtDate.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("dd/MM/yyyy").Replace("-", "/");
                //CalendarExtender3.EndDate = DateTime.Parse(txtDate.Text);
                ddlHH.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("HH");
                ddlmm.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("mm");
                if (keys["CustomerID"] != null)
                {
                    if (keys["CustomerID"].ToString() != string.Empty)
                    {
                        cmbCustomer.SelectedValue = keys["CustomerID"].ToString();
                        cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                    }
                }
                setCombobox(cmbConsigner, keys["Consigner"].ToString()); //cmbConsigner.SelectedValue = keys["Consigner"].ToString();
                cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                setComboboxValue(cmbConsignee, keys["Consignee"].ToString());
                //cmbConsignee.SelectedValue = keys["Consignee"].ToString();
                setCombobox(cmbFrom, keys["FromLoc"].ToString()); //cmbFrom.SelectedValue = keys["FromLoc"].ToString();
                FillDropDownDestination(keys["FromLoc"].ToString());
                setCombobox(cmbTo, keys["ToLoc"].ToString()); //cmbTo.SelectedValue = keys["ToLoc"].ToString();
                //cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                HFExpectedDeliveryDays.Value = "0";
                if (!string.IsNullOrEmpty(keys["ExpectedDeliveryDays"].ToString()))
                {
                    HFExpectedDeliveryDays.Value = keys["ExpectedDeliveryDays"].ToString();
                }

                txtVehicleNo.Text = keys["VehicleNo"].ToString();
                txtWeight.Text = keys["Weight"].ToString();
                txtChWeight.Text = keys["ChargeableWt"].ToString();
                txttotPackage.Text = keys["TotPackages"].ToString();
                txtInvoiceNo.Text = keys["InvoiceNo"].ToString();
                txtInvoiceValue.Text = keys["InvoiceValue"].ToString();
                //cmbType.SelectedIndex = keys["LoadType"].ToString() == "F" ? 0 : 1;
                if (keys["Remarks"] != null) setCombobox(cmbDescription, keys["Remarks"].ToString());
                //cmbDescription.SelectedValue = keys["Remarks"] == null ? "" : keys["Remarks"].ToString();
                //ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,PackingType
                txtConsigneeAddress.Text = keys["ConsigneeAddress"].ToString();
                if (keys["ConsignerAddress"] != null)
                {
                    if (keys["ConsignerAddress"].ToString().Trim() != string.Empty)
                        txtConsignerAddress.Text = keys["ConsignerAddress"].ToString();
                }
                txtConsigneeEmail.Text = keys["ConsigneeEmail"].ToString();
                txtConsigneeContact.Text = keys["ConsigneeTelNo"].ToString();
                txtvehicleSealNo.Text = keys["VehicleSealNo"].ToString();
                if (keys["PackingType"] != null) setCombobox(cmbType, keys["PackingType"].ToString());
                //cmbType.SelectedValue = keys["PackingType"] == null ? "" : keys["PackingType"].ToString(); //keys["PackingType"].ToString();

                txtDriverDetails.Text = keys["DriverDetails"].ToString();

                //if (keys["DeviceID"] != null)
                //{
                //    if (keys["DeviceID"].ToString() != string.Empty)
                //        cmbDevice.SelectedValue = keys["DeviceID"].ToString();
                //}
                if (keys["SupplierID"] != null)
                {
                    if (keys["SupplierID"].ToString() != string.Empty)
                        cmbSupplier.SelectedValue = keys["SupplierID"].ToString();
                }

                if (keys["VehicleTypeID"] != null)
                {
                    if (keys["VehicleTypeID"].ToString() != string.Empty)
                        cmbVehicleType.SelectedValue = keys["VehicleTypeID"].ToString();
                }
                if (keys["RefNo"] != null)
                    txtRefNo.Text = keys["RefNo"].ToString();

                if (keys["DriverNumber"] != null)
                    txtDriverNumber.Text = keys["DriverNumber"].ToString();

                if (keys["FreightType"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["FreightType"].ToString().Trim()))
                        cmbFreight.SelectedValue = keys["FreightType"].ToString();
                }
                if (keys["ConsignorGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsignorGSTNo"].ToString().Trim()))
                        txtConsignorGSTNo.Text = keys["ConsignorGSTNo"].ToString();
                }
                if (keys["ConsigneeGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsigneeGSTNo"].ToString().Trim()))
                        txtConsigneeGSTNo.Text = keys["ConsigneeGSTNo"].ToString();
                }

                if (keys["InvoiceDate"] != null)
                {
                    if (keys["InvoiceDate"].ToString().Trim() != string.Empty)
                        txtInvoiceDate.Text = DateTime.Parse(keys["InvoiceDate"].ToString()).ToString("dd/MM/yyyy");
                }
                //Multipoint Details
                txtEwayBillNo.Text = keys["EwayBillNo"].ToString();
                //chkMultipoint.Checked = bool.Parse(keys["MultiPoint"].ToString());

                //if (chkMultipoint.Checked)
                //    txtMainLRNo.Enabled = true;
                //else
                //    txtMainLRNo.Enabled = false;

                //txtMainLRNo.Text = keys["MainLRNo"].ToString();             


                txtHiddenCustRate.Value = keys["Rate"].ToString();
                txtHiddenCustFreight.Value = keys["TotalFrieght"].ToString();
                txtHiddenSuplFreight.Value = keys["SupplierAmount"].ToString();

                //
                txtConsignorEmail.Text = keys["ConsignorEmail"].ToString();
                txtConsignorContact.Text = keys["ConsignorTelNo"].ToString();
                txtConsignerPIN.Text = keys["ConsignorPincode"].ToString();
                txtConsigneePIN.Text = keys["ConsigneePincode"].ToString();

                if (keys["EwayBillDate"] != null && keys["EwayBillDate"].ToString().Trim() != string.Empty)
                    txtEwayBillDate.Text = DateTime.Parse(keys["EwayBillDate"].ToString()).ToString("dd/MM/yyyy");
                else
                    txtEwayBillDate.Text = "";


                DestinationBO OptBo = new DestinationBO();
                OptBo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
                OptBo.ToLoc = cmbTo.SelectedItem.Text.Trim();
                lblExpDeliveryDays.Text = "(Expected Delivery Days :" + OptBo.GetDestinationExpDays().ToString() + ")";
                txtHiddenExpDate.Value = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(double.Parse(OptBo.GetDestinationExpDays().ToString())).ToString("dd/MM/yyyy");
                //TDA Fields disable
                cmbSupplier.Enabled = false;
                cmbCustomer.Enabled = false;
                cmbVehicleType.Enabled = false;
                txtVehicleNo.Enabled = false;

                cmbFrom.Enabled = false;
                cmbTo.Enabled = false;

                FillDropDownItem(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                LoadGridViewDetailsEditMode(long.Parse(txtHiddenId.Value));
                if (grdItems.Rows.Count > 0)
                {
                    btnSave.Visible = true;
                }
                else
                {
                    btnSave.Visible = false;
                }
                ShowViewByIndex(0);
                cmbItem.Focus();
            }
            if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For LR No. " + keys["LRNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["ID"].ToString()));
            }
            if (e.CommandName == "ViewReport")
            {
                {
                    Session["LRID"] = keys["ID"].ToString();
                    Session["ReportType"] = "LRDoc";

                    //Response.Redirect("~/Reports/PolicyView.aspx?PolicyID=" + policyID.ToString());
                    //string winCmd=;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow","window.open('../Pages/LRPrint.aspx?LRID="+keys["ID"].ToString()+"','_blank')" , true);
                }
            }
            if (e.CommandName == "SavePdf")
            {
                //Session["LRID"] = keys["ID"].ToString();
                SavePdfLRForDownLoad(keys["ID"].ToString());

            }
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtHiddenId.Value == "0")
            {
                //LRBO optbo = new LRBO();
                //optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                //optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                //DataTable dt = optbo.GetLRNoByCompName();
                //if (dt.Rows.Count > 0)
                //{

                //    txtLRNo.Text = dt.Rows[0][0].ToString();
                //    txtLRNo.Enabled = false;
                //}
            }
            cmbBillingCompany.Focus();
        }

        protected void cmbFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExpDeliveryDays.Text = "0";
            FillDropDownItem(0, 0, "", "");
            FillDropDownItemZoneType(0, 0, "", "", "");
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            clearItems();
            if (cmbFrom.SelectedIndex > 0)
            {
                FillDropDownDestination(cmbFrom.SelectedItem.Text.Trim());
                FillDropDownItem(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
            }
            cmbTo.Focus();
        }

        protected void cmbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExpDeliveryDays.Text = "0";
            HFExpectedDeliveryDays.Value = "0";
            FillDropDownItem(0, 0, "", "");
            FillDropDownItemZoneType(0, 0, "", "", "");
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            clearItems();
            if (cmbTo.SelectedIndex > 0)
            {
                DestinationBO OptBo = new DestinationBO();
                OptBo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
                OptBo.ToLoc = cmbTo.SelectedItem.Text.Trim();
                HFExpectedDeliveryDays.Value = OptBo.GetDestinationExpDaysPTL().ToString();
                if (HFExpectedDeliveryDays.Value == null) HFExpectedDeliveryDays.Value = "0";
                if (HFExpectedDeliveryDays.Value.ToString().Trim() == string.Empty) HFExpectedDeliveryDays.Value = "0";
                lblExpDeliveryDays.Text = "(Expected Delivery Days :" + HFExpectedDeliveryDays.Value + ")";
                txtHiddenExpDate.Value = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(double.Parse(HFExpectedDeliveryDays.Value)).ToString("dd/MM/yyyy");
                if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
                {
                    MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Customer Freight Master!!", CommonTypes.ModalTypes.Error);
                    cmbTo.Focus();
                    return;
                }
                FillDropDownItem(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                txtWeight.Focus();
            }
            else
            {
                cmbTo.Focus();
            }
        }

        protected void cmbConsignee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConsignorBO optbo = new ConsignorBO();
            optbo.Srl = long.Parse(cmbConsignee.SelectedValue);

            DataTable dt = optbo.GetConsignorDetailsById();
            //if (txtHiddenId.Value == "0")
            //{
            if (dt.Rows.Count > 0)
            {
                txtConsigneeAddress.Text = dt.Rows[0]["Address"].ToString();
                txtConsigneeEmail.Text = dt.Rows[0]["EmailId"].ToString();
                txtConsigneeContact.Text = dt.Rows[0]["MobileNo"].ToString();
                txtConsigneeGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                txtConsigneePIN.Text = dt.Rows[0]["Pincode"].ToString();

                txtConsignerAddress.Focus();
            }
            else
                cmbConsignee.Focus();
            //}
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            txtDate.Focus();
        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDropDownItem(0, 0, "", "");
            FillDropDownItemZoneType(0, 0, "", "", "");
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            clearItems();
            if (cmbCustomer.SelectedIndex > 0)
            {
                ConsignorBO optbo = new ConsignorBO();
                optbo.CustomerId = long.Parse(cmbCustomer.SelectedValue);
                DataSet ds = optbo.FillConsignorConsignee();

                cmbConsigner.DataSource = null;
                cmbConsigner.Items.Clear();
                cmbConsigner.Items.Add(new ListItem("", "-1"));

                cmbConsigner.DataSource = ds.Tables[0];
                cmbConsigner.DataTextField = "Name";
                cmbConsigner.DataValueField = "ConsignorId";
                cmbConsigner.DataBind();
                cmbConsigner.SelectedIndex = 0;
                if (cmbConsigner.Items.Count == 2)
                {
                    cmbConsigner.SelectedIndex = 1;
                    cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbConsignee.Focus();
                }

                cmbConsignee.DataSource = null;
                cmbConsignee.Items.Clear();
                cmbConsignee.Items.Add(new ListItem("", "-1"));

                cmbConsignee.DataSource = ds.Tables[1];
                cmbConsignee.DataTextField = "Name";
                cmbConsignee.DataValueField = "ConsignorId";
                cmbConsignee.DataBind();
                cmbConsignee.SelectedIndex = 0;
                if (cmbConsignee.Items.Count == 2)
                {
                    cmbConsignee.SelectedIndex = 1;
                    cmbConsignee_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbFrom.Focus();
                }

                if (cmbFrom.SelectedIndex > 0)
                {
                    cmbFrom_SelectedIndexChanged(cmbFrom, EventArgs.Empty);
                }

                if (cmbVehicleType.SelectedIndex > 0)
                {
                    FillDropDownItem(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                    cmbConsigner.Focus();
                }
                else
                    cmbCustomer.Focus();
            }
            else
                cmbCustomer.Focus();
        }

        protected void cmbConsigner_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtConsignerAddress.Text = "0";
            if (cmbConsigner.SelectedIndex > 0)
            {
                txtConsignerAddress.Text = string.Empty;
                txtConsignorGSTNo.Text = string.Empty;
                ConsignorBO OptBo = new ConsignorBO();
                OptBo.Srl = long.Parse(cmbConsigner.SelectedValue);
                DataTable dt = OptBo.GetConsignorDetailsById();
                if (dt == null) return;
                if (dt.Rows.Count <= 0) return;

                txtConsignerAddress.Text = dt.Rows[0]["Address"].ToString();
                txtConsignorGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                txtConsignorEmail.Text = dt.Rows[0]["EmailId"].ToString();
                txtConsignorContact.Text = dt.Rows[0]["MobileNo"].ToString();
                txtConsignerPIN.Text = dt.Rows[0]["Pincode"].ToString();
                cmbConsignee.Focus();
            }
            else
                cmbConsigner.Focus();
        }

        protected void cmbVehicleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDropDownItem(0, 0, "", "");
            FillDropDownItemZoneType(0, 0, "", "", "");
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            clearItems();
            if (cmbVehicleType.SelectedIndex > 0)
            {
                if (cmbCustomer.SelectedIndex > 0)
                {
                    FillDropDownItem(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                    if (cmbVehicleType.SelectedItem.Text.Trim().ToLower().Contains("ptl"))
                    {
                        if (txtDriverDetails.Text.Trim() != string.Empty) txtDriverDetails.Text = "-";
                        if (txtDriverNumber.Text.Trim() != string.Empty) txtDriverNumber.Text = "-";
                    }
                    txtVehicleNo.Focus();
                }
                else
                    cmbVehicleType.Focus();
            }
            else
                cmbVehicleType.Focus();
        }

        protected void cmbItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDropDownItemZoneType(0, 0, "", "", "");
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            if (cmbItem.SelectedIndex > 0)
            {
                FillDropDownItemZoneType(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbItem.SelectedValue, cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                cmbZoneType.Focus();
            }
            else
                cmbItem.Focus();
        }

        protected void cmbZoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDropDownItemZoneTypeSlab(0, 0, "", 0, "", "");
            FillItemDetails(0, 0, "", 0, "", "", "");
            if (cmbZoneType.SelectedIndex > 0)
            {
                FillDropDownItemZoneTypeSlab(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbItem.SelectedValue, long.Parse(cmbZoneType.SelectedValue), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                if (cmbSlab.Items.Count <= 2)
                {
                    cmbSlab.SelectedIndex = 1;
                    cmbSlab_SelectedIndexChanged(cmbSlab, EventArgs.Empty);
                    txtQtyWt.Focus();
                }
                else
                    cmbSlab.Focus();
            }
            else
                cmbZoneType.Focus();
        }

        protected void cmbSlab_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillItemDetails(0, 0, "", 0, "", "", "");
            if (cmbSlab.SelectedIndex > 0)
            {
                FillItemDetails(long.Parse(cmbCustomer.SelectedValue), long.Parse(cmbVehicleType.SelectedValue), cmbItem.SelectedValue, long.Parse(cmbZoneType.SelectedValue), cmbSlab.SelectedItem.Text.Trim(), cmbFrom.SelectedItem.Text.Trim(), cmbTo.SelectedItem.Text.Trim());
                txtQtyWt.Focus();
            }
            else
                cmbSlab.Focus();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtItem") == null)
            {
                dt = CommonTypes.CreateDataTable("Item,ZoneTypeId,ZoneType,Slab,Qty,ChargeableWt,RatePerQtySelling,RatePerKgSelling,RatePerQtyBuying,RatePerKgBuying,MOQ,IsFixedRateSelling,IsFixedRateBuying", "dtItem", "string,long,string,string,decimal,decimal,decimal,decimal,decimal,decimal,decimal,int,int");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtItem");
            }
            DataRow[] foundRows = dt.Select("Item='" + cmbItem.SelectedValue + "' and ZoneTypeId=" + cmbZoneType.SelectedValue + " and Slab='" + cmbSlab.SelectedItem.Text.Trim() + "'");
            if (foundRows.LongLength > 0)
            {
                MsgPopUp.ShowModal("Item already selected!!", CommonTypes.ModalTypes.Error);
                cmbItem.Focus();
                return;
            }
            DataRow dr = dt.NewRow();
            dr["Item"] = cmbItem.SelectedItem.Text.Trim();
            dr["ZoneTypeId"] = cmbZoneType.SelectedValue;
            dr["ZoneType"] = cmbZoneType.SelectedItem.Text.Trim();
            dr["Slab"] = cmbSlab.SelectedItem.Text.Trim();
            dr["Qty"] = decimal.Parse(RatePerQtySelling.Value) > 0 ? txtQtyWt.Text : "0";
            dr["ChargeableWt"] = decimal.Parse(RatePerQtySelling.Value) > 0 ? "0" : txtQtyWt.Text;
            dr["RatePerQtySelling"] = RatePerQtySelling.Value;
            dr["RatePerKgSelling"] = RatePerKgSelling.Value;
            dr["RatePerQtyBuying"] = RatePerQtyBuying.Value;
            dr["RatePerKgBuying"] = RatePerKgBuying.Value;
            dr["MOQ"] = MOQ.Value;
            if (FixedRateSelling.Value.Trim().ToLower() == "0" || FixedRateSelling.Value.Trim().ToLower() == "false")
                dr["IsFixedRateSelling"] = "0";
            else
                dr["IsFixedRateSelling"] = "1";

            if (FixedRateBuying.Value.Trim().ToLower() == "0" || FixedRateBuying.Value.Trim().ToLower() == "false")
                dr["IsFixedRateBuying"] = "0";
            else
                dr["IsFixedRateBuying"] = "1";

            CommonTypes.Addrow(dt, dr);
            grdItems.DataSource = dt;
            grdItems.DataBind();
            if (decimal.Parse(RatePerQtySelling.Value) > 0)
            {
                if (dt.Rows.Count == 1) txttotPackage.Text = "0";

                txttotPackage.Text = dt.Compute("sum(Qty)", "").ToString();
            }
            else
            {
                if (dt.Rows.Count == 1) txtWeight.Text = "0";
                txtWeight.Text = dt.Compute("sum(ChargeableWt)", "").ToString();
            }
            cmbSlab.SelectedIndex = 0;
            cmbZoneType.SelectedIndex = 0;
            cmbItem.SelectedIndex = 0;
            txtQtyWt.Text = "0";
            btnSave.Visible = true;
            cmbItem.Focus();
        }

        protected void grdItems_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;

            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];

            if (e.CommandName == "RemoveItem")
            {
                if (grd != null)
                {
                    DataTable dtItem = null;
                    dtItem = CommonTypes.GetDataTable("dtItem");
                    decimal RatePerQtySelling = decimal.Parse(dtItem.Rows[index]["RatePerQtySelling"].ToString());
                    dtItem.Rows.RemoveAt(index);
                    dtItem.AcceptChanges();
                    grdItems.DataSource = dtItem;
                    grdItems.DataBind();
                    Session["dtItem"] = dtItem;

                    if (RatePerQtySelling > 0)
                    {
                        if (dtItem.Rows.Count == 1) txttotPackage.Text = "0";

                        txttotPackage.Text = dtItem.Compute("sum(Qty)", "").ToString();
                    }
                    else
                    {
                        if (dtItem.Rows.Count == 1) txtWeight.Text = "0";
                        txtWeight.Text = dtItem.Compute("sum(ChargeableWt)", "").ToString();
                    }

                    cmbItem.Focus();
                    //for (int i = 0; i < dtItem.Rows.Count; i++)
                    //{
                    //    if (dtItem.Rows[i]["Item"].ToString(). == int.Parse(keys["Item"].ToString()) )
                    //    {
                    //        dtItem.Rows.RemoveAt(i);
                    //        dtItem.AcceptChanges();
                    //        grdItems.DataSource = dtItem;
                    //        grdItems.DataBind();
                    //        Session["dtItem"] = dtItem;
                    //        cmbItem.Focus();
                    //        break;
                    //    }
                    //}
                    if (grd.Rows.Count <= 0)
                    {
                        btnSave.Visible = false;
                    }
                }
            }
        }
        #endregion
    }
}
