﻿<%@ Page Title="Invoice Entry" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="Invoice.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.Invoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <%-- confirm dialog--%>
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete LR?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--End confirm dialog --%>
            <%-- bill submission date--%>
            <asp:Button ID="btnbillsubmission" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlBillSubmission" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    Update Bill Submission Date
                </div>
                <div align="center" style="padding: 5px">
                    <table>
                        <tr>
                            <td>
                            <asp:HiddenField runat="server" ID="hdfldBillInvRef" />
                                <asp:Label runat="server" Text="Bill Submission Date"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBillSubmissionDate" Enabled="true" runat="server" Width="80px" TabIndex="1"
                                    CssClass="NormalTextBold"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvBillSubmission" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                    ControlToValidate="txtBillSubmissionDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtBillSubmissionDate" TodaysDateFormat="dd/MMM/yyyy" PopupButtonID="dtpBtnBillSubmission"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="dtpBtnBillSubmission" Enabled="true" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                <asp:RequiredFieldValidator ID="rfvBillSubmit" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select date'
                                            ControlToValidate="txtBillSubmissionDate" SetFocusOnError="True" ValidationGroup="saveBillSubmission" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnUpdateBillSubmission" ValidationGroup="saveBillSubmission" runat="server" Text="Update" 
                        CssClass="button" onclick="btnUpdateBillSubmission_Click"     />
                    <asp:Button ID="btnCancelBillSubmission" runat="server" Text="Cancel" 
                        CssClass="button" onclick="btnCancelBillSubmission_Click"   />
                </div>
            </asp:Panel>
              <asp:ModalPopupExtender ID="popBillSubmission" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="btnbillsubmission" PopupControlID="pnlBillSubmission" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%-- end bill submission date--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Invoice Entry
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                                <asp:HiddenField ID="txtRowNumber" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="LR Info" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td style="width: 120px">
                                                <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="80px" TabIndex="1"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtn" Enabled="true" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td style="width: 120px">
                                                <asp:Label ID="Label5" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbBillingCompany_SelectedIndexChanged">
                                                </asp:ComboBox>
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label22" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="4" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" OnSelectedIndexChanged="cmbCustomer_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Ref. No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBRefNo" Width="270px" runat="server" TabIndex="6" MaxLength="50"
                                                    CssClass="NormalTextBold" />
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBdate" runat="server" Width="80px" TabIndex="7" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtBdate" TodaysDateFormat="dd/MMM/yyyy" PopupButtonID="dtpBBtn"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="8" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Bidding Charges" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtBidding" Width="100px" runat="server" TabIndex="6" MaxLength="5"
                                                    CssClass="NormalTextBold" Style="text-align: right" />
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1"
                                                    TargetControlID="txtBidding" FilterType="Numbers">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdPendingLRs" runat="server" Style="width: 100%" AutoGenerateColumns="False"
                                        AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="100"
                                        DataKeyNames="LRId,TotalFrieght,PODPhysicalDate,PODFile" OnRowDataBound="grdPendingLRs_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkLR" TabIndex="9" onclick="checkboxclick(this);"
                                                        AutoPostBack="false" /></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:TemplateField>
                                           <%-- <asp:BoundField DataField="InvRefNo" HeaderText="Ref. No" SortExpression="InvRefNo"
                                                ItemStyle-Width="160px" />--%>
                                            <asp:BoundField DataField="LRNo" HeaderText="LRNo" SortExpression="LRNo" ItemStyle-Width="160px" />
                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:BoundField DataField="FromLoc" HeaderText="From" SortExpression="FromLoc" ItemStyle-Width="120px" />
                                            <asp:BoundField DataField="ToLoc" HeaderText="To" SortExpression="ToLoc" ItemStyle-Width="120px" />
                                            <asp:TemplateField HeaderText="Total Freight" ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtFrieght" CssClass="NormalTextBold" TabIndex="9"
                                                        Enabled="False" Style="text-align: right" AutoPostBack="false" onchange="txtchanged(this);"
                                                        Text='<%#Eval("TotalFrieght") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterFrieght" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtFrieght" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detention Charges" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDetention" CssClass="NormalTextBold" TabIndex="9"
                                                        Enabled="False" Style="text-align: right" AutoPostBack="false" onchange="txtchanged(this);" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterDetention" runat="server" FilterType="Numbers"
                                                        TargetControlID="txtDetention" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Warai Charges" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtWarai" CssClass="NormalTextBold" Style="text-align: right"
                                                        Enabled="False" TabIndex="9" AutoPostBack="false" Text='<%#Eval("UnloadingCharges") %>'
                                                        onchange="txtchanged(this);" /><ajaxToolkit:FilteredTextBoxExtender ID="FilterWarai"
                                                            runat="server" FilterType="Custom,Numbers" ValidChars="." TargetControlID="txtWarai"
                                                            Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Two Point Charges" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTwoPoint" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" TabIndex="9" Style="text-align: right" Enabled="False" /><ajaxToolkit:FilteredTextBoxExtender
                                                            ID="FilterTwoPoint" runat="server" FilterType="Numbers" TargetControlID="txtTwoPoint"
                                                            Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detetion loading Days" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDetloadingDays" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" Style="text-align: right" TabIndex="9" Enabled="False" /><ajaxToolkit:FilteredTextBoxExtender
                                                            ID="FilterOther1" runat="server" FilterType="Numbers" TargetControlID="txtDetloadingDays"
                                                            Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detetion Unloading Days" ControlStyle-Width="60px"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDetUnloadingDays" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" Style="text-align: right" TabIndex="9" Enabled="False" /><ajaxToolkit:FilteredTextBoxExtender
                                                            ID="FilterOther2" runat="server" FilterType="Numbers" TargetControlID="txtDetUnloadingDays"
                                                            Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detention Unloading" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDetUnloadingCharges" CssClass="NormalTextBold"
                                                        Enabled="False" AutoPostBack="false" onchange="txtchanged(this);" Style="text-align: right"
                                                        TabIndex="9" /><ajaxToolkit:FilteredTextBoxExtender ID="FilterOther3" runat="server"
                                                            FilterType="Numbers" TargetControlID="txtDetUnloadingCharges" Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Days" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTransitPenaltyDays" CssClass="NormalTextBold"
                                                        Enabled="False" AutoPostBack="false" onchange="txtchanged(this);" Style="text-align: right"
                                                        TabIndex="9" /><ajaxToolkit:FilteredTextBoxExtender ID="FilterOther4" runat="server"
                                                            FilterType="Numbers" TargetControlID="txtTransitPenaltyDays" Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Transit Penalty" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTransitPenaltyCharges" CssClass="NormalTextBold"
                                                        AutoPostBack="false" onchange="txtchanged(this);" Style="text-align: right" TabIndex="9"
                                                        Enabled="False" /><ajaxToolkit:FilteredTextBoxExtender ID="FilterOther5" runat="server"
                                                            FilterType="Numbers" TargetControlID="txtTransitPenaltyCharges" Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Other Charges" ControlStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtOtherCharges" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" Style="text-align: right" TabIndex="9" Enabled="False" /><ajaxToolkit:FilteredTextBoxExtender
                                                            ID="FilterOther" runat="server" FilterType="Numbers" TargetControlID="txtOtherCharges"
                                                            Enabled="True">
                                                        </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Charges" ControlStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTotalCharges" CssClass="NormalTextBold" TabIndex="9"
                                                        Enabled="False" Style="text-align: right" AutoPostBack="false" onchange="txtchanged(this);" />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterTotal" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtTotalCharges" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks" ControlStyle-Width="150px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtLRRemarks" CssClass="NormalTextBold" AutoPostBack="false"
                                                        MaxLength="50" TabIndex="9" Style="text-align: left" Enabled="False" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Coil No" ControlStyle-Width="150px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtCoilNo" CssClass="NormalTextBold" AutoPostBack="false"
                                                        MaxLength="50" TabIndex="9" Style="text-align: left" Enabled="False" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                                <table class="control_set" style="width: 100%">
                                    <tr class="control_row">
                                        <td>
                                        </td>
                                        <td style="width: 120px">
                                            Total Invoice Amount
                                        </td>
                                        <td style="width: 70px">
                                            <asp:TextBox ID="txtBasicAmount" runat="server" Width="70px" Style="text-align: right"
                                                TabIndex="10" CssClass="NormalTextBold" Enabled="False"></asp:TextBox>
                                        </td>
                                        <td style="width: 160px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 160px">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:Label runat="server" Text="Payment Terms"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPaymentTerms" runat="server" Width="400px" CssClass="NormalTextBold"
                                                TabIndex="11" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text="Particulars"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtParticulars" runat="server" Width="500px" CssClass="NormalTextBold"
                                                TabIndex="12" MaxLength="200"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="13" ID="btnSavePacklist" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="14" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" MaxLength="10" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAddPacklist" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDeletePacklist" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" Visible="False" />&nbsp;
                                            &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="InvoiceNo,InvoiceDate,Name,InvoiceAmount,BillingCompany_Srl,InvRefNo,BillSubmissionDate"
                                                OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                                OnRowCommand="grdPacklistView_RowCommand" AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                    </asp:ButtonField>
                                                      <asp:ButtonField HeaderText="Bill Submission Date" CommandName="UpdateBillSubmission" DataTextField="BillSubmissionDate">
                                                        <ItemStyle Width="140px" />
                                                    </asp:ButtonField>
                                                    <%-- <asp:ButtonField CommandName="ViewReport" Text="Document">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField>--%>
                                                    <asp:ButtonField CommandName="SavePdf" Text="Document">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField> 
                                                    <asp:ButtonField CommandName="annexurepdf" Text="Annexure">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField>
                                                   
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="LRNo, LRDate, TransportationCharges, DetentionCharges, WaraiCharges, TwoPointCharges,OtherCharges, TotalCharges"
                                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="InvRefNo" HeaderText="Ref. No." SortExpression="InvRefNo">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="100px"
                                                                DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TransportationCharges" HeaderText="Basic Charges" SortExpression="TransportationCharges">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TotalCharges" HeaderText="Total Charges" SortExpression="TotalCharges">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddPacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDeletePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSavePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbCustomer" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function checkboxclick(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_chkLR'));
            var rowindex = Rowid.substring(4, Rowid.length);
            document.getElementById("<%= txtRowNumber.ClientID %>").value = rowindex;
            var gv = document.getElementById("<%= grdPendingLRs.ClientID %>");
            var tb = gv.getElementsByTagName("input");
            var frt = 0;
            for (var i = 0; i < tb.length; i++) {

                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtFrieght') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    frt = tb[i].value;
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtDetention') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                    tb[i].focus();
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtWarai') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTwoPoint') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtDetloadingDays') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtDetUnloadingDays') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtDetUnloadingCharges') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTransitPenaltyDays') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTransitPenaltyCharges') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtOtherCharges') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTotalCharges') >= 0) {
                    if (control.checked) {
                        tb[i].value = frt;
                    }
                    if (!control.checked) {
                        var TotalAmt = document.getElementById("<%= txtBasicAmount.ClientID %>").value;
                        document.getElementById("<%= txtBasicAmount.ClientID %>").value = (parseFloat(TotalAmt) - parseFloat(tb[i].value)).toFixed(2);
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtLRRemarks') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtCoilNo') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
            }
            if (!control.checked) {
                document.getElementById("<%= txtRowNumber.ClientID %>").value = '';
            }
            calculate();
        }
        function txtchanged(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_txt'));
            var rowindex = Rowid.substring(4, Rowid.length);
            document.getElementById("<%= txtRowNumber.ClientID %>").value = rowindex;
            calculate();
        }
        function calculate() {
            //document.getElementById("<%= txtParticulars.ClientID %>").value = document.getElementById("<%= txtRowNumber.ClientID %>").value;
            if (document.getElementById("<%= txtRowNumber.ClientID %>").value.toString() != "") {
                var gv = document.getElementById("<%= grdPendingLRs.ClientID %>");
                var tb = gv.getElementsByTagName("input");

                var Total = 0;
                var TotalAmt = 0;
                var newId = "";
                newId = "ctl" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                if (parseInt(document.getElementById("<%= txtRowNumber.ClientID %>").value) < 10) {
                //                    newId = "ctl0" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                }
                //                else {
                //                    newId = "ctl" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                }
                //document.getElementById("<%= txtPaymentTerms.ClientID %>").value = newId;

                for (var i = 0; i < tb.length; i++) {

                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtFrieght') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtDetention') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtWarai') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtTwoPoint') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtOtherCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtTransitPenaltyCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) - parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtDetUnloadingCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtTotalCharges') >= 0) {
                        tb[i].value = parseFloat(Total).toFixed(2);
                    }

                    if (tb[i].type == "text" && tb[i].id.indexOf('txtTotalCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            TotalAmt = parseFloat(TotalAmt) + parseFloat(tb[i].value);
                        }
                    }
                }
                document.getElementById("<%= txtBasicAmount.ClientID %>").value = parseFloat(TotalAmt).toFixed(2);
            }
        }    
    </script>
</asp:Content>
