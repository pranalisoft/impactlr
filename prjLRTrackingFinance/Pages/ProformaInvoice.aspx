﻿<%@ Page Title="Proforma Invoice" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="ProformaInvoice.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.ProformaInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete LR?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Proforma Invoice Entry
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
             <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="LR Info" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td style="width: 100px">
                                                <asp:Label ID="Label4" runat="server" Text="Invoice Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtDate" runat="server" Width="80px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td style="width: 120px">
                                                <asp:Label ID="Label5" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="2" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbBillingCompany_SelectedIndexChanged">
                                                </asp:ComboBox>
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label22" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" OnSelectedIndexChanged="cmbCustomer_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td style="width: 120px">
                                                <asp:Label ID="Label1" runat="server" Text="Vehicle No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:ComboBox ID="cmbVehicle" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="4" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" AutoPostBack="true" OnSelectedIndexChanged="cmbVehicle_SelectedIndexChanged">
                                                </asp:ComboBox>
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label2" runat="server" Text="Destination" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbDestination" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="5" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbDestination_SelectedIndexChanged">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdPendingLRs" runat="server" Style="width: 100%" AutoGenerateColumns="False"
                                        AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="100"
                                        DataKeyNames="ID,TotalFrieght">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkLR" TabIndex="8" OnCheckedChanged="chkLR_CheckedChanged"
                                                        AutoPostBack="true" /></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="28px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LRNo" HeaderText="LRNo" SortExpression="LRNo" ItemStyle-Width="200px">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                DataFormatString="{0:dd/MM/yyyy}">
                                                <ItemStyle Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="VehicleNo" HeaderText="Vehicle No" SortExpression="VehicleNo"
                                                ItemStyle-Width="200px">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Consignee" HeaderText="Consignee" SortExpression="Consignee">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Total Freight" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtFrieght" CssClass="NormalTextBold" TabIndex="9"
                                                        Style="text-align: right" OnTextChanged="txtFrieght_TextChanged" AutoPostBack="true"
                                                        Text='<%#Eval("TotalFrieght") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterFrieght" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtFrieght" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                                <table class="control_set" style="width: 100%">
                                    <tr class="control_row">
                                        <td>
                                        </td>
                                        <td style="width: 150px">
                                            Total Invoice Amount
                                        </td>
                                        <td style="width: 85px">
                                            <asp:TextBox ID="txtBasicAmount" runat="server" Width="80px" Style="text-align: right"
                                                TabIndex="15" CssClass="NormalTextBold" OnTextChanged="txtBasicAmount_TextChanged"
                                                Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="6" ID="btnSavePacklist" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="7" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" MaxLength="10" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAddPacklist" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDeletePacklist" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" />&nbsp; &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="Srl,InvoiceNo,InvoiceDate,Name,InvoiceAmount" OnPageIndexChanging="grdPacklistView_PageIndexChanging"
                                                OnRowDataBound="grdPacklistView_RowDataBound" OnRowCommand="grdPacklistView_RowCommand"
                                                AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                    </asp:ButtonField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="LRNo, LRDate, TransportationCharges, DetentionCharges, WaraiCharges, TwoPointCharges,OtherCharges, TotalCharges"
                                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="100px"
                                                                DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TransportationCharges" HeaderText="Basic Charges" SortExpression="TransportationCharges">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TotalCharges" HeaderText="Total Charges" SortExpression="TotalCharges">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddPacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDeletePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSavePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbCustomer" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
