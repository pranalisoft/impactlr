﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PendingInvoicesCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);

            if (!IsPostBack)
            {
                FillDropDowns();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\CustomerOutstanding_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");



            long BranchId = 0;
            if (cmbBranch.SelectedIndex > 0)
                BranchId = long.Parse(cmbBranch.SelectedValue);

            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = BranchId;
            DataTable dt = OptBO.CustomerPendingInvoices();
            DataSet ds = new DataSet();

            ds.Tables.Add(dt);
            ExportToExcel(ds, filename);

        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    objWorksheet.Cells["E:E"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["V:V"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AB:AB"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AC:AC"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AK:AK"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AS:AS"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        void dataExportExcel_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Header Text Format can be done as follows
                e.Item.Font.Bold = true;

                //Adding Filter/Sorting functionality for the Excel
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    e.Item.Cells[cellIndex].Attributes.Add("x:autofilter", "all");
                    e.Item.Cells[cellIndex].Width = 160;
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Center;
                    cellIndex++;
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    //Any Cell specific formatting should be done here
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Left;
                    cellIndex++;
                }
            }
        }

        private void LoadGridView()
        {
            long BranchId = 0;
            if (cmbBranch.SelectedIndex > 0)
                BranchId = long.Parse(cmbBranch.SelectedValue);

            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = BranchId;
            DataTable dt = OptBO.CustomerPendingInvoices();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();
        }

        private void FillDropDowns()
        {
            cmbBranch.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.Items.Add(new ListItem("", "-1"));
            BranchBO OptBO = new BranchBO();
            OptBO.SearchText = string.Empty;
            DataTable dt = OptBO.GetBranchdetails();
            cmbBranch.DataSource = dt;
            cmbBranch.DataTextField = "Name";
            cmbBranch.DataValueField = "Srl";
            cmbBranch.DataBind();
            cmbBranch.SelectedIndex = 0;
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            LoadGridView();
        }
    }
}