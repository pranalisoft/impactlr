﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using AjaxControlToolkit;
using System.Net.Mail;
using System.Net;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LREntry : System.Web.UI.Page
    {
        enum DropDownTypes
        {
            Consignee, Consigner, Origin, Destination, VehicleNo, PackingType, Description
        }

        #region "Procedure"
        //private void SetPanelMsg(string msg, bool visible, int code)
        //{
        //    if (visible)
        //    {
        //        lblMsg.Text = msg;
        //        pnlMsg.Visible = true;
        //        if (code == 1)
        //        {
        //            pnlMsg.Style.Add("border", "solid 1px #336600");
        //            pnlMsg.Style.Add("color", "black");
        //            pnlMsg.Style.Add("background-color", "#9EDC7F");
        //        }
        //        else
        //        {
        //            pnlMsg.Style.Add("border", "solid 1px #CE180E");
        //            pnlMsg.Style.Add("color", "white");
        //            pnlMsg.Style.Add("background-color", "#D20000");
        //        }
        //    }
        //    else
        //    {
        //        lblMsg.Text = string.Empty;
        //        pnlMsg.Visible = false;
        //    }
        //}

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void LoadGridView()
        {
            LRBO optBO = new LRBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.IsPending = false;
            DataTable dt = optBO.FillLRGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDetails(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataTable dt = optBO.FillLRStatus();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        private void ClearView()
        {
            txtPlacementId.Text = string.Empty;
            txtPlacementId.Enabled = true;
            txtVehicleNo.Enabled = true;
            txtLRNo.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtChWeight.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            txtInvoiceValue.Text = string.Empty;
            txtInvoiceDate.Text = string.Empty;
            cmbDescription.SelectedIndex = -1;
            txtRefNo.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtDriverDetails.Text = string.Empty;
            txttotPackage.Text = string.Empty;
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //CalendarExtender3.EndDate = DateTime.Parse(txtDate.Text);
            ddlHH.Text = DateTime.Now.ToString("HH");
            ddlmm.Text = DateTime.Now.ToString("mm");
            cmbBillingCompany.SelectedIndex = -1;
            txtConsigneeAddress.Text = string.Empty;
            txtConsigneeContact.Text = string.Empty;
            txtConsigneeEmail.Text = string.Empty;
            cmbFrom.SelectedIndex = -1;
            cmbTo.SelectedIndex = -1;
            txtDriverNumber.Text = string.Empty;
            txtvehicleSealNo.Text = string.Empty;
            txtVehicleNo.Text = string.Empty;
            txtConsignorGSTNo.Text = string.Empty;
            txtConsigneeGSTNo.Text = string.Empty;
            txtRefNo_Invoice.Text = string.Empty;
            txtShippingDocNo.Text = string.Empty;
            txtInternalbillingNo.Text = string.Empty;
            txtEwayBillNo.Text = string.Empty;
            txtEwayBillDate.Text = string.Empty;
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
        }

        private void FillDropdown(DropDownTypes cmbtype)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            switch (cmbtype)
            {
                case DropDownTypes.Consignee:
                    //OptBO.FillType = "S";
                    //cmbConsignee.DataSource = null;
                    //cmbConsignee.Items.Clear();
                    //cmbConsignee.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsignee.DataSource = dt;
                    //cmbConsignee.DataTextField = "Consignee";
                    //cmbConsignee.DataValueField = "Consignee";
                    //cmbConsignee.DataBind();
                    //cmbConsignee.SelectedIndex = 0;
                    break;
                case DropDownTypes.Consigner:
                    //OptBO.FillType = "C";
                    //cmbConsigner.DataSource = null;
                    //cmbConsigner.Items.Clear();
                    //cmbConsigner.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsigner.DataSource = dt;
                    //cmbConsigner.DataTextField = "Consigner";
                    //cmbConsigner.DataValueField = "Consigner";
                    //cmbConsigner.DataBind();
                    //cmbConsigner.SelectedIndex = 0;
                    break;
                case DropDownTypes.Origin:
                    OptBO.FillType = "O";
                    cmbFrom.DataSource = null;
                    cmbFrom.Items.Clear();
                    cmbFrom.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRCombo();
                    cmbFrom.DataSource = dt;
                    cmbFrom.DataTextField = "FromLoc";
                    cmbFrom.DataValueField = "FromLoc";
                    cmbFrom.DataBind();
                    cmbFrom.SelectedIndex = 0;
                    break;
                case DropDownTypes.Destination:
                    //OptBO.FillType = "D";
                    //cmbTo.DataSource = null;
                    //cmbTo.Items.Clear();
                    //cmbTo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbTo.DataSource = dt;
                    //cmbTo.DataTextField = "ToLoc";
                    //cmbTo.DataValueField = "ToLoc";
                    //cmbTo.DataBind();
                    //cmbTo.SelectedIndex = 0;
                    break;
                case DropDownTypes.VehicleNo:
                    OptBO.FillType = "V";
                    //cmbVehicleNo.DataSource = null;
                    //cmbVehicleNo.Items.Clear();
                    //cmbVehicleNo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbVehicleNo.DataSource = dt;
                    //cmbVehicleNo.DataTextField = "VehicleNo";
                    //cmbVehicleNo.DataValueField = "VehicleNo";
                    //cmbVehicleNo.DataBind();
                    //cmbVehicleNo.SelectedIndex = 0;
                    break;
                case DropDownTypes.Description:
                    OptBO.SearchText = string.Empty;
                    cmbDescription.DataSource = null;
                    cmbDescription.Items.Clear();
                    cmbDescription.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRDescriptionCombo();
                    cmbDescription.DataSource = dt;
                    cmbDescription.DataTextField = "Name";
                    cmbDescription.DataValueField = "Name";
                    cmbDescription.DataBind();
                    cmbDescription.SelectedIndex = 0;
                    break;
                case DropDownTypes.PackingType:
                    OptBO.SearchText = string.Empty;
                    cmbType.DataSource = null;
                    cmbType.Items.Clear();
                    cmbType.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillPackingType();
                    cmbType.DataSource = dt;
                    cmbType.DataTextField = "Name";
                    cmbType.DataValueField = "Name";
                    cmbType.DataBind();
                    cmbType.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
        }

        private void FillDropDownDevice(long DeviceID = 0)
        {
            //LRBO OptBO = new LRBO();
            //DataTable dt;
            //cmbDevice.DataSource = null;
            //cmbDevice.Items.Clear();
            //cmbDevice.Items.Add(new ListItem("", "-1"));
            //OptBO.DeviceID = DeviceID;
            //dt = OptBO.FillDropDownDevice();
            //cmbDevice.DataSource = dt;
            //cmbDevice.DataTextField = "Name";
            //cmbDevice.DataValueField = "Srl";
            //cmbDevice.DataBind();
            //cmbDevice.SelectedIndex = 0;
        }

        private void FillDropDownVehicleType()
        {
            VehicleTypeBO OptBO = new VehicleTypeBO();
            DataTable dt;
            cmbVehicleType.DataSource = null;
            cmbVehicleType.Items.Clear();
            cmbVehicleType.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetVehicleTypedetails();
            cmbVehicleType.DataSource = dt;
            cmbVehicleType.DataTextField = "Name";
            cmbVehicleType.DataValueField = "Srl";
            cmbVehicleType.DataBind();
            cmbVehicleType.SelectedIndex = 0;
        }

        private void FillDropDownCustomer()
        {
            CustomerBO OptBO = new CustomerBO();
            DataTable dt;
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            OptBO.ShowAll = "Y";
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            dt = OptBO.GetCustomerDetailsUserWise();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        private void FillDropDownSupplier()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void FillDropDownBillingCompany()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 1;
        }

        private void FillDropDownFrom()
        {
            //DestinationBO OptBO = new DestinationBO();
            //DataTable dt;
            //cmbFrom.DataSource = null;
            //cmbFrom.Items.Clear();
            //cmbFrom.Items.Add(new ListItem("", "-1"));
            //OptBO.FromOrTo = "F";
            //dt = OptBO.GetFromOrToLocations();
            //cmbFrom.DataSource = dt;
            //cmbFrom.DataTextField = "FromLoc";
            //cmbFrom.DataValueField = "FromLoc";
            //cmbFrom.DataBind();
            //cmbFrom.SelectedIndex = 0;
        }

        private void FillDropDownDestination(string FromLoc)
        {
            //DestinationBO OptBO = new DestinationBO();
            //DataTable dt;
            //cmbTo.DataSource = null;
            //cmbTo.Items.Clear();
            //cmbTo.Items.Add(new ListItem("", "-1"));
            //OptBO.FromLoc = FromLoc;
            //dt = OptBO.GetToLocations();
            //cmbTo.DataSource = dt;
            //cmbTo.DataTextField = "ToLoc";
            //cmbTo.DataValueField = "ToLoc";
            //cmbTo.DataBind();
            //cmbTo.SelectedIndex = 0;
        }

        private void FillDropDownConsignor()
        {
            //ConsignorBO OptBO = new ConsignorBO();
            //DataTable dt;
            //cmbConsigner.DataSource = null;
            //cmbConsigner.Items.Clear();
            //cmbConsigner.Items.Add(new ListItem("", "-1"));
            //OptBO.SearchText = string.Empty;
            //dt = OptBO.GetConsignordetails();
            //cmbConsigner.DataSource = dt;
            //cmbConsigner.DataTextField = "Name";
            //cmbConsigner.DataValueField = "Srl";
            //cmbConsigner.DataBind();
            //cmbConsigner.SelectedIndex = 0;
        }

        private void FillPreviousDetails()
        {
            LRBO optBO = new LRBO();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = optBO.FillLRPreviousDetails();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["CName"] != null) setCombobox(cmbBillingCompany, dt.Rows[0]["CName"].ToString());
                    if (dt.Rows[0]["Consigner"] != null) setCombobox(cmbConsigner, dt.Rows[0]["Consigner"].ToString());
                    txtConsignerAddress.Text = dt.Rows[0]["ConsignerAddress"].ToString();
                    txtConsignorGSTNo.Text = dt.Rows[0]["ConsignorGSTNo"].ToString();
                    cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    if (dt.Rows[0]["FromLoc"] != null) setCombobox(cmbFrom, dt.Rows[0]["FromLoc"].ToString());
                    cmbFrom_SelectedIndexChanged(cmbFrom, EventArgs.Empty);
                }
            }
        }

        private void setCombobox(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.Text = lst.Value;
            else
            {
                lst = new ListItem(FieldName, FieldName);
                cmb.Items.Add(lst);
                cmb.Text = FieldName;
            }
        }

        private void setComboboxValue(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.SelectedValue = lst.Value;
            else
            {
                cmb.SelectedIndex = 0;
            }
        }

        private void SendMail()
        {
            string IsMail = ConfigurationManager.AppSettings["IsMail"].ToString();

            if (IsMail == "Y")
            {
                string expectedDateOfReporting = txtHiddenExpDate.Value;
                string emailid = txtConsigneeEmail.Text;
                string subject = "Intimation of Dispatch";
                string body = CommonTypes.GetMailCotent("L");
                string filepath = "";



                string fromId = ConfigurationManager.AppSettings["FromId"].ToString();
                string CCId = ""; // ConfigurationManager.AppSettings["CCId"].ToString();
                MailHandler mailhandler = new MailHandler();

                string FilePath = SavePdfLRForEmail(txtLRNo.Text.Trim().Replace("/", ""));

                body = string.Format(body, cmbDescription.Text, txtDate.Text, cmbConsigner.SelectedItem.Text, cmbFrom.Text, txtLRNo.Text, txtDate.Text, txtVehicleNo.Text, expectedDateOfReporting, txtRefNo.Text);

                body = "<html><head><style>body {font-family:arial;font-size:10pt;} td {font-family:arial;font-size:9pt;}</style></head><body bgcolor=#ffffff><table width=100%><tr bgcolor=#336699><td align=left style=font-weight:bold><font color=white> " +
                           " LR Tracker Tool</font></td></tr></table> <table> <tr> <td>" + body;

                body = body + "</td></tr></table><br/><br/><br/>" +
    "<hr size=2 width=100% align=center><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td>NOTE: This is a system generated notification, please <font color=red>DO NOT REPLY</font> to this mail.</td></tr></table><br><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td  valign=top style={font-family:verdana;font-size:60%;color:#888888}>This" +
    "e-mail, and any attachments thereto, are intended only for use by the addressee(s) named herein. If you are not the intended recipient of this e-mail, you are hereby notified that any dissemination, distribution or copying which amounts to misappropriation of this e-mail and any attachments thereto, is strictly prohibited. If you have received this e-mail in error, please immediately notify me and permanently delete the original and any copy of any e-mail and " +
    "any printout thereof.</td></tr></table></body></html>";

                MailMessage msg = mailhandler.CreateMessage(emailid, CCId, subject, body, true, FilePath);
                bool result = mailhandler.SendEmail(msg, true, true, false);
            }

        }

        private void SendMailTemp()
        {
            string smtpAddress = "smtp.bizmail.yahoo.com";
            int portNumber = 465;
            bool enableSSL = true;

            string emailFrom = "Transport.lr@abhigroup.co.in";
            string password = "TLR@1234";
            string emailTo = "onkarmedhekar@gmail.com";
            string subject = "Hello";
            string body = "Hello, Test Mail from lrtracker auto";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Can set to false, if you are sending pure text.

                //mail.Attachments.Add(new Attachment("C:\\SomeFile.txt"));
                //mail.Attachments.Add(new Attachment("C:\\SomeZip.zip"));

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            if (!IsPostBack)
            {
                if (bool.Parse(Session["PlyApplicable"].ToString()))
                {
                    lblnoofply.Visible = true;
                    txtnoofPly.Visible = true;
                }
                else
                {
                    lblnoofply.Visible = false;
                    txtnoofPly.Visible = false;
                }

                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                txtSearch.Focus();
            }
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                    //    LoadGridView();
                    //    MsgPanel.Message = "Record(s) deleted successfully.";
                    //    MsgPanel.DispCode = 1;
                    //    txtFromDate.Focus();
                    //}
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            LRBO optbo = new LRBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //txtLRNo.Enabled = true;
                    //FillDropdown(DropDownTypes.Consignee);
                    //FillDropdown(DropDownTypes.Consigner);
                    //FillDropdown(DropDownTypes.Destination);
                    //FillDropdown(DropDownTypes.Origin);
                    FillDropdown(DropDownTypes.VehicleNo);
                    FillDropdown(DropDownTypes.PackingType);
                    FillDropdown(DropDownTypes.Description);
                    FillDropDownDevice();
                    FillDropDownVehicleType();
                    FillDropDownCustomer();
                    FillDropDownSupplier();
                    //FillDropDownFrom();
                    //FillDropDownDestination();
                    FillDropDownConsignor();
                    ClearView();
                    txtHiddenId.Value = "0";
                    FillDropDownBillingCompany();
                    cmbBillingCompany.Enabled = true;
                    FillPreviousDetails();
                    ShowViewByIndex(0);
                    cmbBillingCompany.SelectedValue = Session["BillingCompanySrl"].ToString();
                    txtPlacementId.Focus();

                    break;
                case CommonTypes.EntryFormCommand.Save:
                    optbo.LRNo = txtLRNo.Text.Trim();
                    optbo.ID = long.Parse(txtHiddenId.Value);
                    if (txtLRNo.Text.Trim() != string.Empty && optbo.LRDuplicateCount() > 0)
                    {
                        MsgPopUp.ShowModal("Duplicate LR No.", CommonTypes.ModalTypes.Error);
                        cmbBillingCompany.Focus();
                        return;
                    }

                    DateTime lrdate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime placementdate = DateTime.ParseExact(txtVehiclePlacementDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (placementdate > lrdate)
                    {
                        MsgPopUp.ShowModal("Vehicle Placement Date cannot be greater than LR Date", CommonTypes.ModalTypes.Error);
                        txtVehiclePlacementDate.Focus();
                        return;
                    }

                    if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
                    {
                        MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Destination Master!!", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    optbo.LRDatetime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text);
                    optbo.Consignee = cmbConsignee.SelectedItem.Text.Trim();
                    optbo.Consigner = cmbConsigner.SelectedItem.Text.Trim();
                    optbo.Origin = cmbFrom.Text.Trim();
                    optbo.Destination = cmbTo.SelectedItem.Text.Trim();
                    optbo.VehicleNo = txtVehicleNo.Text.Trim();
                    optbo.Weight = decimal.Parse(txtWeight.Text);
                    optbo.ChargeableWt = decimal.Parse(txtChWeight.Text);
                    optbo.TotPackage = long.Parse(txttotPackage.Text);
                    optbo.InvoiceNo = txtInvoiceNo.Text.Trim();
                    optbo.InvoiceValue = decimal.Parse(txtInvoiceValue.Text);
                    optbo.LoadType = cmbType.Text.Trim().Substring(0, 1);
                    optbo.Remarks = cmbDescription.SelectedItem.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Status = "P";
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.DriverDtls = txtDriverDetails.Text.Trim();
                    optbo.DeviceID = 0;

                    //if (cmbDevice.Items.Count <= 0)
                    //    optbo.DeviceID = 0;
                    //else if (cmbDevice.SelectedValue == null)
                    //    optbo.DeviceID = 0;
                    //else
                    //    optbo.DeviceID = long.Parse(cmbDevice.SelectedValue);

                    optbo.RefNo = txtRefNo.Text.Trim();
                    optbo.SupplierID = -1;
                    optbo.SubSupplierID = 0;
                    optbo.CustomerID = long.Parse(cmbCustomer.SelectedValue);
                    optbo.TotalFrieght = decimal.Parse(txtHiddenCustFreight.Value);
                    optbo.SupplierAmount = decimal.Parse(txtHiddenSuplFreight.Value); ;
                    optbo.AdvanceToSupplier = -1;
                    optbo.VehicleTypeID = long.Parse(cmbVehicleType.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.Rate = decimal.Parse(txtHiddenCustRate.Value);
                    optbo.ConsignorAddress = txtConsignerAddress.Text.Trim();
                    optbo.ConsigneeAddress = txtConsigneeAddress.Text.Trim();
                    optbo.ConsigneeEmail = txtConsigneeEmail.Text.Trim();
                    optbo.ConsigneeContact = txtConsigneeContact.Text.Trim();
                    optbo.VehicleSealNo = txtvehicleSealNo.Text.Trim();
                    optbo.PackingType = cmbType.SelectedItem.Text;
                    optbo.VehiclePlacementDate = txtVehiclePlacementDate.Text;
                    optbo.ExpectedDeliveryDays = int.Parse(HFExpectedDeliveryDays.Value);
                    optbo.DriverNumber = txtDriverNumber.Text.Trim();
                    optbo.FreightType = cmbFreight.SelectedItem.Text;
                    optbo.ConsignorGSTNo = txtConsignorGSTNo.Text.Trim();
                    optbo.ConsigneeGSTNo = txtConsigneeGSTNo.Text.Trim();
                    optbo.RefNo_Invoice = txtRefNo_Invoice.Text.Trim();
                    optbo.InvoiceDate = txtInvoiceDate.Text.Trim();
                    optbo.EWayBillNo = txtEwayBillNo.Text.Trim();
                    optbo.ShipmentDocNo = txtShippingDocNo.Text.Trim();
                    optbo.InternalBillingDocNo = txtInternalbillingNo.Text.Trim();
                    optbo.PetrolPumpId = 0;

                    if (txtnoofPly.Visible)
                    {
                        optbo.PlyQty = string.IsNullOrEmpty(txtnoofPly.Text) ? 0 : int.Parse(txtnoofPly.Text);
                    }
                    else
                    {
                        optbo.PlyQty = 0;
                    }
                    optbo.ConsignorEmail = txtConsignorEmail.Text.Trim();
                    optbo.ConsignorTelNo = txtConsignorContact.Text.Trim();
                    optbo.ConsignorPincode = txtConsignerPIN.Text.Trim();
                    optbo.ConsigneePincode = txtConsigneePIN.Text.Trim();
                    optbo.EwayBillDate = txtEwayBillDate.Text.Trim();
                    long cnt = optbo.InsertUpdateLR();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {

                            string LRNo = string.Empty;
                            optbo.ID = cnt;
                            LRNo = optbo.GetLRNo();


                            //SendMail();
                            MsgPanel.Message = "Record Saved successfully with LR No. : " + LRNo;
                            MsgPanel.DispCode = 1;
                            //Session["LRID"] = cnt.ToString();
                            //Session["ReportType"] = "LRDoc";
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                        }
                        else
                        {
                            //SendMail();
                            MsgPanel.Message = "Record Updated successfully with LR No. : " + txtLRNo.Text.Trim();
                            MsgPanel.DispCode = 1;
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;

                case CommonTypes.EntryFormCommand.SaveP:
                    CalculateAmount();
                    optbo.LRNo = txtLRNo.Text.Trim();
                    optbo.ID = long.Parse(txtHiddenId.Value);
                    if (txtLRNo.Text.Trim() != string.Empty && optbo.LRDuplicateCount() > 0)
                    {
                        MsgPopUp.ShowModal("Duplicate LR No.", CommonTypes.ModalTypes.Error);
                        cmbBillingCompany.Focus();
                        return;
                    }

                    DateTime lrdate1 = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime placementdate1 = DateTime.ParseExact(txtVehiclePlacementDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (placementdate1 > lrdate1)
                    {
                        MsgPopUp.ShowModal("Vehicle Placement Date cannot be greater than LR Date", CommonTypes.ModalTypes.Error);
                        dtpBtnPlace.Focus();
                        return;
                    }

                    if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
                    {
                        MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Destination Master!!", CommonTypes.ModalTypes.Error);
                        return;
                    }                    

                    optbo.LRDatetime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text);
                    optbo.Consignee = cmbConsignee.SelectedItem.Text.Trim();
                    optbo.Consigner = cmbConsigner.SelectedItem.Text.Trim();
                    optbo.Origin = cmbFrom.Text.Trim();
                    optbo.Destination = cmbTo.SelectedItem.Text.Trim();
                    optbo.VehicleNo = txtVehicleNo.Text.Trim();
                    optbo.Weight = decimal.Parse(txtWeight.Text);
                    optbo.ChargeableWt = decimal.Parse(txtChWeight.Text);
                    optbo.TotPackage = long.Parse(txttotPackage.Text);
                    optbo.InvoiceNo = txtInvoiceNo.Text.Trim();
                    optbo.InvoiceValue = decimal.Parse(txtInvoiceValue.Text);
                    optbo.LoadType = cmbType.Text.Trim().Substring(0, 1);
                    optbo.Remarks = cmbDescription.SelectedItem.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Status = "P";
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.DriverDtls = txtDriverDetails.Text.Trim();
                    optbo.DeviceID = 0;

                    //if (cmbDevice.Items.Count <= 0)
                    //    optbo.DeviceID = 0;
                    //else if (cmbDevice.SelectedValue == null)
                    //    optbo.DeviceID = 0;
                    //else
                    //    optbo.DeviceID = long.Parse(cmbDevice.SelectedValue);

                    optbo.RefNo = txtRefNo.Text.Trim();
                    optbo.SupplierID = txtHiddenSuplId.Value == "" ? -1 : long.Parse(txtHiddenSuplId.Value);
                    optbo.SubSupplierID = 0;
                    optbo.CustomerID = long.Parse(cmbCustomer.SelectedValue);
                    optbo.TotalFrieght = decimal.Parse(txtHiddenCustFreight.Value);
                    optbo.SupplierAmount = decimal.Parse(txtHiddenSuplFreight.Value);
                    optbo.AdvanceToSupplier = -1;
                    optbo.VehicleTypeID = long.Parse(cmbVehicleType.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.Rate = decimal.Parse(txtHiddenCustRate.Value);
                    optbo.ConsignorAddress = txtConsignerAddress.Text.Trim();
                    optbo.ConsigneeAddress = txtConsigneeAddress.Text.Trim();
                    optbo.ConsigneeEmail = txtConsigneeEmail.Text.Trim();
                    optbo.ConsigneeContact = txtConsigneeContact.Text.Trim();
                    optbo.VehicleSealNo = txtvehicleSealNo.Text.Trim();
                    optbo.PackingType = cmbType.SelectedItem.Text;
                    optbo.VehiclePlacementDate = txtVehiclePlacementDate.Text;
                    optbo.ExpectedDeliveryDays = int.Parse(HFExpectedDeliveryDays.Value);
                    optbo.DriverNumber = txtDriverNumber.Text.Trim();
                    optbo.FreightType = cmbFreight.SelectedItem.Text;
                    optbo.ConsignorGSTNo = txtConsignorGSTNo.Text.Trim();
                    optbo.ConsigneeGSTNo = txtConsigneeGSTNo.Text.Trim();
                    optbo.RefNo_Invoice = txtRefNo_Invoice.Text.Trim();
                    optbo.InvoiceDate = txtInvoiceDate.Text.Trim();
                    optbo.EWayBillNo = txtEwayBillNo.Text.Trim();
                    optbo.Multipoint = false;
                    optbo.MainLRNo = "";
                    optbo.ShipmentDocNo = txtShippingDocNo.Text.Trim();
                    optbo.InternalBillingDocNo = txtInternalbillingNo.Text.Trim();
                    optbo.placementId = long.Parse(txtPlacementId.Text);
                    if (txtnoofPly.Visible)
                    {
                        optbo.PlyQty = string.IsNullOrEmpty(txtnoofPly.Text) ? 0 : int.Parse(txtnoofPly.Text);
                    }
                    else
                    {
                        optbo.PlyQty = 0;
                    }

                    optbo.ConsignorEmail = txtConsignorEmail.Text.Trim();
                    optbo.ConsignorTelNo = txtConsignorContact.Text.Trim();
                    optbo.ConsignorPincode = txtConsignerPIN.Text.Trim();
                    optbo.ConsigneePincode = txtConsigneePIN.Text.Trim();
                    optbo.EwayBillDate = txtEwayBillDate.Text;

                    long cnt1 = optbo.InsertUpdateLR();
                    if (cnt1 > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {

                            string LRNo = string.Empty;
                            optbo.ID = cnt1;
                            LRNo = optbo.GetLRNo();

                            if (!string.IsNullOrEmpty(hdTripId.Value))
                            {
                                optbo.TripId = long.Parse(hdTripId.Value);
                                optbo.UpdateTripId();
                            }


                            //SendMail();
                            MsgPanel.Message = "Record Saved successfully.";
                            MsgPanel.DispCode = 1;
                            Session["LRID"] = cnt1.ToString();
                            SavePdfLRForDownLoad(cnt1.ToString());
                            //Session["ReportType"] = "LRDoc";
                            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                        }
                        else
                        {
                            //SendMail();
                            SavePdfLRForDownLoad(txtHiddenId.Value);
                            MsgPanel.Message = "Record Updated successfully.";
                            MsgPanel.DispCode = 1;
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }
                    break;

                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private long UpdateTripIdInLR(long LRId, long TripId)
        {
            LRBO optbo = new LRBO();
            optbo.ID = LRId;
            optbo.TripId = TripId;
            long cnt = 0;
            cnt = optbo.UpdateTripId();
            return cnt;
        }



        private string AddTrip(string refNo, string originAddr, double orglatitude, double orglongitute, string destinationAddr, double destlatitude, double destlongitute, string driverName, string driverNo, string vehicleNumber, string ewayBillNo, string invoiceNo)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");

                var lrObject = new
                {
                    lrnumber = refNo,
                    loading = new
                    {
                        lat = orglatitude,
                        lng = orglongitute,
                        address = originAddr,
                        area = originAddr
                    },
                    unloading = new
                    {
                        lat = destlatitude,
                        lng = destlongitute,
                        address = destinationAddr,
                        area = destinationAddr
                    },
                    vehicleNumber = vehicleNumber,
                    locationSource = "sim",
                    driverName = driverName,
                    driverNumbers = new string[] {
                        driverNo
                    },
                    customValues = new
                    {
                        ewayBillNo = ewayBillNo,
                        invoiceNumber = invoiceNo
                    }
                };

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/add");
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.UploadString(urlStringBuilder.ToString(), javaScriptSerializer.Serialize(lrObject));
                string str = @"""id""" + ":";
                if (!result.ToString().Contains(str))
                {
                    return "";
                }
                string tripId = result.ToString();
                var offset = tripId.IndexOf(':');
                var endOffset = tripId.IndexOf('}') - 1;
                offset = tripId.IndexOf(':', offset + 1);
                offset = tripId.IndexOf(':', offset + 1);

                tripId = tripId.Substring(offset + 1, endOffset - offset);

                return tripId;

                //var responseObject = javaScriptSerializer.DeserializeObject(result);



            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                if (keys["Status"].ToString() == "Delivered" && Session["ROLEID"].ToString() != "1" && Session["ROLEID"].ToString() != "8")
                {
                    MsgPopUp.ShowModal("LR Status is Delivered Hence Cannot Modify.", CommonTypes.ModalTypes.Error);
                    return;
                }
                if (decimal.Parse(keys["AdvanceToSupplier"].ToString()) > -1)
                {
                    MsgPopUp.ShowModal("LR Backend Updation Is Done. For Any Changes Please Contact Admin.", CommonTypes.ModalTypes.Error);
                    return;
                }
                txtLRNo.Enabled = false;
                //FillDropdown(DropDownTypes.Consignee);
                //FillDropdown(DropDownTypes.Consigner);
                FillDropdown(DropDownTypes.Destination);
                FillDropdown(DropDownTypes.Origin);
                FillDropdown(DropDownTypes.VehicleNo);
                FillDropdown(DropDownTypes.PackingType);
                FillDropdown(DropDownTypes.Description);
                FillDropDownVehicleType();
                FillDropDownCustomer();
                //cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                FillDropDownSupplier();
                FillDropDownFrom();
                //FillDropDownDestination();
                FillDropDownConsignor();
                FillDropDownBillingCompany();

                if (keys["DeviceID"] != null)
                {
                    if (keys["DeviceID"].ToString() != string.Empty)
                        FillDropDownDevice(long.Parse(keys["DeviceID"].ToString()));
                    else
                        FillDropDownDevice();
                }
                else
                    FillDropDownDevice();

                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                cmbBillingCompany.Enabled = false;
                txtHiddenId.Value = keys["ID"].ToString();
                txtHiddenSuplId.Value = keys["SupplierID"].ToString();
                txtLRNo.Text = keys["LRNo"].ToString();
                txtDate.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("dd/MM/yyyy").Replace("-", "/");
                //CalendarExtender3.EndDate = DateTime.Parse(txtDate.Text);
                ddlHH.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("HH");
                ddlmm.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("mm");
                if (keys["CustomerID"] != null)
                {
                    if (keys["CustomerID"].ToString() != string.Empty)
                    {
                        cmbCustomer.SelectedValue = keys["CustomerID"].ToString();
                        cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                    }
                }
                setCombobox(cmbConsigner, keys["Consigner"].ToString()); //cmbConsigner.SelectedValue = keys["Consigner"].ToString();
                cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                setComboboxValue(cmbConsignee, keys["Consignee"].ToString());
                //cmbConsignee.SelectedValue = keys["Consignee"].ToString();
                setCombobox(cmbFrom, keys["FromLoc"].ToString()); //cmbFrom.SelectedValue = keys["FromLoc"].ToString();
                FillDropDownDestination(keys["FromLoc"].ToString());
                setCombobox(cmbTo, keys["ToLoc"].ToString()); //cmbTo.SelectedValue = keys["ToLoc"].ToString();
                //cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                HFExpectedDeliveryDays.Value = "0";
                if (!string.IsNullOrEmpty(keys["ExpectedDeliveryDays"].ToString()))
                {
                    HFExpectedDeliveryDays.Value = keys["ExpectedDeliveryDays"].ToString();
                }

                txtVehicleNo.Text = keys["VehicleNo"].ToString();
                txtWeight.Text = keys["Weight"].ToString();
                txtChWeight.Text = keys["ChargeableWt"].ToString();
                txttotPackage.Text = keys["TotPackages"].ToString();
                txtInvoiceNo.Text = keys["InvoiceNo"].ToString();
                txtInvoiceValue.Text = keys["InvoiceValue"].ToString();
                //cmbType.SelectedIndex = keys["LoadType"].ToString() == "F" ? 0 : 1;
                if (keys["Remarks"] != null) setCombobox(cmbDescription, keys["Remarks"].ToString());
                //cmbDescription.SelectedValue = keys["Remarks"] == null ? "" : keys["Remarks"].ToString();
                //ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,PackingType
                txtConsigneeAddress.Text = keys["ConsigneeAddress"].ToString();
                if (keys["ConsignerAddress"] != null)
                {
                    if (keys["ConsignerAddress"].ToString().Trim() != string.Empty)
                        txtConsignerAddress.Text = keys["ConsignerAddress"].ToString();
                }
                txtConsigneeEmail.Text = keys["ConsigneeEmail"].ToString();
                txtConsigneeContact.Text = keys["ConsigneeTelNo"].ToString();
                txtvehicleSealNo.Text = keys["VehicleSealNo"].ToString();
                if (keys["PackingType"] != null) setCombobox(cmbType, keys["PackingType"].ToString());
                //cmbType.SelectedValue = keys["PackingType"] == null ? "" : keys["PackingType"].ToString(); //keys["PackingType"].ToString();

                txtDriverDetails.Text = keys["DriverDetails"].ToString();

                //if (keys["DeviceID"] != null)
                //{
                //    if (keys["DeviceID"].ToString() != string.Empty)
                //        cmbDevice.SelectedValue = keys["DeviceID"].ToString();
                //}
                if (keys["SupplierID"] != null)
                {
                    if (keys["SupplierID"].ToString() != string.Empty)
                        cmbSupplier.SelectedValue = keys["SupplierID"].ToString();
                }

                if (keys["VehicleTypeID"] != null)
                {
                    if (keys["VehicleTypeID"].ToString() != string.Empty)
                        cmbVehicleType.SelectedValue = keys["VehicleTypeID"].ToString();
                }
                if (keys["RefNo"] != null)
                    txtRefNo.Text = keys["RefNo"].ToString();

                if (keys["VehiclePlacementDate"] != null)
                {
                    if (keys["VehiclePlacementDate"].ToString().Trim() != string.Empty)
                        txtVehiclePlacementDate.Text = DateTime.Parse(keys["VehiclePlacementDate"].ToString()).ToString("dd/MM/yyyy");
                }
                if (keys["DriverNumber"] != null)
                    txtDriverNumber.Text = keys["DriverNumber"].ToString();

                if (keys["FreightType"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["FreightType"].ToString().Trim()))
                        cmbFreight.SelectedValue = keys["FreightType"].ToString();
                }
                if (keys["ConsignorGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsignorGSTNo"].ToString().Trim()))
                        txtConsignorGSTNo.Text = keys["ConsignorGSTNo"].ToString();
                }
                if (keys["ConsigneeGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsigneeGSTNo"].ToString().Trim()))
                        txtConsigneeGSTNo.Text = keys["ConsigneeGSTNo"].ToString();
                }
                if (keys["RefNo_Invoice"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["RefNo_Invoice"].ToString().Trim()))
                        txtRefNo_Invoice.Text = keys["RefNo_Invoice"].ToString();
                }
                if (keys["InvoiceDate"] != null)
                {
                    if (keys["InvoiceDate"].ToString().Trim() != string.Empty)
                        txtInvoiceDate.Text = DateTime.Parse(keys["InvoiceDate"].ToString()).ToString("dd/MM/yyyy");
                }
                //Multipoint Details
                txtEwayBillNo.Text = keys["EwayBillNo"].ToString();
                //chkMultipoint.Checked = bool.Parse(keys["MultiPoint"].ToString());

                //if (chkMultipoint.Checked)
                //    txtMainLRNo.Enabled = true;
                //else
                //    txtMainLRNo.Enabled = false;

                //txtMainLRNo.Text = keys["MainLRNo"].ToString();

                if (keys["PlacementID"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["PlacementID"].ToString().Trim()))
                        txtPlacementId.Text = keys["PlacementID"].ToString();
                }
                txtPlacementId.Enabled = false;

                txtHiddenCustRate.Value = keys["Rate"].ToString();
                txtHiddenCustFreight.Value = keys["TotalFrieght"].ToString();
                txtHiddenSuplFreight.Value = keys["SupplierAmount"].ToString();

                // For Finolex
                txtShippingDocNo.Text = keys["ShipmentDocNo"].ToString();
                txtInternalbillingNo.Text = keys["InternalBillingDocNo"].ToString();
                //
                txtConsignorEmail.Text = keys["ConsignorEmail"].ToString();
                txtConsignorContact.Text = keys["ConsignorTelNo"].ToString();
                txtConsignerPIN.Text = keys["ConsignorPincode"].ToString();
                txtConsigneePIN.Text = keys["ConsigneePincode"].ToString();
                txtnoofPly.Text = keys["PlyQty"].ToString();

                if (keys["EwayBillDate"] != null && keys["EwayBillDate"].ToString().Trim() != string.Empty)
                    txtEwayBillDate.Text = DateTime.Parse(keys["EwayBillDate"].ToString()).ToString("dd/MM/yyyy");
                else
                    txtEwayBillDate.Text = "";


                DestinationBO OptBo = new DestinationBO();
                OptBo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
                OptBo.ToLoc = cmbTo.SelectedItem.Text.Trim();
                lblExpDeliveryDays.Text = "(Expected Delivery Days :" + OptBo.GetDestinationExpDays().ToString() + ")";
                txtHiddenExpDate.Value = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(double.Parse(OptBo.GetDestinationExpDays().ToString())).ToString("dd/MM/yyyy");
                //TDA Fields disable
                cmbSupplier.Enabled = false;
                cmbCustomer.Enabled = false;
                cmbVehicleType.Enabled = false;
                txtVehicleNo.Enabled = false;
                txtVehiclePlacementDate.Enabled = false;
                cmbFrom.Enabled = false;
                cmbTo.Enabled = false;

                ShowViewByIndex(0);
                cmbBillingCompany.Focus();
            }
            if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For LR No. " + keys["LRNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["ID"].ToString()));
            }
            if (e.CommandName == "ViewReport")
            {
                {
                    Session["LRID"] = keys["ID"].ToString();
                    Session["ReportType"] = "LRDoc";

                    //Response.Redirect("~/Reports/PolicyView.aspx?PolicyID=" + policyID.ToString());
                    //string winCmd=;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow","window.open('../Pages/LRPrint.aspx?LRID="+keys["ID"].ToString()+"','_blank')" , true);
                }
            }
            if (e.CommandName == "SavePdf")
            {
                //Session["LRID"] = keys["ID"].ToString();
                SavePdfLRForDownLoad(keys["ID"].ToString());

            }
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void cmbDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MsgPanel.Message = "";
            //MsgPanel.DispCode = -1;
            //if (cmbDevice == null) return;
            //if (cmbDevice.SelectedValue == null) return;
            //if (cmbDevice.SelectedIndex > -1)
            //{
            //    LRBO OptBO = new LRBO();
            //    OptBO.DeviceID = long.Parse(cmbDevice.SelectedValue);
            //    long cnt = OptBO.Devise_Used();
            //    if (cnt > 0)
            //    {
            //        MsgPopUp.ShowModal("Device is Allocated to previous LRs.", CommonTypes.ModalTypes.Error);
            //        cmbDevice.Focus();
            //    }
            //}
        }


        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtHiddenId.Value == "0")
            {
                //LRBO optbo = new LRBO();
                //optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                //optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                //DataTable dt = optbo.GetLRNoByCompName();
                //if (dt.Rows.Count > 0)
                //{

                //    txtLRNo.Text = dt.Rows[0][0].ToString();
                //    txtLRNo.Enabled = false;
                //}
            }
            cmbBillingCompany.Focus();
        }


        protected void cmbFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblExpDeliveryDays.Text = "0";
            FillDropDownDestination(cmbFrom.SelectedItem.Text.Trim());
            cmbTo.Focus();
        }

        protected void cmbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lblExpDeliveryDays.Text = "0";
            //HFExpectedDeliveryDays.Value = "0";
            //if (cmbTo.SelectedIndex > 0)
            //{
            //    DestinationBO OptBo = new DestinationBO();
            //    OptBo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
            //    OptBo.ToLoc = cmbTo.SelectedItem.Text.Trim();
            //    HFExpectedDeliveryDays.Value = OptBo.GetDestinationExpDays().ToString();
            //    if (HFExpectedDeliveryDays.Value == null) HFExpectedDeliveryDays.Value = "0";
            //    if (HFExpectedDeliveryDays.Value.ToString().Trim() == string.Empty) HFExpectedDeliveryDays.Value = "0";
            //    lblExpDeliveryDays.Text = "(Expected Delivery Days :" + HFExpectedDeliveryDays.Value + ")";
            //    txtHiddenExpDate.Value = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(double.Parse(HFExpectedDeliveryDays.Value)).ToString("dd/MM/yyyy");
            //    if (int.Parse(HFExpectedDeliveryDays.Value) == 0)
            //    {
            //        MsgPopUp.ShowModal("Please enter From To Expeceted Delivery Days Record in Customer Freight Master!!", CommonTypes.ModalTypes.Error);
            //        return;
            //    }
            //    txtWeight.Focus();
            //}
            //else
            //{
            //    cmbTo.Focus();
            //}

        }



        protected void cmbConsigner_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtConsignerAddress.Text = "0";
            if (cmbConsigner.SelectedIndex > 0)
            {
                txtConsignerAddress.Text = string.Empty;
                txtConsignorGSTNo.Text = string.Empty;
                ConsignorBO OptBo = new ConsignorBO();
                OptBo.Srl = long.Parse(cmbConsigner.SelectedValue);
                DataTable dt = OptBo.GetConsignorDetailsById();
                if (dt == null) return;
                if (dt.Rows.Count <= 0) return;

                txtConsignerAddress.Text = dt.Rows[0]["Address"].ToString();
                txtConsignorGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                txtConsignorEmail.Text = dt.Rows[0]["EmailId"].ToString();
                txtConsignorContact.Text = dt.Rows[0]["MobileNo"].ToString();
                txtConsignerPIN.Text = dt.Rows[0]["Pincode"].ToString();

            }
            cmbConsigner.Focus();
        }
        #endregion

        protected void cmbConsignee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConsignorBO optbo = new ConsignorBO();
            optbo.Srl = long.Parse(cmbConsignee.SelectedValue);

            DataTable dt = optbo.GetConsignorDetailsById();
            //if (txtHiddenId.Value == "0")
            //{
            if (dt.Rows.Count > 0)
            {
                txtConsigneeAddress.Text = dt.Rows[0]["Address"].ToString();
                txtConsigneeEmail.Text = dt.Rows[0]["EmailId"].ToString();
                txtConsigneeContact.Text = dt.Rows[0]["MobileNo"].ToString();
                txtConsigneeGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                txtConsigneePIN.Text = dt.Rows[0]["Pincode"].ToString();
            }
            //}
            txtConsigneeAddress.Focus();
        }

        private string SavePdfLRForEmail(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(txtHiddenId.Value);
            DataTable dt = OptBO.LR_Document();
            string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, "Y");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            return fileName;
        }

        private void SavePdfLRForDownLoad(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(LRNo);
            DataTable dt = OptBO.LR_Document();
            string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, "N");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            //return fileName;
            Session["FilePath"] = fileName;
            RptDoc.Close();
            RptDoc.Dispose();
            GC.Collect();
            //Response.Redirect("ViewFile.aspx", false);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            CalendarExtender3.EndDate = DateTime.Parse(txtDate.Text);
            txtDate.Focus();
        }

        //protected void chkMultipoint_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkMultipoint.Checked)
        //    {
        //        txtMainLRNo.Enabled = true;
        //        txtMainLRNo.Focus();
        //        rfvMainLR.Enabled = true;
        //    }
        //    else
        //    {
        //        txtMainLRNo.Text = string.Empty;
        //        txtMainLRNo.Enabled = false;
        //        rfvMainLR.Enabled = false;
        //        btnSavePacklist.Focus();

        //    }
        //}

        protected void btnPlacement_Click(object sender, EventArgs e)
        {
            LRBO optbo = new LRBO();
            optbo.placementId = long.Parse(txtPlacementId.Text);
            DataTable dt = optbo.GetPlacementDetails();
            if (dt.Rows.Count > 0)
            {
                hdTripId.Value = dt.Rows[0]["TripId"].ToString();
                DataTable dtLR = optbo.GetLRNosForPlacementId();
                string lRNosForPlacementId = "";
                if (dtLR.Rows.Count > 0)
                {
                    for (int i = 0; i < dtLR.Rows.Count; i++)
                    {
                        if (lblLRNosForPlacementId.Text == "")
                        {
                            lRNosForPlacementId = dtLR.Rows[i]["LRNo"].ToString();
                        }
                        else
                        {
                            lRNosForPlacementId = lRNosForPlacementId + "," + dtLR.Rows[i]["LRNo"].ToString();
                        }
                    }
                    txtHiddenCustFreight.Value = "0";
                    txtHiddenSuplFreight.Value = "0";
                    txtHiddenSuplRate.Value = "0";
                    txtHiddenCustRate.Value = "0";

                    lblLrConfirmation.Text = "LR(s) are already created for Placement Id " + txtPlacementId.Text + " <br/>" + lRNosForPlacementId + "<br/>" + "Do you still want to create LR?";
                    msgpopAuth.Show();


                }
                else
                {
                    txtHiddenSuplFreight.Value = dt.Rows[0]["Freight"].ToString();
                    txtHiddenCustFreight.Value = dt.Rows[0]["CustomerFreight"].ToString();
                    txtHiddenSuplRate.Value = dt.Rows[0]["Freight"].ToString();
                    txtHiddenCustRate.Value = dt.Rows[0]["CustomerFreight"].ToString();

                }


                txtHiddenSuplId.Value = dt.Rows[0]["SupplierId"].ToString();
                txtVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                txtVehicleNo.Enabled = false;
                txtDriverNumber.Text = dt.Rows[0]["DriverNo"].ToString();
                //txtDriverNumber.Enabled = false;
                txtPlacementId.Enabled = false;
                setCombobox(cmbFrom, dt.Rows[0]["Origin"].ToString());
                cmbFrom.SelectedValue = dt.Rows[0]["Origin"].ToString();

                cmbFrom_SelectedIndexChanged(cmbFrom, EventArgs.Empty);
                setCombobox(cmbTo, dt.Rows[0]["Destination"].ToString());
                cmbTo.SelectedValue = dt.Rows[0]["Destination"].ToString();
                cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                cmbSupplier.SelectedValue = dt.Rows[0]["SupplierId"].ToString();
                cmbVehicleType.SelectedValue = dt.Rows[0]["VehicleTypeId"].ToString();
                cmbCustomer.SelectedValue = dt.Rows[0]["CustomerId"].ToString();
                cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);

                if (long.Parse(dt.Rows[0]["DealerId"].ToString()) > 0)
                {
                    cmbCustomer.SelectedValue = dt.Rows[0]["DealerId"].ToString();
                }

                HFExpectedDeliveryDays.Value = dt.Rows[0]["ExpDeliveryDays"].ToString();

                lblExpDeliveryDays.Text = "(Expected Delivery Days :" + HFExpectedDeliveryDays.Value + ")";


                txtVehiclePlacementDate.Text = DateTime.Parse(dt.Rows[0]["PlacementDate"].ToString()).ToString("dd/MM/yyyy");
                //Disable the data from TDA
                cmbSupplier.Enabled = false;
                cmbCustomer.Enabled = false;
                cmbVehicleType.Enabled = false;
                txtVehiclePlacementDate.Enabled = false;
                cmbFrom.Enabled = false;
                cmbTo.Enabled = false;
                CalculateAmount();
                cmbBillingCompany.Focus();
            }
            else
            {
                MsgPopUp.ShowModal("Invalid Placement Id", CommonTypes.ModalTypes.Error);
                txtPlacementId.Focus();
                return;
            }
            //txtVehicleNo.Enabled = false;
        }

        private void CalculateAmount()
        {
            if (Session["UOM"] == null) return;
            if (Session["UOM"].ToString() == string.Empty) return;


            if (txtChWeight.Text.Trim() == string.Empty) txtChWeight.Text = "0";
            if (txtHiddenCustRate.Value == string.Empty) txtHiddenCustRate.Value = "0";
            if (txtHiddenSuplRate.Value == string.Empty) txtHiddenSuplRate.Value = "0";
            if (txttotPackage.Text.Trim() == string.Empty) txttotPackage.Text = "0";

            if (Session["UOM"].ToString().ToLower() == "veh")
            {
                txtHiddenSuplFreight.Value = txtHiddenSuplFreight.Value.Replace(".00", "");
                txtHiddenCustFreight.Value = txtHiddenCustFreight.Value.Replace(".00", "");
                txtHiddenCustRate.Value = txtHiddenCustFreight.Value;
                txtHiddenSuplRate.Value = txtHiddenSuplFreight.Value;
            }

            if (Session["UOM"].ToString().ToLower() == "ton")
            {
                txtHiddenSuplFreight.Value = Math.Round(((decimal.Parse(txtChWeight.Text)) * decimal.Parse(txtHiddenSuplRate.Value)), 0).ToString().Replace(".00", "");
                txtHiddenCustFreight.Value = Math.Round(((decimal.Parse(txtChWeight.Text)) * decimal.Parse(txtHiddenCustRate.Value)), 0).ToString().Replace(".00", "");
            }
            if (Session["UOM"].ToString().ToLower() == "pkg")
            {
                txtHiddenSuplFreight.Value = Math.Round((decimal.Parse(txttotPackage.Text) * decimal.Parse(txtHiddenSuplRate.Value)), 0).ToString().Replace(".00", "");
                txtHiddenCustFreight.Value = Math.Round((decimal.Parse(txttotPackage.Text) * decimal.Parse(txtHiddenCustRate.Value)), 0).ToString().Replace(".00", "");
            }


        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName.ToString().ToLower() == "yes")
            {
                LRBO optbo = new LRBO();
                optbo.placementId = long.Parse(txtPlacementId.Text);
                DataTable dt = optbo.GetPlacementDetails();
                if (dt.Rows.Count > 0)
                {

                    txtVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                    txtVehicleNo.Enabled = false;
                    txtPlacementId.Enabled = false;
                    msgpopAuth.Hide();
                }
            }
            if (e.CommandName.ToString().ToLower() == "no")
            {

                txtPlacementId.Text = "";
                txtPlacementId.Focus();
                msgpopAuth.Hide();
            }
        }


        private string AddTrip()
        {
            return string.Empty;
        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex > 0)
            {
                ConsignorBO optbo = new ConsignorBO();
                optbo.CustomerId = long.Parse(cmbCustomer.SelectedValue);
                DataSet ds = optbo.FillConsignorConsignee();

                cmbConsigner.DataSource = null;
                cmbConsigner.Items.Clear();
                cmbConsigner.Items.Add(new ListItem("", "-1"));

                cmbConsigner.DataSource = ds.Tables[0];
                cmbConsigner.DataTextField = "Name";
                cmbConsigner.DataValueField = "ConsignorId";
                cmbConsigner.DataBind();
                cmbConsigner.SelectedIndex = 0;
                if (cmbConsigner.Items.Count==2)
                {
                    cmbConsigner.SelectedIndex = 1;
                    cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbConsignee.Focus();
                }

                cmbConsignee.DataSource = null;
                cmbConsignee.Items.Clear();
                cmbConsignee.Items.Add(new ListItem("", "-1"));

                cmbConsignee.DataSource = ds.Tables[1];
                cmbConsignee.DataTextField = "Name";
                cmbConsignee.DataValueField = "ConsignorId";
                cmbConsignee.DataBind();
                cmbConsignee.SelectedIndex = 0;
                if (cmbConsignee.Items.Count == 2)
                {
                    cmbConsignee.SelectedIndex = 1;
                    cmbConsignee_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbFrom.Focus();
                }
            }
        }
    }
}
