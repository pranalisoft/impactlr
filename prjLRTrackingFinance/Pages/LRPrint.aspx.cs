﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRPrint : System.Web.UI.Page
    {
        private void InitializeContent(long LRID)
        {
            ReportBO pol = new ReportBO();
            pol.ID = LRID;
            DataTable dt = pol.LR_Document();

            //PnlItemDtls.Visible = true;
            //Byte[] logo = (byte[]) dt.Rows[0]["Logo"].ToString(); 
            //lblItemDtls.Text = "Policy Details For Ref. No.: " + LRID.ToString();
            //imgLogo.ImageUrl = "~\\Include\\Common\\images\\ihon.jpg";
           // if (employee.Photo != null && employee.Photo.ToString().Trim() != string.Empty)
           // {
               // byte[] bytes = (byte[])employee.Photo;
               // string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            //imgLogo.ImageUrl = "data:image/png;base64," + dt.Rows[0]["Logo"].ToString();
           // }

            Byte[] bytes = (Byte[])dt.Rows[0]["Logo"];
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            imgLogo.ImageUrl = "data:image/png;base64," + base64String;

            lblCompanyName.Text = dt.Rows[0]["CName"].ToString();
            lblCompanyAddress1.Text = dt.Rows[0]["CAddress1"].ToString();
            lblCompanyAddress2.Text = dt.Rows[0]["CAddress2"].ToString();
            lblCompanyAddress3.Text = dt.Rows[0]["CAddress3"].ToString();
            lblCompanyPhone.Text = dt.Rows[0]["CLandline"].ToString();

            lblLRDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MM/yyyy");
            lblLRNo.Text = dt.Rows[0]["LRNo"].ToString();
            lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
            lblTo.Text = dt.Rows[0]["ToLoc"].ToString();

            lblConsignorName.Text = dt.Rows[0]["Consigner"].ToString();
            lblConsignorAddress.Text = dt.Rows[0]["ConsignerAddress"].ToString();
            lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
            lblVehicleType.Text = dt.Rows[0]["VehicleTypeName"].ToString();

            lblConsigneeName.Text = dt.Rows[0]["Consignee"].ToString();
            lblConsigneeAddress.Text = dt.Rows[0]["ConsigneeAddress"].ToString();
            lblConsigneePhone.Text = dt.Rows[0]["ConsigneeTelNo"].ToString();
            lblConsigneeEmail.Text = dt.Rows[0]["ConsigneeEmail"].ToString();

            lblTypeOfPacking.Text = dt.Rows[0]["PackingType"].ToString();
            lblDriverName.Text = dt.Rows[0]["DriverDetails"].ToString();
            lblDriverMobile.Text = "";// dt.Rows[0]["Owner"].ToString();
            lblNoOfPackages.Text = dt.Rows[0]["TotPackages"].ToString();
            lblChargeableWt.Text = dt.Rows[0]["ChargeableWt"].ToString().Replace(".000", "");
            lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
            lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString().Replace(".00", "");
            lblVehicleSealNo.Text = dt.Rows[0]["VehicleSealNo"].ToString();
            lblPAN.Text = dt.Rows[0]["CPAN"].ToString();
            lblGST.Text = dt.Rows[0]["CServiceTax"].ToString();      
            //if (dt.Rows[0]["PreviousPolicyNo"] != null)
            //{
            //    if (dt.Rows[0]["PreviousPolicyNo"].ToString() != string.Empty)
            //    {
            //        lblPrevCompany.Text = dt.Rows[0]["PreviousCompany"].ToString();
            //        lblPrevPolicyNo.Text = dt.Rows[0]["PreviousPolicyNo"].ToString();
            //        lblPrevStartDate.Text = DateTime.Parse(dt.Rows[0]["PreviousRiskStartDate"].ToString()).ToString("dd/MM/yyyy");
            //        lblPrevEndDate.Text = DateTime.Parse(dt.Rows[0]["PreviousRiskEndDate"].ToString()).ToString("dd/MM/yyyy");
            //    }
            //}

            //pol.SearchText = string.Empty;
            //DataTable dt1 = pol.FillPolicy_Payments();
            //lstViewPayments.DataSource = dt1;
            //lstViewPayments.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["LRID"]))
                    {
                        long LRID = long.Parse(Request.QueryString["LRID"]);
                        InitializeContent(LRID);
                    }
                }
            }
        }
    }
}