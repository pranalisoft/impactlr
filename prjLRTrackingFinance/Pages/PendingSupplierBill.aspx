﻿<%@ Page Title="Supplier Bill Not Rcvd" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="PendingSupplierBill.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PendingSupplierBill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Supplier Bill Not Rcvd
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <%--<td width="30px">
                                        <asp:Label ID="Label18" Text="From" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                    </td>
                                    <td width="30px">
                                        <asp:Label ID="Label17" Text="To" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                    </td>--%>
                                    <td width="50px">
                                        <asp:Label ID="lbl" runat="server" Text="Branch" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="250px">
                                        <asp:DropDownList ID="cmbBranch" runat="server" CssClass="NormalTextBold" TabIndex="5"
                                            Width="250px" AppendDataBoundItems="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td width="50px">
                                        <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" ValidationGroup="show"/>
                                    </td>
                                    <td width="50px">
                                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                            OnClick="btnExcel_Click" ValidationGroup="show"/>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label1" Text="Total Records : " runat="server" CssClass="NormalTextBold"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="lblTotalRecords" Text="0" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="overflow: auto">
                                <asp:GridView ID="grdView" runat="server" Width="100%" AutoGenerateColumns="false"
                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="LRNo,LRDate,SupplierName,Consigner,Consignee,FromLoc,ToLoc,PODDate,ChargeableWt,TotalFrieght,SupplierAmount" ShowFooter="True">
                                    <Columns>
                                        <asp:BoundField DataField="SupplierName" HeaderText="Supplier" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="LRNo" HeaderText="LR No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="LRDate" HeaderText="Date" ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Consigner" HeaderText="Consigner" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="Consignee" HeaderText="Consignee" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="FromLoc" HeaderText="From Loc" ItemStyle-Width="150px" />
                                        <asp:BoundField DataField="ToLoc" HeaderText="To Loc" ItemStyle-Width="150px" />
                                        <asp:BoundField DataField="PODDate" HeaderText="POD Rcvd Date" ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="ChargeableWt" HeaderText="Chargeable Wt." ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TotalFrieght" HeaderText="Frieght" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />  
                                        <asp:BoundField DataField="SupplierAmount" HeaderText="Supl. Amount" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />                                      
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
