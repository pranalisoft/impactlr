﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;               
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void LoadGridView()
        {
            CustomerBO optbo = new CustomerBO();
            optbo.SearchText = txtSearch.Text.Trim();
            optbo.ShowAll = "Y";
            DataTable dt = optbo.GetCustomerDetails();
            grdView.DataSource = dt;
            grdView.DataBind();
            lblTotal.Text = "Total Records :" + dt.Rows.Count.ToString();
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            CustomerBO optbo = new CustomerBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    FillCustomerGroup();
                    txtName.Focus(); 
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Name = txtName.Text.Trim();
                    optbo.Srl = long.Parse(txtHiddenId.Value);
                    if (optbo.isCustomerAlreadyExist() ==true)
                    {
                        SetPanelMsg("Customer Name Already Exists", true, 0);
                        txtName.Focus();
                        return;
                    }

                    optbo.Address1 = txtAddress1.Text.Trim();
                    optbo.Address2 = txtAddress2.Text.Trim();
                    optbo.Address3 = txtAddress3.Text.Trim();
                    optbo.landline = txtLandline.Text.Trim();
                    optbo.mobile = txtMobile.Text.Trim();
                    optbo.emailID = txtEmailID.Text.Trim();
                    optbo.CST = txtCSTNo.Text.Trim();
                    optbo.VAT = txtVAT.Text.Trim();
                    optbo.vendor = txtVendor.Text.Trim();
                    optbo.TIN = "";//txtTIN.Text.Trim();
                    optbo.ServiceTaxno ="";// txtServiceTaxNo.Text.Trim();
                    optbo.creditdays = txtCreditDays.Text.Trim()=="" ? 0: long.Parse(txtCreditDays.Text);
                    optbo.creditLimit = txtCreditLimit.Text.Trim() == "" ? 0 : decimal.Parse(txtCreditLimit.Text.Trim());
                    optbo.isActive = chkActive.Checked;
                    optbo.GLCode = txtGL.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.IsphysicalPODReqd = chkphysicalPOD.Checked;
                    optbo.CustomerGroupId = long.Parse(cmbCustGroup.SelectedValue);

                    long cnt = optbo.InsertCustomer();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Customer Created Successfully", true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Customer Details Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    SetPanelMsg("", false, 0);
                    int cnt1 = 0;
                    foreach (GridViewRow row in grdView.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cnt1 = cnt1 + 1;
                                break;
                            }
                        }
                    }
                    if (cnt1 > 0)
                    {
                        popDelPacklist.Show();
                    }
                    else
                    {
                        SetPanelMsg("Please select atleast one record to delete", true, 0);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private void FillCustomerGroup()
        {
            try
            {
                cmbCustGroup.DataSource = null;
                cmbCustGroup.Items.Clear();
                cmbCustGroup.Items.Add(new ListItem("", "-1"));
                DataTable dt = new CustomerGroupDAO().GetCustomerGroups("");
                cmbCustGroup.DataSource = dt;
                cmbCustGroup.DataTextField = "Name";
                cmbCustGroup.DataValueField = "Srl";
                cmbCustGroup.DataBind();
                cmbCustGroup.SelectedIndex = 0;
            }
            catch (Exception exp)
            {

            }
        }


        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }


        private void ClearView()
        {
            txtName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtAddress3.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLandline.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtCSTNo.Text = string.Empty;
            txtCreditDays.Text = string.Empty;
            txtCreditLimit.Text = string.Empty;
            txtVAT.Text = string.Empty;
            //txtTIN.Text = string.Empty;
            //txtServiceTaxNo.Text = string.Empty;
            txtVendor.Text = string.Empty;
            chkActive.Checked = true;
            txtGL.Text = string.Empty;
            cmbCustGroup.SelectedIndex = -1;
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                FillCustomerGroup();
                txtHiddenId.Value = keys["Srl"].ToString();
                txtName.Text = keys["Name"].ToString();
                txtAddress1.Text = keys["Address1"].ToString();
                txtAddress2.Text = keys["Address2"].ToString();
                txtAddress3.Text = keys["Address3"].ToString();
                txtLandline.Text = keys["Landline"].ToString();
                txtMobile.Text = keys["Mobile"].ToString();
                txtEmailID.Text = keys["EmailId"].ToString();
                txtVendor.Text = keys["VendorCode"].ToString();
                txtCSTNo.Text = keys["CSTNo"].ToString();
                txtVAT.Text = keys["VATNo"].ToString();
                //txtTIN.Text = keys["TINNo"].ToString();
                //txtServiceTaxNo.Text = keys["ServiceTaxNo"].ToString();
                txtCreditDays.Text = keys["CreditDays"].ToString();
                txtCreditLimit.Text = keys["CreditLimit"].ToString();
                chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                chkphysicalPOD.Checked = bool.Parse(keys["PhysicalPODreqd"].ToString());
                txtGL.Text = keys["GLCode"].ToString();
                if (!string.IsNullOrEmpty(keys["CustomerGroupId"].ToString()))
                {
                    cmbCustGroup.SelectedValue = keys["CustomerGroupId"].ToString();
                }
                ShowViewByIndex(0);
                txtName.Focus();
            }
           
        }

        protected void grdView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

        protected void grdView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        LoadGridView();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private long Delete()
        {
            CustomerBO OptBO = new CustomerBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdView.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteCustomer();
            }

            return srl;
        }
    }
}