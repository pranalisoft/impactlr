﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Spire.Xls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DownloadPurhchaseInvoices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            optBO.SearchText = "";
            optBO.BranchID = 0;
            optBO.FromDate = "01/04/2022";
            optBO.ToDate = "17/01/2023";
            DataTable dt = optBO.FillInvoiceGrid();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ExportToExcel(long.Parse(dt.Rows[i]["Srl"].ToString()), dt.Rows[i]["InvoiceDate"].ToString(), dt.Rows[i]["Name"].ToString(), dt.Rows[i]["BillingCompany"].ToString(), dt.Rows[i]["InvRefNo"].ToString(), dt.Rows[i]["Address1"].ToString(), dt.Rows[i]["Address2"].ToString(), dt.Rows[i]["Address3"].ToString(), dt.Rows[i]["CName"].ToString(), dt.Rows[i]["CAddress1"].ToString(), dt.Rows[i]["CAddress2"].ToString(), dt.Rows[i]["CAddress3"].ToString(), dt.Rows[i]["PAN"].ToString(), dt.Rows[i]["InvRefNo"].ToString());
            }
        }

        private void ExportToExcel(long InvoiceSrl, string InvoiceDate, string Customer, string BillingCompany, string InvoiceNo, string Address1, string Address2, string Address3, string CName, string CAddress1, string CAddress2, string CAddress3, string PAN,string InvoiceRefNo)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                string invref = InvoiceRefNo.Replace("/", "_");
                invref = invref.Replace("-","");
                int RowsToAdd = 13;
                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SuplInvoice_" + invref + ".xlsx");
                string filenamepdf = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SuplInvoice_" + invref + ".pdf");
                SuplInvoiceBO OptBo = new SuplInvoiceBO();
                OptBo.Srl = InvoiceSrl;
                DataSet ds = OptBo.InvoicePrint();
                DataTable dt = ds.Tables[0];
                DataTable dtSign = ds.Tables[1];

                int BalColindex = 0;
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName == "Balance")
                    {
                        BalColindex = i;
                        break;
                    }
                }
                if (BalColindex == dt.Columns.Count - 1)
                {
                    dt.Columns[16].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                    dt.Columns[5].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                }
                else
                {
                    //int penaltycols = dt.Columns.Count - 1 - BalColindex;
                    dt.Columns[17].SetOrdinal(dt.Columns.Count - 1);
                    dt.AcceptChanges();
                    dt.Columns[16].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                    dt.Columns[5].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                }
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(5);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(10);
                //dt.AcceptChanges();              

                //dt.Columns.Add("Total", typeof(decimal));
                //decimal total = 0;
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    total = 0;
                //    for (int j = 5; j < dt.Columns.Count - 1; j++)
                //    {
                //        if (dt.Rows[i][j] == null || dt.Rows[i][j].ToString().Trim() == "") dt.Rows[i][j] = "0";
                //        total = total + decimal.Parse(dt.Rows[i][j].ToString());
                //    }
                //    dt.Rows[i]["Total"] = (total).ToString("F0");
                //}
                //dt.AcceptChanges();

                decimal total = 0;
                dt.Rows.Add();
                int Rcnt = dt.Rows.Count - 1;
                dt.Rows[Rcnt][0] = "Total";

                for (int i = 9; i < dt.Columns.Count; i++)
                {
                    total = 0;
                    for (int j = 0; j < dt.Rows.Count - 1; j++)
                    {
                        if (dt.Rows[j][i] == null || dt.Rows[j][i].ToString().Trim() == "") dt.Rows[j][i] = "0";
                        total = total + decimal.Parse(dt.Rows[j][i].ToString());
                    }
                    dt.Rows[Rcnt][i] = total.ToString("F0");
                }
                dt.AcceptChanges();

                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(InvoiceSrl.ToString());
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                objWorksheet.Cells["A1"].LoadFromDataTable(dt, true);
                objWorksheet.Cells["B:B"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["F:F"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["G:G"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["H:H"].Style.Numberformat.Format = "dd/MM/yyyy";
                //objWorksheet.Cells["A1:Z1"].AutoFilter = true;                 

                objWorksheet.InsertRow(1, RowsToAdd);
                int RowCnt = dt.Rows.Count + RowsToAdd;
                string Letter = ReturnLetter(dt.Columns.Count - 1);
                string LetterNoAndDate1 = ReturnLetter(dt.Columns.Count - 5);
                int AccSignRow = dt.Columns.Count - 5;
                string LetterNoAndDate2 = ReturnLetter(dt.Columns.Count - 4);
                string LetterNoAndDate3 = ReturnLetter(dt.Columns.Count - 3);

                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.Column(1).Width = 25;
                objWorksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                objWorksheet.Column(1).Style.WrapText = true;
                objWorksheet.Column(5).Width = 12;

                for (int i = 2; i <= dt.Columns.Count; i++)
                {
                    objWorksheet.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                }

                //for (int i = RowsToAdd+1; i <= RowCnt; i++)
                //{
                //    objWorksheet.Row(i).Height = 18;
                //}
                objWorksheet.Row(RowsToAdd + 1).Height = 20;
                objWorksheet.Row(RowCnt + 1).Height = 21;


                //objWorksheet.Cells["A2"].Value = Customer;
                using (ExcelRange objRange = objWorksheet.Cells["A1:" + Letter + "1"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 20;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //objRange.Value = "Annexure For Invoice No. " + InvoiceNo;
                    objRange.Value = "Settlement Note";
                }
                objWorksheet.Row(1).Height = 30;
                using (ExcelRange objRange = objWorksheet.Cells["A1:" + Letter + "1"])
                {
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A2:" + Letter + "2"])
                {
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                }
                objWorksheet.Cells["A:A"].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                objWorksheet.Cells[Letter + ":" + Letter].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                objWorksheet.Row(3).Height = 20;
                objWorksheet.Row(4).Height = 20;
                objWorksheet.Row(5).Height = 20;
                objWorksheet.Row(6).Height = 20;
                objWorksheet.Row(7).Height = 20;
                objWorksheet.Row(8).Height = 20;
                objWorksheet.Row(9).Height = 20;
                objWorksheet.Row(10).Height = 20;

                using (ExcelRange objRange = objWorksheet.Cells["A3:C3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Customer;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A4:C4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address1;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A5:C5"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address2;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A6:C6"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address3;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A7:C7"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "PAN : " + PAN;
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "3:" + LetterNoAndDate2 + "3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Invoice No. ";
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate3 + "3:" + Letter + "3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = InvoiceNo;
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "4:" + LetterNoAndDate2 + "4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Invoice Date ";
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate3 + "4:" + Letter + "4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = InvoiceDate.Replace("00:00:00", "");
                    objRange.Style.Numberformat.Format = "dd/MM/yyyy";
                }

                using (ExcelRange objRange = objWorksheet.Cells["A7:" + Letter + "7"])
                {
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                }

                using (ExcelRange objRange = objWorksheet.Cells["A8:C8"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Buyer";
                }
                using (ExcelRange objRange = objWorksheet.Cells["A9:C9"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CName;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A10:C10"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress1;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A11:C11"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress2;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A12:C12"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress3;
                }

                //using (ExcelRange objRange = objWorksheet.Cells["A3:" + Letter + "3"])
                //{
                //    //objRange.Merge = true;
                //    objRange.Style.Font.Bold = true;
                //    objRange.Style.Font.Size = 14;
                //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //    objRange.Value = Customer;
                //}
                //using (ExcelRange objRange = objWorksheet.Cells["A4:" + Letter + "4"])
                //{
                //    //objRange.Merge = true;
                //    //objRange.Style.Font.Bold = true;
                //    objRange.Style.Font.Size = 13;
                //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //    objRange.Value = Address1;
                //}


                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowsToAdd + 1).ToString() + ":" + Letter + (RowsToAdd + 1).ToString()])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowsToAdd + 2).ToString() + ":" + Letter + (RowCnt + 1).ToString()])
                {
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowCnt + 1).ToString() + ":" + Letter + (RowCnt + 1).ToString()])
                {
                    objRange.Style.Font.Size = 12;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowCnt + 2).ToString() + ":" + Letter + (RowCnt + 2).ToString()])
                {
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.Font.Bold = true;
                    objRange.Value = "GST Not Applicable.";
                }
                int rcntSign = 36;
                if (RowCnt + 2 >= 36)
                {
                    rcntSign = RowCnt + 2;
                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 1).ToString() + ":" + Letter + (rcntSign + 1).ToString()])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "For " + Customer;
                    //}

                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 5).ToString() + ":" + Letter + (rcntSign + 5).ToString()])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "Authorised Signatory";
                    //}

                    using (ExcelRange objRange = objWorksheet.Cells["B" + (rcntSign + 1).ToString() + ":C" + (rcntSign + 1).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["B" + (rcntSign + 5).ToString() + ":C" + (rcntSign + 5).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Operation Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 1).ToString() + ":" + Letter + (rcntSign + 1).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 5).ToString() + ":" + Letter + (rcntSign + 5).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Accounts Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["A" + (rcntSign + 6).ToString() + ":" + Letter + (rcntSign + 6).ToString()])
                    {
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    }
                }
                else
                {
                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "36:" + Letter + "36"])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "For " + Customer;
                    //}

                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "40:" + Letter + "40"])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "Authorised Signatory";
                    //}


                    using (ExcelRange objRange = objWorksheet.Cells["B36:C36"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }



                    using (ExcelRange objRange = objWorksheet.Cells["B40:C40"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Operation Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "36:" + Letter + "36"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "40:" + Letter + "40"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Accounts Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["A41:" + Letter + "41"])
                    {
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    }
                }
                System.Drawing.Image imgOperation;
                System.Drawing.Image imgAccounts;
                MemoryStream ms;
                byte[] imageOperation = null;
                byte[] imageAccounts = null;

                if (dtSign.Rows[0]["OperationApprovalImage"] != null && dtSign.Rows[0]["OperationApprovalImage"].ToString().Trim() != string.Empty)
                {
                    imageOperation = (byte[])dtSign.Rows[0]["OperationApprovalImage"];
                    ms = new MemoryStream(imageOperation);
                    imgOperation = System.Drawing.Image.FromStream(ms);
                    var picture = objWorksheet.Drawings.AddPicture("OprApp", imgOperation);
                    picture.SetPosition(rcntSign, 0, 1, 0);
                    picture.SetSize(100, 50);

                }
                ms = null;
                if (dtSign.Rows[0]["AccountsApprovalImage"] != null && dtSign.Rows[0]["AccountsApprovalImage"].ToString().Trim() != string.Empty)
                {
                    imageAccounts = (byte[])dtSign.Rows[0]["AccountsApprovalImage"];
                    ms = new MemoryStream(imageAccounts);
                    imgAccounts = System.Drawing.Image.FromStream(ms);
                    var picture1 = objWorksheet.Drawings.AddPicture("AccApp", imgAccounts);
                    picture1.SetPosition(rcntSign, 0, AccSignRow, 0);
                    picture1.SetSize(100, 50);
                }
                //Write it back to the client
                if (File.Exists(filename))
                    File.Delete(filename);
                //Create excel file on physical disk
                FileStream objFileStrm = File.Create(filename);
                objFileStrm.Close();
                //Write content to excel file
                File.WriteAllBytes(filename, objExcelPackage.GetAsByteArray());


                Workbook workbook = new Workbook();
                workbook.LoadFromFile(filename);
                if (File.Exists(filenamepdf))
                    File.Delete(filenamepdf);

                Worksheet sheet = workbook.Worksheets[0];
                sheet.PageSetup.Orientation = PageOrientationType.Landscape;
                sheet.PageSetup.IsFitToPage = true;
                //sheet.PageSetup.FitToPagesWide = 1;
                //sheet.PageSetup.FitToPagesTall = 0;
                sheet.PageSetup.TopMargin = 0.5;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;
                sheet.SaveToPdf(filenamepdf);

                //System.IO.FileInfo fileInfo = new System.IO.FileInfo(filenamepdf);

               // Session["FilePath"] = filenamepdf;

               // ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../pages/ViewFile.aspx','_blank')", true);

                //DownloadFile
                //Response.Clear();
                //Response.ClearContent();
                //Response.Buffer = true;
                //Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                //Response.ContentType = "application/pdf";
                //Response.TransmitFile(fileInfo.FullName);
                //Response.End();
            }
        }

        private string ReturnLetter(int Cnt)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (Cnt >= letters.Length)
                value += letters[Cnt / letters.Length - 1];

            value += letters[Cnt % letters.Length];

            return value;
        }
    }
}