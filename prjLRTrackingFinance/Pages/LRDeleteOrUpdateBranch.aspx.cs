﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Globalization;
using BusinessObjects.DAO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRDeleteOrUpdateBranch : System.Web.UI.Page
    {
        #region Procedures
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }
        private void FillComboLR()
        {
            LRBO OptBO = new LRBO();
            cmbLR.DataSource = null;
            cmbLR.Items.Clear();
            cmbLR.Items.Add(new ListItem("", "-1"));
            DataTable dt = OptBO.FillLRComboDelete();
            cmbLR.DataSource = dt;
            cmbLR.DataTextField = "LRNo";
            cmbLR.DataValueField = "ID";
            cmbLR.DataBind();
            cmbLR.SelectedIndex = 0;
        }
        private void FillComboBranch()
        {            
            UserDAO userDao = new UserDAO();
            DataTable dtBranch = userDao.GetUserBranches(long.Parse(Session["EID"].ToString()));
            cmbBranch.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.Items.Add(new ListItem("", "-1"));
            cmbBranch.DataSource = dtBranch;
            cmbBranch.DataTextField = "Field1";
            cmbBranch.DataValueField = "BranchSrl";
            cmbBranch.DataBind();
            cmbBranch.SelectedIndex = 0;
        }
        private void LoadData(string LRNo)
        {
            lblLRNo.Text = string.Empty;
            lblDate.Text = string.Empty;
            lblConsigner.Text = string.Empty;
            lblConsignee.Text = string.Empty;
            lblFrom.Text = string.Empty;
            lblTo.Text = string.Empty;
            lblVehicleNo.Text = string.Empty;
            lblPackages.Text = string.Empty;
            lblWeight.Text = string.Empty;
            lblChargeableWeight.Text = string.Empty;
            lblInvoiceNo.Text = string.Empty;
            lblInvoiceValue.Text = string.Empty;
            lblType.Text = string.Empty;
            lblRemarks.Text = string.Empty;
            lblDriverDetails.Text = string.Empty;
            lblBranch.Text = string.Empty;
            cmbBranch.SelectedIndex = 0;

            LRBO OptBO = new LRBO();
            OptBO.LRNo = LRNo;
            DataTable dt = OptBO.GetLRHeaderByNo();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    lblLRNo.Text = LRNo;
                    lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                    lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                    lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                    lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                    lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                    lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                    lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                    lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                    lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                    lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                    lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                    lblType.Text = dt.Rows[0]["LoadType"].ToString();
                    lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                    lblDriverDetails.Text = dt.Rows[0]["DriverDetails"].ToString();
                    lblBranch.Text = dt.Rows[0]["BranchName"].ToString();
                    cmbBranch.SelectedValue = dt.Rows[0]["BranchId"].ToString();
                }
            }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboLR();
                FillComboBranch();
            }
            cmbLR.Focus();
        }
        protected void cmbLR_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            LoadData(cmbLR.SelectedItem.Text);
            cmbLR.Focus();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbLR.SelectedIndex<=0)
                {
                    SetPanelMsg("Please Select LR From The List", true, 0);
                    cmbLR.Focus();
                    return;
                }
                if (cmbBranch.SelectedIndex <= 0)
                {
                    SetPanelMsg("Please Select Branch From The List", true, 0);
                    cmbBranch.Focus();
                    return;
                }
                if (cmbBranch.SelectedItem.Text.Trim() == lblBranch.Text.Trim())
                {
                    SetPanelMsg("Please Select Different Branch From The List", true, 0);
                    cmbBranch.Focus();
                    return;
                }

                LRBO OptBO = new LRBO();
                OptBO.ID = long.Parse(cmbLR.SelectedValue);
                OptBO.BranchID = long.Parse(cmbBranch.SelectedValue);
                long Cnt = OptBO.LRUpdateBranch();
                if (Cnt > 0)
                {                    
                    cmbLR.SelectedIndex = 0;
                    cmbLR_SelectedIndexChanged(cmbLR, EventArgs.Empty);
                    SetPanelMsg("Branch Updated Successfully.", true, 1);
                    cmbLR.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                SetPanelMsg(ex.Message, true, 0);
                throw;
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbLR.SelectedIndex <= 0)
                {
                    SetPanelMsg("Please Select LR From The List", true, 0);
                    cmbLR.Focus();
                    return;
                }                

                LRBO OptBO = new LRBO();
                OptBO.ID = long.Parse(cmbLR.SelectedValue);
                OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
                OptBO.CancelledRemarks = txtCancelReason.Text.Trim();
                long Cnt = OptBO.LRDelete();
                if (Cnt > 0)
                {                    
                    FillComboLR();
                    cmbLR_SelectedIndexChanged(cmbLR, EventArgs.Empty);
                    SetPanelMsg("LR Cancelled Successfully.", true, 1);
                    txtCancelReason.Text = string.Empty;
                    cmbLR.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                SetPanelMsg(ex.Message, true, 0);
                throw;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }
        #endregion     
    }
}