﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using System.Net.Mail;
using RestSharp;
using RestSharp.Authenticators;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierPayment : System.Web.UI.Page
    {
        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private long Delete()
        {
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdViewIndex.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdViewIndex.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeletePayment();
            }
            return srl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            txtDate_CalendarExtender.EndDate = DateTime.Now;
            //if (!IsPostBack)
            //{
            //    fillBank();
            //    FillComboSupplier();
            //    cmbSupl.Text = String.Empty;
            //    cmbPaymentMode.SelectedIndex = 1;
            //    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            //    cmbPaymentMode.SelectedIndex = 0;
            //    //lblOutstanding.Visible = false;
            //    txtDate.Focus();

            //}
            if (!IsPostBack)
            {
                Session.Remove("dtRcptInvDtls");
                ShowViewByIndex(0);
                chkDateFilter.Checked = true;
                txtFromDate.Text = DateTime.Now.ToString("01/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                LoadGridView();
                PnlItemDtls.Visible = false;
                txtFromDate.Focus();
                tblSummary.Visible = false;
            }
            RegisterDateTextBox();
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdViewIndex, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdViewIndex, "chkSelect");
            }
        }

        private void LoadGridView()
        {
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.DatefilterYes = chkDateFilter.Checked;
            OptBO.SearchFrom = txtFromDate.Text;
            OptBO.SearchTo = txtToDate.Text;
            OptBO.PayType = "N";
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            OptBO.SearchText = txtSearch.Text.Trim();
            DataTable dt = OptBO.FillPaymentHeader();
            grdViewIndex.DataSource = dt;
            grdViewIndex.DataBind();
            lblRecCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }

        private void LoadGridViewItem(long RcptId, string RcptNo)
        {
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.Srl = RcptId;
            OptBO.refNo = RcptNo;
            DataTable dt = OptBO.FillPaymentInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
            lblItemCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
            {
                lblItemDtls.Text = "Invoice Details (" + RcptNo + ")";
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal adjamtLR = 0;
                decimal adjamtTDS = 0;

                TextBox txt=(TextBox) sender;
                if (txt.ID == "txtTDSPer")
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        if (txt.Text.Trim() != string.Empty)
                            grdPaymentSupl.Rows[i].Cells[7].Text = txt.Text;
                        else
                            grdPaymentSupl.Rows[i].Cells[7].Text = grdPaymentSupl.DataKeys[i]["TDSPer"].ToString();
                    }
                }
                decimal number = 0;
                decimal TDSPaidCount = 0;
                decimal TDS = 0;
                if (!Decimal.TryParse(txtAmount.Text, out number))
                    return;
                if (txtAmount.Text.Trim() != String.Empty)
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txttds = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                        txtadj.Text = String.Empty;
                        txttds.Text = String.Empty;
                    }

                    decimal BalAmt = decimal.Parse(txtAmount.Text);
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        DataKey keys = grdPaymentSupl.DataKeys[i];
                        if (BalAmt > 0)
                        {
                            TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                            TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                            TextBox txttds = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                            TDSPaidCount = decimal.Parse(grdPaymentSupl.DataKeys[i]["TDSPaidCount"].ToString());
                            decimal adjamt = 0;
                            decimal tdsamt = 0;
                            if (decimal.Parse(txtbal.Text) >= BalAmt)
                            {
                                adjamt = BalAmt;
                            }
                            else
                            {
                                adjamt = decimal.Parse(txtbal.Text);
                            }
                            adjamt = decimal.Round(adjamt);
                            TDS = (decimal.Parse(keys["TotalCharges"].ToString()) * decimal.Parse(grdPaymentSupl.Rows[i].Cells[7].Text)) / 100;
                            TDS = decimal.Round(TDS);
                            if (TDSPaidCount <= TDS)
                            {
                                tdsamt = TDS - TDSPaidCount;
                                if (tdsamt <= adjamt)
                                    txttds.Text = decimal.Round(tdsamt).ToString("F2").Replace(".00", "");
                                else
                                    txttds.Text = adjamt.ToString("F2").Replace(".00", "");
                            }
                            else
                            {
                                txttds.Text = "0";
                                tdsamt = 0;
                            }

                            if (decimal.Parse(txttds.Text) <= adjamt)
                                txtadj.Text = decimal.Round((adjamt - decimal.Parse(txttds.Text))).ToString("F2").Replace(".00", "");
                            else
                                txtadj.Text = "0";

                            adjamtLR = adjamtLR + decimal.Parse(txtadj.Text);
                            adjamtTDS = adjamtTDS + decimal.Parse(txttds.Text);
                            BalAmt = BalAmt - adjamt;
                        }
                        else
                            break;
                    }
                }
                else
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txttds = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                        txtadj.Text = String.Empty;
                        txttds.Text = String.Empty;
                    }
                }
                txtTotalLR.Text = adjamtLR.ToString();
                txtTotalTDS.Text = adjamtTDS.ToString();
                txtAmount.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void FillComboSupplier()
        {
            cmbSupl.Items.Clear();
            cmbSupl.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillSupplierCombo();
            cmbSupl.DataSource = dt;
            cmbSupl.DataTextField = "Name";
            cmbSupl.DataValueField = "Srl";
            cmbSupl.DataBind();
            cmbSupl.SelectedIndex = 0;

            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt1 = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt1;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;
        }

        private void fillBank()
        {
            cmbBank.Items.Clear();
            CommonBO bankbo = new CommonBO();
            DataTable dt = bankbo.FillBankDetails();
            cmbBank.DataSource = dt;
            cmbBank.DataTextField = "BankName";
            cmbBank.DataValueField = "BankName";
            cmbBank.DataBind();
        }

        private void FillLRs()
        {
            SupplierPaymentBO custbo = new SupplierPaymentBO();
            custbo.SupplierID = long.Parse(cmbSupl.SelectedValue);
            custbo.BranchID = long.Parse(cmbBillingCompany.SelectedValue);
            DataSet  dt = custbo.ShowPaymentGrid();

            grdPaymentSupl.DataSource = dt.Tables[0];
            grdPaymentSupl.DataBind();

            txtAmount.Text = "0";
            txtMDApproveAmount.Text = "0";
            txtFinalPayable.Text = "0";
            if (dt.Tables[1] != null)
            {
                if (dt.Tables[1].Rows.Count > 0)
                {
                    txtMDApproveAmount.Text = dt.Tables[1].Rows[0]["BalanceApprovedAmount"].ToString().Replace(".00", "");
                    txtFinalPayable.Text = decimal.Round((decimal.Parse(txtMDApproveAmount.Text) * decimal.Parse("1.10"))).ToString().Replace(".00", "");
                    txtAmount.Text = txtMDApproveAmount.Text;
                    txtAmount_TextChanged(txtAmount, EventArgs.Empty);
                }
            }

            if (dt.Tables[2] != null)
            {
                grdItemStatus.DataSource = dt.Tables[2];
                grdItemStatus.DataBind();
            }
            
            DataTable dtStatus = custbo.ShowPaymentStatus();

            GrdPaymentStatus.DataSource = dtStatus;
            GrdPaymentStatus.DataBind();
            cmbPaymentMode.Focus();
        }

        private void setGrid()
        {
            DataTable dt = CommonTypes.CreateDataTable("Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt,LRId,LRNo,TotalCharges,PaidAmount,BalanceLRAmount,TDSPer", "dtPayment", "long,string,dateTime,decimal,decimal,decimal,long,string,decimal,decimal,decimal,decimal");
            grdPaymentSupl.DataSource = dt;
            grdPaymentSupl.DataBind();

            txtAmount.Text = "0";
            txtMDApproveAmount.Text = "0";
        }

        private void checkPaymentHold()
        {
            SupplierPaymentBO custbo = new SupplierPaymentBO();
            custbo.SupplierID = long.Parse(cmbSupl.SelectedValue);
            DataTable dt = custbo.CheckPaymentHold();
            lblPaymentHold.Visible = false;
            btnSave.Visible = true;
            lblPaymentHold.Text = "";
            if (dt != null)
            {
                if (dt.Rows.Count>0)
                {
                    if (int.Parse(dt.Rows[0]["HoldCnt"].ToString())>0)
                    {
                        lblPaymentHold.Visible = true;
                        btnSave.Visible = false;
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        lblPaymentHold.Text = "Payment Is Hold For This Supplier Due To " + textInfo.ToTitleCase(dt.Rows[0]["HoldRemarks"].ToString());
                    }
                }
            }            
        }

        protected void cmbSupl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setGrid();
                lblPaymentHold.Visible = false;
                btnSave.Visible = true;
                tblSummary.Visible = false;
                txtTDSPer.Text = "0";
                if (cmbSupl.SelectedIndex > 0)
                {
                    FillLRs();
                    checkPaymentHold();
                    tblSummary.Visible = true;
                    cmbPaymentMode.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setGrid();
                if (cmbBillingCompany.SelectedIndex > 0)
                {
                    FillLRs();
                    if (cmbSupl.SelectedIndex>0)
                    {
                        checkPaymentHold();
                    }
                    cmbSupl.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdPaymentSupl_Validated(object source, ServerValidateEventArgs args)
        {

        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    if (Delete() > 0)
                    {
                        LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    }
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Payment from Supplier";
                    ShowViewByIndex(1);
                    fillBank();
                    FillComboSupplier();
                    clearAll();
                    //cmbSupl.Text = String.Empty;
                    cmbPaymentMode.SelectedIndex = 1;
                    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                    cmbPaymentMode.SelectedIndex = 4;
                    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    int cntDel = 0;
                    foreach (GridViewRow row in grdViewIndex.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cntDel = cntDel + 1;
                                break;
                            }
                        }
                    }
                    if (cntDel > 0)
                    {
                        MsgPopUp.ShowModal("Are you sure.<br/>Do you want to cancel selected rcpts?", CommonTypes.ModalTypes.Confirm);
                    }
                    else
                    {
                        MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                    }
                    PnlItemDtls.Visible = false;
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    decimal totamt = 0;
                    decimal balAmt = 0;
                    string paymentxml = "";

                    paymentxml = GenerateXML();

                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txtTDS = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                        TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                        if (txtadj.Text.Trim() != String.Empty)
                        {
                            totamt = totamt + decimal.Parse(txtadj.Text) + decimal.Parse(txtTDS.Text);
                            balAmt += decimal.Parse(txtbal.Text);
                        }
                    }

                    if (balAmt < decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount cannot be greater than balance amount", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    if (totamt != decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount and total of adjusted amount should be equal", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    SupplierPaymentBO custbo = new SupplierPaymentBO()
                    {
                        tranDate = txtDate.Text.Trim(),
                        SupplierID = long.Parse(cmbSupl.SelectedValue),
                        refNo = String.Empty,
                        remarks = String.Empty,
                        totalAmount = decimal.Parse(txtAmount.Text),
                        userID = long.Parse(Session["EID"].ToString()),
                        paymentXML = paymentxml,
                        BankName = cmbBank.Text.Trim(),
                        PaymentMode = cmbPaymentMode.SelectedValue,
                        ChqNo = TxtChqNo.Text.Trim(),
                        ChqDate = txtchqdt.Text.Trim(),
                        CardNetBankingDetails = TxtChqNo.Text.Trim(),
                        PayType = "N",
                        BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString())
                    };
                    long cnt = custbo.InsertPaymentDetails();
                    if (cnt > 0)
                    {
                        SupplierPaymentBO refbo = new SupplierPaymentBO();
                        refbo.Srl = cnt;
                        string refNoforemail = "";
                        DataTable dtref = refbo.GetRefNo();
                        refNoforemail = dtref.Rows[0][0].ToString();
                        SavePdfVoucher(refNoforemail);
                        SupplierPaymentBO OptBO = new SupplierPaymentBO();
                        OptBO.refNo = refNoforemail;
                        DataTable dt = OptBO.GetPaymentDetailsForWhatsApp();
                        bool MessageSent = false;
                        if (dt != null & dt.Rows.Count > 0)
                        {
                            if (dt.Rows[0]["MobileNo"].ToString().Trim().Length==10)
                            {
                                string ContactPersonName = dt.Rows[0]["ContactPersonName"].ToString().Trim();
                                string fromDate = DateTime.Parse(dt.Rows[0]["FromDate"].ToString()).ToString("dd/MMM/yyyy");
                                string toDate = DateTime.Parse(dt.Rows[0]["ToDate"].ToString()).ToString("dd/MMM/yyyy");
                                string PhoneNo = dt.Rows[0]["MobileNo"].ToString();
                                int Success = SendMessege(cmbSupl.SelectedItem.Text, refNoforemail, ContactPersonName, fromDate, toDate, "91" + PhoneNo);
                                if (Success == 1)
                                {
                                    MessageSent = true;
                                }
                                else
                                {
                                    SupplierPaymentBO failBo = new SupplierPaymentBO();
                                    failBo.refNo = refNoforemail;
                                    failBo.tranDate = txtDate.Text.Trim();
                                    failBo.supplier = cmbSupl.SelectedItem.Text;
                                    failBo.mobileNo = PhoneNo;
                                    failBo.totalAmount = decimal.Parse(txtAmount.Text);
                                    failBo.user = Session["ENAME"].ToString();
                                    long insertCnt = failBo.InsertPaymentWhatsAppFail();
                                }
                            }
                        }
                        SendMailPaymentVoucher(refNoforemail);
                        if (MessageSent)
                            MsgPanel.Message = "Record Saved successfully.";
                        else
                            MsgPanel.Message = "Record Saved successfully. Whatsapp Messege Not Sent";
                        MsgPanel.DispCode = 1;
                        clearAll();
                        LoadGridView();
                        ShowViewByIndex(0);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    LoadGridView();
                    ShowViewByIndex(0);
                    break;
                case CommonTypes.EntryFormCommand.None:
                    break;
                default:
                    break;
            }
        }

        private void SendMailPaymentVoucher(string RefNo)
        {
            string suplEmailId = "";
            string suplName = "";
            string userEmailId = "";
            string ToId = "";
            string CCId = "";
            ReportBO optbo = new ReportBO();
            optbo.RefNo = RefNo;
            DataSet ds = optbo.GetPaymentVoucherEmailDetails();
            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];

            if (dt1.Rows.Count>0)
            {
                suplEmailId = dt1.Rows[0][0].ToString().Trim();
                suplName = dt1.Rows[0][1].ToString().Trim();
            }

            if (dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(userEmailId))
                    {
                        userEmailId = dt2.Rows[i][0].ToString().Trim();
                    }
                    else
                    {
                        userEmailId = userEmailId +","+ dt2.Rows[i][0].ToString().Trim();
                    }
                }
            }

            if (!string.IsNullOrEmpty(suplEmailId))
            {
                ToId = suplEmailId;
                CCId = userEmailId + "," + "accounts.trpt@impact-logistics.in";
            }
            else
            {
                ToId = userEmailId;
                CCId = "accounts.trpt@impact-logistics.in";
            }

            

            string filePaths = "";

            filePaths = Server.MapPath("~\\Downloads\\" + RefNo + ".pdf");
            string subject = suplName + " Payment Advice No. " + RefNo;
            string body = CommonTypes.GetMailCotent("V");
            body = string.Format(body, cmbBillingCompany.SelectedItem.Text);
            string FilePath = filePaths;
            string fromId = ConfigurationManager.AppSettings["FromId"].ToString().Replace("&gt;", ">").Replace("&lt;", "<"); ;
           
            MailHandler mailhandler = new MailHandler();

            //string FilePath = SavePdfLRForEmail(txtLRNo.Text.Trim().Replace("/", ""));

            //body = string.Format(body, billingCompanyName, LRNos, txtDate.Text, VehicleNo, from, To, expDeliveryDate, Consignee);

            body = "<html><head><style>body {font-family:arial;font-size:10pt;} td {font-family:arial;font-size:9pt;}</style></head><body bgcolor=#ffffff><table width=100%><tr bgcolor=#336699><td align=left style=font-weight:bold><font color=white> " +
                       " LR Tracker Tool</font></td></tr></table> <table> <tr> <td>" + body;

            body = body + "</td></tr></table><br/><br/><br/>" +
"<hr size=2 width=100% align=center><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td>NOTE: This is a system generated notification, please <font color=red>DO NOT REPLY</font> to this mail.</td></tr></table><br><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td  valign=top style={font-family:verdana;font-size:60%;color:#888888}>This" +
"e-mail, and any attachments thereto, are intended only for use by the addressee(s) named herein. If you are not the intended recipient of this e-mail, you are hereby notified that any dissemination, distribution or copying which amounts to misappropriation of this e-mail and any attachments thereto, is strictly prohibited. If you have received this e-mail in error, please immediately notify me and permanently delete the original and any copy of any e-mail and " +
"any printout thereof.</td></tr></table></body></html>";

            MailMessage msg = mailhandler.CreateMessage(ToId, CCId, subject, body, true, FilePath);
            bool result = mailhandler.SendEmail(msg, true, true, false);
        }

        protected void Filter_Command(object sender, CommandEventArgs e)
        {
            try
            {
                MsgPanel.Message = string.Empty;
                MsgPanel.DispCode = -1;
                DataTable dt = null;
                switch (e.CommandName.ToLower())
                {
                    case "filter":
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    case "clearfilter":
                        txtFromDate.Text = string.Empty;
                        txtToDate.Text = string.Empty;
                        chkDateFilter.Checked = false;
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltViewMaster.ActiveViewIndex = index;
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtAmount.Text = txtchqdt.Text = txtDate.Text = TxtChqNo.Text = String.Empty;
            GrdPaymentStatus.DataSource = null;
            GrdPaymentStatus.DataBind();
            grdPaymentSupl.DataSource = null;
            grdPaymentSupl.DataBind();
            if (cmbSupl.Items.Count > 0) cmbSupl.SelectedIndex = 0;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
            lblPaymentHold.Visible = false;
            btnSave.Visible = true;
        }

        private string GenerateXML()
        {
            string strxml = String.Empty;
            string InvoiceNo = String.Empty;
            string LRID = String.Empty;
            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                DataKey keys = grdPaymentSupl.DataKeys[i];
                TextBox txt = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                TextBox txtTDS = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                if (txt.Text.Trim() == string.Empty) txt.Text = "0";
                if (txtTDS.Text.Trim() == string.Empty) txtTDS.Text = "0";
                if (decimal.Parse(txt.Text.Trim()) > 0)
                {
                    InvoiceNo = keys["Srl"].ToString();
                    LRID = keys["LRId"].ToString();
                    strxml = strxml + "<dtPayment><InvoiceNo>" + InvoiceNo + "</InvoiceNo><LRID>" + LRID + "</LRID><AdjAmt>" + txt.Text.Trim() + "</AdjAmt><TDSAmt>" + txtTDS.Text.Trim() + "</TDSAmt><TDSper>" + keys["TDSPer"].ToString() + "</TDSper></dtPayment>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<DocumentElement>" + strxml + "</DocumentElement>";
            }
            return strxml;
        }

        protected void cmbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
            {
                trBank.Visible = true;
                trChqNo.Visible = true;
                lblChqDDNo.Visible = true;
                TxtChqNo.Visible = true;
                cmbBank.Visible = true;
                lblBank.Visible = true;
                CustomBank.Enabled = true;
                reqFVChqNo.Enabled = true;
            }
            else
            {
                trBank.Visible = false;
                trChqNo.Visible = false;
                lblChqDDNo.Visible = false;
                TxtChqNo.Visible = false;
                cmbBank.Visible = false;
                lblBank.Visible = false;
                CustomBank.Enabled = false;
                reqFVChqNo.Enabled = false;
            }
            switch (cmbPaymentMode.SelectedValue)
            {
                case "1":
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "2":
                    lblChqDDNo.Text = "Chq No.";
                    lblchqddDate.Text = "Chq.Date";
                    break;
                case "3":
                    lblChqDDNo.Text = "DD No.";
                    lblchqddDate.Text = "DD Date";
                    break;
                case "4":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "5":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "6":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                default:
                    break;
            }
            txtchqdt.Focus();
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdViewIndex_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdViewIndex_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViewIndex.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void grdViewIndex_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "PayDetails")
                {
                    PnlItemDtls.Visible = true;
                    LoadGridViewItem(0, keys["RefNo"].ToString());
                }
                if (e.CommandName.ToLower()=="report")
                {
                    string refNo = keys["RefNo"].ToString();
                    string tranDate = DateTime.Parse(keys["TranDate"].ToString()).ToString("dd/MM/yyyy");
                    string transporter = keys["SupplierName"].ToString();
                    string paymentMode = keys["PayMode"].ToString();
                    string chqNo = keys["ChqNo"].ToString();
                    string chqDate = keys["ChqDate"].ToString();

                    ReportDocument RptDoc = new ReportDocument();
                    ReportBO OptBO = new ReportBO();
                    prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher ds = new prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher();
                    OptBO.RefNo = refNo;
                    DataTable dt = OptBO.GetPaymentVoucherData();
                    string fileName = Server.MapPath("~\\Downloads\\" + refNo + ".pdf");
                    ds.Tables.RemoveAt(0);
                    ds.Tables.Add(dt);
                    RptDoc.Load(Server.MapPath("~/Reports/PaymentAdvice.rpt"));
                    //condbsLogon(RptDoc);
                    RptDoc.SetDataSource(ds);
                    RptDoc.SetParameterValue(0, tranDate);
                    RptDoc.SetParameterValue(1, transporter);
                    RptDoc.SetParameterValue(2, paymentMode);
                    RptDoc.SetParameterValue(3, chqNo);
                    RptDoc.SetParameterValue(4, chqDate);

                    RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                    //return fileName;
                    Session["FilePath"] = fileName;
                    RptDoc.Close();
                    RptDoc.Dispose();
                    GC.Collect();
                    //Response.Redirect("ViewFile.aspx", false);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SavePdfVoucher(string refNo)
        {             
            string tranDate = txtDate.Text;
            string transporter = cmbSupl.SelectedItem.Text;
            string paymentMode = cmbPaymentMode.SelectedItem.Text;
            string chqNo = TxtChqNo.Text;
            string chqDate = txtchqdt.Text;

            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher ds = new prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher();
            OptBO.RefNo = refNo;
            DataTable dt = OptBO.GetPaymentVoucherData();
            string fileName = Server.MapPath("~\\Downloads\\" + refNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/PaymentAdvice.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, tranDate);
            RptDoc.SetParameterValue(1, transporter);
            RptDoc.SetParameterValue(2, paymentMode);
            RptDoc.SetParameterValue(3, chqNo);
            RptDoc.SetParameterValue(4, chqDate);

            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            //return fileName;
            //Session["FilePath"] = fileName;
            RptDoc.Close();
            RptDoc.Dispose();
            GC.Collect();
            //Response.Redirect("ViewFile.aspx", false);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        }

        private int SendMessege(string SupplierName, string RefNo, string ContactPersonName, string Fromdate, string ToDate, string PhoneNo)
        {
            string fileName = Server.MapPath("~\\Downloads\\" + RefNo + ".pdf");
            string filePaths = "";
            string copyfilePaths = "";
            copyfilePaths = Server.MapPath("~\\Include\\Common\\Images\\WhatsappDocs\\" + RefNo + ".pdf");
            filePaths = "http://45.114.142.166/LRTrackerAuto/Include/Common/images/WhatsappDocs/" + RefNo + ".pdf";

            if (File.Exists(copyfilePaths))
                File.Delete(copyfilePaths);

            File.Copy(fileName, copyfilePaths);

            string accessKey = ConfigurationManager.AppSettings["WatiAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["WatiBaseUrl"].ToString();
            string paymentadvice1 = ConfigurationManager.AppSettings["WatiPaymentTemplate"].ToString();
            var client = new RestClient(baseAddress + "/api/v1/sendTemplateMessage?whatsappNumber=" + PhoneNo);
            //client.Authenticator = new HttpBasicAuthenticator("business@ash-logistics.com", "Valid@2023");
            System.Net.ServicePointManager.Expect100Continue = true;
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", accessKey);
            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("application/json", "{\"parameters\":[{\"name\":\"filepath\",\"value\":\"" + filePaths + "\"},{\"name\":\"name\",\"value\":\"" + ContactPersonName + "\"},{\"name\":\"companyname\",\"value\":\"" + SupplierName + "\"},{\"name\":\"fromdate\",\"value\":\"" + Fromdate + "\"},{\"name\":\"todate\",\"value\":\"" + ToDate + "\"}],\"template_name\":\"paymentadvice1\",\"broadcast_name\":\"Ash Logistics\"}", ParameterType.RequestBody);
            var response = client.Execute(request);
            if (response.Content.Contains("result\":true"))
            {
                return 1;
            }
            else
            {
                return 0;
            }
            //if (response.Content.Contains("result\":true"))
            //{
            //    MsgPopUp.ShowModal("Success!!!!", CommonTypes.ModalTypes.Alert);
            //}
            //else
            //{
            //    MsgPopUp.ShowModal("Failed!!!!", CommonTypes.ModalTypes.Error);
            //}
        }

        protected void txtAdjAmt_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int Rindex = gvRow.RowIndex;
            DataKey keys = grdPaymentSupl.DataKeys[Rindex];
            TextBox txtadj = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtAdjAmt");
            TextBox txttds = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtTDSAmt");
            decimal TDSPaidCount = decimal.Parse(keys["TDSPaidCount"].ToString());
            decimal TDS = 0;


            if (txtadj.Text.Trim() == string.Empty) txtadj.Text = "0";
            if (txttds.Text.Trim() == string.Empty) txttds.Text = "0";

            decimal adjamt = decimal.Parse(txtadj.Text);
            decimal tdsamt = 0;
            if (adjamt>0)
            {
                TDS = decimal.Parse(keys["TotalCharges"].ToString()) * decimal.Parse(grdPaymentSupl.Rows[Rindex].Cells[7].Text) / 100;
                TDS = decimal.Round(TDS);
                if (TDSPaidCount <= TDS)
                {
                    tdsamt = TDS - TDSPaidCount;
                    tdsamt = decimal.Round(tdsamt);
                    txttds.Text = tdsamt.ToString();
                }
                else
                {
                    txttds.Text = "0";
                    tdsamt = 0;
                }

                if (tdsamt <= adjamt)
                    txtadj.Text = (adjamt - tdsamt).ToString();
                else
                    txtadj.Text = "0";
            }
            else
            {
                txttds.Text = "0";
            }
            decimal totAmt = 0;
            decimal totAdjAmt = 0;
            decimal totTdsAmt = 0;
            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                adjamt = 0;
                tdsamt = 0;
                TextBox txtADJ = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                TextBox txtTDS = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");

                if (txtADJ.Text.Trim() == string.Empty) txtADJ.Text = "0";
                if (txtTDS.Text.Trim() == string.Empty) txtTDS.Text = "0";

                adjamt = decimal.Parse(txtADJ.Text);
                tdsamt = decimal.Parse(txtTDS.Text);

                totAdjAmt = totAdjAmt + adjamt ;
                totTdsAmt = totTdsAmt + tdsamt;
                totAmt = totAmt + adjamt + tdsamt;
            }
            txtTotalTDS.Text = totTdsAmt.ToString();
            txtTotalLR.Text = totAdjAmt.ToString();
            txtAmount.Text = totAmt.ToString();
            txtadj.Focus();
        }
    }
}