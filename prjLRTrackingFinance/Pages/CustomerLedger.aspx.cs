﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using CrystalDecisions;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerLedger : System.Web.UI.Page
    {
        private static void condbsLogon(ReportDocument RptDoc)
        {
            string datasource = "";
            string dbname = "LRTracking";
            string username = "";
            string password = "";

            string connSetting = ConfigurationManager.AppSettings["ConnectionString"].ToString();

            string[] tokens = connSetting.Split(";".ToCharArray());
            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i].ToLower().Contains("data source"))
                {
                    datasource = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("user id"))
                {
                    username = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("password"))
                {
                    password = tokens[i].Split("=".ToCharArray())[1];
                }
                else if (tokens[i].ToLower().Contains("initial catalog"))
                {
                    dbname = tokens[i].Split("=".ToCharArray())[1];
                }
            }
            //datasource = tokens[0].Split("=".ToCharArray())[1];
            //username = tokens[3].Split("=".ToCharArray())[1];
            //password = tokens[4].Split("=".ToCharArray())[1];
            //dbname = tokens[1].Split("=".ToCharArray())[1];

            RptDoc.SetDatabaseLogon(username, password, datasource, dbname);

        }

        private void FillComboCustomer()
        {
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillCustomerCombo();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            if (!IsPostBack)
            {
                FillComboCustomer();
                txtFromDate.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Session["CustomerID"] = cmbCustomer.SelectedValue;
                Session["CustomerName"] = cmbCustomer.SelectedItem.Text.Trim().ToUpper();
                Session["FromDate"] = txtFromDate.Text.Trim();
                Session["ToDate"] = txtToDate.Text.Trim();
                Session["ReportType"] = "CustomerLedger";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);                
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {

        }
    }
}