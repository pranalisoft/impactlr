﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class OnAccountAdjustment : System.Web.UI.Page
    {
        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private long Delete()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdViewIndex.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdViewIndex.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteReceipt();
            }
            return srl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            txtDate_CalendarExtender.EndDate = DateTime.Now;
            //if (!IsPostBack)
            //{
            //    fillBank();
            //    FillComboCustomer();
            //    cmbCust.Text = String.Empty;
            //    cmbPaymentMode.SelectedIndex = 1;
            //    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            //    cmbPaymentMode.SelectedIndex = 0;
            //    //lblOutstanding.Visible = false;
            //    txtDate.Focus();

            //}
            if (!IsPostBack)
            {
                Session.Remove("dtRcptInvDtls");
                ShowViewByIndex(0);
                chkDateFilter.Checked = false;
                txtFromDate.Text = "";
                txtToDate.Text = "";
                LoadGridView();
                PnlItemDtls.Visible = false;
                txtFromDate.Focus();

            }
            RegisterDateTextBox();
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdViewIndex, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdViewIndex, "chkSelect");
            }
        }

        private void LoadGridView()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.DatefilterYes = chkDateFilter.Checked;
            OptBO.SearchFrom = txtFromDate.Text;
            OptBO.SearchTo = txtToDate.Text;
            OptBO.PayType = "N";
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            DataTable dt = OptBO.FillReceiptHeader();
            grdViewIndex.DataSource = dt;
            grdViewIndex.DataBind();
            lblRecCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }

        private void LoadGridViewItem(long RcptId)
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.Srl = RcptId;
            DataTable dt = OptBO.FillReceiptInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
            lblItemCount.Text = dt.Rows.Count.ToString();
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal number = 0;
                if (!Decimal.TryParse(txtAmount.Text, out number))
                    return;
                if (txtAmount.Text.Trim() != String.Empty)
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }

                    decimal BalAmt = decimal.Parse(txtAmount.Text);
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        if (BalAmt > 0)
                        {
                            TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                            TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                            decimal adjamt = 0;
                            if (decimal.Parse(txtbal.Text) >= BalAmt)
                            {
                                adjamt = BalAmt;
                            }
                            else
                            {
                                adjamt = decimal.Parse(txtbal.Text);
                            }
                            txtadj.Text = adjamt.ToString();
                            BalAmt = BalAmt - adjamt;
                        }

                    }
                }
                else
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }
                }

                txtAmount.Focus();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void FillComboCustomer()
        {
            cmbCust.Items.Clear();
            cmbCust.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillCustomerCombo();
            cmbCust.DataSource = dt;
            cmbCust.DataTextField = "Name";
            cmbCust.DataValueField = "Srl";
            cmbCust.DataBind();
            cmbCust.SelectedIndex = 0;
        }

        private void fillBank()
        {
            cmbBank.Items.Clear();
            CommonBO bankbo = new CommonBO();
            DataTable dt = bankbo.FillBankDetails();
            cmbBank.DataSource = dt;
            cmbBank.DataTextField = "BankName";
            cmbBank.DataValueField = "BankName";
            cmbBank.DataBind();
        }

        protected void cmbCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CustomerReceiptBO custbo = new CustomerReceiptBO();
                custbo.CustomerID = long.Parse(cmbCust.SelectedValue);
                custbo.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
                DataTable dt = custbo.ShowReceiptGrid();
                grdPaymentSupl.DataSource = dt;
                grdPaymentSupl.DataBind();
                dt = null;
                dt = custbo.ShowReceiptStatus();

                GrdPaymentStatus.DataSource = dt;
                GrdPaymentStatus.DataBind();
                cmbPaymentMode.Focus();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdPaymentSupl_Validated(object source, ServerValidateEventArgs args)
        {

        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    if (Delete() > 0)
                    {
                        LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    }
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Receipt from Customer";
                    ShowViewByIndex(1);
                    fillBank();
                    FillComboCustomer();
                    FillComboRcpts();
                    clearAll();
                    //cmbCust.Text = String.Empty;
                    cmbPaymentMode.SelectedIndex = 1;
                    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                    cmbPaymentMode.SelectedIndex = 0;
                    cmbRcpt.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    int cntDel = 0;
                    foreach (GridViewRow row in grdViewIndex.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cntDel = cntDel + 1;
                                break;
                            }
                        }
                    }
                    if (cntDel > 0)
                    {
                        MsgPopUp.ShowModal("Are you sure.<br/>Do you want to cancel selected rcpts?", CommonTypes.ModalTypes.Confirm);
                    }
                    else
                    {
                        MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                    }
                    PnlItemDtls.Visible = false;
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    decimal totamt = 0;
                    decimal balAmt = 0;
                    string paymentxml = GenerateXML();
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                        if (txtadj.Text.Trim() != String.Empty)
                        {
                            totamt += decimal.Parse(txtadj.Text);
                            balAmt += decimal.Parse(txtbal.Text);
                        }
                    }

                    if (balAmt < decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount cannot be greater than balance amount", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    if (totamt != decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount and total of adjusted amount should be equal", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    CustomerReceiptBO custbo = new CustomerReceiptBO()
                    {
                        RcptID =long.Parse(cmbRcpt.SelectedValue),                        
                        paymentXML = paymentxml,
                        BranchID = long.Parse(Session["BranchID"].ToString())
                    };
                    long cnt = custbo.InsertOnAccountPaymentDetails();
                    if (cnt > 0)
                    {
                        MsgPanel.Message = "Record Saved successfully.";
                        MsgPanel.DispCode = 1;
                        clearAll();
                        LoadGridView();
                        ShowViewByIndex(0);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    LoadGridView();
                    ShowViewByIndex(0);
                    break;
                case CommonTypes.EntryFormCommand.None:

                    break;
                default:

                    break;
            }

        }

        private void FillComboRcpts()
        {
            try
            {
                cmbRcpt.Items.Clear();
                cmbRcpt.Items.Add(new ListItem("", "-1"));
                CustomerReceiptBO optBO = new CustomerReceiptBO();
                optBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
                DataTable dt = optBO.FillReceiptOnAccount();
                cmbRcpt.DataSource = dt;
                cmbRcpt.DataTextField = "RefNo";
                cmbRcpt.DataValueField = "Srl";
                cmbRcpt.DataBind();
                cmbRcpt.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        protected void Filter_Command(object sender, CommandEventArgs e)
        {
            try
            {
                MsgPanel.Message = string.Empty;
                MsgPanel.DispCode = -1;
                DataTable dt = null;
                switch (e.CommandName.ToLower())
                {
                    case "filter":
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    case "clearfilter":
                        txtFromDate.Text = string.Empty;
                        txtToDate.Text = string.Empty;
                        chkDateFilter.Checked = false;
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltViewMaster.ActiveViewIndex = index;
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtAmount.Text = txtchqdt.Text = txtDate.Text = TxtChqNo.Text = String.Empty;
            GrdPaymentStatus.DataSource = null;
            GrdPaymentStatus.DataBind();
            grdPaymentSupl.DataSource = null;
            grdPaymentSupl.DataBind();
            if (cmbCust.Items.Count > 0) cmbCust.SelectedIndex = 0;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
        }

        private string GenerateXML()
        {
            string strxml = String.Empty;
            string InvoiceNo = String.Empty;

            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                TextBox txt = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                TextBox txtTDS = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");

                if (txt.Text.Trim() == string.Empty) txt.Text = "0";
                if (txtTDS.Text.Trim() == string.Empty) txtTDS.Text = "0";

                DataKey key = grdPaymentSupl.DataKeys[i];
                if (decimal.Parse(txt.Text.Trim()) > 0)
                {
                    InvoiceNo = key["Srl"].ToString();
                    strxml = strxml + "<dtPayment><InvoiceNo>" + InvoiceNo + "</InvoiceNo><AdjAmt>" + txt.Text.Trim() + "</AdjAmt><TDSAmt>" + txtTDS.Text.Trim() + "</TDSAmt></dtPayment>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<DocumentElement>" + strxml + "</DocumentElement>";
            }
            return strxml;
        }

        protected void cmbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
            {
                trBank.Visible = true;
                trChqNo.Visible = true;
                lblChqDDNo.Visible = true;
                TxtChqNo.Visible = true;
                cmbBank.Visible = true;
                lblBank.Visible = true;
                CustomBank.Enabled = true;
                reqFVChqNo.Enabled = true;
            }
            else
            {
                trBank.Visible = false;
                trChqNo.Visible = false;
                lblChqDDNo.Visible = false;
                TxtChqNo.Visible = false;
                cmbBank.Visible = false;
                lblBank.Visible = false;
                CustomBank.Enabled = false;
                reqFVChqNo.Enabled = false;
            }
            switch (cmbPaymentMode.SelectedValue)
            {
                case "1":
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "2":
                    lblChqDDNo.Text = "Chq No.";
                    lblchqddDate.Text = "Chq.Date";
                    break;
                case "3":
                    lblChqDDNo.Text = "DD No.";
                    lblchqddDate.Text = "DD Date";
                    break;
                case "4":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "5":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "6":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                default:
                    break;
            }
            txtchqdt.Focus();
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdViewIndex_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdViewIndex_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViewIndex.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void grdViewIndex_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "PayDetails")
                {
                    PnlItemDtls.Visible = true;
                    lblItemDtls.Text = "Invoice Details";
                    LoadGridViewItem(long.Parse(keys["Srl"].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void cmbRcpt_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Fill Receipt Header Info
            CustomerReceiptBO optbo = new CustomerReceiptBO();
            optbo.RcptID = long.Parse(cmbRcpt.SelectedValue);
            DataTable dt = optbo.FillReceiptOnAccountInfo();
           // Srl,RefNo,TranDate,CustomerID,CustomerName,TotalAmount,PaymentMode,UserName,BranchId,ChqNo,ChqDate,PayMode
            if (dt.Rows.Count>0)
            {
                txtDate.Text = DateTime.Parse(dt.Rows[0]["TranDate"].ToString()).ToString("dd/MM/yyyy");
                cmbCust.SelectedValue = dt.Rows[0]["CustomerID"].ToString();
                cmbCust_SelectedIndexChanged(cmbCust, e);
                txtAmount.Text = decimal.Parse(dt.Rows[0]["TotalAmount"].ToString()).ToString("F2");
                cmbPaymentMode.SelectedValue = dt.Rows[0]["PaymentMode"].ToString();
                cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, e);
                TxtChqNo.Text = dt.Rows[0]["ChqNo"].ToString();
                txtchqdt.Text = DateTime.Parse(dt.Rows[0]["ChqDate"].ToString()).ToString("dd/MM/yyyy");
                cmbBank.Text = dt.Rows[0]["BankName"].ToString();

                TextBox txt = (TextBox)grdPaymentSupl.Rows[0].FindControl("txtBalAmt");
                txt.Focus();
            }
        }

        
    }
}