﻿<%@ Page Title="Customer Receivable" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="CustomerReceivable.aspx.cs" EnableEventValidation="false"
    Inherits="prjLRTrackerFinanceAuto.Pages.CustomerReceivable" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Customer Receivable
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="130px">
                                <asp:Label ID="Label18" Text="Billing Company" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="270px">
                                <asp:DropDownList ID="cmbBillingCompany" runat="server" CssClass="NormalTextBold"
                                    TabIndex="1" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="cmbBillingCompany_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td width="60px">
                                <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="2" CssClass="NormalTextBold"
                                    Enabled="false" />
                                <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                    runat="server" Format="dd/MM/yyyy" Enabled="True">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                    Width="22px" TabIndex="3" />&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="80px">
                            </td>
                            <td colspan="5">
                                <asp:Button ID="btnShow" runat="server" Text="Show" CssClass="button" OnClick="btnShow_Click"
                                    TabIndex="4" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" OnClick="btnExcel_Click"
                                    TabIndex="5" />
                                <asp:Button ID="btnExcelAll" runat="server" Text="Excel All" CssClass="button" OnClick="btnExcelAll_Click"
                                    TabIndex="6" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="false"
                                    ShowFooter="true" PageSize="5" CssClass="grid" AllowPaging="false" AllowSorting="false"
                                    DataKeyNames="CustomerId,CustomerName,UnBilledAmount,NotDueAmount,CreditDays,Slot_30Days,Slot_45Days,Slot_60Days,Slot_75Days,Slot_76Days,TotalAbove30,Total,
                                    AmountOverDue,AmountExpected,AmountExpectedOn,PaymentRcvd,PODRcvdBillPending,PODNotRcvdButDeliveredAmount,ExpDate,ExpAmount"
                                    EmptyDataText="No Records Found." OnRowDataBound="grdMain_RowDataBound">
                                    <RowStyle Height="10px" />
                                    <Columns>
                                        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName" />
                                        <asp:BoundField DataField="UnBilledAmount" HeaderText="Unbilled" SortExpression="UnBilledAmount"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60px" />
                                        <asp:BoundField DataField="NotDueAmount" HeaderText="Not Due" SortExpression="NotDueAmount"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60px" />
                                        <asp:BoundField DataField="CreditDays" HeaderText="Credit Days" SortExpression="CreditDays"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60px" />
                                        <asp:BoundField DataField="Slot_30Days" HeaderText="30 Days" SortExpression="Slot_30Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_45Days" HeaderText="31 To 45 Days" SortExpression="Slot_45Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_60Days" HeaderText="46 To 60 Days" SortExpression="Slot_60Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_75Days" HeaderText="61 To 75 Days" SortExpression="Slot_75Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_76Days" HeaderText="76 & Above" SortExpression="Slot_76Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="TotalAbove30" HeaderText="Total Above 30 Days" SortExpression="TotalAbove30"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" ItemStyle-HorizontalAlign="Right"
                                            ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="AmountOverDue" HeaderText="OverDue Amount" SortExpression="AmountOverDue"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="AmountExpected" HeaderText="Due Amount" SortExpression="AmountExpected"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="AmountExpectedOn" HeaderText="Dues Expected On" SortExpression="AmountExpectedOn"
                                            ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ExpDate" HeaderText="Exp. Date" SortExpression="ExpDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ExpAmount" HeaderText="Exp. Amount" SortExpression="ExpAmount"
                                            ItemStyle-Width="100px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="LatestRemarks" HeaderText="Latest Remarks" SortExpression="LatestRemarks"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px" />
                                        <asp:BoundField DataField="PaymentRcvd" HeaderText="Payment Rcvd Current Month" SortExpression="PaymentRcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="PODRcvdBillPending" HeaderText="POD Rcvd Bill Pending"
                                            SortExpression="PODRcvdBillPending" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="PODNotRcvdButDeliveredAmount" HeaderText="POD Not Rcvd But Delivered"
                                            SortExpression="PODNotRcvdButDeliveredAmount" ItemStyle-HorizontalAlign="Right"
                                            ItemStyle-Width="100px" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnShow" />
            <asp:AsyncPostBackTrigger ControlID="cmbBillingCompany" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
       
    </script>
</asp:Content>
