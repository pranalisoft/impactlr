﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerCreditNote : System.Web.UI.Page
    {
        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private long Delete()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdViewIndex.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdViewIndex.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteReceipt();
            }
            return srl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            txtDate_CalendarExtender.EndDate = DateTime.Now;
            //if (!IsPostBack)
            //{
            //    fillBank();
            //    FillComboCustomer();
            //    cmbCust.Text = String.Empty;
            //    cmbPaymentMode.SelectedIndex = 1;
            //    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            //    cmbPaymentMode.SelectedIndex = 0;
            //    //lblOutstanding.Visible = false;
            //    txtDate.Focus();

            //}
            if (!IsPostBack)
            {
                Session.Remove("dtRcptInvDtls");
                ShowViewByIndex(0);
                chkDateFilter.Checked = false;
                txtFromDate.Text = "";
                txtToDate.Text = "";
                LoadGridView();
                PnlItemDtls.Visible = false;
                txtFromDate.Focus();

            }
            RegisterDateTextBox();
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdViewIndex, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdViewIndex, "chkSelect");
            }
        }

        private void LoadGridView()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.DatefilterYes = chkDateFilter.Checked;
            OptBO.SearchFrom = txtFromDate.Text;
            OptBO.SearchTo = txtToDate.Text;
            OptBO.PayType = "D";
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            DataTable dt = OptBO.FillReceiptHeader();
            grdViewIndex.DataSource = dt;
            grdViewIndex.DataBind();
            lblRecCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }

        private void LoadGridViewItem(long RcptId)
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.Srl = RcptId;
            DataTable dt = OptBO.FillReceiptInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
            lblItemCount.Text = dt.Rows.Count.ToString();
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal number = 0;
                if (!Decimal.TryParse(txtAmount.Text, out number))
                    return;
                if (txtAmount.Text.Trim() != String.Empty)
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }

                    decimal BalAmt = decimal.Parse(txtAmount.Text);
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        if (BalAmt > 0)
                        {
                            TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                            TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                            decimal adjamt = 0;
                            if (decimal.Parse(txtbal.Text) >= BalAmt)
                            {
                                adjamt = BalAmt;
                            }
                            else
                            {
                                adjamt = decimal.Parse(txtbal.Text);
                            }
                            txtadj.Text = adjamt.ToString();
                            BalAmt = BalAmt - adjamt;
                        }
                        else
                            break;

                    }
                }
                else
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }

                }

                txtAmount.Focus();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void FillComboCustomer()
        {
            cmbCust.Items.Clear();
            cmbCust.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillCustomerCombo();
            cmbCust.DataSource = dt;
            cmbCust.DataTextField = "Name";
            cmbCust.DataValueField = "Srl";
            cmbCust.DataBind();
            cmbCust.SelectedIndex = 0;
        }        

        protected void cmbCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CustomerReceiptBO custbo = new CustomerReceiptBO();
                custbo.CustomerID = long.Parse(cmbCust.SelectedValue);
                custbo.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
                DataTable dt = custbo.ShowReceiptGrid();
                grdPaymentSupl.DataSource = dt;
                grdPaymentSupl.DataBind();
                dt = null;
                dt = custbo.ShowReceiptStatus();

                GrdPaymentStatus.DataSource = dt;
                GrdPaymentStatus.DataBind();
                txtNarration.Focus();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdPaymentSupl_Validated(object source, ServerValidateEventArgs args)
        {

        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    if (Delete() > 0)
                    {
                        LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    }
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:                    
                    ShowViewByIndex(1);                   
                    FillComboCustomer();
                    clearAll();                    
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    int cntDel = 0;
                    foreach (GridViewRow row in grdViewIndex.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cntDel = cntDel + 1;
                                break;
                            }
                        }
                    }
                    if (cntDel > 0)
                    {
                        MsgPopUp.ShowModal("Are you sure.<br/>Do you want to delete selected credit note(s)?", CommonTypes.ModalTypes.Confirm);
                    }
                    else
                    {
                        MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                    }
                    PnlItemDtls.Visible = false;
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    decimal totamt = 0;
                    decimal balAmt = 0;
                    string paymentxml = GenerateXML();
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                        if (txtadj.Text.Trim() != String.Empty)
                        {
                            totamt += decimal.Parse(txtadj.Text);
                            balAmt += decimal.Parse(txtbal.Text);
                        }
                    }

                    if (balAmt < decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount cannot be greater than balance amount", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    if (totamt != decimal.Parse(txtAmount.Text))
                    {
                        MsgPopUp.ShowModal("Entered amount and total of adjusted amount should be equal", CommonTypes.ModalTypes.Error);
                        return;
                    }

                    CustomerReceiptBO custbo = new CustomerReceiptBO()
                    {
                        tranDate = txtDate.Text.Trim(),
                        CustomerID = long.Parse(cmbCust.SelectedValue),
                        refNo = String.Empty,
                        remarks = String.Empty,
                        totalAmount = decimal.Parse(txtAmount.Text),
                        userID = long.Parse(Session["EID"].ToString()),
                        paymentXML = paymentxml,
                        BankName = String.Empty,
                        PaymentMode = "1",
                        ChqNo = String.Empty,
                        ChqDate = txtDate.Text.Trim(),
                        CardNetBankingDetails = String.Empty,
                        PayType = "D",
                        BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString())
                    };
                    long cnt = custbo.InsertPaymentDetails();
                    if (cnt > 0)
                    {
                        MsgPanel.Message = "Record Saved successfully.";
                        MsgPanel.DispCode = 1;
                        clearAll();
                        LoadGridView();
                        ShowViewByIndex(0);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    LoadGridView();
                    ShowViewByIndex(0);
                    break;
                case CommonTypes.EntryFormCommand.None:
                    break;
                default:
                    break;
            }
        }

        protected void Filter_Command(object sender, CommandEventArgs e)
        {
            try
            {
                MsgPanel.Message = string.Empty;
                MsgPanel.DispCode = -1;
                DataTable dt = null;
                switch (e.CommandName.ToLower())
                {
                    case "filter":
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    case "clearfilter":
                        txtFromDate.Text = string.Empty;
                        txtToDate.Text = string.Empty;
                        chkDateFilter.Checked = false;
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltViewMaster.ActiveViewIndex = index;
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtAmount.Text =txtNarration.Text= txtDate.Text = String.Empty;
            GrdPaymentStatus.DataSource = null;
            GrdPaymentStatus.DataBind();
            grdPaymentSupl.DataSource = null;
            grdPaymentSupl.DataBind();
            if (cmbCust.Items.Count > 0) cmbCust.SelectedIndex = 0;           
        }

        private string GenerateXML()
        {
            string strxml = String.Empty;
            string InvoiceNo = String.Empty;

            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                TextBox txt = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                if (txt.Text.Trim() == string.Empty) txt.Text = "0";

                DataKey key = grdPaymentSupl.DataKeys[i];
                if (decimal.Parse(txt.Text.Trim()) >0)
                {
                    InvoiceNo = key["InvoiceNo"].ToString();
                    strxml = strxml + "<dtPayment><InvoiceNo>" + InvoiceNo + "</InvoiceNo><AdjAmt>" + txt.Text.Trim() + "</AdjAmt><TDSAmt>0</TDSAmt></dtPayment>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<DocumentElement>" + strxml + "</DocumentElement>";
            }
            return strxml;
        }        

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdViewIndex_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdViewIndex_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViewIndex.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void grdViewIndex_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "PayDetails")
                {
                    PnlItemDtls.Visible = true;
                    lblItemDtls.Text = "Invoice Details";
                    LoadGridViewItem(long.Parse(keys["Srl"].ToString()));
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}