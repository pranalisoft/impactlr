﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class VehicleTypeWisePlyQty : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillDropDown()
        {
            PlyItemBO Optbo = new PlyItemBO();
            cmbPlyItem.DataSource = null;
            cmbPlyItem.DataBind();
            cmbPlyItem.Items.Clear();
            cmbPlyItem.Items.Add(new ListItem("", "-1"));

            Optbo.SearchText = "";
            DataTable dt = Optbo.GetPlyItemdetails();
            cmbPlyItem.DataSource = dt;
            cmbPlyItem.DataTextField = "Name";
            cmbPlyItem.DataValueField = "srl";
            cmbPlyItem.DataBind();
            cmbPlyItem.SelectedIndex = 0;
        }

        private void FillGrid()
        {
            PlyItemBO OptBo = new PlyItemBO();
            OptBo.Srl = long.Parse(cmbPlyItem.SelectedValue);
            grdVehicleType.DataSource = OptBo.GetVehicleTypeWiseQty();
            grdVehicleType.DataBind();
        }

        protected void cmbPlyItem_SelectedIndexChanged1(object sender, EventArgs e)
        {
            SetPanelMsg("", false, 0);
            FillGrid();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDropDown();
                cmbPlyItem.Focus();
            }
        }

        protected void grdVehicleType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chk1 = (CheckBox)e.Row.FindControl("chkSelect");
                if (long.Parse(grdVehicleType.DataKeys[e.Row.RowIndex].Values["Sel"].ToString()) > 0)
                    chk1.Checked = true;
                else
                    chk1.Checked = false;
            }
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "yes")
                {
                    //Response.Redirect("JobWorkMain.aspx", false);
                }
            }
            catch (Exception exp)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelMsg("", false, 0);
                long ans = 0;
                PlyItemBO OptBo = new PlyItemBO();
                string strmenuxml = "";
                if (cmbPlyItem.SelectedIndex > 0)
                {
                    strmenuxml = "<DocumentElement><VehicleTypes>";
                    for (int i = 0; i < grdVehicleType.Rows.Count; i++)
                    {
                        CheckBox chkBx = (CheckBox)grdVehicleType.Rows[i].FindControl("chkSelect");
                        TextBox txt = (TextBox)grdVehicleType.Rows[i].FindControl("txtQty");
                        if (txt.Text.Trim() == "") txt.Text = "0";
                        if (chkBx.Checked && long.Parse(txt.Text) > 0)
                        {
                            strmenuxml = strmenuxml + "<VehicleType><VehicleTypeId>" + grdVehicleType.DataKeys[i].Values["Srl"].ToString() + "</VehicleTypeId>";
                            strmenuxml = strmenuxml + "<Qty>" + txt.Text + "</Qty></VehicleType>";
                        }
                    }
                    strmenuxml = strmenuxml + "</VehicleTypes></DocumentElement>";
                }
                OptBo.xml = strmenuxml;
                OptBo.Srl = long.Parse(cmbPlyItem.SelectedValue);
                ans = OptBo.InsertPlyQty();
                if (ans > 0)
                {
                    SetPanelMsg("Record Saved Successfully", true, 1);
                    cmbPlyItem.SelectedIndex = 0;
                    cmbPlyItem_SelectedIndexChanged1(cmbPlyItem, EventArgs.Empty);
                }
                else
                {
                    SetPanelMsg("Operation Failed. Can Not Insert Record.", true, 0);
                }

            }
            catch (Exception exp)
            {

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelMsg("", false, 0);
                cmbPlyItem.SelectedIndex = 0;
                Response.Redirect("LRMain.aspx", false);
            }
            catch (Exception exp)
            {

            }
        }
    }
}