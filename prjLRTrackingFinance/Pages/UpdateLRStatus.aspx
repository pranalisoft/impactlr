﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="UpdateLRStatus.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateLRStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                LR Status Updation
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                    <div>
                        <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                            <asp:Label ID="lblMsg" Text="" runat="server" />
                        </asp:Panel>
                    </div>
                    <div style="padding-left: 15px;">
                        <div>
                            <table style="border: 1px solid blue" cellspacing="10px">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl001" Text="LR No." runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl002" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLRNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label3" Text="Date" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label131" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDate" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label5" Text="Consigner" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td style="width: 10px">
                                        <asp:Label ID="Label6" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConsigner" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td style="width: 150px">
                                        <asp:Label ID="Label01" Text="Consignee" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td style="width: 10px">
                                        <asp:Label ID="Label141" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConsignee" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" Text="From" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label8" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFrom" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label16" Text="To" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label18" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" Text="Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label10" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label11" Text="Chargeable Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label121" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblChargeableWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label15" Text="Vehicle No" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label17" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblVehicleNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label19" Text="Total Packages" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label20" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPackages" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label132" Text="Invoice No" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label142" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label21" Text="Invoice Value" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label22" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceValue" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label23" Text="Type" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label24" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblType" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label26" Text="Remarks" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label27" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRemarks" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" Text="Driver's Name & No." runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label13" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td colspan="4">
                                        <asp:Label ID="lblDriverDetails" Text="" runat="server" CssClass="NormalTextBold" /> &nbsp;&nbsp;
                                        <asp:Label ID="lblDriverMobNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                        <asp:HiddenField ID="txtHiddendetailId" runat="server" />
                                         <asp:HiddenField ID="txtHiddenTripId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="Trip Id" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTripId" Enabled="false"></asp:TextBox>
                                        &nbsp;<asp:Button ID="btnUpdateTripId" runat="server" Text="Update" CssClass="button"
                                              onclick="btnUpdateTripId_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Status" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbStatus" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="120" TabIndex="1" CssClass="WindowsStyle" DropDownStyle="DropDown"
                                            RenderMode="Block">
                                            <asp:ListItem Value="L">Vehicle Left</asp:ListItem>
                                            <asp:ListItem Value="T">In-Transit</asp:ListItem>
                                            <asp:ListItem Value="R">Reported</asp:ListItem>                                            
                                            <asp:ListItem Value="U">Waiting For Unload</asp:ListItem>
                                            <asp:ListItem Value="D">Delivered</asp:ListItem>
                                        </asp:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Tran Date" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDate" runat="server" Width="80px" TabIndex="2" CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                            ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                            PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Time"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlHH" runat="server" Width="50px" TabIndex="4" CssClass="NormalTextBold">
                                            <asp:ListItem>00</asp:ListItem>
                                            <asp:ListItem>01</asp:ListItem>
                                            <asp:ListItem>02</asp:ListItem>
                                            <asp:ListItem>03</asp:ListItem>
                                            <asp:ListItem>04</asp:ListItem>
                                            <asp:ListItem>05</asp:ListItem>
                                            <asp:ListItem>06</asp:ListItem>
                                            <asp:ListItem>07</asp:ListItem>
                                            <asp:ListItem>08</asp:ListItem>
                                            <asp:ListItem>09</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>11</asp:ListItem>
                                            <asp:ListItem>12</asp:ListItem>
                                            <asp:ListItem>13</asp:ListItem>
                                            <asp:ListItem>14</asp:ListItem>
                                            <asp:ListItem>15</asp:ListItem>
                                            <asp:ListItem>16</asp:ListItem>
                                            <asp:ListItem>17</asp:ListItem>
                                            <asp:ListItem>18</asp:ListItem>
                                            <asp:ListItem>19</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>21</asp:ListItem>
                                            <asp:ListItem>22</asp:ListItem>
                                            <asp:ListItem>23</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblSeperator" runat="server" Text=":" BorderStyle="None" BorderWidth="1px"
                                            Font-Bold="True" Width="29px" Style="text-align: center"></asp:Label>
                                        <asp:DropDownList ID="ddlmm" runat="server" Width="50px" CssClass="NormalTextBold"
                                            TabIndex="5">
                                            <asp:ListItem>00</asp:ListItem>
                                            <asp:ListItem>01</asp:ListItem>
                                            <asp:ListItem>02</asp:ListItem>
                                            <asp:ListItem>03</asp:ListItem>
                                            <asp:ListItem>04</asp:ListItem>
                                            <asp:ListItem>05</asp:ListItem>
                                            <asp:ListItem>06</asp:ListItem>
                                            <asp:ListItem>07</asp:ListItem>
                                            <asp:ListItem>08</asp:ListItem>
                                            <asp:ListItem>09</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>11</asp:ListItem>
                                            <asp:ListItem>12</asp:ListItem>
                                            <asp:ListItem>13</asp:ListItem>
                                            <asp:ListItem>14</asp:ListItem>
                                            <asp:ListItem>15</asp:ListItem>
                                            <asp:ListItem>16</asp:ListItem>
                                            <asp:ListItem>17</asp:ListItem>
                                            <asp:ListItem>18</asp:ListItem>
                                            <asp:ListItem>19</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>21</asp:ListItem>
                                            <asp:ListItem>22</asp:ListItem>
                                            <asp:ListItem>23</asp:ListItem>
                                            <asp:ListItem>24</asp:ListItem>
                                            <asp:ListItem>25</asp:ListItem>
                                            <asp:ListItem>26</asp:ListItem>
                                            <asp:ListItem>27</asp:ListItem>
                                            <asp:ListItem>28</asp:ListItem>
                                            <asp:ListItem>29</asp:ListItem>
                                            <asp:ListItem>30</asp:ListItem>
                                            <asp:ListItem>31</asp:ListItem>
                                            <asp:ListItem>32</asp:ListItem>
                                            <asp:ListItem>33</asp:ListItem>
                                            <asp:ListItem>34</asp:ListItem>
                                            <asp:ListItem>35</asp:ListItem>
                                            <asp:ListItem>36</asp:ListItem>
                                            <asp:ListItem>37</asp:ListItem>
                                            <asp:ListItem>38</asp:ListItem>
                                            <asp:ListItem>39</asp:ListItem>
                                            <asp:ListItem>40</asp:ListItem>
                                            <asp:ListItem>41</asp:ListItem>
                                            <asp:ListItem>42</asp:ListItem>
                                            <asp:ListItem>43</asp:ListItem>
                                            <asp:ListItem>44</asp:ListItem>
                                            <asp:ListItem>45</asp:ListItem>
                                            <asp:ListItem>46</asp:ListItem>
                                            <asp:ListItem>47</asp:ListItem>
                                            <asp:ListItem>48</asp:ListItem>
                                            <asp:ListItem>49</asp:ListItem>
                                            <asp:ListItem>50</asp:ListItem>
                                            <asp:ListItem>51</asp:ListItem>
                                            <asp:ListItem>52</asp:ListItem>
                                            <asp:ListItem>53</asp:ListItem>
                                            <asp:ListItem>54</asp:ListItem>
                                            <asp:ListItem>55</asp:ListItem>
                                            <asp:ListItem>56</asp:ListItem>
                                            <asp:ListItem>57</asp:ListItem>
                                            <asp:ListItem>58</asp:ListItem>
                                            <asp:ListItem>59</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Remarks" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="6" ID="txtRemarks" Width="350" TextMode="MultiLine" runat="server"
                                            MaxLength="100" CssClass="NormalTextBold"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvpartDesc" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtRemarks" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="7" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="8" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="font-size: 12px; font-weight: bold; height: 15px">
                                        Records :
                                        <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="DetailID, TransitStatus,TranDateTime,TransitRemarks" EmptyDataText="No records found."
                                            Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True" 
                                            onrowcommand="grdViewItemDetls_RowCommand">
                                            <Columns>
                                             <asp:ButtonField ButtonType="Link" CommandName="Modify"  DataTextField="TransitStatus" HeaderText="Status"
                                                        SortExpression="TransitStatus">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField>                                            
                                                <asp:BoundField DataField="TranDateTime" HeaderText="Date" SortExpression="TranDateTime"
                                                    ItemStyle-Width="100px" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                    <ItemStyle Width="100px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TransitRemarks" HeaderText="Remarks" SortExpression="TransitRemarks">
                                                    <ItemStyle Width="250px" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
             <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
