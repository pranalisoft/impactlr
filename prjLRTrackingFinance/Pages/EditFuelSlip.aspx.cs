﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;

namespace prjLRTrackerFinanceAuto
{
    public partial class EditFuelSlip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrptMgr = ScriptManager.GetCurrent(this.Page);
            scrptMgr.RegisterPostBackControl(btnSave);
            if (!IsPostBack)
            {
                FillDropDownPetrolPump();
            }
        }

        private void FillDropDownPetrolPump()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbPetrolPump.DataSource = null;
            cmbPetrolPump.Items.Clear();
            cmbPetrolPump.Items.Add(new ListItem("", "-1"));
            OptBO.SupplierType = 2;
            dt = OptBO.GetSuppliersByType();
            cmbPetrolPump.DataSource = dt;
            cmbPetrolPump.DataTextField = "Name";
            cmbPetrolPump.DataValueField = "Srl";
            cmbPetrolPump.DataBind();
            cmbPetrolPump.SelectedIndex = 0;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            long LRId = long.Parse(hdfld.Value);
            lblError.Visible = false;
            if (txtCashAdvanceFuel.Text == "") txtCashAdvanceFuel.Text = "0";
            if (txtFuelAdvanceFuel.Text == "") txtFuelAdvanceFuel.Text = "0";
            if (txtTotFuelAdvance.Text == "") txtTotFuelAdvance.Text = "0";
            if (txtFuelAdvanceTDA.Text == "") txtFuelAdvanceTDA.Text = "0";

            if (decimal.Parse(txtTotFuelAdvance.Text) > decimal.Parse(txtFuelAdvanceTDA.Text))
            {
                lblError.Visible = true;
                lblError.Text = "Total Fuel Amount cannot exceed entered value in TDA";
                txtTotFuelAdvance.Focus();
                return;
            }

            if (decimal.Parse(txtTotFuelAdvance.Text) != decimal.Parse(txtCashAdvanceFuel.Text) + decimal.Parse(txtFuelAdvanceFuel.Text))
            {
                lblError.Visible = true;
                lblError.Text = "Total Fuel Amount doesnt match with sum of cash and fuel amount";
                txtTotFuelAdvance.Focus();
                return;
            }

            LRBO optbo = new LRBO();
            optbo.FuelSlipId = long.Parse(txtHdFuelSlipId.Value);
            optbo.ID = LRId;
            optbo.CashAdvance = decimal.Parse(txtCashAdvanceFuel.Text);
            optbo.FuelAdvance = decimal.Parse(txtFuelAdvanceFuel.Text);
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            optbo.TdFuelAdvance = decimal.Parse(txtFuelAdvanceTDA.Text);
            optbo.PetrolPumpId = int.Parse(cmbPetrolPump.SelectedValue);
            long cnt = optbo.InsertFuelSlip();
            if (cnt > 0)
            {
                //SetPanelMsg("Fuel Slip Generated successfully with Srl "+ cnt.ToString(), true, 1);
                SavePdfLRForDownLoad(LRId.ToString());
                txtLRNo.Text = "";
                InitializeContent(string.Empty);

            }
        }

        private void SavePdfLRForDownLoad(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(LRNo);
            DataTable dt = OptBO.LR_Document();
            string fileName = Server.MapPath("~\\Downloads\\FS_" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/FuelSlip.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, "N");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            //return fileName;
            Session["FilePath"] = fileName;
            RptDoc.Close();
            RptDoc.Dispose();
            GC.Collect();
            //Response.Redirect("ViewFile.aspx", false);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        }

        private bool IsAdvanceExceeding()
        {
            lblError.Visible = false;

            decimal roundedAmt = 0;
            if (txtHiddenAdvanceper.Value == string.Empty) txtHiddenAdvanceper.Value = "0";
            decimal advancePer = decimal.Parse(txtHiddenAdvanceper.Value);
            decimal advancePaid = txtTotFuelAdvance.Text.Trim() == "" ? 0 : decimal.Parse(txtTotFuelAdvance.Text);
            decimal freight = txtHiddenSupplierAmount.Value == "" ? 0 : decimal.Parse(txtHiddenSupplierAmount.Value);
            decimal advanceLimit = advancePer * freight / 100;
            advanceLimit = Math.Round(advanceLimit, 0);
            roundedAmt = Math.Round(advanceLimit / 500);
            roundedAmt = Math.Round(roundedAmt, 0);
            roundedAmt = roundedAmt * 500;

            if (advancePaid > roundedAmt)
            {
                lblError.Visible = true;
                lblError.Text = "Advance cannot exceed the Limit " + roundedAmt.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void InitializeContent(string LRNo)
        {
            lblError.Visible = false;
            lblDate.Text = "-";
            lblConsigner.Text = "-";
            lblConsignee.Text = "-";
            lblFrom.Text = "-";
            lblTo.Text = "-";
            lblVehicleNo.Text = "-";
            lblPackages.Text = "-";
            lblWeight.Text = "-";
            lblChargeableWeight.Text = "-";
            lblInvoiceNo.Text = "-";
            lblInvoiceValue.Text = "-";
            lblType.Text = "-";
            lblRemarks.Text = "-";
            lblDriverDetails.Text = "-";
            txtCashAdvanceFuel.Text = "";
            txtFuelAdvanceFuel.Text = "";
            txtTotFuelAdvance.Text = "";
            txtHdFuelSlipId.Value = "0";
            LRBO pol = new LRBO();
            pol.LRNo = LRNo;
            if (LRNo != string.Empty)
            {
                DataTable dt = pol.GetLRHeaderByNo();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        hdfld.Value = dt.Rows[0]["ID"].ToString();
                        lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                        lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                        lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                        lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                        lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                        lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                        lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                        lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                        lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                        lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                        lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                        lblType.Text = dt.Rows[0]["LoadType"].ToString();
                        lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                        lblDriverDetails.Text = dt.Rows[0]["DriverNumber"].ToString();
                        txtHiddenAdvanceper.Value = dt.Rows[0]["AdvancePer"].ToString();
                        txtHiddenSupplierAmount.Value = dt.Rows[0]["SupplierAmount"].ToString();
                        txtTotFuelAdvance.Text = dt.Rows[0]["FuelAdvance"].ToString();
                        txtFuelAdvanceTDA.Text = dt.Rows[0]["FuelAdvance"].ToString();

                        if (decimal.Parse(dt.Rows[0]["SupplierAmount"].ToString()) > 0)
                        {

                            if (dt.Rows[0]["FuelSlipId"].ToString() != "")
                            {
                                lblError.Visible = true;
                                txtHdFuelSlipId.Value = dt.Rows[0]["FuelSlipId"].ToString();
                                lnkDownload.Visible = true;
                                txtCashAdvanceFuel.Text = dt.Rows[0]["FuelAdvanceCash"].ToString();
                                txtFuelAdvanceFuel.Text = dt.Rows[0]["FuelAdvanceFuel"].ToString();
                                if (dt.Rows[0]["PetrolPumpId"].ToString() != "")
                                {
                                    if (int.Parse(dt.Rows[0]["PetrolPumpId"].ToString()) > 0)
                                    {
                                        cmbPetrolPump.SelectedValue = dt.Rows[0]["PetrolPumpId"].ToString();
                                    }
                                }
                                txtTotFuelAdvance.Text = (decimal.Parse(txtCashAdvanceFuel.Text) + decimal.Parse(txtFuelAdvanceFuel.Text)).ToString();

                                if (decimal.Parse(dt.Rows[0]["AdvanceToSupplier"].ToString()) > 0)
                                {
                                    lblError.Text = "Advance is already paid for this Fuel Slip. Hence cannot modify Fuel Slip Details";
                                    cmbPetrolPump.Enabled = txtCashAdvanceFuel.Enabled = txtFuelAdvanceFuel.Enabled = txtTotFuelAdvance.Enabled = false;
                                    btnSave.Visible = false;
                                }
                                else
                                {
                                    lblError.Text = "";
                                    cmbPetrolPump.Enabled = txtCashAdvanceFuel.Enabled = txtFuelAdvanceFuel.Enabled = txtTotFuelAdvance.Enabled = true;
                                    btnSave.Visible = true;
                                }

                            }
                            else
                            {
                                lblError.Text = "No Fuel Slip is generated for this LR No.";
                                lnkDownload.Visible = false;
                                lblError.Visible = false;
                                lblError.Text = "";
                                cmbPetrolPump.Enabled = txtCashAdvanceFuel.Enabled = txtFuelAdvanceFuel.Enabled = txtTotFuelAdvance.Enabled = false;
                                btnSave.Visible = false;
                            }
                        }
                        else
                        {
                            lnkDownload.Visible = false;
                            lblError.Visible = true;
                            lblError.Text = "There is no Freight for this LR. Hence cannot generate fuel slip";
                            btnSave.Visible = false;
                        }
                    }
                    else
                    {
                        lnkDownload.Visible = false;
                        lblError.Text = "Invalid LR No.";
                        lblError.Visible = true;
                    }
                }
                else
                {
                    lnkDownload.Visible = false;
                    lblError.Visible = true;
                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            InitializeContent(txtLRNo.Text.Trim());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            long LRId = long.Parse(hdfld.Value);
            SavePdfLRForDownLoad(LRId.ToString());
        }
    }
}