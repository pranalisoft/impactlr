﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="FrieghtTigerReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.FrieghtTigerReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Freight Tiger Tracking MIS
                        </td>
                    </tr>
                </table>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td width="80px">
                            </td>
                            <td colspan="5">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="3" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="4"
                                    OnClick="btnExcel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width: 100%">
                        <tr>
                        <td>
                              &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px; width: 470px;vertical-align:top">
                                <asp:GridView ID="grdMain" runat="server" Width="470px" AutoGenerateColumns="false"
                                    CssClass="grid" AllowPaging="false" AllowSorting="true" DataKeyNames="Particulars,CntVal,ColName"
                                    EmptyDataText="No Records Found." OnRowDataBound="grdMain_RowDataBound" OnRowCommand="grdMain_RowCommand">
                                    <RowStyle Height="10px" />
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Particulars" DataField="Particulars" />
                                       <asp:BoundField HeaderText="Value" DataField="CntVal" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:ButtonField ButtonType="Link" CommandName="CntShow" Text="View" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Width="50px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="CntClick" Text="Download" ControlStyle-Font-Underline="false"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td style="vertical-align:top">
                                <div class="grid_region" style="overflow: auto;height:500px">
                                    <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="True"
                                        AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10">
                                        <%--<Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                        ItemStyle-Width="200px" />
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" />
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="50px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                </Columns>--%>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtFromDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtFromDate.ClientID %>').val('');
                }
            });

            $('#<%= txtToDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtToDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });
        
    </script>
</asp:Content>
