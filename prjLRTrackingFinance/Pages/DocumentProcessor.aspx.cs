﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DocumentProcessor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.ContentType = "application/vnd.ms-excel";
            //string filename = Server.MapPath("~/Docs/" + Session["filepath"].ToString());
            //byte[] bytes = System.IO.File.ReadAllBytes(filename);

            //Response.BinaryWrite(bytes);
            hdFilePath.Value = Session["FilePath"].ToString();
            string filepath = hdFilePath.Value;
            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);
            // Checking if file exists
            //ImpersonateUser impersonateuser = new ImpersonateUser();
            //if (impersonateuser.ImpersonateValidUser())
            //{
            if (file.Exists)
            {
                //string path = Server.MapPath(filepath);
                WebClient client = new WebClient();
                Byte[] buffer = client.DownloadData(filepath);
                if (buffer != null)
                {
                    Response.ContentType = "application/jpg";
                    Response.AddHeader("content-length", buffer.Length.ToString());
                    Response.BinaryWrite(buffer);
                }
            }
        }
    }
}