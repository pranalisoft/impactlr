﻿<%@ Page Title="Pending POD Report" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="PendingPODReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PendingPODReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Pending POD Report
                        </td>
                    </tr>
                </table>
                <div style="border: solid 1px black;">
                    <table width="100%">                       
                        <tr>
                            <td width="80px">
                            </td>
                            <td colspan="5">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="3" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="1"
                                    OnClick="btnExcel_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="3">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="true"
                                    CssClass="grid" AllowPaging="True" AllowSorting="true" DataKeyNames=""
                                    EmptyDataText="No Records Found." OnPageIndexChanging="grdMain_PageIndexChanging"
                                    OnSorting="grdMain_Sorting" OnRowDataBound="grdMain_RowDataBound">
                                    <RowStyle Height="10px" />                                    
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

       
        
    </script>

</asp:Content>