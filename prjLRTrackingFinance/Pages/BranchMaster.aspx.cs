﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
	public partial class BranchMaster : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetPanelMsg("", false, -1);
			if (!IsPostBack)	
			{
				FillGrid();
                FillDropDownBillingCompany();
                FillDropDownLOBGroup();
                clearAll();
			}
		}

        private void FillDropDownBillingCompany()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 1;
        }

        private void FillDropDownLOBGroup()
        {
            cmbLOBGroup.DataSource = null;
            cmbLOBGroup.Items.Clear();
            cmbLOBGroup.Items.Add(new ListItem("", "-1"));
            LOBGroupBO billingBO = new LOBGroupBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetLOBGroups();
            cmbLOBGroup.DataSource = dt;
            cmbLOBGroup.DataTextField = "Name";
            cmbLOBGroup.DataValueField = "Srl";
            cmbLOBGroup.DataBind();
            cmbLOBGroup.SelectedIndex = 0;
        }

        private void clearAll()
        {
            txtHiddenId.Value = "0";
            txtBranchName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtAddress3.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLandline.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtCSTNo.Text = string.Empty;
            txtVAT.Text = string.Empty;
            cmbBillingCompany.SelectedIndex = 0;
            chkActive.Checked = false;
            chkPly.Checked = false;
            txtBranchName.Focus();
        }

		protected void btnInfoOk_Click(object sender, EventArgs e)
		{
			//Response.Redirect("~/pages/SiteReturnMain.aspx", false);
		}

		protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				if (e.CommandName.Equals("Page"))
					return;
				if (e.CommandName.Equals("Sort"))
					return;
				int index = Convert.ToInt32(e.CommandArgument);
				GridView grd = (GridView)e.CommandSource;
				DataKey keys = grd.DataKeys[index];
				GridViewRow row1 = grd.Rows[index];

				if (e.CommandName == "Modify")
				{
                    FillDropDownBillingCompany();
                    clearAll();                    
					txtHiddenId.Value = keys["Srl"].ToString(); 
					txtBranchName.Text = keys["Name"].ToString();
                    ddlUOM.SelectedValue = keys["UOM"].ToString();
					chkActive.Checked = bool.Parse(keys["Active"].ToString());
                    chkPly.Checked = bool.Parse(keys["PlyApplicable"].ToString());
                    cmbBillingCompany.SelectedValue = keys["BillingCompanySrl"].ToString();
                    txtAddress1.Text = keys["AddressLine1"].ToString();
                    txtAddress2.Text = keys["AddressLine2"].ToString();
                    txtAddress3.Text = keys["AddressLine3"].ToString();
                    txtLandline.Text = keys["Landline"].ToString();
                    txtMobile.Text = keys["Mobile"].ToString();
                    txtEmailID.Text = keys["EmailId"].ToString();
                    txtCSTNo.Text = keys["GSTNo"].ToString();
                    txtVAT.Text = keys["PAN"].ToString();
                    chkPendingPOD.Checked = bool.Parse(keys["PendingPODShow"].ToString());
                    cmbLOBGroup.SelectedValue = keys["LOBGroupId"].ToString(); 
					txtBranchName.Focus();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}		

		protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			grdMst.PageIndex = e.NewPageIndex;
			FillGrid();
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			FillGrid();
			txtSearch.Focus();
		}

		protected void btnClearSearch_Click(object sender, EventArgs e)
		{
			txtSearch.Text = string.Empty;
			FillGrid();
			txtSearch.Focus();
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{
			try
			{  
				BranchBO optbo = new BranchBO();
				optbo.BranchName = txtBranchName.Text.Trim();
				optbo.Srl = long.Parse(txtHiddenId.Value);
				optbo.ShortName = "-"; 
                optbo.UOM = ddlUOM.SelectedValue;
                optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                optbo.Active = chkActive.Checked;
                optbo.IsPlyApplicable = chkPly.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                optbo.Address1 = txtAddress1.Text.Trim();
                optbo.Address2 = txtAddress2.Text.Trim();
                optbo.Address3 = txtAddress3.Text.Trim();
                optbo.landline = txtLandline.Text.Trim();
                optbo.mobile = txtMobile.Text.Trim();
                optbo.emailID = txtEmailID.Text.Trim();
                optbo.GSTNo = txtCSTNo.Text.Trim();
                optbo.PAN = txtVAT.Text.Trim();
                optbo.LOBGroupId = long.Parse(cmbLOBGroup.SelectedValue);
                optbo.PendingPODShow = chkPendingPOD.Checked;
				if (optbo.isBranchAlreadyExist() == false)
				{
					long flgCnt = 0;
					flgCnt = optbo.InsertBranch();

					if (long.Parse(txtHiddenId.Value) == 0)
					{						
						SetPanelMsg("Line Of Business added successfully.", true, 1);
					}
					else
					{						
						SetPanelMsg("Line Of Business updated successfully.", true, 1);
					}
                    clearAll();
					txtSearch.Text = string.Empty;
					FillGrid();
					txtBranchName.Focus();
				}
				else
				{
					SetPanelMsg("Branch Name Already Exist.", true, 0);
				}
			}
			catch (Exception exp)
			{			  
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			try
			{
				Response.Redirect("~/pages/LRMain.aspx", false);
			}
			catch (Exception exp)
			{
				
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			try
			{
                clearAll();
				txtBranchName.Focus();
			}
			catch (Exception exp)
			{                
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}

		private void SetPanelMsg(string msg, bool visible, int code)
		{
			if (visible)
			{
				MsgPanel.Message = msg;
				MsgPanel.DispCode = code;
			}
			else
			{
				MsgPanel.Message = "";
				MsgPanel.DispCode = -1;
			}
		}


		private void FillGrid()
		{
			try
			{
				BranchBO optbo = new BranchBO();
				optbo.SearchText=txtSearch.Text.Trim();
				DataTable dt = optbo.GetBranchdetails();
				grdMst.DataSource = dt;
				grdMst.DataBind();
				lbltotRecords.Text = dt.Rows.Count.ToString();
			}
			catch (Exception exp)            
			{			  
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}
	}
}