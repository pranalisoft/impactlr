﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LobCustomerXref : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillDropDownCustomer()
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            CustomerBO billingBO = new CustomerBO();
            billingBO.SearchText = string.Empty;
            billingBO.ShowAll = "N";
            DataTable dt = billingBO.GetCustomerDetails();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 1;
        }

        private void FillDropDownLob()
        {
            cmbLob.DataSource = null;
            cmbLob.Items.Clear();
            cmbLob.Items.Add(new ListItem("", "-1"));
            BranchBO OptBO = new BranchBO();
            OptBO.SearchText = string.Empty;
            DataTable dt = OptBO.GetBranchdetails();
            cmbLob.DataSource = dt;
            cmbLob.DataTextField = "Name";
            cmbLob.DataValueField = "Srl";
            cmbLob.DataBind();
            if (cmbLob.Items.Count > 1)
                cmbLob.SelectedIndex = 1;
        }

        private void clearAll()
        {
            cmbCustomer.SelectedIndex = 0;
            cmbLob.SelectedIndex = 0;
            cmbLob.Focus();
        }

        private void FillGrid()
        {
            try
            {
                CustomerBO optbo = new CustomerBO();
                optbo.SearchText = txtSearch.Text.Trim();
                DataTable dt = optbo.GetLobWiseCustomer();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                FillDropDownCustomer();
                FillDropDownLob();
                FillGrid();
                clearAll();
                cmbLob.Focus();
            }
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "DelRecord")
                {
                    CustomerBO optbo = new CustomerBO();
                    optbo.Srl = long.Parse(keys["CustomerId"].ToString());
                    optbo.LobId = long.Parse(keys["LobId"].ToString());
                    long cnt = optbo.DeleteCustomerLob();
                    if (cnt > 0)
                    {
                        FillGrid();
                        cmbLob.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerBO optbo = new CustomerBO();
                optbo.LobId = long.Parse(cmbLob.SelectedValue);
                optbo.Srl = long.Parse(cmbCustomer.SelectedValue);
                long flgCnt = 0;
                flgCnt = optbo.InsertCustomerLob();

                if (flgCnt > 0)
                {
                    SetPanelMsg("Customer Lob Saved Successfully.", true, 1);
                }
                else
                {
                    SetPanelMsg("Record Not Updated", true, 0);
                }
                cmbCustomer.SelectedIndex = 0;
                txtSearch.Text = string.Empty;
                FillGrid();
                cmbCustomer.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearAll();
                cmbLob.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }
    }
}