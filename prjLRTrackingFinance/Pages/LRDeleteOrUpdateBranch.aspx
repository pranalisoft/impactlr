﻿<%@ Page Title="Update Branch Or Delete LR" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="LRDeleteOrUpdateBranch.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRDeleteOrUpdateBranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                Update Branch Or Cancel LR
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                    <div>
                        <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                            <asp:Label ID="lblMsg" Text="" runat="server" />
                        </asp:Panel>
                    </div>
                    <div style="padding-left: 15px;">
                        <div>
                            <table class="control_set" width="99%">
                                <tr class="control_row">
                                    <td style="width: 30%">
                                        <table class="control_set">
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text="LR No" CssClass="NormalTextBold"></asp:Label>
                                                    &nbsp;<span style="color: red">*</span>
                                                </td>
                                                <td>
                                                    <asp:ComboBox ID="cmbLR" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                        MaxLength="120" TabIndex="1" CssClass="WindowsStyle" DropDownStyle="DropDownList"
                                                        RenderMode="Block" AutoPostBack="true" 
                                                        OnSelectedIndexChanged="cmbLR_SelectedIndexChanged" AppendDataBoundItems="True">
                                                    </asp:ComboBox>
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text="Branch" CssClass="NormalTextBold"></asp:Label>
                                                    &nbsp;<span style="color: red">*</span>
                                                </td>
                                                <td>
                                                    <asp:ComboBox ID="cmbBranch" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                        MaxLength="120" TabIndex="2" CssClass="WindowsStyle" DropDownStyle="DropDownList"
                                                        RenderMode="Block" AppendDataBoundItems="True">
                                                    </asp:ComboBox>
                                                </td>
                                            </tr>
                                             <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label25" runat="server" Text="Cancel Reason" CssClass="NormalTextBold"></asp:Label>
                                                    &nbsp;<span style="color: red">*</span>
                                                </td>
                                                <td>
                                                   <asp:TextBox runat="server" ID="txtCancelReason" MaxLength="50" Width="300px" CssClass="NormalTextBold" ></asp:TextBox>
                                                   &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvCancelReason" ControlToValidate="txtCancelReason" ValidationGroup="save" SetFocusOnError="true" ErrorMessage="Cancellation Reason is Required" ></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table class="control_set" cellpadding="0" cellspacing="0" width="100%">
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Button TabIndex="3" ID="btnSave" runat="server" Text="Update Branch" CssClass="button"
                                                        OnClick="btnSave_Click"/>
                                                    &nbsp &nbsp
                                                     <asp:Button TabIndex="4" ID="btnDelete" runat="server" ValidationGroup="save" Text="Cancel LR" CssClass="button"
                                                        OnClick="btnDelete_Click"/>
                                                    &nbsp &nbsp
                                                    <asp:Button TabIndex="5" ID="btnCancel" runat="server" Text="Back" CssClass="button"
                                                        OnClick="btnCancel_Click" />
                                                    &nbsp&nbsp
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <hr style="color: Gray; width: 100%" align="right" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td >
                                        <table class="control_set" style="border: 1px solid blue; width:99%">
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="lbl001" Text="LR No." runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbl002" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLRNo" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" Text="Date" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label131" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDate" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label14" Text="Branch" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>    
                                                 <td colspan="4">
                                                    <asp:Label ID="lblBranch" Text="" runat="server" CssClass="NormalTextBold" style="color:Blue;font-weight:bold"/>
                                                </td>                                            
                                            </tr>
                                            <tr class="control_row">
                                                <td style="width: 100px">
                                                    <asp:Label ID="Label5" Text="Consigner" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td style="width: 10px">
                                                    <asp:Label ID="Label6" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblConsigner" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td style="width: 150px">
                                                    <asp:Label ID="Label01" Text="Consignee" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td style="width: 10px">
                                                    <asp:Label ID="Label141" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblConsignee" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label7" Text="From" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFrom" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label16" Text="To" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label18" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTo" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label9" Text="Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label10" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label11" Text="Chargeable Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label121" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblChargeableWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label15" Text="Vehicle No" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label17" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVehicleNo" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label19" Text="Total Packages" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label20" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPackages" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label132" Text="Invoice No" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label142" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblInvoiceNo" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label21" Text="Invoice Value" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label22" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblInvoiceValue" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label23" Text="Type" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label24" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblType" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label26" Text="Remarks" runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label27" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblRemarks" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                            <tr class="control_row">
                                                <td>
                                                    <asp:Label ID="Label12" Text="Driver's Name & No." runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label13" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDriverDetails" Text="" runat="server" CssClass="NormalTextBold" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cmbLR" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
