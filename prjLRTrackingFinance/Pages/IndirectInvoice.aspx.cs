﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using prjLRTrackerFinanceAuto.Common;
using BusinessObjects.BO;
using System.Data;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class IndirectInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            if (!IsPostBack)
            {
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");               
                ShowViewByIndex(1);
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtDate.Enabled = false;
                LoadGridView();
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void FillDropdowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;

            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            SupplierBO CustBO = new SupplierBO(); 
            DataTable dtCust = CustBO.GetPlySuppliers();
            cmbSupplier.DataSource = dtCust;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;

            PlyItemBO optbo = new PlyItemBO();
            cmbItem.Items.Add(new ListItem("", "-1"));
            optbo.SearchText = "";
            DataTable dtply = optbo.GetPlyItemdetails();
            cmbItem.DataSource = dtply;
            cmbItem.DataTextField = "Name";
            cmbItem.DataValueField = "Srl";
            cmbItem.DataBind();
            cmbItem.SelectedIndex = 0;

        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            SuplInvoiceIndirectBO optbo = new SuplInvoiceIndirectBO();
            NumberToWords wordBO = new NumberToWords();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillDropdowns();
                    ClearView();                   
                    ShowViewByIndex(0);
                    cmbBillingCompany.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (CommonTypes.GetDataTable("dtInvoice") == null)
                    {
                        SetPanelMsg("Please select atleast one item to continue", true, 0);
                        return;
                    }
                    DataTable dt = CommonTypes.GetDataTable("dtInvoice");
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    string itemXML = ds.GetXml();
                    optbo.Srl = 0;
                    optbo.BillCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.SupplierSrl = long.Parse(cmbSupplier.SelectedValue);
                    optbo.BuyerRefNo = txtBRefNo.Text.Trim();
                    optbo.BuyerRefDate = txtBdate.Text;
                    optbo.basicAmount = decimal.Parse(lblInvoiceAmount.Text);//decimal.Parse(lblBasicAmount.Text);
                    optbo.InvoiceAmount = decimal.Parse(lblInvoiceAmount.Text);
                    optbo.AmountInWords = wordBO.Num2WordConverter(lblInvoiceAmount.Text).ToString();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.ItemXML = itemXML;
                    long cnt = optbo.InsertUpdateInvoice();
                    if (cnt>0)
                    {
                        SetPanelMsg("Record Saved Successfully", true, 1);
                        ShowViewByIndex(1);
                        LoadGridView();
                    }

                    break;
                case CommonTypes.EntryFormCommand.Submit:
                    ConfirmItems();
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
                case CommonTypes.EntryFormCommand.SaveP:
                    break;
                default:
                    break;
            }

        }

        private void LoadGridView()
        {
            SuplInvoiceIndirectBO optbo = new SuplInvoiceIndirectBO();
            optbo.FromDate = txtFromDate.Text;
            optbo.ToDate = txtToDate.Text;
            optbo.SearchText = txtSearch.Text;
            DataTable dt = optbo.ShowHeaderData();
            grdInvoiceHeader.DataSource = dt;
            grdInvoiceHeader.DataBind();

            lblTotalPacklist.Text = "Total Record(s) : " + dt.Rows.Count.ToString();
        }

        private void ClearView()
        {
            cmbBillingCompany.SelectedIndex = 0; cmbSupplier.SelectedIndex = 0;
            txtBRefNo.Text = string.Empty; txtBdate.Text = string.Empty; txtQty.Text = string.Empty; txtRate.Text = string.Empty;
            lblInvoiceAmount.Text = string.Empty; //lblBasicAmount.Text = string.Empty; 

            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtInvoice") == null)
            {
                dt = CommonTypes.CreateDataTable("PlyItemSrl,PlyItemName,Qty,Rate,HSNId,HSNCode,GST,BasicAmount,NetAmount", "dtInvoice");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtInvoice");
            }

            dt.Rows.Clear();
            dt.AcceptChanges();

            grdItems.DataSource = null;
            grdItems.DataBind();
            //cmbItem.SelectedIndex = 1; 
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void ConfirmItems()
        {
            PlyItemBO optbo= new PlyItemBO();
            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtInvoice") == null)
            {
                dt = CommonTypes.CreateDataTable("PlyItemSrl,PlyItemName,Qty,Rate,HSNId,HSNCode,GST,BasicAmount,NetAmount", "dtInvoice");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtInvoice");
            }
             DataRow[] foundRows = dt.Select("PlyItemSrl=" + cmbItem.SelectedValue + "");
             if (foundRows.LongLength > 0)
             {
                 foundRows[0]["Qty"] = txtQty.Text;
                 foundRows[0]["Rate"] = txtRate.Text;
                 foundRows[0]["BasicAmount"] = (decimal.Parse(txtRate.Text) * decimal.Parse(txtQty.Text)).ToString("F2");
                 foundRows[0]["NetAmount"] = (decimal.Parse(foundRows[0]["BasicAmount"].ToString()) * (1 + decimal.Parse(foundRows[0]["GST"].ToString()) / 100)).ToString("F2");
                 dt.AcceptChanges();
             }
             else
             {
                 DataRow dr = dt.NewRow();

                 dr["PlyItemSrl"] = cmbItem.SelectedValue;
                 dr["PlyItemName"] = cmbItem.SelectedItem.Text.Trim();
                 dr["Qty"] = txtQty.Text;
                 dr["Rate"] = txtRate.Text;
                 dr["BasicAmount"] = (decimal.Parse(txtRate.Text) * decimal.Parse(txtQty.Text)).ToString("F2");

                 optbo.Srl = long.Parse(cmbItem.SelectedValue);
                 DataTable dthsnGst = optbo.GetPlyItemHSNGST();
                 if (dthsnGst.Rows.Count > 0)
                 {
                     dr["HSNId"] = dthsnGst.Rows[0]["HSNId"].ToString();
                     dr["HSNCode"] = dthsnGst.Rows[0]["HSNCode"].ToString();
                     dr["GST"] = dthsnGst.Rows[0]["GST"].ToString();
                     dr["NetAmount"] = (decimal.Parse(dr["BasicAmount"].ToString()) * (1 + decimal.Parse(dr["GST"].ToString()) / 100)).ToString("F2");
                 }
                 dt.AcceptChanges();
                 CommonTypes.Addrow(dt, dr);
             }
            
            grdItems.DataSource = dt;
            grdItems.DataBind();
            CalculateAmounts();
            cmbItem.Focus();
        }

        private void CalculateAmounts()
        {
            decimal basicamount = 0;
            decimal invoiceamount = 0;
            for (int i = 0; i < grdItems.Rows.Count; i++)
            {
                basicamount += decimal.Parse(grdItems.DataKeys[i]["BasicAmount"].ToString());
                invoiceamount += decimal.Parse(grdItems.DataKeys[i]["NetAmount"].ToString());
            }

            //lblBasicAmount.Text = basicamount.ToString("F2");
            lblInvoiceAmount.Text = invoiceamount.ToString("F2");
        }

        protected void grdItems_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdInvoiceHeader.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;

          

            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];
            if (e.CommandName.ToLower()=="item")
            {
                SuplInvoiceIndirectBO optbo1 = new SuplInvoiceIndirectBO();
                DataTable dtview = optbo1.ShowDetailData();
                grdViewItemDetls.DataSource = dtview;
                grdViewItemDetls.DataBind();
             
            }
            else if (e.CommandName.ToLower() == "modify")
            {
                ClearView();
                FillDropdowns();
                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                cmbSupplier.SelectedValue = keys["Supplier_Srl"].ToString();
                txtBRefNo.Text = keys["BuyerRefNo"].ToString();
                txtBdate.Text = DateTime.Parse(keys["BuyerRefDate"].ToString()).ToString("dd/MM/yyyy");
                lblInvoiceAmount.Text = keys["InvoiceAmount"].ToString();
                SuplInvoiceIndirectBO optbo = new SuplInvoiceIndirectBO();
                optbo.Srl = long.Parse(keys["BillingCompany_Srl"].ToString());
                DataTable dt = optbo.ShowDetailData();
                grdItems.DataSource = dt;
                grdItems.DataBind();
                CommonTypes.Insertdata(dt);
                ShowViewByIndex(0);

            }
        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                        //LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    //}
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    //LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

       
    }
}