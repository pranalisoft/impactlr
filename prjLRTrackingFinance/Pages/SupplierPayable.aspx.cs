﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierPayable : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillComboSupplier()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void LoadGridView()
        {
            SupplierPaymentBO optBO = new SupplierPaymentBO();
            if (cmbSupplier.SelectedIndex > 0)
                optBO.SupplierID = long.Parse(cmbSupplier.SelectedValue);
            else
                optBO.SupplierID = 0;
            DataTable dt = optBO.ShowPayable();
            grdView.DataSource = dt;
            grdView.DataBind();
            lblTotalRecords.Text = dt.Rows.Count.ToString();
        }

        //public void ExportToExcel(DataTable table, string filename)
        //{
        //    HttpContext.Current.Response.Clear();
        //    HttpContext.Current.Response.ClearContent();
        //    HttpContext.Current.Response.ClearHeaders();
        //    HttpContext.Current.Response.Buffer = true;
        //    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xlsx");

        //    using (ExcelPackage pack = new ExcelPackage())
        //    {
        //        ExcelWorksheet ws = pack.Workbook.Worksheets.Add(filename);
        //        ws.Cells["A1"].LoadFromDataTable(table, true);
        //        ws.Cells.Style.Font.SetFromFont(new Font("Calibri", 12));
        //        ws.Cells["B:B"].Style.Numberformat.Format = "dd-MMM-yyyy";
        //        ws.Cells["G:G"].Style.Numberformat.Format = "dd-MMM-yyyy";
        //        ws.Cells["H:H"].Style.Numberformat.Format = "dd-MMM-yyyy";
        //        ws.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";               
        //        ws.Cells["A1:BZ1"].AutoFilter = true;
        //        ws.Cells.AutoFitColumns();

        //        using (ExcelRange objRange = ws.Cells["A1:XFD1"])
        //        {
        //            objRange.Style.Font.Bold = true;
        //            objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //            objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //            objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            objRange.Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
        //        }
        //        var ms = new System.IO.MemoryStream();
        //        pack.SaveAs(ms);
        //        ms.WriteTo(HttpContext.Current.Response.OutputStream);
        //    }
        //    HttpContext.Current.Response.Flush();
        //    HttpContext.Current.Response.End();
        //}
        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["G:G"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["H:H"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            if (!IsPostBack)
            {
                FillComboSupplier();
                cmbSupplier.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {            
            SupplierPaymentBO optBO = new SupplierPaymentBO();
            optBO.SupplierID = long.Parse(cmbSupplier.SelectedValue);
            DataTable dt = optBO.ShowPayable();
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SupplierPayable_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            ExportToExcel(ds, filename);
        }
    }
}