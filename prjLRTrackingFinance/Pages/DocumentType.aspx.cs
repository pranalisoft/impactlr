﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DocumentType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                FillGrid();
                clearAll();
            }
        }

        

        

        private void clearAll()
        {
            txtHiddenId.Value = "0";
            txtDocumentTypeName.Text = string.Empty;
            chkActive.Checked = false;
            chkIsMandatory.Checked = false;
            txtDocumentTypeName.Focus();
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    
                    clearAll();
                    txtHiddenId.Value = keys["DocumentTypeId"].ToString();
                    txtDocumentTypeName.Text = keys["DocumentTypeName"].ToString();
                    
                    chkActive.Checked = bool.Parse(keys["Active"].ToString());
                    chkIsMandatory.Checked = bool.Parse(keys["IsMandatory"].ToString());

                    txtDocumentTypeName.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DocumentTypeBO optbo = new DocumentTypeBO();
                optbo.DocumentTypeName = txtDocumentTypeName.Text.Trim();
                optbo.DocumentTypeId = int.Parse(txtHiddenId.Value);
                optbo.Active = chkActive.Checked;
                optbo.IsMandatory = chkIsMandatory.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                if (optbo.isDocumentTypeExist() == false)
                {
                    long flgCnt = 0;
                    flgCnt = optbo.InsertUpdateDocumentType();

                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        SetPanelMsg("Document Type added successfully.", true, 1);
                    }
                    else
                    {
                        SetPanelMsg("Document Type updated successfully.", true, 1);
                    }
                    clearAll();
                    txtSearch.Text = string.Empty;
                    FillGrid();
                    txtDocumentTypeName.Focus();
                }
                else
                {
                    SetPanelMsg("Document Type Already Exist.", true, 0);
                }
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearAll();
                txtDocumentTypeName.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }


        private void FillGrid()
        {
            try
            {
                DocumentTypeBO optbo = new DocumentTypeBO();
                optbo.SearchText = txtSearch.Text.Trim();
                DataTable dt = optbo.ShowData();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }
    }
}