﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.ErrorPage" Title="Error Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="/Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="/Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
  
    <div>
        <table style="width: 100%">
            <tr>
                <td width="68%">
                    <img alt="HAIL Logo" height="35px" src="../Include/Common/images/ihon.jpg" />
                </td>
               
                <td>
                </td>
            </tr>
        </table>
        <table style="width: 100%; font-size: x-small;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 25px; height: 25px">
                    <img alt="" src="../Include/Common/images/left.png" />
                </td>
                <td style="background-color: #3A66AF; height: 25px; color: #3A66AF">
                    &nbsp;
                </td>
                <td style="width: 25px; height: 25px">
                    <img alt="" src="../Include/Common/images/right.png" />
                </td>
            </tr>
            <tr style="height: 2px; background-color: #dce8f5">
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        
    
    </div>
    <div style="padding:25px 10px 10px 10px; vertical-align:middle; text-align:center;">
    <table width="98%">
    <tr>
    <td style="font-size:14px; font-weight:bold; color:Red;">Either Session Has Timed 
        Out Or Unexpected Error Has Occured. Please Contact your System Administrator.</td>
    </tr>
     <tr>
                <td style="font-size: 14px;color:Red">
                        <a href="../login.aspx">Click Here To Re-Login</a>
                    </td>
                </tr>
           
    </table>
    </div>
    </form>
</body>
</html>
