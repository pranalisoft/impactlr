﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateLRAmounts : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        private void FillComboLR()
        {
            LRBO OptBO = new LRBO();
            cmbLR.DataSource = null;
            cmbLR.Items.Clear();
            cmbLR.Items.Add(new ListItem("", "-1"));
            DataTable dt = OptBO.FillLRComboDelete();
            cmbLR.DataSource = dt;
            cmbLR.DataTextField = "LRNo";
            cmbLR.DataValueField = "ID";
            cmbLR.DataBind();
            cmbLR.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboLR();
                cmbLR.Focus();
            }
        }

        protected void cmbLR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LRBO OptBO = new LRBO();
                OptBO.ID = long.Parse(cmbLR.SelectedValue);
                DataTable dt = OptBO.GetLRHeaderById();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtFreightAmount.Text = dt.Rows[0]["TotalFrieght"].ToString();
                        txtSupplierAmount.Text = dt.Rows[0]["SupplierAmount"].ToString();
                        txtRemarks.Text = string.Empty;
                        if (bool.Parse(dt.Rows[0]["IsPTL"].ToString()))
                        {
                            txtCashAdvance.Text = "0";
                            txtFuelAdvance.Text = "0";
                            txtTotalAdvance.Text = "0";
                            txtCashAdvance.Enabled = false;
                            txtFuelAdvance.Enabled = false;
                            txtTotalAdvance.Enabled = false;
                            txtRemarks.Enabled = false;
                        }
                        else
                        {
                            txtCashAdvance.Text = dt.Rows[0]["CashAdvance"].ToString();
                            txtFuelAdvance.Text = dt.Rows[0]["FuelAdvance"].ToString();
                            txtTotalAdvance.Text = dt.Rows[0]["AdvanceToSupplier"].ToString();
                            txtCashAdvance.Enabled = true;
                            txtFuelAdvance.Enabled = true;
                            txtTotalAdvance.Enabled = true;
                            txtRemarks.Enabled = true;
                        }
                        txtFreightAmount.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbLR.SelectedIndex <= 0)
                {
                    SetPanelMsg("Please Select LR From The List", true, 0);
                    cmbLR.Focus();
                    return;
                }
               
                LRBO OptBO = new LRBO();
                OptBO.ID = long.Parse(cmbLR.SelectedValue);
                OptBO.CashAdvance = decimal.Parse(txtCashAdvance.Text);
                OptBO.FuelAdvance = decimal.Parse(txtFuelAdvance.Text);
                OptBO.TotalAdvance = decimal.Parse(txtTotalAdvance.Text);
                OptBO.TotalFrieght = decimal.Parse(txtFreightAmount.Text);
                OptBO.SupplierAmount = decimal.Parse(txtSupplierAmount.Text);
                OptBO.Remarks = txtRemarks.Text;
                OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
                long Cnt = OptBO.UpdateLRAmounts();
                if (Cnt > 0)
                {
                    cmbLR.SelectedIndex = 0;                   
                    SetPanelMsg("Amount(s) Updated Successfully.", true, 1);
                    btnCancel_Click(btnCancel, EventArgs.Empty);
                    return;
                }
            }
            catch (Exception ex)
            {
                SetPanelMsg(ex.Message, true, 0);
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillComboLR();
            txtFreightAmount.Text = string.Empty;
            txtSupplierAmount.Text = string.Empty;

            txtCashAdvance.Text = string.Empty;
            txtFuelAdvance.Text = string.Empty;
            txtTotalAdvance.Text = string.Empty;

            txtRemarks.Text = string.Empty;
            cmbLR.Focus();

        }
    }
}