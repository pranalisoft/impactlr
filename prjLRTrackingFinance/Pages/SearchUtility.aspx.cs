﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SearchUtility : System.Web.UI.Page
    {
        private void InitializeContent(string LRNo)
        {
            lblError.Visible = false;
            lblDate.Text = "-";
            lblConsigner.Text = "-";
            lblConsignee.Text = "-";
            lblFrom.Text = "-";
            lblTo.Text = "-";
            lblVehicleNo.Text = "-";
            lblPackages.Text = "-";
            lblWeight.Text = "-";
            lblChargeableWeight.Text = "-";
            lblInvoiceNo.Text = "-";
            lblInvoiceValue.Text = "-";
            lblType.Text = "-";
            lblRemarks.Text = "-";
            lblPODDate.Text = "-";
            lblDriverDtls.Text = "-";
            LRBO pol = new LRBO();
            pol.LRNo = LRNo;
            if (LRNo != string.Empty)
            {
                DataTable dt = pol.GetLRHeaderByNo();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                        lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                        lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                        lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                        lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                        lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                        lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                        lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                        lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                        lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                        lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                        lblType.Text = dt.Rows[0]["LoadType"].ToString();
                        lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                        lblDriverDtls.Text = dt.Rows[0]["DriverDetails"].ToString();
                        if (dt.Rows[0]["PODDate"].ToString().Trim() != "")
                        {
                            lblPODDate.Text = DateTime.Parse(dt.Rows[0]["PODDate"].ToString()).ToString("dd/MMM/yyyy");
                        }

                        if (dt.Rows[0]["PODFile"].ToString().Trim() != "")
                        {
                            hplPODFile.Visible = true;
                            Session["FilePath"] = Server.MapPath(ConfigurationManager.AppSettings["FilesPath"].ToString()) + @"\" + dt.Rows[0]["PODFile"].ToString().Trim();
                        }
                        else
                        {
                            hplPODFile.Visible = false;
                        }

                    }
                    else
                    {
                        lblError.Visible = true;
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            DataTable dt1 = pol.GetLRDtlsByNo();
            lstViewPayments.DataSource = dt1;
            lstViewPayments.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hplPODFile.Visible = false;
                lblError.Visible = false;
                txtLRNo.Focus();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            InitializeContent(txtLRNo.Text.Trim());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void hplPODFile_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewFile.aspx", false);
        }
    }
}