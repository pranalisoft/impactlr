﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using AjaxControlToolkit;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PostPODUpdationEdit : System.Web.UI.Page
    {
        #region "Procedure"
        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void LoadGridView(string AddOrEdit)
        {
            CommonBO optBO = new CommonBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.AddOrEdit = AddOrEdit;
            optBO.PendingForApproval = chkPending.Checked;
            DataTable dt = optBO.GetLRForPostPOD();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
            PnlItemDtls.Visible = false;
        }

        private void LoadGridViewPenalty(long ID)
        {
            CommonBO optBO = new CommonBO();
            optBO.srl = ID;
            DataTable dt = optBO.GetPODPenalty();
            CommonTypes.Insertdata(dt);
            grdItems.DataSource = dt;
            grdItems.DataBind();
        }

        private void LoadGridViewDetails(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataTable dt = optBO.FillLRStatus();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        private void LoadGridViewZeroLR(long ID)
        {
            CommonBO optBO = new CommonBO();
            optBO.srl = ID;
            DataTable dt = optBO.GetZeroLR();
            grdLrZero.DataSource = dt;
            grdLrZero.DataBind();
            btnSavePostPOD.Visible = true;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["PODScanned"].ToString().ToLower() == "no")
                        {
                            btnSavePostPOD.Visible = false;
                            break;
                        }
                    }
                }
            }
        }

        private void FillDropDownReason()
        {
            PenaltyReasonBO optbo = new PenaltyReasonBO();
            DataTable dt;

            cmbReason.DataSource = null;
            cmbReason.Items.Clear();
            cmbReason.Items.Add(new ListItem("", "-1"));
            optbo.SearchText = "";
            dt = optbo.GetPenaltyReasondetails();
            cmbReason.DataSource = dt;
            cmbReason.DataTextField = "Name";
            cmbReason.DataValueField = "Srl";
            cmbReason.DataBind();
            cmbReason.SelectedIndex = 0;
        }

        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void calculateCharges()
        {
            if (txtDetention.Text.Trim() == "") txtDetention.Text = "0";
            if (txtWarai.Text.Trim() == "") txtWarai.Text = "0";
            if (txtTwoPoint.Text.Trim() == "") txtTwoPoint.Text = "0";
            if (txtOtherCharges.Text.Trim() == "") txtOtherCharges.Text = "0";
            if (txtTotalPenalty.Text.Trim() == "") txtTotalPenalty.Text = "0";
            if (txtLatePODCharges.Text.Trim() == "") txtLatePODCharges.Text = "0";

            txtTotalCharges.Text = (decimal.Parse(txtDetention.Text) + decimal.Parse(txtWarai.Text) + decimal.Parse(txtTwoPoint.Text) + decimal.Parse(txtOtherCharges.Text) - decimal.Parse(txtTotalPenalty.Text) - decimal.Parse(txtLatePODCharges.Text)).ToString().Replace(".00", "");
            txtTotalBalance.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(txtBalance.Text)).ToString();
            if (!txtWarai.Enabled)
            {
                txtTotalBalance.Text = (decimal.Parse(txtTotalBalance.Text) - decimal.Parse(txtWarai.Text)).ToString().Replace(".00", "");
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            string FileName = "PODUpdationEdit_" + DateTime.Now + ".xls";
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                grdPacklistView.AllowPaging = false;
                this.LoadGridView("Edit");

                //grdPacklistView.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in grdPacklistView.HeaderRow.Cells)
                {
                    cell.BackColor = grdPacklistView.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in grdPacklistView.Rows)
                {
                    //row.BackColor = Color.White;
                    DataKey key = grdPacklistView.DataKeys[row.RowIndex];
                    string LRNo = key["LRNo"].ToString();
                    string Approved = "No";
                    if (key["Approved"] != null)
                    {
                        if (key["Approved"].ToString() == "1" || key["Approved"].ToString().ToLower() == "true")
                            Approved = "Yes";
                    } 
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        TableCell cell = row.Cells[i];
                        List<Control> controls = new List<Control>();

                        //Add controls to be removed to Generic List
                        foreach (Control control in cell.Controls)
                        {
                            controls.Add(control);
                        }

                        //Loop through the controls to be removed and replace then with Literal
                        foreach (Control control in controls)
                        {
                            switch (control.GetType().Name)
                            {
                                case "HyperLink":
                                    cell.Controls.Add(new Literal { Text = (control as HyperLink).Text });
                                    break;
                                case "TextBox":
                                    cell.Controls.Add(new Literal { Text = (control as TextBox).Text });
                                    break;
                                case "LinkButton":
                                    cell.Controls.Add(new Literal { Text = (control as LinkButton).Text });
                                    break;
                                case "CheckBox":
                                    cell.Controls.Add(new Literal { Text = (control as CheckBox).Checked.ToString() });
                                    break;
                                case "RadioButton":
                                    cell.Controls.Add(new Literal { Text = (control as RadioButton).Text });
                                    break;
                            }
                            cell.Controls.Remove(control);
                            if (i == 0)
                                cell.Text = LRNo;
                            else if (i==12)
                                cell.Text = Approved.ToString();
                            else
                                cell.Text = "";
                        }
                    }                    
                }
                for (int i = 0; i < grdPacklistView.Columns.Count; i++)
                {
                    grdPacklistView.HeaderRow.Cells[i].Font.Bold = true;
                    grdPacklistView.HeaderRow.Cells[i].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                    grdPacklistView.HeaderRow.Cells[i].ForeColor = System.Drawing.Color.White;
                }
                grdPacklistView.RowStyle.Height = 20;

                grdPacklistView.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExport);
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdLrZero);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                LoadGridView("Edit");
                if (Session["ROLEID"].ToString() == "1" || Session["ROLEID"].ToString() == "8")
                {
                    chkAccountsApproval.Checked = false;
                    chkAccountsApproval.Visible = true;
                    btnApprove.Visible = true;
                }
                else
                {
                    chkAccountsApproval.Checked = false;
                    chkAccountsApproval.Visible = false;
                    btnApprove.Visible = false;
                }
                txtSearch.Focus();
            }
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:
                    //cmbDevice.Focus();
                    LoadGridView("Edit");
                    
                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                    //    LoadGridView();
                    //    MsgPanel.Message = "Record(s) deleted successfully.";
                    //    MsgPanel.DispCode = 1;
                    //    txtFromDate.Focus();
                    //}
                    break;
                case CommonTypes.ModalPopupCommand.No:
                   LoadGridView("Edit");
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            CommonBO optbo = new CommonBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                   LoadGridView("Edit");
                    FillDropDownReason();
                    txtSearch.Focus();
                    break;
                case CommonTypes.EntryFormCommand.SaveP:
                    LoadGridView("Edit");
                    txtSearch.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    //if (decimal.Parse(txtTotalBalance.Text) < 0 )
                    //{
                    //    MsgPopUp.ShowModal("Total Balance Payment Cannot be negative.", CommonTypes.ModalTypes.Error);
                    //    txtRemarks.Focus();
                    //    return;
                    //}
                    optbo.srl = long.Parse(txtHiddenId.Value);
                    optbo.LRId = long.Parse(txtLRId.Value);
                    optbo.DetentionCharges = decimal.Parse(txtDetention.Text);
                    optbo.WaraiCharges = decimal.Parse(txtWarai.Text);
                    optbo.TwoPointCharges = decimal.Parse(txtTwoPoint.Text);
                    optbo.OtherCharges = decimal.Parse(txtOtherCharges.Text);
                    optbo.Penalty = 0;
                    optbo.Remarks = txtRemarks.Text;
                    optbo.LatePODCharges = decimal.Parse(txtLatePODCharges.Text);
                    optbo.TotalCharges = decimal.Parse(txtTotalCharges.Text);
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Approved = chkAccountsApproval.Checked;
                    optbo.ReportingDate = txtReportingDate.Text;
                    optbo.UnloadingDate = txtUnloadingDate.Text;
                    optbo.PODDate = txtPODDate.Text;

                    string strItem = "";
                    DataTable dt = CommonTypes.GetDataTable("dtPenalty");
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            strItem = ds.GetXml();
                            strItem = strItem.Replace("'", "");
                            ds.Tables.Remove(dt);
                        }
                    }
                    optbo.strItem = strItem;
                    long cnt = optbo.InsertUpdate_PostPOD();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            MsgPanel.Message = "Record Saved successfully.";
                            MsgPanel.DispCode = 1;
                           LoadGridView("Edit");
                        }
                        else
                        {
                            MsgPanel.Message = "Record Updated successfully.";
                            MsgPanel.DispCode = 1;
                            LoadGridView("Edit");
                        }
                        ShowViewByIndex(1);
                    }
                    break;
                case CommonTypes.EntryFormCommand.None:
                   LoadGridView("Edit");
                    ShowViewByIndex(1);
                    break;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView("Edit");
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView("Edit");
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                if (bool.Parse(keys["Approved"].ToString()) == true)
                {
                    MsgPopUp.ShowModal("Post POD Edit is approved, so cannot edit now.", CommonTypes.ModalTypes.Error);
                    return;
                }

                Session["dtPenalty"] = null;
                FillDropDownReason();
                txtHiddenId.Value = keys["PODID"].ToString();
                txthdPODFileName.Value = keys["PODFile"].ToString();
                txtLRId.Value = keys["ID"].ToString();
                txtLRNo.Text = keys["LRNo"].ToString();
                txtDetention.Text = keys["DetentionCharges"].ToString().Replace(".00", "");
                txtWarai.Text = keys["WaraiCharges"].ToString().Replace(".00", "");
                txtTwoPoint.Text = keys["TwoPointCharges"].ToString().Replace(".00", "");
                txtOtherCharges.Text = keys["OtherCharges"].ToString().Replace(".00", "");
                txtTotalPenalty.Text = keys["Penalty"].ToString().Replace(".00", "");
                txtRemarks.Text = keys["PODRemarks"].ToString();
                txtLatePODCharges.Text = keys["LatePODCharges"].ToString().Replace(".00", "");
                if (txtHiddenId.Value == "0")
                    txtTotalCharges.Text = (decimal.Parse(keys["TotalCharges"].ToString()) - decimal.Parse(keys["LatePODCharges"].ToString())).ToString().Replace(".00", "");
                else
                    txtTotalCharges.Text = keys["TotalCharges"].ToString().Replace(".00", "");

                txtFright.Text = keys["SupplierAmount"].ToString().Replace(".00", "");
                txtAdvance.Text = keys["AdvanceToSupplier"].ToString().Replace(".00", "");
                txtBalance.Text = (decimal.Parse(keys["SupplierAmount"].ToString()) - decimal.Parse(keys["AdvanceToSupplier"].ToString())).ToString().Replace(".00", "");
                //txtTotalBalance.Text = (decimal.Parse(txtBalance.Text) + decimal.Parse(txtTotalCharges.Text)).ToString().Replace(".00", "");
                lblLRDate.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("dd/MM/yyyy");
                txtReportingDate.Text = DateTime.Parse(keys["ReportingDate"].ToString()).ToString("dd/MM/yyyy");
                txtUnloadingDate.Text = DateTime.Parse(keys["UnloadingDate"].ToString()).ToString("dd/MM/yyyy");
                lblDelay.Text = keys["DelayForDelivery"].ToString().Replace(".00", "");
                lblDetain.Text = keys["DetainDays"].ToString().Replace(".00", "");
                lblSupplier.Text = keys["SupplierName"].ToString();
                lblCustomer.Text = keys["CustomerName"].ToString();
                txtPODDate.Text = DateTime.Parse(keys["PODDate"].ToString()).ToString("dd/MM/yyyy");
                txtReportingDate_CalendarExtender.StartDate = DateTime.Parse(keys["LRDate"].ToString());
                txtUnloadingDate_CalendarExtender.StartDate = DateTime.Parse(keys["LRDate"].ToString());
                txtPODDate_CalendarExtender.StartDate = DateTime.Parse(keys["LRDate"].ToString());
                int poddays = int.Parse((DateTime.Parse(keys["PODDate"].ToString()) - DateTime.Parse(keys["UnloadingDate"].ToString())).TotalDays.ToString("F0"));
                if (poddays <= 15)
                    lblPODLate.Text = "0";
                else
                    lblPODLate.Text = (poddays - 15).ToString();
                chkAccountsApproval.Checked = bool.Parse(keys["Approved"].ToString());
                ShowViewByIndex(0);
                LoadGridViewPenalty(long.Parse(txtHiddenId.Value));
                LoadGridViewZeroLR(long.Parse(txtLRId.Value)); 
                if (decimal.Parse(keys["UnloadingChargesPaid"].ToString().Replace(".00", "")) > 0)
                {
                    txtWarai.Enabled = false;
                }
                else
                {
                    txtWarai.Enabled = true;
                }
                calculateCharges();
                txtDetention.Focus();
            }
            if (e.CommandName == "Item")
            {
                txtHiddenId.Value = keys["PODID"].ToString();
                txthdPODFileName.Value = keys["PODFile"].ToString();
                txtLRId.Value = keys["ID"].ToString();
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For LR No. " + keys["LRNo"].ToString();
                //LoadGridViewDetails(long.Parse(keys["ID"].ToString()));

                lblDetention.Text = keys["DetentionCharges"].ToString().Replace(".00", "");
                lblWarai.Text = keys["WaraiCharges"].ToString().Replace(".00", "");
                lblTwopoint.Text = keys["TwoPointCharges"].ToString().Replace(".00", "");
                lblOther.Text = keys["OtherCharges"].ToString().Replace(".00", "");
                lblPenalty.Text = keys["Penalty"].ToString().Replace(".00", ""); 
                lblLatePOD.Text = keys["LatePODCharges"].ToString().Replace(".00", "");
                if (txtHiddenId.Value == "0")
                    lblTotalCharges.Text = (decimal.Parse(keys["TotalCharges"].ToString()) - decimal.Parse(keys["LatePODCharges"].ToString())).ToString().Replace(".00", "");
                else
                    lblTotalCharges.Text = keys["TotalCharges"].ToString().Replace(".00", "");

                lblFrieght.Text = keys["SupplierAmount"].ToString().Replace(".00", "");
                lblAdvance.Text = keys["AdvanceToSupplier"].ToString().Replace(".00", "");
                lblBalance.Text = (decimal.Parse(keys["SupplierAmount"].ToString()) - decimal.Parse(keys["AdvanceToSupplier"].ToString())).ToString().Replace(".00", "");
                lblBalancePay.Text = (decimal.Parse(lblBalance.Text) + decimal.Parse(lblTotalCharges.Text)).ToString().Replace(".00", "");

                if (bool.Parse(keys["Approved"].ToString()))
                {
                    btnApprove.Visible = false;
                }
                else
                {
                    if (Session["ROLEID"].ToString() == "2" || Session["ROLEID"].ToString() == "13" || Session["ROLEID"].ToString() == "14")
                    {
                        btnApprove.Visible = true;
                    }
                    else
                    {
                        btnApprove.Visible = false;
                    }
                }
            }
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void grdLrZero_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "viewFile")
            {
                Session["FilePath"] = Server.MapPath(ConfigurationManager.AppSettings["FilesPath"].ToString()) + @"\" + keys["PODFile"];
                Session["ReportType"] = "SuplInvDoc";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../pages/ViewFile.aspx','_blank')", true);
            }
        }

        protected void grdLrZero_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Text.ToLower() == "no")
                {
                    e.Row.Cells[4].Text = "";
                    e.Row.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtPenalty") == null)
            {
                dt = CommonTypes.CreateDataTable("ReasonSrl,Reason,Charges", "dtPenalty","long,string,decimal");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtPenalty");
            }
            DataRow[] foundRows = dt.Select("ReasonSrl=" + cmbReason.SelectedValue + "");
            if (foundRows.LongLength > 0)
            {
                //SetPanelMsg("Part No And Part Description already selected for this Packlist.", true, 0);
                //cmbPartNo.Focus();
                txtTotalCharges.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(foundRows[0]["Charges"].ToString())).ToString().Replace(".00", "");

                dt.Select("ReasonSrl=" + cmbReason.SelectedValue + "")
                .ToList<DataRow>()
                .ForEach(r =>
                {
                    r["Charges"] = double.Parse(txtPenalty.Text.Trim());
                });
                dt.AcceptChanges();
                grdItems.DataSource = dt;
                grdItems.DataBind();
                cmbReason.SelectedIndex = 0;
                txtTotalPenalty.Text = decimal.Parse(dt.Compute("sum(Charges)", "").ToString()).ToString().Replace(".00", "");
                calculateCharges();               
                txtPenalty.Text = "0";
                cmbReason.Focus();
                return;
            }
            DataRow dr = dt.NewRow();
            dr["ReasonSrl"] = cmbReason.SelectedValue;
            dr["Reason"] = cmbReason.SelectedItem.Text.Trim();
            dr["Charges"] = txtPenalty.Text;
            CommonTypes.Addrow(dt, dr);
            grdItems.DataSource = dt;
            grdItems.DataBind();
            cmbReason.SelectedIndex = 0;
            txtTotalPenalty.Text = decimal.Parse(dt.Compute("sum(Charges)", "").ToString()).ToString().Replace(".00", "");
            calculateCharges();
            txtTotalBalance.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(txtBalance.Text)).ToString();    
            txtPenalty.Text = "0";
            cmbReason.Focus();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DataTable dt = CommonTypes.GetDataTable("dtPenalty");
            for (int grdItemIndex = 0; grdItemIndex < grdItems.Rows.Count; grdItemIndex++)
            {
                CheckBox rowcheck = (CheckBox)grdItems.Rows[grdItemIndex].FindControl("chkSelect");
                if (rowcheck.Checked)
                {
                    DataKey keys = grdItems.DataKeys[grdItemIndex];
                    int dtItemIndex = dt.Rows.Count - 1;
                    while (dtItemIndex >= 0)
                    {
                        if (dt.Rows[dtItemIndex]["ReasonSrl"].ToString() == keys["ReasonSrl"].ToString())
                        {
                            //dt.Rows.RemoveAt(dtItemIndex);
                            //txtTotalCharges.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(keys["Charges"].ToString())).ToString().Replace(".00", "");
                            //txtTotalBalance.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(txtBalance.Text)).ToString().Replace(".00", "");
                            CommonTypes.DeleteRow("dtPenalty", dtItemIndex);
                        }
                        dtItemIndex = dtItemIndex - 1;
                    }
                    dt.AcceptChanges();
                }
            }
            grdItems.DataSource = CommonTypes.GetDataTable("dtPenalty");
            grdItems.DataBind();
            if (dt.Rows.Count > 0)
                txtTotalPenalty.Text = decimal.Parse(dt.Compute("sum(Charges)", "").ToString()).ToString().Replace(".00", "");
            else
                txtTotalPenalty.Text = "0";

            calculateCharges();
            txtTotalBalance.Text = (decimal.Parse(txtTotalCharges.Text) + decimal.Parse(txtBalance.Text)).ToString();
            cmbReason.Focus();
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdItems, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdItems, "chkSelect");
            }
        }

        protected void hplPODFile_Click(object sender, EventArgs e)
        {
            Session["FilePath"] = Server.MapPath(ConfigurationManager.AppSettings["FilesPath"].ToString()) + @"\" + txthdPODFileName.Value;
            Session["ReportType"] = "SuplInvDoc";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../pages/ViewFile.aspx','_blank')", true);

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            CommonBO optbo = new CommonBO();
            optbo.srl = long.Parse(txtHiddenId.Value);
            optbo.LRId = long.Parse(txtLRId.Value);
            optbo.DetentionCharges = decimal.Parse(lblDetention.Text);
            optbo.WaraiCharges = decimal.Parse(lblWarai.Text);
            optbo.TwoPointCharges = decimal.Parse(lblTwopoint.Text);
            optbo.OtherCharges = decimal.Parse(lblOther.Text);
            optbo.Penalty = 0;
            optbo.Remarks = "";
            optbo.LatePODCharges = decimal.Parse(lblLatePOD.Text);
            optbo.TotalCharges = decimal.Parse(lblTotalCharges.Text);
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            optbo.Approved = true;
            optbo.strItem = "";
            long cnt = optbo.InsertUpdate_PostPOD();
            if (cnt > 0)
            {
                MsgPopUp.ShowModal("Record Approved successfully ", CommonTypes.ModalTypes.Alert);
            }
        }
        #endregion
    }
}