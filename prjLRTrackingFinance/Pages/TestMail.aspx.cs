﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using prjLRTrackerFinanceAuto.Common;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class TestMail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtUserId.Text = "onkarmedhekar@gmail.com";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string ToId = txtUserId.Text;
                string subject = " Advance Shipment Notification LR No(s). " + "";
                string body = CommonTypes.GetMailCotent("L");

                string fromId = ConfigurationManager.AppSettings["FromId"].ToString().Replace("&gt;", ">").Replace("&lt;", "<"); ;
                string CCId = "vishal.d.pasalkar@gmail.com"; //"pramod.more@impact-logistics.in" ; 
                MailHandler mailhandler = new MailHandler();

                //string FilePath = SavePdfLRForEmail(txtLRNo.Text.Trim().Replace("/", ""));

                body = string.Format(body, "Ash Logistics", "12345", "01/Nov/2020", "MH12NQ4567", "PUNE", "PATNA", "06/nOV/2020");

                body = "<html><head><style>body {font-family:arial;font-size:10pt;} td {font-family:arial;font-size:9pt;}</style></head><body bgcolor=#ffffff><table width=100%><tr bgcolor=#336699><td align=left style=font-weight:bold><font color=white> " +
                           " LR Tracker Tool</font></td></tr></table> <table> <tr> <td>" + body;

                body = body + "</td></tr></table><br/><br/><br/>" +
    "<hr size=2 width=100% align=center><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td>NOTE: This is a system generated notification, please <font color=red>DO NOT REPLY</font> to this mail.</td></tr></table><br><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td  valign=top style={font-family:verdana;font-size:60%;color:#888888}>This" +
    "e-mail, and any attachments thereto, are intended only for use by the addressee(s) named herein. If you are not the intended recipient of this e-mail, you are hereby notified that any dissemination, distribution or copying which amounts to misappropriation of this e-mail and any attachments thereto, is strictly prohibited. If you have received this e-mail in error, please immediately notify me and permanently delete the original and any copy of any e-mail and " +
    "any printout thereof.</td></tr></table></body></html>";

                MailMessage msg = mailhandler.CreateMessage(ToId, CCId, subject, body, true, "");



                string smtphost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                string SenderMail = ConfigurationManager.AppSettings["SenderEmail"].ToString();
                string Password = ConfigurationManager.AppSettings["Pwd"].ToString();
                int port = int.Parse(ConfigurationManager.AppSettings["PortNo"].ToString());

                var client = new SmtpClient(smtphost, port)
                {
                    UseDefaultCredentials = bool.Parse(txtoldpwd.Text),
                    Credentials = new NetworkCredential(SenderMail, Password),
                    EnableSsl = true
                };


                client.Send(msg);
            }
            catch (Exception ex)
            {
                SetPanelMsg(ex.Message, true, 1);
                return;
            }
            


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }
    }
}