﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRTrackingHomeNew : System.Web.UI.Page
    {
        public void FillFUP()
        {
            lblRecCount.Text = "Total LR Pending For Freight Updation : 0";
            LRBO OptBO = new LRBO();
            OptBO.BranchID = 0;// long.Parse(Session["BranchID"].ToString());
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = OptBO.LRFreightUpdationCount();
            grdViewFUP.DataSource = dt;
            grdViewFUP.DataBind();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    lblRecCount.Text = "Total LR Pending For Freight Updation : " + dt.AsEnumerable().Sum(row => row.Field<int>("TotalRecords")).ToString();
                }
            }
        }

        public void FillEWO()
        {
            lblRecCount.Text = "Due & Overdue Ewaybills : 0";
            ReportBO rptBo = new ReportBO();
            rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dtewaybill = rptBo.GetDueEwayBillsDashboard();  
            grdEwaydBill.DataSource = dtewaybill;
            grdEwaydBill.DataBind();
            if (dtewaybill != null)
            {
                if (dtewaybill.Rows.Count > 0)
                {
                    lblRecCount.Text = "Due & Overdue Ewaybills : " + dtewaybill.Rows.Count.ToString();
                }
            }
        }

        public void FillPOD()
        {
            lblRecCount.Text = "LR Count Pending For Post POD,Approval & Bill : 0";
            ReportBO rptBo = new ReportBO();
            rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dtPending = rptBo.GetPendingDashboard();
            grdSuplBill.DataSource = dtPending;
            grdSuplBill.DataBind();
            if (dtPending != null)
            {
                if (dtPending.Rows.Count > 0)
                {
                    lblRecCount.Text = "LR Count Pending For Post POD,Approval & Bill : " + dtPending.Rows.Count.ToString();
                }
            }
        }

        public void FillVSU()
        {
            lblRecCount.Text = "Total LR Pending For Vehicle Status Updation : 0";
            LRBO OptBO = new LRBO();
            OptBO.BranchID = 0;// long.Parse(Session["BranchID"].ToString());
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dtStatus = OptBO.LRStatusUpdationCount();
            grdViewVSU.DataSource = dtStatus;
            grdViewVSU.DataBind();
            if (dtStatus != null)
            {
                if (dtStatus.Rows.Count > 0)
                {
                    lblRecCount.Text = "Total LR Pending For Vehicle Status Updation : " + dtStatus.AsEnumerable().Sum(row => row.Field<int>("TotalRecords")).ToString();
                }
            }
        }

        public void FillTVA()
        {
            lblRecCount.Text = "Sales Target Vs Actual For This Month : 0";
            ReportBO rptBo = new ReportBO();
            rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dtManager = rptBo.DashboardManager();
            grdViewManager.DataSource = dtManager;
            grdViewManager.DataBind();
            if (dtManager != null)
            {
                if (dtManager.Rows.Count > 0)
                {
                    lblRecCount.Text = "Sales Target Vs Actual For This Month : " + dtManager.Rows.Count.ToString();
                }
            }
        }

        public void FillACC()
        {
            lblRecCount.Text = "POD Overview For This Month : 0";
            ReportBO rptBo = new ReportBO();
            rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dtAccount = rptBo.DashboardAccounts();
            grdViewAccount.DataSource = dtAccount;
            grdViewAccount.DataBind();
            if (dtAccount != null)
            {
                if (dtAccount.Rows.Count > 0)
                {
                    lblRecCount.Text = "POD Overview For This Month : " + dtAccount.Rows.Count.ToString();
                }
            }
        }

        public void resetbutton(Button btn)
        {
            btn.BackColor = System.Drawing.Color.FromName("#3A66AF");
            btn.ForeColor = System.Drawing.Color.White;
            btn.BorderStyle = BorderStyle.None;
            //btn.Attributes.Clear();
            //btn.Attributes.Add("class", "button");
        }

        public void enablebutton(string GridName,bool IsDefault)
        {
            switch (GridName)
            {
                case "grdViewFUP":
                    btnFUP.Visible = true;
                    resetbutton(btnFUP);
                    if (IsDefault)
                    {
                        btn_Click(btnFUP, EventArgs.Empty);
                    }
                    break;
                case "grdEwaydBill":
                    btnEBO.Visible = true;
                    resetbutton(btnEBO);
                    if (IsDefault)
                    {
                        btn_Click(btnEBO, EventArgs.Empty);
                    }
                    break;
                case "grdSuplBill":
                    btnPOD.Visible = true;
                    resetbutton(btnPOD);
                    if (IsDefault)
                    {
                        btn_Click(btnPOD, EventArgs.Empty);
                    }
                    break;
                case "grdViewVSU":
                    btnVSU.Visible = true;
                    resetbutton(btnVSU);
                    if (IsDefault)
                    {
                        btn_Click(btnVSU, EventArgs.Empty);
                    }
                    break;
                case "grdViewManager":
                    btnTVA.Visible = true;
                    resetbutton(btnTVA);
                    if (IsDefault)
                    {
                        btn_Click(btnTVA, EventArgs.Empty);
                    }
                    break;
                case "grdViewAccount":
                    btnACC.Visible = true;
                    resetbutton(btnACC);
                    if (IsDefault)
                    {
                        btn_Click(btnACC, EventArgs.Empty);
                    }
                    break;
                default:
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdSuplBill);
            scrp1.RegisterPostBackControl(grdViewManager);
            if (!IsPostBack)
            {
                btnFUP.Visible = false;
                btnEBO.Visible = false;
                btnPOD.Visible = false;
                btnVSU.Visible = false;
                btnTVA.Visible = false;
                btnACC.Visible = false;

                ReportBO rptBo = new ReportBO();
                rptBo.RoleId = long.Parse(Session["ROLEID"].ToString());
                DataTable dt = rptBo.DashboardFillAllocatedGrids();
                if (dt!=null)
                {
                    if (dt.Rows.Count>0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            enablebutton(dt.Rows[i][1].ToString(), bool.Parse(dt.Rows[i][3].ToString()));
                        }
                    }
                }                
            }
        }

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    LoadGridView();
        //}

        //private void LoadGridView()
        //{
        //    ReportBO optbo = new ReportBO();
        //    optbo.FromDate = txtFromDate.Text;
        //    optbo.ToDate = txtToDate.Text;
        //    DataTable dt = optbo.DashBoard();
        //    grdMain.DataSource = dt;
        //    grdMain.DataBind();
        //}

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells[0].Text.ToLower() == "total")
            {
                e.Row.BackColor = System.Drawing.Color.Yellow;
                e.Row.Font.Bold = true;
            }
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        }

        protected void grdViewFUP_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "ShowLR")
            {
                lblViewLR.Text = "Pending LR For Freight Updation of '" + keys["BranchName"].ToString() + "' : " + keys["TotalRecords"].ToString();
                LRBO OptBO = new LRBO();
                OptBO.BranchID = long.Parse(keys["BranchId"].ToString());
                DataTable dt = OptBO.LRFreightUpdationCount_ShowLR();
                grdViewFUP_LR.DataSource = dt;
                grdViewFUP_LR.DataBind();
                popFUP_ShowLR.Show();
            }
        }

        protected void grdViewVSU_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "ShowLR")
            {
                lblViewLR.Text = "Pending LR For Status Updation of '" + keys["BranchName"].ToString() + "' : " + keys["TotalRecords"].ToString();
                LRBO OptBO = new LRBO();
                OptBO.BranchID = long.Parse(keys["BranchId"].ToString());
                DataTable dt = OptBO.LRStatusUpdationCount_ShowLR();
                grdViewFUP_LR.DataSource = dt;
                grdViewFUP_LR.DataBind();
                popFUP_ShowLR.Show();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            popFUP_ShowLR.Hide();
        }

        protected void grdSuplBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "PostPOD" || e.CommandName == "Approval" || e.CommandName == "Bill")
            {
                ReportBO rptBo = new ReportBO();
                rptBo.BranchId = long.Parse(keys["BranchId"].ToString());
                rptBo.type = e.CommandName;
                DataTable dt = rptBo.GetPendingDashboard_Link();

                if (dt.Rows.Count > 0)
                {
                    string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + e.CommandName + " Pending LR_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

                    ExportToExcel(dt, filename);
                }
            }
        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:P1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:P1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        private void ExportToExcelPendingPOD(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["D:D"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:S1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:S1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void grdEwaydBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DateTime.Parse(DateTime.Parse(e.Row.Cells[3].Text).ToString("dd/MM/yyyy")) <= DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")))
                {
                    //e.Row.Font.Bold = true;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[1].Text = "**" + e.Row.Cells[1].Text;
                    e.Row.ToolTip = e.Row.Cells[2].Text;
                    //e.Row.Font.Size = 20;
                }
            }
        }

        protected void grdViewManager_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "PendingPOD")
            {
                ReportBO rptBo = new ReportBO();
                rptBo.BranchId = long.Parse(keys["BranchId"].ToString());
                if (rptBo.BranchId==999999)
                {
                    return;
                }
                DataTable dt = rptBo.DashboardManagerExcel();

                if (dt.Rows.Count > 0)
                {
                    string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + "PendingPOD.xlsx");

                    ExportToExcelPendingPOD(dt, filename);
                }
            }
        }        

        protected void btn_Click(object sender, EventArgs e)
        {
            dvFUP.Visible = false;
            dvEWO.Visible = false;
            dvPOD.Visible = false;
            dvVSU.Visible = false;
            dvTVA.Visible = false;
            dvACC.Visible = false;

            resetbutton(btnFUP);
            resetbutton(btnEBO);
            resetbutton(btnPOD);
            resetbutton(btnVSU);
            resetbutton(btnTVA);
            resetbutton(btnACC);

            Button btn = (Button)sender;
            btn.BackColor = System.Drawing.Color.FromName("#c5d4ed");
            btn.BorderStyle = BorderStyle.Solid;
            btn.BorderColor = System.Drawing.Color.Red;
            btn.ForeColor = System.Drawing.Color.Black;
            btn.BorderWidth = 5;

            //btn.Attributes.Clear();
            //btn.Attributes.Remove("class");

            //btn.Attributes.Add("font-family","Arial");
            //btn.Attributes.Add("font-size", "13px");
            //btn.Attributes.Add("margin", "2px 0 2px 2px");
            //btn.Attributes.Add("text-transform", "capitalize");
            //btn.Attributes.Add("background-color", "#c5d4ed");
            //btn.Attributes.Add("color", "Black");
            //btn.Attributes.Add("border", "10px");
            //btn.Attributes.Add("border-color", "#3A66AF");
            //btn.Attributes.Add("box-shadow", " 0 0 20px 5px rgba(0,0,0,0.5)");
            //btn.Attributes.Remove("class");
            //btn.Attributes.Add("class", "buttonClickDashboard");           

            switch (btn.ID.ToLower())
            {
                case "btnfup":
                    dvFUP.Visible = true;
                    FillFUP();
                    break;
                case "btnebo":
                    dvEWO.Visible = true;
                    FillEWO();
                    break;
                case "btnpod":
                    dvPOD.Visible = true;
                    FillPOD();
                    break;
                case "btnvsu":
                    dvVSU.Visible = true;
                    FillVSU();
                    break;
                case "btntva":
                    dvTVA.Visible = true;
                    FillTVA();
                    break;
                case "btnacc":
                    dvACC.Visible = true;
                    FillACC();
                    break;
                default:
                    break;
            }

        }

        protected void grdViewManager_RowDataBound(object sender, GridViewRowEventArgs e)
        {
             if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Text.Contains("ZZZZ_"))
                {
                    e.Row.Font.Bold = true;
                    e.Row.BackColor = System.Drawing.Color.Olive;
                    e.Row.Cells[0].Text = e.Row.Cells[0].Text.Replace("ZZZZ_", "");
                }                
            }            
        }
    }
}