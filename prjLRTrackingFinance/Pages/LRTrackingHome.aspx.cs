﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRTrackingHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdSuplBill);
            if (!IsPostBack)
            {
                LRBO OptBO = new LRBO();
                OptBO.BranchID = 0;// long.Parse(Session["BranchID"].ToString());
                OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
                DataTable dt = OptBO.LRFreightUpdationCount();
                grdViewFUP.DataSource = dt;
                grdViewFUP.DataBind();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        lblLRFUP.Text = dt.AsEnumerable().Sum(row => row.Field<int>("TotalRecords")).ToString();
                    }
                }
                DataTable dtStatus = OptBO.LRStatusUpdationCount();
                grdViewVSU.DataSource = dtStatus;
                grdViewVSU.DataBind();
                if (dtStatus != null)
                {
                    if (dtStatus.Rows.Count > 0)
                    {
                        lblLRVSU.Text = dtStatus.AsEnumerable().Sum(row => row.Field<int>("TotalRecords")).ToString();
                    }
                }
                ReportBO rptBo = new ReportBO();
                rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
                DataTable dtPending = rptBo.GetPendingDashboard();
                grdSuplBill.DataSource = dtPending;
                grdSuplBill.DataBind();
                
                tblDashboard.Visible = false;
                 
                // due Eway Bills

                DataTable dtewaybill = rptBo.GetDueEwayBillsDashboard();
                if (dtewaybill != null)
                {
                    if (dtewaybill.Rows.Count > 0)
                    {
                        lblDueEwayBillCnt.Text = dtewaybill.Rows.Count.ToString();
                    }
                }
                
                grdEwaydBill.DataSource = dtewaybill;
                grdEwaydBill.DataBind();

                DataTable dtManager = rptBo.DashboardManager();
                grdViewManager.DataSource = dtManager;
                grdViewManager.DataBind();

                DataTable dtAccount = rptBo.DashboardAccounts();
                grdViewAccount.DataSource = dtAccount;
                grdViewAccount.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void LoadGridView()
        {
            ReportBO optbo = new ReportBO();
            optbo.FromDate = txtFromDate.Text;
            optbo.ToDate = txtToDate.Text;
            DataTable dt = optbo.DashBoard();
            grdMain.DataSource = dt;
            grdMain.DataBind();
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells[0].Text.ToLower() == "total")
            {
                e.Row.BackColor = System.Drawing.Color.Yellow;
                e.Row.Font.Bold = true;
            }
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        }

        protected void grdViewFUP_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "ShowLR")
            {
                lblViewLR.Text="Pending LR For Freight Updation of '"+ keys["BranchName"].ToString()+"' : "+keys["TotalRecords"].ToString();
                LRBO OptBO = new LRBO();
                OptBO.BranchID = long.Parse(keys["BranchId"].ToString());
                DataTable dt = OptBO.LRFreightUpdationCount_ShowLR();
                grdViewFUP_LR.DataSource = dt;
                grdViewFUP_LR.DataBind();
                popFUP_ShowLR.Show();
            }
        }

        protected void grdViewVSU_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "ShowLR")
            {
                lblViewLR.Text = "Pending LR For Status Updation of '" + keys["BranchName"].ToString() + "' : " + keys["TotalRecords"].ToString();
                LRBO OptBO = new LRBO();
                OptBO.BranchID = long.Parse(keys["BranchId"].ToString());
                DataTable dt = OptBO.LRStatusUpdationCount_ShowLR();
                grdViewFUP_LR.DataSource = dt;
                grdViewFUP_LR.DataBind();
                popFUP_ShowLR.Show();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            popFUP_ShowLR.Hide();
        }

        protected void grdSuplBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "PostPOD" || e.CommandName == "Approval" || e.CommandName == "Bill")
            {
                ReportBO rptBo = new ReportBO();               
                rptBo.BranchId = long.Parse(keys["BranchId"].ToString());
                rptBo.type = e.CommandName;
                DataTable dt = rptBo.GetPendingDashboard_Link();

                if (dt.Rows.Count > 0)
                {
                    string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + e.CommandName + " Pending LR_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

                    ExportToExcel(dt, filename);
                }
            }
        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:P1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:P1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }
                
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        private void ExportToExcelPendingPOD(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["D:D"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:S1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:S1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void grdEwaydBill_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DateTime.Parse(DateTime.Parse(e.Row.Cells[3].Text).ToString("dd/MM/yyyy")) <= DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")))
                {
                    //e.Row.Font.Bold = true;
                    e.Row.ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[1].Text = "**" + e.Row.Cells[1].Text;
                    e.Row.ToolTip = e.Row.Cells[2].Text;
                    //e.Row.Font.Size = 20;
                }
            }
        }

        protected void grdViewManager_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "PendingPOD")
            {
                ReportBO rptBo = new ReportBO();
                rptBo.BranchId = long.Parse(keys["BranchId"].ToString());
                DataTable dt = rptBo.DashboardManagerExcel();

                if (dt.Rows.Count > 0)
                {
                    string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + "PendingPOD_" + keys["BranchName"].ToString() + ".xlsx");

                    ExportToExcelPendingPOD(dt, filename);
                }
            }
        }
    }
}
