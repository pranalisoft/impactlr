﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using System.Net;
using System.Web.Script.Serialization;
using RestSharp;
using RestSharp.Authenticators;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class TestWhatsApp : System.Web.UI.Page
    {
        private void FillComboSupplier()
        {
            cmbSupl.Items.Clear();
            cmbSupl.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierBO OptBO = new SupplierBO();
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupl.DataSource = dt;
            cmbSupl.DataTextField = "Name";
            cmbSupl.DataValueField = "Srl";
            cmbSupl.DataBind();
            cmbSupl.SelectedIndex = 0;
        }

        private void FillComboLR()
        {
            cmbLR.Items.Clear();
            cmbLR.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            OptBO.SupplierID = cmbSupl.SelectedIndex == null ? 0 : long.Parse(cmbSupl.SelectedValue);
            dt = OptBO.FillPaymentForWhatsApp();
            cmbLR.DataSource = dt;
            cmbLR.DataTextField = "PaymentNo";
            cmbLR.DataValueField = "PaymentNo";
            cmbLR.DataBind();
            cmbLR.SelectedIndex = 0;
        }

        private void SavePdfVoucher(string refNo)
        {

            string tranDate = txtDate.Text;
            string transporter = cmbSupl.SelectedItem.Text;
            string paymentMode = "Cash";
            string chqNo = "";
            string chqDate = "";

            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher ds = new prjLRTrackerFinanceAuto.Datasets.DsPaymentVoucher();
            OptBO.RefNo = refNo;
            DataTable dt = OptBO.GetPaymentVoucherData();
            string fileName = Server.MapPath("~\\Include\\Common\\images\\WhatsappDocs\\" + refNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/PaymentAdvice.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, tranDate);
            RptDoc.SetParameterValue(1, transporter);
            RptDoc.SetParameterValue(2, paymentMode);
            RptDoc.SetParameterValue(3, chqNo);
            RptDoc.SetParameterValue(4, chqDate);

            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
            //return fileName;
            //Session["FilePath"] = fileName;
            RptDoc.Close();
            RptDoc.Dispose();
            GC.Collect();
            //Response.Redirect("ViewFile.aspx", false);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
        }

        private void SendMessege(string SupplierName, string RefNo, string ContactPersonName, string Fromdate, string ToDate, string PhoneNo)
        {
            string filePaths = "";
            //filePaths = Server.MapPath("~\\Downloads\\" + RefNo + ".pdf");
            filePaths = "http://45.114.142.166/LRTrackerAuto/Include/Common/images/WhatsappDocs/" + RefNo + ".pdf";
            string accessKey = ConfigurationManager.AppSettings["WatiAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["WatiBaseUrl"].ToString();
            string paymentadvice1 = ConfigurationManager.AppSettings["WatiPaymentTemplate"].ToString();
            var client = new RestClient(baseAddress + "/api/v1/sendTemplateMessage?whatsappNumber=" + PhoneNo);
            //client.Authenticator = new HttpBasicAuthenticator("business@ash-logistics.com", "Valid@2023");
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", accessKey);
            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("application/json", "{\"parameters\":[{\"name\":\"filepath\",\"value\":\"" + filePaths + "\"},{\"name\":\"name\",\"value\":\"" + ContactPersonName + "\"},{\"name\":\"companyname\",\"value\":\"" + SupplierName + "\"},{\"name\":\"fromdate\",\"value\":\"" + Fromdate + "\"},{\"name\":\"todate\",\"value\":\"" + ToDate + "\"}],\"template_name\":\"paymentadvice1\",\"broadcast_name\":\"Ash Logistics\"}", ParameterType.RequestBody);
            var response = client.Execute(request);
            if (response.Content.Contains("result\":true"))
            {
                MsgPopUp.ShowModal("Success!!!!", CommonTypes.ModalTypes.Alert);
            }
            else
            {
                MsgPopUp.ShowModal("Failed!!!!", CommonTypes.ModalTypes.Error);
            }
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                    //    LoadGridView();
                    //    MsgPanel.Message = "Record(s) deleted successfully.";
                    //    MsgPanel.DispCode = 1;
                    //    txtFromDate.Focus();
                    //}
                    txtDate.Focus();
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    txtDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            if (!IsPostBack)
            {
                FillComboSupplier();
                txtDate.Focus();
            }
        }

        protected void cmbSupl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmbLR.DataSource = null;
                cmbLR.DataBind();
                cmbLR.Items.Clear();
                FillComboLR();
                cmbLR.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Payment from Supplier";                   
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;
                    string refNoforemail = cmbLR.SelectedItem.Text;
                    SavePdfVoucher(refNoforemail);
                    SupplierPaymentBO OptBO = new SupplierPaymentBO();
                    OptBO.refNo = refNoforemail;
                    DataTable dt = OptBO.GetPaymentDetailsForWhatsApp();
                    if (dt != null & dt.Rows.Count > 0)
                    {
                        string ContactPersonName = dt.Rows[0]["ContactPersonName"].ToString().Trim();
                        string fromDate = DateTime.Parse(dt.Rows[0]["FromDate"].ToString()).ToString("dd/MMM/yyyy");
                        string toDate = DateTime.Parse(dt.Rows[0]["ToDate"].ToString()).ToString("dd/MMM/yyyy");
                        SendMessege(cmbSupl.SelectedItem.Text, refNoforemail, ContactPersonName, fromDate, toDate, "91" + txtDate.Text);
                    }

                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    FillComboSupplier();
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.None:
                    break;
                default:
                    break;
            }
        }
    }
}