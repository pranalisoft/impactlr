﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PendingPODReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            RegisterDateTextBox();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\PendingPOD_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            LRBO OptBO = new LRBO();
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = OptBO.PendingPODReport();

            ExportToExcel(dt, filename);

        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["D:D"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";               
                objWorksheet.Cells["A1:BZ1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }
      

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void LoadGridView()
        {
            LRBO OptBO = new LRBO();
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = OptBO.PendingPODReport();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (Session["Item_Expr"] != null)
                    dataView.Sort = Session["Item_Expr"].ToString() + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        protected void grdMain_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Session["Item_Sort"] == null)
            {
                Session["Item_Sort"] = "ASC";
            }
            else
            {
                if (Session["Item_Sort"].ToString() == "ASC")
                    Session["Item_Sort"] = "DESC";
                else
                    Session["Item_Sort"] = "ASC";
            }
            Session["Item_Expr"] = e.SortExpression;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}