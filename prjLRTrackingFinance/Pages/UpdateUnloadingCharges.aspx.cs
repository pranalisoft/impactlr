﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateUnloadingCharges : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            SetPanelMsg("", false, 1);
            if (txthdLRId.Value == "0")
            {
                 SetPanelMsg("Please Select Valid LR No. from the List", true, 0);
                 return;
            }
            LRBO optbo = new LRBO();
            optbo.ID = long.Parse(txthdLRId.Value);
            optbo.UnloadingCharges = decimal.Parse(txtUnloadingCharges.Text);

            long cnt = optbo.UpdateUnloadingCharges();
            if (cnt > 0)
            {
                ClearInfoPanel();
                txtUnloadingCharges.Text = "";
                txtLRNoUnloading.Text = "";
                SetPanelMsg("Record Updated Successfully !!", true, 1);
                txtLRNoUnloading.Focus();
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        private void LoadGridViewDetails(long ID)
        {
           
        }

        protected void grdViewItemDetls_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
        }

        protected void btnSearchUnloadingLR_Click(object sender, EventArgs e)
        {
            ClearInfoPanel();
            SetPanelMsg("", false, 1);
            LRBO OptBO = new LRBO();
            OptBO.LRNo = txtLRNoUnloading.Text.Trim();
            DataTable dt = OptBO.GetLRHeaderByNoForUnloading();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    txthdLRId.Value = dt.Rows[0]["ID"].ToString();
                    lblLRNo.Text = txtLRNoUnloading.Text.Trim();
                    lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                    lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                    lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                    lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                    lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                    lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                    lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                    lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                    lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                    lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                    lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                    lblType.Text = dt.Rows[0]["LoadType"].ToString();
                    lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                    lblDriverDetails.Text = dt.Rows[0]["DriverDetails"].ToString();
                    lblDriverMobNo.Text = dt.Rows[0]["DriverNumber"].ToString();
                    txtUnloadingCharges.Text = dt.Rows[0]["UnloadingCharges"].ToString();
                    if (decimal.Parse(dt.Rows[0]["UnloadingChargesPaid"].ToString()) > 0)
                    {
                        txtUnloadingCharges.Enabled = false;
                    }
                    else
                    {
                        txtUnloadingCharges.Enabled = true;
                    }
                }
                else
                {
                    txthdLRId.Value = "0";
                    SetPanelMsg("Either LR No. is Invalid Or Invoice is generated for this LR", true, 0);
                    txtLRNoUnloading.Focus();
                    return;
                }
            }
            //cmbStatus.Focus();
        }

        private void ClearInfoPanel()
        {
            txthdLRId.Value = "0";
            lblLRNo.Text = "";
            lblDate.Text = "";
            lblConsigner.Text = "";
            lblFrom.Text = "";
            lblTo.Text = "";
            lblVehicleNo.Text = "";
            lblPackages.Text = "";
            lblWeight.Text = "";
            lblChargeableWeight.Text = "";
            lblInvoiceNo.Text = "";
            lblInvoiceValue.Text = "";
            lblType.Text = "";
            lblRemarks.Text = "";
            lblDriverDetails.Text = "";
            lblDriverMobNo.Text = "";
        }
    }
}