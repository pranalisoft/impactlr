﻿<%@ Page Title="Supplier Payable Approval" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" EnableEventValidation="false" CodeBehind="SupplierPayableApproval.aspx.cs"
    Inherits="prjLRTrackerFinanceAuto.Pages.SupplierPayableApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Supplier Payable Approval
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="130px">
                                <asp:Label ID="Label18" Text="Billing Company" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbBillingCompany" runat="server" CssClass="NormalTextBold"
                                    TabIndex="1" Width="250px" AutoPostBack="True" 
                                    onselectedindexchanged="cmbBillingCompany_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="80px">
                            </td>
                            <td colspan="5">
                                <asp:Button ID="btnShow" runat="server" Text="Show" CssClass="button" OnClick="btnShow_Click"
                                    TabIndex="2" />&nbsp;&nbsp;
                                <asp:Button ID="btnGenerate" runat="server" Text="Generate" CssClass="button" OnClick="btnGenerate_Click"
                                    TabIndex="3" Visible="false"/>&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" OnClick="btnExcel_Click"
                                    TabIndex="4" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>                                   
                            </td>                            
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="false"
                                    ShowFooter="true" PageSize="5" CssClass="grid" AllowPaging="false" AllowSorting="true"
                                    DataKeyNames="SupplierId,SupplierName,CreditDays,Slot_10Days,Slot_30Days,Slot_60Days,Slot_61Days,Total,PaymentRecommended,MDApproved,PaidAmount,UnBilledAmount,UnBilledAmountPODNotRcvd,NotDueAmount,HoldAmount,BilledNotDueAmount" EmptyDataText="No Records Found." OnRowCreated="grdMain_RowCreated"
                                    OnRowDataBound="grdMain_RowDataBound" OnSorting="grdMain_Sorting" OnRowCommand="grdMain_RowCommand">
                                    <RowStyle Height="10px" />
                                    <Columns>
                                      <asp:ButtonField ButtonType="Link" CommandName="SuplInfo" DataTextField="SupplierName"
                                            ControlStyle-Font-Underline="false" HeaderText="Supplier Name" SortExpression="SupplierName"/>
                                      <%--  <asp:BoundField DataField="SupplierName" HeaderText="Supplier Name" SortExpression="SupplierName" />--%>
                                        <asp:BoundField DataField="CreditDays" HeaderText="Credit Days" SortExpression="CreditDays"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="60px" />
                                              <asp:BoundField DataField="UnBilledAmount" HeaderText="Unbilled POD Rcvd" SortExpression="UnBilledAmount"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                             <asp:BoundField DataField="UnBilledAmountPODNotRcvd" HeaderText="Unbilled POD Not Rcvd" SortExpression="UnBilledAmountPODNotRcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                              <asp:BoundField DataField="NotDueAmount" HeaderText="Approved" SortExpression="NotDueAmount"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" /> 
                                              <asp:BoundField DataField="BilledNotDueAmount" HeaderText="Billed Not Due" SortExpression="BilledNotDueAmount"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />                                            
                                        <asp:BoundField DataField="Slot_10Days" HeaderText="10 Days" SortExpression="Slot_10Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_30Days" HeaderText="11 To 30 Days" SortExpression="Slot_30Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_60Days" HeaderText="31 To 60 Days" SortExpression="Slot_60Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Slot_61Days" HeaderText="61 & Above" SortExpression="Slot_61Days"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <%-- <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" ItemStyle-HorizontalAlign="Right"
                                            ItemStyle-Width="100px" />--%>
                                        <asp:TemplateField HeaderText="Total" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTotalAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    CssClass="NormalTextBold" MaxLength="10" Width="93%" AutoPostBack="false" Enabled="false"
                                                    Text='<%#Eval("Total").ToString().Replace(".00","") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Hold" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtHoldAmt" Style="text-align: right" TabIndex="9" runat="server" ForeColor="Red"
                                                    CssClass="NormalTextBold" MaxLength="10" Width="93%" AutoPostBack="false" Enabled="false"
                                                    Text='<%#Eval("HoldAmount").ToString().Replace(".00","") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Recommended" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPaymentRecommended" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="93%" AutoPostBack="false" CssClass="NormalTextBold" Text='<%#Eval("PaymentRecommended").ToString().Replace(".00","") %>'
                                                    onchange="calculatePaymentRecommended();"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterPRAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtPaymentRecommended" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExPRAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtPaymentRecommended" Display="None" SetFocusOnError="True"
                                                    ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExPRAmtCall" runat="server" Enabled="True" TargetControlID="regExPRAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompPRAmt" runat="server" ErrorMessage="Payment Recommended cannot be greater than Total Amount"
                                                    ControlToCompare="txtTotalAmt" ControlToValidate="txtPaymentRecommended" Display="None"
                                                    SetFocusOnError="true" ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:ValidatorCalloutExtender ID="CompPRAmtCall" runat="server" Enabled="True" TargetControlID="CompPRAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtPRTotal" Style="text-align: right" TabIndex="9" BackColor="#3A66AF" ForeColor="White" Font-Bold="true"
                                                    runat="server" MaxLength="10" Width="93%" Enabled="false" CssClass="NormalTextBold"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Approved By MD/Director" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMDAmount" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="93%" AutoPostBack="false" CssClass="NormalTextBold" Text='<%#Eval("MDApproved").ToString().Replace(".00","") %>'
                                                    onchange="calculateMDAmount();"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterMDAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtMDAmount" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExMDAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtMDAmount" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExMDAmtCall" runat="server" Enabled="True" TargetControlID="regExMDAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompMDAmt" runat="server" ErrorMessage="Amount cannot be greater than Total Amount"
                                                    ControlToCompare="txtTotalAmt" ControlToValidate="txtMDAmount" Display="None"
                                                    SetFocusOnError="true" ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:ValidatorCalloutExtender ID="CompMDAmtCall" runat="server" Enabled="True" TargetControlID="CompMDAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtMDTotal" Style="text-align: right" TabIndex="9" runat="server" BackColor="#3A66AF" ForeColor="White" Font-Bold="true"
                                                    MaxLength="10" Width="93%" Enabled="false" CssClass="NormalTextBold"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>                           
                            <td align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click"
                                    TabIndex="2" />                             
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnShow" />
            <asp:AsyncPostBackTrigger ControlID="btnGenerate" />
            <asp:AsyncPostBackTrigger ControlID="cmbBillingCompany" 
                EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function calculatePaymentRecommended() {
            var gv = document.getElementById("<%= grdMain.ClientID %>");
            var tb = gv.getElementsByTagName("input");
           
            var Total = 0;       

            for (var i = 0; i < tb.length; i++) {                

                if (tb[i].type == "text" && tb[i].id.indexOf('_txtPaymentRecommended') >= 0) {
                    if (parseFloat(tb[i].value) > 0) {
                        Total = parseFloat(Total) + parseFloat(tb[i].value);                       
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_txtPRTotal') >= 0) {
                        tb[i].value = parseFloat(Total).toFixed(2);
                }              
            }
        }

        function calculateMDAmount() {
            var gv = document.getElementById("<%= grdMain.ClientID %>");
            var tb = gv.getElementsByTagName("input");

            var Total = 0;

            for (var i = 0; i < tb.length; i++) {

                if (tb[i].type == "text" && tb[i].id.indexOf('_txtMDAmount') >= 0) {
                    if (parseFloat(tb[i].value) > 0) {
                        Total = parseFloat(Total) + parseFloat(tb[i].value);
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_txtMDTotal') >= 0) {
                    tb[i].value = parseFloat(Total).toFixed(2);
                }
            }
        }
    </script>
</asp:Content>
