﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="UpdateLRStatusFreightTiger.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateLRStatusFreightTiger" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div>
                <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                    <asp:Label ID="lblMsg" Text="" runat="server" />
                </asp:Panel>
            </div>
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <div style="height: 25px; width: 98%; margin-left: 12px; margin-top: 12px; background-color: #3A66AF;
                        color: white; font-size: 16px; font-weight: bold; text-align: center">
                        &nbsp;&nbsp; Update LR Status For In Transit LRs
                    </div>
                    <table style="border: 1px solid black; width: 90%">
                        <tr>
                            <td>
                                Click Update Button to Update the LR Status of All In Transit LR(s).
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="btnSave" Text="Update" CssClass="button" Width="70px"
                                    OnClick="btnSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
             <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
