﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Text;
using System.Net;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateLRStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtHiddenId.Value = Session["StatusLRID"].ToString();
                txtHiddenTripId.Value = Session["TripId"].ToString();
                if (string.IsNullOrEmpty(txtHiddenTripId.Value))
                {
                    txtTripId.Enabled = true;
                    btnUpdateTripId.Visible = true;
                }
                else
                {
                    txtTripId.Enabled = false;
                    txtTripId.Text = txtHiddenTripId.Value;
                    btnUpdateTripId.Visible = false;
                }
                txtHiddendetailId.Value = "0";
                LoadGridViewDetails(long.Parse(txtHiddenId.Value));

                LRBO OptBO = new LRBO();
                OptBO.LRNo = Session["StatusLRNo"].ToString();
                DataTable dt = OptBO.GetLRHeaderByNo();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        lblLRNo.Text = Session["StatusLRNo"].ToString();
                        lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                        lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                        lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                        lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                        lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                        lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                        lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                        lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                        lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                        lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                        lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                        lblType.Text = dt.Rows[0]["LoadType"].ToString();
                        lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                        lblDriverDetails.Text = dt.Rows[0]["DriverDetails"].ToString();
                        lblDriverMobNo.Text = dt.Rows[0]["DriverNumber"].ToString();
                    }
                }
                cmbStatus.Focus();

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            DataKey keys = grdViewItemDetls.DataKeys[0];
            DateTime Prevtrandate = DateTime.Parse(keys["TranDateTime"].ToString());
            DateTime Curtrandate = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text + ":00");

            if (long.Parse(txtHiddenId.Value)==0 && Curtrandate < Prevtrandate)
            {
                SetPanelMsg("Current Status Datetime Cannot Be Less Than Previous Datetime.", true, 0);
                txtDate_CalendarExtender.Focus();
                return;
            }

            if (keys["TransitStatus"].ToString().ToLower() == "vehicle placed")
            {
                if (cmbStatus.SelectedItem.Text.ToLower() == "in-transit" || cmbStatus.SelectedItem.Text.ToLower() == "reported" || cmbStatus.SelectedItem.Text.ToLower() == "waiting for unload" || cmbStatus.SelectedItem.Text.ToLower() == "delivered")
                {
                    SetPanelMsg("You cannot directly set Status as " + cmbStatus.SelectedItem.Text + ". Vehicle Left Stage is Pending.", true, 0);
                    cmbStatus.Focus();
                    return;
                }
            }

            if (keys["TransitStatus"].ToString().ToLower() == "vehicle left")
            {
                if (cmbStatus.SelectedItem.Text.ToLower() == "vehicle left")
                {
                    SetPanelMsg("Vehicle Left Stage is already done.", true, 0);
                    cmbStatus.Focus();
                    return;
                }

                if (cmbStatus.SelectedItem.Text.ToLower() == "reported" || cmbStatus.SelectedItem.Text.ToLower() == "waiting for unload" || cmbStatus.SelectedItem.Text.ToLower() == "delivered")
                {
                    SetPanelMsg("You cannot directly set Status as " + cmbStatus.SelectedItem.Text + ". In-Transit Stage is Pending.", true, 0);
                    cmbStatus.Focus();
                    return;
                }
            }

            if (keys["TransitStatus"].ToString().ToLower() == "in-transit")
            {
                if (cmbStatus.SelectedItem.Text.ToLower() == "vehicle left")
                {
                    SetPanelMsg("Vehicle Left Stage is already done.", true, 0);
                    cmbStatus.Focus();
                    return;
                }

                if (cmbStatus.SelectedItem.Text.ToLower() == "waiting for unload" || cmbStatus.SelectedItem.Text.ToLower() == "delivered")
                {
                    SetPanelMsg("You cannot directly set Status as " + cmbStatus.SelectedItem.Text + ". Reported Stage is Pending.", true, 0);
                    cmbStatus.Focus();
                    return;
                }
            }

            if (keys["TransitStatus"].ToString().ToLower() == "reported" || keys["TransitStatus"].ToString().ToLower() == "waiting for unload")
            {
                if (cmbStatus.SelectedItem.Text.ToLower() == "vehicle left" || cmbStatus.SelectedItem.Text.ToLower() == "in-transit")
                {
                    SetPanelMsg(cmbStatus.SelectedItem.Text + " Stage is already done.", true, 0);
                    cmbStatus.Focus();
                    return;
                }
            }     

            //if (keys["TransitStatus"].ToString().ToLower() == "vehicle left")
            //{
            //    if (cmbStatus.SelectedItem.Text.ToLower() == "vehicle left")
            //    {
            //        SetPanelMsg("Vehicle Left Stage is already done.", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
                
            //}

            //if (keys["TransitStatus"].ToString().ToLower() == "vehicle placed")
            //{
            //    if (cmbStatus.SelectedItem.Text.ToLower() == "in-transit")
            //    {
            //        SetPanelMsg("You cannot directly set Status as in-transit. Vehicle Left Stage is Pending.", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
            //}


            //if (keys["TransitStatus"].ToString().ToLower() == "waiting for unload" || keys["TransitStatus"].ToString().ToLower() == "reported")
            //{
            //    if (cmbStatus.SelectedValue=="T")
            //    {
            //        SetPanelMsg("You can only set Status as waiting for unload or delivered", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
            //}

            //if (keys["TransitStatus"].ToString().ToLower() == "in-transit")
            //{
            //    if (cmbStatus.SelectedValue == "U")
            //    {
            //        SetPanelMsg("First Select Status as  Reporting and Enter Reporting Date", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
            //}

            //if (keys["TransitStatus"].ToString().ToLower() == "waiting for unload")
            //{
            //    if (cmbStatus.SelectedValue == "R")
            //    {
            //        SetPanelMsg("You can only set Status as waiting for unload or delivered", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
            //}

            //if (keys["TransitStatus"].ToString().ToLower() == "in-transit" || keys["TransitStatus"].ToString().ToLower() == "reported")
            //{
            //    if (cmbStatus.SelectedValue == "D")
            //    {
            //        SetPanelMsg("You cannot directly set Status as Delivered. Unloading Stage is Pending.", true, 0);
            //        cmbStatus.Focus();
            //        return;
            //    }
            //}

            ////if (cmbStatus.SelectedItem.Text.ToLower() == "vehicle left")
            ////{
            ////    LRBO optbo1 = new LRBO();
            ////    optbo1.placementId = long.Parse(Session["StatusPlacementId"].ToString());
            ////    long cnt1 = optbo1.UpdateFulfillment();
            ////}
              
           
            LRBO optbo = new LRBO();
            optbo.ID = long.Parse(txtHiddenId.Value);
            optbo.TranDateTime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") +" "+ ddlHH.Text+":"+ddlmm.Text+":00");
            optbo.Status = cmbStatus.SelectedValue;
            optbo.Remarks = txtRemarks.Text.Trim();
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            optbo.DetailID = long.Parse(txtHiddendetailId.Value);
            long cnt = optbo.InsertLRStatus();
            if (cnt>0)
            {
              
                if (cmbStatus.SelectedValue == "R")
                {
                    CloseTrip(txtHiddenTripId.Value, txtHiddenTripId.Value, DateTime.Now.ToString("yyyy-MM-dd HH:mm"), "Trip Closed For" + lblLRNo.Text);
                } 
                txtHiddendetailId.Value = "0";
                if (Session["StatusLRFromPage"] == null) Session["StatusLRFromPage"] = "";
                if (Session["StatusLRFromPage"].ToString().Trim() == "")
                {
                    Response.Redirect("LRMain.aspx");
                }
                else
                {
                    Session["StatusLRFromPage"] = "";
                    Response.Redirect("LRStatusUpdationFromTrip.aspx");
                }
            }
        }

        private void UpdateTripIdForNonTrips(long LRID, long tripId)
        {
            LRBO optbo = new LRBO();
            optbo.ID = LRID;
            optbo.TripId = tripId;
            long cnt = optbo.UpdateTripIdForNonTrips();
        }

        private void CloseTrip(string tripId, string uniqueId, string closeDateTime, string comment)
        {
            try
            {
                string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
                string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();

                using (System.Net.WebClient webClient = new System.Net.WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                    webClient.BaseAddress = baseAddress;
                    webClient.Headers.Set("accept", "application/json");
                    webClient.Headers.Set("content-type", "application/json");

                    var lrObject = new
                    {
                        trip_id = tripId,                         
                        close_date_time = closeDateTime,
                        comment = comment
                    };

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/close");
                    urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                    var result = webClient.UploadString(urlStringBuilder.ToString(), javaScriptSerializer.Serialize(lrObject));

                    var responseObject = javaScriptSerializer.DeserializeObject(result);

                    //var status = responseObject["status"];

                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Session["StatusLRFromPage"] == null) Session["StatusLRFromPage"] = "";
            if (Session["StatusLRFromPage"].ToString().Trim() == "")
            {
                Response.Redirect("LRMain.aspx");
            }
            else
            {
                Session["StatusLRFromPage"] = "";
                Response.Redirect("LRStatusUpdationFromTrip.aspx");
            }
        }

        private void LoadGridViewDetails(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataTable dt = optBO.FillLRStatus();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
            lbltotRecords.Text = dt.Rows.Count.ToString();
        }

        protected void grdViewItemDetls_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];
            if (string.IsNullOrEmpty(txtHiddenTripId.Value))
            {
                txtTripId.Enabled = true;
                btnUpdateTripId.Visible = true;
            }
            else
            {
                txtTripId.Enabled = false;
                txtTripId.Text = txtHiddenTripId.Value;
                btnUpdateTripId.Visible = false;
            }
            txtHiddendetailId.Value = "0";
            txtRemarks.Text = "";
            cmbStatus.Enabled = true;
            txtDate.Text = "";
            if (e.CommandName == "Modify")
            {
                if (Session["ROLEID"].ToString() != "1" && Session["ROLEID"].ToString() != "8")
                {
                    SetPanelMsg("Only Admin Can Modify LR Status", true, 0);
                    return;
                }
                if (keys["TransitStatus"].ToString().ToLower() == "vehicle placed")
                {
                    SetPanelMsg("Cannot Modify LR Status, Vehicle Placed done from TD.", true, 0);
                    return;
                }
                txtHiddendetailId.Value = keys["DetailID"].ToString();
                if (keys["TransitStatus"].ToString()=="In-Transit")
                    cmbStatus.SelectedValue = "T";
                else if (keys["TransitStatus"].ToString() == "Waiting For Unload")
                    cmbStatus.SelectedValue = "U";
                else if (keys["TransitStatus"].ToString() == "Delivered")
                    cmbStatus.SelectedValue = "D";
                else if (keys["TransitStatus"].ToString() == "Reported")
                    cmbStatus.SelectedValue = "R";
                else if (keys["TransitStatus"].ToString() == "Vehicle Left")
                    cmbStatus.SelectedValue = "L";

                cmbStatus.Enabled = false;
                txtRemarks.Text = keys["TransitRemarks"].ToString();
                txtDate.Text = DateTime.Parse(keys["TranDateTime"].ToString()).ToString("dd/MM/yyyy");
                string timeInHrs = DateTime.Parse(keys["TranDateTime"].ToString()).ToString("dd/MM/yyyy HH:mm").Substring(11, 2);
                string timeInMins = DateTime.Parse(keys["TranDateTime"].ToString()).ToString("dd/MM/yyyy HH:mm").Substring(14, 2);
                ddlHH.Text = timeInHrs;
                ddlmm.Text = timeInMins;
              
            }
        }

        protected void btnUpdateTripId_Click(object sender, EventArgs e)
        {
            if (txtTripId.Enabled == true)
            {
                if (!string.IsNullOrEmpty(txtTripId.Text.Trim()))
                {
                    UpdateTripIdForNonTrips(long.Parse(txtHiddenId.Value), long.Parse(txtTripId.Text));
                    txtTripId.Enabled = false;
                }

            }
        }

        
    }
}
