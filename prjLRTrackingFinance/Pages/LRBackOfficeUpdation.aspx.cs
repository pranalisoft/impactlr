﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using AjaxControlToolkit;
using System.Net.Mail;
using CrystalDecisions.CrystalReports.Engine;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRBackOfficeUpdation : System.Web.UI.Page
    {
        enum DropDownTypes
        {
            Consignee, Consigner, Origin, Destination, VehicleNo, PackingType, Description
        }     

        #region "Procedure"
      
        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void LoadGridView()
        {
            LRBO optBO = new LRBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.IsPending = false;
            DataTable dt = optBO.FillLRGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDetails(long ID)
        {
            LRBO optBO = new LRBO();
            optBO.ID = ID;
            DataTable dt = optBO.FillLRStatus();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        private void ClearView()
        {
            txtLRNo.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtChWeight.Text = string.Empty;
            txtInvoiceNo.Text = string.Empty;
            txtInvoiceValue.Text = string.Empty;
            txtInvoiceDate.Text = string.Empty;
            cmbDescription.SelectedIndex = -1;
            txtRefNo.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtDriverDetails.Text = string.Empty;
            txttotPackage.Text = string.Empty;
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            ddlHH.Text = DateTime.Now.ToString("HH");
            ddlmm.Text = DateTime.Now.ToString("mm");
            cmbBillingCompany.SelectedIndex = -1;
            txtConsigneeAddress.Text = string.Empty;
            txtConsigneeContact.Text = string.Empty;
            txtConsigneeEmail.Text = string.Empty;
            cmbFrom.SelectedIndex = 0;
            cmbTo.SelectedIndex = 0;
            txtDriverNumber.Text = string.Empty;
            txtvehicleSealNo.Text = string.Empty;
            txtCashAdvance.Text = string.Empty;
            txtFuelAdvance.Text = string.Empty;
            txtShippingDocNo.Text = string.Empty;
            txtInternalbillingNo.Text = string.Empty;
            txtEwayBillNo.Text = string.Empty;
            txtEwayBillDate.Text = string.Empty;
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
        }

        private void FillDropdown(DropDownTypes cmbtype)
        {
            LRBO OptBO = new LRBO();
            DataTable dt;
            switch (cmbtype)
            {
                case DropDownTypes.Consignee:
                    //OptBO.FillType = "S";
                    //cmbConsignee.DataSource = null;
                    //cmbConsignee.Items.Clear();
                    //cmbConsignee.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsignee.DataSource = dt;
                    //cmbConsignee.DataTextField = "Consignee";
                    //cmbConsignee.DataValueField = "Consignee";
                    //cmbConsignee.DataBind();
                    //cmbConsignee.SelectedIndex = 0;
                    break;
                case DropDownTypes.Consigner:
                    //OptBO.FillType = "C";
                    //cmbConsigner.DataSource = null;
                    //cmbConsigner.Items.Clear();
                    //cmbConsigner.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbConsigner.DataSource = dt;
                    //cmbConsigner.DataTextField = "Consigner";
                    //cmbConsigner.DataValueField = "Consigner";
                    //cmbConsigner.DataBind();
                    //cmbConsigner.SelectedIndex = 0;
                    break;
                case DropDownTypes.Origin:
                    OptBO.FillType = "O";
                    cmbFrom.DataSource = null;
                    cmbFrom.Items.Clear();
                    cmbFrom.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRCombo();
                    cmbFrom.DataSource = dt;
                    cmbFrom.DataTextField = "FromLoc";
                    cmbFrom.DataValueField = "FromLoc";
                    cmbFrom.DataBind();
                    cmbFrom.SelectedIndex = 0;
                    break;
                case DropDownTypes.Destination:
                    //OptBO.FillType = "D";
                    //cmbTo.DataSource = null;
                    //cmbTo.Items.Clear();
                    //cmbTo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbTo.DataSource = dt;
                    //cmbTo.DataTextField = "ToLoc";
                    //cmbTo.DataValueField = "ToLoc";
                    //cmbTo.DataBind();
                    //cmbTo.SelectedIndex = 0;
                    break;
                case DropDownTypes.VehicleNo:
                    OptBO.FillType = "V";
                    //cmbVehicleNo.DataSource = null;
                    //cmbVehicleNo.Items.Clear();
                    //cmbVehicleNo.Items.Add(new ListItem("", "-1"));
                    //dt = OptBO.FillLRCombo();
                    //cmbVehicleNo.DataSource = dt;
                    //cmbVehicleNo.DataTextField = "VehicleNo";
                    //cmbVehicleNo.DataValueField = "VehicleNo";
                    //cmbVehicleNo.DataBind();
                    //cmbVehicleNo.SelectedIndex = 0;
                    break;
                case DropDownTypes.Description:
                    OptBO.SearchText = string.Empty;
                    cmbDescription.DataSource = null;
                    cmbDescription.Items.Clear();
                    cmbDescription.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillLRDescriptionCombo();
                    cmbDescription.DataSource = dt;
                    cmbDescription.DataTextField = "Name";
                    cmbDescription.DataValueField = "Name";
                    cmbDescription.DataBind();
                    cmbDescription.SelectedIndex = 0;
                    break;
                case DropDownTypes.PackingType:
                    OptBO.SearchText = string.Empty;
                    cmbType.DataSource = null;
                    cmbType.Items.Clear();
                    cmbType.Items.Add(new ListItem("", "-1"));
                    dt = OptBO.FillPackingType();
                    cmbType.DataSource = dt;
                    cmbType.DataTextField = "Name";
                    cmbType.DataValueField = "Name";
                    cmbType.DataBind();
                    cmbType.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
        }

        private void FillDropDownDevice(long DeviceID = 0)
        {
            //LRBO OptBO = new LRBO();
            //DataTable dt;
            //cmbDevice.DataSource = null;
            //cmbDevice.Items.Clear();
            //cmbDevice.Items.Add(new ListItem("", "-1"));
            //OptBO.DeviceID = DeviceID;
            //dt = OptBO.FillDropDownDevice();
            //cmbDevice.DataSource = dt;
            //cmbDevice.DataTextField = "Name";
            //cmbDevice.DataValueField = "Srl";
            //cmbDevice.DataBind();
            //cmbDevice.SelectedIndex = 0;
        }

        private void FillDropDownVehicleType()
        {
            VehicleTypeBO OptBO = new VehicleTypeBO();
            DataTable dt;
            cmbVehicleType.DataSource = null;
            cmbVehicleType.Items.Clear();
            cmbVehicleType.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetVehicleTypedetails();
            cmbVehicleType.DataSource = dt;
            cmbVehicleType.DataTextField = "Name";
            cmbVehicleType.DataValueField = "Srl";
            cmbVehicleType.DataBind();
            cmbVehicleType.SelectedIndex = 0;
        }

        private void FillDropDownCustomer()
        {
            CustomerBO OptBO = new CustomerBO();
            DataTable dt;
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            OptBO.ShowAll = "Y";
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            dt = OptBO.GetCustomerDetailsUserWise();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        private void GetDetailsFromTDA(long PlacementId,long LRId,string Status="L")
        {
            LRBO optbo = new LRBO();
            optbo.placementId = PlacementId;
            optbo.Status = Status;
            optbo.ID = LRId;
            DataTable dt = optbo.GetPlacementDetailsView();
            if (dt.Rows.Count > 0)
            {
                txtVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                txtVehicleNo.Enabled = false;
                //cmbFrom.SelectedValue = dt.Rows[0]["Origin"].ToString();
                //cmbFrom_SelectedIndexChanged(cmbFrom, EventArgs.Empty);
                //cmbTo.SelectedValue = dt.Rows[0]["Destination"].ToString();
                //cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                cmbSupplier.SelectedValue = dt.Rows[0]["SupplierId"].ToString();
                cmbSupplier_SelectedIndexChanged(cmbSupplier, EventArgs.Empty);
                cmbVehicleType.SelectedValue = dt.Rows[0]["VehicleTypeId"].ToString();
                //cmbCustomer.SelectedValue = dt.Rows[0]["CustomerId"].ToString();
                txtVehiclePlacementDate.Text = DateTime.Parse(dt.Rows[0]["PlacementDate"].ToString()).ToString("dd/MM/yyyy");
                txtRate.Text = dt.Rows[0]["CustomerFreight"].ToString();
                txtFrieght.Text = dt.Rows[0]["CustomerFreight"].ToString();
                txtSuplRate.Text = dt.Rows[0]["Freight"].ToString();
                txtSupplierFreight.Text = dt.Rows[0]["Freight"].ToString();
                txtTDFuelAdvance.Text = dt.Rows[0]["TDFuelAdvance"].ToString();  
                long cnt = optbo.GetCntNonZeroLRs();
                if (cnt>0)
                {
                     txtRate.Text = "0";
                     txtFrieght.Text = "0";
                     txtSuplRate.Text = "0";
                     txtSupplierFreight.Text = "0";
                     txtTDFuelAdvance.Text = "0";
                }

                cmbSupplier.Enabled = false;
                cmbCustomer.Enabled = false;
                cmbVehicleType.Enabled = false;
                txtVehiclePlacementDate.Enabled=false;
                txtRate.Enabled=false;
                txtFrieght.Enabled=false;
                txtSuplRate.Enabled=false;
                txtSupplierFreight.Enabled = false;
                txtTDFuelAdvance.Enabled = false;

                CalculateAmount();
            }
        }

        private void FillDropDownSupplier()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void FillDropDownPetrolPump()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbPetrolPump.DataSource = null;
            cmbPetrolPump.Items.Clear();
            cmbPetrolPump.Items.Add(new ListItem("", "-1"));
            OptBO.SupplierType = 2;
            dt = OptBO.GetSuppliersByType();
            cmbPetrolPump.DataSource = dt;
            cmbPetrolPump.DataTextField = "Name";
            cmbPetrolPump.DataValueField = "Srl";
            cmbPetrolPump.DataBind();
            cmbPetrolPump.SelectedIndex = 0;
        }

        private void FillDropDownFrom()
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbFrom.DataSource = null;
            cmbFrom.Items.Clear();
            cmbFrom.Items.Add(new ListItem("", "-1"));
            OptBO.FromOrTo = "F";
            dt = OptBO.GetFromOrToLocations();
            cmbFrom.DataSource = dt;
            cmbFrom.DataTextField = "FromLoc";
            cmbFrom.DataValueField = "FromLoc";
            cmbFrom.DataBind();
            cmbFrom.SelectedIndex = 0;
        }

        private void FillDropDownDestination(string FromLoc)
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbTo.DataSource = null;
            cmbTo.Items.Clear();
            cmbTo.Items.Add(new ListItem("", "-1"));
            OptBO.FromLoc = FromLoc;
            dt = OptBO.GetToLocations();
            cmbTo.DataSource = dt;
            cmbTo.DataTextField = "ToLoc";
            cmbTo.DataValueField = "ToLoc";
            cmbTo.DataBind();
            cmbTo.SelectedIndex = 0;
        }

        private void FillDropDownConsignor()
        {
            //ConsignorBO OptBO = new ConsignorBO();
            //DataTable dt;
            //cmbConsigner.DataSource = null;
            //cmbConsigner.Items.Clear();
            //cmbConsigner.Items.Add(new ListItem("", "-1"));
            //OptBO.SearchText = string.Empty;
            //dt = OptBO.GetConsignordetails();
            //cmbConsigner.DataSource = dt;
            //cmbConsigner.DataTextField = "Name";
            //cmbConsigner.DataValueField = "Srl";
            //cmbConsigner.DataBind();
            //cmbConsigner.SelectedIndex = 0;
        }

        private void FillDropDownBillingCompany()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;
        }

        private void setCombobox(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.Text = lst.Value;
            else
            {
                lst = new ListItem(FieldName, FieldName);
                cmb.Items.Add(lst);
                cmb.Text = FieldName;
            }
        }

        private void setComboboxValue(ComboBox cmb, string FieldName)
        {
            ListItem lst = cmb.Items.FindByText(FieldName);
            if (lst != null)
                cmb.SelectedValue = lst.Value;
            else
            {
                cmb.SelectedIndex = 0;
            }
        }

        private void CalculateAmount()
        {
            if (Session["UOM"] == null) return;
            if (Session["UOM"].ToString() == string.Empty) return;
           

            if (txtChWeight.Text.Trim() == string.Empty) txtChWeight.Text="0";
            if (txtRate.Text.Trim() == string.Empty) txtRate.Text = "0";
            if (txttotPackage.Text.Trim() == string.Empty) txttotPackage.Text = "0";

            if (Session["UOM"].ToString().ToLower() == "veh")
            {
                txtSupplierFreight.Text = txtSupplierFreight.Text.Replace(".00", "");
                txtFrieght.Text = txtFrieght.Text.Replace(".00", "");
                txtRate.Text = txtFrieght.Text;
                txtSuplRate.Text = txtSupplierFreight.Text ;
            }

            if (Session["UOM"].ToString().ToLower()=="ton")
            {
                txtSupplierFreight.Text = Math.Round(((decimal.Parse(txtChWeight.Text)) * decimal.Parse(txtSuplRate.Text)), 0).ToString().Replace(".00", "");
                txtFrieght.Text = Math.Round(((decimal.Parse(txtChWeight.Text)) * decimal.Parse(txtRate.Text)), 0).ToString().Replace(".00", "");
            }
            if (Session["UOM"].ToString().ToLower() == "pkg")
            {
                txtSupplierFreight.Text = Math.Round((decimal.Parse(txttotPackage.Text) * decimal.Parse(txtSuplRate.Text)), 0).ToString().Replace(".00", "");
                txtFrieght.Text = Math.Round((decimal.Parse(txttotPackage.Text) * decimal.Parse(txtRate.Text)), 0).ToString().Replace(".00", "");
            }
            txtSupplierFreight_TextChanged(txtSupplierFreight, EventArgs.Empty);
            
        }

        private void CalculateAdvance()
        {
            if (txtCashAdvance.Text.Trim() == string.Empty) txtCashAdvance.Text = "0";
            if (txtFuelAdvance.Text.Trim() == string.Empty) txtFuelAdvance.Text = "0";
            txtAdvanceToSupplier.Text = (decimal.Parse(txtCashAdvance.Text) + decimal.Parse(txtFuelAdvance.Text)).ToString("F2");
        }

        private void CalculateAdvancePerWise()
        {
            decimal advancePer = 0;
            decimal advanceAmt = 0;
            decimal advanceAmt1 = txtAdvanceToSupplier.Text == "" ? 0 : decimal.Parse(txtAdvanceToSupplier.Text);
            decimal roundedAmt = 0;
            decimal freight = txtSupplierFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtSupplierFreight.Text);
            txtHiddenAdvanceper.Value = "0";
            if (cmbSupplier.SelectedIndex > 0)
            {
                LRBO optBO = new LRBO();
                optBO.SupplierID = long.Parse(cmbSupplier.SelectedValue);
                DataTable dt = optBO.GetSupplierDetailsById();
                if (dt.Rows.Count > 0)
                {
                    advancePer = decimal.Parse(dt.Rows[0]["AdvancePer"].ToString());
                    txtHiddenAdvanceper.Value = advancePer.ToString();
                    advanceAmt = freight * advancePer / 100;
                    roundedAmt = Math.Round(advanceAmt / 500);
                    roundedAmt = Math.Round(roundedAmt, 0);
                    roundedAmt = roundedAmt * 500;
                    if (advanceAmt1 == 0)
                    {
                        txtAdvanceToSupplier.Text = roundedAmt.ToString();
                    }

                    if (string.IsNullOrEmpty(txtFuelAdvance.Text.Trim()))
                    {
                        txtFuelAdvance.Text = "0";
                    }
                }
                txtCashAdvance.Text = (decimal.Parse(txtAdvanceToSupplier.Text) - decimal.Parse(txtFuelAdvance.Text)).ToString().Replace(".00", "");
            }
        }

        private bool IsAdvanceExceeding()
        {
            decimal roundedAmt = 0;
            if (txtHiddenAdvanceper.Value == string.Empty) txtHiddenAdvanceper.Value = "0";
            decimal advancePer = decimal.Parse(txtHiddenAdvanceper.Value);
            decimal advancePaid = txtAdvanceToSupplier.Text.Trim() == "" ? 0 : decimal.Parse(txtAdvanceToSupplier.Text);
            decimal freight = txtSupplierFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtSupplierFreight.Text);
            decimal advanceLimit = advancePer * freight / 100;
            advanceLimit = Math.Round(advanceLimit, 0);
            roundedAmt = Math.Round(advanceLimit / 500);
            roundedAmt = Math.Round(roundedAmt, 0);
            roundedAmt = roundedAmt * 500;

            if (advancePaid > roundedAmt)
            {
                MsgPopUp.ShowModal("Advance cannot exceed the Limit " + roundedAmt.ToString() + " For Supplier " + cmbSupplier.SelectedItem.Text, CommonTypes.ModalTypes.Error);
                return true;
            }
            else if (advancePaid > freight)
            {
                MsgPopUp.ShowModal("Advance cannot exceed the freight " + freight.ToString() + " For Supplier " + cmbSupplier.SelectedItem.Text, CommonTypes.ModalTypes.Error);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool AdvanceBreakupProper()
        {
            decimal cashAdvance = txtCashAdvance.Text.Trim() == "" ? 0 : decimal.Parse(txtCashAdvance.Text);
            decimal fuelAdvance = txtFuelAdvance.Text.Trim() == "" ? 0 : decimal.Parse(txtFuelAdvance.Text);
            decimal totAdvance = txtAdvanceToSupplier.Text.Trim() == "" ? 0 : decimal.Parse(txtAdvanceToSupplier.Text);
            if (totAdvance != cashAdvance + fuelAdvance)
            {
                MsgPopUp.ShowModal("Total Advance should match with cash and fuel advance", CommonTypes.ModalTypes.Error);
                return false;
            }
            else
            {
                return true;
            }
        }

        private string SavePdfLRForEmail(string LRNo)
        {
            ReportDocument RptDoc = new ReportDocument();
            ReportBO OptBO = new ReportBO();
            prjLRTrackerFinanceAuto.Datasets.DsLR ds = new prjLRTrackerFinanceAuto.Datasets.DsLR();
            OptBO.ID = long.Parse(txtHiddenId.Value);
            DataTable dt = OptBO.LR_Document();
            string fileName = Server.MapPath("~\\Downloads\\" + LRNo + ".pdf");
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(dt);
            RptDoc.Load(Server.MapPath("~/Reports/LR.rpt"));
            //condbsLogon(RptDoc);
            RptDoc.SetDataSource(ds);
            RptDoc.SetParameterValue(0, "Y");
            RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);

            return fileName;
        }

        private void SendMail()
        {
            string IsMail = ConfigurationManager.AppSettings["IsMail"].ToString();

            if (IsMail == "Y")
            {

                string consigneeEmailId = ""; string consignorEmailId = ""; string userEmailId = ""; int expdays = 0;
                string billingCompanyName = ""; string LRNos = ""; string LRDate = ""; string VehicleNo = ""; string from = ""; string To = ""; string expDeliveryDate = "";
                bool isMailSent = false;
                string consignee = "";
                string filePaths = "";
                userEmailId = GetUserEmail();
                consignorEmailId = txtConsignorEmail.Text.Trim();

                LRBO optbo = new LRBO();
                optbo.placementId = long.Parse(txtHiddenPlacementId.Value);
                DataSet DsLREmail = optbo.ShowLREmailDetails();

                DataTable dtLrNo = DsLREmail.Tables[0];
                DataTable dtLrDetails = DsLREmail.Tables[1];

                if (dtLrNo.Rows.Count > 0)
                {
                    if (dtLrDetails.Rows.Count > 0)
                    {
                        billingCompanyName = dtLrDetails.Rows[0]["BillingCompany"].ToString();
                        LRDate = DateTime.Parse(dtLrDetails.Rows[0]["LRDate"].ToString()).ToString("dd/MM/yyyy");
                        VehicleNo = dtLrDetails.Rows[0]["VehicleNo"].ToString();
                        from = dtLrDetails.Rows[0]["FromLoc"].ToString();
                        To = dtLrDetails.Rows[0]["ToLoc"].ToString();
                        expdays = int.Parse(dtLrDetails.Rows[0]["ExpectedDeliveryDays"].ToString());
                        expDeliveryDate = DateTime.Parse(dtLrDetails.Rows[0]["ExpDateofDelivery"].ToString()).ToString("dd/MM/yyyy");
                        consigneeEmailId = dtLrDetails.Rows[0]["ConsigneeEmail"].ToString();  //"onkarmedhekar@gmail.com";//
                        consignee = dtLrDetails.Rows[0]["Consignee"].ToString();
                        isMailSent = bool.Parse(dtLrDetails.Rows[0]["MailSent"].ToString());
                    }


                    if (!isMailSent)
                    {
                        for (int i = 0; i < dtLrNo.Rows.Count; i++)
                        {
                            if (string.IsNullOrEmpty(LRNos))
                            {
                                LRNos = dtLrNo.Rows[i][0].ToString();
                            }
                            else
                            {
                                LRNos = LRNos + "," + dtLrNo.Rows[i][0].ToString();
                            }

                            SavePdfLRForEmail(dtLrNo.Rows[i][1].ToString());
                            if (string.IsNullOrEmpty(filePaths))
                            {

                                filePaths = Server.MapPath("~\\Downloads\\" + dtLrNo.Rows[i][1].ToString() + ".pdf");
                            }
                            else
                            {
                                filePaths = filePaths + "," + Server.MapPath("~\\Downloads\\" + dtLrNo.Rows[i][1].ToString() + ".pdf");
                            }


                        }
                    }

                }
                MsgPanel.Message = filePaths;
                if (!isMailSent)
                {
                    if (expdays > 1)
                    {
                        string ToId = consigneeEmailId;
                        string subject = " Advance Shipment Notification LR No(s). " + LRNos;
                        string body = CommonTypes.GetMailCotent("L");
                        string FilePath = filePaths;
                        string fromId = ConfigurationManager.AppSettings["FromId"].ToString().Replace("&gt;", ">").Replace("&lt;", "<"); ;
                        string CCId = userEmailId + "," + txtConsignorEmail.Text;
                        MailHandler mailhandler = new MailHandler();

                        //string FilePath = SavePdfLRForEmail(txtLRNo.Text.Trim().Replace("/", ""));

                        body = string.Format(body, billingCompanyName, LRNos, txtDate.Text, VehicleNo, from, To, expDeliveryDate, Consignee);

                        body = "<html><head><style>body {font-family:arial;font-size:10pt;} td {font-family:arial;font-size:9pt;}</style></head><body bgcolor=#ffffff><table width=100%><tr bgcolor=#336699><td align=left style=font-weight:bold><font color=white> " +
                                   " LR Tracker Tool</font></td></tr></table> <table> <tr> <td>" + body;

                        body = body + "</td></tr></table><br/><br/><br/>" +
            "<hr size=2 width=100% align=center><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td>NOTE: This is a system generated notification, please <font color=red>DO NOT REPLY</font> to this mail.</td></tr></table><br><table cellspacing=0 cellpadding=0 width=100% border=0><tr><td  valign=top style={font-family:verdana;font-size:60%;color:#888888}>This" +
            "e-mail, and any attachments thereto, are intended only for use by the addressee(s) named herein. If you are not the intended recipient of this e-mail, you are hereby notified that any dissemination, distribution or copying which amounts to misappropriation of this e-mail and any attachments thereto, is strictly prohibited. If you have received this e-mail in error, please immediately notify me and permanently delete the original and any copy of any e-mail and " +
            "any printout thereof.</td></tr></table></body></html>";

                        MailMessage msg = mailhandler.CreateMessage(ToId, CCId, subject, body, true, FilePath);
                        bool result = mailhandler.SendEmail(msg, true, true, false);
                        if (result)
                        {
                            long cnt = optbo.UpdateEmailCnt();
                        }
                    }
                }

            }

        }

        private string GetUserEmail()
        {
            string userEmail = "";
            UserDAO optdao = new UserDAO();
            DataTable dtuser = optdao.GetUserDataByID(long.Parse(Session["EID"].ToString()));
            if (dtuser.Rows.Count > 0)
            {
                userEmail = dtuser.Rows[0]["Email_Id"].ToString();
            }
            return userEmail;
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            ScriptManager scrptMgr = ScriptManager.GetCurrent(this.Page);
            scrptMgr.RegisterPostBackControl(btnSaveFiles);
            if (!IsPostBack)
            {
                if (bool.Parse(Session["PlyApplicable"].ToString()))
                {
                    lblnoofply.Visible = true;
                    txtnoofPly.Visible = true;
                }
                else
                {
                    lblnoofply.Visible = false;
                    txtnoofPly.Visible = false;
                }
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                txtSearch.Focus();
            }
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:
                    //cmbDevice.Focus();
                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    //if (Delete() > 0)
                    //{
                    //    LoadGridView();
                    //    MsgPanel.Message = "Record(s) deleted successfully.";
                    //    MsgPanel.DispCode = 1;
                    //    txtFromDate.Focus();
                    //}
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            LRBO optbo = new LRBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    txtLRNo.Enabled = true;
                    FillDropdown(DropDownTypes.Consignee);
                    FillDropdown(DropDownTypes.Consigner);
                    FillDropdown(DropDownTypes.Destination);
                    FillDropdown(DropDownTypes.Origin);
                    FillDropdown(DropDownTypes.VehicleNo);
                    FillDropdown(DropDownTypes.PackingType);
                    FillDropdown(DropDownTypes.Description);
                    FillDropDownDevice();
                    FillDropDownVehicleType();
                    FillDropDownCustomer();
                    FillDropDownSupplier();
                    FillDropDownBillingCompany();
                    FillDropDownFrom();
                    FillDropDownConsignor();
                    ClearView();

                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    cmbBillingCompany.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    optbo.LRNo = txtLRNo.Text.Trim();
                    optbo.ID = long.Parse(txtHiddenId.Value);
                    if (optbo.LRDuplicateCount() > 0)
                    {
                        MsgPopUp.ShowModal("Duplicate LR No.", CommonTypes.ModalTypes.Error);
                        cmbBillingCompany.Focus();
                        return;
                    }
                    
                    DateTime lrdate1 = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime placementdate1 = DateTime.ParseExact(txtVehiclePlacementDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (placementdate1 > lrdate1)
                    {
                        MsgPopUp.ShowModal("Vehicle Placement Date cannot be greater than LR Date", CommonTypes.ModalTypes.Error);
                        dtpBtnPlace.Focus();
                        return;
                    }
                    if (IsAdvanceExceeding())
                    {
                        txtAdvanceToSupplier.Focus();
                        return;
                    }

                    if (!AdvanceBreakupProper())
                    {
                        txtAdvanceToSupplier.Focus();
                        return;
                    }

                    if (txtFuelAdvance.Text == "") txtFuelAdvance.Text = "0";

                    if ( decimal.Parse(txtFuelAdvance.Text)>0 )
                    {
                        if (cmbPetrolPump.SelectedIndex<=0)
                        {
                            MsgPopUp.ShowModal("Please select petrol pump from the list", CommonTypes.ModalTypes.Error);
                            cmbPetrolPump.Focus();
                            return;
                        }                      
                    }

                    if (txtTDFuelAdvance.Text.Trim()=="") txtTDFuelAdvance.Text = "0";

                    if (decimal.Parse(txtFuelAdvance.Text) == 0 && decimal.Parse(txtTDFuelAdvance.Text) > 0)
                    {
                        MsgPopUp.ShowModal("Fuel Slip is not generated for this LR. Please generate Fuel Slip and then Proceed ", CommonTypes.ModalTypes.Error);
                        return;
                    }
                    if (!chkEwayBillVerified.Checked)
                    {
                         MsgPopUp.ShowModal("Please Verify E-Way Bill Details", CommonTypes.ModalTypes.Error);
                        txtEwayBillNo.Focus();
                        return;
                    }
                    //txtEwayBillNo.Text = txtEwayBillNo.Text.Replace(" ", "");
                    //string[] ewaybill = txtEwayBillNo.Text.Split(',');
                    //string ewaybillNo = "";
                    //if (txtEwayBillNo.Text.Contains(","))
                    //{
                    //    for (int i = 0; i < ewaybill.Length; i++)
                    //    {
                    //        if (ewaybill[i].Trim().Length != 12)
                    //        {
                    //            ewaybillNo = ewaybill[i];
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    if (txtEwayBillNo.Text.Trim().Length != 12)
                    //        ewaybillNo = txtEwayBillNo.Text;
                    //}
                    //if (ewaybillNo!="")
                    //{
                    //    txtEwayBillNo.Focus();
                    //     MsgPopUp.ShowModal("Please enter Proper Ewaybill No : " +ewaybillNo, CommonTypes.ModalTypes.Error);
                    //    return;
                    //}

                    optbo.LRDatetime = DateTime.Parse(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MMM/yyyy") + " " + ddlHH.Text + ":" + ddlmm.Text);
                    optbo.Consignee = cmbConsignee.SelectedItem.Text.Trim();
                    optbo.Consigner = cmbConsigner.SelectedItem.Text.Trim();
                    optbo.Origin = cmbFrom.Text.Trim();
                    optbo.Destination = cmbTo.SelectedItem.Text.Trim();
                    optbo.VehicleNo = txtVehicleNo.Text.Trim();
                    optbo.Weight = decimal.Parse(txtWeight.Text);
                    optbo.ChargeableWt = decimal.Parse(txtChWeight.Text);
                    optbo.TotPackage = long.Parse(txttotPackage.Text);
                    optbo.InvoiceNo = txtInvoiceNo.Text.Trim();
                    optbo.InvoiceValue = decimal.Parse(txtInvoiceValue.Text);
                    optbo.LoadType = cmbType.Text.Trim().Substring(0, 1);
                    optbo.Remarks = cmbDescription.SelectedItem.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.Status = "T";
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.DriverDtls = txtDriverDetails.Text.Trim();
                    optbo.DeviceID = 0;
                  

                    optbo.RefNo = txtRefNo.Text.Trim();
                    optbo.SupplierID = long.Parse(cmbSupplier.SelectedValue);
                    optbo.SubSupplierID = 0;
                    optbo.CustomerID = long.Parse(cmbCustomer.SelectedValue);

                    optbo.Incentive = txtIncentive.Text.Trim() == "" ? 0 : decimal.Parse(txtIncentive.Text);
                    optbo.SupplierIncentive = txtSupplierIncentive.Text.Trim() == "" ? 0 : decimal.Parse(txtSupplierIncentive.Text);

                    optbo.TotalFrieght = txtFrieght.Text.Trim() == "" ? 0 : decimal.Parse(txtFrieght.Text) + optbo.Incentive;
                    optbo.SupplierAmount = txtSupplierFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtSupplierFreight.Text) + optbo.SupplierIncentive;
                    optbo.CashAdvance = txtCashAdvance.Text.Trim() == "" ? 0 : decimal.Parse(txtCashAdvance.Text);
                    optbo.FuelAdvance = txtFuelAdvance.Text.Trim() == "" ? 0 : decimal.Parse(txtFuelAdvance.Text);
                    optbo.AdvanceToSupplier = txtAdvanceToSupplier.Text.Trim()==""? 0: decimal.Parse(txtAdvanceToSupplier.Text);
                    optbo.VehicleTypeID = long.Parse(cmbVehicleType.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.Rate = txtRate.Text.Trim()==""? 0: decimal.Parse(txtRate.Text);
                    optbo.ConsignorAddress = txtConsignerAddress.Text.Trim();
                    optbo.ConsigneeAddress = txtConsigneeAddress.Text.Trim();
                    optbo.ConsigneeEmail = txtConsigneeEmail.Text.Trim();
                    optbo.ConsigneeContact = txtConsigneeContact.Text.Trim();
                    optbo.VehicleSealNo = txtvehicleSealNo.Text.Trim();
                    optbo.PackingType = cmbType.SelectedItem.Text;
                    optbo.ExpectedDeliveryDays = int.Parse(txtExpDeliveryDays.Text);
                    optbo.VehiclePlacementDate = txtVehiclePlacementDate.Text.Trim()==string.Empty?txtDate.Text:txtVehiclePlacementDate.Text;
                    optbo.DriverNumber = txtDriverNumber.Text.Trim();
                    optbo.FreightType = cmbFreight.SelectedItem.Text;
                    optbo.ConsignorGSTNo = txtConsignorGSTNo.Text.Trim();
                    optbo.ConsigneeGSTNo = txtConsigneeGSTNo.Text.Trim();
                    optbo.RefNo_Invoice = string.Empty; // txtRefNo_Invoice.Text.Trim();
                    optbo.InvoiceDate = txtInvoiceDate.Text.Trim();
                    optbo.EWayBillNo = txtEwayBillNo.Text.Trim();
                    optbo.AdvancePer = decimal.Parse(txtHiddenAdvanceper.Value);
                    optbo.Multipoint = false;//chkMultipoint.Checked;
                    optbo.MainLRNo = "";//txtMainLRNo.Text.Trim();
                    optbo.ShipmentDocNo = txtShippingDocNo.Text.Trim();
                    optbo.InternalBillingDocNo = txtInternalbillingNo.Text.Trim();
                    optbo.PetrolPumpId = cmbPetrolPump.SelectedIndex > 0 ? int.Parse(cmbPetrolPump.SelectedValue) : 0;
                    if (txtnoofPly.Visible)
                    {
                        optbo.PlyQty = string.IsNullOrEmpty(txtnoofPly.Text) ? 0 : int.Parse(txtnoofPly.Text);
                    }
                    else
                    {
                        optbo.PlyQty = 0;
                    }
                    optbo.ConsignorEmail = txtConsignorEmail.Text.Trim();
                    optbo.ConsignorTelNo = txtConsignorContact.Text.Trim();
                    optbo.ConsignorPincode = txtConsignerPIN.Text.Trim();
                    optbo.ConsigneePincode = txtConsigneePIN.Text.Trim();
                    optbo.EwayBillDate = txtEwayBillDate.Text.Trim();
                    long cnt = optbo.InsertUpdateLR();
                    //Send Mail to consignee
                    if (txtEwayBillNo.Text.Trim()!=string.Empty)
                        SendMail();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            MsgPanel.Message = "Record Saved successfully.";
                            MsgPanel.DispCode = 1;
                        }
                        else
                        {
                            MsgPanel.Message = "Record Updated successfully.";
                            MsgPanel.DispCode = 1;
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = "";
            MsgPanel.DispCode = -1;
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                decimal TotalFreight=keys["TotalFrieght"]==null?0:keys["TotalFrieght"].ToString().Trim()==string.Empty?0:decimal.Parse(keys["TotalFrieght"].ToString());
                if (keys["Status"].ToString() == "Delivered" && TotalFreight > 0 && Session["ROLEID"].ToString() != "1" && Session["ROLEID"].ToString() != "8")
                {
                    MsgPopUp.ShowModal("LR Status is Delivered Hence Cannot Modify.", CommonTypes.ModalTypes.Error);
                    return;
                }
                if (keys["EwayBillFileName"] == null || keys["EwayBillFileName"].ToString()=="")
                {
                    MsgPopUp.ShowModal("Please Upload Eway Bill File First.", CommonTypes.ModalTypes.Error);
                    return;
                }
                //txtLRNo.Enabled = true;
                FillDropdown(DropDownTypes.Consignee);
                FillDropdown(DropDownTypes.Consigner);
                FillDropdown(DropDownTypes.Destination);
                FillDropdown(DropDownTypes.Origin);
                FillDropdown(DropDownTypes.VehicleNo);
                FillDropdown(DropDownTypes.PackingType);
                FillDropdown(DropDownTypes.Description);
                FillDropDownVehicleType();
                FillDropDownCustomer();
                FillDropDownSupplier();
                FillDropDownPetrolPump();
                FillDropDownFrom();
                FillDropDownConsignor();
                FillDropDownBillingCompany();

                if (keys["DeviceID"] != null)
                {
                    if (keys["DeviceID"].ToString() != string.Empty)
                        FillDropDownDevice(long.Parse(keys["DeviceID"].ToString()));
                    else
                        FillDropDownDevice();
                }
                else
                    FillDropDownDevice();

                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                txtHiddenId.Value = keys["ID"].ToString();
                txtHiddenPlacementId.Value = keys["PlacementID"].ToString();
                txtPlacementId.Text = keys["PlacementID"].ToString();
                txtLRNo.Text = keys["LRNo"].ToString();
                txtDate.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("dd/MM/yyyy").Replace("-", "/");
                txtDate_TextChanged(txtDate, EventArgs.Empty);
                ddlHH.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("HH");
                ddlmm.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("mm");
                if (keys["CustomerID"] != null)
                {
                    if (keys["CustomerID"].ToString() != string.Empty)
                    {
                        cmbCustomer.SelectedValue = keys["CustomerID"].ToString();
                        cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                    }
                    
                }
                setCombobox(cmbConsigner, keys["Consigner"].ToString()); //cmbConsigner.SelectedValue = keys["Consigner"].ToString();
                cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                setComboboxValue(cmbConsignee, keys["Consignee"].ToString());
                //cmbConsignee.SelectedValue = keys["Consignee"].ToString();
                setCombobox(cmbFrom, keys["FromLoc"].ToString()); //cmbFrom.SelectedValue = keys["FromLoc"].ToString();
                FillDropDownDestination(keys["FromLoc"].ToString());
                setCombobox(cmbTo, keys["ToLoc"].ToString()); //cmbTo.SelectedValue = keys["ToLoc"].ToString();
                //cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                txtVehicleNo.Text = keys["VehicleNo"].ToString();
                txtWeight.Text = keys["Weight"].ToString();
                txtChWeight.Text = keys["ChargeableWt"].ToString();
                txttotPackage.Text = keys["TotPackages"].ToString();
                txtInvoiceNo.Text = keys["InvoiceNo"].ToString();
                txtInvoiceValue.Text = keys["InvoiceValue"].ToString();
                //cmbType.SelectedIndex = keys["LoadType"].ToString() == "F" ? 0 : 1;
                if (keys["Remarks"] != null) setCombobox(cmbDescription, keys["Remarks"].ToString());
                //cmbDescription.SelectedValue = keys["Remarks"].ToString();
                //ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,PackingType
                txtConsigneeAddress.Text = keys["ConsigneeAddress"].ToString();
                if (keys["ConsignerAddress"] != null)
                {
                    if (keys["ConsignerAddress"].ToString().Trim() != string.Empty)
                        txtConsignerAddress.Text = keys["ConsignerAddress"].ToString();
                }

                txtConsigneeEmail.Text = keys["ConsigneeEmail"].ToString();
                txtConsigneeContact.Text = keys["ConsigneeTelNo"].ToString();
                txtvehicleSealNo.Text = keys["VehicleSealNo"].ToString();

                txtIncentive.Text = keys["FreightIncentive"].ToString() == "0.00" ? "0" : keys["FreightIncentive"].ToString().Replace(".00", "");      
                txtFrieght.Text = keys["TotalFrieght"].ToString()=="-1.00" ? "0" : keys["TotalFrieght"].ToString().Replace(".00","");
                txtFrieght.Text = (decimal.Parse(txtFrieght.Text) - decimal.Parse(txtIncentive.Text)).ToString().Replace(".00", "");

                txtRate.Text = keys["Rate"].ToString() == "-1.00" ? "" : keys["Rate"].ToString();
                
                if (keys["PackingType"] != null) setCombobox(cmbType, keys["PackingType"].ToString()); 
                //cmbType.SelectedValue = keys["PackingType"].ToString();

                txtDriverDetails.Text = keys["DriverDetails"].ToString();

                 
                if (keys["SupplierID"] != null)
                {
                    if (keys["SupplierID"].ToString() != string.Empty)
                    {
                        cmbSupplier.SelectedValue = keys["SupplierID"].ToString();                       
                            CalculateAdvancePerWise();
                    }
                }
                txtSupplierIncentive.Text = keys["SupplierFreightIncentive"].ToString() == "0.00" ? "0" : keys["SupplierFreightIncentive"].ToString().Replace(".00", "");      
                txtSupplierFreight.Text = keys["SupplierAmount"].ToString() == "0.00" ? "0" : keys["SupplierAmount"].ToString().Replace(".00", "");
                txtSupplierFreight.Text = (decimal.Parse(txtSupplierFreight.Text) - decimal.Parse(txtSupplierIncentive.Text)).ToString().Replace(".00", "");

                txtCashAdvance.Text = keys["CashAdvance"].ToString() == "0.00" ? "" : keys["CashAdvance"].ToString();
                txtFuelAdvance.Text = keys["TotFuelAdvance"].ToString() == "0.00" ? "" : keys["TotFuelAdvance"].ToString();
                txtAdvanceToSupplier.Text = decimal.Parse(keys["AdvanceToSupplier"].ToString()) < 0 ? "" : keys["AdvanceToSupplier"].ToString();

                if (int.Parse(keys["AdvancePaid"].ToString()) > 0)
                {
                    txtCashAdvance.Enabled = false;
                    txtFuelAdvance.Enabled = false;
                    txtAdvanceToSupplier.Enabled = false;
                    cmbPetrolPump.Enabled = false;
                }
                else
                {
                    txtCashAdvance.Enabled = true;
                    txtFuelAdvance.Text = keys["TotFuelAdvance"].ToString();
                    txtFuelAdvance.Enabled = false;
                    txtAdvanceToSupplier.Enabled = true;
                    cmbPetrolPump.Enabled = false;
                }

               
                if (keys["VehicleTypeID"] != null)
                {
                    if (keys["VehicleTypeID"].ToString() != string.Empty)
                        cmbVehicleType.SelectedValue = keys["VehicleTypeID"].ToString();
                }
                if (keys["RefNo"] != null)
                    txtRefNo.Text = keys["RefNo"].ToString();

                if (keys["VehiclePlacementDate"] != null)
                {
                    if (keys["VehiclePlacementDate"].ToString().Trim() != "")
                    {
                        txtVehiclePlacementDate.Text = DateTime.Parse(keys["VehiclePlacementDate"].ToString()).ToString("dd/MM/yyyy");
                        CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["VehiclePlacementDate"].ToString());
                    }
                    else
                    {
                        CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["LRDate"].ToString());
                    }
                }
                else
                {
                    CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["LRDate"].ToString());
                }

                if (keys["DriverNumber"] != null)
                    txtDriverNumber.Text = keys["DriverNumber"].ToString();

                if (keys["FreightType"] != null)
                    cmbFreight.SelectedValue = keys["FreightType"].ToString();

                if (keys["ConsignorGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsignorGSTNo"].ToString().Trim()))
                        txtConsignorGSTNo.Text = keys["ConsignorGSTNo"].ToString();
                }
                if (keys["ConsigneeGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsigneeGSTNo"].ToString().Trim()))
                        txtConsigneeGSTNo.Text = keys["ConsigneeGSTNo"].ToString();
                }
                //if (keys["RefNo_Invoice"] != null)
                //{
                //    if (!string.IsNullOrEmpty(keys["RefNo_Invoice"].ToString().Trim()))
                //        txtRefNo_Invoice.Text = keys["RefNo_Invoice"].ToString();
                //}
                if (keys["InvoiceDate"] != null)
                {
                    if (keys["InvoiceDate"].ToString().Trim() != "")
                    {
                        txtInvoiceDate.Text = DateTime.Parse(keys["InvoiceDate"].ToString()).ToString("dd/MM/yyyy");
                    }
                }
                if (keys["PetrolPumpId"] != null)
                {
                    if (keys["PetrolPumpId"].ToString().Trim() != "" && keys["PetrolPumpId"].ToString().Trim() != "0")
                    {
                        cmbPetrolPump.SelectedValue = keys["PetrolPumpId"].ToString();
                    }
                }

                if (keys["FuelSlipId"] != null)
                {
                    if (keys["FuelSlipId"].ToString().Trim() != "" && keys["FuelSlipId"].ToString().Trim() != "0")
                    {
                        lblFuelSlipId.Text = "FS-"+ keys["FuelSlipId"].ToString();
                    }
                    else
                    {
                        lblFuelSlipId.Text = "";
                    }
                }
                else
                {
                    lblFuelSlipId.Text = "";
                }
                if (keys["ExpectedDeliveryDays"] != null)
                    txtExpDeliveryDays.Text = keys["ExpectedDeliveryDays"].ToString();

                //Multipoint Details
                txtEwayBillNo.Text = keys["EwayBillNo"].ToString();
                //chkMultipoint.Checked = keys["MultiPoint"].ToString() ==string.Empty ? false :  bool.Parse(keys["MultiPoint"].ToString());
                //if (chkMultipoint.Checked)
                //    txtMainLRNo.Enabled = true;
                //else
                //    txtMainLRNo.Enabled = false;

                //txtMainLRNo.Text = keys["MainLRNo"].ToString();
                txtShippingDocNo.Text = keys["ShipmentDocNo"].ToString();
                txtInternalbillingNo.Text = keys["InternalBillingDocNo"].ToString();

                txtConsignorEmail.Text = keys["ConsignorEmail"].ToString();
                txtConsignorContact.Text = keys["ConsignorTelNo"].ToString();
                txtConsignerPIN.Text = keys["ConsignorPincode"].ToString();
                txtConsigneePIN.Text = keys["ConsigneePincode"].ToString();
                txtnoofPly.Text = keys["PlyQty"].ToString();

                if (keys["EwayBillDate"] != null && keys["EwayBillDate"].ToString().Trim() != string.Empty)
                    txtEwayBillDate.Text = DateTime.Parse(keys["EwayBillDate"].ToString()).ToString("dd/MM/yyyy");
                else
                    txtEwayBillDate.Text = "";

                GetDetailsFromTDA(long.Parse(txtHiddenPlacementId.Value), long.Parse(txtHiddenId.Value),"N");

                ShowViewByIndex(0);

                
                btnSavePacklist.Visible = true;
                pnlEntry.Enabled = true;
                cmbBillingCompany.Focus();
            }
            else if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For LR No. " + keys["LRNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["ID"].ToString()));
            }
            else if (e.CommandName.ToLower()=="viewlr")
            {
                decimal TotalFreight=keys["TotalFrieght"]==null?0:keys["TotalFrieght"].ToString().Trim()==string.Empty?0:decimal.Parse(keys["TotalFrieght"].ToString());
                ////if (keys["Status"].ToString() == "Delivered" && TotalFreight>0 && Session["ROLEID"].ToString() != "1")
                ////{
                ////    MsgPopUp.ShowModal("LR Status is Delivered Hence Cannot Modify.", CommonTypes.ModalTypes.Error);
                ////    return;
                ////}

                //txtLRNo.Enabled = true;
                FillDropdown(DropDownTypes.Consignee);
                FillDropdown(DropDownTypes.Consigner);
                FillDropdown(DropDownTypes.Destination);
                FillDropdown(DropDownTypes.Origin);
                FillDropdown(DropDownTypes.VehicleNo);
                FillDropdown(DropDownTypes.PackingType);
                FillDropdown(DropDownTypes.Description);
                FillDropDownVehicleType();
                FillDropDownCustomer();
                FillDropDownSupplier();
                FillDropDownFrom();
                FillDropDownConsignor();
                FillDropDownBillingCompany();

                if (keys["DeviceID"] != null)
                {
                    if (keys["DeviceID"].ToString() != string.Empty)
                        FillDropDownDevice(long.Parse(keys["DeviceID"].ToString()));
                    else
                        FillDropDownDevice();
                }
                else
                    FillDropDownDevice();

                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                txtHiddenId.Value = keys["ID"].ToString();
                txtHiddenPlacementId.Value = keys["PlacementID"].ToString();
                txtPlacementId.Text = keys["PlacementID"].ToString();
                txtLRNo.Text = keys["LRNo"].ToString();
                txtDate.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("dd/MM/yyyy").Replace("-", "/");
                txtDate_TextChanged(txtDate, EventArgs.Empty);
                ddlHH.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("HH");
                ddlmm.Text = DateTime.Parse(keys["LRDate"].ToString()).ToString("mm");
                if (keys["CustomerID"] != null)
                {
                    if (keys["CustomerID"].ToString() != string.Empty)
                    {
                        cmbCustomer.SelectedValue = keys["CustomerID"].ToString();
                        cmbCustomer_SelectedIndexChanged(cmbCustomer, EventArgs.Empty);
                    }
                }
                setCombobox(cmbConsigner, keys["Consigner"].ToString()); //cmbConsigner.SelectedValue = keys["Consigner"].ToString();
                cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                setComboboxValue(cmbConsignee, keys["Consignee"].ToString());
                //cmbConsignee.SelectedValue = keys["Consignee"].ToString();
                setCombobox(cmbFrom, keys["FromLoc"].ToString()); //cmbFrom.SelectedValue = keys["FromLoc"].ToString();
                FillDropDownDestination(keys["FromLoc"].ToString());
                setCombobox(cmbTo, keys["ToLoc"].ToString()); //cmbTo.SelectedValue = keys["ToLoc"].ToString();
                //cmbTo_SelectedIndexChanged(cmbTo, EventArgs.Empty);
                txtVehicleNo.Text = keys["VehicleNo"].ToString();
                txtWeight.Text = keys["Weight"].ToString();
                txtChWeight.Text = keys["ChargeableWt"].ToString();
                txttotPackage.Text = keys["TotPackages"].ToString();
                txtInvoiceNo.Text = keys["InvoiceNo"].ToString();
                txtInvoiceValue.Text = keys["InvoiceValue"].ToString();
                //cmbType.SelectedIndex = keys["LoadType"].ToString() == "F" ? 0 : 1;
                if (keys["Remarks"] != null) setCombobox(cmbDescription, keys["Remarks"].ToString());
                //cmbDescription.SelectedValue = keys["Remarks"].ToString();
                //ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,PackingType
                txtConsigneeAddress.Text = keys["ConsigneeAddress"].ToString();
                if (keys["ConsignerAddress"] != null)
                {
                    if (keys["ConsignerAddress"].ToString().Trim() != string.Empty)
                        txtConsignerAddress.Text = keys["ConsignerAddress"].ToString();
                }

                txtConsigneeEmail.Text = keys["ConsigneeEmail"].ToString();
                txtConsigneeContact.Text = keys["ConsigneeTelNo"].ToString();
                txtvehicleSealNo.Text = keys["VehicleSealNo"].ToString();

                txtIncentive.Text = keys["FreightIncentive"].ToString() == "0.00" ? "0" : keys["FreightIncentive"].ToString().Replace(".00", "");      
                txtFrieght.Text = keys["TotalFrieght"].ToString()=="-1.00" ? "0" : keys["TotalFrieght"].ToString().Replace(".00","");
                txtFrieght.Text = (decimal.Parse(txtFrieght.Text) - decimal.Parse(txtIncentive.Text)).ToString().Replace(".00", "");

                txtRate.Text = keys["Rate"].ToString() == "-1.00" ? "" : keys["Rate"].ToString();
                
                if (keys["PackingType"] != null) setCombobox(cmbType, keys["PackingType"].ToString()); 
                //cmbType.SelectedValue = keys["PackingType"].ToString();

                txtDriverDetails.Text = keys["DriverDetails"].ToString();

                
                if (keys["SupplierID"] != null)
                {
                    if (keys["SupplierID"].ToString() != string.Empty)
                    {
                        cmbSupplier.SelectedValue = keys["SupplierID"].ToString();                       
                            CalculateAdvancePerWise();
                    }
                }
                txtSupplierIncentive.Text = keys["SupplierFreightIncentive"].ToString() == "0.00" ? "0" : keys["SupplierFreightIncentive"].ToString().Replace(".00", "");      
                txtSupplierFreight.Text = keys["SupplierAmount"].ToString() == "0.00" ? "0" : keys["SupplierAmount"].ToString().Replace(".00", "");
                txtSupplierFreight.Text = (decimal.Parse(txtSupplierFreight.Text) - decimal.Parse(txtSupplierIncentive.Text)).ToString().Replace(".00", "");

                txtCashAdvance.Text = keys["CashAdvance"].ToString() == "0.00" ? "" : keys["CashAdvance"].ToString();
                txtFuelAdvance.Text = keys["FuelAdvance"].ToString() == "0.00" ? "" : keys["FuelAdvance"].ToString();
                txtAdvanceToSupplier.Text = decimal.Parse(keys["AdvanceToSupplier"].ToString()) < 0 ? "" : keys["AdvanceToSupplier"].ToString();

                if (int.Parse(keys["AdvancePaid"].ToString()) > 0)
                {
                    txtCashAdvance.Enabled = false;
                    txtFuelAdvance.Enabled = false;
                    txtAdvanceToSupplier.Enabled = false;
                }
                else
                {
                    txtCashAdvance.Enabled = true;
                    txtFuelAdvance.Enabled = true;
                    txtAdvanceToSupplier.Enabled = true;
                }               
                if (keys["VehicleTypeID"] != null)
                {
                    if (keys["VehicleTypeID"].ToString() != string.Empty)
                        cmbVehicleType.SelectedValue = keys["VehicleTypeID"].ToString();
                }
                if (keys["RefNo"] != null)
                    txtRefNo.Text = keys["RefNo"].ToString();

                if (keys["VehiclePlacementDate"] != null)
                {
                    if (keys["VehiclePlacementDate"].ToString().Trim() != "")
                    {
                        txtVehiclePlacementDate.Text = DateTime.Parse(keys["VehiclePlacementDate"].ToString()).ToString("dd/MM/yyyy");
                        CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["VehiclePlacementDate"].ToString());
                    }
                    else
                    {
                        CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["LRDate"].ToString());
                    }
                }
                else
                {
                    CalExtVehiclePlacement.SelectedDate = DateTime.Parse(keys["LRDate"].ToString());
                }

                if (keys["DriverNumber"] != null)
                    txtDriverNumber.Text = keys["DriverNumber"].ToString();

                if (keys["FreightType"] != null)
                    cmbFreight.SelectedValue = keys["FreightType"].ToString();

                if (keys["ConsignorGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsignorGSTNo"].ToString().Trim()))
                        txtConsignorGSTNo.Text = keys["ConsignorGSTNo"].ToString();
                }
                if (keys["ConsigneeGSTNo"] != null)
                {
                    if (!string.IsNullOrEmpty(keys["ConsigneeGSTNo"].ToString().Trim()))
                        txtConsigneeGSTNo.Text = keys["ConsigneeGSTNo"].ToString();
                }
                //if (keys["RefNo_Invoice"] != null)
                //{
                //    if (!string.IsNullOrEmpty(keys["RefNo_Invoice"].ToString().Trim()))
                //        txtRefNo_Invoice.Text = keys["RefNo_Invoice"].ToString();
                //}
                if (keys["InvoiceDate"] != null)
                {
                    if (keys["InvoiceDate"].ToString().Trim() != "")
                    {
                        txtInvoiceDate.Text = DateTime.Parse(keys["InvoiceDate"].ToString()).ToString("dd/MM/yyyy");
                    }
                }
                if (keys["ExpectedDeliveryDays"] != null)
                    txtExpDeliveryDays.Text = keys["ExpectedDeliveryDays"].ToString();

                //Multipoint Details
                txtEwayBillNo.Text = keys["EwayBillNo"].ToString();
                //chkMultipoint.Checked = keys["MultiPoint"].ToString() ==string.Empty ? false :  bool.Parse(keys["MultiPoint"].ToString());
                //if (chkMultipoint.Checked)
                //    txtMainLRNo.Enabled = true;
                //else
                //    txtMainLRNo.Enabled = false;

                //txtMainLRNo.Text = keys["MainLRNo"].ToString();
                txtShippingDocNo.Text = keys["ShipmentDocNo"].ToString();
                txtInternalbillingNo.Text = keys["InternalBillingDocNo"].ToString();

                GetDetailsFromTDA(long.Parse(txtHiddenPlacementId.Value), long.Parse(txtHiddenId.Value),"A");

                ShowViewByIndex(0);

                cmbBillingCompany.Focus();
                btnSavePacklist.Visible = false;
                pnlEntry.Enabled = false;
            }
            else if (e.CommandName == "FileAttached")
            {
                if (keys["FileAttached"].ToString() == "@")
                {
                    Session["FilePath"] = Server.MapPath(ConfigurationManager.AppSettings["EwayBillFilesUploadPath"].ToString()) + "\\" + keys["EwayBillFileName"];
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile1.aspx','_blank')", true);
                }
                else
                {
                    hdFldSrl.Value = keys["ID"].ToString();
                    hdFldLRNo.Value = keys["LRNo"].ToString();
                    popAttachFIles.Show();
                }
            }
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex > 0)
            {
                ConsignorBO optbo = new ConsignorBO();
                optbo.CustomerId = long.Parse(cmbCustomer.SelectedValue);
                DataSet ds = optbo.FillConsignorConsignee();

                cmbConsigner.DataSource = null;
                cmbConsigner.Items.Clear();
                cmbConsigner.Items.Add(new ListItem("", "-1"));

                cmbConsigner.DataSource = ds.Tables[0];
                cmbConsigner.DataTextField = "Name";
                cmbConsigner.DataValueField = "ConsignorId";
                cmbConsigner.DataBind();
                cmbConsigner.SelectedIndex = 0;
                if (cmbConsigner.Items.Count == 2)
                {
                    cmbConsigner.SelectedIndex = 1;
                    cmbConsigner_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbConsignee.Focus();
                }

                cmbConsignee.DataSource = null;
                cmbConsignee.Items.Clear();
                cmbConsignee.Items.Add(new ListItem("", "-1"));

                cmbConsignee.DataSource = ds.Tables[1];
                cmbConsignee.DataTextField = "Name";
                cmbConsignee.DataValueField = "ConsignorId";
                cmbConsignee.DataBind();
                cmbConsignee.SelectedIndex = 0;
                if (cmbConsignee.Items.Count == 2)
                {
                    cmbConsignee.SelectedIndex = 1;
                    cmbConsignee_SelectedIndexChanged(cmbConsigner, EventArgs.Empty);
                    cmbFrom.Focus();
                }
            }
        }

        protected void txtChWeight_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
            txttotPackage.Focus();
        }

        protected void txttotPackage_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
            cmbType.Focus();
        }

        protected void txtRate_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();            
        }

        protected void txtSuplRate_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();          
        }       

        protected void cmbFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtExpDeliveryDays.Text = "0";
            FillDropDownDestination(cmbFrom.SelectedItem.Text.Trim());
            cmbFrom.Focus();
        }

        protected void cmbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtExpDeliveryDays.Text = "0";
            if (cmbTo.SelectedIndex > 0)
            {
                DestinationBO OptBo = new DestinationBO();
                OptBo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
                OptBo.ToLoc = cmbTo.SelectedItem.Text.Trim();
                txtExpDeliveryDays.Text = OptBo.GetDestinationExpDays().ToString();
            }
            cmbTo.Focus();
        }

        protected void cmbConsigner_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtConsignerAddress.Text = "0";
            if (cmbConsigner.SelectedIndex > 0)
            {
                ConsignorBO OptBo = new ConsignorBO();
                OptBo.Name = cmbConsigner.SelectedItem.Text.Trim();
                txtConsignerAddress.Text = OptBo.GetConsignorAddress().ToString();
            }
            cmbConsigner.Focus();
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            //CalExtVehiclePlacement.EndDate = DateTime.Parse(txtDate.Text);
            txtVehiclePlacementDate.Text = string.Empty;
            txtDate.Focus();
        }

        protected void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateAdvancePerWise();
        }

        protected void cmbConsignee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConsignorBO optbo = new ConsignorBO();
            optbo.Srl = long.Parse(cmbConsignee.SelectedValue);

            DataTable dt = optbo.GetConsignorDetailsById();
            //if (txtHiddenId.Value == "0")
            //{
            if (dt.Rows.Count > 0)
            {
                txtConsigneeAddress.Text = dt.Rows[0]["Address"].ToString();
                txtConsigneeEmail.Text = dt.Rows[0]["EmailId"].ToString();
                txtConsigneeContact.Text = dt.Rows[0]["MobileNo"].ToString();
                txtConsigneeGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                txtConsigneePIN.Text = dt.Rows[0]["Pincode"].ToString();
            }
            //}
            txtConsigneeAddress.Focus();
        }

        protected void txtSupplierFreight_TextChanged(object sender, EventArgs e)
        {
            //txtAdvanceToSupplier.Text = string.Empty;
            decimal advance = txtAdvanceToSupplier.Text == "" ? 0 : decimal.Parse(txtAdvanceToSupplier.Text);
            if (!string.IsNullOrEmpty(txtSupplierFreight.Text))
            {
                if (decimal.Parse(txtSupplierFreight.Text) > 0)
                {
                    if (advance == 0)
                    {
                        CalculateAdvancePerWise();
                    }

                    txtSupplierFreight.Focus();
                }
            }
        }

        protected void btnFileCancel_Click(object sender, EventArgs e)
        {
            //SavePolicy("");
            popAttachFIles.Hide();
        }

        protected void btnSaveFiles_Click(object sender, EventArgs e)
        {
            popAttachFIles.Show();
            string fileName = flCopy.FileName;
            string[] objFileName = fileName.Split('.');
            string newId = hdFldLRNo.Value + "." + objFileName[1].ToString();
            string filepath = Server.MapPath(ConfigurationManager.AppSettings["EwayBillFilesUploadPath"].ToString()) + @"\" + newId;
            flCopy.SaveAs(filepath);

            LRBO chbo = new LRBO();
            chbo.ID = long.Parse(hdFldSrl.Value);
            chbo.FileName = newId;
            chbo.CreatedBy = long.Parse(Session["EID"].ToString());

            long cnt = chbo.UploadEwayFile();
            if (cnt > 0)
            {
                hdFldSrl.Value = "0";
                hdFldLRNo.Value = "";
                LoadGridView();
                popAttachFIles.Hide();
            }
            else
            {
                popAttachFIles.Show();
            }
        }
        #endregion  
    }
}