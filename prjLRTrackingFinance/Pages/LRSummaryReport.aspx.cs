﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            RegisterDateTextBox();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename= HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\LRSummaryReg_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();
            OptBO.BranchID = long.Parse(Session["BranchID"].ToString());

            DataTable dt = OptBO.LRSummaryReport();

            ExportToExcel(dt, filename);

        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["B:B"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["Q:Q"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:BZ1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        private void ExportToExcel(DataTable dt, string fileName, string worksheetName)
        {
            //Response.Clear();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWriter);
            DataGrid dataExportExcel = new DataGrid();
            dataExportExcel.ItemDataBound += new DataGridItemEventHandler(dataExportExcel_ItemDataBound);
            dataExportExcel.DataSource = dt;
            dataExportExcel.DataBind();
            dataExportExcel.RenderControl(htmlWrite);
            StringBuilder sbResponseString = new StringBuilder();
            sbResponseString.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"> <head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>" + worksheetName + "</x:Name><x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head> <body>");
            sbResponseString.Append(stringWriter + "</body></html>");
            Response.Write(sbResponseString.ToString());
            Response.End();
        }

        void dataExportExcel_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Header Text Format can be done as follows
                e.Item.Font.Bold = true;

                //Adding Filter/Sorting functionality for the Excel
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    e.Item.Cells[cellIndex].Attributes.Add("x:autofilter", "all");
                    e.Item.Cells[cellIndex].Width = 160;
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Center;
                    cellIndex++;
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    //Any Cell specific formatting should be done here
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Left;
                    cellIndex++;
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void LoadGridView()
        {
            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();
            OptBO.BranchID = long.Parse(Session["BranchID"].ToString());
            DataTable dt = OptBO.LRSummaryReport();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (Session["Item_Expr"] != null)
                    dataView.Sort = Session["Item_Expr"].ToString() + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        protected void grdMain_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Session["Item_Sort"] == null)
            {
                Session["Item_Sort"] = "ASC";
            }
            else
            {
                if (Session["Item_Sort"].ToString() == "ASC")
                    Session["Item_Sort"] = "DESC";
                else
                    Session["Item_Sort"] = "ASC";
            }
            Session["Item_Expr"] = e.SortExpression;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}