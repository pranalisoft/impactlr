﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierPayableApproval : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void setGrid()
        {
            DataTable dt = CommonTypes.CreateDataTable("SupplierId,SupplierName,CreditDays,UnBilledAmount,UnBilledAmountPODNotRcvd,NotDueAmount,BilledNotDueAmount,Slot_10Days,Slot_30Days,Slot_60Days,Slot_61Days,Total,HoldAmount,PaymentRecommended,MDApproved,PaidAmount", "dtSuplPayableApproval", "long,string,int,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal");
            grdMain.DataSource = dt;
            grdMain.DataBind();
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            //cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdMain);
            RegisterDateTextBox();
            if (!IsPostBack)
            {
                btnSave.Visible = false;
                btnGenerate.Visible = false;
                if (Session["ROLEID"].ToString() == "8")
                {
                    btnGenerate.Visible = true;
                }
                else
                {
                    if (DateTime.Now.DayOfWeek.ToString().ToLower()=="friday")
                        btnGenerate.Visible = true;
                }                
                FillDropDowns();
                cmbBillingCompany.Focus();
            }
        }

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            setGrid();
            btnShow.Focus();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SupplierPayableApproval_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            CommonBO OptBO = new CommonBO();
            OptBO.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            OptBO.ShowOrGenerate = "S";
            DataTable dt = OptBO.GetSupplierPayableApproval();

            if (dt.Rows.Count == 0)
            {
                OptBO.ShowOrGenerate = "G";
                dt = OptBO.GetSupplierPayableApproval();
            }

            dt.Columns.RemoveAt(0);
            dt.AcceptChanges();

            dt.Rows.Add();
            int RInd = dt.Rows.Count - 1;
            dt.Rows[RInd][0] = "Total";
            //dt.Rows[RInd][1] = "";
            dt.Rows[RInd][2] = dt.Compute("SUM(UnBilledAmount)", "").ToString();
            dt.Rows[RInd][3] = dt.Compute("SUM(UnBilledAmountPODNotRcvd)", "").ToString();
            dt.Rows[RInd][4] = dt.Compute("SUM(NotDueAmount)", "").ToString();
            dt.Rows[RInd][5] = dt.Compute("SUM(BilledNotDueAmount)", "").ToString();
            dt.Rows[RInd][6] = dt.Compute("SUM(Slot_10Days)", "").ToString();
            dt.Rows[RInd][7] = dt.Compute("SUM(Slot_30Days)", "").ToString();
            dt.Rows[RInd][8] = dt.Compute("SUM(Slot_60Days)", "").ToString();
            dt.Rows[RInd][9] = dt.Compute("SUM(Slot_61Days)", "").ToString();
            dt.Rows[RInd][10] = dt.Compute("SUM(Total)", "").ToString();
            dt.Rows[RInd][11] = dt.Compute("SUM(HoldAmount)", "").ToString();
            dt.Rows[RInd][12]= dt.Compute("SUM(PaymentRecommended)", "").ToString();
            dt.Rows[RInd][13] = dt.Compute("SUM(MDApproved)", "").ToString();
            dt.Rows[RInd][14] = dt.Compute("SUM(PaidAmount)", "").ToString();

            dt.Columns[0].ColumnName = "Supplier Name";
            dt.Columns[1].ColumnName = "Credit Days";
            dt.Columns[2].ColumnName = "Unbilled POD Rcvd";
            dt.Columns[3].ColumnName = "Unbilled POD Not Rcvd";
            dt.Columns[4].ColumnName = "Not Due";
            dt.Columns[5].ColumnName = "Billed Not Due";
            dt.Columns[6].ColumnName = "10 Days";
            dt.Columns[7].ColumnName = "11 To 30 Days";
            dt.Columns[8].ColumnName = "31 To 60 Days";
            dt.Columns[9].ColumnName = "61 & Above";
            dt.Columns[10].ColumnName = "Total";
            dt.Columns[11].ColumnName = "Hold";
            dt.Columns[12].ColumnName = "Payment Recommended";
            dt.Columns[13].ColumnName = "Amount Aprroved By MD";
            dt.Columns[14].ColumnName = "Paid Amount";

            dt.AcceptChanges();

            ExportToExcel(dt, filename);

            //if (grdMain.Rows.Count == 0)
            //{
            //    return;
            //}
            //Response.Clear();
            //Response.Buffer = true;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.Charset = "";
            //string FileName = "SupplierPayableApproval_" + DateTime.Now + ".xls";
            //StringWriter strwritter = new StringWriter();
            //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            //grdMain.GridLines = GridLines.Both;
            //for (int i = 0; i < grdMain.Columns.Count; i++)
            //{
            //    grdMain.HeaderRow.Cells[i].Font.Bold = true;
            //    grdMain.HeaderRow.Cells[i].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            //    grdMain.HeaderRow.Cells[i].ForeColor = System.Drawing.Color.White;
            //}
            //grdMain.RowStyle.Height = 20;
            ////grdMain.FooterStyle.Font.Bold = true;
            ////grdMain.FooterStyle.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            ////grdMain.FooterStyle.ForeColor = System.Drawing.Color.White;
            //grdMain.RenderControl(htmltextwrtter);
            //Response.Write(strwritter.ToString());
            //Response.End();

        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                //objWorksheet.Cells["B:B"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["Q:Q"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:O1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:O1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }
                using (ExcelRange objRange = objWorksheet.Cells["A1:O1".Replace("1", (RCnt + 1).ToString())])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                }
                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        private void ExportToExcelDetail(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["E:E"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:M1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:V1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }
                for (int i = 0; i < ds.Rows.Count; i++)
                {
                    if (decimal.Parse(ds.Rows[i]["BalanceAmount"].ToString()) <0)
                    {
                        using (ExcelRange objRange = objWorksheet.Cells["A" + (i + 2).ToString() + ":S" + (i + 2).ToString()])
                        {
                            objRange.Style.Font.Color.SetColor(System.Drawing.Color.Red);
                            //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                            //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                        }
                    }
                }
                //using (ExcelRange objRange = objWorksheet.Cells["A1:J1".Replace("1", (RCnt + 1).ToString())])
                //{
                //    objRange.Style.Font.Bold = true;
                //    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                //    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                //    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                //}
                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }        


        private void ExportToExcel(DataTable dt, string fileName, string worksheetName)
        {
            //Response.Clear();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWriter);
            DataGrid dataExportExcel = new DataGrid();
            dataExportExcel.ItemDataBound += new DataGridItemEventHandler(dataExportExcel_ItemDataBound);
            dataExportExcel.DataSource = dt;
            dataExportExcel.DataBind();
            dataExportExcel.RenderControl(htmlWrite);
            StringBuilder sbResponseString = new StringBuilder();
            sbResponseString.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"> <head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>" + worksheetName + "</x:Name><x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head> <body>");
            sbResponseString.Append(stringWriter + "</body></html>");
            Response.Write(sbResponseString.ToString());
            Response.End();
        }

        void dataExportExcel_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Header Text Format can be done as follows
                e.Item.Font.Bold = true;

                //Adding Filter/Sorting functionality for the Excel
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    e.Item.Cells[cellIndex].Attributes.Add("x:autofilter", "all");
                    e.Item.Cells[cellIndex].Width = 160;
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Center;
                    cellIndex++;
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    //Any Cell specific formatting should be done here
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Left;
                    cellIndex++;
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {            
            LoadGridView("S");
            btnSave.Visible = true;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            LoadGridView("G");
            btnSave.Visible = true;
        }

        private void LoadGridView(string ShowORGenerate)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            CommonBO OptBO = new CommonBO();
            OptBO.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            OptBO.ShowOrGenerate = ShowORGenerate;
            DataTable dt = OptBO.GetSupplierPayableApproval();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();

            decimal total = 0; ;
            grdMain.FooterRow.Cells[0].Text = "Total";
            grdMain.FooterRow.Cells[0].Font.Bold = true;
            grdMain.FooterRow.Cells[0].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[0].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Left;

            for (int k = 2; k < 12; k++)
            {
                total = dt.AsEnumerable().Sum(row => row.Field<Decimal>(dt.Columns[k + 1].ToString()));

                grdMain.FooterRow.Cells[k].Text = total.ToString().Replace(".00", "");
                grdMain.FooterRow.Cells[k].Font.Bold = true;
                grdMain.FooterRow.Cells[k].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                grdMain.FooterRow.Cells[k].ForeColor = System.Drawing.Color.White;
                grdMain.FooterRow.Cells[k].HorizontalAlign = HorizontalAlign.Right;
            }
            //TextBox txtHoldTotal = (TextBox)grdMain.FooterRow.FindControl("txtHoldAmt");
            //txtHoldTotal.Text = dt.Compute("SUM(HoldAmount)", "").ToString();
            TextBox txtPRTotal = (TextBox)grdMain.FooterRow.FindControl("txtPRTotal");
            txtPRTotal.Text = dt.Compute("SUM(PaymentRecommended)", "").ToString();
            TextBox txtMDTotal = (TextBox)grdMain.FooterRow.FindControl("txtMDTotal");
            txtMDTotal.Text = dt.Compute("SUM(MDApproved)", "").ToString();

            for (int i = 0; i < grdMain.Rows.Count; i++)
            {
                TextBox txtPaymentRecommended = (TextBox)grdMain.Rows[i].FindControl("txtPaymentRecommended");
                TextBox txtMDAmount = (TextBox)grdMain.Rows[i].FindControl("txtMDAmount");
                if (Session["ROLEID"].ToString() == "15")
                {
                    txtMDAmount.Text = txtPaymentRecommended.Text;
                    txtMDAmount.Enabled = true;
                    txtPaymentRecommended.Enabled = false;
                }
                else
                {
                    txtMDAmount.Enabled = false;
                    txtPaymentRecommended.Enabled = true;
                }
            }            
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (Session["Item_Expr"] != null)
                    dataView.Sort = Session["Item_Expr"].ToString() + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        protected void grdMain_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Session["Item_Sort"] == null)
            {
                Session["Item_Sort"] = "ASC";
            }
            else
            {
                if (Session["Item_Sort"].ToString() == "ASC")
                    Session["Item_Sort"] = "DESC";
                else
                    Session["Item_Sort"] = "ASC";
            }
            Session["Item_Expr"] = e.SortExpression;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 1; i < 8; i++)
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(".00", "");
                }
            }
        }

        protected void grdMain_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    GridView HeaderGrid = (GridView)sender;
            //    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            //    TableCell HeaderCell = new TableCell();
            //    HeaderCell.Text = "";
            //    HeaderCell.ColumnSpan = 1;
            //    HeaderCell.BackColor = System.Drawing.Color.White;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "POD Received";
            //    HeaderCell.ColumnSpan = 2;
            //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            //    HeaderCell.VerticalAlign = VerticalAlign.Middle;
            //    HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            //    HeaderCell.ForeColor = System.Drawing.Color.White;
            //    HeaderCell.Font.Bold = true;
            //    HeaderCell.Height = 30;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "POD Not Received";
            //    HeaderCell.ColumnSpan = 2;
            //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            //    HeaderCell.VerticalAlign = VerticalAlign.Middle;
            //    HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            //    HeaderCell.ForeColor = System.Drawing.Color.White;
            //    HeaderCell.Font.Bold = true;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "";
            //    HeaderCell.ColumnSpan = 1;
            //    HeaderCell.BackColor = System.Drawing.Color.White;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "Received performance in %";
            //    HeaderCell.ColumnSpan = 2;
            //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            //    HeaderCell.VerticalAlign = VerticalAlign.Middle;
            //    HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            //    HeaderCell.ForeColor = System.Drawing.Color.White;
            //    HeaderCell.Font.Bold = true;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "Not Received performance in %";
            //    HeaderCell.ColumnSpan = 2;
            //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            //    HeaderCell.VerticalAlign = VerticalAlign.Middle;
            //    HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            //    HeaderCell.ForeColor = System.Drawing.Color.White;
            //    HeaderCell.Font.Bold = true;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    grdMain.Controls[0].Controls.AddAt(0, HeaderGridRow);

            //}
        }

        protected void grdMain_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "SuplInfo")
            {
                ReportBO rptBo = new ReportBO();               
                rptBo.SupplierId = long.Parse(keys["SupplierId"].ToString());
                rptBo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                DataTable dt = rptBo.GetSupplierPayableLink();

                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + e.CommandName + "SupplierPayable_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

                ExportToExcelDetail(dt, filename);
            }
        }

        private string GetItemXML()
        {
            string strxml = string.Empty;
            string SupplierId = string.Empty;
            string SupplierName = string.Empty;
            string PaidAmount = string.Empty;
            string Slot_10Days = string.Empty;
            string Slot_30Days = string.Empty;
            string Slot_60Days = string.Empty;
            string Slot_61Days = string.Empty;


            for (int i = 0; i < grdMain.Rows.Count; i++)
            {
                TextBox txtTotalAmt = (TextBox)grdMain.Rows[i].FindControl("txtTotalAmt");
                TextBox txtPaymentRecommended = (TextBox)grdMain.Rows[i].FindControl("txtPaymentRecommended");
                TextBox txtMDAmount = (TextBox)grdMain.Rows[i].FindControl("txtMDAmount");


                if (txtTotalAmt.Text == string.Empty) txtTotalAmt.Text = "0";
                if (txtPaymentRecommended.Text == string.Empty) txtPaymentRecommended.Text = "0";
                if (txtMDAmount.Text == string.Empty) txtMDAmount.Text = "0";


                DataKey key = grdMain.DataKeys[i];
                SupplierId = key["SupplierId"].ToString();
                SupplierName = key["SupplierName"].ToString();
                PaidAmount = key["PaidAmount"].ToString();
                Slot_10Days = key["Slot_10Days"].ToString();
                Slot_30Days = key["Slot_30Days"].ToString();
                Slot_60Days = key["Slot_60Days"].ToString();
                Slot_61Days = key["Slot_61Days"].ToString();

                strxml = strxml + "<dtSuplPayableApproval><SupplierId>" + SupplierId + "</SupplierId><SupplierName>" + SupplierName + "</SupplierName><Slot_10Days>" + Slot_10Days + "</Slot_10Days><Slot_30Days>" + Slot_30Days + "</Slot_30Days><Slot_60Days>" + Slot_60Days + "</Slot_60Days><Slot_61Days>" + Slot_61Days + "</Slot_61Days><Total>" + txtTotalAmt.Text + "</Total><PaymentRecommended>" + txtPaymentRecommended.Text + "</PaymentRecommended><MDApproved>" + txtMDAmount.Text + "</MDApproved><PaidAmount>" + PaidAmount + "</PaidAmount></dtSuplPayableApproval>";
            }

            if (strxml != String.Empty)
            {
                strxml = "<NewDataSet>" + strxml + "</NewDataSet>";
            }

            return strxml;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CommonBO optbo = new CommonBO();
            optbo.BillingCompanySrl = int.Parse(cmbBillingCompany.SelectedValue);
            optbo.strItem = GetItemXML().Replace("&","and");
             long cnt = optbo.InsertSupplierPayableApproval();
             if (cnt > 0)
             {
                 SetPanelMsg("Supplier Payable Updated Successfully", true, 1);
                 setGrid();
                 cmbBillingCompany.Focus();
             }
        }        
    }
}