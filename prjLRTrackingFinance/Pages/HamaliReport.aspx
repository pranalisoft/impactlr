﻿<%@ Page Title="Hamali Report" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="HamaliReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.HamaliReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Hamali Invoice Report
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                                <asp:HiddenField ID="txtRowNumber" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="Hamali Invoice" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" Text="Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton5"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="ImageButton5" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="80px">
                                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="3" ContentEditable="False"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                            </td>
                                            <td width="80px">
                                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label5" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td style="width: 230px">
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="220px" MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                            <td width="60px" style="padding-top: 3px">
                                                <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td width="220px">
                                                <asp:TextBox ID="txtSearch" runat="server" TabIndex="6" MaxLength="10" Width="220px"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td width="80px">
                                                <asp:Button TabIndex="7" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                                    OnClick="btnSearch_Click" />
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="grid_region" style="height: 500px; overflow: auto">
                                        <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                            AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                            DataKeyNames="InvoiceNo,InvoiceDate,Name,InvoiceAmount,BillingCompany_Srl,InvRefNo"
                                            OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowCommand="grdPacklistView_RowCommand">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                    ItemStyle-Width="200px" />
                                                <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                    ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                    <ItemStyle Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" />
                                                <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                    ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                                <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="50px"
                                                    ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                            <PagerSettings Mode="Numeric" />
                                        </asp:GridView>
                                    </div>
                                    <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                        <div class="grid_top_region">
                                            <div class="grid_top_region_lft">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                Font-Size="13px" Font-Underline="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="grid_top_region_rght">
                                                Records :
                                                <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                            </div>
                                        </div>
                                        <div class="grid_region">
                                            <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                DataKeyNames="LRNo,LRDate,TransportationCharges,DetentionCharges,WaraiCharges,TwoPointCharges,OtherCharges,TotalCharges"
                                                EmptyDataText="No records found." Width="50%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                                <Columns>
                                                    <asp:BoundField DataField="InvRefNo" HeaderText="Ref. No." SortExpression="InvRefNo">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="100px"
                                                        DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="TransportationCharges" HeaderText="Basic Charges" SortExpression="TransportationCharges"
                                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="TotalCharges" HeaderText="Total Charges" SortExpression="TotalCharges"
                                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="13" ID="btnSavePacklist" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="14" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label1" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDateView" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDateView" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton3"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton3" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label2" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDateView" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDateView" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton4"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton4" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label3" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearchView" runat="server" TabIndex="1" MaxLength="10" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="2" ID="btnSearchView" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearchView_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="7" ID="btnAddPacklist" runat="server" Text="Add" CssClass="button"
                                            CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                        <asp:Button TabIndex="8" ID="btnDeletePacklist" runat="server" Text="Delete" CssClass="button"
                                            CommandName="Delete" OnCommand="EntryForm_Command" Visible="False" />&nbsp;
                                        &nbsp;
                                        <asp:Button TabIndex="9" ID="Button3" runat="server" Text="Goto Main Page" CssClass="button"
                                            OnClick="btnMainPg_Click" Style="width: 120px" />
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Label4" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="height: 500px; overflow: auto">
                                <asp:GridView ID="grdInvoiceView" runat="server" Width="670px" AutoGenerateColumns="False"
                                    AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="Srl,InvoiceNo,InvoiceDate,InvoiceAmount,InvRefNo,BillingCompany_Srl" OnPageIndexChanging="grdInvoiceView_PageIndexChanging"
                                    OnRowDataBound="grdInvoiceView_RowDataBound" OnRowCommand="grdInvoiceView_RowCommand"
                                    AllowSorting="True" OnSorting="grdInvoiceView_Sorting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                            ItemStyle-Width="200px">
                                            <ItemStyle Width="200px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                            ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right">
                                            <ItemStyle Width="100px" />
                                        </asp:BoundField>
                                        <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="ViewReport" Text="Document">
                                            <ItemStyle Width="80px" />
                                        </asp:ButtonField>
                                        <asp:ButtonField CommandName="ItemReport" Text="Annexure">
                                            <ItemStyle Width="80px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                                <asp:Panel ID="pnlInvoiceDetails" runat="server" Visible="false">
                                    <div class="grid_top_region">
                                        <div class="grid_top_region_lft">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblTotalInvoice" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                            Font-Size="13px" Font-Underline="True"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%--<div class="grid_top_region_rght">
                                            Records :
                                            <asp:Label ID="Label7" Text="0" runat="server" />
                                        </div>--%>
                                    </div>
                                    <div class="grid_region">
                                        <asp:GridView ID="grdInvoiceDetails" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="InvoiceId,Srl" EmptyDataText="No records found." Width="670px" PageSize="1000"
                                            ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                    ItemStyle-Width="200px" />
                                                <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                    ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                    <ItemStyle Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" />
                                                <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                    ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
