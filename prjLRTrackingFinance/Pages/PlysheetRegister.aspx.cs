﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PlysheetRegister : System.Web.UI.Page
    {
        private void LoadGridView()
        {
            CommonBO optBO = new CommonBO();            
            DataTable dt = optBO.FillPlysheetRegister();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalRecords.Text = dt.Rows.Count.ToString();
        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    //objWorksheet.Cells["E:E"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["X:X"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    ////objWorksheet.Cells["AC:AC"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["AD:AD"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["AE:AE"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["AM:AM"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["AU:AU"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells["BK:BK"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["A1:E1"].AutoFilter = true;
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            //RegisterDateTextBox();            
            if (!IsPostBack)
            {              
                LoadGridView();
                btnExcel.Focus();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\PlysheetRegister_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            CommonBO OptBO = new CommonBO();
            DataTable dt = OptBO.FillPlysheetRegister();
            DataSet ds = new DataSet();            
            ds.Tables.Add(dt);
            ExportToExcel(ds, filename);
        }
    }
}