﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerPTLItemMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {               
                FillDropDownCustomer();
                FillDropDownVehicleType();
                FillDropDownFrom();
                FillDropDownTo();
                clearAll();
                cmbCustomer.Focus();
            }
        }

        private void FillDropDownCustomer()
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            CustomerBO billingBO = new CustomerBO();
            billingBO.SearchText = string.Empty;
            billingBO.ShowAll = "N";
            DataTable dt = billingBO.GetCustomerDetails();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 1;
        }

        private void FillDropDownZoneType(long CustomerId)
        {
            cmbZoneType.DataSource = null;
            cmbZoneType.Items.Clear();
            cmbZoneType.Items.Add(new ListItem("", "-1"));
            CustomerPTLItemMasterBO optbo = new CustomerPTLItemMasterBO();
            optbo.CustomerId = CustomerId;
            DataTable dt = optbo.GetZoneType();
            cmbZoneType.DataSource = dt;
            cmbZoneType.DataTextField = "ZoneType";
            cmbZoneType.DataValueField = "ZoneTypeId";
            cmbZoneType.DataBind();
            if (cmbZoneType.Items.Count>1)
                cmbZoneType.SelectedIndex = 1;
        }

        private void FillDropDownVehicleType()
        {
            VehicleTypeBO OptBO = new VehicleTypeBO();
            DataTable dt;
            cmbVehicleType.DataSource = null;
            cmbVehicleType.Items.Clear();
            cmbVehicleType.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetVehicleTypedetails();
            cmbVehicleType.DataSource = dt;
            cmbVehicleType.DataTextField = "Name";
            cmbVehicleType.DataValueField = "Srl";
            cmbVehicleType.DataBind();
            cmbVehicleType.SelectedIndex = 0;
        }

        private void FillDropDownFrom()
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbFrom.DataSource = null;
            cmbFrom.Items.Clear();
            cmbFrom.Items.Add(new ListItem("", "-1"));
            OptBO.FromOrTo = "F";
            dt = OptBO.GetFromOrToLocationsPTL();
            cmbFrom.DataSource = dt;
            cmbFrom.DataTextField = "FromLoc";
            cmbFrom.DataValueField = "FromLoc";
            cmbFrom.DataBind();
            cmbFrom.SelectedIndex = 0;
        }

        private void FillDropDownTo()
        {
            DestinationBO OptBO = new DestinationBO();
            DataTable dt;
            cmbTo.DataSource = null;
            cmbTo.Items.Clear();
            cmbTo.Items.Add(new ListItem("", "-1"));
            OptBO.FromOrTo = "T";
            dt = OptBO.GetFromOrToLocationsPTL();
            cmbTo.DataSource = dt;
            cmbTo.DataTextField = "ToLoc";
            cmbTo.DataValueField = "ToLoc";
            cmbTo.DataBind();
            cmbTo.SelectedIndex = 0;
        }

        private void clearAll()
        {
            txtHiddenId.Value = "0";
            cmbCustomer.SelectedIndex = 0;
            cmbZoneType.SelectedIndex = 0;
            cmbVehicleType.SelectedIndex = 0;
            cmbFrom.SelectedIndex = 0;
            cmbTo.SelectedIndex = 0;
            txtItem.Text = "";
            txtSlab.Text = "";
            txtRatePerQtySelling.Text = "";
            txtRatePerKgSelling.Text = "";
            txtRatePerQtyBuying.Text = "";
            txtRatePerKgBuying.Text = "";
            chkActive.Checked = true;
            chkFixedSelling.Checked = false;
            chkFixedBuying.Checked = false;
            cmbZoneType.Focus();
        }

        private void clearAllWOCustomer()
        {
            txtHiddenId.Value = "0";
            cmbZoneType.SelectedIndex = 0;
            cmbVehicleType.SelectedIndex = 0;
            cmbFrom.SelectedIndex = 0;
            cmbTo.SelectedIndex = 0;
            txtItem.Text = "";
            txtSlab.Text = "";
            txtRatePerQtySelling.Text = "";
            txtRatePerKgSelling.Text = "";
            txtRatePerQtyBuying.Text = "";
            txtRatePerKgBuying.Text = "";
            chkActive.Checked = true;
            cmbZoneType.Focus();
        }   

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid(long CustomerId)
        {
            try
            {
                CustomerPTLItemMasterBO optbo = new CustomerPTLItemMasterBO();
                optbo.SearchText = txtSearch.Text.Trim();
                optbo.CustomerId = CustomerId;
                DataTable dt = optbo.GetCustomerPTLItemMaster();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    FillDropDownCustomer();
                    FillDropDownVehicleType();
                    FillDropDownFrom();
                    FillDropDownTo();
                    clearAllWOCustomer();
                    txtHiddenId.Value = keys["Srl"].ToString();
                    cmbZoneType.SelectedValue = keys["ZoneTypeId"].ToString();
                    cmbCustomer.SelectedValue = keys["CustomerId"].ToString();
                    if (keys["FromLoc"] != null && keys["FromLoc"].ToString() != "")
                    {
                        cmbFrom.SelectedValue = keys["FromLoc"].ToString();
                    }
                    else
                    {
                        cmbFrom.SelectedIndex = 0;
                    }
                    if (keys["ToLoc"] != null && keys["ToLoc"].ToString() != "")
                    {
                        cmbTo.SelectedValue = keys["ToLoc"].ToString();
                    }
                    else
                    {
                        cmbTo.SelectedIndex = 0;
                    }
                    if (keys["VehicleTypeId"] != null && keys["VehicleTypeId"].ToString() != "")
                    {
                        cmbVehicleType.SelectedValue = keys["VehicleTypeId"].ToString();
                    }
                    else
                    {
                        cmbVehicleType.SelectedIndex = 0;
                    }
                    txtItem.Text = keys["Item"].ToString();
                    txtSlab.Text = keys["Slab"].ToString();
                    txtRatePerQtySelling.Text = keys["RatePerQtySelling"].ToString().Replace(".00","");
                    txtRatePerQtyBuying.Text = keys["RatePerQtyBuying"].ToString().Replace(".00", "");
                    txtRatePerKgSelling.Text = keys["RatePerKgSelling"].ToString().Replace(".00", "");
                    txtRatePerKgBuying.Text = keys["RatePerKgBuying"].ToString().Replace(".00", "");
                    txtMOQ.Text = keys["MOQ"].ToString().Replace(".000", "").Replace(".00", "");
                    if (keys["IsFixedRateBuying"] != null && keys["IsFixedRateBuying"].ToString() != "")
                    {
                        if (keys["IsFixedRateBuying"].ToString().Trim().ToLower() == "0" || keys["IsFixedRateBuying"].ToString().Trim().ToLower() == "false")
                        {
                            chkFixedBuying.Checked = false;
                        }
                        else
                        {
                            chkFixedBuying.Checked = true;
                        }
                    }

                    if (keys["IsFixedRateSelling"] != null && keys["IsFixedRateSelling"].ToString() != "")
                    {
                        if (keys["IsFixedRateSelling"].ToString().Trim().ToLower() == "0" || keys["IsFixedRateSelling"].ToString().Trim().ToLower() == "false")
                        {
                            chkFixedSelling.Checked = false;
                        }
                        else
                        {
                            chkFixedSelling.Checked = true;
                        }
                    }
                    chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                    cmbZoneType.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            if (cmbCustomer.Items.Count > 1)
                FillGrid(long.Parse(cmbCustomer.SelectedValue));
            else
                FillGrid(0);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.Items.Count > 1)
                FillGrid(long.Parse(cmbCustomer.SelectedValue));
            else
                FillGrid(0);
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            if (cmbCustomer.Items.Count > 1)
                FillGrid(long.Parse(cmbCustomer.SelectedValue));
            else
                FillGrid(0);
            txtSearch.Focus();
        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex > 0)
            {
                FillGrid(long.Parse(cmbCustomer.SelectedValue));
                FillDropDownZoneType(long.Parse(cmbCustomer.SelectedValue));                
            }
            else
            {
                FillGrid(0);
                FillDropDownZoneType(0);
            }
            cmbZoneType.Focus();
        }     

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerPTLItemMasterBO optbo = new CustomerPTLItemMasterBO();

                optbo.Srl = long.Parse(txtHiddenId.Value);
                optbo.ZoneTypeId =long.Parse(cmbZoneType.SelectedValue);
                optbo.CustomerId = long.Parse(cmbCustomer.SelectedValue);
                optbo.Item = txtItem.Text.Trim();
                optbo.Slab = txtSlab.Text.Trim();
                optbo.VehicleTypeId = cmbVehicleType.SelectedIndex == 0 ? 0 : long.Parse(cmbVehicleType.SelectedValue);
                optbo.RatePerKgBuying =txtRatePerKgBuying.Text.Trim()==""?0: decimal.Parse(txtRatePerKgBuying.Text);
                optbo.RatePerKgSelling = txtRatePerKgSelling.Text.Trim() == "" ? 0 : decimal.Parse(txtRatePerKgSelling.Text);
                optbo.RatePerQtyBuying = txtRatePerQtyBuying.Text.Trim() == "" ? 0 : decimal.Parse(txtRatePerQtyBuying.Text);
                optbo.RatePerQtySelling = txtRatePerQtySelling.Text.Trim() == "" ? 0 : decimal.Parse(txtRatePerQtySelling.Text);
                optbo.MOQ = txtMOQ.Text.Trim() == "" ? 0 : decimal.Parse(txtMOQ.Text);
                optbo.FromLoc = cmbFrom.SelectedItem.Text.Trim();
                optbo.ToLoc = cmbTo.SelectedItem.Text.Trim();
                optbo.IsActive = chkActive.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                optbo.IsFixedRateSelling = chkFixedSelling.Checked ? 1 : 0;
                optbo.IsFixedRateBuying = chkFixedBuying.Checked ? 1 : 0;
                long flgCnt = 0;
                flgCnt = optbo.InsertUpdateCustomerPTLItemMaster();

                if (long.Parse(txtHiddenId.Value) == 0)
                {
                    SetPanelMsg("Item added successfully.", true, 1);
                }
                else
                {
                    SetPanelMsg("Item updated successfully.", true, 1);
                }
                clearAllWOCustomer();
                txtSearch.Text = string.Empty;
                FillGrid(long.Parse(cmbCustomer.SelectedValue));

            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearAll();
                cmbZoneType.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }       
    }
}