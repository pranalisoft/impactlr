﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using CrystalDecisions;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class HamaliReport : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void ClearView()
        {
            txtDate.Text = string.Empty;            
            SetPanelMsg("", false, 0);
            if (cmbBillingCompany.Items.Count > 0) cmbBillingCompany.SelectedIndex = 0; 
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 1;
        }

        private void LoadGridView()
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            DataTable dt = optBO.FillInvoiceGrid_Hamali();
            grdInvoiceView.DataSource = dt;
            grdInvoiceView.DataBind();
            //lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDetails(long Srl)
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.Srl = Srl;
            DataTable dt = optBO.FillInvoiceDetails_Hamali();
            grdInvoiceDetails.DataSource = dt;
            grdInvoiceDetails.DataBind();
        }        

        private void LoadGridViewAdd()
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            DataTable dt = optBO.FillFinalInvoiceGrid_Hamali();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            //lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDetailsAdd(string InvRefNo, long BillingCompanySrl)
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.InvRefNo = InvRefNo;
            optBO.BillingCompanySrl = BillingCompanySrl;
            DataTable dt = optBO.FillMainInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        public string CkeckedSrls(long Srl)
        {
            string strcode = string.Empty;

            InvoiceBO optBO = new InvoiceBO();
            optBO.Srl = Srl;
            DataTable dt = optBO.FillInvoiceDetails_Hamali();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (strcode == "")
                            strcode = dt.Rows[i]["InvoiceId"].ToString();
                        else
                            strcode = strcode + "," + dt.Rows[i]["InvoiceId"].ToString();
            } 
            return strcode;
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                FillDropDowns();
                LoadGridView();
                txtSearch.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridViewAdd();
        }        

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            InvoiceBO optbo = new InvoiceBO();
            NumberToWords wordBO = new NumberToWords();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillDropDowns();
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    DataTable dt = new DataTable("dtItem");
                    dt.Columns.Add("InvoiceId");
                    dt.Columns.Add("Amount",typeof(decimal));

                    for (int i = 0; i < grdPacklistView.Rows.Count; i++)
                    {
                        CheckBox chk = (CheckBox)grdPacklistView.Rows[i].FindControl("chkSelect");

                        DataKey key = grdPacklistView.DataKeys[i];
                        if (chk.Checked)
                        {
                            dt.Rows.Add();
                            dt.Rows[dt.Rows.Count - 1]["InvoiceId"] = key["InvoiceNo"].ToString();
                            dt.Rows[dt.Rows.Count - 1]["Amount"] = key["InvoiceAmount"].ToString();
                        }
                    }
                    dt.AcceptChanges();

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    string ItemXML = ds.GetXml();
                    ds.Tables.RemoveAt(0);

                    optbo.Srl = 0;                    
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.EnteredBy = long.Parse(Session["EID"].ToString());
                    optbo.ItemXML = ItemXML;
                    optbo.BasicAmount = decimal.Parse(dt.Compute("sum(Amount)", "").ToString());
                    optbo.PaymentTerms ="";// txtPaymentTerms.Text.Trim();
                    optbo.Particulars = "";//txtParticulars.Text.Trim();
                    optbo.AmountInWords = wordBO.Num2WordConverter(optbo.BasicAmount.ToString()).ToString();                    
                    optbo.InvoiceDate = txtDate.Text.Trim();
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                   
                    long cnt = optbo.InsertInvoice_Hamali();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Invoice details updated Successfully. Now you can Print the Invoice!!", true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Invoice Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridViewAdd();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For Invoice No.:" + keys["InvRefNo"].ToString();
                LoadGridViewDetailsAdd(keys["InvRefNo"].ToString(), long.Parse(keys["BillingCompany_Srl"].ToString()));
            }           
        }

        protected void btnSearchView_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdInvoiceView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdInvoiceView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearchView.Focus();
        }

        protected void grdInvoiceView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Item")
            {
                pnlInvoiceDetails.Visible = true;
                lblTotalInvoice.Text = "Details For Invoice No.:" + keys["InvRefNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["Srl"].ToString()));
            }
            else if (e.CommandName == "ViewReport")
            {
                string CheckedSrls = CkeckedSrls(long.Parse(keys["Srl"].ToString()));
                if (CheckedSrls != string.Empty)
                {
                    Session["InvSrls"] = CheckedSrls;
                    Session["BillingCompSrlDoc"] = keys["BillingCompany_Srl"].ToString();
                    Session["ReportType"] = "Hamali";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
            else if (e.CommandName == "ItemReport")
            {
                string CheckedSrls = CkeckedSrls(long.Parse(keys["Srl"].ToString()));
                if (CheckedSrls != string.Empty)
                {
                    Session["InvSrls"] = CheckedSrls;
                    Session["InvYear"] = DateTime.Parse(keys["InvoiceDate"].ToString()).Year;
                    Session["BillingCompSrlDoc"] = keys["BillingCompany_Srl"].ToString();
                    Session["ReportType"] = "HamaliAnn";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
        }

        protected void grdInvoiceView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdInvoiceView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
    }
}