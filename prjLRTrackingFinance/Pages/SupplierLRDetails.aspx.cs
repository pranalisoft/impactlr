﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierLRDetails : System.Web.UI.Page
    {       
        private void FillComboSupplier()
        {
            SupplierBO OptBO = new SupplierBO();
            DataTable dt;
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            OptBO.SearchText = string.Empty;
            dt = OptBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void clearGrid()
        {
            DataTable dt = null;
            grdMain.DataSource = dt;
            grdMain.DataBind();
        }

        private DataTable GetData()
        {
            ReportBO optBO = new ReportBO();
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.SupplierId = long.Parse(cmbSupplier.SelectedValue);
            DataTable dt = optBO.SupplierLRDetails();

            if (dt.Rows.Count > 0)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    for (int i = 30; i < dt.Columns.Count; i++)
                    {
                        if (dt.Rows[j][i] == null || dt.Rows[j][i].ToString()=="")
                        dt.Rows[j][i] = 0;
                    }
                }
                dt.AcceptChanges();

                dt.Columns[25].SetOrdinal(dt.Columns.Count - 1);
                dt.AcceptChanges();

                dt.Columns[25].SetOrdinal(dt.Columns.Count - 1);
                dt.AcceptChanges();

                dt.Columns[25].SetOrdinal(dt.Columns.Count - 1);
                dt.AcceptChanges();

                dt.Columns[25].SetOrdinal(dt.Columns.Count - 1);
                dt.AcceptChanges();

                dt.Columns[25].SetOrdinal(dt.Columns.Count - 1);
                dt.AcceptChanges();  
            }
            return dt;
        }

        private void FillGrid()
        {
            DataTable dt = GetData();
            grdMain.DataSource = dt;
            grdMain.DataBind();            
        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["M:M"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AR:AR"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            if (!IsPostBack)
            {
                FillComboSupplier();
                cmbSupplier.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {            
            DataTable dt = GetData();
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\"+ cmbSupplier.SelectedValue +"_LRDetails_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            ExportToExcel(ds, filename);
        }
    }
}