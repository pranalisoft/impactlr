﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class LRMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                txtSearch.Focus();
            }

        }

        private void LoadGridView()
        {
            LRBO optBO = new LRBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.IsPending = true;
            DataTable dt = optBO.FillLRGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Pending Records : " + dt.Rows.Count.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {

        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Remarks")
            {
                Session["StatusLRID"] = keys["ID"].ToString();
                Session["StatusLRNo"] = keys["LRNo"].ToString();
                Session["StatusPlacementId"] = keys["PlacementID"].ToString();
                Session["TripId"] = keys["TripId"].ToString();
                Response.Redirect("UpdateLRStatus.aspx");
            }
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "yes":                   
                    msgpopAuth.Hide();
                    break;

                case "no":
                    msgpopAuth.Hide();
                    break;
                default:
                    break;
            }
        }
    }
}
