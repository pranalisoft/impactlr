﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateUnloadingChargesPayment : System.Web.UI.Page
    {

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        private void FillComboLR()
        {
            LRBO OptBO = new LRBO();
            cmbLR.DataSource = null;
            cmbLR.Items.Clear();
            cmbLR.Items.Add(new ListItem("", "-1"));
            DataTable dt = OptBO.FillLRComboUnloadingPayment();
            cmbLR.DataSource = dt;
            cmbLR.DataTextField = "LRNo";
            cmbLR.DataValueField = "ID";
            cmbLR.DataBind();
            cmbLR.SelectedIndex = 0;
        }

        private void ClearInfoPanel()
        {
            lblLRNo.Text = "";
            lblDate.Text = "";
            lblConsigner.Text = "";
            lblFrom.Text = "";
            lblTo.Text = "";
            lblVehicleNo.Text = "";
            lblPackages.Text = "";
            lblWeight.Text = "";
            lblChargeableWeight.Text = "";
            lblInvoiceNo.Text = "";
            lblInvoiceValue.Text = "";
            lblType.Text = "";
            lblRemarks.Text = "";
            lblDriverDetails.Text = "";
            lblDriverMobNo.Text = "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillComboLR();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                cmbLR.Focus();
            }
        }

        protected void cmbLR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearInfoPanel();
                SetPanelMsg("", false, 1);
                LRBO OptBO = new LRBO();                
                OptBO.LRNo = cmbLR.SelectedItem.Text.Trim();
                DataTable dt = OptBO.GetLRHeaderByNoForUnloading();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        lblLRNo.Text = cmbLR.SelectedItem.Text.Trim();
                        lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                        lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                        lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                        lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                        lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                        lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                        lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                        lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                        lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                        lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                        lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                        lblType.Text = dt.Rows[0]["LoadType"].ToString();
                        lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                        lblDriverDetails.Text = dt.Rows[0]["DriverDetails"].ToString();
                        lblDriverMobNo.Text = dt.Rows[0]["DriverNumber"].ToString();
                        txtUnloadingCharges.Text = dt.Rows[0]["UnloadingCharges"].ToString();
                    }
                    else
                    {                        
                        SetPanelMsg("Either LR No. is Invalid Or Invoice is generated for this LR", true, 0);
                        cmbLR.Focus();
                        return;
                    }
                    txtDate.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            SetPanelMsg("", false, 1);

            SupplierPaymentBO custbo = new SupplierPaymentBO();

            custbo.tranDate = txtDate.Text.Trim();
            custbo.LRId = long.Parse(cmbLR.SelectedValue);
            custbo.UnloadingCharges = decimal.Parse(txtUnloadingCharges.Text);
            custbo.userID = long.Parse(Session["EID"].ToString());

            long cnt = custbo.UpdateUnloadingChargesPayment();
            if (cnt > 0)
            {
                ClearInfoPanel();
                txtUnloadingCharges.Text = "";
                FillComboLR();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");                
                SetPanelMsg("Record Updated Successfully !!", true, 1);
                cmbLR.Focus();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        } 
    }
}