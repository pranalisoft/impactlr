﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierDocuments : System.Web.UI.Page
    {
        private void FillDropDownDocumentType()
        {
            cmbDocumentType.DataSource = null;
            cmbDocumentType.Items.Clear();
            cmbDocumentType.Items.Add(new ListItem("", "-1"));

            DocumentTypeBO documentBO = new DocumentTypeBO();
            documentBO.SearchText = string.Empty;
            DataTable dt = documentBO.ShowData();
            cmbDocumentType.DataSource = dt;
            cmbDocumentType.DataTextField = "DocumentTypeName";
            cmbDocumentType.DataValueField = "DocumentTypeId";
            cmbDocumentType.DataBind();
            cmbDocumentType.SelectedIndex = 1;
        }

        private void FillDropDownSupplier()
        {
            cmbSupplier.DataSource = null;
            cmbSupplier.Items.Clear();
            cmbSupplier.Items.Add(new ListItem("", "-1"));
            SupplierBO supplierBO = new SupplierBO();
            supplierBO.SearchText = string.Empty;
            DataTable dt = supplierBO.GetSupplierDetails();
            cmbSupplier.DataSource = dt;
            cmbSupplier.DataTextField = "Name";
            cmbSupplier.DataValueField = "Srl";
            cmbSupplier.DataBind();
            cmbSupplier.SelectedIndex = 0;
        }

        private void clearAll()
        {
            txtHiddenId.Value = "0";
            cmbDocumentType.SelectedIndex = 0;
            //cmbSupplier.SelectedIndex = 0;
            txtRemarks.Text = string.Empty;
            //flUpload. = string.Empty;
            cmbDocumentType.Focus();
        }

        public string GetDynamicPath(string fileName)
        {
            // string dynamicFolderPath = Server.MapPath(ConfigurationManager.AppSettings["PhysicalFilesUploadPath"].ToString()); //dynamic folder
            string dynamicFolderPath = ConfigurationManager.AppSettings["PhysicalFilesUploadPath"].ToString(); //dynamic folder
            return dynamicFolderPath + fileName.ToString();

        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid()
        {
            try
            {
                DocumentTypeBO optbo = new DocumentTypeBO();
                optbo.SupplierId = long.Parse(cmbSupplier.SelectedValue);
                DataTable dt = optbo.ShowSupplierDocs();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private long Delete()
        {
            long srl = 0;
            string ciFilePath = Server.MapPath(ConfigurationManager.AppSettings["PhysicalFilesUploadPath"].ToString());
            string fileNamewithPath = ciFilePath + "\\" + txtHiddenIdFileName.Value;

            DocumentTypeBO pol = new DocumentTypeBO();
            pol.DocumentId = long.Parse(txtHiddenIdDocumentId.Value);
            if (File.Exists(fileNamewithPath))
            {
                File.Delete(fileNamewithPath);
                pol.CreatedBy = long.Parse(Session["EID"].ToString());
                long cnt = pol.DeleteDocument();
                if (cnt > 0)
                {
                    srl = cnt;
                    txtHiddenIdFileName.Value = "";
                    txtHiddenIdDocumentId.Value = "0";
                }
            }  
            return srl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnSave);

            if (!IsPostBack)
            {

                FillDropDownDocumentType();
                FillDropDownSupplier();

                clearAll();
                cmbSupplier.SelectedValue = Session["DocSupplierId"].ToString();
                cmbSupplier.Enabled = false;
                FillGrid();
            }
        }       

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        SetPanelMsg("File deleted Successfully", true, 1);
                        clearAll();
                        FillGrid();
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "DocFile")
                {
                    string ciFilePath = Server.MapPath(ConfigurationManager.AppSettings["PhysicalFilesUploadPath"].ToString());
                    string fileNamewithPath = ciFilePath + "\\" + keys["FileName"].ToString();
                    Session["FilePath"] = fileNamewithPath;
                    Response.Redirect("ViewFile.aspx");
                }
                if (e.CommandName == "DelFile")
                {
                    txtHiddenIdDocumentId.Value = keys["DocumentId"].ToString();
                    txtHiddenIdFileName.Value = keys["FileName"].ToString();
                    popDelPacklist.Show();                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string ciFilePath = Server.MapPath(ConfigurationManager.AppSettings["PhysicalFilesUploadPath"].ToString());

                //code for copy file to selected path.

                string flPath = "";
                if (flUpload.FileName == "")
                {
                    flPath = string.Empty;
                    SetPanelMsg("Please Select File", true, 1);
                }
                else
                {
                    flUpload.SaveAs(ciFilePath + "/" + flUpload.FileName);
                    flPath = ciFilePath + "/" + flUpload.FileName;
                }
                DocumentTypeBO optbo = new DocumentTypeBO();

                optbo.DocumentId = long.Parse(txtHiddenId.Value);
                optbo.DocumentTypeId = int.Parse(cmbDocumentType.SelectedValue);
                optbo.SupplierId = long.Parse(cmbSupplier.SelectedValue);
                optbo.Remarks = txtRemarks.Text.Trim();
                optbo.FileName = flUpload.FileName;
                optbo.Active = true;// chkActive.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());

                long cnt = optbo.InsertDocuments();
                if (cnt > 0)
                {
                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        SetPanelMsg("Document Saved Successfully", true, 1);
                    }
                    else
                    {
                        SetPanelMsg("Document Updated Successfully", true, 1);
                    }
                    clearAll();
                    txtSearch.Text = string.Empty;
                    FillGrid();
                    cmbDocumentType.Focus();
                }

            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearAll();
                cmbDocumentType.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }
    }
}