﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierTypeMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                FillGrid();
                txtHiddenId.Value = "0";
                txtName.Focus();
            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                SetPanelMsg("", false, 0);
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    ClearView();
                    txtHiddenId.Value = keys["Srl"].ToString();
                    txtName.Text = keys["Name"].ToString();
                    chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                    txtName.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ClearView()
        {
            txtName.Text = string.Empty;
            chkActive.Checked = true;
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                SupplierTypeBO optbo = new SupplierTypeBO();
                optbo.Name = txtName.Text.Trim();
                optbo.Srl = long.Parse(txtHiddenId.Value);
                optbo.IsActive = chkActive.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                optbo.Ind = rdbDirect.Checked?"D":"I";

                if (optbo.isSupplierTypeAlreadyExist() == false)
                {
                    long flgCnt = 0;
                    flgCnt = optbo.InsertSupplierType();

                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        ClearView();
                        SetPanelMsg("Record added successfully.", true, 1);
                    }
                    else
                    {
                        ClearView();
                        SetPanelMsg("Record updated successfully.", true, 1);
                    }

                    txtSearch.Text = string.Empty;
                    txtHiddenId.Value = "0";
                    FillGrid();
                    txtName.Focus();

                }
                else
                {
                    SetPanelMsg("Name Already Exists.", true, 0);
                }
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearView();
                txtName.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid()
        {
            try
            {
                SupplierTypeBO optbo = new SupplierTypeBO();
                optbo.SearchText = txtSearch.Text.Trim();
                optbo.ShowAll = "Y";
                DataTable dt = optbo.GetSupplierTypedetails();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void grdMst_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SetPanelMsg("", false, 0);
            int cnt1 = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    if (rowcheck.Checked)
                    {
                        cnt1 = cnt1 + 1;
                        break;
                    }
                }
            }
            if (cnt1 > 0)
            {
                popDelPacklist.Show();
            }
            else
            {
                SetPanelMsg("Please select atleast one record to delete", true, 0);
            }
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        FillGrid();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        private long Delete()
        {
            SupplierTypeBO OptBO = new SupplierTypeBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdMst.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteSupplierType();
            }

            return srl;
        }
    }
}