﻿<%@ Page Title="Supplier Payable" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="SupplierPayable.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierPayable" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Supplier Payable Report
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <%--<td width="30px">
                                        <asp:Label ID="Label18" Text="From" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                    </td>
                                    <td width="30px">
                                        <asp:Label ID="Label17" Text="To" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                    </td>--%>
                                    <td width="50px">
                                        <asp:Label ID="lbl" runat="server" Text="Supplier" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="250px">
                                        <asp:DropDownList ID="cmbSupplier" runat="server" CssClass="NormalTextBold" TabIndex="5"
                                            Width="250px" AppendDataBoundItems="True" >
                                        </asp:DropDownList>
                                    </td>
                                    <td width="50px">
                                        <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" ValidationGroup="show"/>
                                    </td>
                                    <td width="50px">
                                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                            OnClick="btnExcel_Click" ValidationGroup="show"/>
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label1" Text="Total Records : " runat="server" CssClass="NormalTextBold"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="lblTotalRecords" Text="0" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="overflow: auto">
                                <asp:GridView ID="grdView" runat="server" Width="100%" AutoGenerateColumns="false"
                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="LRNo,LRDate,Billing_Company,Branch,Supplier,Supplier_Invoice_No,Supplier_Invoice_Date,POD_Rcvd_Date,
                                    Overdue_Days,Destination,VehicleNo,TotalFrieght,DetentionCharges,UnloadingCharges,OtherCharges,TotalAmount,AdvancePaid,TDS,OtherDuduction,
                                    Balance,PartPayment,Net_Payable" ShowFooter="True">
                                    <Columns>
                                        <asp:BoundField DataField="Supplier" HeaderText="Supplier" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="LRNo" HeaderText="LR No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="LRDate" HeaderText="Date" ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Billing_Company" HeaderText="Billing Company" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="Branch" HeaderText="Branch" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="Supplier_Invoice_No" HeaderText="Supplier Invoice No" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="Supplier_Invoice_Date" HeaderText="Supplier Invoice Date" ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="POD_Rcvd_Date" HeaderText="POD Rcvd Date" ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Overdue_Days" HeaderText="Overdue Days" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Destination" HeaderText="Destination" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="VehicleNo" HeaderText="Vehicle No" ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="TotalFrieght" HeaderText="Frieght" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="DetentionCharges" HeaderText="Detention" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="UnloadingCharges" HeaderText="Unloading" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="OtherCharges" HeaderText="Other" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="AdvancePaid" HeaderText="Advance Paid" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="TDS" HeaderText="TDS" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="OtherDuduction" HeaderText="Other Duduction" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Balance" HeaderText="Balance" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PartPayment" HeaderText="Part Payment" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Net_Payable" HeaderText="Net Payable" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PetrolPump" HeaderText="Petrol Pump" ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Left" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
