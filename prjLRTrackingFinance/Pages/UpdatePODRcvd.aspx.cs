﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdatePODRcvd : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }

        private void InitializeContent(string LRNo)
        {
            hplPODFile.Visible = false;
            lblError.Visible = false;
            lblDate.Text = "-";
            lblConsigner.Text = "-";
            lblConsignee.Text = "-";
            lblFrom.Text = "-";
            lblTo.Text = "-";
            lblVehicleNo.Text = "-";
            lblPackages.Text = "-";
            lblWeight.Text = "-";
            lblChargeableWeight.Text = "-";
            lblInvoiceNo.Text = "-";
            lblInvoiceValue.Text = "-";
            lblType.Text = "-";
            lblRemarks.Text = "-";
            lblPODDate.Text = "-";
            lblDriverDetails.Text = "-";
            trphcopyyesno.Visible = false;
            trphcopy2.Visible = false;
            trphcopy1.Visible = false;

            LRBO pol = new LRBO();
            pol.LRNo = LRNo;
            if (LRNo != string.Empty)
            {
                DataTable dt = pol.GetLRHeaderByNo();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        lblDate.Text = DateTime.Parse(dt.Rows[0]["LRDate"].ToString()).ToString("dd/MMM/yyyy");
                        lblConsigner.Text = dt.Rows[0]["Consigner"].ToString();
                        lblConsignee.Text = dt.Rows[0]["Consignee"].ToString();
                        lblFrom.Text = dt.Rows[0]["FromLoc"].ToString();
                        lblTo.Text = dt.Rows[0]["ToLoc"].ToString();
                        lblVehicleNo.Text = dt.Rows[0]["VehicleNo"].ToString();
                        lblPackages.Text = dt.Rows[0]["TotPackages"].ToString();
                        lblWeight.Text = dt.Rows[0]["Weight"].ToString();
                        lblChargeableWeight.Text = dt.Rows[0]["ChargeableWt"].ToString();
                        lblInvoiceNo.Text = dt.Rows[0]["InvoiceNo"].ToString();
                        lblInvoiceValue.Text = dt.Rows[0]["InvoiceValue"].ToString();
                        lblType.Text = dt.Rows[0]["LoadType"].ToString();
                        lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
                        lblDriverDetails.Text = dt.Rows[0]["DriverDetails"].ToString();
                        hdfld.Value = dt.Rows[0]["ID"].ToString();
                        txtLocation.Text = "";

                        if (dt.Rows[0]["Status"].ToString() == "D")
                        {
                            if (dt.Rows[0]["PODDate"] != null && dt.Rows[0]["PODDate"].ToString().Trim() != "")
                            {
                                lblPODDate.Text = DateTime.Parse(dt.Rows[0]["PODDate"].ToString()).ToString("dd/MM/yyyy");
                                txtDate.Text = DateTime.Parse(dt.Rows[0]["PODDate"].ToString()).ToString("dd/MM/yyyy");
                            }

                            if (dt.Rows[0]["PODDate"].ToString().Trim() != "" && dt.Rows[0]["PODFile"].ToString().Trim() != "")
                            {
                                Session["FilePath"] = Server.MapPath(ConfigurationManager.AppSettings["FilesPath"].ToString()) + @"\" + dt.Rows[0]["PODFile"].ToString().Trim();
                                lblError.Visible = true;
                                lblError.Text = "POD Details are already updated";
                                btnSave.Visible = false;
                                hplPODFile.Visible = true;
                                trphcopyyesno.Visible = true;
                                trphcopy1.Visible = false;
                                trphcopy2.Visible = false;
                            }
                            else
                            {
                                trphcopyyesno.Visible = false;
                                trphcopy1.Visible = false;
                                trphcopy2.Visible = false;
                                hplPODFile.Visible = false;
                                btnSave.Visible = true;
                            }

                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = "LR is still in transit mode. Hence cannot update POD Details";
                            btnSave.Visible = false;
                            trphcopyyesno.Visible = false;
                            trphcopy1.Visible = false;
                            trphcopy2.Visible = false;

                        }
                        if (!string.IsNullOrEmpty(dt.Rows[0]["PODPhysicalDate"].ToString()))
                        {
                            trphcopy1.Visible = true;
                            trphcopy2.Visible = true;
                            trphcopyyesno.Visible = false;
                            txtphcopydate.Text = DateTime.Parse(dt.Rows[0]["PODPhysicalDate"].ToString()).ToString("dd/MMM/yyyy");
                            txtLocation.Text = dt.Rows[0]["PODPhysicalLocation"].ToString();
                            btnUpdatePhCopy.Visible = false;
                        }
                        else
                        {
                            trphcopyyesno.Visible = true;
                        }

                    }
                    else
                    {
                        lblError.Text = "LR No Not Found.";
                        btnSave.Visible = false;
                        lblError.Visible = true;
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            DataTable dt1 = pol.GetLRDtlsByNo();
            lstViewPayments.DataSource = dt1;
            lstViewPayments.DataBind();
        }

        private void FillDropDownReason()
        {
            PODReasonBO optbo = new PODReasonBO();
            DataTable dt;

            cmbReason.DataSource = null;
            cmbReason.Items.Clear();
            cmbReason.Items.Add(new ListItem("", "-1"));
            optbo.SearchText = "";
            dt = optbo.GetPODReasondetails();
            cmbReason.DataSource = dt;
            cmbReason.DataTextField = "Name";
            cmbReason.DataValueField = "Srl";
            cmbReason.DataBind();
            cmbReason.SelectedIndex = 0;
        }

        private void CheckChange()
        {
            tridemnity.Visible = false;
            rfvIdenmityAmt.Enabled = false;
            if (rdbYs.Checked)
            {
                lblPODDateLabel.Text = "POD Date";
                trReason.Visible = false;
                custValidReason.Enabled = false;
                dtpBtn.Focus();
            }
            else
            {
                lblPODDateLabel.Text = "Date";
                trReason.Visible = true;
                custValidReason.Enabled = true;
                FillDropDownReason();
                cmbReason.Focus();
            }
        }

        private void ReasonChange()
        {
            tridemnity.Visible = false;
            rfvIdenmityAmt.Enabled = false;

            if (cmbReason.SelectedValue == "1")
            {
                tridemnity.Visible = true;
                rfvIdenmityAmt.Enabled = true;
                txtIdenmityAmt.Focus();
            }
            else
                dtpBtn.Focus();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrptMgr = ScriptManager.GetCurrent(this.Page);
            scrptMgr.RegisterPostBackControl(btnSave);
            if (!IsPostBack)
            {
                lblError.Visible = false;
                rdbYs.Checked = true;
                CheckChange();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                trphcopyyesno.Visible = false;
                trphcopy1.Visible = false;
                trphcopy2.Visible = false;
                btnUpdatePhCopy.Visible = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string filepath = "";

            if (flUpload.FileName != string.Empty)
            {
                filepath = Server.MapPath(ConfigurationManager.AppSettings["FilesPath"].ToString()) + @"\" + txtLRNo.Text + "_" + flUpload.FileName;
                flUpload.SaveAs(filepath);
            }
            else
            {
                if (rdbYs.Checked)
                {
                    SetPanelMsg("Please Select File", true, 0);
                    flUpload.Focus();
                    return;
                }
            }

            LRBO optbo = new LRBO();
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            if (flUpload.FileName != "")
            {
                optbo.PODFile = txtLRNo.Text + "_" + flUpload.FileName;
            }
            else
            {
                optbo.PODFile = "";
            }

            optbo.PODDate = txtDate.Text;
            optbo.ID = long.Parse(hdfld.Value);
            optbo.PODReq = rdbYs.Checked ? 1 : 0;
            optbo.PODReasonId = rdbYs.Checked ? 0 : int.Parse(cmbReason.SelectedValue);
            if (txtIdenmityAmt.Text.Trim() == "") txtIdenmityAmt.Text = "0";
            optbo.IdemnityAmt = rdbYs.Checked ? 0 : decimal.Parse(txtIdenmityAmt.Text);
            long cnt = optbo.UpdatePODData();
            if (cnt > 0)
            {
                SetPanelMsg("Record updated successfully", true, 1);
                txtLRNo.Text = "";
                InitializeContent(string.Empty);
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            InitializeContent(txtLRNo.Text.Trim());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void hplPODFile_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewFile.aspx", false);
        }

        protected void rdbYs_CheckedChanged(object sender, EventArgs e)
        {
            CheckChange();
        }

        protected void rdbNo_CheckedChanged(object sender, EventArgs e)
        {
            CheckChange();
        }

        protected void cmbReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReasonChange();
        }

        protected void rdbPhCopyNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbPhCopyNo.Checked)
            {
                rfvphcopydate.Enabled = false;
                rfvphcopylocation.Enabled = false;
                trphcopy1.Visible = false;
                trphcopy2.Visible = false;
                btnUpdatePhCopy.Visible = false;
            }
        }

        protected void rdbPhCopyYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbPhCopyYes.Checked)
            {
                rfvphcopydate.Enabled = true;
                rfvphcopylocation.Enabled = true;
                trphcopy1.Visible = true;
                trphcopy2.Visible = true;
                btnUpdatePhCopy.Visible = true;
                txtphcopydate.Text = DateTime.Now.ToString("dd/MMM/yyyy");
                txtLocation.Text = string.Empty;
                txtLocation.Focus();
            }
        }

        protected void btnUpdatePhCopy_Click(object sender, EventArgs e)
        {
            LRBO optbo = new LRBO();
            optbo.PODDate = txtphcopydate.Text;
            optbo.ID = long.Parse(hdfld.Value);
            optbo.Location = txtLocation.Text.Trim();
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            long cnt = optbo.UpdatePODPhysicalCopyData();
            if (cnt > 0)
            {
                txtLRNo.Text = "";
                InitializeContent(string.Empty);
            }
        }
    }
}