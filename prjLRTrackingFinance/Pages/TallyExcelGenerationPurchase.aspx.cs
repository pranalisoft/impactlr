﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using CrystalDecisions;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class TallyExcelGenerationPurchase : System.Web.UI.Page
    {
        private void FillComboBillingCompany()
        {
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            BillingCompanyBO OptBO = new BillingCompanyBO();
            OptBO.SearchText = "";
            dt = OptBO.GetBillingCompanyDetails();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            if (!IsPostBack)
            {
                FillComboBillingCompany();
                txtFromDate.Focus();
            }
        }


        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\TallyPurchaseTemplate_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            CommonBO optbo = new CommonBO();
            optbo.fromdate = txtFromDate.Text;
            optbo.todate = txtToDate.Text;
            optbo.BillingCompanySrl = long.Parse(cmbCustomer.SelectedValue);
            DataTable dt = optbo.GenerateTallyPurchaseExcel();
            ExportToExcel(dt, filename);

        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["A1:BZ1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }
    }
}