﻿<%@ Page Title="Item Return" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="ItemReturn.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.ItemReturn" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Return Entry
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="view1" runat="server">
                            <div class="buttons_top">
                                <asp:Button ID="btnAdd" Text="Add" runat="server" CssClass="button" CommandName="Add"
                                    OnCommand="EntryForm_Command" TabIndex="1" />
                                <%-- <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="button" CommandName="Delete"
                                    OnCommand="EntryForm_Command" TabIndex="2" />--%>
                            </div>
                            <div class="grid_top_region">
                                <div class="grid_top_region_lft">
                                    <%-- <asp:CheckBox ID="chkDateFilter" runat="server" AutoPostBack="true" Text="Date Filter"
                                        TabIndex="3" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtFromDate" Width="100px" runat="server" TabIndex="3" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_From" TargetControlID="txtFromDate" PopupButtonID="imgBtnCalcPopupFrom"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupFrom" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="4" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtToDate" Width="100px" runat="server" TabIndex="5" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_To" TargetControlID="txtToDate" PopupButtonID="imgBtnCalcPopupTo"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupTo" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="6" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnFilter" Text="Show" runat="server" CssClass="button" CommandName="Filter"
                                        OnCommand="Filter_Command" TabIndex="7" ValidationGroup="save" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnClearFilter" Text="Clear Filter" runat="server" CssClass="button"
                                        CommandName="ClearFilter" OnCommand="Filter_Command" TabIndex="8" />--%>
                                </div>
                                <div class="grid_top_region_rght">
                                    Records :
                                    <asp:Label ID="lblRecCount" Text="0" runat="server" />
                                    <%--<table border="0" class="cnt_region">
                                    <tr>
                                        <td>
                                            Records :
                                            <asp:Label ID="lblRecCount" Text="1000" runat="server" />
                                        </td>
                                    </tr>
                                </table>--%>
                                </div>
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdViewIndex" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    AllowPaging="True" DataKeyNames="Srl,SupplierId,SupplierName,ItemSrl,TranDate,ItemName,Qty,LostQty,Remarks,UserName"
                                    OnRowCommand="grdViewIndex_RowCommand" EmptyDataText="No records found." Width="100%"
                                    OnPageIndexChanging="grdViewIndex_PageIndexChanging" PageSize="15">
                                    <Columns>
                                        <%-- <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                    AutoPostBack="true" TabIndex="5" /></HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" TabIndex="6" /></ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="28px" />
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>--%>
                                        <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Srl" HeaderText="No."
                                            SortExpression="Srl" ItemStyle-Width="50px" />
                                        <asp:BoundField DataField="TranDate" HeaderText="Date" SortExpression="TranDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="ItemName" HeaderText="Item" SortExpression="ItemName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="Qty" HeaderText="Qty" SortExpression="Qty" ReadOnly="True"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="LostQty" HeaderText="Damaged Qty" SortExpression="LostQty" ReadOnly="True"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="UserName" HeaderText="Entered By" SortExpression="UserName"
                                            ReadOnly="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                </asp:GridView>
                            </div>
                        </asp:View>
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%;vertical-align:top">
                                            <table class="control_set" style="width: 100%">
                                                <%--<tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td colspan="6">
                                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="1" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="2" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the Payment date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>--%>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label2" Text="Supplier" runat="server" AssociatedControlID="cmbSupplier" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbSupplier" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupplier_SelectedIndexChanged" TabIndex="3" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="cvSupplier" runat="server" ControlToValidate="cmbItem" ErrorMessage="Please select Supplier from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="Validatorcalloutextender3" runat="server" Enabled="True"
                                                            TargetControlID="cvSupplier">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label4" Text="Item" runat="server" AssociatedControlID="cmbItem" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbItem" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbItem_SelectedIndexChanged" TabIndex="4" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Item" runat="server" ControlToValidate="cmbItem" ErrorMessage="Please select Item from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="Validatorcalloutextender1" runat="server" Enabled="True"
                                                            TargetControlID="Item">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" AssociatedControlID="txtQtyStock" Text="Balance Qty" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtQtyStock" runat="server" AutoPostBack="false" MaxLength="100"
                                                            Enabled="false" Font-Bold="true" TabIndex="7" Width="100px" Style="text-align: right"
                                                            CssClass="NormalTextBold" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 30px">
                                                        <asp:Label runat="server" AssociatedControlID="txtQty" Text="Qty" />
                                                    </td>
                                                    <td style="width: 320px">
                                                        <asp:TextBox ID="txtQty" runat="server" MaxLength="5" TabIndex="5" Width="100px"
                                                            Style="text-align: right" CssClass="NormalTextBold" />                                                        
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            FilterType="Numbers" TargetControlID="txtQty">
                                                        </asp:FilteredTextBoxExtender>                                                       
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 30px">
                                                        <asp:Label ID="Label5" runat="server" AssociatedControlID="txtLostQty" Text="Damage Qty" />
                                                    </td>
                                                    <td style="width: 320px">
                                                        <asp:TextBox ID="txtLostQty" runat="server" MaxLength="5" TabIndex="6" Width="100px"
                                                            Style="text-align: right" CssClass="NormalTextBold" />                                                       
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                                            FilterType="Numbers" TargetControlID="txtQty">
                                                        </asp:FilteredTextBoxExtender>                                                       
                                                    </td>
                                                </tr>                                                
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label1" Text="Damaged Qty Remarks" runat="server" AssociatedControlID="txtRemarks" />
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txtRemarks" Width="300px" runat="server" TabIndex="8" CssClass="NormalTextBold"
                                                            TextMode="MultiLine" Rows="3" />
                                                    </td>
                                                </tr>
 						<tr>
						<td colspan="5">
						</td>
						</tr>
						<tr>
						<td colspan="2">
						</td>
						<td>
						  <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="9" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="10" />
                            </div>
						</td>
						</tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%;border-bottom:1px solid #3A66AF" valign="top">
					<div style="height:500px;overflow:auto;">
                                            <table width="100%">
                                                <asp:GridView ID="grdItemStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                    DataKeyNames="Srl,Name,TranDate,Remarks,IssueQty,RcptQty,LostQty" EmptyDataText="No records found."
                                                    Width="100%" TabIndex="100">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="Item" />
                                                        <asp:BoundField DataField="TranDate" HeaderText="Date" SortExpression="TranDate"
                                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:BoundField DataField="IssueQty" HeaderText="Issue" SortExpression="IssueQty"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                        <asp:BoundField DataField="RcptQty" HeaderText="Rcpt" SortExpression="RcptQty" ItemStyle-HorizontalAlign="Right"
                                                            ItemStyle-Width="100px" /> 
                                                             <asp:BoundField DataField="LostQty" HeaderText="Damaged" SortExpression="LostQty" ItemStyle-HorizontalAlign="Right"
                                                            ItemStyle-Width="100px" />                                                       
                                                    </Columns>
                                                    <HeaderStyle CssClass="header" />
                                                    <RowStyle CssClass="row" />
                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                </asp:GridView>
                                            </table>
					</div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <br />
                            <table class="control_set" style="width: 100%">
                            </table>
                          
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
