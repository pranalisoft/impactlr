﻿<%@ Page Title="Billing Company Master" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="BillingCompany.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.BillingCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
<%--start attach file popup--%>
    <asp:Button ID="btnAttachFile" runat="server" Text="Button" Style="display: none" />
    <asp:Panel ID="pnlAttachFile" runat="server" CssClass="dialog" Style="display: none">
        <div style="background-color: #236099; color: White; padding: 3px; font-size: 14px;
            font-weight: bold">
            Company - Upload Logo
        </div>
        <div>
            <asp:Panel ID="pnlInwardFiles" runat="server">
                <table width="100%" cellspacing="10px" style="margin-top: 10px;">
                    <tr>
                        <td width="80px">
                            <asp:Label ID="Label19" runat="server" Text="Company Logo" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload2" runat="server" Width="280px" Font-Size="13px" TabIndex="0" />&nbsp
                            &nbsp
                            <asp:RequiredFieldValidator ID="ReqfldflCopy" runat="server" ErrorMessage="Please select logo To Upload"
                                ControlToValidate="FileUpload2" SetFocusOnError="True" ValidationGroup="saveFile"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnSaveFiles" runat="server" Text="Ok" CssClass="button" OnClick="btnSaveFiles_Click"
                                TabIndex="2" ValidationGroup="saveFile" />
                            &nbsp;&nbsp;<asp:Button ID="btnFileCancel" runat="server" Text="Close" CssClass="button"
                                OnClick="btnFileCancel_Click" TabIndex="3" />&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="popAttachFIles" runat="server" DynamicServicePath=""
        Enabled="True" TargetControlID="btnAttachFile" PopupControlID="pnlAttachFile"
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <%-- end attach file popup--%>
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>

            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Billing Company Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
             <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="Billing Company Info" ForeColor="Black"
                                                    Font-Bold="true" Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Name" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" Width="400px" TabIndex="1" MaxLength="200"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfv" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Name'
                                                    ControlToValidate="txtName" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label17" runat="server" Text="Short Name" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtShortName" runat="server" Width="100px" TabIndex="2" MaxLength="5"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Name'
                                                    ControlToValidate="txtShortName" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label18" runat="server" Text="Bill Type" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBillType" runat="server" Width="50px" TabIndex="3" MaxLength="2"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Name'
                                                    ControlToValidate="txtBillType" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Address Line1" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress1" runat="server" Width="400px" TabIndex="4" CssClass="NormalTextBold"
                                                    MaxLength="100"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Address Line 1'
                                                    ControlToValidate="txtAddress1" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Address Line2" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress2" runat="server" Width="400px" TabIndex="5" CssClass="NormalTextBold"
                                                    MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Address Line3" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress3" runat="server" Width="400px" TabIndex="6" CssClass="NormalTextBold"
                                                    MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Landline No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLandline" runat="server" Width="300px" TabIndex="7" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="Mobile No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server" Width="300px" TabIndex="8" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="Email ID" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmailID" runat="server" Width="300px" TabIndex="9" CssClass="NormalTextBold"
                                                    MaxLength="100"></asp:TextBox>
                                                <asp:RegularExpressionValidator runat="server" ID="regexpEmail" SetFocusOnError="true"
                                                    ValidationGroup="save" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter valid email id'
                                                    ControlToValidate="txtEmailId" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Text="Vendor Code" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVendor" runat="server" Width="200px" TabIndex="10" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="GST No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCSTNo" runat="server" Width="200px" TabIndex="11" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label9" runat="server" Text="VAT No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVAT" runat="server" Width="200px" TabIndex="12" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label10" runat="server" Text="TIN No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTIN" runat="server" Width="200px" TabIndex="13" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label12" runat="server" Text="Service Tax No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtServiceTaxNo" runat="server" Width="200px" TabIndex="14" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label14" runat="server" Text="Credit Days" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCreditDays" runat="server" Width="100px" TabIndex="15" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label15" runat="server" Text="Credit Limit" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCreditLimit" runat="server" Width="100px" TabIndex="16" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label16" runat="server" Text="Active" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkActive" runat="server" TabIndex="17" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" Text="Logo"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkBtnAttachFiles" runat="server" Font-Bold="true" TabIndex="18"
                                                    Font-Size="13px" OnClick="lnkBtnAttachFiles_Click">Update Company Logo</asp:LinkButton>
                                                <asp:FileUpload ID="flCopy" runat="server" Width="300px" Font-Size="13px" TabIndex="10" Visible="false"/>
                                                <asp:Button ID="btnconfirmFile" runat="server" Text="Show Image" OnClick="btnconfirmFile_Click" Visible="false"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label Text="Preview" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:Panel runat="server" CssClass="pnlimage" Width="100px">
                                                <asp:Image runat="server" ID="imgLogo" Width="100px" Height="100px" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="18" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" OnClick="btnSave_Click" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="19" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" MaxLength="10" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAdd" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" />&nbsp; &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotal" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="Srl, Name, Address1, Address2, Address3, Landline, Mobile, EmailId, VendorCode, CSTNo, VATNo, TINNo, ServiceTaxNo, CreditDays, CreditLimit, IsActive, UsedCnt,ShortName,BillingType,Logo"
                                                OnPageIndexChanging="grdView_PageIndexChanging" OnRowDataBound="grdView_RowDataBound"
                                                OnRowCommand="grdView_RowCommand" AllowSorting="True" OnSorting="grdView_Sorting">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Name"
                                                        SortExpression="LRNo">
                                                        <ItemStyle Width="200px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="ShortName" HeaderText="Short Name" SortExpression="ShortName"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="BillingType" HeaderText="Type" SortExpression="BillingType"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Address1" HeaderText="Address" SortExpression="Address1"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Landline" HeaderText="Landline" SortExpression="Landline"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnconfirmFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
