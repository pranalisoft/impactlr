﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerReceipt : System.Web.UI.Page
    {
        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private long Delete()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdViewIndex.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdViewIndex.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteReceipt();
            }
            return srl;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            txtDate_CalendarExtender.EndDate = DateTime.Now;
            //if (!IsPostBack)
            //{
            //    fillBank();
            //    FillComboCustomer();
            //    cmbCust.Text = String.Empty;
            //    cmbPaymentMode.SelectedIndex = 1;
            //    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            //    cmbPaymentMode.SelectedIndex = 0;
            //    //lblOutstanding.Visible = false;
            //    txtDate.Focus();

            //}
            if (!IsPostBack)
            {
                Session.Remove("dtRcptInvDtls");
                ShowViewByIndex(0);
                chkDateFilter.Checked = false;
                txtFromDate.Text = "";
                txtToDate.Text = "";
                LoadGridView();
                PnlItemDtls.Visible = false;
                txtFromDate.Focus();

            }
            RegisterDateTextBox();
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdViewIndex, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdViewIndex, "chkSelect");
            }
        }

        private void LoadGridView()
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.DatefilterYes = chkDateFilter.Checked;
            OptBO.SearchFrom = txtFromDate.Text;
            OptBO.SearchTo = txtToDate.Text;
            OptBO.PayType = "N";
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            DataTable dt = OptBO.FillReceiptHeader();
            grdViewIndex.DataSource = dt;
            grdViewIndex.DataBind();
            lblRecCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
                btnDelete.Visible = true;
            else
                btnDelete.Visible = false;
        }

        private void LoadGridViewItem(long RcptId, string RcptNo)
        {
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.Srl = RcptId;
            DataTable dt = OptBO.FillReceiptInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
            lblItemCount.Text = dt.Rows.Count.ToString();
            if (dt.Rows.Count > 0)
            {
                lblItemDtls.Text = "Invoice Details (" + RcptNo + ")";
            }
            else
            {
                lblItemDtls.Text = "On Account Transaction (" + RcptNo + ")";
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal number = 0;
                decimal totAmt = 0;
                decimal adjamtLR = 0;
                decimal adjamtTDS = 0;
                // if (!grdPaymentSupl.Visible) return;
                if (!Decimal.TryParse(txtAmount.Text, out number))
                    return;
                if (txtAmount.Text.Trim() != String.Empty)
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }

                    decimal BalAmt = decimal.Parse(txtAmount.Text);
                    if (txtTDSPer.Text.Trim() == string.Empty) txtTDSPer.Text = "0";

                    decimal TDSPer = decimal.Parse(txtTDSPer.Text);

                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        if (BalAmt > 0)
                        {
                            TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                            TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");
                            TextBox txtTDSAmt = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                            decimal adjamt = 0;
                            if (decimal.Parse(txtbal.Text) >= BalAmt)
                            {
                                adjamt = BalAmt;
                            }
                            else
                            {
                                adjamt = decimal.Parse(txtbal.Text);
                            }
                            txtTDSAmt.Text = ((adjamt * TDSPer) / 100).ToString("F0");
                            if (txtTDSAmt.Text.Trim() == string.Empty) txtTDSAmt.Text = "0";
                            txtadj.Text = (adjamt - (decimal.Parse(txtTDSAmt.Text))).ToString();
                            adjamtLR=adjamtLR + decimal.Parse(txtadj.Text);
                            adjamtTDS = adjamtTDS + decimal.Parse(txtTDSAmt.Text);
                            BalAmt = BalAmt - adjamt;
                            totAmt = totAmt + decimal.Parse(txtadj.Text) + decimal.Parse(txtTDSAmt.Text);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        txtadj.Text = String.Empty;
                    }

                }
                txtAmount.Text = totAmt.ToString("F2");
                txtTotalLR.Text = adjamtLR.ToString("F2");
                txtTotalTDS.Text = adjamtTDS.ToString("F2");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void calculateAmount()
        {
            decimal adjamt = 0;
            decimal adjamtLR = 0;
            decimal adjamtTDS = 0;

            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");

                if (txtadj.Text.Trim() != string.Empty)
                {
                    adjamt = adjamt + decimal.Parse(txtadj.Text);
                    adjamtLR = adjamtLR + decimal.Parse(txtadj.Text);
                }

                if (txtbal.Text.Trim() != string.Empty)
                {
                    adjamt = adjamt + decimal.Parse(txtbal.Text);
                    adjamtTDS = adjamtTDS + decimal.Parse(txtbal.Text);
                }
            }
            txtAmount.Text = adjamt.ToString();
            txtTotalLR.Text = adjamtLR.ToString();
            txtTotalTDS.Text = adjamtTDS.ToString();
        }

        protected void txtAmountGrid_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtAmountGrid = (TextBox)sender;
                GridViewRow row = txtAmountGrid.NamingContainer as GridViewRow;
                int rowIndex = row.RowIndex;

                if (txtAmountGrid.ID == "txtAdjAmt")
                {
                    TextBox txtadj = (TextBox)grdPaymentSupl.Rows[rowIndex].FindControl("txtAdjAmt");
                    TextBox txtbal = (TextBox)grdPaymentSupl.Rows[rowIndex].FindControl("txtTDSAmt");


                    //if (txtadj.Text.Trim() != string.Empty)
                    //{
                    //    txtbal.Text = (decimal.Parse(txtadj.Text) * decimal.Parse(txtTDSPer.Text) / 100).ToString("F0");
                    //    txtadj.Text = (decimal.Parse(txtadj.Text) - decimal.Parse(txtbal.Text)).ToString("F0");
                    //}
                }
                else if (txtAmountGrid.ID == "txtTDSAmt")
                {
                    TextBox txtadj = (TextBox)grdPaymentSupl.Rows[rowIndex].FindControl("txtAdjAmt");
                    TextBox txtbal = (TextBox)grdPaymentSupl.Rows[rowIndex].FindControl("txtTDSAmt");


                    if (txtbal.Text.Trim() == string.Empty)
                    {
                        txtbal.Text = "0";
                    }
                }

                calculateAmount();
                txtAmountGrid.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void FillComboCustomer()
        {
            cmbCust.Items.Clear();
            cmbCust.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            CustomerReceiptBO OptBO = new CustomerReceiptBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillCustomerCombo();
            cmbCust.DataSource = dt;
            cmbCust.DataTextField = "Name";
            cmbCust.DataValueField = "Srl";
            cmbCust.DataBind();
            cmbCust.SelectedIndex = 0;
        }

        private void fillBank()
        {
            cmbBank.Items.Clear();
            CommonBO bankbo = new CommonBO();
            DataTable dt = bankbo.FillBankDetails();
            cmbBank.DataSource = dt;
            cmbBank.DataTextField = "BankName";
            cmbBank.DataValueField = "BankName";
            cmbBank.DataBind();
        }

        protected void cmbCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CustomerReceiptBO custbo = new CustomerReceiptBO();
                custbo.CustomerID = long.Parse(cmbCust.SelectedValue);
                custbo.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
                DataTable dt = custbo.ShowReceiptGrid();
                grdViewInvSelection.DataSource = dt;
                grdViewInvSelection.DataBind();
                grdPaymentSupl.Visible = false;
                btnSave.Visible = false;
                btnConfirm.Visible = true;
                //dt = null;

                DataTable dt1 = custbo.ShowReceiptStatus();
                GrdPaymentStatus.DataSource = dt1;
                GrdPaymentStatus.DataBind();
                cmbPaymentMode.Focus();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void grdPaymentSupl_Validated(object source, ServerValidateEventArgs args)
        {

        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    if (Delete() > 0)
                    {
                        LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    }
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Receipt from Customer";
                    ShowViewByIndex(1);
                    fillBank();
                    FillComboCustomer();
                    clearAll();
                    //cmbCust.Text = String.Empty;
                    cmbPaymentMode.SelectedIndex = 1;
                    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                    cmbPaymentMode.SelectedIndex = 0;
                    btnSave.Visible = false;
                    btnConfirm.Visible = true;
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    int cntDel = 0;
                    foreach (GridViewRow row in grdViewIndex.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cntDel = cntDel + 1;
                                break;
                            }
                        }
                    }
                    if (cntDel > 0)
                    {
                        MsgPopUp.ShowModal("Are you sure.<br/>Do you want to cancel selected rcpts?", CommonTypes.ModalTypes.Confirm);
                    }
                    else
                    {
                        MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                    }
                    PnlItemDtls.Visible = false;
                    break;
                case CommonTypes.EntryFormCommand.Submit:
                    //DataTable dt = new DataTable("dtPayment");
                    //dt.Columns.Add("Srl", typeof(long));
                    //dt.Columns.Add("InvoiceNo", typeof(string));
                    //dt.Columns.Add("InvoiceDate", typeof(DateTime));
                    //dt.Columns.Add("InvoiceAmount", typeof(decimal));
                    //dt.Columns.Add("TDSAmount", typeof(decimal));
                    //dt.Columns.Add("ReceivedAmount", typeof(decimal));
                    //dt.Columns.Add("BalAmt", typeof(decimal));

                    //decimal totalamount = 0;
                    //for (int i = 0; i < grdViewInvSelection.Rows.Count; i++)
                    //{
                    //    CheckBox chk =(CheckBox) grdViewInvSelection.Rows[i].FindControl("chkLR");
                    //    if (chk.Checked)
                    //    {
                    //        DataKey key = grdViewInvSelection.DataKeys[i];
                    //        //Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt
                    //        dt.Rows.Add();
                    //        int rIndex=dt.Rows.Count-1;
                    //        dt.Rows[rIndex]["Srl"] = key["Srl"];
                    //        dt.Rows[rIndex]["InvoiceNo"] = key["InvoiceNo"];
                    //        dt.Rows[rIndex]["InvoiceDate"] = key["InvoiceDate"];
                    //        dt.Rows[rIndex]["InvoiceAmount"] = key["InvoiceAmount"];
                    //        dt.Rows[rIndex]["TDSAmount"] = 0;
                    //        if (txtTDSPer.Text.Trim() != string.Empty)
                    //        {
                    //            if (decimal.Parse(txtTDSPer.Text) > 0)
                    //            {
                    //                dt.Rows[rIndex]["TDSAmount"] = ((decimal.Parse(key["InvoiceAmount"].ToString()) * decimal.Parse(txtTDSPer.Text)) / 100).ToString("F0");
                    //            }
                    //        }
                    //        dt.Rows[rIndex]["BalAmt"] = key["BalAmt"];// (decimal.Parse(key["BalAmt"].ToString()) - decimal.Parse(dt.Rows[rIndex]["TDSAmount"].ToString()));
                    //        dt.Rows[rIndex]["ReceivedAmount"] = key["ReceivedAmount"];
                    //        totalamount = totalamount+(decimal.Parse(key["BalAmt"].ToString()) - decimal.Parse(dt.Rows[rIndex]["TDSAmount"].ToString()));
                    //        //dt.Rows[rIndex]["BalAmt"] = key["BalAmt"];
                    //        dt.AcceptChanges();
                    //    }
                    //}

                    //txtAmount.Text = totalamount.ToString();

                    //grdPaymentSupl.DataSource = dt;
                    //grdPaymentSupl.DataBind();

                    string InvoiceNos = "";
                    for (int i = 0; i < grdViewInvSelection.Rows.Count; i++)
                    {
                        CheckBox chk = (CheckBox)grdViewInvSelection.Rows[i].FindControl("chkLR");
                        if (chk.Checked)
                        {
                            DataKey key = grdViewInvSelection.DataKeys[i];
                            if (InvoiceNos == string.Empty)
                                InvoiceNos = "'" + key["InvoiceNo"].ToString() + "'";
                            else
                                InvoiceNos = InvoiceNos + ",'" + key["InvoiceNo"].ToString() + "'";

                            //if (txtTDSPer.Text.Trim() != string.Empty)
                            //{
                            //    if (decimal.Parse(txtTDSPer.Text) > 0)
                            //    {
                            //        TDSAmount = System.Math.Round(((decimal.Parse(key["InvoiceAmount"].ToString()) * decimal.Parse(txtTDSPer.Text)) / 100),2);
                            //    }
                            //}

                            //totalamount = totalamount + (decimal.Parse(key["BalAmt"].ToString()) - TDSAmount);
                        }
                    }

                    CustomerReceiptBO custbo1 = new CustomerReceiptBO();
                    custbo1.CustomerID = long.Parse(cmbCust.SelectedValue);
                    custbo1.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
                    custbo1.InvoiceNos = InvoiceNos;
                    DataTable dt = custbo1.ShowReceiptGridLR();
                    grdPaymentSupl.DataSource = dt;
                    grdPaymentSupl.DataBind();

                    //txtAmount.Text = totalamount.ToString();
                    //txtAmount_TextChanged(txtAmount, EventArgs.Empty);
                    txtAmount.Enabled = true;
                    btnConfirm.Visible = false;
                    btnSave.Visible = true;
                    grdPaymentSupl.Visible = true;
                    grdViewInvSelection.Visible = false;
                    txtAmount.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    decimal totamt = 0;
                    decimal balAmt = 0;
                    string paymentxml = "";
                    if (!chkOnAccount.Checked)
                    {
                        paymentxml = GenerateXML();
                    }
                    for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
                    {
                        TextBox txtadj = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                        TextBox txttds = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                        TextBox txtbal = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtBalAmt");

                        if (txtadj.Text.Trim() != String.Empty)
                        {
                            totamt += decimal.Parse(txtadj.Text);
                            if (txttds.Text.Trim() != String.Empty) totamt += decimal.Parse(txttds.Text);
                            balAmt += decimal.Parse(txtbal.Text);
                        }
                    }

                    if (chkOnAccount.Checked == false)
                    {
                        if (balAmt < decimal.Parse(txtAmount.Text))
                        {
                            MsgPopUp.ShowModal("Entered amount cannot be greater than balance amount", CommonTypes.ModalTypes.Error);
                            return;
                        }

                        if (totamt != decimal.Parse(txtAmount.Text))
                        {
                            MsgPopUp.ShowModal("Entered amount and total of adjusted amount should be equal", CommonTypes.ModalTypes.Error);
                            return;
                        }
                    }


                    CustomerReceiptBO custbo = new CustomerReceiptBO()
                    {
                        tranDate = txtDate.Text.Trim(),
                        CustomerID = long.Parse(cmbCust.SelectedValue),
                        refNo = String.Empty,
                        remarks = String.Empty,
                        totalAmount = decimal.Parse(txtAmount.Text),
                        userID = long.Parse(Session["EID"].ToString()),
                        paymentXML = paymentxml,
                        BankName = cmbBank.Text.Trim(),
                        PaymentMode = cmbPaymentMode.SelectedValue,
                        ChqNo = TxtChqNo.Text.Trim(),
                        ChqDate = txtchqdt.Text.Trim(),
                        CardNetBankingDetails = TxtChqNo.Text.Trim(),
                        PayType = "N",
                        BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString())
                    };
                    long cnt = custbo.InsertPaymentDetails();
                    if (cnt > 0)
                    {
                        MsgPanel.Message = "Record Saved successfully.";
                        MsgPanel.DispCode = 1;
                        clearAll();
                        LoadGridView();
                        ShowViewByIndex(0);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    clearAll();
                    //cmbCust.Text = String.Empty;
                    cmbCust_SelectedIndexChanged(cmbCust, EventArgs.Empty);
                    cmbPaymentMode.SelectedIndex = 1;
                    cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                    cmbPaymentMode.SelectedIndex = 0;
                    btnSave.Visible = false;
                    btnConfirm.Visible = true;
                    txtDate.Focus();
                    grdPaymentSupl.Visible = false;
                    grdViewInvSelection.Visible = true;
                    LoadGridView();
                    ShowViewByIndex(0);
                    break;
                case CommonTypes.EntryFormCommand.None:

                    break;
                default:

                    break;
            }

        }

        protected void Filter_Command(object sender, CommandEventArgs e)
        {
            try
            {
                MsgPanel.Message = string.Empty;
                MsgPanel.DispCode = -1;
                DataTable dt = null;
                switch (e.CommandName.ToLower())
                {
                    case "filter":
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    case "clearfilter":
                        txtFromDate.Text = string.Empty;
                        txtToDate.Text = string.Empty;
                        chkDateFilter.Checked = false;
                        LoadGridView();
                        PnlItemDtls.Visible = false;
                        break;
                    default:
                        break;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltViewMaster.ActiveViewIndex = index;
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtAmount.Text = txtchqdt.Text = txtDate.Text = TxtChqNo.Text = txtTotalLR.Text = txtTotalTDS.Text = String.Empty;
            GrdPaymentStatus.DataSource = null;
            GrdPaymentStatus.DataBind();
            grdPaymentSupl.DataSource = null;
            grdPaymentSupl.DataBind();
            if (cmbCust.Items.Count > 0) cmbCust.SelectedIndex = 0;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
        }

        private string GenerateXML()
        {
            string strxml = String.Empty;
            string InvoiceNo = String.Empty;
            string InvoiceId = string.Empty;
            string LRId = string.Empty;

            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                TextBox txt = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjAmt");
                TextBox txtTDS = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtTDSAmt");
                if (txt.Text.Trim() == string.Empty) txt.Text = "0";
                if (txtTDS.Text.Trim() == string.Empty) txtTDS.Text = "0";

                DataKey key = grdPaymentSupl.DataKeys[i];
                if (decimal.Parse(txt.Text.Trim()) > 0)
                {
                    InvoiceNo = key["InvoiceNo"].ToString();
                    InvoiceId = key["HeaderSrl"].ToString();
                    LRId = key["LRId"].ToString();
                    strxml = strxml + "<dtPayment><InvoiceNo>" + InvoiceNo + "</InvoiceNo><InvoiceId>" + InvoiceId + "</InvoiceId><LRId>" + LRId + "</LRId><AdjAmt>" + txt.Text.Trim() + "</AdjAmt><TDSAmt>" + txtTDS.Text.Trim() + "</TDSAmt></dtPayment>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<DocumentElement>" + strxml + "</DocumentElement>";
            }
            return strxml;
        }

        protected void cmbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
            {
                trBank.Visible = true;
                trChqNo.Visible = true;
                lblChqDDNo.Visible = true;
                TxtChqNo.Visible = true;
                cmbBank.Visible = true;
                lblBank.Visible = true;
                CustomBank.Enabled = true;
                reqFVChqNo.Enabled = true;
            }
            else
            {
                trBank.Visible = false;
                trChqNo.Visible = false;
                lblChqDDNo.Visible = false;
                TxtChqNo.Visible = false;
                cmbBank.Visible = false;
                lblBank.Visible = false;
                CustomBank.Enabled = false;
                reqFVChqNo.Enabled = false;
            }
            switch (cmbPaymentMode.SelectedValue)
            {
                case "1":
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "2":
                    lblChqDDNo.Text = "Chq No.";
                    lblchqddDate.Text = "Chq.Date";
                    break;
                case "3":
                    lblChqDDNo.Text = "DD No.";
                    lblchqddDate.Text = "DD Date";
                    break;
                case "4":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "5":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "6":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                default:
                    break;
            }
            txtchqdt.Focus();
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdViewIndex_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdViewIndex_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViewIndex.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void grdViewIndex_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "PayDetails")
                {
                    PnlItemDtls.Visible = true;
                    LoadGridViewItem(long.Parse(keys["Srl"].ToString()), keys["RefNo"].ToString());

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void chkOnAccount_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOnAccount.Checked)
            {
                grdPaymentSupl.Visible = false;
            }
            else
            {
                cmbCust_SelectedIndexChanged(cmbCust, e);
                grdPaymentSupl.Visible = true;
            }
        }
    }
}