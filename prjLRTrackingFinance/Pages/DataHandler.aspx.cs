﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using BusinessObjects.BO;
using System.Data;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DataHandler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string GetLRDetails()
        {
            LRBO optbo = new LRBO();
            optbo.LRNo = "40";
            DataTable dt = optbo.GetLRHeaderByNo();
            return JsonConvert.SerializeObject(dt, Formatting.Indented);

           
        }
    }
}
