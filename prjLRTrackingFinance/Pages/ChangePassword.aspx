﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.ChangePassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
          <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                 Change Password
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                 <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                    <div style="padding-left: 15px;">                        
                        <div>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="User Id" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtUserId" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" ReadOnly="True"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="reqfldBUName" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtUserId" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regExpValtxtBUName" runat="server" ControlToValidate="txtUserId"
                                            Display="Dynamic" ErrorMessage="Special Charactres Not Allowed" SetFocusOnError="True"
                                            ValidationExpression="^[a-zA-Z0-9-_()\s]*$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="User Name" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtUserName" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" ReadOnly="True"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtUserName" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUserName"
                                            Display="Dynamic" ErrorMessage="Special Charactres Not Allowed" SetFocusOnError="True"
                                            ValidationExpression="^[a-zA-Z0-9-_()\s]*$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Current Password" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtoldpwd" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" TextMode="Password"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvoldpwd" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtoldpwd" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="New Password" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtPassword" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" TextMode="Password"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtPassword" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Confirm New Password" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtConfirmpwd" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" TextMode="Password"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtConfirmpwd" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmppwd" runat="server" ControlToValidate ="txtConfirmpwd" ControlToCompare = "txtPassword" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Password should match with the confirmed password'></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="3" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="4" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                           <%-- <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="100px">
                                        <asp:Button TabIndex="5" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                            OnClick="btnDelete_Click" />
                                    </td>
                                    <td style="font-size: 12px; font-weight: bold; height: 15px">
                                        Records :
                                        <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                                    </td>
                                </tr>
                            </table>--%>
                       
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
