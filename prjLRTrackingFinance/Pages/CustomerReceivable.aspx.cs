﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerReceivable : System.Web.UI.Page
    {
        private void setGrid()
        {
            DataTable dt = CommonTypes.CreateDataTable("CustomerId,CustomerName,UnBilledAmount,NotDueAmount,CreditDays,Slot_30Days,Slot_45Days,Slot_60Days,Slot_75Days,Slot_76Days,TotalAbove30,Total,AmountOverDue,AmountExpected,AmountExpectedOn,PaymentRcvd,PODRcvdBillPending,PODNotRcvdButDeliveredAmount,ExpDate,ExpAmount", "dtSuplPayableApproval", "long,string,decimal,decimal,int,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,dateTime,decimal,decimal,decimal,decimal,dateTime,decimal");
            grdMain.DataSource = dt;
            grdMain.DataBind();
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            //cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;
        }

        private void LoadGridView()
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            CommonBO OptBO = new CommonBO();
            OptBO.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            OptBO.fromdate = txtDate.Text;
            DataTable dt = OptBO.GetCustomerReceivable();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();

            decimal total = 0; ;
            grdMain.FooterRow.Cells[0].Text = "Total";
            grdMain.FooterRow.Cells[0].Font.Bold = false;
            grdMain.FooterRow.Cells[0].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[0].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Left;

            int ColIndex = 1;
            for (int k = 2; k < dt.Columns.Count; k++)
            {
                if (k == 4 || k == 14 || k == 15 || k == 17)    
                {
                    ColIndex = ColIndex + 1;
                }
                else
                {
                    total = dt.AsEnumerable().Sum(row => row.Field<Decimal>(dt.Columns[k].ToString()));
                    
                    grdMain.FooterRow.Cells[ColIndex].Text = total.ToString().Replace(".00", "");
                    grdMain.FooterRow.Cells[ColIndex].Font.Bold = false;
                    grdMain.FooterRow.Cells[ColIndex].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                    grdMain.FooterRow.Cells[ColIndex].ForeColor = System.Drawing.Color.White;
                    grdMain.FooterRow.Cells[ColIndex].HorizontalAlign = HorizontalAlign.Right;
                    ColIndex = ColIndex + 1;
                }
            }            
        }

        private DataTable ReturnData(long BillingCompanySrl,string BillingCompanyName)
        {
            CommonBO OptBO = new CommonBO();
            OptBO.BillingCompanySrl = BillingCompanySrl;
            OptBO.fromdate = txtDate.Text;
            DataTable dt = OptBO.GetCustomerReceivable();            

            dt.Columns.RemoveAt(0);
            dt.AcceptChanges();
            if (dt.Rows.Count>0)
            {
                dt.Rows.Add();
                int RInd = dt.Rows.Count - 1;
                dt.Rows[RInd][0] = "Total " + BillingCompanyName;
                //dt.Rows[RInd][1] = "";
                dt.Rows[RInd][1] = dt.Compute("SUM(UnBilledAmount)", "").ToString();
                dt.Rows[RInd][2] = dt.Compute("SUM(NotDueAmount)", "").ToString();
                dt.Rows[RInd][4] = dt.Compute("SUM(Slot_30Days)", "").ToString();
                dt.Rows[RInd][5] = dt.Compute("SUM(Slot_45Days)", "").ToString();
                dt.Rows[RInd][6] = dt.Compute("SUM(Slot_60Days)", "").ToString();
                dt.Rows[RInd][7] = dt.Compute("SUM(Slot_75Days)", "").ToString();
                dt.Rows[RInd][8] = dt.Compute("SUM(Slot_76Days)", "").ToString();
                dt.Rows[RInd][9] = dt.Compute("SUM(TotalAbove30)", "").ToString();
                dt.Rows[RInd][10] = dt.Compute("SUM(Total)", "").ToString();
                dt.Rows[RInd][11] = dt.Compute("SUM(AmountOverDue)", "").ToString();
                dt.Rows[RInd][12] = dt.Compute("SUM(AmountExpected)", "").ToString();
                dt.Rows[RInd][15] = dt.Compute("SUM(ExpAmount)", "").ToString();
                dt.Rows[RInd][17] = dt.Compute("SUM(PaymentRcvd)", "").ToString();
                dt.Rows[RInd][18] = dt.Compute("SUM(PODRcvdBillPending)", "").ToString();
                dt.Rows[RInd][19] = dt.Compute("SUM(PODNotRcvdButDeliveredAmount)", "").ToString();

                dt.AcceptChanges();
            }    
            return dt;
        }

        private void AllCompanyExport()
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\CustomerReceivable_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            DataTable dt = CommonTypes.CreateDataTable("CustomerName,UnBilledAmount,NotDueAmount,CreditDays,Slot_30Days,Slot_45Days,Slot_60Days,Slot_75Days,Slot_76Days,TotalAbove30,Total,AmountOverDue,AmountExpected,AmountExpectedOn,ExpDate,ExpAmount,LatestRemarks,PaymentRcvd,PODRcvdBillPending,PODNotRcvdButDeliveredAmount", "dtSuplPayableApproval", "string,decimal,decimal,int,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,decimal,dateTime,dateTime,decimal,string,decimal,decimal,decimal");
            DataTable dtComp = null;
            ArrayList Countdt = new ArrayList();
            for (int i = 0; i < cmbBillingCompany.Items.Count; i++)
            {
                dtComp = ReturnData(long.Parse(cmbBillingCompany.Items[i].Value), cmbBillingCompany.Items[i].Text);
                Countdt.Add(dtComp.Rows.Count);

                foreach (DataRow dr in dtComp.Rows)
                {
                    DataRow drNew = dt.NewRow();
                    drNew.ItemArray = dr.ItemArray;
                    dt.Rows.Add(drNew);                    
                }
            }
            if (Countdt.Count > 1)
            {
                dt.Rows.Add();
                int Rcnt = dt.Rows.Count - 1;
                int tot = 0;
                dt.Rows[Rcnt][0] = "Grand Total";
                dt.Rows[Rcnt][1] = "0";
                dt.Rows[Rcnt][2] = "0";
                dt.Rows[Rcnt][4] = "0";
                dt.Rows[Rcnt][5] = "0";
                dt.Rows[Rcnt][6] = "0";
                dt.Rows[Rcnt][7] = "0";
                dt.Rows[Rcnt][8] = "0";
                dt.Rows[Rcnt][9] = "0";
                dt.Rows[Rcnt][10] = "0";
                dt.Rows[Rcnt][11] = "0";
                dt.Rows[Rcnt][12] = "0";
                dt.Rows[Rcnt][15] = "0";
                dt.Rows[Rcnt][17] = "0";
                dt.Rows[Rcnt][18] = "0";
                dt.Rows[Rcnt][19] = "0";
                for (int i = 0; i < Countdt.Count; i++)
                {
                    if (int.Parse(Countdt[i].ToString())>0)
                    {
                        tot = tot + int.Parse(Countdt[i].ToString());
                        dt.Rows[Rcnt][1] = decimal.Parse(dt.Rows[Rcnt][1].ToString()) + decimal.Parse(dt.Rows[tot - 1][1].ToString());
                        dt.Rows[Rcnt][2] = decimal.Parse(dt.Rows[Rcnt][2].ToString()) + decimal.Parse(dt.Rows[tot - 1][2].ToString());
                        dt.Rows[Rcnt][4] = decimal.Parse(dt.Rows[Rcnt][4].ToString()) + decimal.Parse(dt.Rows[tot - 1][4].ToString());
                        dt.Rows[Rcnt][5] = decimal.Parse(dt.Rows[Rcnt][5].ToString()) + decimal.Parse(dt.Rows[tot - 1][5].ToString());
                        dt.Rows[Rcnt][6] = decimal.Parse(dt.Rows[Rcnt][6].ToString()) + decimal.Parse(dt.Rows[tot - 1][6].ToString());
                        dt.Rows[Rcnt][7] = decimal.Parse(dt.Rows[Rcnt][7].ToString()) + decimal.Parse(dt.Rows[tot - 1][7].ToString());
                        dt.Rows[Rcnt][8] = decimal.Parse(dt.Rows[Rcnt][8].ToString()) + decimal.Parse(dt.Rows[tot - 1][8].ToString());
                        dt.Rows[Rcnt][9] = decimal.Parse(dt.Rows[Rcnt][9].ToString()) + decimal.Parse(dt.Rows[tot - 1][9].ToString());
                        dt.Rows[Rcnt][10] = decimal.Parse(dt.Rows[Rcnt][10].ToString()) + decimal.Parse(dt.Rows[tot - 1][10].ToString());
                        dt.Rows[Rcnt][11] = decimal.Parse(dt.Rows[Rcnt][11].ToString()) + decimal.Parse(dt.Rows[tot - 1][11].ToString());
                        dt.Rows[Rcnt][12] = decimal.Parse(dt.Rows[Rcnt][12].ToString()) + decimal.Parse(dt.Rows[tot - 1][12].ToString());
                        dt.Rows[Rcnt][15] = decimal.Parse(dt.Rows[Rcnt][15].ToString()) + decimal.Parse(dt.Rows[tot - 1][15].ToString());
                        dt.Rows[Rcnt][17] = decimal.Parse(dt.Rows[Rcnt][17].ToString()) + decimal.Parse(dt.Rows[tot - 1][17].ToString());                        
                        dt.Rows[Rcnt][18] = decimal.Parse(dt.Rows[Rcnt][18].ToString()) + decimal.Parse(dt.Rows[tot - 1][18].ToString());
                        dt.Rows[Rcnt][19] = decimal.Parse(dt.Rows[Rcnt][19].ToString()) + decimal.Parse(dt.Rows[tot - 1][19].ToString()); 
                    }                    
                }                
            }
            dt.Columns[0].ColumnName = "Customer Name";
            dt.Columns[1].ColumnName = "Unbilled";
            dt.Columns[2].ColumnName = "Not due";
            dt.Columns[3].ColumnName = "Credit Days";
            dt.Columns[4].ColumnName = "30 Days";
            dt.Columns[5].ColumnName = "31 To 45 Days";
            dt.Columns[6].ColumnName = "46 To 60 Days";
            dt.Columns[7].ColumnName = "61 To 75 Days";
            dt.Columns[8].ColumnName = "76 & Above";
            dt.Columns[9].ColumnName = "Total Above 30 Days";
            dt.Columns[10].ColumnName = "Total";
            dt.Columns[11].ColumnName = "OverDue Amount";
            dt.Columns[12].ColumnName = "Due Amount Expected";
            dt.Columns[13].ColumnName = "Dues Expected On";
            dt.Columns[14].ColumnName = "Exp Date";
            dt.Columns[15].ColumnName = "Exp Amount";
            dt.Columns[16].ColumnName = "Latest Remarks";
            dt.Columns[17].ColumnName = "Payment Rcvd Current Month";
            dt.Columns[18].ColumnName = "POD Rcvd Bill Pending";
            dt.Columns[19].ColumnName = "POD Not Rcvd But Delivered";
            ExportToExcel(dt, filename,Countdt);
        }

        private void ExportToExcel(DataTable ds, string XLPath, ArrayList Countdt)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet                
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["N:N"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["O:O"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:Q1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:T1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);        
                }
                using (ExcelRange objRange = objWorksheet.Cells["A1:T1".Replace("T1", "T"+(ds.Rows.Count + 1).ToString())])
                {                                        
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }

                using (ExcelRange objRange = objWorksheet.Cells["E2:E1".Replace("1", (ds.Rows.Count + 1).ToString())])
                {
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(197, 224, 178));
                }
                using (ExcelRange objRange = objWorksheet.Cells["F2:H1".Replace("1", (ds.Rows.Count + 1).ToString())])
                {
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(248, 203, 172));
                }
                int tot = 0;
                for (int i = 0; i < Countdt.Count; i++)
                {
                    tot = tot + int.Parse(Countdt[i].ToString());
                    using (ExcelRange objRange = objWorksheet.Cells["A1:T1".Replace("1", (tot + 1).ToString())])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                        objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium; 
                    }
                }
                if (Countdt.Count > 1)
                {
                    using (ExcelRange objRange = objWorksheet.Cells["A1:T1".Replace("1", (tot + 2).ToString())])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                        objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        objRange.Style.Border.Top.Style = ExcelBorderStyle.Thick;
                        objRange.Style.Border.Left.Style = ExcelBorderStyle.Thick;
                        objRange.Style.Border.Right.Style = ExcelBorderStyle.Thick;
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thick; 
                    }
                }
               
                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(btnExcelAll);
            if (!IsPostBack)
            {
                FillDropDowns();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                cmbBillingCompany.Focus();
            }
        }

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            setGrid();
            btnShow.Focus();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (grdMain.Rows.Count == 0)
            {
                return;
            }
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "CustomerReceivable_" + cmbBillingCompany.SelectedItem.Text + "_" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            grdMain.GridLines = GridLines.Both;
            for (int i = 0; i < grdMain.Columns.Count; i++)
            {
                grdMain.HeaderRow.Cells[i].Font.Bold = true;
                grdMain.HeaderRow.Cells[i].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                grdMain.HeaderRow.Cells[i].ForeColor = System.Drawing.Color.White;
            }
            grdMain.RowStyle.Height = 20;

            grdMain.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        protected void btnExcelAll_Click(object sender, EventArgs e)
        {
            if (grdMain.Rows.Count == 0)
            {
                return;
            }
            AllCompanyExport();  
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Replace(".00", "");
                }
            }
        }
    }
}