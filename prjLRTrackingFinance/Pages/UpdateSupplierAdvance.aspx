﻿<%@ Page Title="Update Supplier Advance" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="UpdateSupplierAdvance.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateSupplierAdvance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Update Supplier Advance
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 90%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="1" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="2" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the Payment date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label1" Text="Supplier" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbSupl" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupl_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbSupl" ErrorMessage="Please select Supplier from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label4" Text="LR No" runat="server" AssociatedControlID="cmbLR" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbLR" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbLR_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="LR" runat="server" ControlToValidate="cmbLR" ErrorMessage="Please select LR from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="LRCall" runat="server" Enabled="True" TargetControlID="LR">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top" style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top" style="width: 120px">
                                                        <asp:Label ID="Label3" Text="Payment Mode" runat="server" AssociatedControlID="cmbPaymentMode" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbPaymentMode" runat="server" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" Width="200px" MaxLength="100"
                                                            TabIndex="3" CssClass="WindowsStyle" RenderMode="Block" OnSelectedIndexChanged="cmbPaymentMode_SelectedIndexChanged"
                                                            AutoPostBack="True">
                                                            <asp:ListItem Text="" Value="-1" />
                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                            <asp:ListItem Value="2">Cheque</asp:ListItem>
                                                            <asp:ListItem Value="3">DD</asp:ListItem>
                                                            <asp:ListItem Value="4">Net Banking</asp:ListItem>
                                                            <asp:ListItem Value="5">Debit Card</asp:ListItem>
                                                            <asp:ListItem Value="6">Credit Card</asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvpaymode" runat="server" ErrorMessage="Please select payment mode from the list"
                                                            ValidationGroup="save" ControlToValidate="cmbPaymentMode" Display="None" SetFocusOnError="true"
                                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvpaymodeCall" runat="server" Enabled="True" TargetControlID="rfvpaymode">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblchqddDate" Text="Cheque Date" runat="server" AssociatedControlID="txtchqdt" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtchqdt" Width="100px" runat="server" TabIndex="4" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtchqdt" PopupButtonID="imgBtnCalcPopupPODate1"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate1" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Height="22px"
                                                            ImageAlign="AbsMiddle" Width="22px" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfvchqDt" runat="server" ErrorMessage="Please select the Cheque/DD date"
                                                            ValidationGroup="save" ControlToValidate="txtchqdt" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvchqDtCall" runat="server" Enabled="True" TargetControlID="rfvchqDt">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trBank" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblBank" Text="Bank Name" runat="server" AssociatedControlID="cmbBank" />
                                                    </td>
                                                    <td>
                                                        <asp:ComboBox ID="cmbBank" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDown"
                                                            AppendDataBoundItems="true" ItemInsertLocation="Append" Width="300px" MaxLength="100"
                                                            TabIndex="6" CssClass="WindowsStyle" RenderMode="Block">
                                                            <asp:ListItem Value=""></asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="CustomBank" runat="server" ControlToValidate="cmbBank" ErrorMessage="Please select bank from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true" Enabled="false"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="CustomBankCall" runat="server" Enabled="True" TargetControlID="CustomBank">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trChqNo" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblChqDDNo" Text="Cheque No" runat="server" AssociatedControlID="TxtChqNo" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtChqNo" Width="300px" runat="server" TabIndex="7" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the Cheque/DD/Transaction no."
                                                            ValidationGroup="save" ControlToValidate="TxtChqNo" Display="None" SetFocusOnError="true"
                                                            Enabled="false"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true" TargetControlID="reqFVChqNo">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label5" runat="server" AssociatedControlID="txtAmount" Text="Cash Amount" />
                                                    </td>
                                                    <td style="width: 300px">
                                                        <asp:TextBox ID="txtCashAmount" runat="server" AutoPostBack="True" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="true" OnTextChanged="txtCashAmount_TextChanged" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCashAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtCashAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCashAmount"
                                                            Display="None" ErrorMessage="Please enter valid cash amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:CompareValidator ID="compCashAmt" runat="server" Enabled="true" ForeColor="Red"
                                                            ControlToValidate="txtCashAmount" ControlToCompare="txtCashAmountBal" Operator="LessThanEqual"
                                                            SetFocusOnError="true" ValidationGroup="save" Type="Double" Display="Dynamic"
                                                            ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle">Cash Amount cannot be greater than Balance'></asp:CompareValidator>
                                                    </td>
                                                    <td style="width: 150px">
                                                        <asp:Label ID="Label8" runat="server" AssociatedControlID="txtCashAmountBal" Text="Balance Cash Amount" />
                                                        &nbsp;&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCashAmountBal" runat="server" AutoPostBack="false" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="false" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" AssociatedControlID="txtAmount" Text="Fuel Amount" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFuelAmount" runat="server" AutoPostBack="True" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="true" OnTextChanged="txtFuelAmount_TextChanged" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFuelAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtFuelAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFuelAmount"
                                                            Display="None" ErrorMessage="Please enter valid fuel amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:CompareValidator ID="CompFuel" runat="server" Enabled="true" ForeColor="Red"
                                                            ControlToValidate="txtFuelAmount" ControlToCompare="txtFuelAmountBal" Operator="LessThanEqual"
                                                            SetFocusOnError="true" ValidationGroup="save" Type="Double" Display="Dynamic"
                                                            ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle">Fuel Amount cannot be greater than Balance'></asp:CompareValidator>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label7" runat="server" AssociatedControlID="txtFuelAmountBal" Text="Balance Fuel Amount" />
                                                        &nbsp;&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFuelAmountBal" runat="server" AutoPostBack="false" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="false" />
                                                    </td>
                                                </tr>
                                                <br />
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Amount" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="false" MaxLength="100" TabIndex="8"
                                                            Width="100px" Style="text-align: right" CssClass="NormalTextBold" Enabled="false" />
                                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                            TargetControlID="txtAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtAmount"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <%--<table width="100%">
                                                <asp:GridView ID="GrdPaymentStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                    DataKeyNames="" EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                    <Columns>
                                                        <asp:BoundField DataField="BillCnt" HeaderText="Total Bills" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="BillAmt" HeaderText="Bill Amount" ItemStyle-Width="200px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="PaidAmt" HeaderText="Paid Amount" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Outstanding" HeaderText="OutStanding" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="header" />
                                                    <RowStyle CssClass="row" />
                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                </asp:GridView>
                                            </table>--%>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="10" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="11" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbSupl" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cmbPaymentMode" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });

            $('#<%= txtchqdt.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtchqdt.ClientID %>').val('');
                }
            });

        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
