﻿<%@ Page Title="Supplier Master" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="SupplierMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Supplier Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2">
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td colspan="2" style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="Supplier Info" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td style="width: 50%">
                                                <table class="control_set" style="width: 100%">
                                                    <tr class="control_row">
                                                        <td>
                                                            <asp:Label ID="Label1" runat="server" Text="Name" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtName" runat="server" Width="400px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfv" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Name'
                                                                ControlToValidate="txtName" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label2" runat="server" Text="Address Line1" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAddress1" runat="server" Width="400px" TabIndex="2" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Address Line 1'
                                                                ControlToValidate="txtAddress1" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server" Text="Address Line2" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAddress2" runat="server" Width="400px" TabIndex="3" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label4" runat="server" Text="Address Line3" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAddress3" runat="server" Width="400px" TabIndex="4" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label5" runat="server" Text="Contact No." CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtLandline" runat="server" Width="300px" TabIndex="5" CssClass="NormalTextBold"
                                                                MaxLength="200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label6" runat="server" Text="WhatAapp No." CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMobile" runat="server" Width="300px" TabIndex="6" CssClass="NormalTextBold"
                                                                MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                                FilterType="Numbers" TargetControlID="txtMobile" Enabled="True"></ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label7" runat="server" Text="Email ID" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtEmailID" runat="server" Width="300px" TabIndex="7" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                            <asp:RegularExpressionValidator runat="server" ID="regexpEmail" SetFocusOnError="true"
                                                                ValidationGroup="save" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter valid email id'
                                                                ControlToValidate="txtEmailId" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
                                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter email id'
                                                                ControlToValidate="txtEmailId" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label11" runat="server" Text="Vendor Code" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtVendor" runat="server" Width="200px" TabIndex="8" CssClass="NormalTextBold"
                                                                Enabled="false" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label8" runat="server" Text="GST No." CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCSTNo" runat="server" Width="200px" TabIndex="9" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <%--<tr>
											<td>
												<asp:Label ID="Label9" runat="server" Text="VAT No." CssClass="NormalTextBold"></asp:Label>
												&nbsp;<span style="color: red"></span>
											</td>
											<td>
												<asp:TextBox ID="txtVAT" runat="server" Width="200px" TabIndex="10" 
													CssClass="NormalTextBold" MaxLength="50"></asp:TextBox>                                             
											</td>
										</tr>
										  <tr>
											<td>
												<asp:Label ID="Label10" runat="server" Text="TIN No." CssClass="NormalTextBold"></asp:Label>
												&nbsp;<span style="color: red"></span>
											</td>
											<td>
												<asp:TextBox ID="txtTIN" runat="server" Width="200px" TabIndex="11" 
													CssClass="NormalTextBold" MaxLength="50"></asp:TextBox>                                             
											</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="Label12" runat="server" Text="Service Tax No." CssClass="NormalTextBold"></asp:Label>
												&nbsp;<span style="color: red"></span>
											</td>
											<td>
												<asp:TextBox ID="txtServiceTaxNo" runat="server" Width="200px" TabIndex="12" 
													CssClass="NormalTextBold" MaxLength="50"></asp:TextBox>                                             
											</td>
										</tr>--%>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label14" runat="server" Text="Credit Days" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCreditDays" runat="server" Width="100px" TabIndex="13" CssClass="NormalTextBold"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilterWeight" runat="server" FilterType="Numbers"
                                                                TargetControlID="txtCreditDays" Enabled="True">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label15" runat="server" Text="Credit Limit" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCreditLimit" runat="server" Width="100px" TabIndex="14" CssClass="NormalTextBold"
                                                                MaxLength="10"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                FilterType="Numbers" TargetControlID="txtCreditLimit" Enabled="True">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label9" runat="server" Text="Advance %" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancePer" runat="server" Width="100px" TabIndex="15" CssClass="NormalTextBold"
                                                                MaxLength="5"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtAdvancePer" Enabled="True">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                            <asp:RegularExpressionValidator ID="rngAdvancePer" runat="server" ErrorMessage="Please enter valid Advance %"
                                                                ControlToValidate="txtAdvancePer" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$"
                                                                ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label18" runat="server" Text="TDS %" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTDSPer" runat="server" Width="100px" TabIndex="15" CssClass="NormalTextBold"
                                                                MaxLength="5"></asp:TextBox>
                                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                                FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtTDSPer" Enabled="True">
                                                            </ajaxToolkit:FilteredTextBoxExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter valid TDS %"
                                                                ControlToValidate="txtTDSPer" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$"
                                                                ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label10" runat="server" Text="Agreement Done" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <ajaxToolkit:ComboBox ID="cmbAgreementDone" runat="server" AutoCompleteMode="SuggestAppend"
                                                                DropDownStyle="DropDownList" ItemInsertLocation="Append" Width="200px" MaxLength="100"
                                                                TabIndex="15" CssClass="WindowsStyle" RenderMode="Block">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            </ajaxToolkit:ComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label12" runat="server" Text="Agreement Date" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAgreementDate" Width="100px" runat="server" TabIndex="15" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" TargetControlID="txtAgreementDate"
                                                                PopupButtonID="imgBtnCalcPopupDate" runat="server" Format="dd/MM/yyyy" Enabled="True"
                                                                DaysModeTitleFormat="MMM yyyy">
                                                            </ajaxToolkit:CalendarExtender>
                                                            <asp:ImageButton ID="imgBtnCalcPopupDate" runat="server" AlternateText="Popup Button"
                                                                ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                                Width="22px" TabIndex="15" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label31" runat="server" Text="Valid Till Date" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtValidTillDate" Width="100px" runat="server" TabIndex="16" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtValidTillDate"
                                                                PopupButtonID="imgValidtill" runat="server" Format="dd/MM/yyyy" Enabled="True"
                                                                DaysModeTitleFormat="MMM yyyy">
                                                            </ajaxToolkit:CalendarExtender>
                                                            <asp:ImageButton ID="imgValidtill" runat="server" AlternateText="Popup Button" ImageUrl="~/Include/Common/images/calendar_img.png"
                                                                Height="22px" ImageAlign="AbsMiddle" Width="22px" TabIndex="17" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label33" runat="server" Text="Type" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="cmbSuplType" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                                MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                                DropDownStyle="DropDownList" RenderMode="Block">
                                                            </asp:ComboBox>
                                                            <asp:CustomValidator ID="SuplType" runat="server" ControlToValidate="cmbSuplType"
                                                                ErrorMessage="Please select supplier type from the list" ClientValidationFunction="ValidatorCombobox"
                                                                Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                            <asp:ValidatorCalloutExtender ID="SuplTypeCall" runat="server" Enabled="True" TargetControlID="SuplType">
                                                            </asp:ValidatorCalloutExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label16" runat="server" Text="Active" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkActive" runat="server" TabIndex="18" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label17" runat="server" Text="GL" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtGL" runat="server" Width="400px" TabIndex="19" CssClass="NormalTextBold"
                                                                MaxLength="200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label19" runat="server" Text="Firm Name" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFirmName" runat="server" Width="300px" TabIndex="20" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvFirmName" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtFirmName" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label20" runat="server" Text="VendorType" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="cmbVendorType" Width="100px" TabIndex="21">
                                                                <asp:ListItem Value="O" Selected="True">Owner</asp:ListItem>
                                                                <asp:ListItem Value="B">Broker</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label21" runat="server" Text="Contact Person" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtContactPerson" runat="server" Width="300px" TabIndex="22" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtContactPerson" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label22" runat="server" Text="Aadhar No." CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red"></span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAadharNo" runat="server" Width="300px" TabIndex="23" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label23" runat="server" Text="PAN" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPAN" runat="server" Width="300px" TabIndex="24" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtPAN" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label24" runat="server" Text="Searched By" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="cmbSearchedBy" runat="server" AutoCompleteMode="SuggestAppend"
                                                                Width="250px" MaxLength="200" TabIndex="25" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                                DropDownStyle="DropDownList" RenderMode="Block">
                                                            </asp:ComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label25" runat="server" Text="Approved By" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="cmbApprovedBy" runat="server" AutoCompleteMode="SuggestAppend"
                                                                Width="250px" MaxLength="200" TabIndex="26" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                                DropDownStyle="DropDownList" RenderMode="Block">
                                                            </asp:ComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label26" runat="server" Text="Bank Name" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBank" runat="server" Width="300px" TabIndex="27" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtBank" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label27" runat="server" Text="Bank Branch" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtBankBranch" runat="server" Width="300px" TabIndex="28" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtBankBranch" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label28" runat="server" Text="A/c No." CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAccountNo" runat="server" Width="300px" TabIndex="29" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtAccountNo" SetFocusOnError="True" ValidationGroup="save"
                                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label29" runat="server" Text="IFSC" CssClass="NormalTextBold"></asp:Label>
                                                            &nbsp;<span style="color: red">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtIFSC" runat="server" Width="300px" TabIndex="30" CssClass="NormalTextBold"
                                                                MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                                ControlToValidate="txtIFSC" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label13" runat="server" Text="Hold Payment" CssClass="NormalTextBold"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkHoldPayment" runat="server" TabIndex="31" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label34" runat="server" Text="Hold Remarks" CssClass="NormalTextBold"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtHoldRemarks" runat="server" Width="400px" TabIndex="32" CssClass="NormalTextBold"
                                                                MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 48%; vertical-align: top">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                            <asp:Label ID="Label30" runat="server" Text="Destinations" ForeColor="#3A66AF" Font-Bold="true"
                                                                Font-Size="13px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 1px solid #3A66AF">
                                                            <div style="height: 340px; overflow: auto">
                                                                <asp:GridView ID="grdViewDestinations" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                                    DataKeyNames="Srl,Name">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox runat="server" ID="chkSelect" Checked='<%# Eval("Selected").ToString()=="1"?true:Eval("Selected").ToString()=="true"?true:false %>' /></ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Destination" SortExpression="Name" />
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="header" />
                                                                    <RowStyle CssClass="row" />
                                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                                    <PagerSettings Mode="Numeric" />
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                            <asp:Label ID="Label32" runat="server" Text="Vehicle(s) Own" ForeColor="#3A66AF"
                                                                Font-Bold="true" Font-Size="13px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border: 1px solid #3A66AF">
                                                            <div style="height: 340px; overflow: auto">
                                                                <asp:GridView ID="grdViewVehicleType" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                                    DataKeyNames="Srl,Name">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox runat="server" ID="chkSelect" Checked='<%#Eval("Selected").ToString()=="1"?true:Eval("Selected").ToString()=="true"?true:false %>' /></ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Vehicle Type" SortExpression="Name" />
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="header" />
                                                                    <RowStyle CssClass="row" />
                                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                                    <PagerSettings Mode="Numeric" />
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="31" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" OnClick="btnSave_Click" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="32" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="355px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" Width="350px" CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAdd" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" />&nbsp; &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotal" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="Srl,Name,Address1,Address2,Address3,Landline,Mobile,EmailId,VendorCode,CSTNo,VATNo,TINNo,ServiceTaxNo,CreditDays,CreditLimit,IsActive,UsedCnt,GLCode,AdvancePer,AgreementDone,AgreementDate,TDSPer,
                                                              FirmName,VendorType,VendorTypeName,ContactPersonName,AadharNo,PAN,SearchedBy,ApprovedBy,BankName,BankBranch,AccountNo,IFSC,ValidTillDate,SupplierType,HoldPayment,HoldRemarks"
                                                OnPageIndexChanging="grdView_PageIndexChanging" OnRowDataBound="grdView_RowDataBound"
                                                OnRowCommand="grdView_RowCommand" AllowSorting="True" OnSorting="grdView_Sorting">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Name"
                                                        SortExpression="Name">
                                                        <ItemStyle Width="200px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code" SortExpression="VendorCode">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Address1" HeaderText="Address" SortExpression="Address1"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Landline" HeaderText="Landline" SortExpression="Landline"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" ItemStyle-Width="200px">
                                                        <ItemStyle Width="200px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AdvancePer" HeaderText="Advance %" SortExpression="AdvancePer">
                                                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="TDSPer" HeaderText="TDS" SortExpression="TDSPer">
                                                        <ItemStyle Width="100px" HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                     <asp:ButtonField ButtonType="Link" CommandName="Doc" Text="@" HeaderText="Documents" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" /> 
                                                      
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
