﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class AddTripFreightTiger : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAddTrip_Click(object sender, EventArgs e)
        {
            AddTrip("AS1901234", "Mumbai", 19.0759837, 72.87765590000004, "Mahabaleshwar", 17.9307285, 73.64773420000006, "Shabbir", "9877366637", "MH12NQ4089", "EW1902020", "Inv6633");
        }

        private void AddTrip(string refNo, string originAddr, double orglatitude, double orglongitute, string destinationAddr, double destlatitude, double destlongitute,string driverName,string driverNo,string vehicleNumber,string ewayBillNo,string invoiceNo)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");

                var lrObject = new
                {
                    lrnumber = refNo,
                    loading = new
                    {
                        lat = orglatitude,
                        lng = orglongitute,
                        address = originAddr,
                        area = originAddr
                    },
                    unloading = new
                    {
                        lat = destlatitude,
                        lng = destlongitute,
                        address = destinationAddr,
                        area = destinationAddr
                    },
                    vehicleNumber = vehicleNumber,
                    locationSource = "sim",
                    driverName = driverName,
                    driverNumbers = new string[] {
                        driverNo
                    },
                    customValues = new
                    {
                        ewayBillNo = ewayBillNo,
                        invoiceNumber = invoiceNo
                    }
                };

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/add");
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.UploadString(urlStringBuilder.ToString(), javaScriptSerializer.Serialize(lrObject));
                string str = @"""id""" + ":";
                if (!result.ToString().Contains(str))
                {
                    return;
                }
                string tripId = result.ToString();
                var offset = tripId.IndexOf(':');
                var endOffset = tripId.IndexOf('}')-1;
                offset = tripId.IndexOf(':', offset + 1);
                offset = tripId.IndexOf(':', offset + 1);

                tripId = tripId.Substring(offset + 1, endOffset-offset );
                 


                var responseObject = javaScriptSerializer.DeserializeObject(result);

                
                
            }
        }

        private void CloseTrip(string tripId,string uniqueId,string closeDateTime,string comment)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();

            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");

                var lrObject = new
                {
                    trip_id = tripId,
                    feed_unique_id = uniqueId,
                    close_date_time= closeDateTime,
                    comment = comment
                };

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/close");
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.UploadString(urlStringBuilder.ToString(), javaScriptSerializer.Serialize(lrObject));

                var responseObject = javaScriptSerializer.DeserializeObject(result);

                //var status = responseObject["status"];

            }
        }

        private void GetTripDetails(string tripId)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();

            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");

               

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/id/"+tripId);
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.DownloadString(urlStringBuilder.ToString());

                 int pos =  result.IndexOf("last_known_location");
                //var responseObject = javaScriptSerializer.DeserializeObject<Dictionary<string, dynamic>>(result);
                 var offset = result.IndexOf(':', pos);
                 var endOffset = result.IndexOf(',', pos) - 1;

                 result = result.Substring(offset+1, endOffset - offset );

               var responseObject = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(result);
                
            }
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            GetTripDetails("448882");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseTrip("448149", "1", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Closed");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            EditTrip(0,"AS1901234", "Mumbai", 19.0759837, 72.87765590000004, "Mahabaleshwar", 17.9307285, 73.64773420000006, "Shabbir", "9877366637", "MH12NQ4089", "EW1902020", "Inv6633");

        }

        private void EditTrip(long lrid,string refNo, string originAddr, double orglatitude, double orglongitute, string destinationAddr, double destlatitude, double destlongitute, string driverName, string driverNo, string vehicleNumber, string ewayBillNo, string invoiceNo)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");

                var lrObject = new
                {
                    lrid = lrid,
                    lrnumber = refNo,
                    loading = new
                    {
                        lat = orglatitude,
                        lng = orglongitute,
                        address = originAddr,
                        area = originAddr
                    },
                    unloading = new
                    {
                        lat = destlatitude,
                        lng = destlongitute,
                        address = destinationAddr,
                        area = destinationAddr
                    },
                    vehicleNumber = vehicleNumber,
                    locationSource = "sim",
                    driverName = driverName,
                    driverNumbers = new string[] {
                        driverNo
                    },
                    customValues = new
                    {
                        ewayBillNo = ewayBillNo,
                        invoiceNumber = invoiceNo
                    }
                };

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/update");
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.UploadString(urlStringBuilder.ToString(), javaScriptSerializer.Serialize(lrObject));
                //string str = @"""id""" + ":";
                //if (!result.ToString().Contains(str))
                //{
                //    return;
                //}
                //string tripId = result.ToString();
                //var offset = tripId.IndexOf(':');
                //var endOffset = tripId.IndexOf('}') - 1;
                //offset = tripId.IndexOf(':', offset + 1);
                //offset = tripId.IndexOf(':', offset + 1);

                //tripId = tripId.Substring(offset + 1, endOffset - offset);



                //var responseObject = javaScriptSerializer.DeserializeObject(result);



            }
        }
    }
}