﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UploadFTReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnSave);
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {

            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strXLFilePath = MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString()) + "\\" + System.IO.Path.GetFileName(flUpload.FileName);
                string strExtension = System.IO.Path.GetExtension(flUpload.FileName);
                flUpload.SaveAs(strXLFilePath);

                string conStr = "";
                long cnt = -1;

                switch (strExtension.ToLower())
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;

                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ToString();
                        break;
                }

                conStr = String.Format(conStr, strXLFilePath, "YES");

                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();

                DataTable dt = new DataTable("Record");
                cmdExcel.Connection = connExcel;


                string SheetName = string.Empty;
                string Otype = string.Empty;
                // Get the name of First Sheet

                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();


                // Read Data from First Sheet


                connExcel.Open();


                cmdExcel.CommandText = "SELECT * from [" + SheetName + "]";

                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);

                connExcel.Close();


                string sqlConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
                using (SqlConnection con = new SqlConnection(sqlConnectionString))
                {
                    SqlCommand cmd;
                    con.Open();
                    cmd = new SqlCommand("DELETE FROM FTTemplate", con);
                    cmd.ExecuteNonQuery();
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnectionString))
                    {
                        bulkCopy.DestinationTableName = "FTTemplate";
                        bulkCopy.BulkCopyTimeout = 0;
                        bulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }

                CommonBO optbo = new CommonBO();
                cnt = optbo.UploadFreightTigerDataDump();
                if (cnt > 0)
                {
                    SetPanelMsg(cnt.ToString() + " Records uploaded successfully", true, 1);
                }


            }
            catch (Exception exp)
            {
                SetPanelMsg(exp.Message, true, 1);
                //Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }



        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }


    }
}