﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessObjects.BO;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DataDump : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);

            if (!IsPostBack)
            {
                txtFromDate.Text = DateTime.Now.AddDays(-180).ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            DatadumpBO optbo = new DatadumpBO();
            optbo.FromDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            optbo.ToDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DataSet ds = optbo.GetDataDump();
            string XLPath = Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString())+"\\DataDump.xlsx";
            optbo.ds = ds;
            optbo.XLPath = XLPath;

            ////string sheetName = string.Empty;
            ////HttpContext.Current.Response.Clear();
            ////HttpContext.Current.Response.ClearContent();
            ////HttpContext.Current.Response.ClearHeaders();
            ////HttpContext.Current.Response.Buffer = true;
            ////HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
            ////HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            ////HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ////HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=Datadump.xlsx");

            ////using (ExcelPackage objExcelPackage = new ExcelPackage())
            ////{
            ////    for (int i = 0; i < ds.Tables.Count; i++)
            ////    {
            ////        sheetName = "Sheet_" + i.ToString(); //GetSheetName(i);
            ////        //Create the worksheet    
            ////        ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(sheetName);
            ////        //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1    
            ////        objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
            ////        //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12));
            ////        objWorksheet.Cells.AutoFitColumns();

            ////        using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
            ////        {
            ////            objRange.Style.Font.Bold = true;
            ////            objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            ////            objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            ////            objRange.AutoFilter = true;
            ////            //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
            ////            //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua);
            ////        }

            ////    }
            ////    var ms = new System.IO.MemoryStream();
            ////    objExcelPackage.SaveAs(ms);
            ////    ms.WriteTo(HttpContext.Current.Response.OutputStream);
            ////}
            optbo.SaveExcelWorkSheetsEscalation();
            Session["FilePath"] = XLPath;
            Response.Redirect("ViewFile.aspx");
        }

        public void SaveExcelWorkSheetsEscalation(DataSet ds, string XLPath)
        {
           

        }
    }
}