﻿<%@ Page Title="Supplier Invoice PTL" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="SupplierInvoicePTL.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierInvoicePTL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete LR?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--Penalty Popup--%>
            <asp:Button ID="btnPenaltyPop" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlPenaltyPop" runat="server" CssClass="modalPopup" Style="display: none;
                width: 550px; height: 370px">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Penalty Entry
                </div>
                <div align="center" style="padding: 5px">
                    <table class="control_set" style="width: 100%">
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="LR No" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblLRNoPenalty" runat="server" Text="" CssClass="NormalTextBold" Font-Bold="true"></asp:Label>
                                <asp:HiddenField ID="HFLRId" runat="server" Value="0" />
                                <asp:HiddenField ID="HFRowId" runat="server" Value="0" />
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label9" runat="server" Text="Penalty Remark" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="cmbReason" runat="server" CssClass="NormalTextBold" Width="250px"
                                    TabIndex="1">
                                </asp:DropDownList>
                                <%-- <asp:ComboBox ID="cmbReason" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                    MaxLength="200" TabIndex="9" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="false">
                                </asp:ComboBox>--%>
                                <asp:CustomValidator ID="custValidReason" runat="server" ControlToValidate="cmbReason"
                                    ErrorMessage="Please select Reason from the list" ClientValidationFunction="ValidatorCombobox"
                                    Display="Dynamic" ValidateEmptyText="true" ValidationGroup="confirmPenalty" SetFocusOnError="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td style="width: 120px">
                                <asp:Label ID="Label6" runat="server" Text="Penalty Charges" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td style="width: 250px">
                                <asp:TextBox runat="server" ID="txtPenalty" CssClass="NormalTextBold" TabIndex="2"
                                    Style="text-align: right" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                    ControlToValidate="txtPenalty" SetFocusOnError="True" ValidationGroup="confirmPenalty"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterPenalty" runat="server" FilterType="Numbers"
                                    TargetControlID="txtPenalty" Enabled="True">
                                </ajaxToolkit:FilteredTextBoxExtender>
                                <asp:RegularExpressionValidator ID="revAvailablePeriod" runat="server" ErrorMessage="Must be greater than 0"
                                    ControlToValidate="txtPenalty" ValidationExpression="^[1-9][0-9]*$" SetFocusOnError="true"
                                    ValidationGroup="confirmPenalty">
                                </asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 80px">
                                <asp:Button runat="server" ID="btnConfirm" Text="Confirm" CssClass="button" Width="70px"
                                    TabIndex="3" ValidationGroup="confirmPenalty" OnClick="btnConfirm_Click" />
                            </td>
                            <td style="width: 80px">
                                <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button" Width="70px"
                                    TabIndex="4" OnClick="btnDelete_Click" />
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td colspan="2">
                                <div class="grid_region" style="height: 200px">
                                    <asp:GridView ID="grdItems" runat="server" Width="100%" AutoGenerateColumns="False"
                                        CssClass="grid" DataKeyNames="ReasonSrl,Reason,Charges" EmptyDataText="No Records Found.">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                        TabIndex="5" AutoPostBack="true" /></HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkSelect" TabIndex="6" /></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="28px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason"></asp:BoundField>
                                            <asp:BoundField DataField="Charges" HeaderText="Charges" SortExpression="Charges">
                                                <ItemStyle Width="80px" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button" CommandName="ok"
                        OnCommand="btnConfirm_Command" TabIndex="7" />
                    <asp:Button ID="btnCancelPenalty" runat="server" Text="Cancel" CssClass="button"
                        CommandName="cancel" OnCommand="btnConfirm_Command" TabIndex="8" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popPenalty" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnPenaltyPop" PopupControlID="pnlPenaltyPop" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--End Penalty Popup--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Supplier Invoice Entry PTL
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                                <asp:HiddenField ID="txtRowNumber" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="LR Info" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td style="width: 100px">
                                                <asp:Label ID="Label4" runat="server" Text="Invoice Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtDate" runat="server" Width="80px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td style="width: 120px">
                                                <asp:Label ID="Label5" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label22" runat="server" Text="Supplier" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="4" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block" OnSelectedIndexChanged="cmbCustomer_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Lm2" runat="server" Text="Invoice No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBRefNo" Width="300px" runat="server" TabIndex="5" MaxLength="50" />
                                                <asp:RequiredFieldValidator ID="reqFVRefNo" runat="server" ErrorMessage="Please enter the supplier invoice no"
                                                    ValidationGroup="save" ControlToValidate="txtBRefNo" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:ValidatorCalloutExtender ID="reqFVRefNoCall" runat="server" Enabled="True" TargetControlID="reqFVRefNo">
                                                </asp:ValidatorCalloutExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBdate" runat="server" Width="80px" TabIndex="6" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtBdate" TodaysDateFormat="dd/MMM/yyyy" PopupButtonID="dtpBBtn"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="7" Width="23px" />
                                                <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the supplier invoice date"
                                                    ValidationGroup="save" ControlToValidate="txtBdate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                    TargetControlID="reqFVfrmDate">
                                                </asp:ValidatorCalloutExtender>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdPendingLRs" runat="server" Style="width: 100%" AutoGenerateColumns="False"
                                        AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                        DataKeyNames="LRId,LRNo,LRDate,TransportationCharges,DetentionCharges,WaraiCharges,TwoPointCharges,OtherCharges,Penalty,Narration,LatePODCharges,TotalCharges,Consigner,Consignee,FromLoc,ToLoc,AdvanceToSupplier"
                                        OnRowCommand="grdPendingLRs_RowCommand">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkLR" TabIndex="9" onclick="checkboxclick(this);"
                                                        Checked="true" Enabled="true" AutoPostBack="false" /></ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="28px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="LRNo" HeaderText="LRNo" SortExpression="LRNo" ItemStyle-Width="80px">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                                            <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Consignee" HeaderText="Consignee" SortExpression="Consignee">
                                                <ItemStyle Width="160px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc">
                                                <ItemStyle Width="120px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AdvanceToSupplier" HeaderText="Advance" SortExpression="AdvanceToSupplier">
                                                <ItemStyle Width="80px" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Total Freight" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtFrieght" CssClass="NormalTextBold" TabIndex="9"
                                                        Style="text-align: right" AutoPostBack="false" onchange="txtchanged(this);" Text='<%#Eval("TransportationCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterFrieght" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtFrieght" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detention Charges" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtDetention" CssClass="NormalTextBold" TabIndex="9"
                                                        Style="text-align: right" AutoPostBack="false" onchange="txtchanged(this);" Text='<%#Eval("DetentionCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterDetention" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtDetention" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Warai Charges" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtWarai" CssClass="NormalTextBold" Style="text-align: right"
                                                        TabIndex="9" AutoPostBack="false" onchange="txtchanged(this);" Text='<%#Eval("WaraiCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterWarai" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtWarai" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Two Point Charges" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTwoPoint" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" TabIndex="9" Style="text-align: right" Text='<%#Eval("TwoPointCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterTwoPoint" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtTwoPoint" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Other Charges" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtOtherCharges" CssClass="NormalTextBold" AutoPostBack="false"
                                                        onchange="txtchanged(this);" Style="text-align: right" TabIndex="9" Text='<%#Eval("OtherCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterOther" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtOtherCharges" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:ButtonField ButtonType="Link" CommandName="PenaltyEdit" DataTextField="Narration"
                                                HeaderText="Narration" SortExpression="Narration"></asp:ButtonField>
                                            <asp:TemplateField HeaderText="Penalty" ControlStyle-Width="80px" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtPenalty" CssClass="NormalTextBold" TabIndex="9"
                                                        Enabled="false" Style="text-align: right" onchange="txtchanged(this);" Text='<%#Eval("Penalty") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterPenalty" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtPenalty" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Late POD Chrg" ControlStyle-Width="80px" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtLatePODCharges" CssClass="NormalTextBold" TabIndex="9"
                                                        Style="text-align: right" onchange="txtchanged(this);" Text='<%#Eval("LatePODCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterLatePODCharges" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtLatePODCharges" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Charges" ControlStyle-Width="80px" ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtTotalCharges" CssClass="NormalTextBold" TabIndex="9"
                                                        Style="text-align: right" Enabled="false" Text='<%#Eval("TotalCharges") %>' />
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterTotal" runat="server" FilterType="Numbers,Custom"
                                                        ValidChars="." TargetControlID="txtTotalCharges" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                                <table class="control_set" style="width: 100%">
                                    <tr class="control_row">
                                        <td>
                                        </td>
                                        <td style="width: 150px">
                                            Total Invoice Amount
                                        </td>
                                        <td style="width: 85px">
                                            <asp:TextBox ID="txtBasicAmount" runat="server" Width="80px" Style="text-align: right"
                                                TabIndex="15" CssClass="NormalTextBold" OnTextChanged="txtBasicAmount_TextChanged"
                                                Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:Label ID="Label1" runat="server" Text="Payment Terms"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPaymentTerms" runat="server" Width="400px" CssClass="NormalTextBold"
                                                TabIndex="16" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="Particulars"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtParticulars" runat="server" Width="500px" CssClass="NormalTextBold"
                                                TabIndex="17" MaxLength="200"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkApprove" runat="server" Text="Approve" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="18" ID="btnSavePacklist" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="19" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" CssClass="NormalTextBold"
                                            ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" CssClass="NormalTextBold"
                                            ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="6" Width="350px" CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="7" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td width="80px">
                                            <asp:Label ID="Label8" Text="Generate From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtFromDateGen" runat="server" Width="100px" TabIndex="8" CssClass="NormalTextBold"
                                                ContentEditable="False"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                TargetControlID="txtFromDateGen" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton3"
                                                FirstDayOfWeek="Sunday">
                                            </asp:CalendarExtender>
                                            <asp:ImageButton ID="ImageButton3" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="9" Width="23px" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtFromDateGen" SetFocusOnError="True" ValidationGroup="GenerateInv" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </td>
                                        <td width="80px">
                                            <asp:Label ID="Label10" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtToDateGen" runat="server" Width="100px" TabIndex="10" CssClass="NormalTextBold"
                                                ContentEditable="False"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                TargetControlID="txtToDateGen" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton4"
                                                FirstDayOfWeek="Sunday">
                                            </asp:CalendarExtender>
                                            <asp:ImageButton ID="ImageButton4" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="11" Width="23px" />
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtToDateGen" SetFocusOnError="True" ValidationGroup="GenerateInv" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            <asp:Button TabIndex="12" ID="btnGenerate" runat="server" Text="Generate Invoice"
                                                CssClass="button" OnClick="btnGenerate_Click" ValidationGroup="GenerateInv"/>
                                            <asp:Button TabIndex="13" ID="btnAddPacklist" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" Visible="false" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="14" ID="btnDeletePacklist" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" Visible="false" />&nbsp;
                                            &nbsp;
                                            <asp:Button TabIndex="15" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="6">
                                            <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="Srl,InvoiceNo,InvoiceDate,Name,InvoiceAmount,InvRefNo,ReceivedAmount,AdvancePaid,BillingCompany_Srl,Customer_Srl,BillingCompany,CName,CAddress1,CAddress2,CAddress3,Address1,Address2,Address3,PAN,Approved,ApprovedBy,ApprovedOn"
                                                OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                                OnRowCommand="grdPacklistView_RowCommand" AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                                <Columns>
                                                    <%--<asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>--%>
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="InvRefNo"
                                                        HeaderText="Invoice No." SortExpression="InvRefNo">
                                                        <ItemStyle Width="100px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Supplier" SortExpression="Name"></asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="100px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                    </asp:ButtonField>
                                                    <%--<asp:ButtonField CommandName="PrintDoc" HeaderText="" Text="Document" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:ButtonField>--%>
                                                    <asp:ButtonField CommandName="ItemAnn" HeaderText="" Text="Document" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:ButtonField>
                                                    <asp:TemplateField HeaderText="Approved" ItemStyle-Width="60px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkApproved" Checked='<%#bool.Parse(Eval("Approved").ToString()=="0"?"false":"true") %>'
                                                                Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="LRNo,LRDate,TransportationCharges,DetentionCharges,WaraiCharges,TwoPointCharges,OtherCharges,Penalty,Narration,LatePODCharges,TotalCharges"
                                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True"
                                                        OnRowDataBound="grdViewItemDetls_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="100px"
                                                                DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TransportationCharges" HeaderText="Basic" SortExpression="TransportationCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DetentionCharges" HeaderText="Detention" SortExpression="DetentionCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="WaraiCharges" HeaderText="Warrai" SortExpression="WaraiCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TwoPointCharges" HeaderText="Two Points" SortExpression="TwoPointCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="OtherCharges" HeaderText="Other" SortExpression="OtherCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Narration" HeaderText="Narration" SortExpression="Narration">
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Penalty" HeaderText="Penalty" SortExpression="Penalty">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LatePODCharges" HeaderText="Late POD" SortExpression="LatePODCharges">
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TotalCharges" HeaderText="Total Charges" SortExpression="TotalCharges">
                                                                <ItemStyle Width="150px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddPacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDeletePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSavePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbCustomer" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }


        function checkboxclick(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_chkLR'));
            var rowindex = Rowid.substring(4, Rowid.length);
            //            var txtFrieght = cntrlId.substring(0, cntrlId.indexOf('chkLR')) + 'txtFrieghts';
            //            var texbox = document.getElementById(txtFrieght);
            document.getElementById("<%= txtRowNumber.ClientID %>").value = rowindex;
            var gv = document.getElementById("<%= grdPendingLRs.ClientID %>");
            var tb = gv.getElementsByTagName("input");
            var frt = 0;
            for (var i = 0; i < tb.length; i++) {

                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtFrieght') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    frt = tb[i].value;
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtDetention') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                    tb[i].focus();
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtWarai') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTwoPoint') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtOtherCharges') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtPenalty') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtRemarks') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtLatePODCharges') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    if (!control.checked) {
                        tb[i].value = '';
                    }
                }

                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtTotalCharges') >= 0) {
                    if (control.checked) {
                        tb[i].value = frt;
                    }
                    if (!control.checked) {
                        var TotalAmt = document.getElementById("<%= txtBasicAmount.ClientID %>").value;
                        document.getElementById("<%= txtBasicAmount.ClientID %>").value = (parseFloat(TotalAmt) - parseFloat(tb[i].value)).toFixed(2);
                        tb[i].value = '';
                    }
                }
            }
            if (!control.checked) {
                document.getElementById("<%= txtRowNumber.ClientID %>").value = '';
            }
            calculate();
        }
        function txtchanged(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_txt'));
            var rowindex = Rowid.substring(4, Rowid.length);
            document.getElementById("<%= txtRowNumber.ClientID %>").value = rowindex;
            calculate();
        }
        function calculate() {
            //document.getElementById("<%= txtParticulars.ClientID %>").value = document.getElementById("<%= txtRowNumber.ClientID %>").value;
            if (document.getElementById("<%= txtRowNumber.ClientID %>").value.toString() != "") {
                var gv = document.getElementById("<%= grdPendingLRs.ClientID %>");
                var tb = gv.getElementsByTagName("input");

                var Total = 0;
                var TotalAmt = 0;
                var newId = "";
                newId = "ctl" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                if (parseInt(document.getElementById("<%= txtRowNumber.ClientID %>").value) < 10) {
                //                    newId = "ctl0" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                }
                //                else {
                //                    newId = "ctl" + document.getElementById("<%= txtRowNumber.ClientID %>").value;
                //                }
                //document.getElementById("<%= txtPaymentTerms.ClientID %>").value = newId;

                for (var i = 0; i < tb.length; i++) {

                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtFrieght') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtDetention') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtWarai') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtTwoPoint') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtOtherCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) + parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtPenalty') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) - parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtLatePODCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            Total = parseFloat(Total) - parseFloat(tb[i].value);
                        }
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf(newId + '_txtTotalCharges') >= 0) {
                        tb[i].value = parseFloat(Total).toFixed(2);
                    }
                    if (tb[i].type == "text" && tb[i].id.indexOf('txtTotalCharges') >= 0) {
                        if (parseFloat(tb[i].value) > 0) {
                            TotalAmt = parseFloat(TotalAmt) + parseFloat(tb[i].value);
                        }
                    }
                }
                document.getElementById("<%= txtBasicAmount.ClientID %>").value = parseFloat(TotalAmt).toFixed(2);
            }
        }    
    </script>
</asp:Content>
