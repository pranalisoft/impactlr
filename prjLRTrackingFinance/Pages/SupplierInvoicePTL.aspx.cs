﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Globalization;
using OfficeOpenXml;
using System.Configuration;
using OfficeOpenXml.Style;
using System.IO;
using Spire.Xls;
using CrystalDecisions.CrystalReports.Engine;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierInvoicePTL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void LoadGridView()
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.IsPTL = 1;
            DataTable dt = optBO.FillInvoiceGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;

            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            SuplInvoiceBO CustBO = new SuplInvoiceBO();
            CustBO.BranchID = long.Parse(Session["BranchID"].ToString());
            DataTable dtCust = CustBO.FillPendingCustomers();
            cmbCustomer.DataSource = dtCust;
            cmbCustomer.DataTextField = "SupplierName";
            cmbCustomer.DataValueField = "SupplierID";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        private long Delete()
        {
            SuplInvoiceBO OptBO = new SuplInvoiceBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdPacklistView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelectPacklist");
                    long id = long.Parse(grdPacklistView.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }
            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteInvoices();
            }
            return srl;
        }

        void MsgPopUp_modalPopupCommand(object sender, CommandEventArgs e)
        {
            CommonTypes.ModalPopupCommand command = CommonTypes.StringToEnum<CommonTypes.ModalPopupCommand>(e.CommandName);

            switch (command)
            {
                case CommonTypes.ModalPopupCommand.Ok:

                    break;
                case CommonTypes.ModalPopupCommand.Yes:
                    if (Delete() > 0)
                    {
                        LoadGridView();
                        MsgPanel.Message = "Record(s) deleted successfully.";
                        MsgPanel.DispCode = 1;
                        txtFromDate.Focus();
                    }
                    break;
                case CommonTypes.ModalPopupCommand.No:
                    LoadGridView();
                    txtFromDate.Focus();
                    break;
                default:
                    break;
            }
        }

        protected void txtBasicAmount_TextChanged(object sender, EventArgs e)
        {

        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            SuplInvoiceBO optbo = new SuplInvoiceBO();
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
            optbo.IsPTL = 1;
            DataTable dt = optbo.FillPendingLRS();
            grdPendingLRs.DataSource = dt;
            grdPendingLRs.DataBind();

            DisableGridAmounts(-1);
            txtBRefNo.Focus();
        }

        private void DisableGridAmounts(int rowindex)
        {
            if (rowindex == -1)
            {
                for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
                {
                    GridViewRow grdrow = grdPendingLRs.Rows[i];
                    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
                    TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
                    TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
                    TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
                    TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
                    TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
                    TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
                    TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

                    txtFreight.Enabled = false;
                    txtDetention.Enabled = false;
                    txtDetention.Text = string.Empty;
                    txtWarai.Enabled = false;
                    txtWarai.Text = string.Empty;
                    txtOtherCharges.Enabled = false;
                    txtOtherCharges.Text = string.Empty;
                    txtTwoPoint.Enabled = false;
                    txtTwoPoint.Text = string.Empty;
                    txtTotalCharges.Enabled = false;
                    txtTotalCharges.Text = string.Empty;
                    txtPenalty.Enabled = false;
                    txtPenalty.Text = string.Empty;
                    txtLatePODCharges.Enabled = false;
                    txtLatePODCharges.Text = string.Empty;
                    CalculateTotalAmount();
                }
            }
            else
            {
                GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
                TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
                TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
                TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
                TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
                TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
                TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
                TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
                TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

                txtFreight.Enabled = false;
                txtDetention.Enabled = false;
                txtDetention.Text = string.Empty;
                txtWarai.Enabled = false;
                txtWarai.Text = string.Empty;
                txtOtherCharges.Enabled = false;
                txtOtherCharges.Text = string.Empty;
                txtTwoPoint.Enabled = false;
                txtTwoPoint.Text = string.Empty;
                txtTotalCharges.Enabled = false;
                txtTotalCharges.Text = string.Empty;
                txtPenalty.Enabled = false;
                txtPenalty.Text = string.Empty;
                txtLatePODCharges.Enabled = false;
                txtLatePODCharges.Text = string.Empty;

                DataKey keys = grdPendingLRs.DataKeys[rowindex];
                txtFreight.Text = keys["TotalFrieght"].ToString();

                CalculateTotalAmount();
            }
        }

        private void EnableGridAmounts(int rowindex)
        {
            if (rowindex == -1)
            {
                for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
                {
                    GridViewRow grdrow = grdPendingLRs.Rows[i];
                    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
                    TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
                    TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
                    TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
                    TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
                    TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
                    TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
                    TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

                    txtFreight.Enabled = true;
                    txtDetention.Enabled = true;
                    txtWarai.Enabled = true;
                    txtOtherCharges.Enabled = true;
                    txtTwoPoint.Enabled = true;
                    txtPenalty.Enabled = false;
                    txtLatePODCharges.Enabled = true;

                    txtTotalCharges.Text = txtFreight.Text;
                    CalculateTotalAmount();
                    txtFreight.Focus();
                }
            }
            else
            {
                GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
                TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
                TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
                TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
                TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
                TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
                TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
                TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
                TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

                txtFreight.Enabled = true;
                txtDetention.Enabled = true;
                txtWarai.Enabled = true;
                txtOtherCharges.Enabled = true;
                txtTwoPoint.Enabled = true;
                txtTotalCharges.Enabled = false;
                txtPenalty.Enabled = false;
                txtLatePODCharges.Enabled = true;

                txtTotalCharges.Text = txtFreight.Text;
                CalculateTotalAmount();
                txtFreight.Focus();
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            SuplInvoiceBO optbo = new SuplInvoiceBO();
            NumberToWords wordBO = new NumberToWords();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillDropDowns();
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    int cntDel = 0;
                    foreach (GridViewRow row in grdPacklistView.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelectPacklist");
                            if (rowcheck.Checked)
                            {
                                cntDel = cntDel + 1;
                                break;
                            }
                        }
                    }
                    if (cntDel > 0)
                    {
                        MsgPopUp.ShowModal("Are you sure.<br/>Do you want to delete selected bills?", CommonTypes.ModalTypes.Confirm);
                    }
                    else
                    {
                        MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                    }
                    PnlItemDtls.Visible = false;
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Srl = long.Parse(txtHiddenId.Value);
                    optbo.InvoiceDate = txtDate.Text;
                    optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.BasicAmount = decimal.Parse(txtBasicAmount.Text);
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.EnteredBy = long.Parse(Session["EID"].ToString());
                    optbo.ItemXML = GetItemXML();
                    optbo.PaymentTerms = txtPaymentTerms.Text.Trim();
                    optbo.Particulars = txtParticulars.Text.Trim();
                    optbo.AmountInWords = wordBO.Num2WordConverter(txtBasicAmount.Text).ToString();
                    optbo.InvoiceNo = txtBRefNo.Text.Trim();
                    if (chkApprove.Checked)
                        optbo.ApprovedBy = long.Parse(Session["EID"].ToString());
                    else
                        optbo.ApprovedBy = 0;

                    string strItem = "";
                    DataTable dt = CommonTypes.GetDataTable("dtPenalty");
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(dt);
                            strItem = ds.GetXml();
                            strItem = strItem.Replace("'", "");
                            ds.Tables.Remove(dt);
                        }
                    }
                    optbo.PenaltyXML = strItem;
                    if (optbo.ItemXML == string.Empty)
                    {
                        MsgPopUp.ShowModal("Please select atleast one record.", CommonTypes.ModalTypes.Error);
                        return;
                    }
                    long cnt = optbo.UpdatetInvoice();
                    //long cntDebit = 0;
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Supplier Invoice Created Successfully", true, 1);
                            //for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
                            //{
                            //    CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                            //    TextBox txtPenalty = (TextBox)grdPendingLRs.Rows[i].FindControl("txtPenalty");
                            //    TextBox txtRemarks = (TextBox)grdPendingLRs.Rows[i].FindControl("txtRemarks");

                            //    if (txtPenalty.Text == string.Empty) txtPenalty.Text = "0";
                            //    if (txtRemarks.Text == string.Empty) txtRemarks.Text = "";

                            //    DataKey key = grdPendingLRs.DataKeys[i];
                            //    if (chk.Checked)
                            //    {
                            //        string strxml = String.Empty;
                            //        strxml = strxml + "<dtPayment><InvoiceNo>" + cnt + "</InvoiceNo><LRID>" + key["ID"].ToString() + "</LRID><AdjAmt>" + txtPenalty.Text.Trim() + "</AdjAmt><TDSAmt>0</TDSAmt><TDSper>0</TDSper></dtPayment>";
                            //        strxml = "<DocumentElement>" + strxml + "</DocumentElement>";
                            //        SupplierPaymentBO custbo = new SupplierPaymentBO()
                            //        {
                            //            tranDate = txtDate.Text.Trim(),
                            //            SupplierID = long.Parse(cmbCustomer.SelectedValue),
                            //            refNo = String.Empty,
                            //            remarks = String.Empty,
                            //            totalAmount = decimal.Parse(txtPenalty.Text),
                            //            userID = long.Parse(Session["EID"].ToString()),
                            //            paymentXML = strxml,
                            //            BankName = String.Empty,
                            //            PaymentMode = "1",
                            //            ChqNo = String.Empty,
                            //            ChqDate = txtDate.Text.Trim(),
                            //            CardNetBankingDetails = String.Empty,
                            //            PayType = "D",
                            //            BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString())
                            //        };
                            //        cntDebit += custbo.InsertPaymentDetails();
                            //    }
                            //}
                        }
                        else
                        {
                            SetPanelMsg("Supplier Invoice Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private string GetItemXML()
        {
            string strxml = String.Empty;
            string LRID = String.Empty;

            for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                TextBox txtFreight = (TextBox)grdPendingLRs.Rows[i].FindControl("txtFrieght");
                TextBox txtDetention = (TextBox)grdPendingLRs.Rows[i].FindControl("txtDetention");
                TextBox txtWarai = (TextBox)grdPendingLRs.Rows[i].FindControl("txtWarai");
                TextBox txtTwoPoint = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTwoPoint");
                TextBox txtOtherCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtOtherCharges");
                TextBox txtTotalCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTotalCharges");
                TextBox txtPenalty = (TextBox)grdPendingLRs.Rows[i].FindControl("txtPenalty");
                TextBox txtLatePODCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtLatePODCharges");

                if (txtDetention.Text == string.Empty) txtDetention.Text = "0";
                if (txtWarai.Text == string.Empty) txtWarai.Text = "0";
                if (txtTwoPoint.Text == string.Empty) txtTwoPoint.Text = "0";
                if (txtOtherCharges.Text == string.Empty) txtOtherCharges.Text = "0";
                if (txtTotalCharges.Text == string.Empty) txtTotalCharges.Text = "0";
                if (txtPenalty.Text == string.Empty) txtPenalty.Text = "0";
                if (txtLatePODCharges.Text == string.Empty) txtLatePODCharges.Text = "0";

                DataKey key = grdPendingLRs.DataKeys[i];
                if (chk.Checked)
                {
                    LRID = key["LRId"].ToString();
                    strxml = strxml + "<dtItem><LRId>" + LRID + "</LRId><TransportationCharges>" + txtFreight.Text.Trim() + "</TransportationCharges><DetentionCharges>" + txtDetention.Text.Trim() + "</DetentionCharges><WaraiCharges>" + txtWarai.Text.Trim() + "</WaraiCharges><TwoPointCharges>" + txtTwoPoint.Text.Trim() + "</TwoPointCharges><OtherCharges>" + txtOtherCharges.Text + "</OtherCharges><TotalCharges>" + txtTotalCharges.Text + "</TotalCharges><AdvanceToSupplier>" + key["AdvanceToSupplier"].ToString() + "</AdvanceToSupplier><Penalty>" + txtPenalty.Text + "</Penalty><LatePODCharges>" + txtLatePODCharges.Text + "</LatePODCharges></dtItem>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<NewDataSet>" + strxml + "</NewDataSet>";
            }
            return strxml;
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetPanelMsg("", false, 1);
            LoadGridView();
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            SuplInvoiceBO optbo = new SuplInvoiceBO();
            optbo.EnteredBy = long.Parse(Session["EID"].ToString());
            optbo.FromDate = txtFromDateGen.Text;
            optbo.ToDate = txtToDateGen.Text;
            long cnt = optbo.InsertInvoice_Bulk();
            if (cnt > 0)
            {
                SetPanelMsg(cnt.ToString() + " Supplier Invoice Created Successfully", true, 1);
                LoadGridView();
                txtSearch.Focus();
            }
        }

        protected void txtFrieght_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow grdrow = (GridViewRow)txt.Parent.Parent;
            int rowIndex = 0;
            rowIndex = grdrow.RowIndex;
            CalculateRowAmount(rowIndex);

            TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
            TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
            TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
            TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
            TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
            TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
            TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
            TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

            switch (txt.ID)
            {
                case "txtFrieght":
                    txtDetention.Focus();
                    break;
                case "txtDetention":
                    txtWarai.Focus();
                    break;
                case "txtWarai":
                    txtTwoPoint.Focus();
                    break;
                case "txtTwoPoint":
                    txtOtherCharges.Focus();
                    break;
                case "txtOtherCharges":
                    txtLatePODCharges.Focus();
                    break;
                case "txtLatePODCharges":
                    if (rowIndex < grdPendingLRs.Rows.Count - 1)
                    {
                        CheckBox chkLR = (CheckBox)grdPendingLRs.Rows[rowIndex + 1].FindControl("chkLR");
                        chkLR.Focus();
                    }
                    else
                    {
                        txtPaymentTerms.Focus();
                    }
                    break;
            }
        }

        private void CalculateRowAmount(int rowIndex)
        {
            GridViewRow grdrow = grdPendingLRs.Rows[rowIndex];
            TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
            TextBox txtDetention = (TextBox)grdrow.FindControl("txtDetention");
            TextBox txtWarai = (TextBox)grdrow.FindControl("txtWarai");
            TextBox txtTwoPoint = (TextBox)grdrow.FindControl("txtTwoPoint");
            TextBox txtOtherCharges = (TextBox)grdrow.FindControl("txtOtherCharges");
            TextBox txtTotalCharges = (TextBox)grdrow.FindControl("txtTotalCharges");
            TextBox txtTotalAmount = (TextBox)grdrow.FindControl("txtTotalAmount");
            TextBox txtPenalty = (TextBox)grdrow.FindControl("txtPenalty");
            TextBox txtLatePODCharges = (TextBox)grdrow.FindControl("txtLatePODCharges");

            decimal freightCharges = txtFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtFreight.Text.Trim());
            decimal DetentionCharges = txtDetention.Text.Trim() == "" ? 0 : decimal.Parse(txtDetention.Text.Trim());
            decimal WaraiCharges = txtWarai.Text.Trim() == "" ? 0 : decimal.Parse(txtWarai.Text.Trim());
            decimal twoPointCharges = txtTwoPoint.Text.Trim() == "" ? 0 : decimal.Parse(txtTwoPoint.Text.Trim());
            decimal OtherCharges = txtOtherCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtOtherCharges.Text.Trim());
            decimal PenaltyCharges = txtPenalty.Text.Trim() == "" ? 0 : decimal.Parse(txtPenalty.Text.Trim());
            decimal LatePODCharges = txtLatePODCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtLatePODCharges.Text.Trim());
            decimal TotalCharges = freightCharges + DetentionCharges + WaraiCharges + twoPointCharges + OtherCharges - PenaltyCharges - LatePODCharges;
            txtTotalCharges.Text = TotalCharges.ToString("F2");

            CalculateTotalAmount();
        }

        private void CalculateTotalAmount()
        {
            decimal Total = 0;
            for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                if (chk.Checked)
                {
                    TextBox TotalCharges = (TextBox)grdPendingLRs.Rows[i].FindControl("txtTotalCharges");
                    if (TotalCharges.Text.Trim() == "") TotalCharges.Text = "0";
                    Total += TotalCharges.Text == "" ? 0 : decimal.Parse(TotalCharges.Text.Trim());
                }
            }
            txtBasicAmount.Text = Total.ToString("F2");
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    popDelPacklist.Hide();
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                case "ok":
                    DataTable dt = null;
                    dt = CommonTypes.GetDataTable("dtPenaltyTemp");

                    DataTable dtMain = null;
                    if (CommonTypes.GetDataTable("dtPenalty") == null)
                    {
                        dtMain = CommonTypes.CreateDataTable("LRID,ReasonSrl,Reason,Charges", "dtPenalty", "long,long,string,decimal");
                    }
                    else
                    {
                        dtMain = CommonTypes.GetDataTable("dtPenalty");
                    }

                    int RInd = int.Parse(HFRowId.Value);
                    if (dt != null)
                    {
                        TextBox txtPenalty = (TextBox)grdPendingLRs.Rows[RInd].FindControl("txtPenalty");
                        LinkButton btnlink = (LinkButton)grdPendingLRs.Rows[RInd].Cells[13].Controls[0];
                        if (dt.Rows.Count == 0)
                        {

                            txtPenalty.Text = "0";
                            btnlink.Text = "Add Penalty";
                        }
                        else
                        {
                            txtPenalty.Text = decimal.Parse(dt.Compute("sum(Charges)", "").ToString()).ToString().Replace(".00", "");
                            string Reasons = "Add Penalty";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (i == 0)
                                    Reasons = dt.Rows[i]["Reason"].ToString();
                                else
                                    Reasons = Reasons + "," + dt.Rows[i]["Reason"].ToString();
                            }
                            btnlink.Text = Reasons;
                        }
                        foreach (DataRow dr in dtMain.Rows)
                        {
                            if (dr["LRID"].ToString() == HFLRId.Value)
                                dr.Delete();
                        }
                        dtMain.AcceptChanges();

                        foreach (DataRow item in dt.Rows)
                        {
                            dtMain.Rows.Add();
                            int Rindex = dtMain.Rows.Count - 1;
                            dtMain.Rows[Rindex]["LRID"] = item["LRID"];
                            dtMain.Rows[Rindex]["ReasonSrl"] = item["ReasonSrl"];
                            dtMain.Rows[Rindex]["Reason"] = item["Reason"];
                            dtMain.Rows[Rindex]["Charges"] = item["Charges"];
                        }
                        dtMain.AcceptChanges();
                        CommonTypes.Insertdata(dtMain);
                        CalculateRowAmount(RInd);
                        CalculateTotalAmount();
                        Session.Remove("dtPenaltyTemp");
                        TextBox txtLatePODCharges = (TextBox)grdPendingLRs.Rows[RInd].FindControl("txtLatePODCharges");
                        txtLatePODCharges.Focus();
                    }
                    popPenalty.Hide();
                    break;
                case "cancel":
                    Session.Remove("dtPenaltyTemp");
                    popPenalty.Hide();
                    break;
                default:
                    break;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void ClearView()
        {
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtParticulars.Text = string.Empty;
            txtPaymentTerms.Text = string.Empty;
            txtBasicAmount.Text = string.Empty;
            cmbBillingCompany.SelectedIndex = 0;
            cmbCustomer.SelectedIndex = 0;
            SetPanelMsg("", false, 0);
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];
            if (e.CommandName == "Modify")
            {
                if (keys["Approved"].ToString() == "1")
                {
                    MsgPopUp.ShowModal("invoice is approved, so cannot edit this invoice.", CommonTypes.ModalTypes.Error);
                    return;
                }
                decimal rcvdAmt = decimal.Parse(keys["ReceivedAmount"].ToString());
                decimal AdvanceAmt = 0;
                if (keys["AdvancePaid"] != null && keys["AdvancePaid"].ToString() != "")
                {
                    AdvanceAmt = decimal.Parse(keys["AdvancePaid"].ToString());
                }

                if (rcvdAmt > AdvanceAmt)
                {
                    MsgPopUp.ShowModal("Payment is done against this invoice so cannot edit this invoice.", CommonTypes.ModalTypes.Error);
                    return;
                }
                FillDropDowns();
                txtHiddenId.Value = keys["Srl"].ToString();
                txtDate.Text = keys["InvoiceDate"].ToString().Replace("00:00:00", "");
                cmbBillingCompany.SelectedValue = keys["BillingCompany_Srl"].ToString();
                cmbCustomer.SelectedValue = keys["Customer_Srl"].ToString();
                txtBRefNo.Text = keys["InvRefNo"].ToString();
                txtBRefNo.Enabled = false;
                txtBdate.Text = keys["InvoiceDate"].ToString().Replace("00:00:00", "");
                txtBasicAmount.Text = keys["InvoiceAmount"].ToString().Replace(".00", "");
                ShowViewByIndex(0);
                cmbCustomer.Enabled = false;
                cmbBillingCompany.Enabled = false;
                LoadGridViewDetailsEdit(long.Parse(keys["Srl"].ToString()));
                LoadGridViewPenalty(long.Parse(keys["Srl"].ToString()));
                cmbBillingCompany.Focus();
            }
            else if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For Invoice No. " + keys["InvRefNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["Srl"].ToString()));
            }
            else if (e.CommandName == "ViewReport")
            {
                {
                    Session["InvoiceID"] = keys["Srl"].ToString();
                    Session["ReportType"] = "InvDoc";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
            else if (e.CommandName == "ItemAnn")
            {
                Session["ReportType"] = "SuplInvDoc";
                ExportToExcel(long.Parse(keys["Srl"].ToString()), keys["InvoiceDate"].ToString(), keys["Name"].ToString(), keys["BillingCompany"].ToString(), keys["InvRefNo"].ToString(), keys["Address1"].ToString(), keys["Address2"].ToString(), keys["Address3"].ToString(), keys["CName"].ToString(), keys["CAddress1"].ToString(), keys["CAddress2"].ToString(), keys["CAddress3"].ToString(), keys["PAN"].ToString());

            }
            else if (e.CommandName == "PrintDoc")
            {
                ReportDocument RptDoc = new ReportDocument();
                SuplInvoiceBO OptBO = new SuplInvoiceBO();
                prjLRTrackerFinanceAuto.Datasets.DsSuplMainInvoice ds = new prjLRTrackerFinanceAuto.Datasets.DsSuplMainInvoice();
                OptBO.Srl = long.Parse(keys["Srl"].ToString());
                DataTable dt = OptBO.GetSuplMainInvoiceDoc();
                string fileName = Server.MapPath("~\\Downloads\\" + keys["Srl"].ToString() + ".pdf");
                ds.Tables.RemoveAt(0);
                ds.Tables.Add(dt);
                RptDoc.Load(Server.MapPath("~/Reports/SupplierInvoice.rpt"));
                //condbsLogon(RptDoc);
                RptDoc.SetDataSource(ds);

                RptDoc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fileName);
                //return fileName;
                Session["FilePath"] = fileName;
                RptDoc.Close();
                RptDoc.Dispose();
                GC.Collect();
                //Response.Redirect("ViewFile.aspx", false);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Pages/ViewFile.aspx','_blank')", true);
            }
        }

        private void ExportToExcel(long InvoiceSrl, string InvoiceDate, string Customer, string BillingCompany, string InvoiceNo, string Address1, string Address2, string Address3, string CName, string CAddress1, string CAddress2, string CAddress3, string PAN)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                int RowsToAdd = 13;
                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SuplInvoice_" + InvoiceSrl.ToString() + ".xlsx");
                string filenamepdf = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\SuplInvoice_" + InvoiceSrl.ToString() + ".pdf");
                SuplInvoiceBO OptBo = new SuplInvoiceBO();
                OptBo.Srl = InvoiceSrl;
                DataSet ds = OptBo.InvoicePrint();
                DataTable dt = ds.Tables[0];
                DataTable dtSign = ds.Tables[1];

                int BalColindex = 0;
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ColumnName == "Balance")
                    {
                        BalColindex = i;
                        break;
                    }
                }
                if (BalColindex == dt.Columns.Count - 1)
                {
                    dt.Columns[16].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                    dt.Columns[5].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                }
                else
                {
                    //int penaltycols = dt.Columns.Count - 1 - BalColindex;
                    dt.Columns[17].SetOrdinal(dt.Columns.Count - 1);
                    dt.AcceptChanges();
                    dt.Columns[16].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                    dt.Columns[5].SetOrdinal(dt.Columns.Count - 2);
                    dt.AcceptChanges();
                }
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(1);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(5);
                //dt.AcceptChanges();
                //dt.Columns.RemoveAt(10);
                //dt.AcceptChanges();              

                //dt.Columns.Add("Total", typeof(decimal));
                //decimal total = 0;
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    total = 0;
                //    for (int j = 5; j < dt.Columns.Count - 1; j++)
                //    {
                //        if (dt.Rows[i][j] == null || dt.Rows[i][j].ToString().Trim() == "") dt.Rows[i][j] = "0";
                //        total = total + decimal.Parse(dt.Rows[i][j].ToString());
                //    }
                //    dt.Rows[i]["Total"] = (total).ToString("F0");
                //}
                //dt.AcceptChanges();

                decimal total = 0;
                dt.Rows.Add();
                int Rcnt = dt.Rows.Count - 1;
                dt.Rows[Rcnt][0] = "Total";

                for (int i = 9; i < dt.Columns.Count; i++)
                {
                    total = 0;
                    for (int j = 0; j < dt.Rows.Count - 1; j++)
                    {
                        if (dt.Rows[j][i] == null || dt.Rows[j][i].ToString().Trim() == "") dt.Rows[j][i] = "0";
                        total = total + decimal.Parse(dt.Rows[j][i].ToString());
                    }
                    dt.Rows[Rcnt][i] = total.ToString("F0");
                }
                dt.AcceptChanges();

                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(InvoiceSrl.ToString());
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                objWorksheet.Cells["A1"].LoadFromDataTable(dt, true);
                objWorksheet.Cells["B:B"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["F:F"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["G:G"].Style.Numberformat.Format = "dd/MM/yyyy";
                objWorksheet.Cells["H:H"].Style.Numberformat.Format = "dd/MM/yyyy";
                //objWorksheet.Cells["A1:Z1"].AutoFilter = true;                 

                objWorksheet.InsertRow(1, RowsToAdd);
                int RowCnt = dt.Rows.Count + RowsToAdd;
                string Letter = ReturnLetter(dt.Columns.Count - 1);
                string LetterNoAndDate1 = ReturnLetter(dt.Columns.Count - 5);
                int AccSignRow = dt.Columns.Count - 5;
                string LetterNoAndDate2 = ReturnLetter(dt.Columns.Count - 4);
                string LetterNoAndDate3 = ReturnLetter(dt.Columns.Count - 3);

                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.Column(1).Width = 25;
                objWorksheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                objWorksheet.Column(1).Style.WrapText = true;
                objWorksheet.Column(5).Width = 12;

                for (int i = 2; i <= dt.Columns.Count; i++)
                {
                    objWorksheet.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                }

                //for (int i = RowsToAdd+1; i <= RowCnt; i++)
                //{
                //    objWorksheet.Row(i).Height = 18;
                //}
                objWorksheet.Row(RowsToAdd + 1).Height = 20;
                objWorksheet.Row(RowCnt + 1).Height = 21;


                //objWorksheet.Cells["A2"].Value = Customer;
                using (ExcelRange objRange = objWorksheet.Cells["A1:" + Letter + "1"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 20;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //objRange.Value = "Annexure For Invoice No. " + InvoiceNo;
                    objRange.Value = "Settlement Note";
                }
                objWorksheet.Row(1).Height = 30;
                using (ExcelRange objRange = objWorksheet.Cells["A1:" + Letter + "1"])
                {
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A2:" + Letter + "2"])
                {
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                }
                objWorksheet.Cells["A:A"].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                objWorksheet.Cells[Letter + ":" + Letter].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                objWorksheet.Row(3).Height = 20;
                objWorksheet.Row(4).Height = 20;
                objWorksheet.Row(5).Height = 20;
                objWorksheet.Row(6).Height = 20;
                objWorksheet.Row(7).Height = 20;
                objWorksheet.Row(8).Height = 20;
                objWorksheet.Row(9).Height = 20;
                objWorksheet.Row(10).Height = 20;

                using (ExcelRange objRange = objWorksheet.Cells["A3:C3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Customer;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A4:C4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address1;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A5:C5"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address2;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A6:C6"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = Address3;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A7:C7"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "PAN : " + PAN;
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "3:" + LetterNoAndDate2 + "3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Invoice No. ";
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate3 + "3:" + Letter + "3"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = InvoiceNo;
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "4:" + LetterNoAndDate2 + "4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Invoice Date ";
                }
                using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate3 + "4:" + Letter + "4"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = InvoiceDate.Replace("00:00:00", "");
                    objRange.Style.Numberformat.Format = "dd/MM/yyyy";
                }

                using (ExcelRange objRange = objWorksheet.Cells["A7:" + Letter + "7"])
                {
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                }

                using (ExcelRange objRange = objWorksheet.Cells["A8:C8"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = "Buyer";
                }
                using (ExcelRange objRange = objWorksheet.Cells["A9:C9"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CName;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A10:C10"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress1;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A11:C11"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress2;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A12:C12"])
                {
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Value = CAddress3;
                }

                //using (ExcelRange objRange = objWorksheet.Cells["A3:" + Letter + "3"])
                //{
                //    //objRange.Merge = true;
                //    objRange.Style.Font.Bold = true;
                //    objRange.Style.Font.Size = 14;
                //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //    objRange.Value = Customer;
                //}
                //using (ExcelRange objRange = objWorksheet.Cells["A4:" + Letter + "4"])
                //{
                //    //objRange.Merge = true;
                //    //objRange.Style.Font.Bold = true;
                //    objRange.Style.Font.Size = 13;
                //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                //    objRange.Value = Address1;
                //}


                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowsToAdd + 1).ToString() + ":" + Letter + (RowsToAdd + 1).ToString()])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;

                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowsToAdd + 2).ToString() + ":" + Letter + (RowCnt + 1).ToString()])
                {
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowCnt + 1).ToString() + ":" + Letter + (RowCnt + 1).ToString()])
                {
                    objRange.Style.Font.Size = 12;
                    objRange.Style.Font.Bold = true;
                    objRange.Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                }
                using (ExcelRange objRange = objWorksheet.Cells["A" + (RowCnt + 2).ToString() + ":" + Letter + (RowCnt + 2).ToString()])
                {
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    objRange.Merge = true;
                    objRange.Style.Font.Size = 12;
                    objRange.Style.Font.Bold = true;
                    objRange.Value = "GST Not Applicable.";
                }
                int rcntSign = 36;
                if (RowCnt + 2 >= 36)
                {
                    rcntSign = RowCnt + 2;
                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 1).ToString() + ":" + Letter + (rcntSign + 1).ToString()])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "For " + Customer;
                    //}

                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 5).ToString() + ":" + Letter + (rcntSign + 5).ToString()])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "Authorised Signatory";
                    //}

                    using (ExcelRange objRange = objWorksheet.Cells["B" + (rcntSign + 1).ToString() + ":C" + (rcntSign + 1).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["B" + (rcntSign + 5).ToString() + ":C" + (rcntSign + 5).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Operation Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 1).ToString() + ":" + Letter + (rcntSign + 1).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + (rcntSign + 5).ToString() + ":" + Letter + (rcntSign + 5).ToString()])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Accounts Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["A" + (rcntSign + 6).ToString() + ":" + Letter + (rcntSign + 6).ToString()])
                    {
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    }
                }
                else
                {
                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "36:" + Letter + "36"])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "For " + Customer;
                    //}

                    //using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "40:" + Letter + "40"])
                    //{
                    //    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //    objRange.Merge = true;
                    //    objRange.Style.Font.Size = 12;
                    //    objRange.Value = "Authorised Signatory";
                    //}


                    using (ExcelRange objRange = objWorksheet.Cells["B36:C36"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }



                    using (ExcelRange objRange = objWorksheet.Cells["B40:C40"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Operation Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "36:" + Letter + "36"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Authorised Signatory";
                    }

                    using (ExcelRange objRange = objWorksheet.Cells[LetterNoAndDate1 + "40:" + Letter + "40"])
                    {
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        objRange.Merge = true;
                        objRange.Style.Font.Size = 12;
                        objRange.Value = "Accounts Signatory";
                    }
                    using (ExcelRange objRange = objWorksheet.Cells["A41:" + Letter + "41"])
                    {
                        objRange.Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    }
                }
                System.Drawing.Image imgOperation;
                System.Drawing.Image imgAccounts;
                MemoryStream ms;
                byte[] imageOperation = null;
                byte[] imageAccounts = null;

                if (dtSign.Rows[0]["OperationApprovalImage"] != null && dtSign.Rows[0]["OperationApprovalImage"].ToString().Trim() != string.Empty)
                {
                    imageOperation = (byte[])dtSign.Rows[0]["OperationApprovalImage"];
                    ms = new MemoryStream(imageOperation);
                    imgOperation = System.Drawing.Image.FromStream(ms);
                    var picture = objWorksheet.Drawings.AddPicture("OprApp", imgOperation);
                    picture.SetPosition(rcntSign, 0, 1, 0);
                    picture.SetSize(100, 50);

                }
                ms = null;
                if (dtSign.Rows[0]["AccountsApprovalImage"] != null && dtSign.Rows[0]["AccountsApprovalImage"].ToString().Trim() != string.Empty)
                {
                    imageAccounts = (byte[])dtSign.Rows[0]["AccountsApprovalImage"];
                    ms = new MemoryStream(imageAccounts);
                    imgAccounts = System.Drawing.Image.FromStream(ms);
                    var picture1 = objWorksheet.Drawings.AddPicture("AccApp", imgAccounts);
                    picture1.SetPosition(rcntSign, 0, AccSignRow, 0);
                    picture1.SetSize(100, 50);
                }
                //Write it back to the client
                if (File.Exists(filename))
                    File.Delete(filename);
                //Create excel file on physical disk
                FileStream objFileStrm = File.Create(filename);
                objFileStrm.Close();
                //Write content to excel file
                File.WriteAllBytes(filename, objExcelPackage.GetAsByteArray());


                Workbook workbook = new Workbook();
                workbook.LoadFromFile(filename);
                if (File.Exists(filenamepdf))
                    File.Delete(filenamepdf);

                Worksheet sheet = workbook.Worksheets[0];
                sheet.PageSetup.Orientation = PageOrientationType.Landscape;
                sheet.PageSetup.IsFitToPage = true;
                //sheet.PageSetup.FitToPagesWide = 1;
                //sheet.PageSetup.FitToPagesTall = 0;
                sheet.PageSetup.TopMargin = 0.5;
                sheet.PageSetup.LeftMargin = 0.5;
                sheet.PageSetup.RightMargin = 0.5;
                sheet.PageSetup.BottomMargin = 0.5;
                sheet.SaveToPdf(filenamepdf);

                //System.IO.FileInfo fileInfo = new System.IO.FileInfo(filenamepdf);

                Session["FilePath"] = filenamepdf;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../pages/ViewFile.aspx','_blank')", true);

                //DownloadFile
                //Response.Clear();
                //Response.ClearContent();
                //Response.Buffer = true;
                //Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                //Response.ContentType = "application/pdf";
                //Response.TransmitFile(fileInfo.FullName);
                //Response.End();
            }
        }

        private string ReturnLetter(int Cnt)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (Cnt >= letters.Length)
                value += letters[Cnt / letters.Length - 1];

            value += letters[Cnt % letters.Length];

            return value;
        }

        private void LoadGridViewDetails(long InvoiceSrl)
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            optBO.Srl = InvoiceSrl;
            DataTable dt = optBO.FillInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        private void LoadGridViewDetailsEdit(long InvoiceSrl)
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            optBO.Srl = InvoiceSrl;
            DataTable dt = optBO.FillInvoiceDetails();
            grdPendingLRs.DataSource = dt;
            grdPendingLRs.DataBind();
        }

        private void LoadGridViewPenalty(long InvoiceSrl)
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            optBO.Srl = InvoiceSrl;
            DataTable dt = optBO.FillInvoicePenaltyDetails();
            Session["dtPenalty"] = dt;
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void chkLR_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            GridViewRow grdrow = (GridViewRow)chk.Parent.Parent;
            int rowIndex = 0;
            rowIndex = grdrow.RowIndex;

            if (chk.Checked)
            {
                EnableGridAmounts(rowIndex);
                txtRowNumber.Value = (rowIndex + 2).ToString();
            }
            else
            {
                DisableGridAmounts(rowIndex);
                txtRowNumber.Value = "";
            }
        }

        protected void grdViewItemDetls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[7].Text.Trim() == "Add Penalty")
                    e.Row.Cells[7].Text = "";
            }
        }

        private void FillDropDownReason()
        {
            PenaltyReasonBO optbo = new PenaltyReasonBO();
            DataTable dt;

            cmbReason.DataSource = null;
            cmbReason.Items.Clear();
            cmbReason.Items.Add(new ListItem("", "-1"));
            optbo.SearchText = "";
            dt = optbo.GetPenaltyReasondetails();
            cmbReason.DataSource = dt;
            cmbReason.DataTextField = "Name";
            cmbReason.DataValueField = "Srl";
            cmbReason.DataBind();
            cmbReason.SelectedIndex = 0;
        }

        protected void grdPendingLRs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];
            if (e.CommandName == "PenaltyEdit")
            {
                lblLRNoPenalty.Text = keys["LRNo"].ToString();
                HFLRId.Value = keys["LRId"].ToString();
                HFRowId.Value = index.ToString();

                FillDropDownReason();
                DataTable dt = null;
                if (CommonTypes.GetDataTable("dtPenalty") == null)
                {
                    dt = CommonTypes.CreateDataTable("LRID,ReasonSrl,Reason,Charges", "dtPenalty", "long,long,string,decimal");
                }
                else
                {
                    dt = CommonTypes.GetDataTable("dtPenalty");
                }

                if (dt.Rows.Count > 0)
                {
                    DataTable dtTarget = new DataTable();
                    dtTarget = dt.Clone();
                    dtTarget.TableName = "dtPenaltyTemp";
                    DataRow[] rowsToCopy;
                    rowsToCopy = dt.Select("LRID=" + keys["LRId"].ToString() + "");
                    foreach (DataRow temp in rowsToCopy)
                    {
                        dtTarget.ImportRow(temp);
                    }
                    CommonTypes.Insertdata(dtTarget);
                    grdItems.DataSource = dtTarget;
                    grdItems.DataBind();
                }

                popPenalty.Show();
            }
        }

        private void CheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (rowcheck.Enabled)
                    {
                        if (rowcheck.Checked)
                        {
                            continue;
                        }
                        else
                        {
                            rowcheck.Checked = true;
                        }
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        private void UnCheckAllRows(GridView grdView, string CntrlName)
        {
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl(CntrlName);
                    if (!rowcheck.Checked)
                    {
                        continue;
                    }
                    else
                    {
                        rowcheck.Checked = false;
                    }
                }
                else
                {
                    continue;
                }

            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            popPenalty.Show();
            CheckBox checkAll = (CheckBox)sender;
            if (checkAll.Checked)
            {
                CheckAllRows(grdItems, "chkSelect");
            }
            else
            {
                UnCheckAllRows(grdItems, "chkSelect");
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            DataTable dt = null;
            if (CommonTypes.GetDataTable("dtPenaltyTemp") == null)
            {
                dt = CommonTypes.CreateDataTable("LRID,ReasonSrl,Reason,Charges", "dtPenaltyTemp", "long,long,string,decimal");
            }
            else
            {
                dt = CommonTypes.GetDataTable("dtPenaltyTemp");
            }
            DataRow[] foundRows = dt.Select("ReasonSrl=" + cmbReason.SelectedValue + "");
            if (foundRows.LongLength > 0)
            {
                dt.Select("ReasonSrl=" + cmbReason.SelectedValue + "")
                .ToList<DataRow>()
                .ForEach(r =>
                {
                    r["Charges"] = double.Parse(txtPenalty.Text.Trim());
                });
                dt.AcceptChanges();
                grdItems.DataSource = dt;
                grdItems.DataBind();
                cmbReason.SelectedIndex = 0;
                txtPenalty.Text = "0";
                cmbReason.Focus();
                popPenalty.Show();
                return;
            }
            DataRow dr = dt.NewRow();
            dr["LRID"] = HFLRId.Value;
            dr["ReasonSrl"] = cmbReason.SelectedValue;
            dr["Reason"] = cmbReason.SelectedItem.Text.Trim();
            dr["Charges"] = txtPenalty.Text;
            CommonTypes.Addrow(dt, dr);
            grdItems.DataSource = dt;
            grdItems.DataBind();
            cmbReason.SelectedIndex = 0;
            txtPenalty.Text = "0";
            cmbReason.Focus();
            popPenalty.Show();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DataTable dt = CommonTypes.GetDataTable("dtPenaltyTemp");
            for (int grdItemIndex = 0; grdItemIndex < grdItems.Rows.Count; grdItemIndex++)
            {
                CheckBox rowcheck = (CheckBox)grdItems.Rows[grdItemIndex].FindControl("chkSelect");
                if (rowcheck.Checked)
                {
                    DataKey keys = grdItems.DataKeys[grdItemIndex];
                    int dtItemIndex = dt.Rows.Count - 1;
                    while (dtItemIndex >= 0)
                    {
                        if (dt.Rows[dtItemIndex]["ReasonSrl"].ToString() == keys["ReasonSrl"].ToString())
                        {
                            CommonTypes.DeleteRow("dtPenaltyTemp", dtItemIndex);
                        }
                        dtItemIndex = dtItemIndex - 1;
                    }
                    dt.AcceptChanges();
                }
            }
            grdItems.DataSource = CommonTypes.GetDataTable("dtPenaltyTemp");
            grdItems.DataBind();
            cmbReason.Focus();
            popPenalty.Show();
        }
    }
}