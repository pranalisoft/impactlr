﻿<%@ Page Title="Advance Paid" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="PaidAdvanceReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PaidAdvanceReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                       Advance Paid Report
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">                                
                                <tr>
                                    <td width="30px">
                                        <asp:Label ID="Label18" Text="From" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                    </td>
                                    <td width="30px">
                                        <asp:Label ID="Label17" Text="To" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                    </td>                                    
                                    <td width="50px">
                                        <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td width="50px">
                                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                            OnClick="btnExcel_Click" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label1" Text="Total Records : " runat="server" CssClass="NormalTextBold"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="lblTotalRecords" Text="0" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="overflow:auto">
                                <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="True"
                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                     ShowFooter="True">
                                    <%--<Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="InvRefNo" HeaderText="Invoice No." SortExpression="InvRefNo"
                                                        ItemStyle-Width="200px" />
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name" />
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="160px" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="50px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                </Columns>--%>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
