﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ItemScrap : System.Web.UI.Page
    {
        #region Procedures
        private void ShowViewByIndex(int index)
        {
            mltViewMaster.ActiveViewIndex = index;
        }

        private void filldropdown()
        {
            cmbItem.DataSource = null;
            cmbItem.Items.Clear();
            cmbItem.Items.Add(new ListItem("", "-1"));
            CommonBO optBo = new CommonBO();
            DataTable dt = optBo.FillScrapItem();
            cmbItem.DataSource = dt;
            cmbItem.DataTextField = "Name";
            cmbItem.DataValueField = "Srl";
            cmbItem.DataBind();
            if (dt.Rows.Count == 1)
            {
                cmbItem.SelectedIndex = 1;
                txtQtyStock.Text = dt.Rows[0]["Stock"].ToString();
            }
            DataTable dt1 = dt.Copy();
            grdItemStatus.DataSource = dt1;
            grdItemStatus.DataBind();
        }

        private void LoadGridView()
        {
            CommonBO OptBO = new CommonBO();
            DataTable dt = OptBO.FillScrap();
            grdViewIndex.DataSource = dt;
            grdViewIndex.DataBind();
            lblRecCount.Text = dt.Rows.Count.ToString();
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtQty.Text = txtQtyStock.Text = txtRemarks.Text = String.Empty;
            grdItemStatus.DataSource = null;
            grdItemStatus.DataBind();
            if (cmbItem.Items.Count > 0) cmbItem.SelectedIndex = 0;
            cmbItem.Enabled = true;
        }
        #endregion
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //MsgPopUp.modalPopupCommand += new CommandEventHandler(MsgPopUp_modalPopupCommand);
            //txtDate_CalendarExtender.EndDate = DateTime.Now;           
            if (!IsPostBack)
            {
                ShowViewByIndex(0);
                LoadGridView();
                btnAdd.Focus();
            }
        }

        protected void cmbItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            if (cmbItem.SelectedIndex > 0)
            {
                for (int i = 0; i < grdItemStatus.Rows.Count; i++)
                {
                    if (cmbItem.SelectedValue == grdItemStatus.DataKeys[i]["Srl"].ToString())
                    {
                        txtQtyStock.Text = grdItemStatus.DataKeys[i]["Stock"].ToString();
                        break;
                    }
                }
                txtQty.Focus();
            }
        }

        protected void grdViewIndex_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                MsgPanel.Message = string.Empty;
                MsgPanel.DispCode = -1;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    clearAll();
                    filldropdown();
                    ShowViewByIndex(1);
                    HFCode.Value = keys["Srl"].ToString();
                    cmbItem.SelectedValue = keys["ItemSrl"].ToString();
                    cmbItem_SelectedIndexChanged(cmbItem, EventArgs.Empty);
                    txtQty.Text = keys["Qty"].ToString().Replace(".00", "");
                    if (txtQtyStock.Text == "") txtQtyStock.Text = "0";
                    txtQtyStock.Text = (int.Parse(txtQtyStock.Text) + int.Parse(txtQty.Text)).ToString();
                    txtRemarks.Text = keys["Remarks"].ToString().Replace("Quarantine -", "").Trim();
                    cmbItem.Enabled = false;
                    txtQty.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        protected void grdViewIndex_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdViewIndex.PageIndex = e.NewPageIndex;
            LoadGridView();
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            MsgPanel.Message = string.Empty;
            MsgPanel.DispCode = -1;
            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Payment from Supplier";
                    ShowViewByIndex(1);                    
                    clearAll();
                    filldropdown();
                    //cmbSupl.Text = String.Empty;                  
                    if (cmbItem.Items.Count > 2)
                        cmbItem.Focus();
                    else
                        txtQty.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                //int cntDel = 0;
                //foreach (GridViewRow row in grdViewIndex.Rows)
                //{
                //    if (row.RowType == DataControlRowType.DataRow)
                //    {
                //        CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                //        if (rowcheck.Checked)
                //        {
                //            cntDel = cntDel + 1;
                //            break;
                //        }
                //    }
                //}
                //if (cntDel > 0)
                //{
                //    MsgPopUp.ShowModal("Are you sure.<br/>Do you want to cancel selected rcpts?", CommonTypes.ModalTypes.Confirm);
                //}
                //else
                //{
                //    MsgPopUp.ShowModal("Please select atleast one record to cancel", CommonTypes.ModalTypes.Error);
                //}                    
                //break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    CommonBO optbo = new CommonBO()
                    {
                        srl = long.Parse(HFCode.Value),
                        ItemSrl = long.Parse(cmbItem.SelectedValue),
                        Qty = int.Parse(txtQty.Text),
                        Remarks = txtRemarks.Text.Trim(),
                        CreatedBy = long.Parse(Session["EID"].ToString())
                    };
                    long cnt = optbo.InsertUpdateScrap();
                    if (cnt > 0)
                    {
                        if (long.Parse(HFCode.Value) == 0)
                        {
                            MsgPanel.Message = "Record Saved successfully.";
                            ShowViewByIndex(1);
                            txtQty.Focus();
                        }
                        else
                        {
                            MsgPanel.Message = "Record Updated successfully.";
                            ShowViewByIndex(0);
                        }
                        MsgPanel.DispCode = 1;
                        clearAll();
                        filldropdown();
                        LoadGridView();
                        
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    LoadGridView();
                    ShowViewByIndex(0);
                    break;
                case CommonTypes.EntryFormCommand.None:
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}