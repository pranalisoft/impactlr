﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class FrieghtTigerReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdMain);
            RegisterDateTextBox();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\FreightTigerReport_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();
            DataTable dt = OptBO.FreightTigerSummary();

            ExportToExcel(dt, filename,"");

        }

        private void ExportToExcel(DataTable ds, string XLPath, string DType)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);

                if (DType == "")
                    DType = "";
                else if (DType == "4")
                    objWorksheet.Cells["B:B"].Style.Numberformat.Format = "dd-MMM-yyyy";
                else
                    objWorksheet.Cells["D:D"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["Q:Q"].Style.Numberformat.Format = "dd-MMM-yyyy";
                //objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["A1:BZ1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        //private void ExportToExcel(DataTable dt, string fileName, string worksheetName)
        //{
        //    //Response.Clear();
        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment;filename=" + fileName + "");
        //    Response.ContentType = "application/vnd.ms-excel";

        //    StringWriter stringWriter = new StringWriter();
        //    HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWriter);
        //    DataGrid dataExportExcel = new DataGrid();
        //    dataExportExcel.ItemDataBound += new DataGridItemEventHandler(dataExportExcel_ItemDataBound);
        //    dataExportExcel.DataSource = dt;
        //    dataExportExcel.DataBind();
        //    dataExportExcel.RenderControl(htmlWrite);
        //    StringBuilder sbResponseString = new StringBuilder();
        //    sbResponseString.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"> <head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>" + worksheetName + "</x:Name><x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head> <body>");
        //    sbResponseString.Append(stringWriter + "</body></html>");
        //    Response.Write(sbResponseString.ToString());
        //    Response.End();
        //}

        void dataExportExcel_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Header Text Format can be done as follows
                e.Item.Font.Bold = true;

                //Adding Filter/Sorting functionality for the Excel
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    e.Item.Cells[cellIndex].Attributes.Add("x:autofilter", "all");
                    e.Item.Cells[cellIndex].Width = 160;
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Center;
                    cellIndex++;
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    //Any Cell specific formatting should be done here
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Left;
                    cellIndex++;
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void LoadGridView()
        {
            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();
            DataTable dt = GenerateTransposedTable(OptBO.FreightTigerSummary());
            dt.Columns.Add("ColName",typeof(string));
            dt.AcceptChanges();
            if (dt.Rows.Count==18)
            {
                dt.Rows[0][2] = "TotVehicles";
                dt.Rows[1][2] = "LocalTrips";
                dt.Rows[2][2] = "OutstationTrips";
                dt.Rows[3][2] = "TripsAdded";
                dt.Rows[4][2] = "TripsInFTNotInLR";
                dt.Rows[5][2] = "TripsInLRNotInFT";
                dt.Rows[6][2] = "Tracked";
                dt.Rows[7][2] = "NotTracked";
                dt.Rows[8][2] = "PerformancePer";
                dt.Rows[9][2] = "TotalRelianceJio";
                dt.Rows[10][2] = "RelianceTracked";
                dt.Rows[11][2] = "RelianceNotTracked";
                dt.Rows[12][2] = "JioPerformance";
                dt.Rows[13][2] = "JioPerformanceofNonTracked";
                dt.Rows[14][2] = "TripsNotAdded";
                dt.Rows[15][2] = "TripsNotAddedPerformance";
                dt.Rows[16][2] = "BSNL";
                dt.Rows[17][2] = "BSNLPerformance";
            }
            grdPacklistView.Visible = false;
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "";
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (Session["Item_Expr"] != null)
                    dataView.Sort = Session["Item_Expr"].ToString() + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        protected void grdMain_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Session["Item_Sort"] == null)
            {
                Session["Item_Sort"] = "ASC";
            }
            else
            {
                if (Session["Item_Sort"].ToString() == "ASC")
                    Session["Item_Sort"] = "DESC";
                else
                    Session["Item_Sort"] = "ASC";
            }
            Session["Item_Expr"] = e.SortExpression;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 8 || e.Row.RowIndex == 12 || e.Row.RowIndex == 13 || e.Row.RowIndex == 15 || e.Row.RowIndex == 17)
                {
                    e.Row.Cells[2].Text = "";
                    e.Row.Cells[3].Text = "";
                }
            }
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            //outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            outputTable.Columns.Add("Particulars", typeof(string));
            outputTable.Columns.Add("CntVal", typeof(float));

            // Header row's second column onwards, 'inputTable's first column taken
            //foreach (DataRow inRow in inputTable.Rows)
            //{
            //    string newColName = inRow[0].ToString();
            //    outputTable.Columns.Add(newColName);
            //}

            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }

        protected void grdMain_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
             GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            string FName = "";
            string LinkType = index.ToString();
            LRBO optbo = new LRBO();
            optbo.Fromdate = txtFromDate.Text;
            optbo.ToDate = txtToDate.Text;

            optbo.LinkType = LinkType;
            DataTable dt = optbo.FreightTigerSummaryLinks();

            if (e.CommandName.ToLower() == "cntshow")
            {
                grdPacklistView.Visible = true;
                grdPacklistView.DataSource = dt;
                grdPacklistView.DataBind();

               // LoadGridView();
                grdMain.Rows[8].Cells[2].Text = "";
                grdMain.Rows[12].Cells[2].Text = "";
                grdMain.Rows[13].Cells[2].Text = "";
                grdMain.Rows[15].Cells[2].Text = "";
                grdMain.Rows[17].Cells[2].Text = "";

                grdMain.Rows[8].Cells[3].Text = "";
                grdMain.Rows[12].Cells[3].Text = "";
                grdMain.Rows[13].Cells[3].Text = "";
                grdMain.Rows[15].Cells[3].Text = "";
                grdMain.Rows[17].Cells[3].Text = "";

                lblTotalPacklist.Visible = true;
                lblTotalPacklist.Text = keys["Particulars"].ToString();
            }

            else if (e.CommandName.ToLower() == "cntclick")
            {
                FName = "FreightTigerMIS_" + keys["ColName"].ToString();
                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\" + FName + ".xlsx");
                ExportToExcel(dt, filename, LinkType);

                grdMain.Rows[8].Cells[2].Text = "";
                grdMain.Rows[12].Cells[2].Text = "";
                grdMain.Rows[13].Cells[2].Text = "";
                grdMain.Rows[15].Cells[2].Text = "";
                grdMain.Rows[17].Cells[2].Text = "";

                grdMain.Rows[8].Cells[3].Text = "";
                grdMain.Rows[12].Cells[3].Text = "";
                grdMain.Rows[13].Cells[3].Text = "";
                grdMain.Rows[15].Cells[3].Text = "";
                grdMain.Rows[17].Cells[3].Text = "";
            }
           
            

            ///OLD Code
            //if (index == 2)
            //    {
            //        LinkType = "D";
            //        FName = "FreightTigerMIS_" + "Outstations";
            //    }
            //    else if (index == 4)
            //    {
            //        LinkType = "E";
            //        FName = "FreightTigerMIS_" + "InFTNotLR";
            //    }
            //    else if (index == 5)
            //    {
            //        LinkType = "F";
            //        FName = "FreightTigerMIS_" + "NotFTInLR";
            //    }
            //    else if (index == 7)
            //    {
            //        LinkType = "A";
            //        FName = "FreightTigerMIS_" + "NotTracked";
            //    }
            //    else if (index == 11)
            //    {
            //        LinkType = "B";
            //        FName = "FreightTigerMIS_" + "RelianceNotTracked";
            //    }

            //    else if (index == 14)
            //    {
            //        LinkType = "C";
            //        FName = "FreightTigerMIS_" + "TripsNotAdded";
            //    }
        }
    }
}