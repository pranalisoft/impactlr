﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ConsignorMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                FillGrid();
                //FillCustomerGrid(0);
                txtHiddenId.Value = "0";
                chkActive.Checked = true;
                //txtName.Focus();
                ShowViewByIndex(1);
            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                SetPanelMsg("", false, 0);
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    ClearView();
                    txtHiddenId.Value = keys["Srl"].ToString();
                    txtName.Text = keys["Name"].ToString();
                    txtAddress.Text = keys["Address"].ToString();
                    txtGSTNo.Text = keys["GSTNo"].ToString();
                    chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                    txtEmail.Text = keys["EmailId"].ToString();
                    txtMobile.Text = keys["MobileNo"].ToString();
                    txtPhone.Text = keys["PhoneNo"].ToString();
                    txtPinCode.Text = keys["Pincode"].ToString();
                    if (keys["ConsignorType"].ToString() == "R")
                    {
                        rdbconsignor.Checked = true;
                    }
                    else if (keys["ConsignorType"].ToString() == "E")
                    {
                        rdbconsignee.Checked = true;
                    }
                    else
                    {
                        rdbBoth.Checked = true;
                    }
                    FillCustomerGrid(long.Parse(txtHiddenId.Value));
                    ShowViewByIndex(0);
                    txtName.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void ClearView()
        {
            txtName.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtGSTNo.Text = string.Empty;
            chkActive.Checked = true;
            txtEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtMobile.Text = string.Empty;

        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid();
            txtSearch.Focus();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string ConsignorType = "";
                string CustomerXML = GenerateXML();

                if (rdbconsignor.Checked)
                {
                    ConsignorType = "R";
                }
                else if (rdbconsignee.Checked)
                {
                    ConsignorType = "E";
                }
                else
                {
                    ConsignorType = "B";
                }

                ConsignorBO optbo = new ConsignorBO();
                optbo.Name = txtName.Text.Trim();
                optbo.Srl = long.Parse(txtHiddenId.Value);
                optbo.Address = txtAddress.Text;
                optbo.GSTNo = txtGSTNo.Text.Trim();
                optbo.IsActive = chkActive.Checked;
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                optbo.EmailId = txtEmail.Text.Trim();
                optbo.MobileNo = txtMobile.Text.Trim();
                optbo.PhoneNo = txtPhone.Text.Trim();
                optbo.ConsignorType = ConsignorType;
                optbo.CustomerXML = CustomerXML;
                optbo.Pincode = txtPinCode.Text.Trim();

                if (optbo.isConsignorAlreadyExist() == false)
                {
                    long flgCnt = 0;
                    flgCnt = optbo.InsertConsignor();

                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        ClearView();
                        SetPanelMsg("Record added successfully.", true, 1);
                        FillCustomerGrid(0);
                        txtName.Focus();
                    }
                    else
                    {
                        ClearView();
                        SetPanelMsg("Record updated successfully.", true, 1);
                        FillGrid();
                        ShowViewByIndex(1);
                        txtSearch.Text = string.Empty;
                        txtSearch.Focus();
                    }


                    txtHiddenId.Value = "0";



                }
                else
                {
                    SetPanelMsg("Name Already Exists.", true, 0);
                }
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }


        private string GenerateXML()
        {
            string matXml = "";
            for (int i = 0; i < grdCustomer.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdCustomer.Rows[i].FindControl("chkSelectCust");
                if (chk.Checked)
                {
                    matXml = matXml + "<dtCustomer><CustomerId>" + grdCustomer.DataKeys[i]["CustomerId"].ToString() + "</CustomerId></dtCustomer>";
                }
            }
            if (!string.IsNullOrEmpty(matXml))
            {
                matXml = "<NewDataSet>" + matXml + "</NewDataSet>";
            }
            return matXml;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
                ShowViewByIndex(1);

                //Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearView();
                txtName.Focus();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid()
        {
            try
            {
                ConsignorBO optbo = new ConsignorBO();
                optbo.SearchText = txtSearch.Text.Trim();
                DataTable dt = optbo.GetConsignordetails();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {

                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void grdMst_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SetPanelMsg("", false, 0);
            int cnt1 = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    if (rowcheck.Checked)
                    {
                        cnt1 = cnt1 + 1;
                        break;
                    }
                }
            }
            if (cnt1 > 0)
            {
                popDelPacklist.Show();
            }
            else
            {
                SetPanelMsg("Please select atleast one record to delete", true, 0);
            }
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        FillGrid();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        private long Delete()
        {
            ConsignorBO OptBO = new ConsignorBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdMst.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteConsignor();
            }

            return srl;
        }

        private void FillCustomerGrid(long ConsignorId)
        {
            ConsignorBO optbo = new ConsignorBO();
            optbo.Srl = ConsignorId;
            DataTable dtcust = optbo.FillCustomersofConsignors();
            grdCustomer.DataSource = dtcust;
            grdCustomer.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtHiddenId.Value = "0";
            ClearView();
            FillCustomerGrid(0);
            ShowViewByIndex(0);
            txtName.Focus();
        }
    }
}