﻿<%@ Page Title="Update LR Status" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="LRStatusUpdationFromTrip.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRStatusUpdationFromTrip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
 <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />            
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            LR Status Updation
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <table style="padding: 0px 0px 0px 10px; width: 100%;">
                </table>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table width="100%">
                        <tr>
                            <td>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblTotalPacklist" Text="Pending Count : 000" runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="2">
                                <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="ID,LRNo,BranchName,PlacementID,TripID,FromLoc,ToLoc,Status,CurrentRemarks"
                                    OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                    OnRowCommand="grdPacklistView_RowCommand" AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="BranchName" HeaderText="Branch" SortExpression="BranchName" ItemStyle-Width="250px"/>                                           
                                        <asp:BoundField DataField="PlacementID" HeaderText="PlacementID" SortExpression="PlacementID" ItemStyle-Width="100px"/>
                                        <asp:BoundField DataField="TripID" HeaderText="TripID" SortExpression="TripID" ItemStyle-Width="100px"/>
                                        <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc" ItemStyle-Width="180px"/>
                                        <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc" ItemStyle-Width="180px"/>
                                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="120px"/>
                                        <asp:ButtonField ButtonType="Link" CommandName="Remarks" DataTextField="CurrentRemarks" HeaderText="Remarks" SortExpression="CurrentRemarks"/>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>           
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
