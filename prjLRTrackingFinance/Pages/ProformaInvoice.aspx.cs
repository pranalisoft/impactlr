﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Globalization;


namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ProformaInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void LoadGridView()
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.SearchText = txtSearch.Text.Trim();
            optBO.BranchID = long.Parse(Session["BranchID"].ToString());
            optBO.FromDate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            DataTable dt = optBO.FillInvoiceGrid();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalPacklist.Text = "Total Records : " + dt.Rows.Count.ToString();
        }

        private void FillDropDowns()
        {
            cmbBillingCompany.DataSource = null;
            cmbBillingCompany.Items.Clear();
            cmbBillingCompany.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBillingCompany.DataSource = dt;
            cmbBillingCompany.DataTextField = "Name";
            cmbBillingCompany.DataValueField = "Srl";
            cmbBillingCompany.DataBind();
            cmbBillingCompany.SelectedIndex = 0;         
        }

        protected void txtBasicAmount_TextChanged(object sender, EventArgs e)
        {

        }

        protected void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            FillPendingVehicles();
            FillPendingLR();
        }

        private void FillPendingLRsForProforma(long BillingCompanySrl, long CustomerSrl, string destination, string vehicleNo)
        {            
            InvoiceBO optbo = new InvoiceBO();
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            optbo.CustomerSrl = CustomerSrl;
            optbo.BillingCompanySrl =BillingCompanySrl ;
            
            optbo.VehicleNo = vehicleNo;
            optbo.Destination = destination;
            DataTable dt = optbo.FillPendingLRS();
            grdPendingLRs.DataSource = dt;
            grdPendingLRs.DataBind();

            DisableGridAmounts(-1);
        }

        private void FillPendingLR()
        {
            string destination = string.Empty;
            string vehicleNo = string.Empty;
            if (cmbDestination.SelectedValue != "-1")
            {
                destination = cmbDestination.SelectedValue;
            }
            if (cmbVehicle.SelectedValue != "-1")
            {
                vehicleNo = cmbVehicle.SelectedValue;
            }
            FillPendingLRsForProforma(long.Parse(cmbBillingCompany.SelectedValue), long.Parse(cmbCustomer.SelectedValue), destination, vehicleNo);
        }

        private void DisableGridAmounts(int rowindex)
        {
            if (rowindex == -1)
            {
                for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
                {
                    GridViewRow grdrow = grdPendingLRs.Rows[i];
                    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");                

                    txtFreight.Enabled = false;              
                }
            }
            else
            {
                GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
                TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
              

                txtFreight.Enabled = false;               
                DataKey keys = grdPendingLRs.DataKeys[rowindex];
                txtFreight.Text = keys["TotalFrieght"].ToString();          
            }
        }

        private void EnableGridAmounts(int rowindex)
        {
            if (rowindex == -1)
            {
                for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
                {
                    GridViewRow grdrow = grdPendingLRs.Rows[i];
                    TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");

                    CalculateTotalAmount();
                    txtFreight.Enabled = true;                   
                    txtFreight.Focus();
                }
            }
            else
            {
                GridViewRow grdrow = grdPendingLRs.Rows[rowindex];
                TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");

                CalculateTotalAmount();
                txtFreight.Enabled = true;
                txtFreight.Focus();
            }
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            InvoiceBO optbo = new InvoiceBO();
            NumberToWords wordBO = new NumberToWords();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillDropDowns();
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtDate.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Srl = long.Parse(txtHiddenId.Value);
                    optbo.InvoiceDate = txtDate.Text;
                    optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
                    optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
                    optbo.BasicAmount = decimal.Parse(txtBasicAmount.Text);
                    optbo.BranchID = long.Parse(Session["BranchID"].ToString());
                    optbo.EnteredBy = long.Parse(Session["EID"].ToString());
                    optbo.ItemXML = GetItemXML();
                    optbo.PaymentTerms = string.Empty;
                    optbo.Particulars = string.Empty;
                    optbo.AmountInWords = wordBO.Num2WordConverter(txtBasicAmount.Text).ToString();
                    optbo.BuyerRefNo = string.Empty;
                    optbo.BuyerRefDate = string.Empty;

                    long cnt = optbo.InsertInvoice();
                    if (cnt > 0)
                    {
                        string invno = "";
                        optbo.Srl = cnt;
                        invno = optbo.GetInvoiceNoFromID();
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Invoice Generated Successfully with No. :" + invno   ,true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Invoice Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private string GetItemXML()
        {
            string strxml = String.Empty;
            string LRID = String.Empty;

            for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                TextBox txtFreight = (TextBox)grdPendingLRs.Rows[i].FindControl("txtFrieght");   

                DataKey key = grdPendingLRs.DataKeys[i];
                if (chk.Checked)
                {
                    LRID = key["ID"].ToString();
                    strxml = strxml + "<dtItem><LRId>" + LRID + "</LRId><TransportationCharges>" + txtFreight.Text.Trim() + "</TransportationCharges><DetentionCharges>" + "0" + "</DetentionCharges><WaraiCharges>" + "0" + "</WaraiCharges><TwoPointCharges>" + "0" + "</TwoPointCharges><OtherCharges>" + "0" + "</OtherCharges><TotalCharges>" + "0" + "</TotalCharges></dtItem>";
                }
            }

            if (strxml != String.Empty)
            {
                strxml = "<NewDataSet>" + strxml + "</NewDataSet>";
            }
            return strxml;
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void txtFrieght_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow grdrow = (GridViewRow)txt.Parent.Parent;
            int rowIndex = 0;
            rowIndex = grdrow.RowIndex;
            CalculateTotalAmount();

            btnSavePacklist.Focus();          
        }

        private void CalculateRowAmount(int rowIndex)
        {
            GridViewRow grdrow = grdPendingLRs.Rows[rowIndex];
            TextBox txtFreight = (TextBox)grdrow.FindControl("txtFrieght");
         

            decimal freightCharges = txtFreight.Text.Trim() == "" ? 0 : decimal.Parse(txtFreight.Text.Trim());

            decimal TotalCharges = freightCharges;
          
            CalculateTotalAmount();
        }

        private void CalculateTotalAmount()
        {
            decimal Total = 0;
            for (int i = 0; i < grdPendingLRs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grdPendingLRs.Rows[i].FindControl("chkLR");
                if (chk.Checked)
                {
                    TextBox txttotal = (TextBox)grdPendingLRs.Rows[i].FindControl("txtFrieght");
                    Total += txttotal.Text == "" ? 0 : decimal.Parse(txttotal.Text.Trim());
                }
            }
            txtBasicAmount.Text = Total.ToString("F2");
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    popDelPacklist.Hide();
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        private void ClearView()
        {
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");          
            txtBasicAmount.Text = string.Empty;            
            SetPanelMsg("", false, 0);
            if (cmbBillingCompany.Items.Count > 0) cmbBillingCompany.SelectedIndex = 0;
            if (cmbCustomer.Items.Count > 0) cmbCustomer.SelectedIndex = 0;
            if (cmbVehicle.Items.Count > 0) cmbVehicle.SelectedIndex = 0;
            if (cmbDestination.Items.Count > 0) cmbDestination.SelectedIndex = 0;
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdPacklistView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Item")
            {
                PnlItemDtls.Visible = true;
                lblItemDtls.Text = "Details For Invoice No. " + keys["InvoiceNo"].ToString();
                LoadGridViewDetails(long.Parse(keys["Srl"].ToString()));
            }
            if (e.CommandName == "ViewReport")
            {
                {
                    Session["InvoiceID"] = keys["Srl"].ToString();
                    Session["ReportType"] = "InvDoc";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
            if (e.CommandName == "ItemReport")
            {
                {
                    Session["InvoiceID"] = keys["Srl"].ToString();
                    Session["ReportType"] = "InvItems";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OpenWindow", "window.open('../Reports/ShowReport.aspx','_blank')", true);
                }
            }
        }

        private void LoadGridViewDetails(long InvoiceSrl)
        {
            InvoiceBO optBO = new InvoiceBO();
            optBO.Srl = InvoiceSrl;
            DataTable dt = optBO.FillProformaInvoiceDetails();
            grdViewItemDetls.DataSource = dt;
            grdViewItemDetls.DataBind();
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdPacklistView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void chkLR_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            GridViewRow grdrow = (GridViewRow)chk.Parent.Parent;
            int rowIndex = 0;
            rowIndex = grdrow.RowIndex;

            if (chk.Checked)
            {
                EnableGridAmounts(rowIndex);
            }
            else
            {
                DisableGridAmounts(rowIndex);
            }
        }

        protected void cmbVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            FillPendingDestinations();
            FillPendingLR();
        }

        protected void cmbDestination_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            FillPendingLR();
        }

        protected void cmbBillingCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdPendingLRs.DataSource = null;
            grdPendingLRs.DataBind();
            FillPendingCustomers();          
        }

        private void FillPendingCustomers()
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            InvoiceBO optbo = new InvoiceBO();
            optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            DataTable dtCust = optbo.FillPendingCustomers();
            cmbCustomer.DataSource = dtCust;
            cmbCustomer.DataTextField = "CustomerName";
            cmbCustomer.DataValueField = "CustomerID";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 0;
        }

        private void FillPendingVehicles()
        {
            cmbVehicle.DataSource = null;
            cmbVehicle.Items.Clear();
            cmbVehicle.Items.Add(new ListItem("", "-1"));
            InvoiceBO optbo = new InvoiceBO();
            optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
            DataTable dtCust = optbo.FillPendingVehicles();
            cmbVehicle.DataSource = dtCust;
            cmbVehicle.DataTextField = "VehicleNo";
            cmbVehicle.DataValueField = "VehicleNo";
            cmbVehicle.DataBind();
            cmbVehicle.SelectedIndex = 0;
        }

        private void FillPendingDestinations()
        {
            cmbDestination.DataSource = null;
            cmbDestination.Items.Clear();
            cmbDestination.Items.Add(new ListItem("", "-1"));
            InvoiceBO optbo = new InvoiceBO();
            optbo.BillingCompanySrl = long.Parse(cmbBillingCompany.SelectedValue);
            optbo.BranchID = long.Parse(Session["BranchID"].ToString());
            optbo.CustomerSrl = long.Parse(cmbCustomer.SelectedValue);
            optbo.VehicleNo = cmbVehicle.SelectedValue;
            DataTable dtCust = optbo.FillPendingDestinations();
            cmbDestination.DataSource = dtCust;
            cmbDestination.DataTextField = "ToLoc";
            cmbDestination.DataValueField = "ToLoc";
            cmbDestination.DataBind();
            cmbDestination.SelectedIndex = 0;
        }
    }
}