﻿<%@ Page Title="Supplier Debit Note" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="SupplierDebitNote.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierDebitNote" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Supplier Debit Note
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="view1" runat="server">
                            <div class="buttons_top">
                                <asp:Button ID="btnAdd" Text="Add" runat="server" CssClass="button" CommandName="Add"
                                    OnCommand="EntryForm_Command" TabIndex="1" />
                                <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="button" CommandName="Delete"
                                    OnCommand="EntryForm_Command" TabIndex="2" />
                            </div>
                            <div class="grid_top_region">
                                <div class="grid_top_region_lft">
                                    <asp:CheckBox ID="chkDateFilter" runat="server" AutoPostBack="true" Text="Date Filter"
                                        TabIndex="3" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtFromDate" Width="100px" runat="server" TabIndex="3" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_From" TargetControlID="txtFromDate" PopupButtonID="imgBtnCalcPopupFrom"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupFrom" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="4" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtToDate" Width="100px" runat="server" TabIndex="5" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_To" TargetControlID="txtToDate" PopupButtonID="imgBtnCalcPopupTo"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupTo" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="6" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnFilter" Text="Show" runat="server" CssClass="button" CommandName="Filter"
                                        OnCommand="Filter_Command" TabIndex="7" ValidationGroup="save" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnClearFilter" Text="Clear Filter" runat="server" CssClass="button"
                                        CommandName="ClearFilter" OnCommand="Filter_Command" TabIndex="8" />
                                </div>
                                <div class="grid_top_region_rght">
                                    Records :
                                    <asp:Label ID="lblRecCount" Text="0" runat="server" />
                                    <%--<table border="0" class="cnt_region">
                                    <tr>
                                        <td>
                                            Records :
                                            <asp:Label ID="lblRecCount" Text="1000" runat="server" />
                                        </td>
                                    </tr>
                                </table>--%>
                                </div>
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdViewIndex" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    AllowPaging="True" DataKeyNames="Srl,RefNo,TranDate,SupplierID,SupplierName,TotalAmount,PaymentMode,UserName,BranchId,ChqNo,ChqDate,PayMode"
                                    OnRowCommand="grdViewIndex_RowCommand" OnRowDataBound="grdViewIndex_RowDataBound"
                                    EmptyDataText="No records found." Width="100%" OnPageIndexChanging="grdViewIndex_PageIndexChanging"
                                    PageSize="15">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                    AutoPostBack="true" TabIndex="5" /></HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" TabIndex="6" /></ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="28px" />
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RefNo" HeaderText="Srl." SortExpression="RefNo" ReadOnly="True"
                                            ItemStyle-Width="80px" />
                                        <%--  <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="RefNo"
                                            HeaderText="No." SortExpression="RefNo" ItemStyle-Width="100px" />--%>
                                        <asp:BoundField DataField="TranDate" HeaderText="Date" SortExpression="TranDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="PayMode" HeaderText="Payment Mode" SortExpression="PayMode"
                                            ReadOnly="True" ItemStyle-Width="100px" />
                                        <%--<asp:BoundField DataField="ChqNo" HeaderText="Chq. No." SortExpression="ChqNo" ReadOnly="True"
                                            ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ChqDate" HeaderText="Chq. Date" SortExpression="ChqDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />--%>
                                        <asp:BoundField DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                            ReadOnly="True" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="UserName" HeaderText="Entered By" SortExpression="UserName"
                                            ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:ButtonField CommandName="PayDetails" HeaderText="" Text="Details" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <%--  <asp:ButtonField CommandName="Report" HeaderText="" Text="Document" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />--%>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                <div class="grid_top_region">
                                    <div class="grid_top_region_lft">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="grid_top_region_rght">
                                        Records :
                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                    </div>
                                </div>
                                <div class="grid_region">
                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="LRNo,LRDate,InvoiceNo,InvoiceDate,TotalAmount,TDSAmount,TDSPer"
                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo" ReadOnly="True"
                                                ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." SortExpression="InvoiceNo"
                                                ReadOnly="True" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TDSPer" HeaderText="TDS %" SortExpression="TDSPer" ItemStyle-Width="80px"
                                                ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TDSAmount" HeaderText="TDS Amount" SortExpression="TDSAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                    <%--<asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="InvoiceID,InvoiceNo,InvoiceDate,InvoiceAmount,AdjAmt,DetailSrl"
                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." SortExpression="InvoiceNo"
                                                ReadOnly="True" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="InvoiceAmount" HeaderText="Inv. Amount" SortExpression="InvoiceAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="AdjAmt" HeaderText="Adj. Amount" SortExpression="AdjAmt"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>--%>
                                </div>
                            </asp:Panel>
                        </asp:View>
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="1" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="2" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the receipt date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label1" Text="Supplier" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbSupl" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupl_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbSupl" ErrorMessage="Please select Supplier from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trChqNo" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblNarration" Text="Narration" runat="server" AssociatedControlID="txtNarration" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtNarration" Width="300px" runat="server" TabIndex="3" MaxLength="50"
                                                            CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the narration"
                                                            ValidationGroup="save" ControlToValidate="txtNarration" Display="None" SetFocusOnError="true"
                                                            Enabled="false"></asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true"
                                                            TargetControlID="reqFVChqNo">
                                                        </ajaxToolkit:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <caption>
                                                    <br />
                                                    <tr class="control_row">
                                                        <td>
                                                            <span class="req_mark">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Amount" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="True" MaxLength="100" CssClass="NormalTextBold"
                                                                OnTextChanged="txtAmount_TextChanged" TabIndex="8" Width="100px" Style="text-align: right" />
                                                            <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                            <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                            <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                                TargetControlID="txtAmount" ValidChars=".">
                                                            </asp:FilteredTextBoxExtender>
                                                            <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                                ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                            <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                                TargetControlID="regExAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                        </td>
                                                    </tr>
                                                </caption>
                                            </table>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table width="100%">
                                                <asp:GridView ID="GrdPaymentStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                    DataKeyNames="" EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                    <Columns>
                                                        <asp:BoundField DataField="BillCnt" HeaderText="Total Bills" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="BillAmt" HeaderText="Bill Amount" ItemStyle-Width="200px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="PaidAmt" HeaderText="Paid Amount" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Outstanding" HeaderText="OutStanding" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="header" />
                                                    <RowStyle CssClass="row" />
                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                </asp:GridView>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <br />
                            <div class="grid_top_region">
                            </div>
                            <div class="grid_region">
                            <asp:GridView ID="grdPaymentSupl" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt,LRId,LRNo,TotalCharges,PaidAmount,BalanceLRAmount,TDSPer"
                                    EmptyDataText="No records found." Width="100%" TabIndex="100">
                                    <Columns>
                                    <asp:BoundField DataField="LRNo" HeaderText="LR No." ItemStyle-Width="120px" />
                                        <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Date" ItemStyle-Width="100px"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="TotalCharges" HeaderText="Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PaidAmount" HeaderText="Paid" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <%--<asp:BoundField DataField="BalAmt" HeaderText="Balance" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:TemplateField HeaderText="Balance">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false" Text='<%#Eval("BalanceLRAmount") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:filteredtextboxextender id="FilterAmt" runat="server" filtertype="Numbers,Custom"
                                                    validchars="." targetcontrolid="txtAdjAmt" enabled="True">
                                                </asp:filteredtextboxextender>
                                                <asp:RegularExpressionValidator ID="regExAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:validatorcalloutextender id="regExAmtCall" runat="server" enabled="True"
                                                    targetcontrolid="regExAmt" popupposition="Left">
                                                </asp:validatorcalloutextender>
                                                <asp:CompareValidator ID="CompQty" runat="server" ErrorMessage="Adjust amount cannot be greater than balance amount"
                                                    ControlToCompare="txtBalAmt" ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="true"
                                                    ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:validatorcalloutextender id="CompQtyCall" runat="server" enabled="True"
                                                    targetcontrolid="CompQty" popupposition="Left">
                                                </asp:validatorcalloutextender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                                <%--<asp:GridView ID="grdPaymentSupl" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt"
                                    EmptyDataText="No records found." Width="100%" TabIndex="100">
                                    <Columns>
                                        <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Date" ItemStyle-Width="100px"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="ReceivedAmount" HeaderText="Paid" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />                                        
                                        <asp:TemplateField HeaderText="Balance">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    CssClass="NormalTextBold" MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false"
                                                    Text='<%#Eval("BalAmt") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtAdjAmt" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExAmtCall" runat="server" Enabled="True" TargetControlID="regExAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompQty" runat="server" ErrorMessage="Adjust amount cannot be greater than balance amount"
                                                    ControlToCompare="txtBalAmt" ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="true"
                                                    ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:ValidatorCalloutExtender ID="CompQtyCall" runat="server" Enabled="True" TargetControlID="CompQty"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>--%>
                            </div>
                            <br />
                            <table class="control_set" style="width: 100%">
                            </table>
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="10" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="11" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbSupl" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });

        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
