﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using BusinessObjects.BO;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class CustomerZoneTypeMaster : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetPanelMsg("", false, -1);
			if (!IsPostBack)	
			{				
                FillDropDownCustomer();
                FillGrid();
                clearAll();
                cmbCustomer.Focus();
			}
		}

        private void FillDropDownCustomer()
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.Items.Clear();
            cmbCustomer.Items.Add(new ListItem("", "-1"));
            CustomerBO billingBO = new CustomerBO();
            billingBO.SearchText = string.Empty;
            billingBO.ShowAll ="N";
            DataTable dt = billingBO.GetCustomerDetails();
            cmbCustomer.DataSource = dt;
            cmbCustomer.DataTextField = "Name";
            cmbCustomer.DataValueField = "Srl";
            cmbCustomer.DataBind();
            cmbCustomer.SelectedIndex = 1;
        }

        private void clearAll()
        {
            txtHiddenId.Value = "0";
            txtZoneType.Text = "";
            cmbCustomer.SelectedIndex = 0;
            txtZoneType.Focus();
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid()
        {
            try
            {
                CustomerZoneTypeBO optbo = new CustomerZoneTypeBO();
                optbo.SearchText = txtSearch.Text.Trim();
                DataTable dt = optbo.GetCustomerZoneType();
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private long Delete()
        {
            CustomerZoneTypeBO OptBO = new CustomerZoneTypeBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdMst.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }
            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.CustomerZoneTypeDelete();
            }
            return srl;
        }

		protected void btnInfoOk_Click(object sender, EventArgs e)
		{
			//Response.Redirect("~/pages/SiteReturnMain.aspx", false);
		}

		protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			try
			{
				if (e.CommandName.Equals("Page"))
					return;
				if (e.CommandName.Equals("Sort"))
					return;
				int index = Convert.ToInt32(e.CommandArgument);
				GridView grd = (GridView)e.CommandSource;
				DataKey keys = grd.DataKeys[index];
				GridViewRow row1 = grd.Rows[index];

				if (e.CommandName == "Modify")
				{
                    FillDropDownCustomer();
                    clearAll();                    
					txtHiddenId.Value = keys["Srl"].ToString();
                    txtZoneType.Text = keys["ZoneType"].ToString();
                    cmbCustomer.SelectedValue = keys["CustomerId"].ToString();
                    chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                    txtZoneType.Focus();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}		

		protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			grdMst.PageIndex = e.NewPageIndex;
			FillGrid();
		}

        protected void grdMst_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			FillGrid();
			txtSearch.Focus();
		}

		protected void btnClearSearch_Click(object sender, EventArgs e)
		{
			txtSearch.Text = string.Empty;
			FillGrid();
			txtSearch.Focus();
		}        

		protected void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
                CustomerZoneTypeBO optbo = new CustomerZoneTypeBO();
                
                optbo.Srl = long.Parse(txtHiddenId.Value);
                optbo.ZoneType = txtZoneType.Text.Trim();
               
                optbo.CustomerId = long.Parse(cmbCustomer.SelectedValue);
                optbo.IsActive = chkActive.Checked;
               
                optbo.CreatedBy = long.Parse(Session["EID"].ToString());
              
                    long flgCnt = 0;
                    flgCnt = optbo.InsertUpdateCustomerZoneType();

                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        SetPanelMsg("Zone Type added successfully.", true, 1);
                        txtHiddenId.Value = "0";
                        txtZoneType.Text = "";
                        txtZoneType.Focus();
                    }
                    else
                    {
                        SetPanelMsg("Zone Type updated successfully.", true, 1);
                        clearAll();
                    }
                    //clearAll();
                    txtSearch.Text = string.Empty;
                    FillGrid();
                    
			}
			catch (Exception exp)
			{			  
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SetPanelMsg("", false, 0);
            int cnt1 = 0;
            foreach (GridViewRow row in grdMst.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    if (rowcheck.Checked)
                    {
                        cnt1 = cnt1 + 1;
                        break;
                    }
                }
            }
            if (cnt1 > 0)
            {
                popDelPacklist.Show();
            }
            else
            {
                SetPanelMsg("Please select atleast one record to delete", true, 0);
            }
        }

		protected void btnBack_Click(object sender, EventArgs e)
		{
			try
			{
				Response.Redirect("~/pages/LRMain.aspx", false);
			}
			catch (Exception exp)
			{
				
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			try
			{
                clearAll();
                txtZoneType.Focus();
			}
			catch (Exception exp)
			{                
				Response.Redirect("~/pages/ErrorPage.aspx", false);
			}
		}		

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        FillGrid();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }        
	}
}