﻿<%@ Page Title=" Customer Wise Zone Type" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="CustomerZoneTypeMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.CustomerZoneTypeMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Customer Wise Zone Type
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Customer Wise Zone Type
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table style="width: 99.5%">
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label3" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbCustomer" ErrorMessage="Please select customer from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="Zone Type" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtZoneType" runat="server" MaxLength="50" TabIndex="2" Width="270px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtZoneType" SetFocusOnError="True" ValidationGroup="save"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" Enabled="True"
                                            TargetControlID="RequiredFieldValidator1">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Active" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" TabIndex="3" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="4" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="5" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="6" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="6" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="7" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                            <asp:Button TabIndex="8" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                OnClick="btnDelete_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,Name,ZoneType,IsActive,CustomerId,UsedCnt"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand" OnRowDataBound="grdMst_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="30px" />
                                        <ItemStyle Width="30px" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                    </asp:TemplateField>
                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Customer"
                                        SortExpression="Name" />
                                    <asp:BoundField DataField="ZoneType" HeaderText="Zone Type" SortExpression="ZoneType"
                                        ItemStyle-Width="200px" />
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
