﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PODPerformance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            ScriptManager scrp1 = ScriptManager.GetCurrent(this.Page);
            scrp1.RegisterPostBackControl(grdMain);
            RegisterDateTextBox();            
            txtFromDate.Focus();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }        

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (cmbSlab.SelectedIndex==0)
            {
                if (grdMain.Rows.Count == 0)
                {
                    return;
                }
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = "";
                string FileName = "PODPerformance_" + DateTime.Now + ".xls";
                StringWriter strwritter = new StringWriter();
                HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                grdMain.GridLines = GridLines.Both;
                for (int i = 0; i < grdMain.Columns.Count; i++)
                {
                    grdMain.HeaderRow.Cells[i].Font.Bold = true;
                    grdMain.HeaderRow.Cells[i].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                    grdMain.HeaderRow.Cells[i].ForeColor = System.Drawing.Color.White;
                }
                grdMain.RowStyle.Height = 20;
                //grdMain.FooterStyle.Font.Bold = true;
                //grdMain.FooterStyle.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                //grdMain.FooterStyle.ForeColor = System.Drawing.Color.White;
                grdMain.RenderControl(htmltextwrtter);
                Response.Write(strwritter.ToString());
                Response.End();    
            }
            else
            {
                ReportBO rptBo = new ReportBO();
                rptBo.FromDate = txtFromDate.Text.Trim();
                rptBo.ToDate = txtToDate.Text.Trim();
                rptBo.BranchId = 0;
                rptBo.type = cmbSlab.SelectedValue;
                rptBo.CreatedBy = long.Parse(Session["EID"].ToString());
                DataTable dt = rptBo.GetPODPerformanceSlabWise(); 

                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\PODPerformanceDetail_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
                ExportToExcel(dt, filename);
            }
              

        }

        private void ExportToExcel(DataTable ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                //Create the worksheet
                int RCnt = ds.Rows.Count;
                ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.TableName);
                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                objWorksheet.Cells["A1"].LoadFromDataTable(ds, true);
                objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["K:K"].Style.Numberformat.Format = "dd-MMM-yyyy";
                objWorksheet.Cells["L:L"].Style.Numberformat.Format = "dd-MMM-yyyy";
                
                objWorksheet.Cells["A1:P1"].AutoFilter = true;
                objWorksheet.Cells.AutoFitColumns();
                objWorksheet.View.FreezePanes(2, 1);
                //Format the header              
                using (ExcelRange objRange = objWorksheet.Cells["A1:P1"])
                {
                    objRange.Style.Font.Bold = true;
                    objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    objRange.AutoFilter = true;
                    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                    //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                }
                for (int i = 0; i < ds.Rows.Count; i++)
                {
                    if (int.Parse(ds.Rows[i]["PODDays"].ToString()) > 15)
                    {
                        using (ExcelRange objRange = objWorksheet.Cells["A"+(i+2).ToString()+":P"+(i+2).ToString()])
                        { 
                            objRange.Style.Font.Color.SetColor(System.Drawing.Color.Red);
                            //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                            //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                        }
                    }
                }
                //using (ExcelRange objRange = objWorksheet.Cells["A1:J1".Replace("1", (RCnt + 1).ToString())])
                //{
                //    objRange.Style.Font.Bold = true;
                //    objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                //    objRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(13, 126, 164));
                //    objRange.Style.Font.Color.SetColor(System.Drawing.Color.White);
                //}
                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }        

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        private void LoadGridView()
        {
            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();

            string ReportType = "A";
            if (rdbPODNot.Checked) ReportType = "N";
            else if (rdbPODReq.Checked) ReportType = "P";

            OptBO.ReportType = ReportType;
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = OptBO.PODPerformanceReport();
            grdMain.DataSource = dt;
            grdMain.DataBind();
            lblTotalPacklist.Text = "Records : " + dt.Rows.Count.ToString();

            decimal total = 0; ;
            grdMain.FooterRow.Cells[0].Text = "Total";
            grdMain.FooterRow.Cells[0].Font.Bold = true;
            grdMain.FooterRow.Cells[0].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[0].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Left;

            for (int k = 2; k <= 8; k++)
            {
                total = dt.AsEnumerable().Sum(row => row.Field<Int32>(dt.Columns[k].ToString()));

                grdMain.FooterRow.Cells[k-1].Text = total.ToString().Replace(".00", "");
                grdMain.FooterRow.Cells[k-1].Font.Bold = true;
                grdMain.FooterRow.Cells[k-1].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                grdMain.FooterRow.Cells[k-1].ForeColor = System.Drawing.Color.White;
                grdMain.FooterRow.Cells[k-1].HorizontalAlign = HorizontalAlign.Right;
            }

            decimal Under15R = decimal.Parse(grdMain.FooterRow.Cells[1].Text);
            decimal Above15R = decimal.Parse(grdMain.FooterRow.Cells[2].Text);
            decimal Above60R = decimal.Parse(grdMain.FooterRow.Cells[3].Text);
            decimal Under15N = decimal.Parse(grdMain.FooterRow.Cells[4].Text);
            decimal Above15N = decimal.Parse(grdMain.FooterRow.Cells[5].Text);
            decimal Above60N = decimal.Parse(grdMain.FooterRow.Cells[6].Text);
            decimal Total = decimal.Parse(grdMain.FooterRow.Cells[7].Text);

            grdMain.FooterRow.Cells[8].Text = Under15R + Above15R + Above60R == 0 ? "0" : System.Math.Round((Under15R * 100 / (Under15R + Above15R + Above60R)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[8].Font.Bold = true;
            grdMain.FooterRow.Cells[8].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[8].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[9].Text = Under15R + Above15R + Above60R == 0 ? "0" : System.Math.Round((Above15R * 100 / (Under15R + Above15R + Above60R)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[9].Font.Bold = true;
            grdMain.FooterRow.Cells[9].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[9].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[9].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[10].Text = Under15R + Above15R + Above60R == 0 ? "0" : System.Math.Round((Above60R * 100 / (Under15R + Above15R + Above60R)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[10].Font.Bold = true;
            grdMain.FooterRow.Cells[10].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[10].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[10].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[11].Text = Under15N + Above15N + Above60N == 0 ? "0" : System.Math.Round((Under15N * 100 / (Under15N + Above15N + Above60N)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[11].Font.Bold = true;
            grdMain.FooterRow.Cells[11].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[11].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[11].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[12].Text = Under15N + Above15N + Above60N == 0 ? "0" : System.Math.Round((Above15N * 100 / (Under15N + Above15N + Above60N)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[12].Font.Bold = true;
            grdMain.FooterRow.Cells[12].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[12].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[12].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[13].Text = Under15N + Above15N + Above60N == 0 ? "0" : System.Math.Round((Above60N * 100 / (Under15N + Above15N + Above60N)), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[13].Font.Bold = true;
            grdMain.FooterRow.Cells[13].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[13].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[13].HorizontalAlign = HorizontalAlign.Right;

            grdMain.FooterRow.Cells[14].Text = System.Math.Round(((Under15R + Under15N) * 100 / Total), 2).ToString().Replace(".00", "");
            grdMain.FooterRow.Cells[14].Font.Bold = true;
            grdMain.FooterRow.Cells[14].BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
            grdMain.FooterRow.Cells[14].ForeColor = System.Drawing.Color.White;
            grdMain.FooterRow.Cells[14].HorizontalAlign = HorizontalAlign.Right;
        }

        protected void grdMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMain.PageIndex = e.NewPageIndex;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                if (Session["Item_Expr"] != null)
                    dataView.Sort = Session["Item_Expr"].ToString() + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        protected void grdMain_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Session["Item_Sort"] == null)
            {
                Session["Item_Sort"] = "ASC";
            }
            else
            {
                if (Session["Item_Sort"].ToString() == "ASC")
                    Session["Item_Sort"] = "DESC";
                else
                    Session["Item_Sort"] = "ASC";
            }
            Session["Item_Expr"] = e.SortExpression;
            DataTable dataTable = Session["Item_data"] as DataTable;

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                dataView.Sort = e.SortExpression + " " + Session["Item_Sort"].ToString();

                grdMain.DataSource = dataView;
                grdMain.DataBind();
            }
        }

        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdMain_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridView HeaderGrid = (GridView)sender;
                GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
                TableCell HeaderCell = new TableCell();
                HeaderCell.Text = "";
                HeaderCell.ColumnSpan = 1;
                HeaderCell.BackColor = System.Drawing.Color.White;
                HeaderGridRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "POD Received";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.VerticalAlign = VerticalAlign.Middle;
                HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                HeaderCell.ForeColor = System.Drawing.Color.White;
                HeaderCell.Font.Bold = true;
                HeaderCell.Height = 30;
                HeaderGridRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "POD Not Received";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center; 
                HeaderCell.VerticalAlign = VerticalAlign.Middle;
                HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                HeaderCell.ForeColor = System.Drawing.Color.White;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "";
                HeaderCell.ColumnSpan = 1;
                HeaderCell.BackColor = System.Drawing.Color.White;
                HeaderGridRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Received performance in %";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.VerticalAlign = VerticalAlign.Middle;
                HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                HeaderCell.ForeColor = System.Drawing.Color.White;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);

                HeaderCell = new TableCell();
                HeaderCell.Text = "Not Received performance in %";
                HeaderCell.ColumnSpan = 3;
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.VerticalAlign = VerticalAlign.Middle;
                HeaderCell.BackColor = System.Drawing.Color.FromArgb(13, 126, 164);
                HeaderCell.ForeColor = System.Drawing.Color.White;
                HeaderCell.Font.Bold = true;
                HeaderGridRow.Cells.Add(HeaderCell);

                grdMain.Controls[0].Controls.AddAt(0, HeaderGridRow);

            } 
        }

        protected void grdMain_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Under15" || e.CommandName == "Above15" || e.CommandName == "Above45" || e.CommandName == "Under15NR" || e.CommandName == "Above15NR" || e.CommandName == "Above45NR" || e.CommandName == "Total")
            {
                ReportBO rptBo = new ReportBO();
                rptBo.FromDate = txtFromDate.Text.Trim();
                rptBo.ToDate = txtToDate.Text.Trim();
                rptBo.BranchId = long.Parse(keys["BranchId"].ToString());
                rptBo.type = e.CommandName;
                DataTable dt = rptBo.GEtPODPerformance();

                string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\"+e.CommandName+"PODPerformance_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

                ExportToExcel(dt, filename);
            }
        }
    }
}