﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class BillingCompany : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void LoadGridView()
        {
            BillingCompanyBO optbo = new BillingCompanyBO();
            optbo.SearchText = txtSearch.Text.Trim();
            DataTable dt = optbo.GetBillingCompanyDetails();
            grdView.DataSource = dt;
            grdView.DataBind();
            lblTotal.Text = "Total Records :" + dt.Rows.Count.ToString();
        }

        protected void 
            EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            BillingCompanyBO optbo = new BillingCompanyBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    ClearView();
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtName.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Name = txtName.Text.Trim();
                    optbo.Srl = long.Parse(txtHiddenId.Value);
                    if (optbo.isBillingCompanyAlreadyExist() == true)
                    {
                        SetPanelMsg("Company Name Already Exists", true, 0);
                        txtName.Focus();
                        return;
                    }

                    optbo.Address1 = txtAddress1.Text.Trim();
                    optbo.Address2 = txtAddress2.Text.Trim();
                    optbo.Address3 = txtAddress3.Text.Trim();
                    optbo.landline = txtLandline.Text.Trim();
                    optbo.mobile = txtMobile.Text.Trim();
                    optbo.emailID = txtEmailID.Text.Trim();
                    optbo.CST = txtCSTNo.Text.Trim();
                    optbo.VAT = txtVAT.Text.Trim();
                    optbo.vendor = txtVendor.Text.Trim();
                    optbo.TIN = txtTIN.Text.Trim();
                    optbo.ServiceTaxno = txtServiceTaxNo.Text.Trim();
                    optbo.creditdays = txtCreditDays.Text.Trim() == "" ? 0 : long.Parse(txtCreditDays.Text);
                    optbo.creditLimit = txtCreditLimit.Text.Trim() == "" ? 0 : decimal.Parse(txtCreditLimit.Text.Trim());
                    optbo.isActive = chkActive.Checked;
                    optbo.shortName = txtShortName.Text.Trim();
                    optbo.billType = txtBillType.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());

                    if (Session["FileUpload1"] != null)
                    {
                        flCopy = (FileUpload)Session["FileUpload1"];
                    }
                    byte[] Image = null;
                    //if (flCopy.HasFile)
                    //{
                    //    Image = new byte[flCopy.PostedFile.ContentLength];
                    //    HttpPostedFile UploadedImage = flCopy.PostedFile;
                    //    UploadedImage.InputStream.Read(Image, 0, (int)flCopy.PostedFile.ContentLength);
                    //}
                    if (Session["ImageBytes"] != null)
                    {
                        Image = (byte[])Session["ImageBytes"];
                    }
                    optbo.logo = Image;

                    long cnt = optbo.InsertBillingCompany();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Billing Company Created Successfully", true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Billing Company Details Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    SetPanelMsg("", false, 0);
                    int cnt1 = 0;
                    foreach (GridViewRow row in grdView.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cnt1 = cnt1 + 1;
                                break;
                            }
                        }
                    }
                    if (cnt1 > 0)
                    {
                        popDelPacklist.Show();
                    }
                    else
                    {
                        SetPanelMsg("Please select atleast one record to delete", true, 0);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void ClearView()
        {
            txtName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtAddress3.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLandline.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtCSTNo.Text = string.Empty;
            txtCreditDays.Text = string.Empty;
            txtCreditLimit.Text = string.Empty;
            txtVAT.Text = string.Empty;
            txtTIN.Text = string.Empty;
            txtServiceTaxNo.Text = string.Empty;
            txtVendor.Text = string.Empty;
            chkActive.Checked = true;
            txtShortName.Text = string.Empty;
            txtBillType.Text = string.Empty;
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                imgLogo.ImageUrl = "";
                txtHiddenId.Value = keys["Srl"].ToString();
                txtName.Text = keys["Name"].ToString();
                txtAddress1.Text = keys["Address1"].ToString();
                txtAddress2.Text = keys["Address2"].ToString();
                txtAddress3.Text = keys["Address3"].ToString();
                txtLandline.Text = keys["Landline"].ToString();
                txtMobile.Text = keys["Mobile"].ToString();
                txtEmailID.Text = keys["EmailId"].ToString();
                txtVendor.Text = keys["VendorCode"].ToString();
                txtCSTNo.Text = keys["CSTNo"].ToString();
                txtVAT.Text = keys["VATNo"].ToString();
                txtTIN.Text = keys["TINNo"].ToString();
                txtServiceTaxNo.Text = keys["ServiceTaxNo"].ToString();
                txtCreditDays.Text = keys["CreditDays"].ToString();
                txtCreditLimit.Text = keys["CreditLimit"].ToString();
                chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                txtBillType.Text = keys["BillingType"].ToString();
                txtShortName.Text = keys["ShortName"].ToString();
                if (keys["Logo"] != null && keys["Logo"].ToString().Trim() != string.Empty)
                {
                    byte[] bytes = (byte[])keys["Logo"];
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    imgLogo.ImageUrl = "data:image/png;base64," + base64String;
                }
                ShowViewByIndex(0);
                txtName.Focus();
            }

        }

        protected void grdView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

        protected void grdView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        LoadGridView();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private long Delete()
        {
            BillingCompanyBO OptBO = new BillingCompanyBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdView.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteBillingCompany();
            }

            return srl;
        }

        protected void lnkBtnAttachFiles_Click(object sender, EventArgs e)
        {
            popAttachFIles.Show();
            flCopy.Focus();
        }

        protected void btnFileCancel_Click(object sender, EventArgs e)
        {
            popAttachFIles.Hide();
        }

        protected void btnSaveFiles_Click(object sender, EventArgs e)
        {
            popAttachFIles.Show();
            //BranchBO OptBO = new BranchBO();
            //byte[] Image = null;
            if (FileUpload2.HasFile)
            {
                Session["FileUpload1"] = FileUpload2;
                //flCopy = FileUpload2;
                //Image = new byte[FileUpload2.PostedFile.ContentLength];
                //HttpPostedFile UploadedImage = FileUpload2.PostedFile;
                //UploadedImage.InputStream.Read(Image, 0, (int)FileUpload2.PostedFile.ContentLength);
            }
            //OptBO.CompanyLogo = Image;
            //OptBO.CompanyID = long.Parse(Session["CompanyID"].ToString());
            //long cnt= OptBO.Insert_Update_Company_Logo();
            btnconfirmFile_Click(btnconfirmFile, EventArgs.Empty);
            popAttachFIles.Hide();
        }

        protected void btnconfirmFile_Click(object sender, EventArgs e)
        {
            byte[] Image = null;
            if (Session["FileUpload1"] != null)
            {
                flCopy = (FileUpload)Session["FileUpload1"];
            }

            if (flCopy.HasFile)
            {
                Image = new byte[flCopy.PostedFile.ContentLength];
                HttpPostedFile UploadedImage = flCopy.PostedFile;
                UploadedImage.InputStream.Read(Image, 0, (int)flCopy.PostedFile.ContentLength);
                Session["ImageBytes"] = Image;
                string base64String = Convert.ToBase64String(Image, 0, Image.Length);
                imgLogo.ImageUrl = "data:image/png;base64," + base64String;
            }
        }
    }
}