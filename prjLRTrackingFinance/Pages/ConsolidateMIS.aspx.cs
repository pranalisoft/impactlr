﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ConsolidateMIS : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillDropDowns()
        {
            cmbBranch.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.Items.Add(new ListItem("", "-1"));
            BillingCompanyBO billingBO = new BillingCompanyBO();
            billingBO.SearchText = string.Empty;
            DataTable dt = billingBO.GetBillingCompanyDetails();
            cmbBranch.DataSource = dt;
            cmbBranch.DataTextField = "Name";
            cmbBranch.DataValueField = "Srl";
            cmbBranch.DataBind();
            cmbBranch.SelectedIndex = 0;
        }

        private void LoadGridView()
        {
            LRBO optBO = new LRBO();
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.BranchID = long.Parse(cmbBranch.SelectedValue);
            optBO.LRWise = rdbDatewiseLRWise.Checked;
            DataTable dt = optBO.LRMISConsolidateReport();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalRecords.Text = dt.Rows.Count.ToString();

            if (rdbDatewiseLRWise.Checked)
            {
                grdPacklistView.FooterRow.Cells[4].Text = "Total";
                grdPacklistView.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;
                grdPacklistView.FooterRow.Cells[4].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[4].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleSelling = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Selling"));
                grdPacklistView.FooterRow.Cells[5].Text = totalVehicleSelling.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[5].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[5].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleBuying = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Buying"));
                grdPacklistView.FooterRow.Cells[6].Text = totalVehicleBuying.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[6].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[6].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleMargin = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Margin"));
                grdPacklistView.FooterRow.Cells[7].Text = totalVehicleMargin.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[7].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[7].BackColor = System.Drawing.Color.Yellow;

                if (grdPacklistView.FooterRow.Cells[5].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[5].Text = "0";
                if (grdPacklistView.FooterRow.Cells[6].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[6].Text = "0";
                if (grdPacklistView.FooterRow.Cells[6].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[7].Text = "0";

                if (decimal.Parse(grdPacklistView.FooterRow.Cells[5].Text.Trim()) > 0)
                {
                    grdPacklistView.FooterRow.Cells[8].Text = ((totalVehicleMargin.Value * 100) / totalVehicleSelling.Value).ToString("F2");
                    grdPacklistView.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                    grdPacklistView.FooterRow.Cells[8].Font.Bold = true;
                    grdPacklistView.FooterRow.Cells[8].BackColor = System.Drawing.Color.Yellow;
                }
            }
            else
            {
                grdPacklistView.FooterRow.Cells[2].Text = "Total";
                grdPacklistView.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;
                grdPacklistView.FooterRow.Cells[2].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[2].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleSelling = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Selling"));
                grdPacklistView.FooterRow.Cells[3].Text = totalVehicleSelling.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[3].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[3].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleBuying = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Buying"));
                grdPacklistView.FooterRow.Cells[4].Text = totalVehicleBuying.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[4].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[4].BackColor = System.Drawing.Color.Yellow;

                decimal? totalVehicleMargin = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Margin"));
                grdPacklistView.FooterRow.Cells[5].Text = totalVehicleMargin.Value.ToString("N2");
                grdPacklistView.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                grdPacklistView.FooterRow.Cells[5].Font.Bold = true;
                grdPacklistView.FooterRow.Cells[5].BackColor = System.Drawing.Color.Yellow;

                if (grdPacklistView.FooterRow.Cells[3].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[5].Text = "0";
                if (grdPacklistView.FooterRow.Cells[4].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[6].Text = "0";
                if (grdPacklistView.FooterRow.Cells[5].Text.Trim() == string.Empty) grdPacklistView.FooterRow.Cells[7].Text = "0";

                if (decimal.Parse(grdPacklistView.FooterRow.Cells[3].Text.Trim()) > 0)
                {
                    grdPacklistView.FooterRow.Cells[6].Text = ((totalVehicleMargin.Value * 100) / totalVehicleSelling.Value).ToString("F2");
                    grdPacklistView.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                    grdPacklistView.FooterRow.Cells[6].Font.Bold = true;
                    grdPacklistView.FooterRow.Cells[6].BackColor = System.Drawing.Color.Yellow;
                }
            }
        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            //RegisterDateTextBox();
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                rdbDatewiseLRWise.Checked = true;
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                FillDropDowns();
                LoadGridView();
                btnSearch.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
            btnSearch.Focus();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\LRMISConsolidate_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            LRBO OptBO = new LRBO();
            OptBO.Fromdate = txtFromDate.Text.Trim();
            OptBO.ToDate = txtToDate.Text.Trim();
            OptBO.BranchID = long.Parse(cmbBranch.SelectedValue);
            OptBO.LRWise = rdbDatewiseLRWise.Checked;
            DataTable dt = OptBO.LRMISConsolidateReport();

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                   string tot = "Total";
                    

                    decimal? totalVehicleSelling = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Selling"));                    

                    decimal? totalVehicleBuying = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Vehicle Buying"));                    

                    decimal? totalVehicleMargin = dt.AsEnumerable().Sum(row => row.Field<decimal?>("Margin"));

                    string perStr = "0";
                    decimal per = 0;
                    if (totalVehicleSelling.Value > 0)                   
                    {
                        perStr = ((totalVehicleMargin.Value * 100) / totalVehicleSelling.Value).ToString("F2");
                        per = decimal.Parse(perStr);
                    }

                    dt.Rows.Add();
                    dt.AcceptChanges();
                    int Rcnt=dt.Rows.Count-1;
                    if (rdbDatewiseLRWise.Checked)
                    {
                        dt.Rows[Rcnt][4] = tot;
                        dt.Rows[Rcnt][5] = totalVehicleSelling;
                        dt.Rows[Rcnt][6] = totalVehicleBuying;
                        dt.Rows[Rcnt][7] = totalVehicleMargin;
                        dt.Rows[Rcnt][8] = per;
                    }
                    else
                    {
                        dt.Rows[Rcnt][2] = tot;
                        dt.Rows[Rcnt][3] = totalVehicleSelling;
                        dt.Rows[Rcnt][4] = totalVehicleBuying;
                        dt.Rows[Rcnt][5] = totalVehicleMargin;
                        dt.Rows[Rcnt][6] = per;
                    }
                    dt.AcceptChanges();
                }
            }
            DataSet ds = new DataSet();
            //if (cmbBranch.SelectedIndex == 0)
            //{
            //    List<DataTable> result = dt.AsEnumerable()
            //                            .GroupBy(x => x.Field<string>("BranchName"))
            //                            .Select(grp => grp.CopyToDataTable())
            //                            .ToList();
            //    for (int i = 0; i < result.Count; i++)
            //    {
            //        result[i].TableName = result[i].Rows[0]["BranchName"].ToString();
            //        ds.Tables.Add(result[i]);
            //    }
            //}
            //else
                ds.Tables.Add(dt);

            ExportToExcel(ds, filename);
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            btnSearch.Focus();
        }

        protected void grdPacklistView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (rdbDatewiseLRWise.Checked)
                {
                    for (int i = 5; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
                else
                {
                    for (int i = 3; i < e.Row.Cells.Count; i++)
                    {
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                    }
                }
            }
        }
    }
}