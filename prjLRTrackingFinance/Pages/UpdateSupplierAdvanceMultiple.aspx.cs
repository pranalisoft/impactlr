﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateSupplierAdvanceMultiple : System.Web.UI.Page
    {
        #region "Procedure"
        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        private void ResetGrid()
        {
            DataTable dtItem = CommonTypes.CreateDataTable("LRDate,LRNo,ID,Srl,CashAmount,FuelAmount,PaidCashAmount,PaidFuelAmount,BalCashAmount,BalFuelAmount", "dtLRGrid");            
            grdPaymentSupl.DataSource = dtItem;
            grdPaymentSupl.DataBind();            
        }

        private void clearAll()
        {
            HFCode.Value = "0";
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtAmount.Text = txtchqdt.Text = TxtChqNo.Text = txtCashAmount.Text = txtFuelAmount.Text = string.Empty;
            if (cmbSupl.Items.Count > 0) cmbSupl.SelectedIndex = 0;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
            //cmbLR.DataSource = null;
            //cmbLR.DataBind();
            //cmbLR.Items.Clear();
            //cmbLR.Text = string.Empty;
            ResetGrid();
            cmbPaymentMode.SelectedIndex = 4;
            cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            cmbSupl.Focus();
        }

        private void clearPayment()
        {
            txtAmount.Text = txtchqdt.Text = TxtChqNo.Text = String.Empty;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
            cmbPaymentMode.SelectedIndex = 4;
            cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
        }

        private void FillComboSupplier()
        {
            cmbSupl.Items.Clear();
            cmbSupl.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillSupplierComboAdvance();
            cmbSupl.DataSource = dt;
            cmbSupl.DataTextField = "Name";
            cmbSupl.DataValueField = "Srl";
            cmbSupl.DataBind();
            cmbSupl.SelectedIndex = 0;
        }

        private void FillLR()
        {
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            OptBO.SupplierID = cmbSupl.SelectedIndex == null ? 0 : long.Parse(cmbSupl.SelectedValue);
            dt = OptBO.FillLRAdvance();
            grdPaymentSupl.DataSource = dt;
            grdPaymentSupl.DataBind();

            grdPaymentSupl.FooterRow.Cells[1].Text = "Total";
            grdPaymentSupl.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
            grdPaymentSupl.FooterRow.Cells[1].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[1].BackColor = System.Drawing.Color.Yellow;

            decimal? totalCashAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("CashAmount"));
            grdPaymentSupl.FooterRow.Cells[2].Text = totalCashAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[2].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[2].BackColor = System.Drawing.Color.Yellow;

            decimal? totalFuelAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("FuelAmount"));
            grdPaymentSupl.FooterRow.Cells[3].Text = totalFuelAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[3].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[3].BackColor = System.Drawing.Color.Yellow;

            decimal? PadCashAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("PaidCashAmount"));
            grdPaymentSupl.FooterRow.Cells[4].Text = PadCashAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[4].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[4].BackColor = System.Drawing.Color.Yellow;

            decimal? PadFuelAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("PaidFuelAmount"));
            grdPaymentSupl.FooterRow.Cells[5].Text = PadFuelAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[5].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[5].BackColor = System.Drawing.Color.Yellow;

            decimal? BalCashAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("BalCashAmount"));
            grdPaymentSupl.FooterRow.Cells[6].Text = BalCashAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[6].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[6].BackColor = System.Drawing.Color.Yellow;

            decimal? BalFuelAmount = dt.AsEnumerable().Sum(row => row.Field<decimal?>("BalFuelAmount"));
            grdPaymentSupl.FooterRow.Cells[7].Text = BalFuelAmount.Value.ToString("N2");
            grdPaymentSupl.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
            grdPaymentSupl.FooterRow.Cells[7].Font.Bold = true;
            grdPaymentSupl.FooterRow.Cells[7].BackColor = System.Drawing.Color.Yellow;
        }

        private void fillBank()
        {
            cmbBank.Items.Clear();
            CommonBO bankbo = new CommonBO();
            DataTable dt = bankbo.FillBankDetails();
            cmbBank.DataSource = dt;
            cmbBank.DataTextField = "BankName";
            cmbBank.DataValueField = "BankName";
            cmbBank.DataBind();
        }

        private void CalculateAmount(int Rindex, int Cindex)
        {
            txtCashAmount.Text = "0";
            txtFuelAmount.Text = "0";
            for (int i = 0; i < grdPaymentSupl.Rows.Count; i++)
            {
                TextBox txtCashAmountgrdI = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjCashAmount");
                TextBox txtFuelAmountgrdI = (TextBox)grdPaymentSupl.Rows[i].FindControl("txtAdjFuelAmount");
                if (txtCashAmountgrdI.Text.Trim() == string.Empty) txtCashAmountgrdI.Text = "0";
                if (txtFuelAmountgrdI.Text.Trim() == string.Empty) txtFuelAmountgrdI.Text = "0";

                if (decimal.Parse(txtCashAmountgrdI.Text) > 0) txtCashAmount.Text = (decimal.Parse(txtCashAmount.Text) + decimal.Parse(txtCashAmountgrdI.Text)).ToString("F2");
                if (decimal.Parse(txtFuelAmountgrdI.Text) > 0) txtFuelAmount.Text = (decimal.Parse(txtFuelAmount.Text) + decimal.Parse(txtFuelAmountgrdI.Text)).ToString("F2");
            }
            txtAmount.Text = (decimal.Parse(txtCashAmount.Text) + decimal.Parse(txtFuelAmount.Text)).ToString("F2");

            TextBox txtCashAmountgrd = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtAdjCashAmount");
            TextBox txtFuelAmountgrd = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtAdjFuelAmount");

            if (Cindex == 8)
                txtCashAmountgrd.Focus();
            else
                txtFuelAmountgrd.Focus();
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterDateTextBox();
            if (!IsPostBack)
            {
                clearAll();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
                FillComboSupplier();
                cmbSupl.Focus();
            }

        }

        protected void cmbSupl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillLR();
                cmbSupl.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void cmbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
            {
                trBank.Visible = true;
                trChqNo.Visible = true;
                lblChqDDNo.Visible = true;
                TxtChqNo.Visible = true;
                cmbBank.Visible = true;
                lblBank.Visible = true;
                CustomBank.Enabled = true;
                reqFVChqNo.Enabled = true;
            }
            else
            {
                trBank.Visible = false;
                trChqNo.Visible = false;
                lblChqDDNo.Visible = false;
                TxtChqNo.Visible = false;
                cmbBank.Visible = false;
                lblBank.Visible = false;
                CustomBank.Enabled = false;
                reqFVChqNo.Enabled = false;
            }
            switch (cmbPaymentMode.SelectedValue)
            {
                case "1":
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "2":
                    lblChqDDNo.Text = "Chq No.";
                    lblchqddDate.Text = "Chq.Date";
                    break;
                case "3":
                    lblChqDDNo.Text = "DD No.";
                    lblchqddDate.Text = "DD Date";
                    break;
                case "4":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "5":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "6":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                default:
                    break;
            }
            txtchqdt.Focus();
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Payment from Supplier";                   
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;
                    
                    long cnt = 0;
                    SupplierPaymentBO custbo = new SupplierPaymentBO();

                    custbo.tranDate = txtDate.Text.Trim();
                    custbo.SupplierID = long.Parse(cmbSupl.SelectedValue);
                    custbo.refNo = String.Empty;
                    custbo.remarks = String.Empty;
                    custbo.userID = long.Parse(Session["EID"].ToString());
                    custbo.BankName = cmbBank.Text.Trim();
                    custbo.PaymentMode = cmbPaymentMode.SelectedValue;
                    custbo.ChqNo = TxtChqNo.Text.Trim();
                    custbo.ChqDate = txtchqdt.Text.Trim();
                    custbo.CardNetBankingDetails = TxtChqNo.Text.Trim();

                    for (int Rindex = 0; Rindex < grdPaymentSupl.Rows.Count; Rindex++)
                    {
                        DataKey keys = grdPaymentSupl.DataKeys[Rindex];
                        TextBox txtCashAmountgrd = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtAdjCashAmount");
                        TextBox txtFuelAmountgrd = (TextBox)grdPaymentSupl.Rows[Rindex].FindControl("txtAdjFuelAmount");

                        if (txtCashAmountgrd.Text.Trim() == string.Empty) txtCashAmountgrd.Text = "0";
                        if (txtFuelAmountgrd.Text.Trim() == string.Empty) txtFuelAmountgrd.Text = "0";

                        if (decimal.Parse(txtCashAmountgrd.Text) > 0 || decimal.Parse(txtFuelAmountgrd.Text) > 0)
                        {
                            custbo.CashAmount = decimal.Parse(txtCashAmountgrd.Text);
                            custbo.FuelAmount = decimal.Parse(txtFuelAmountgrd.Text);
                            custbo.totalAmount = decimal.Parse(txtCashAmountgrd.Text) + decimal.Parse(txtFuelAmountgrd.Text);
                            custbo.Srl = long.Parse(keys["Srl"].ToString());

                            cnt = cnt + custbo.UpdatePaymentHeader();
                        }
                    }

                    if (cnt > 0)
                    {
                        MsgPanel.Message = "Record Updated successfully.";
                        MsgPanel.DispCode = 1;
                        FillComboSupplier();
                        cmbSupl_SelectedIndexChanged(cmbSupl, EventArgs.Empty);
                        clearAll();
                        cmbSupl.Focus();
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    FillComboSupplier();
                    clearAll();
                    break;
                case CommonTypes.EntryFormCommand.None:

                    break;
                default:
                    break;
            }
        }

        protected void txtCashAmount_TextChanged(object sender, EventArgs e)
        {
            //CalculateAmount();
            //txtCashAmount.Focus();
        }

        protected void txtFuelAmount_TextChanged(object sender, EventArgs e)
        {
            //CalculateAmount();
            //txtFuelAmount.Focus();
        }

        protected void txtAdjCashAmount_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int Rindex = gvRow.RowIndex;
            CalculateAmount(Rindex, 8);
        }

        protected void txtAdjFuelAmount_TextChanged(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int Rindex = gvRow.RowIndex;
            CalculateAmount(Rindex, 9);
        }
        #endregion
    }
}