﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessObjects.BO;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateLRStatusFreightTiger : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            LRBO lrbo = new LRBO();
            DataTable dtUpdate = new DataTable("dtUpdate");
            dtUpdate.Columns.Add("TripId");
            dtUpdate.Columns.Add("LastLocation");
            dtUpdate.Columns.Add("TranDatetime");
            dtUpdate.AcceptChanges();
            DataTable dt = lrbo.GetTripsForUpdation();
            string lastknownLocation = "";
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lastknownLocation = GetTripDetails(dt.Rows[i][0].ToString());
                if (!string.IsNullOrEmpty(lastknownLocation))
                {
                    DataRow dr;
                    String[] lastlocdatetime = lastknownLocation.Split(new[] { "#dttime#" }, StringSplitOptions.None);


                    dr = dtUpdate.NewRow();
                    dr["TripId"] = dt.Rows[i][0].ToString();
                    dr["LastLocation"] = lastlocdatetime[0];
                    dr["TranDatetime"] = lastlocdatetime[1];
                    dtUpdate.Rows.Add(dr); 
                }
               
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtUpdate);
            string ItemXML = ds.GetXml();

            LRBO optbo = new LRBO();
            optbo.itemXML = ItemXML;
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            long cnt = optbo.UpdateStatusFromFreightTiger();


            SetPanelMsg("Record updated successfully", true, 1);
        }

        private string GetTripDetails(string tripId)
        {
            string accessKey = ConfigurationManager.AppSettings["FreightTigerAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["FreightTigerBaseAddress"].ToString();

            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.BaseAddress = baseAddress;
                webClient.Headers.Set("accept", "application/json");
                webClient.Headers.Set("content-type", "application/json");



                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                StringBuilder urlStringBuilder = new StringBuilder("/saas/trip/id/" + tripId);
                urlStringBuilder.Append(string.Format("?accessKey={0}", accessKey));
                var result = webClient.DownloadString(urlStringBuilder.ToString());
                var result1 = "";
                var result2 = "";
                int pos = result.IndexOf("address");
                //var responseObject = javaScriptSerializer.DeserializeObject<Dictionary<string, dynamic>>(result);
                var offset = result.IndexOf(':', pos);
                var endOffset = result.IndexOf('"', result.IndexOf('"', pos+2)+3) - 1;

                result1 = result.Substring(offset + 1, endOffset - offset);

                result1 = result1.Replace("\"", string.Empty);


                int pos2 = result.IndexOf("recorded_at");
                if (pos2>0)
                {
                    offset = result.IndexOf(':', pos2);
                    result2 = result.Substring(offset + 1, 20);

                    result2 = result2.Replace("\"", string.Empty);
                    return result1 + "#dttime#" + result2;
                }
                else
                {
                    
                    return "";
                }
               

                
                //var responseObject = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(result);

            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                lblMsg.Text = msg;
                pnlMsg.Visible = true;
                if (code == 1)
                {
                    pnlMsg.Style.Add("border", "solid 1px #336600");
                    pnlMsg.Style.Add("color", "black");
                    pnlMsg.Style.Add("background-color", "#9EDC7F");
                }
                else
                {
                    pnlMsg.Style.Add("border", "solid 1px #CE180E");
                    pnlMsg.Style.Add("color", "white");
                    pnlMsg.Style.Add("background-color", "#D20000");
                }
            }
            else
            {
                lblMsg.Text = string.Empty;
                pnlMsg.Visible = false;
            }
        }
    }
}