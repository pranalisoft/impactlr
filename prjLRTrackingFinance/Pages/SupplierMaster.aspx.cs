﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class SupplierMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                ShowViewByIndex(1);
                txtSearch.Text = string.Empty;
                LoadGridView();
                txtSearch.Focus();
            }
        }

        private void FillComboSearchedBy()
        {
            try
            {
                cmbSearchedBy.DataSource = null;
                cmbSearchedBy.Items.Clear();
                cmbSearchedBy.Items.Add(new ListItem("", "-1"));
                DataTable dt = new UserDAO().GetUserData("N", "");
                cmbSearchedBy.DataSource = dt;
                cmbSearchedBy.DataTextField = "UserName";
                cmbSearchedBy.DataValueField = "Srl";
                cmbSearchedBy.DataBind();
                cmbSearchedBy.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
            }
        }

        private void FillComboSuplType()
        {
            try
            {
                cmbSuplType.DataSource = null;
                cmbSuplType.Items.Clear();
                cmbSuplType.Items.Add(new ListItem("", "-1"));
                DataTable dt = new SupplierTypeDAO().GetSupplierTypes("","N");
                cmbSuplType.DataSource = dt;
                cmbSuplType.DataTextField = "Name";
                cmbSuplType.DataValueField = "Srl";
                cmbSuplType.DataBind();
                cmbSuplType.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
            }
        }

        private void FillComboApprovedBy()
        {
            try
            {
                cmbApprovedBy.DataSource = null;
                cmbApprovedBy.Items.Clear();
                cmbApprovedBy.Items.Add(new ListItem("", "-1"));
                DataTable dt = new UserDAO().GetUserData("N", "");
                cmbApprovedBy.DataSource = dt;
                cmbApprovedBy.DataTextField = "UserName";
                cmbApprovedBy.DataValueField = "Srl";
                cmbApprovedBy.DataBind();
                cmbApprovedBy.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
            }
        }

        private void LoadGridView()
        {
            SupplierBO optbo = new SupplierBO();
            optbo.SearchText = txtSearch.Text.Trim();
            DataTable dt = optbo.GetSupplierDetails();
            grdView.DataSource = dt;
            grdView.DataBind();
            lblTotal.Text = "Total Records :" + dt.Rows.Count.ToString();
        }

        private void LoadGridViewDestinationsAndVehicleTypes(long SupplierSrl)
        {
            SupplierBO optbo = new SupplierBO();
            optbo.Srl = SupplierSrl;
            DataTable dt = optbo.SupplierDestinations();
            grdViewDestinations.DataSource = dt;
            grdViewDestinations.DataBind();

            DataTable dt1 = optbo.SupplierVehicleType();
            grdViewVehicleType.DataSource = dt1;
            grdViewVehicleType.DataBind();
        }

        private string GenerateXML(GridView grd)
        {
            string itemXML = "";
            for (int i = 0; i < grd.Rows.Count; i++)
            {
                DataKey keys = grd.DataKeys[i];
                CheckBox chk = (CheckBox)grd.Rows[i].FindControl("chkSelect");
                if (chk.Checked)
                {
                    itemXML = itemXML + "<Record><Srl>" + keys["Srl"].ToString() + "</Srl></Record>";
                }
            }

            if (!string.IsNullOrEmpty(itemXML))
                itemXML = "<NewDataSet>" + itemXML + "</NewDataSet>";

            return itemXML;
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);
            SupplierBO optbo = new SupplierBO();
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    FillComboSearchedBy();
                    FillComboApprovedBy();
                    FillComboSuplType();
                    ClearView();
                    LoadGridViewDestinationsAndVehicleTypes(0);
                    ShowViewByIndex(0);
                    txtHiddenId.Value = "0";
                    txtName.Focus();
                    break;
                case CommonTypes.EntryFormCommand.Save:

                    optbo.Name = txtName.Text.Trim();
                    optbo.Srl = long.Parse(txtHiddenId.Value);
                    if (optbo.isSupplierAlreadyExist() == true)
                    {
                        SetPanelMsg("Supplier Name Already Exists", true, 0);
                        txtName.Focus();
                        return;
                    }
                    if (chkHoldPayment.Checked && txtHoldRemarks.Text.Trim()=="")
                    {
                        SetPanelMsg("Please Enter Hold Remarks", true, 0);
                        txtHoldRemarks.Focus();
                        return;
                    }

                    if (!chkHoldPayment.Checked) txtHoldRemarks.Text = "";

                    optbo.Address1 = txtAddress1.Text.Trim();
                    optbo.Address2 = txtAddress2.Text.Trim();
                    optbo.Address3 = txtAddress3.Text.Trim();
                    optbo.landline = txtLandline.Text.Trim();
                    optbo.mobile = txtMobile.Text.Trim();
                    optbo.emailID = txtEmailID.Text.Trim();
                    optbo.CST = txtCSTNo.Text.Trim();
                    optbo.VAT = "";//txtVAT.Text.Trim();
                    optbo.vendor = txtVendor.Text.Trim();
                    optbo.TIN = "";//txtTIN.Text.Trim();
                    optbo.ServiceTaxno = "";//txtServiceTaxNo.Text.Trim();
                    optbo.creditdays = txtCreditDays.Text.Trim() == "" ? 0 : long.Parse(txtCreditDays.Text);
                    optbo.creditLimit = txtCreditLimit.Text.Trim() == "" ? 0 : decimal.Parse(txtCreditLimit.Text.Trim());
                    optbo.isActive = chkActive.Checked;
                    optbo.GLCode = txtGL.Text.Trim();
                    optbo.AdvancePer = txtAdvancePer.Text==""?0: decimal.Parse(txtAdvancePer.Text);
                    optbo.TDSPer = txtTDSPer.Text == "" ? 0 : decimal.Parse(txtTDSPer.Text);

                    optbo.VendorType = cmbVendorType.SelectedValue;
                    optbo.SearchedBy = long.Parse(cmbSearchedBy.SelectedValue);
                    optbo.ApprovedBy = long.Parse(cmbApprovedBy.SelectedValue);
                    optbo.FirmName = txtFirmName.Text;
                    optbo.ContactPersonName = txtContactPerson.Text;
                    optbo.AadharNo = txtAadharNo.Text;
                    optbo.PAN = txtPAN.Text;
                    optbo.BankName = txtBank.Text;
                    optbo.BankBranch = txtBankBranch.Text;
                    optbo.AccountNo = txtAccountNo.Text;
                    optbo.IFSC = txtIFSC.Text;

                    optbo.DestinationXML = GenerateXML(grdViewDestinations);
                    optbo.VehicleTypeXML = GenerateXML(grdViewVehicleType);

                    if (cmbAgreementDone.SelectedIndex == 0)
                        optbo.AgreementDone = false;
                    else
                        optbo.AgreementDone = true;
                    optbo.AgreementDate = txtAgreementDate.Text.Trim();
                    optbo.ValidTillDate = txtValidTillDate.Text.Trim();
                    optbo.CreatedBy = long.Parse(Session["EID"].ToString());
                    optbo.SupplierType = int.Parse(cmbSuplType.SelectedValue);
                    optbo.HoldPayment = chkHoldPayment.Checked;
                    optbo.HoldRemarks = txtHoldRemarks.Text.Trim();

                    long cnt = optbo.InsertSupplier();
                    if (cnt > 0)
                    {
                        if (long.Parse(txtHiddenId.Value) == 0)
                        {
                            SetPanelMsg("Supplier Created Successfully", true, 1);
                        }
                        else
                        {
                            SetPanelMsg("Supplier Details Updated Successfully", true, 1);
                        }
                        LoadGridView();
                        ShowViewByIndex(1);
                    }
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    SetPanelMsg("", false, 0);
                    int cnt1 = 0;
                    foreach (GridViewRow row in grdView.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                            if (rowcheck.Checked)
                            {
                                cnt1 = cnt1 + 1;
                                break;
                            }
                        }
                    }
                    if (cnt1 > 0)
                    {
                        popDelPacklist.Show();
                    }
                    else
                    {
                        SetPanelMsg("Please select atleast one record to delete", true, 0);
                    }

                    break;
                case CommonTypes.EntryFormCommand.None:
                    LoadGridView();
                    ShowViewByIndex(1);
                    break;
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void ClearView()
        {
            txtName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtAddress3.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtLandline.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtCSTNo.Text = string.Empty;
            txtCreditDays.Text = string.Empty;
            txtCreditLimit.Text = string.Empty;
            txtTDSPer.Text = string.Empty;
            //txtTIN.Text = string.Empty;
            //txtServiceTaxNo.Text = string.Empty;
            txtVendor.Text = string.Empty;
            chkActive.Checked = true;
            txtGL.Text = string.Empty;
            txtAdvancePer.Text = string.Empty;
            cmbAgreementDone.SelectedIndex = 0;
            txtAgreementDate.Text = string.Empty;
            txtValidTillDate.Text = string.Empty;

            cmbVendorType.SelectedIndex = 0;
            cmbSearchedBy.SelectedIndex = 0;
            cmbApprovedBy.SelectedIndex = 0;
            txtFirmName.Text = string.Empty;
            txtContactPerson.Text = string.Empty;
            txtAadharNo.Text = string.Empty;
            txtPAN.Text = string.Empty;
            txtBank.Text = string.Empty;
            txtBankBranch.Text = string.Empty;
            txtAccountNo.Text = string.Empty;
            txtIFSC.Text = string.Empty;
            chkHoldPayment.Checked = false;
            txtHoldRemarks.Text = string.Empty;
        }

        protected void btnMainPg_Click(object sender, EventArgs e)
        {
            Response.Redirect("LRMain.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdView.PageIndex = e.NewPageIndex;
            LoadGridView();
            txtSearch.Focus();
        }

        protected void grdView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Page"))
                return;
            if (e.CommandName.Equals("Sort"))
                return;
            int index = Convert.ToInt32(e.CommandArgument);
            GridView grd = (GridView)e.CommandSource;
            DataKey keys = grd.DataKeys[index];
            GridViewRow row1 = grd.Rows[index];

            if (e.CommandName == "Modify")
            {
                FillComboApprovedBy();
                FillComboSearchedBy();
                FillComboSuplType();
                txtHiddenId.Value = keys["Srl"].ToString();
                LoadGridViewDestinationsAndVehicleTypes(long.Parse(txtHiddenId.Value));
                txtName.Text = keys["Name"].ToString();
                txtAddress1.Text = keys["Address1"].ToString();
                txtAddress2.Text = keys["Address2"].ToString();
                txtAddress3.Text = keys["Address3"].ToString();
                txtLandline.Text = keys["Landline"].ToString();
                txtMobile.Text = keys["Mobile"].ToString();
                txtEmailID.Text = keys["EmailId"].ToString();
                txtVendor.Text = keys["VendorCode"].ToString();
                txtCSTNo.Text = keys["CSTNo"].ToString();
                // txtVAT.Text = keys["VATNo"].ToString();
                // txtTIN.Text = keys["TINNo"].ToString();
                //txtServiceTaxNo.Text = keys["ServiceTaxNo"].ToString();
                txtCreditDays.Text = keys["CreditDays"].ToString();
                txtCreditLimit.Text = keys["CreditLimit"].ToString().Replace(".00","");
                chkActive.Checked = bool.Parse(keys["IsActive"].ToString());
                txtGL.Text = keys["GLCode"].ToString();
                txtAdvancePer.Text = keys["AdvancePer"].ToString();
                txtTDSPer.Text = keys["TDSPer"].ToString();
                if (bool.Parse(keys["AgreementDone"].ToString()))
                    cmbAgreementDone.SelectedIndex = 1;
                else
                    cmbAgreementDone.SelectedIndex = 0;

                cmbVendorType.SelectedValue = keys["VendorType"] == null ? "O" : keys["VendorType"].ToString().Trim() == string.Empty ? "O" : keys["VendorType"].ToString();
                cmbSearchedBy.SelectedValue = keys["SearchedBy"] == null ? "-1" : keys["SearchedBy"].ToString().Trim() == string.Empty ? "-1" : keys["SearchedBy"].ToString();
                cmbApprovedBy.SelectedValue = keys["ApprovedBy"] == null ? "-1" : keys["ApprovedBy"].ToString().Trim() == string.Empty ? "-1" : keys["ApprovedBy"].ToString();
                cmbSuplType.SelectedValue = keys["SupplierType"] == null ? "-1" : keys["SupplierType"].ToString().Trim() == string.Empty ? "-1" : keys["SupplierType"].ToString();
                txtFirmName.Text = keys["FirmName"].ToString();
                txtContactPerson.Text = keys["ContactPersonName"].ToString();
                txtAadharNo.Text = keys["AadharNo"].ToString();
                txtPAN.Text = keys["PAN"].ToString();
                txtBank.Text = keys["BankName"].ToString();
                txtBankBranch.Text = keys["BankBranch"].ToString();
                txtAccountNo.Text = keys["AccountNo"].ToString();
                txtIFSC.Text = keys["IFSC"].ToString();
                
                txtAgreementDate.Text = string.Empty;
                if (!string.IsNullOrEmpty(keys["AgreementDate"].ToString()))
                    txtAgreementDate.Text = DateTime.Parse(keys["AgreementDate"].ToString()).ToString("dd/MM/yyyy");

                if (!string.IsNullOrEmpty(keys["ValidTillDate"].ToString()))
                    txtValidTillDate.Text = DateTime.Parse(keys["ValidTillDate"].ToString()).ToString("dd/MM/yyyy");

                chkHoldPayment.Checked = bool.Parse(keys["HoldPayment"].ToString());
                txtHoldRemarks.Text = keys["HoldRemarks"].ToString().Trim();

                ShowViewByIndex(0);
                txtName.Focus();
            }
            if (e.CommandName == "Doc")
            {
                Session["DocSupplierId"] = keys["Srl"].ToString();
                Response.Redirect("SupplierDocuments.aspx");
            }
        }

        protected void grdView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey keys = ((GridView)sender).DataKeys[e.Row.RowIndex];
                int usedcount = int.Parse(keys["UsedCnt"].ToString());
                if (usedcount > 0)
                {
                    CheckBox chk = e.Row.FindControl("chkSelect") as CheckBox;
                    chk.Enabled = false;
                }
            }
        }

        protected void grdView_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        private void ShowViewByIndex(int index)
        {
            mltVwPacklist.ActiveViewIndex = index;
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    if (Delete() > 0)
                    {
                        txtSearch.Text = string.Empty;
                        LoadGridView();
                        txtSearch.Focus();
                        SetPanelMsg("Record(s) deleted successfully", true, 1);
                    }
                    break;
                case "no":
                    popDelPacklist.Hide();
                    break;
                default:
                    break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        private long Delete()
        {
            SupplierBO OptBO = new SupplierBO();
            string strcode = string.Empty;
            long srl = 0;
            foreach (GridViewRow row in grdView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox rowcheck = (CheckBox)row.FindControl("chkSelect");
                    long id = long.Parse(grdView.DataKeys[row.RowIndex].Values["Srl"].ToString());
                    if (!rowcheck.Enabled)
                    {
                        continue;
                    }
                    if (rowcheck.Checked)
                    {
                        if (strcode == "")
                            strcode = id.ToString();
                        else
                            strcode = strcode + "," + id.ToString();
                    }
                }
                else
                {
                    continue;
                }
            }


            if (strcode.Trim() != string.Empty)
            {
                OptBO.Srls = strcode;
                srl = OptBO.DeleteSupplier();
            }

            return srl;
        }
    }
}