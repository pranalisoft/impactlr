﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="AddTripFreightTiger.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.AddTripFreightTiger" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnAddTrip" Text="Add Trip" OnClick="btnAddTrip_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnGet" Text="Get details" OnClick="btnGet_Click" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Button runat="server" ID="btnClose" Text="Close Trip" 
                                onclick="btnClose_Click"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnEditTrip" Text="Edit Trip" 
                                onclick="btnEdit_Click"  />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddTrip" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
