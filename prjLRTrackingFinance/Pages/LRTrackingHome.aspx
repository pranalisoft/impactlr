﻿<%@ Page Title="LR Tracking Home" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="LRTrackingHome.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRTrackingHome" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <link href="/Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Include/JS/layout/jquery-1.7.1.js"></script>
    <%--<script language="javascript">
        $(document).ready(function () {
            var grdSuplBillHeader = $('#<%=grdSuplBill.ClientID %>').clone(true);
            $(grdSuplBillHeader).find("tr:gt(0)").remove();
            $('#<%=grdSuplBill.ClientID %> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", grdSuplBillHeader).css('width', $(this).width().toString() + "px");
            });
            $("#grdSuplBillGHead").append(grdSuplBillHeader);
            $("#grdSuplBillGHead").css('position', 'absolute');
            $("#grdSuplBillGHead").css('top', $('#<%=grdSuplBill.ClientID %>').offset().top);

//            var rows = document.getElementById("<%= grdSuplBill.ClientID %>").rows;
//            alert(rows.length);
//            for (var i = 0; i < rows.length; i++) {
//                if (i == 0) {
//                    alert('hi');
//                    rows[i].style.display = "none";
//                }
//            };

            var grdEwaydBillHeader = $('#<%=grdEwaydBill.ClientID %>').clone(true);
            $(grdEwaydBillHeader).find("tr:gt(0)").remove();
            $('#<%=grdEwaydBill.ClientID %> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", grdEwaydBillHeader).css('width', $(this).width().toString() + "px");
            });
            $("#grdEwaydBillGHead").append(grdEwaydBillHeader);
            $("#grdEwaydBillGHead").css('position', 'absolute');
            $("#grdEwaydBillGHead").css('top', $('#<%=grdEwaydBill.ClientID %>').offset().top);
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Width="810px" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    <asp:Label Text="" ID="lblViewLR" runat="server"></asp:Label>
                </div>
                <div align="center" style="padding: 5px; width: 800px; height: 300px; overflow: auto">
                    <asp:GridView ID="grdViewFUP_LR" runat="server" Width="100%" AutoGenerateColumns="False"
                        AllowPaging="False" EmptyDataText="No Records Found." CssClass="grid" PageSize="10">
                        <Columns>
                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="80px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner" />
                            <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc"
                                ItemStyle-Width="120px">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc"
                                ItemStyle-Width="120px">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="header" />
                        <RowStyle CssClass="row" />
                        <AlternatingRowStyle CssClass="alter_row" />
                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                        <PagerSettings Mode="Numeric" />
                    </asp:GridView>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button" OnClick="btnOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popFUP_ShowLR" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div style="text-align: center; vertical-align: middle; width: 100%;">
                <center>
                    <div>
                        <table width="100%">
                            <tr class="centerPageHeader">
                                <td class="centerPageHeader" style="height: 25px; font-size: 16px; font-weight: bold;
                                    vertical-align: middle;">
                                    <center>
                                        Transport Management System</center>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: White; height: 2px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table style="text-align: center; vertical-align: middle;">
                        <tr>
                            <td>
                                <%--<img src="/Common/images/OBDTrackerIcon.jpg" height="25px" width="35px" />--%>
                            </td>
                            <td style="font-size: 14px; font-weight: bold;">
                                &nbsp<asp:HyperLink ID="hyplOBDMain" runat="server" NavigateUrl="~/Pages/LRMain.aspx"
                                    ForeColor="#39478E">Goto Main Page</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 20%">
                                <asp:Label Text="Total LR Pending For Freight Updation : " ID="Label1" runat="server"
                                    Font-Bold="True" ForeColor="#39478E" Font-Size="14px"></asp:Label>
                                <asp:Label ID="lblLRFUP" Text="0" runat="server" Font-Bold="True" ForeColor="#39478E"
                                    Font-Size="14px"></asp:Label>
                            </td>
                            <td style="width: 30%">
                                <asp:Label Text="Due & Overdue Ewaybills :" ID="Label5" runat="server" Font-Bold="True"
                                    ForeColor="#39478E" Font-Size="14px"></asp:Label>
                                <asp:Label ID="lblDueEwayBillCnt" Text="" runat="server" Font-Bold="True" ForeColor="#39478E"
                                    Font-Size="14px"></asp:Label>
                            </td>
                            <td style="width: 20%">
                                <asp:Label Text="LR Count Pending For Post POD,Approval & Bill" ID="Label3" runat="server"
                                    Font-Bold="True" ForeColor="#39478E" Font-Size="14px"></asp:Label>
                                <asp:Label ID="Label4" Text="" runat="server" Font-Bold="True" ForeColor="#39478E"
                                    Font-Size="14px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <div style="height: 550px; overflow: auto">
                                    <asp:GridView ID="grdViewFUP" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId,BranchName,TotalRecords" EmptyDataText="No records found."
                                        Width="100%" ShowHeaderWhenEmpty="True" OnRowCommand="grdViewFUP_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                            <asp:ButtonField ButtonType="Link" CommandName="ShowLR" DataTextField="TotalRecords"
                                                HeaderText="Count" SortExpression="TotalRecords">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                            </td>
                            <td style="vertical-align: top">
                                <div style="width: 100%">
                                    <%--<div id="grdEwaydBillGHead" style="margin-top: -5px">
                                    </div>--%>
                                    <div style="height: 550px; overflow: auto">
                                        <asp:GridView ID="grdEwaydBill" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="" EmptyDataText="No records found." Width="100%" ShowHeaderWhenEmpty="True"
                                            OnRowCommand="grdSuplBill_RowCommand" OnRowDataBound="grdEwaydBill_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="LOB" HeaderText="LOB" SortExpression="LOB" />
                                                <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo" />
                                                <asp:BoundField DataField="EwayBillNo" HeaderText="Eway Bill No." SortExpression="LRNo">
                                                    <ItemStyle CssClass="ellipsis" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EwayBillDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Eway Bill Date"
                                                    SortExpression="LRNo" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                            <td style="vertical-align: top">
                                <div style="width: 100%">
                                    <%--  <div id="grdSuplBillGHead" style="margin-top: -5px">
                                    </div>--%>
                                    <div style="height: 550px; overflow: auto">
                                        <asp:GridView ID="grdSuplBill" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="BranchId,BranchName,PostPOD,Approval,Bill" EmptyDataText="No records found."
                                            Width="100%" ShowHeaderWhenEmpty="True" OnRowCommand="grdSuplBill_RowCommand">
                                            <Columns>
                                                <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                                <asp:ButtonField ButtonType="Link" CommandName="PostPOD" DataTextField="PostPOD"
                                                    HeaderText="Post POD" SortExpression="PostPOD" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                                <asp:ButtonField ButtonType="Link" CommandName="Approval" DataTextField="Approval"
                                                    HeaderText="Approval" SortExpression="Approval" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                                <asp:ButtonField ButtonType="Link" CommandName="Bill" DataTextField="Bill" HeaderText="Bill"
                                                    SortExpression="Bill" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label Text="Total LR Pending For Vehicle Status Updation : " ID="Label2" runat="server"
                                    Font-Bold="True" ForeColor="#39478E" Font-Size="14px"></asp:Label>
                                <asp:Label ID="lblLRVSU" Text="0" runat="server" Font-Bold="True" ForeColor="#39478E"
                                    Font-Size="14px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label Text="Sales Target Vs Actual For This Month" ID="Label6" runat="server" Font-Bold="True"
                                    ForeColor="#39478E" Font-Size="14px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label Text="POD Overview For This Month" ID="Label8" runat="server" Font-Bold="True"
                                    ForeColor="#39478E" Font-Size="14px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <asp:GridView ID="grdViewVSU" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="BranchId,BranchName,TotalRecords" EmptyDataText="No records found."
                                    Width="100%" ShowHeaderWhenEmpty="True" OnRowCommand="grdViewVSU_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                        <asp:ButtonField ButtonType="Link" CommandName="ShowLR" DataTextField="TotalRecords"
                                            HeaderText="Count" SortExpression="TotalRecords">
                                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </td>
                            <td style="vertical-align: top">
                                <div style="width: 100%">
                                    <%--<div id="grdEwaydBillGHead" style="margin-top: -5px">
                                    </div>--%>
                                    <div style="overflow: auto">
                                        <asp:GridView ID="grdViewManager" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="BranchId,BranchName" EmptyDataText="No records found." Width="100%"
                                            ShowHeaderWhenEmpty="True" OnRowCommand="grdViewManager_RowCommand">
                                            <Columns>
                                                <asp:BoundField DataField="BranchName" HeaderText="LOB" SortExpression="BranchName" />
                                                <asp:BoundField DataField="TargetSale" HeaderText="Target Sale" SortExpression="TargetSale"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="SalesAsOn" HeaderText="Sales As On" SortExpression="SalesAsOn"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="Projection" HeaderText="Projection" SortExpression="Projection"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ShortFall" HeaderText="Short Fall" SortExpression="ShortFall"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="TargetMargin" HeaderText="Target Margin" SortExpression="TargetMargin"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ActualMargin" HeaderText="Actual Margin" SortExpression="ActualMargin"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ShortFallMargin" HeaderText="Shortfall Margin" SortExpression="ShortFallMargin"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="PODTarget" HeaderText="POD Target" SortExpression="PODTarget"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="PODActual" HeaderText="POD Actual" SortExpression="PODActual"
                                                   ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="OverallAchievement" HeaderText="Target Points" SortExpression="OverallAchievement"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="GainPoints" HeaderText="Gain Points" SortExpression="GainPoints"
                                                ItemStyle-HorizontalAlign="Right" />
                                                <asp:ButtonField ButtonType="Link" CommandName="PendingPOD" DataTextField="PendingPOD"
                                                    HeaderText="Pending POD" SortExpression="PendingPOD" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                            <td style="vertical-align: top">
                                <div style="width: 100%">
                                    <%--  <div id="grdSuplBillGHead" style="margin-top: -5px">
                                    </div>--%>
                                    <div style="overflow: auto">
                                        <asp:GridView ID="grdViewAccount" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="BranchId" EmptyDataText="No records found." Width="100%" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:BoundField DataField="BranchName" HeaderText="LOB" SortExpression="BranchName" />
                                                <asp:BoundField DataField="PODScanned" HeaderText="PODScanned" SortExpression="PODScanned"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="BillGenerated" HeaderText="BillGenerated" SortExpression="BillGenerated"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="Pending" HeaderText="Pending" SortExpression="Pending"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="TargetPercentage" HeaderText="TargetPercentage" SortExpression="TargetPercentage"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ActualPercentage" HeaderText="ActualPercentage" SortExpression="ActualPercentage"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="TotalReceivable" HeaderText="TotalReceivable" SortExpression="TotalReceivable"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="DueReceived" HeaderText="DueReceived" SortExpression="DueReceived"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="TargetRcvable" HeaderText="TargetRcvable" SortExpression="TargetRcvable"
                                                    ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ActualPercentageRcvable" HeaderText="ActualPercentageRcvable"
                                                    SortExpression="ActualPercentageRcvable" ItemStyle-HorizontalAlign="Right" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 700px" id="tblDashboard" runat="server">
                        <tr>
                            <td colspan="6" align="center">
                                <asp:Label Text="DASHBOARD" ID="lbldashboard" runat="server" Font-Bold="True" Font-Italic="True"
                                    ForeColor="Red" Font-Size="Medium"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                    OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="true"
                                    CssClass="grid" DataKeyNames="" EmptyDataText="No Records Found." OnRowDataBound="grdMain_RowDataBound">
                                    <RowStyle Height="10px" />
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
