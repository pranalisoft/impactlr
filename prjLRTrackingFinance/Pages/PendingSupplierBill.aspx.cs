﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class PendingSupplierBill : System.Web.UI.Page
    {
        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillComboBranch()
        {
            cmbBranch.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.Items.Add(new ListItem("", "-1"));
            BranchBO OptBO = new BranchBO();
            OptBO.SearchText = string.Empty;
            DataTable dt = OptBO.GetBranchdetails();
            cmbBranch.DataSource = dt;
            cmbBranch.DataTextField = "Name";
            cmbBranch.DataValueField = "Srl";
            cmbBranch.DataBind();
            cmbBranch.SelectedIndex = 0;
        }

        private void LoadGridView()
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            if (cmbBranch.SelectedIndex > 0)
                optBO.BranchID = long.Parse(cmbBranch.SelectedValue);
            else
                optBO.BranchID = 0;
            DataTable dt = optBO.FillLRPendingForBill();
            grdView.DataSource = dt;
            grdView.DataBind();
            lblTotalRecords.Text = dt.Rows.Count.ToString();
        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    objWorksheet.Cells["C:C"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["H:H"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            if (!IsPostBack)
            {
                FillComboBranch();
                cmbBranch.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            SuplInvoiceBO optBO = new SuplInvoiceBO();
            if (cmbBranch.SelectedIndex > 0)
                optBO.BranchID = long.Parse(cmbBranch.SelectedValue);
            else
                optBO.BranchID = 0;
            DataTable dt = optBO.FillLRPendingForBill();
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\LRPendingForBill_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");
            ExportToExcel(ds, filename);
        }
    }
}