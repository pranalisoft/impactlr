﻿<%@ Page Title="Party Master" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="ConsignorMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.ConsignorMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Consignor Master
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Consinger Created Successfully."
                        CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Party Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                    <asp:View ID="vwEntry" runat="server">
                        <table style="width: 99%">
                            <tr>
                                <td style="width: 60%; vertical-align: top">
                                    <table style="width: 99.5%">
                                        <tr>
                                            <td colspan="2">
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblusername" runat="server" Text="Name" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" MaxLength="50" TabIndex="1" Width="400px"
                                                    CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                        ControlToValidate="txtName" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Address" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" runat="server" MaxLength="500" TabIndex="2" Width="400px"
                                                    TextMode="MultiLine" Rows="7" CssClass="NormalTextBold"></asp:TextBox>&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                    ControlToValidate="txtAddress" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Email Id" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" TabIndex="3" Width="400px"
                                                    CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                        runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                        ControlToValidate="txtEmail" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regexpEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="save" 
                                                    ForeColor="Red" ValidationExpression="^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                    Display="Dynamic" ErrorMessage="Invalid email address" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Mobile No." CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server" MaxLength="50" TabIndex="4" Width="400px"
                                                    CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                        ControlToValidate="txtMobile" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Phone No." CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" TabIndex="5" Width="400px"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="Pin Code" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                                    style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPinCode" runat="server" MaxLength="6" TabIndex="6" Width="400px"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" TargetControlID="txtPinCode"
                                                    FilterType="Numbers">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="GST No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtGSTNo" runat="server" Width="200px" TabIndex="7" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="Type" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:RadioButton runat="server" ID="rdbconsignor" Text="Consignor" GroupName="con"
                                                    TabIndex="8" />
                                                &nbsp;<asp:RadioButton runat="server" ID="rdbconsignee" Text="Consignee" GroupName="con"
                                                    TabIndex="9" />
                                                &nbsp;<asp:RadioButton runat="server" ID="rdbBoth" Text="Both" GroupName="con" TabIndex="10" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblactive" runat="server" Text="Active"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkActive" runat="server" TabIndex="11" />
                                            </td>
                                        </tr>
                                        <tr style="height: 15px;">
                                            <td colspan="2" style="height: 15px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                                    ValidationGroup="save" TabIndex="12" />
                                                <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                                    TabIndex="13" />
                                                <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                                    TabIndex="14" Visible="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="vertical-align: top;border:1px solid #3A66AF">
                                    <div style="height: 600px; overflow: auto">
                                        <asp:GridView ID="grdCustomer" runat="server" Width="100%" AutoGenerateColumns="False"
                                            CssClass="grid" DataKeyNames="CustomerId,Name" EmptyDataText="No Records Found.">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelectCust" Checked='<%# Convert.ToBoolean(Eval("Sel")) %>' /></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Name" HeaderText="Customer" SortExpression="Name"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                            <PagerSettings Mode="Numeric" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="vwPacklistView" runat="server">
                        <table cellpadding="0" cellspacing="0" width="98%">
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                        style="color: red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="14" Width="300px"
                                        CssClass="NormalTextBold"></asp:TextBox>
                                    <asp:Button TabIndex="15" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                        OnClick="btnSearch_Click" />
                                    <asp:Button TabIndex="16" ID="btnClearSearch" runat="server" Text="Clear Filter"
                                        CssClass="button" OnClick="btnClearSearch_Click" />
                                    <asp:Button TabIndex="17" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                        OnClick="btnDelete_Click" />
                                    <asp:Button TabIndex="18" ID="btnAdd" runat="server" Text="Add" CssClass="button"
                                        OnClick="btnAdd_Click" />
                                </td>
                                <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                                    Records :
                                    <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table width="99%">
                            <tr>
                                <td>
                                    <div>
                                        <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                            AllowPaging="True" CssClass="grid" DataKeyNames="Srl,Name,Address,UsedCnt,IsActive,GSTNo,EmailId,MobileNo,PhoneNo,ConsignorType,Pincode"
                                            EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                            OnRowCommand="grdMst_RowCommand" OnRowDataBound="grdMst_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle Width="30px" />
                                                    <ItemStyle Width="30px" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Name"
                                                    ItemStyle-Width="250px" SortExpression="Name"></asp:ButtonField>
                                                <asp:BoundField HeaderText="Address" DataField="Address" />
                                                <asp:BoundField HeaderText="GSTNo" DataField="GSTNo" ItemStyle-Width="200px" />
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                            <PagerSettings Mode="Numeric" />
                                        </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
