﻿<%@ Page Title="POD Performance" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="PODPerformance.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PODPerformance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            POD Performance
                        </td>
                    </tr>
                </table>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                            </td>
                            <td width="60px">
                                <asp:RadioButton runat="server" ID="rdbAll" Text="All" Checked="true" GroupName="PODReq"
                                    TabIndex="6" />
                            </td>
                            <td width="120px">
                                <asp:RadioButton runat="server" ID="rdbPODReq" Text="POD Required" GroupName="PODReq"
                                    TabIndex="7" />
                            </td>
                            <td>
                                <asp:RadioButton runat="server" ID="rdbPODNot" Text="POD Not Required" GroupName="PODReq"
                                    TabIndex="8" />
                            </td>
                             <td width="80px">
                                <asp:Label ID="Label1" Text="Days Slab" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="cmbSlab" CssClass="NormalTextBold">
                                    <asp:ListItem  Value=""></asp:ListItem>
                                    <asp:ListItem  Value="Under15">0 To 15</asp:ListItem>
                                    <asp:ListItem  Value="Above15">16 To 45</asp:ListItem>
                                    <asp:ListItem  Value="Above45">46 & Above</asp:ListItem>
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>
                            <td width="80px">
                            </td>
                            <td colspan="5">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="3" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" OnClick="btnExcel_Click"
                                    TabIndex="4" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="3">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="false"
                                    ShowFooter="true" PageSize="5" CssClass="grid" AllowPaging="false" AllowSorting="false"
                                    DataKeyNames="BranchId" EmptyDataText="No Records Found." OnRowCreated="grdMain_RowCreated"
                                    OnRowDataBound="grdMain_RowDataBound" OnSorting="grdMain_Sorting" OnRowCommand="grdMain_RowCommand">
                                    <RowStyle Height="10px" />
                                    <Columns>
                                        <asp:BoundField DataField="BranchName" HeaderText="Branch Name" SortExpression="BranchName" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Under15" DataTextField="Under15_Rcvd"
                                            ControlStyle-Font-Underline="false" HeaderText="0 To 15" SortExpression="Under15_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Above15" DataTextField="Above15_Rcvd"
                                            ControlStyle-Font-Underline="false" HeaderText="16 To 45" SortExpression="Above15_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                            <asp:ButtonField ButtonType="Link" CommandName="Above45" DataTextField="Above45_Rcvd"
                                            ControlStyle-Font-Underline="false" HeaderText="46 & Above" SortExpression="Above45_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Under15NR" DataTextField="Under15_NRcv"
                                            ControlStyle-Font-Underline="false" HeaderText="0 To 15" SortExpression="Under15_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Above15NR" DataTextField="Above15_NRcv"
                                            ControlStyle-Font-Underline="false" HeaderText="16 To 45" SortExpression="Above15_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Above45NR" DataTextField="Above45_NRcv"
                                            ControlStyle-Font-Underline="false" HeaderText="46 & Above" SortExpression="Above45_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:ButtonField ButtonType="Link" CommandName="Total" DataTextField="Total" ControlStyle-Font-Underline="false"
                                            HeaderText="Total" SortExpression="Total" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <%-- <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" ItemStyle-HorizontalAlign="Right"
                                            ItemStyle-Width="100px" />--%>
                                        <asp:BoundField DataField="Under15_Performance_Rcvd" HeaderText="0 To 15" SortExpression="Under15_Performance_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Above15_Performance_Rcvd" HeaderText="16 To 45" SortExpression="Above15_Performance_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="Above45_Performance_Rcvd" HeaderText="46 & Above" SortExpression="Above45_Performance_Rcvd"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Under15_Performance_NRcv" HeaderText="0 To 15" SortExpression="Under15_Performance_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="Above15_Performance_NRcv" HeaderText="16 To 45" SortExpression="Above15_Performance_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="Above45_Performance_NRcv" HeaderText="46 & Above" SortExpression="Above45_Performance_NRcv"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="OverAll" HeaderText="Over All" SortExpression="OverAll"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtFromDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtFromDate.ClientID %>').val('');
                }
            });

            $('#<%= txtToDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtToDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });
        
    </script>
</asp:Content>
