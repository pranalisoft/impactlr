﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using BusinessObjects.BO;
using System.Data;
using Logger;
using prjLRTrackerFinanceAuto.Common;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UserMaster : System.Web.UI.Page
    {
        LogWriter logger;
        enum ddlFillType { roles, location };
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPanelMsg("", false, -1);
                ClearView();
                Fillddl(ddlFillType.roles, cmbRoles);
                FillGrid(string.Empty);
                txtUserId.Focus();
            }
            else
            {
                txtPassword.Attributes.Add("value", txtPassword.Text);
            }
        }

        private void ClearView()
        {
            SetPanelMsg("", false, 1);
            txtUserId.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtPassword.Attributes.Add("value", string.Empty);
            txtEmailId.Text = string.Empty;
            txtWhatsappNo.Text = string.Empty;
            txtHiddenId.Value = "0";
            LoadGridViewPlant();
            if (cmbRoles.Items.Count > 0)
                cmbRoles.SelectedIndex = 0;
            txtPassword.Enabled = true;
            imgLogo.ImageUrl = "";
        }

        private void LoadGridViewPlant()
        {
            CommonBO OptBO = new CommonBO();
            OptBO.srl = 0;
            DataTable dt = OptBO.GetBranch();
            grdPlant.DataSource = dt;
            grdPlant.DataBind();
        }

        private void Fillddl(ddlFillType ddtype, DropDownList ddlname)
        {
            try
            {
                ddlname.DataSource = null;
                ddlname.DataBind();
                switch (ddtype)
                {
                    case ddlFillType.roles:
                        cmbRoles.Items.Clear();
                        cmbRoles.Items.Add(new ListItem("Select Roles", "-1"));
                        cmbRoles.DataSource = new UserDAO().GetRole();
                        cmbRoles.DataTextField = "CodeName";
                        cmbRoles.DataValueField = "Srl";
                        cmbRoles.DataBind();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("User Master Access-Fill DDL Function", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }



        public bool isValidateData()
        {

            //User Already Exist

            return true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool plantselected = false;
                for (int plantindex = 0; plantindex < grdPlant.Rows.Count; plantindex++)
                {
                    CheckBox rowcheck = (CheckBox)grdPlant.Rows[plantindex].FindControl("chkSelect");
                    if (rowcheck.Checked)
                    {
                        plantselected = true;
                        break;
                    }
                }

                if (!plantselected)
                {
                    SetPanelMsg("Please select atleast one plant for the user.", true, 0);
                    return;
                }

                DataTable dtplant = null;
                dtplant = CommonTypes.CreateDataTable("Srl", "dtPlant");
                for (int plantindex = 0; plantindex < grdPlant.Rows.Count; plantindex++)
                {
                    CheckBox rowcheck = (CheckBox)grdPlant.Rows[plantindex].FindControl("chkSelect");
                    if (rowcheck.Checked)
                    {
                        DataKey keys = grdPlant.DataKeys[plantindex];
                        DataRow dr = dtplant.NewRow();
                        dr["Srl"] = keys["Srl"].ToString();
                        CommonTypes.Addrow(dtplant, dr);
                    }
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dtplant);
                string strplant = ds.GetXml();
                ds.Tables.Remove(dtplant);

                UserDAO udao = new UserDAO();
                if (udao.isUserAlreadyExist(txtUserId.Text.Trim(), long.Parse(txtHiddenId.Value)) == false)
                {
                    long flgCnt = 0;
                    if (Session["FileUpload1"] != null)
                    {
                        flCopy = (FileUpload)Session["FileUpload1"];
                    }
                    byte[] Image = null;
                    //if (flCopy.HasFile)
                    //{
                    //    Image = new byte[flCopy.PostedFile.ContentLength];
                    //    HttpPostedFile UploadedImage = flCopy.PostedFile;
                    //    UploadedImage.InputStream.Read(Image, 0, (int)flCopy.PostedFile.ContentLength);
                    //}
                    if (Session["ImageBytes"] != null)
                    {
                        Image = (byte[])Session["ImageBytes"];
                    }
                    


                    flgCnt = udao.InsertUser(txtUserId.Text.Trim(), txtPassword.Text.Trim(), txtUserName.Text.Trim(), txtEmailId.Text.Trim(), long.Parse(cmbRoles.SelectedValue.ToString()), strplant, long.Parse(txtHiddenId.Value), chkActive.Checked,Image,txtWhatsappNo.Text.Trim());

                    if (long.Parse(txtHiddenId.Value) == 0)
                    {
                        ClearView();
                        SetPanelMsg("User added successfully.", true, 1);
                    }
                    else
                    {
                        ClearView();
                        SetPanelMsg("User updated successfully.", true, 1);
                    }

                    txtSearch.Text = string.Empty;
                    FillGrid(txtSearch.Text);
                    txtUserId.Focus();

                }
                else
                {
                    SetPanelMsg("User Already Exist.", true, 0);
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("User Master - Save Click.", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/pages/LRMain.aspx", false);
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("User Master - Back Click", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearView();
                Fillddl(ddlFillType.roles, cmbRoles);
                txtUserId.Focus();
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("User Master - Back Click", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillGrid(string Search)
        {
            try
            {
                DataTable dt = new UserDAO().GetUserData("Y", Search);
                grdMst.DataSource = dt;
                grdMst.DataBind();
                lbltotRecords.Text = dt.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("User Master-FillGrid", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnInfoOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/pages/SiteReturnMain.aspx", false);
        }

        protected void grdMst_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Page"))
                    return;
                if (e.CommandName.Equals("Sort"))
                    return;
                int index = Convert.ToInt32(e.CommandArgument);
                GridView grd = (GridView)e.CommandSource;
                DataKey keys = grd.DataKeys[index];
                GridViewRow row1 = grd.Rows[index];

                if (e.CommandName == "Modify")
                {
                    ClearView();
                    Fillddl(ddlFillType.roles, cmbRoles);
                    txtHiddenId.Value = keys["Srl"].ToString();
                    txtUserId.Text = keys["UserId"].ToString();
                    //txtPassword.Text = keys["Userpwd"].ToString();
                    txtPassword.Attributes.Add("value", keys["Userpwd"].ToString());
                    txtUserName.Text = keys["UserName"].ToString();
                    txtEmailId.Text = keys["Email_Id"].ToString();
                    txtWhatsappNo.Text = keys["WhatsappNo"].ToString();
                    cmbRoles.SelectedValue = keys["Role_Srl"].ToString();
                    chkActive.Checked = bool.Parse(keys["isActive"].ToString());
                    if (keys["DigitalSignature"] != null && keys["DigitalSignature"].ToString().Trim() != string.Empty)
                    {
                        byte[] bytes = (byte[])keys["DigitalSignature"];
                        string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        imgLogo.ImageUrl = "data:image/png;base64," + base64String;
                    }
                    //txtPassword.Enabled = false;

                    UserDAO userDao = new UserDAO();
                    DataTable dtBranch = userDao.GetUserBranches(long.Parse(txtHiddenId.Value));

                    if (dtBranch != null)
                    {
                        for (int dtindex = 0; dtindex < dtBranch.Rows.Count; dtindex++)
                        {
                            for (int plantindex = 0; plantindex < grdPlant.Rows.Count; plantindex++)
                            {
                                DataKey keysPlant = grdPlant.DataKeys[plantindex];
                                if (long.Parse(dtBranch.Rows[dtindex][1].ToString()) == long.Parse(keysPlant["Srl"].ToString()))
                                {
                                    CheckBox rowcheck = (CheckBox)grdPlant.Rows[plantindex].FindControl("chkSelect");
                                    rowcheck.Checked = true;
                                }
                            }
                        }
                    }
                    txtEmailId.Focus();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void grdMst_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdMst.PageIndex = e.NewPageIndex;
            FillGrid(txtSearch.Text.Trim());
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid(txtSearch.Text.Trim());
            txtSearch.Focus();
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Text = string.Empty;
            FillGrid(txtSearch.Text.Trim());
            txtSearch.Focus();
        }
        protected void lnkBtnAttachFiles_Click(object sender, EventArgs e)
        {
            popAttachFIles.Show();
            flCopy.Focus();
        }

        protected void btnFileCancel_Click(object sender, EventArgs e)
        {
            popAttachFIles.Hide();
        }

        protected void btnSaveFiles_Click(object sender, EventArgs e)
        {
            popAttachFIles.Show();
            //BranchBO OptBO = new BranchBO();
            //byte[] Image = null;
            if (FileUpload2.HasFile)
            {
                Session["FileUpload1"] = FileUpload2;
                //flCopy = FileUpload2;
                //Image = new byte[FileUpload2.PostedFile.ContentLength];
                //HttpPostedFile UploadedImage = FileUpload2.PostedFile;
                //UploadedImage.InputStream.Read(Image, 0, (int)FileUpload2.PostedFile.ContentLength);
            }
            //OptBO.CompanyLogo = Image;
            //OptBO.CompanyID = long.Parse(Session["CompanyID"].ToString());
            //long cnt= OptBO.Insert_Update_Company_Logo();
            btnconfirmFile_Click(btnconfirmFile, EventArgs.Empty);
            popAttachFIles.Hide();
        }

        protected void btnconfirmFile_Click(object sender, EventArgs e)
        {
            byte[] Image = null;
            if (Session["FileUpload1"] != null)
            {
                flCopy = (FileUpload)Session["FileUpload1"];
            }

            if (flCopy.HasFile)
            {
                Image = new byte[flCopy.PostedFile.ContentLength];
                HttpPostedFile UploadedImage = flCopy.PostedFile;
                UploadedImage.InputStream.Read(Image, 0, (int)flCopy.PostedFile.ContentLength);
                Session["ImageBytes"] = Image;
                string base64String = Convert.ToBase64String(Image, 0, Image.Length);
                imgLogo.ImageUrl = "data:image/png;base64," + base64String;
            }
        }
    }
}
