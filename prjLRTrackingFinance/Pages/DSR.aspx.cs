﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class DSR : System.Web.UI.Page
    {
        private DataTable getData()
        {
            ReportBO optbo = new ReportBO();
            optbo.FromDate = txtFromDate.Text;
            optbo.ToDate = txtToDate.Text;
            optbo.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = optbo.DashBoard();
            return dt;
        }

        private void LoadGridView()
        {
            DataTable dt = getData();
            grdMain.DataSource = dt;
            grdMain.DataBind();
        }

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12)); 
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }

                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);

            if (!IsPostBack)
            {
                txtFromDate.Text = "01/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString(); ///+ DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                btnSearch_Click(btnSearch, EventArgs.Empty);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
        }

        protected void grdMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells[0].Text.ToLower() == "total")
            {
                e.Row.BackColor = System.Drawing.Color.Yellow;
                e.Row.Font.Bold = true;
            }
            for (int i = 1; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\DSR_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            DataTable dt = getData();
            
            DataSet ds = new DataSet();            
            ds.Tables.Add(dt);

            ExportToExcel(ds, filename);
        }
    }
}