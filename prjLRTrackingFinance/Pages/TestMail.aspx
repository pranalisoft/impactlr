﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="TestMail.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.TestMail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
       <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
          <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                 Test Mail</td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                 <div class="msg_region">
                   <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                    <div style="padding-left: 15px;">                        
                        <div>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Email Id" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtUserId" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr> 
                                 <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Use Default Creadentials" 
                                            CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td width="10px">
                                        :
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="1" ID="txtoldpwd" Width="150" runat="server" MaxLength="75"
                                            CssClass="TextBox" Text="true"></asp:TextBox> 
                                    </td>
                                </tr> 
                            </table>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="3" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="4" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                       
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
 