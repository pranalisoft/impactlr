﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.BO;
using System.Data;
using prjLRTrackerFinanceAuto.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class MIS : System.Web.UI.Page
    {
        //private void RegisterDateTextBox()
        //{
        //    if (!IsClientScriptBlockRegistered("blockkeys"))
        //    {
        //        ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
        //    }
        //}

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;
            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }

        private void FillDropDowns()
        {
            cmbBranch.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.Items.Add(new ListItem("", "-1"));
            BranchBO OptBO = new BranchBO();
            OptBO.SearchText = string.Empty;
            OptBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = OptBO.GetBranchesUserWise();
            cmbBranch.DataSource = dt;
            cmbBranch.DataTextField = "Name";
            cmbBranch.DataValueField = "Srl";
            cmbBranch.DataBind();
            cmbBranch.SelectedIndex = 0;
        }

        private DataTable getData()
        {
            LRBO optBO = new LRBO();
            optBO.Fromdate = txtFromDate.Text;
            optBO.ToDate = txtToDate.Text;
            optBO.BranchID = long.Parse(cmbBranch.SelectedValue);
            optBO.CreatedBy = long.Parse(Session["EID"].ToString());
            DataTable dt = optBO.LRMISReport();
            return dt;
        }

        private void LoadGridView()
        {
            DataTable dt = getData();
            grdPacklistView.DataSource = dt;
            grdPacklistView.DataBind();
            lblTotalRecords.Text = dt.Rows.Count.ToString();
        }        

        private void ExportToExcel(DataSet ds, string XLPath)
        {
            using (ExcelPackage objExcelPackage = new ExcelPackage())
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //Create the worksheet
                    ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(ds.Tables[i].TableName);
                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1     
                    objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                    objWorksheet.Cells["D:D"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["I:I"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["J:J"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["S:S"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["T:T"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AB:AB"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AC:AC"].Style.Numberformat.Format = "dd-MMM-yyyy"; 
                    objWorksheet.Cells["AF:AF"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["AG:AG"].Style.Numberformat.Format = "dd-MMM-yyyy"; 
                    objWorksheet.Cells["AP:AP"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["BI:BI"].Style.Numberformat.Format = "dd-MMM-yyyy";
                    objWorksheet.Cells["BM:BM"].Style.Numberformat.Format = "dd-MMM-yyyy"; 
                    objWorksheet.Cells["A1:BZ1"].AutoFilter = true;
                    objWorksheet.Cells.AutoFitColumns();
                    //Format the header              
                    using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                    {
                        objRange.Style.Font.Bold = true;
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.AutoFilter = true;
                        //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;  
                        //objRange.Style.Fill.BackgroundColor.SetColor(Color.Aqua); 
                    }
                }
                
                //Write it back to the client      
                if (File.Exists(XLPath))
                    File.Delete(XLPath);
                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(XLPath);
                objFileStrm.Close();
                //Write content to excel file     
                File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());


                System.IO.FileInfo fileInfo = new System.IO.FileInfo(XLPath);

                //DownloadFile
                //Response.Clear();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + fileInfo.Name + "");
                Response.ContentType = "application/vnd.ms-excel";
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scrp = ScriptManager.GetCurrent(this.Page);
            scrp.RegisterPostBackControl(btnExcel);
            //RegisterDateTextBox();
            SetPanelMsg("", false, -1);
            if (!IsPostBack)
            {
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.AddMonths(-1).ToString("01/MM/yyyy");
                FillDropDowns();
                LoadGridView();
                btnSearch.Focus();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadGridView();
            btnSearch.Focus();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string filename =HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FilesPathDownload"].ToString() + @"\LRMIS_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx");

            DataTable dt = getData();
            DataSet ds = new DataSet();
            if (cmbBranch.SelectedIndex == 0)
            {             
                List<DataTable> result = dt.AsEnumerable()
                                        .GroupBy(x => x.Field<string>("BranchName"))
                                        .Select(grp => grp.CopyToDataTable())
                                        .ToList();
                for (int i = 0; i < result.Count; i++)
                {
                    result[i].TableName = (i+1).ToString()+ "_"+ result[i].Rows[0]["BranchName"].ToString();
                    ds.Tables.Add(result[i]);
                }                
            }
            else
                ds.Tables.Add(dt);

            ExportToExcel(ds, filename);
        }

        protected void grdPacklistView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdPacklistView.PageIndex = e.NewPageIndex;
            LoadGridView();
            btnSearch.Focus();
        }
    }
}