﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using System.Data.SqlClient;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        LogWriter logger;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetPanelMsg("", false, -1);
                txtUserId.Text = Session["UserId"].ToString();
                txtUserName.Text = Session["ENAME"].ToString();
                txtoldpwd.Attributes.Add("value", "");
                txtPassword.Attributes.Add("value", "");
                txtConfirmpwd.Attributes.Add("value", "");
                txtoldpwd.Focus();
            }
            else
            {
                txtoldpwd.Attributes.Add("value", txtoldpwd.Text);
                txtPassword.Attributes.Add("value", txtPassword.Text);
                txtConfirmpwd.Attributes.Add("value", txtConfirmpwd.Text);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            txtPassword.Attributes.Add("value", txtPassword.Text);
            txtConfirmpwd.Attributes.Add("value", txtConfirmpwd.Text);
            string strPass = new CommonDAO().GetPassword(long.Parse(Session["EID"].ToString()));
            if (strPass.ToLower().Trim() == txtoldpwd.Text.ToLower().Trim())
            {
                if (txtPassword.Text.ToLower().Trim() == txtConfirmpwd.Text.ToLower().Trim())
                {
                try
                {

                    new CommonDAO().ChangePassword(long.Parse(Session["EID"].ToString()), txtConfirmpwd.Text);
                    SetPanelMsg("Password change will take effect from next login.", true, 1);
                }
                catch (Exception exp)
                {
                    logger = new LogWriter();
                    logger.WriteLogError("User Master - Back Click", exp);
                    Response.Redirect("~/pages/ErrorPage.aspx", false);
                }
                }
                else
                {
                    SetPanelMsg("New password and confirm password should be same.", true, 0);
                    txtPassword.Focus();

                }
            }
            else
            {
                SetPanelMsg("Incorrect Old Password.", true, 0);
                txtoldpwd.Focus();

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("LRMain.aspx", false);
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Change Password - Cancel Click", exp);
                Response.Redirect("ErrorPage.aspx", false);
            }
        }

        private void SetPanelMsg(string msg, bool visible, int code)
        {
            if (visible)
            {
                MsgPanel.Message = msg;
                MsgPanel.DispCode = code;

            }
            else
            {
                MsgPanel.Message = "";
                MsgPanel.DispCode = -1;
            }
        }
    }
}
