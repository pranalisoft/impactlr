﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class ViewFile : System.Web.UI.Page
    {
        private string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        { 
            // Get the physical Path of the file(test.doc)
            string filepath = Session["FilePath"].ToString();

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);
            // Checking if file exists
            //ImpersonateUser impersonateuser = new ImpersonateUser();
            //if (impersonateuser.ImpersonateValidUser(username,domainname,pwd))
            //{
            if (file.Exists)
            {
                if (Session["ReportType"]!=null && Session["ReportType"].ToString() == "SuplInvDoc")
                {

                    //Code added by Vishal on 22 Oct 2020
                    WebClient User = new WebClient();
                    Byte[] fileBuffer = User.DownloadData(filepath);
                    if (fileBuffer != null)
                    {
                        Response.ContentType = ReturnExtension(file.Extension.ToLower());
                        Response.AddHeader("Content-Length", fileBuffer.Length.ToString());
                        Response.BinaryWrite(fileBuffer);
                    }  
                }
                else
                { 
                    // Clear the content of the response
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();

                    // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);

                    // Add the file size into the response header
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    // Set the ContentType
                    Response.ContentType = ReturnExtension(file.Extension.ToLower());

                    // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                    Response.TransmitFile(file.FullName);

                    // End the response
                    Response.End();
                    file.Delete();
                } 
            } 
        }
    }
}
