﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using BusinessObjects;
using BusinessObjects.DAO;
using Logger;
using BusinessObjects.BO;
using prjLRTrackerFinanceAuto.Common;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class UpdateSupplierAdvance : System.Web.UI.Page
    {
        #region "Procedure"
        private void RegisterDateTextBox()
        {
            if (!IsClientScriptBlockRegistered("blockkeys"))
            {
                ScriptManager.RegisterStartupScript(uPanel, uPanel.GetType(), "blockkeys", "blockkeys();", true);
            }
        }

        private void clearAll()
        {            
            HFCode.Value = "0";
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtAmount.Text = txtchqdt.Text = TxtChqNo.Text = txtCashAmount.Text = txtFuelAmount.Text = txtCashAmountBal.Text = txtFuelAmountBal.Text = String.Empty;
            if (cmbSupl.Items.Count > 0) cmbSupl.SelectedIndex = 0;
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0;
            if (cmbLR.Items.Count > 0) cmbLR.SelectedIndex = 0;
            //cmbLR.DataSource = null;
            //cmbLR.DataBind();
            //cmbLR.Items.Clear();
            //cmbLR.Text = string.Empty;
            cmbPaymentMode.SelectedIndex = 4;
            cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
            cmbSupl.Focus();
        }

        private void clearPayment()
        {            
            txtAmount.Text = txtchqdt.Text = TxtChqNo.Text = String.Empty;           
            if (cmbBank.Items.Count > 0) cmbBank.SelectedIndex = 0; 
            cmbPaymentMode.SelectedIndex = 4;
            cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
        }

        private void FillComboSupplier()
        {
            cmbSupl.Items.Clear();
            cmbSupl.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            dt = OptBO.FillSupplierComboAdvance();
            cmbSupl.DataSource = dt;
            cmbSupl.DataTextField = "Name";
            cmbSupl.DataValueField = "Srl";
            cmbSupl.DataBind();
            cmbSupl.SelectedIndex = 0;
        }

        private void FillComboLR()
        {
            cmbLR.Items.Clear();
            cmbLR.Items.Add(new ListItem("", "-1"));
            DataTable dt = null;
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.BranchID = long.Parse(string.IsNullOrEmpty(Session["BranchID"].ToString()) ? "1" : Session["BranchID"].ToString());
            OptBO.SupplierID = cmbSupl.SelectedIndex == null ? 0 : long.Parse(cmbSupl.SelectedValue);
            dt = OptBO.FillLRComboAdvance();
            cmbLR.DataSource = dt;
            cmbLR.DataTextField = "LRNo";
            cmbLR.DataValueField = "Srl";
            cmbLR.DataBind();
            cmbLR.SelectedIndex = 0;
        }

        private void fillBank()
        {
            cmbBank.Items.Clear();
            CommonBO bankbo = new CommonBO();
            DataTable dt = bankbo.FillBankDetails();
            cmbBank.DataSource = dt;
            cmbBank.DataTextField = "BankName";
            cmbBank.DataValueField = "BankName";
            cmbBank.DataBind();
        }

        private void CalculateAmount()
        {
            if (txtCashAmount.Text.Trim() == string.Empty) txtCashAmount.Text = "0";
            if (txtFuelAmount.Text.Trim() == string.Empty) txtFuelAmount.Text = "0";
            txtAmount.Text = (decimal.Parse(txtCashAmount.Text) + decimal.Parse(txtFuelAmount.Text)).ToString("F2");
        }

        private void LoadData()
        {
            fillBank();
            SupplierPaymentBO OptBO = new SupplierPaymentBO();
            OptBO.Srl = long.Parse(cmbLR.SelectedValue);
            DataTable dt = OptBO.FillPaymentHeaderAdvance();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["PaymentMode"] != null)
                    {
                        if (dt.Rows[0]["PaymentMode"].ToString().Trim() != string.Empty)
                        {
                            cmbPaymentMode.SelectedValue = dt.Rows[0]["PaymentMode"].ToString();
                            cmbPaymentMode_SelectedIndexChanged(cmbPaymentMode, EventArgs.Empty);
                            txtchqdt.Text = DateTime.Parse(dt.Rows[0]["ChqDate"].ToString()).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            cmbPaymentMode.SelectedValue = "1";
                            txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        }
                    }
                    else
                    {
                        cmbPaymentMode.SelectedValue = "1";
                        txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    
                    txtCashAmount.Text = dt.Rows[0]["CashAdvanceBal"].ToString();
                    txtFuelAmount.Text = dt.Rows[0]["FuelAdvanceBal"].ToString();
                    txtCashAmountBal.Text = dt.Rows[0]["CashAdvanceBal"].ToString();
                    txtFuelAmountBal.Text = dt.Rows[0]["FuelAdvanceBal"].ToString();
                    txtAmount.Text = dt.Rows[0]["TotalAmountBal"].ToString();
                    
                    if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
                    {
                        TxtChqNo.Text = dt.Rows[0]["ChqNo"].ToString();
                        cmbBank.SelectedValue = dt.Rows[0]["BankName"].ToString();
                    }
                }
                else
                {
                    clearPayment();
                }
            }
            else
            {
                clearPayment();
            }
        }
        #endregion

        #region "Events"
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterDateTextBox();
            if (!IsPostBack)
            {
                clearAll();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtchqdt.Text = DateTime.Now.ToString("dd/MM/yyyy");
                FillComboSupplier();                
                cmbSupl.Focus();
            }

        }

        protected void cmbSupl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmbLR.DataSource = null;
                cmbLR.DataBind();
                cmbLR.Items.Clear();
                FillComboLR();
                cmbLR.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void cmbLR_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadData();
                cmbPaymentMode.Focus();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void cmbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(cmbPaymentMode.SelectedValue) > 1)
            {
                trBank.Visible = true;
                trChqNo.Visible = true;
                lblChqDDNo.Visible = true;
                TxtChqNo.Visible = true;
                cmbBank.Visible = true;
                lblBank.Visible = true;
                CustomBank.Enabled = true;
                reqFVChqNo.Enabled = true;
            }
            else
            {
                trBank.Visible = false;
                trChqNo.Visible = false;
                lblChqDDNo.Visible = false;
                TxtChqNo.Visible = false;
                cmbBank.Visible = false;
                lblBank.Visible = false;
                CustomBank.Enabled = false;
                reqFVChqNo.Enabled = false;
            }
            switch (cmbPaymentMode.SelectedValue)
            {
                case "1":
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "2":
                    lblChqDDNo.Text = "Chq No.";
                    lblchqddDate.Text = "Chq.Date";
                    break;
                case "3":
                    lblChqDDNo.Text = "DD No.";
                    lblchqddDate.Text = "DD Date";
                    break;
                case "4":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "5":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                case "6":
                    lblChqDDNo.Text = "Remarks";
                    lblchqddDate.Text = "Tran Date";
                    break;
                default:
                    break;
            }
            txtchqdt.Focus();
        }

        protected void EntryForm_Command(object sender, CommandEventArgs e)
        {
            CommonTypes.EntryFormCommand command = CommonTypes.StringToEnum<CommonTypes.EntryFormCommand>(e.CommandName);

            long srl = 0;
            switch (command)
            {
                case CommonTypes.EntryFormCommand.Add:
                    //lblMasterHeader.Text = "Add Payment from Supplier";                   
                    break;
                case CommonTypes.EntryFormCommand.Delete:
                    break;
                case CommonTypes.EntryFormCommand.Save:
                    if (!Page.IsValid)
                        return;

                    decimal totamt = 0;
                    decimal balAmt = 0;
                    string paymentxml = "";

                    SupplierPaymentBO custbo = new SupplierPaymentBO();
                    
                       custbo.tranDate = txtDate.Text.Trim();
                        custbo.SupplierID = long.Parse(cmbSupl.SelectedValue);
                        custbo.refNo = String.Empty;
                        custbo.remarks = String.Empty;
                        custbo.CashAmount = decimal.Parse(txtCashAmount.Text);
                        custbo.FuelAmount = decimal.Parse(txtFuelAmount.Text);
                        custbo.totalAmount = decimal.Parse(txtAmount.Text);
                        custbo.userID = long.Parse(Session["EID"].ToString());
                        custbo.BankName = cmbBank.Text.Trim();
                        custbo.PaymentMode = cmbPaymentMode.SelectedValue;
                        custbo.ChqNo = TxtChqNo.Text.Trim();
                        custbo.ChqDate = txtchqdt.Text.Trim();
                        custbo.CardNetBankingDetails = TxtChqNo.Text.Trim();
                        custbo.Srl = long.Parse(cmbLR.SelectedValue);
                    
                    long cnt = custbo.UpdatePaymentHeader();
                    if (cnt > 0)
                    {
                        MsgPanel.Message = "Record Updated successfully.";
                        MsgPanel.DispCode = 1;
                        FillComboSupplier();                        
                        clearAll();
                        cmbLR.Focus();
                        cmbSupl.Focus();
                    }
                    break;
                case CommonTypes.EntryFormCommand.Clear:
                    FillComboSupplier();
                    clearAll();
                    break;
                case CommonTypes.EntryFormCommand.None:

                    break;
                default:
                    break;
            }
        }

        protected void txtCashAmount_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
            txtCashAmount.Focus();
        }

        protected void txtFuelAmount_TextChanged(object sender, EventArgs e)
        {
            CalculateAmount();
            txtFuelAmount.Focus();
        }
        #endregion
    }
}