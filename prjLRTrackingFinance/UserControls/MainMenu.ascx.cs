﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;

namespace InsuranceWebApplication.UserControls
{
    public partial class MainMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadMenuFromXML();
            if (Session["UserID"] != null)
            {
                if ( Session["UserRoleMenu"] == null)
                {                   
                    Session["UserRoleMenu"] = new CommonDAO().GetUserRoleMenu(long.Parse(Session["EId"].ToString())).Replace("~", "..");
                    LoadMenuFromXML();
                }
                else
                {                   
                    LoadMenuFromXML();
                }              
            }
            //else
            //{
            //    if (Session["UserRoleMenu"] == null)
            //    {
            //        UserBO UserService = new UserBO();
            //        UserService.roleid = long.Parse(Session["RoleID"].ToString());

            //        Session["UserRoleMenu"] = UserService.GetUserRoleMenuByRoleId().Replace("~", "..");

            //        LoadMenuFromXML();
            //    }
            //    else
            //    {
            //        LoadMenuFromXML();
            //    }
              
            //}
            if (!IsPostBack)
            {
                //if (Session["UserID"] != null && Session["UserRoleMenu"] == null)
                //{
                //    UserBO UserService = new UserBO();
                //    UserService.userid = long.Parse(Session["UserID"].ToString());
                //    Session["UserRoleMenu"] = UserService.GetUserRoleMenu().Replace("~", "..");

                //    LoadMenuFromXML();
                //}
                //else
                //{
                //    LoadMenuFromXML();
                //}
            }
        }

        private void LoadMenuFromXML()
        {
            string menuXML = Session["UserRoleMenu"].ToString();
            if (menuXML.Length > 0)
            {
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(menuXML);

                System.Xml.Xsl.XslTransform trans = new System.Xml.Xsl.XslTransform();
                trans.Load(Server.MapPath("~/UserControls/MenuGenerator.xslt"));
                mainMenuXML.Document = doc;
                mainMenuXML.Transform = trans;
            }
        }
    }
}