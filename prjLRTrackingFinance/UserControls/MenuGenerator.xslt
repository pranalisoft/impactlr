﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="html" indent="yes"/>
  
  <xsl:template match="/menu">
    <xsl:comment>MENU</xsl:comment>
    <ul class="pureCssMenu pureCssMenum">
      <xsl:for-each select="menu_item">
        <li class="pureCssMenui">
          <xsl:element name="a">
            <xsl:attribute name="className">pureCssMenui</xsl:attribute>
            <xsl:attribute name="href"><![CDATA[#]]></xsl:attribute>
            <span>
              <xsl:value-of select="@header"/>
            </span>
            <xsl:comment>
              <![CDATA[[if gt IE 6]]]>
            </xsl:comment>
          </xsl:element>
          <xsl:comment>
            <![CDATA[[endif]]]>
          </xsl:comment>
          <xsl:comment><![CDATA[[if lte IE 6]><table><tr><td><![endif]]]></xsl:comment>
          <ul class="pureCssMenum">
            <xsl:for-each select="sub_menu_item">
              <li class="pureCssMenui">
                <xsl:element name="a">
                  <xsl:attribute name="className">pureCssMenui</xsl:attribute>
                  <xsl:attribute name="href">
                    <xsl:value-of select="web_url"/>
                  </xsl:attribute>
                  <xsl:value-of select="display_text"/>                  
                </xsl:element>
              </li>
            </xsl:for-each>
          </ul>
          <xsl:comment><![CDATA[[if lte IE 6]></td></tr></table></a><![endif]]]></xsl:comment>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>
</xsl:stylesheet>
