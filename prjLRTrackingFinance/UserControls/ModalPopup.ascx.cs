﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using prjLRTrackerFinanceAuto.Common;

namespace prjLRTrackerFinanceAuto.UserControls
{
    public partial class ModalPopup : System.Web.UI.UserControl
    {       
        public event CommandEventHandler modalPopupCommand;

        public string ModalPopupHeader 
        {
            get
            {
                return lblModalPopHeader.Text;
            }

            set
            {
                lblModalPopHeader.Text = value;
            }
        
        }

        public string ModalPopupMsg
        {
            get
            {
                return lblModalPopMsg.Text;
            }

            set
            {
                lblModalPopMsg.Text = value;
            }

        }

        public CommonTypes.ModalTypes ModalType { set; get; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                //if (ModalType == CommonTypes.ModalTypes.Alert)
                //{
                //    btnYes.Visible = false;
                //    btnNo.Visible = false;
                //}
                //else if (ModalType == CommonTypes.ModalTypes.Confirm)
                //{
                //    btnOk.Visible = false;
                //}
            }
        }

        public void ShowModal(string message,CommonTypes.ModalTypes type)
        {
            ModalPopupMsg = message;
            if (type == CommonTypes.ModalTypes.Alert)
            {
                ModalPopupHeader = "LR Tracker - Information";               
                btnYes.Visible = false;
                btnNo.Visible = false;
                btnOk.Visible = true;
                btnOk.Focus();
                //modalPopup.DefaultButton = btnOk.UniqueID;
            }
            else if (type == CommonTypes.ModalTypes.Confirm)
            {
                ModalPopupHeader = "LR Tracker - Confirm";
                btnYes.Visible = true;
                btnNo.Visible = true;
                btnOk.Visible = false;
                btnYes.Visible = true;
                btnNo.Visible = true;
                btnNo.Focus();
                //modalPopup.DefaultButton = btnNo.UniqueID;
            }
            else if (type == CommonTypes.ModalTypes.Error)
            {
                ModalPopupHeader = "LR Tracker - Error";
                btnYes.Visible = false;
                btnNo.Visible = false;
                btnOk.Visible = true;
                btnNo.Focus();
                //modalPopup.DefaultButton = btnOk.UniqueID;
            }
            modalPopupExtender.Show();
        }

        public void HideModal()
        {
            modalPopupExtender.Hide();
        }
        
        protected void ModalPopup_Command(object sender, CommandEventArgs e)
        {
            if (modalPopupCommand != null)
            {
                modalPopupCommand(sender, e);
            }
        }
    }
}