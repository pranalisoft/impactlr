﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace prjLRTrackerFinanceAuto.UserControls
{
    public partial class MessagePanel : System.Web.UI.UserControl
    {
        public string Message
        {
            set
            {
                litMsg.Text = value;
            }
        }

        private int dispCode;
        public int DispCode
        {
            get { return dispCode; }
            set
            {
                dispCode = value;
                ApplyBorder();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ApplyBorder()
        {
            pnlMsgRegion.BackColor = System.Drawing.Color.White;
            switch (DispCode)
            {
                case 0:
                    pnlMsgRegion.BorderStyle = BorderStyle.Solid;
                    pnlMsgRegion.BorderWidth = new Unit(1);
                    pnlMsgRegion.BorderColor = pnlMsgRegion.ForeColor = System.Drawing.Color.Red;
                    break;
                case 1:
                    pnlMsgRegion.BorderStyle = BorderStyle.Solid;
                    pnlMsgRegion.BorderWidth = new Unit(1);
                    pnlMsgRegion.BorderColor = pnlMsgRegion.ForeColor = System.Drawing.Color.Green;
                    break;
                case 2:
                    pnlMsgRegion.BorderStyle = BorderStyle.Solid;
                    pnlMsgRegion.BorderWidth = new Unit(1);
                    pnlMsgRegion.BorderColor = pnlMsgRegion.ForeColor = System.Drawing.Color.Orange;
                    break;
                default:
                    pnlMsgRegion.BorderStyle = BorderStyle.None;
                    pnlMsgRegion.BorderWidth = new Unit(0);
                    break;
            }
        }
    }
}