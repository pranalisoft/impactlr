﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalPopup.ascx.cs" Inherits="prjLRTrackerFinanceAuto.UserControls.ModalPopup" %>

<asp:Button ID="btnPopup" runat="server" style="display:none"/>
<asp:Panel ID="modalPopup" runat="server" style="display:none" CssClass="dialog">
    <div class="dialog_header">
        <asp:Label ID="lblModalPopHeader" runat="server" />
    </div>
    <div class="dialog_body">
        <asp:Label ID="lblModalPopMsg" runat="server" />
    </div>
    <div class="dialog_buttons">
        <asp:Button CssClass="button" ID="btnOk" runat="server" Text="Ok" CommandName="Ok" oncommand="ModalPopup_Command"/>
        <asp:Button CssClass="button" ID="btnYes" runat="server" Text="Yes" CommandName="Yes" oncommand="ModalPopup_Command"/>
        <asp:Button CssClass="button" ID="btnNo" runat="server" Text="No" CommandName="No" oncommand="ModalPopup_Command"/>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="modalPopupExtender" runat="server" TargetControlID="btnPopup" PopupControlID="modalPopup" BackgroundCssClass="DarkenBackground">
</ajaxToolkit:ModalPopupExtender>
