﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagePanel.ascx.cs" Inherits="prjLRTrackerFinanceAuto.UserControls.MessagePanel" %>

<asp:Panel ID="pnlMsgRegion" runat="server" CssClass="msg_region" HorizontalAlign="Center">
    <asp:Literal ID="litMsg" runat="server" Text=""/>
</asp:Panel>