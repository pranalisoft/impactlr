﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SqlManager;
using Logger;
using BusinessObjects.DAO;
using RestSharp;
namespace prjLRTrackerFinanceAuto.Pages
{
    public partial class login : System.Web.UI.Page
    {
        LogWriter logger;
        protected void Page_Load(object sender, EventArgs e)
        {

            //lblIP.Text = ipAddress;
            if (!IsPostBack)
            {
                if (Session["EID"] != null)
                {
                    Response.Redirect("Default.aspx?personid=" + Session["EID"].ToString(), false);
                }
                else
                {
                    lblMessage.Visible = false;
                    txtlogin.Focus();
                }

            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            try
            {
                CommonDAO cDaoLoin = new CommonDAO();
                long cnt = 0;
                if (txtlogin.Text.ToLower()=="admin")
                    cnt = cDaoLoin.ValidateLogin(txtPwd.Text.Trim(), txtlogin.Text.Trim());
                else
                    cnt = cDaoLoin.ValidateLoginwithOtp(txtPwd.Text.Trim(), txtlogin.Text.Trim(),txtOtp.Text.Trim());
                long id = 0;
                if (cnt > 0)
                {
                    lblMessage.Text = "";
                    DBLink dbconnector = new DBLink();
                    dbconnector.GetConnection();
                    string str = "Select Srl from tbl_User where UserId = '" + txtlogin.Text.Trim() + "'";
                    id = long.Parse(dbconnector.GetValue(str).ToString());
                    Session["EID"] = id.ToString();
                    UserDAO userDao = new UserDAO();
                    DataTable dtBranch = userDao.GetUserBranches(id);
                    cmbPlant.DataSource = null;
                    cmbPlant.Items.Clear();
                    cmbPlant.Items.Add(new ListItem("", "-1"));
                    cmbPlant.DataSource = dtBranch;
                    cmbPlant.DataTextField = "Field1";
                    cmbPlant.DataValueField = "BranchSrl";
                    cmbPlant.DataBind();
                    cmbPlant.SelectedIndex = 0;
                    lblUserSrl.Text = id.ToString();
                    if (dtBranch != null)
                    {
                        if (dtBranch.Rows.Count == 0)
                        {
                            lblMessage.Text = "There is no Line Of Business assigned to this userid.";
                            lblMessage.Visible = true;
                            return;
                        }
                        else
                        {
                            if (dtBranch.Rows.Count == 1)
                            {
                                Session.Clear();
                                Session["BranchID"] = dtBranch.Rows[0]["BranchSrl"].ToString();
                                Session["IsNotify"] = true;
                                Session["PlntName"] = dtBranch.Rows[0]["Field1"].ToString();
                                Session["Duty"] = dtBranch.Rows[0]["Duty"].ToString();
                                Session["BondDtls"] = dtBranch.Rows[0]["BondDtls"].ToString();
                                Session["ReminderDays"] = dtBranch.Rows[0]["Field3"].ToString();
                                Session["PermInd"] = dtBranch.Rows[0]["PermissionInd"].ToString();
                                Session["PlyApplicable"] = dtBranch.Rows[0]["PlyApplicable"].ToString();
                                Session["BillingCompanySrl"] = dtBranch.Rows[0]["BillingCompanySrl"].ToString();
                                Session["UOM"] = dtBranch.Rows[0]["UOM"].ToString();
                                Response.Redirect("Default.aspx?personid=" + id.ToString(), false);
                            }
                            else
                            {
                                dvLogin.Attributes["class"] = "row animated fadeOut";
                                dvLogin.Visible = false;
                                dvBranch.Visible = true;
                                dvBranch.Attributes["class"] = "row animated fadeIn";
                                //dvBranch.Style.Add(HtmlTextWriterStyle.Padding, "5px 18px 5px 20px");
                                //popPlant.Show();
                                cmbPlant.Focus();
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "There is no Line Of Business assigned to this userid.";
                        lblMessage.Visible = true;
                        return;
                    }
                }
                else
                {
                    lblMessage.Text = "Invalid User Name Or Password Or OTP";
                    lblMessage.Visible = true;
                    return;
                }
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Login", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }

        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    popPlant.Show();
                    Session.Clear();
                    Session["BranchID"] = cmbPlant.SelectedValue;
                    Session["PlntName"] = cmbPlant.SelectedItem.Text.Trim();
                    Response.Redirect("Default.aspx?personid=" + lblUserSrl.Text, false);
                    CommonDAO cmdao = new CommonDAO();

                    DataTable dtbranch = cmdao.GetBranch(long.Parse(Session["BranchID"].ToString()));
                    Session["Duty"] = dtbranch.Rows[0]["Duty"].ToString();
                    Session["BondDtls"] = dtbranch.Rows[0]["BondDtls"].ToString();
                    Session["ReminderDays"] = dtbranch.Rows[0]["Field3"].ToString();
                    Session["PermInd"] = dtbranch.Rows[0]["PermissionInd"].ToString();
                    Session["BillingCompanySrl"] = dtbranch.Rows[0]["BillingCompanySrl"].ToString();
                    Session["PlyApplicable"] = dtbranch.Rows[0]["PlyApplicable"].ToString();
                    Session["UOM"] = dtbranch.Rows[0]["UOM"].ToString();
                    popPlant.Hide();
                    break;
                case "no":
                    //popPlant.Hide();  
                    dvBranch.Attributes["class"] = "row animated fadeOut";
                    dvBranch.Visible = false;
                    dvLogin.Visible = true;
                    dvLogin.Attributes["class"] = "row animated fadeIn";
                    //dvLogin.Style.Add(HtmlTextWriterStyle.Padding, "5px 18px 5px 20px");
                    break;
                default:
                    break;
            }
        }

        protected void btnOtpGenerate_Click(object sender, EventArgs e)
        {
            CommonDAO cDaoLoin = new CommonDAO();
            long userId = 0;
            string phoneNo = "";
            string pwd = txtPwd.Text.Trim();
            long cnt = cDaoLoin.ValidateLogin(pwd, txtlogin.Text.Trim());
            if (cnt>0)
            {
                DBLink dbconnector = new DBLink();
                dbconnector.GetConnection();
                string str = "Select Srl from tbl_User where UserId = '" + txtlogin.Text.Trim() + "'";
                userId = long.Parse(dbconnector.GetValue(str).ToString());

                str = "Select WhatsAppNo from tbl_User where UserId = '" + txtlogin.Text.Trim() + "'";
                phoneNo =  dbconnector.GetValue(str).ToString();

                if (phoneNo==null || phoneNo.Length != 10)
                {
                    lblMessage.Text = "Invalid WhatsApp Number";
                    lblMessage.Visible = true;
                    return;
                }
                phoneNo = "91" + phoneNo;
                string otp = GenerateOtp(userId,phoneNo);
                if (otp!="")
                {

                    lblMessage.Text = "OTP sent successfully on your whatsapp Number";
                    lblMessage.Visible = true;
                    txtPwd.Text = pwd;
                }
            }
            else
            {
                lblMessage.Text = "Invalid User Name Or Password.";
                lblMessage.Visible = true;
                return;
            }

            
        }

        private string GenerateOtp(long userId,string phoneNo)
        {
            
            int _min = 100000;
            int _max = 999999;

            Random _rdm = new Random();
            string otp = _rdm.Next(_min, _max).ToString();
            new CommonDAO().UpdateOtp(userId, otp);

            SendOtp(phoneNo, otp); 
            return otp;
 
        }


        private int SendOtp(string PhoneNo,string otp)
        {
            string accessKey = ConfigurationManager.AppSettings["WatiAccessKey"].ToString();
            string baseAddress = ConfigurationManager.AppSettings["WatiBaseUrl"].ToString();
            string otpTemplate = ConfigurationManager.AppSettings["WatiOtpTemplate"].ToString();
            var client = new RestClient(baseAddress + "/api/v1/sendTemplateMessage?whatsappNumber=" + PhoneNo);
             
            System.Net.ServicePointManager.Expect100Continue = true;
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", accessKey);
            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("application/json", "{\"parameters\":[{\"name\":\"1\",\"value\":\"" + otp + "\"}],\"template_name\":\"" + otpTemplate + "\",\"broadcast_name\":\"Ash Logistics\"}", ParameterType.RequestBody);
            var response = client.Execute(request);
            if (response.Content.Contains("result\":true"))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
