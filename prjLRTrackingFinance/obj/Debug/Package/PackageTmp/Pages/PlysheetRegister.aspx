﻿<%@ Page Title="Plysheet Register" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="PlysheetRegister.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PlysheetRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        PlySheet Register
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="720px">
                                <tr>
                                    <td width="50px">
                                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                            OnClick="btnExcel_Click" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label1" Text="Total Records : " runat="server" CssClass="NormalTextBold"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="lblTotalRecords" Text="0" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="overflow: auto">
                                <asp:GridView ID="grdPacklistView" runat="server" Width="720px" AutoGenerateColumns="false"
                                    AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10">
                                    <Columns>
                                        <asp:BoundField DataField="SupplierName" HeaderText="SupplierName" SortExpression="SupplierName"
                                            ItemStyle-Width="300px" />
                                        <asp:BoundField DataField="IssueQty" HeaderText="Issue Qty" SortExpression="IssueQty"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="RcptQty" HeaderText="Rcpt Qty" SortExpression="RcptQty"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="DamagedQty" HeaderText="Lost Qty" SortExpression="LostQty"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="BalQty" HeaderText="Bal Qty" SortExpression="BalQty"
                                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
