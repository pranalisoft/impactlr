﻿<%@ Page Title="Pending Advance" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="PendingAdvanceReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PendingAdvanceReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button2" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none;
                width: 750px; height: 650px">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    E-Waybill
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Image ID="Image1" runat="server" Width="700px" Height="550px" />
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button" OnClick="btnOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="msgpopAuth" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="Button2" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:HiddenField ID="hdfldLRId" runat="server" />
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to approve this record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="Panel1" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Pending Advance Report
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <%--<td width="30px">
                                        <asp:Label ID="Label18" Text="From" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                    </td>
                                    <td width="30px">
                                        <asp:Label ID="Label17" Text="To" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                    </td>
                                    <td width="50px">
                                        <asp:Label ID="lbl" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="250px">
                                        <asp:DropDownList ID="cmbBranch" runat="server" CssClass="NormalTextBold" TabIndex="5"
                                            Width="250px" AppendDataBoundItems="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="50px">
                                        <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>--%>
                                    <td width="50px">
                                        <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                            OnClick="btnExcel_Click" />
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label1" Text="Total Records : " runat="server" CssClass="NormalTextBold"
                                            Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="lblTotalRecords" Text="0" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region" style="overflow: auto">
                                <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="false"
                                    AllowPaging="true" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="FileAttached,EwayBillFileName,Id" OnPageIndexChanging="grdPacklistView_PageIndexChanging"
                                    OnRowDataBound="grdPacklistView_RowDataBound" OnRowCommand="grdPacklistView_RowCommand"
                                    ShowFooter="True">
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                    <Columns>
                                        <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
                                        <asp:BoundField DataField="LRNo" HeaderText="LRNo" SortExpression="LRNo" />
                                        <asp:BoundField DataField="RefNo" HeaderText="RefNo" SortExpression="RefNo" />
                                        <asp:BoundField DataField="LRDate" HeaderText="LRDate" SortExpression="LRDate" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="LoadingLocation" HeaderText="LoadingLocation" SortExpression="LoadingLocation" />
                                        <asp:BoundField DataField="LoadingDate" HeaderText="LoadingDate" SortExpression="LoadingDate"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Destination" HeaderText="Destination" SortExpression="Destination" />
                                        <asp:BoundField DataField="Supplier" HeaderText="Supplier" SortExpression="Supplier" />
                                        <asp:BoundField DataField="VehicleNo" HeaderText="VehicleNo" SortExpression="VehicleNo" />
                                        <asp:BoundField DataField="VehicleType" HeaderText="VehicleType" SortExpression="VehicleType" />
                                        <asp:BoundField DataField="VehicleBuyingCharges" HeaderText="VehicleBuyingCharges"
                                            SortExpression="VehicleBuyingCharges" />
                                        <asp:BoundField DataField="VehicleSellingCharges" HeaderText="VehicleSellingCharges"
                                            SortExpression="VehicleSellingCharges" />
                                        <asp:BoundField DataField="Unloading Charges" HeaderText="Unloading Charges" SortExpression="Unloading Charges" />
                                        <asp:BoundField DataField="CashAdvance" HeaderText="CashAdvance" SortExpression="CashAdvance" />
                                        <asp:BoundField DataField="CashBalance" HeaderText="CashBalance" SortExpression="CashBalance" />
                                        <asp:BoundField DataField="FuelAdvance" HeaderText="FuelAdvance" SortExpression="FuelAdvance" />
                                        <asp:BoundField DataField="FuelAdvancePaid" HeaderText="FuelAdvancePaid" SortExpression="FuelAdvancePaid" />
                                        <asp:BoundField DataField="FuelBalance" HeaderText="FuelBalance" SortExpression="FuelBalance" />
                                        <asp:BoundField DataField="PetrolPump" HeaderText="PetrolPump" SortExpression="PetrolPump" />
                                        <asp:BoundField DataField="EwayBillNo" HeaderText="EwayBillNo" SortExpression="EwayBillNo" />
                                        <asp:BoundField DataField="EwayBillDate" HeaderText="EwayBillDate" SortExpression="EwayBillDate"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:ButtonField ButtonType="Link" CommandName="FileAttached" DataTextField="FileAttached"
                                            HeaderText="Eway Bill File" SortExpression="FileAttached">
                                            <ItemStyle Width="70px" />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Link" CommandName="PendingAdvanceApproved" DataTextField="PendingAdvanceApproved"
                                            HeaderText="Approval" SortExpression="PendingAdvanceApproved">
                                            <ItemStyle Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                </asp:GridView>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
