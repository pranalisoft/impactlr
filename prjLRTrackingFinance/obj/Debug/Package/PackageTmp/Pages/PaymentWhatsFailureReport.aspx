﻿<%@ Page Title="WhatsApp Messege Deviation Report" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="PaymentWhatsFailureReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PaymentWhatsFailureReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            WhatsApp Messege Deviation Register
                        </td>
                    </tr>
                </table>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="3" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="4"
                                    OnClick="btnExcel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="3">
                                <asp:GridView ID="grdMain" runat="server" Width="70%" AutoGenerateColumns="false"
                                    CssClass="grid" AllowPaging="True" AllowSorting="true" DataKeyNames="" EmptyDataText="No Records Found."
                                    OnPageIndexChanging="grdMain_PageIndexChanging" OnSorting="grdMain_Sorting" OnRowDataBound="grdMain_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="40px" />
                                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PaymentNo" HeaderText="Srl." SortExpression="PaymentNo"
                                            ReadOnly="True" ItemStyle-Width="150px" />
                                        <%--  <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="RefNo"
                                            HeaderText="No." SortExpression="RefNo" ItemStyle-Width="100px" />--%>
                                        <asp:BoundField DataField="PaymentDate" HeaderText="Date" SortExpression="PaymentDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="MobileNo" HeaderText="Mobile No" SortExpression="MobileNo"
                                            ReadOnly="True" ItemStyle-Width="120px" />
                                        <asp:BoundField DataField="EntryUser" HeaderText="Entered By" SortExpression="EntryUser"
                                            ReadOnly="True" />
                                    </Columns>
                                    <RowStyle Height="10px" />
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {
            $('#<%= txtFromDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtFromDate.ClientID %>').val('');
                }
            });

            $('#<%= txtToDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtToDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });        
    </script>
</asp:Content>
