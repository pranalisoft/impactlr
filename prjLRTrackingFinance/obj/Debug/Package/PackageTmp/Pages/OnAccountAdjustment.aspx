﻿<%@ Page Title="On Account Adjustment" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="OnAccountAdjustment.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.OnAccountAdjustment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            On Account Adjustment
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="view1" runat="server">
                            <div class="buttons_top">
                                <asp:Button ID="btnAdd" Text="Add" runat="server" CssClass="button" CommandName="Add"
                                    OnCommand="EntryForm_Command" TabIndex="1" />
                                <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="button" CommandName="Delete"
                                    OnCommand="EntryForm_Command" TabIndex="2" />
                            </div>
                            <div class="grid_top_region">
                                <div class="grid_top_region_lft">
                                    <asp:CheckBox ID="chkDateFilter" runat="server" AutoPostBack="true" Text="Date Filter"
                                        TabIndex="3" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtFromDate" Width="100px" runat="server" TabIndex="3" />
                                    <asp:CalendarExtender ID="Calendar_From" TargetControlID="txtFromDate" PopupButtonID="imgBtnCalcPopupFrom"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupFrom" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="4" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtToDate" Width="100px" runat="server" TabIndex="5" />
                                    <asp:CalendarExtender ID="Calendar_To" TargetControlID="txtToDate" PopupButtonID="imgBtnCalcPopupTo"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupTo" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="6" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnFilter" Text="Show" runat="server" CssClass="button" CommandName="Filter"
                                        OnCommand="Filter_Command" TabIndex="7" ValidationGroup="save" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnClearFilter" Text="Clear Filter" runat="server" CssClass="button"
                                        CommandName="ClearFilter" OnCommand="Filter_Command" TabIndex="8" />
                                </div>
                                <div class="grid_top_region_rght">
                                    Records :
                                    <asp:Label ID="lblRecCount" Text="0" runat="server" />
                                    <%--<table border="0" class="cnt_region">
                                    <tr>
                                        <td>
                                            Records :
                                            <asp:Label ID="lblRecCount" Text="1000" runat="server" />
                                        </td>
                                    </tr>
                                </table>--%>
                                </div>
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdViewIndex" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    AllowPaging="True" DataKeyNames="Srl,RefNo,TranDate,CustomerID,CustomerName,TotalAmount,PaymentMode,UserName,BranchId,ChqNo,ChqDate,PayMode"
                                    OnRowCommand="grdViewIndex_RowCommand" OnRowDataBound="grdViewIndex_RowDataBound"
                                    EmptyDataText="No records found." Width="100%" OnPageIndexChanging="grdViewIndex_PageIndexChanging"
                                    PageSize="15">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                    AutoPostBack="true" TabIndex="5" /></HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" TabIndex="6" /></ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="28px" />
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RefNo" HeaderText="Srl." SortExpression="RefNo" ReadOnly="True"
                                            ItemStyle-Width="80px" />
                                        <%--  <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="RefNo"
                                            HeaderText="No." SortExpression="RefNo" ItemStyle-Width="100px" />--%>
                                        <asp:BoundField DataField="TranDate" HeaderText="Date" SortExpression="TranDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="PayMode" HeaderText="Payment Mode" SortExpression="PayMode"
                                            ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ChqNo" HeaderText="Chq. No." SortExpression="ChqNo" ReadOnly="True"
                                            ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ChqDate" HeaderText="Chq. Date" SortExpression="ChqDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                            ReadOnly="True" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="UserName" HeaderText="Entered By" SortExpression="UserName"
                                            ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:ButtonField CommandName="PayDetails" HeaderText="" Text="Details" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <%--  <asp:ButtonField CommandName="Report" HeaderText="" Text="Document" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />--%>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                <div class="grid_top_region">
                                    <div class="grid_top_region_lft">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="grid_top_region_rght">
                                        Records :
                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                    </div>
                                </div>
                                <div class="grid_region">
                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="InvoiceID,InvoiceNo,InvoiceDate,InvoiceAmount,AdjAmt,DetailSrl"
                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." SortExpression="InvoiceNo"
                                                ReadOnly="True" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="InvoiceAmount" HeaderText="Inv. Amount" SortExpression="InvoiceAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="AdjAmt" HeaderText="Adj. Amount" SortExpression="AdjAmt"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TDSAmt" HeaderText="TDS. Amount" SortExpression="TDSAmt"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                            </asp:Panel>
                        </asp:View>
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label4" Text="Receipt No." runat="server" AssociatedControlID="cmbRcpt" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbRcpt" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            TabIndex="1" AutoPostBack="true" CssClass="WindowsStyle" RenderMode="Block" OnSelectedIndexChanged="cmbRcpt_SelectedIndexChanged">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="cmbRcpt"
                                                            ErrorMessage="Please select receipt no. from the list" ClientValidationFunction="ValidatorCombobox"
                                                            Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="Validatorcalloutextender1" runat="server" Enabled="True"
                                                            TargetControlID="CustomValidator1">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td colspan="4">
                                                        <asp:TextBox ID="txtDate" Width="100px" CssClass="NormalTextBold" runat="server"
                                                            TabIndex="2" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="3" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the receipt date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label1" Text="Customer" runat="server" AssociatedControlID="cmbCust" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbCust" runat="server" Enabled="false" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" AppendDataBoundItems="true"
                                                            Width="300px" MaxLength="100" OnSelectedIndexChanged="cmbCust_SelectedIndexChanged"
                                                            TabIndex="4" AutoPostBack="true" CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbCust" ErrorMessage="Please select customer from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top" style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top" style="width: 120px">
                                                        <asp:Label ID="Label3" Text="Payment Mode" runat="server" Enabled="false" AssociatedControlID="cmbPaymentMode" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbPaymentMode" runat="server" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" Width="200px" MaxLength="100"
                                                            TabIndex="5" CssClass="WindowsStyle" RenderMode="Block" OnSelectedIndexChanged="cmbPaymentMode_SelectedIndexChanged"
                                                            AutoPostBack="True">
                                                            <asp:ListItem Text="" Value="-1" />
                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                            <asp:ListItem Value="2">Cheque</asp:ListItem>
                                                            <asp:ListItem Value="3">DD</asp:ListItem>
                                                            <asp:ListItem Value="4">Net Banking</asp:ListItem>
                                                            <asp:ListItem Value="5">Debit Card</asp:ListItem>
                                                            <asp:ListItem Value="6">Credit Card</asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvpaymode" runat="server" ErrorMessage="Please select payment mode from the list"
                                                            ValidationGroup="save" ControlToValidate="cmbPaymentMode" Display="None" SetFocusOnError="true"
                                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvpaymodeCall" runat="server" Enabled="True" TargetControlID="rfvpaymode">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblchqddDate" Text="Cheque Date" Enabled="false" runat="server" AssociatedControlID="txtchqdt" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtchqdt" Width="100px" runat="server" TabIndex="6" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtchqdt" PopupButtonID="imgBtnCalcPopupPODate1"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate1" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Height="22px"
                                                            ImageAlign="AbsMiddle" Width="22px" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfvchqDt" runat="server" ErrorMessage="Please select the Cheque/DD date"
                                                            ValidationGroup="save" ControlToValidate="txtchqdt" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvchqDtCall" runat="server" Enabled="True" TargetControlID="rfvchqDt">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trBank" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblBank" Text="Bank Name" runat="server" AssociatedControlID="cmbBank" />
                                                    </td>
                                                    <td>
                                                        <asp:ComboBox ID="cmbBank" runat="server" Enabled="false" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDown" AppendDataBoundItems="true" ItemInsertLocation="Append"
                                                            Width="300px" MaxLength="100" TabIndex="7" CssClass="WindowsStyle" RenderMode="Block">
                                                            <asp:ListItem Value=""></asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="CustomBank" runat="server" ControlToValidate="cmbBank" ErrorMessage="Please select bank from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true" Enabled="false"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="CustomBankCall" runat="server" Enabled="True" TargetControlID="CustomBank">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trChqNo" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblChqDDNo" Text="Cheque No" runat="server" AssociatedControlID="TxtChqNo" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtChqNo" Width="300px" Enabled="false" CssClass="NormalTextBold"
                                                            runat="server" TabIndex="8" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the Cheque/DD/Transaction no."
                                                            ValidationGroup="save" ControlToValidate="TxtChqNo" Display="None" SetFocusOnError="true"
                                                            Enabled="false"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true" TargetControlID="reqFVChqNo">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <caption>
                                                    <br />
                                                    <tr class="control_row">
                                                        <td>
                                                            <span class="req_mark">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Amount" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAmount" runat="server" Enabled="false" AutoPostBack="True" CssClass="NormalTextBold"
                                                                MaxLength="100" OnTextChanged="txtAmount_TextChanged" TabIndex="9" Width="100px"
                                                                Style="text-align: right" />
                                                            <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                            <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                            <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                                TargetControlID="txtAmount" ValidChars=".">
                                                            </asp:FilteredTextBoxExtender>
                                                            <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                                ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                            <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                                TargetControlID="regExAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                        </td>
                                                    </tr>
                                                </caption>
                                            </table>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table width="100%">
                                                <asp:GridView ID="GrdPaymentStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                    DataKeyNames="" EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                    <Columns>
                                                        <asp:BoundField DataField="BillCnt" HeaderText="Total Bills" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="BillAmt" HeaderText="Bill Amount" ItemStyle-Width="200px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="PaidAmt" HeaderText="Paid Amount" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="Outstanding" HeaderText="OutStanding" ItemStyle-Width="100px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="header" />
                                                    <RowStyle CssClass="row" />
                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                </asp:GridView>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <br />
                            <div class="grid_top_region">
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdPaymentSupl" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt"
                                    EmptyDataText="No records found." Width="100%" TabIndex="100">
                                    <Columns>
                                        <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Date" ItemStyle-Width="100px"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="ReceivedAmount" HeaderText="Paid" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <%--<asp:BoundField DataField="BalAmt" HeaderText="Balance" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:TemplateField HeaderText="Balance">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalAmt" Style="text-align: right" TabIndex="10" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false" Text='<%#Eval("BalAmt") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtAdjAmt" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExAmtCall" runat="server" Enabled="True" TargetControlID="regExAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompQty" runat="server" ErrorMessage="Adjust amount cannot be greater than balance amount"
                                                    ControlToCompare="txtBalAmt" ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="true"
                                                    ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:ValidatorCalloutExtender ID="CompQtyCall" runat="server" Enabled="True" TargetControlID="CompQty"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TDS">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTDSAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterAmttds" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtTDSAmt" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmtTDS" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtTDSAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExAmtTDSCall" runat="server" Enabled="True"
                                                    TargetControlID="regExAmtTDS" PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </div>
                            <br />
                            <table class="control_set" style="width: 100%">
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
                <div class="buttons_bottom">
                    <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                        ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="10" />
                    <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                        OnCommand="EntryForm_Command" TabIndex="11" />
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbCust" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cmbPaymentMode" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });

            $('#<%= txtchqdt.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtchqdt.ClientID %>').val('');
                }
            });

        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
