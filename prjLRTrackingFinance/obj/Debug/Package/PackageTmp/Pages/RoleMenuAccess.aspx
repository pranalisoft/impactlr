﻿<%@ Page Title="Role Menu Access" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="RoleMenuAccess.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.RoleMenuAccess" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function SelectSingleRadiobutton(control) {
            var cntrlId = control.id;
            var gv = document.getElementById("<%= grdViewDashboardGrids.ClientID %>");
            var rdBtnList = gv.getElementsByTagName("input");
            //alert(rdBtnList.length);
            for (i = 0; i < rdBtnList.length; i++) {
                //alert(rdBtnList[i].id);  
                if (rdBtnList[i].type == "checkbox" && rdBtnList[i].id.indexOf('_chkSelDefault') >= 0 && rdBtnList[i].id != cntrlId) {
                    //alert('hi');                  
                    rdBtnList[i].checked = false;
                }
            }
        }

        function checkboxclick(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_chkSelectGrid'));
            var rowindex = Rowid.substring(4, Rowid.length);
          
            var gv = document.getElementById("<%= grdViewDashboardGrids.ClientID %>");
            var tb = gv.getElementsByTagName("input");
            var frt = 0;
            for (var i = 0; i < tb.length; i++) {
                if (tb[i].type == "checkbox" && tb[i].id.indexOf('_ctl' + rowindex + '_chkSelDefault') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                    tb[i].checked = false;                   
                }   
            }            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="btnPopUpYesNo" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlPopupYesNo" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Information
                </div>
                <div align="center" style="padding: 5px">
                    <br />
                    <asp:Label ID="lblError" runat="server" Text="Menus Assigned Successfully." CssClass="NormalTextBold"></asp:Label>
                    <br />
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="OK" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    &nbsp
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender runat="server" ID="popUpYesNo" DynamicServicePath="" Enabled="True"
                TargetControlID="btnPopUpYesNo" PopupControlID="pnlPopupYesNo" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                Role Menu Access
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                    <div class="msg_region">
                        <iControl:MsgPanel ID="MsgPanel" runat="server" />
                        <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                    </div>
                    <div style="padding-left: 15px;">
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Role" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRoles" runat="server" AppendDataBoundItems="true" TabIndex="2"
                                            Width="300px" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged1" AutoPostBack="True">
                                            <asp:ListItem Value="-1">Select Roles</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;<asp:RequiredFieldValidator ID="reqFldddlRoles" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="ddlRoles" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdEmpRoleMst" runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="Sel, SName, Srl, IName " EmptyDataText="No Records Found." BackColor="#DCE8F5"
                                            Font-Size="11px" OnRowDataBound="grdEmpRoleMst_RowDataBound">
                                            <RowStyle Height="10px" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SName" HeaderText="Sections" SortExpression="SName">
                                                    <ItemStyle Width="200px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IName" HeaderText="Menu Option" SortExpression="IName">
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                            <PagerSettings Mode="Numeric" />
                                        </asp:GridView>
                                    </td>
                                    <td style="width: 50px">
                                        &nbsp;
                                    </td>
                                    <td style="vertical-align:top">
                                        <asp:GridView ID="grdViewDashboardGrids" runat="server" Width="356px" CssClass="grid"
                                            AutoGenerateColumns="False" DataKeyNames="Sel,GridDescription,Srl,IsDefault " EmptyDataText="No Records Found."
                                            BackColor="#DCE8F5" Font-Size="11px">
                                            <RowStyle Height="10px" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelectGrid" onclick="checkboxclick(this);" Checked='<%# Eval("Sel").ToString()=="1"?true:Eval("Sel").ToString()=="true"?true:false %>'/></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="GridDescription" HeaderText="Dashboard Grid" SortExpression="GridDescription">
                                                    <ItemStyle Width="280px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Default">
                                                    <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkSelDefault" Enabled="false" OnClick="SelectSingleRadiobutton(this);" Checked='<%# Eval("IsDefault").ToString()=="1"?true:Eval("IsDefault").ToString()=="true"?true:false %>'/></ItemTemplate>
                                                        <%--<asp:RadioButton ID="RdbDefault" runat="server" GroupName="DefaultGroup" Enabled="false" OnClick="javascript:SelectSingleRadiobutton(this.id)" Checked='<%# Eval("IsDefault").ToString()=="1"?true:Eval("IsDefault").ToString()=="true"?true:false %>'/></ItemTemplate>--%>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                            <PagerSettings Mode="Numeric" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="3" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="4" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlRoles" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
