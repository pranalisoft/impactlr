﻿<%@ Page Title="Update LR Amount(s)" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="UpdateLRAmounts.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateLRAmounts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Update LR Amounts
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div>
                    <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                        <asp:Label ID="lblMsg" Text="" runat="server" />
                    </asp:Panel>
                </div>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td valign="top" style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top" style="width: 170px">
                                                        <asp:Label ID="Label1" Text="LR No" runat="server" AssociatedControlID="cmbLR" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbLR" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbLR_SelectedIndexChanged" TabIndex="1" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbLR" ErrorMessage="Please select LR from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td colspan="3">
                                                        <asp:Label ID="Label7" runat="server" Text="Freight" Font-Bold="true" Font-Underline="true" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl" runat="server" AssociatedControlID="txtFreightAmount" Text="Freight(Selling Rate)" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFreightAmount" runat="server" AutoPostBack="false" MaxLength="10"
                                                            TabIndex="2" Width="100px" Style="text-align: right" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtFreightAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                            TargetControlID="txtFreightAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtFreightAmount"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" AssociatedControlID="txtSupplierAmount" Text="Supplier Amount(Buying Rate)" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtSupplierAmount" runat="server" AutoPostBack="false" MaxLength="10"
                                                            TabIndex="3" Width="100px" Style="text-align: right" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSupplierAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtSupplierAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSupplierAmount"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td colspan="3">
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td colspan="3">
                                                        <asp:Label ID="Label6" runat="server" AssociatedControlID="txtCashAdvance" Text="Advance"
                                                            Font-Bold="true" Font-Underline="true" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" AssociatedControlID="txtCashAdvance" Text="Cash Advance" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCashAdvance" runat="server" AutoPostBack="false" MaxLength="10"
                                                            TabIndex="4" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            onchange="txtchanged(this);" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCashAdvance"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtCashAdvance" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCashAdvance"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" AssociatedControlID="txtFuelAdvance" Text="Fuel Advance" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFuelAdvance" runat="server" AutoPostBack="false" MaxLength="10"
                                                            TabIndex="5" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            onchange="txtchanged(this);" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFuelAdvance"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtFuelAdvance" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtFuelAdvance"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label5" runat="server" AssociatedControlID="txtTotalAdvance" Text="Total Advance" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalAdvance" runat="server" AutoPostBack="false" MaxLength="10"
                                                            Enabled="false" TabIndex="6" Width="100px" Style="text-align: right" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTotalAdvance"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtTotalAdvance" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtTotalAdvance"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblChqDDNo" Text="remarks" runat="server" AssociatedControlID="txtRemarks" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRemarks" Width="300px" runat="server" TabIndex="7" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the Remarks."
                                                            ValidationGroup="save" ControlToValidate="txtRemarks" Display="None" SetFocusOnError="true"
                                                            Enabled="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true" TargetControlID="reqFVChqNo">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <br />
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" ValidationGroup="save"
                                    TabIndex="9" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="button" TabIndex="10"
                                    OnClick="btnCancel_Click" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbLR" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function txtchanged(control) {
            var CashAmt = 0;
            var FuleAmt = 0;
            if (parseFloat(document.getElementById("<%= txtCashAdvance.ClientID %>").value) > 0) {
                CashAmt = document.getElementById("<%= txtCashAdvance.ClientID %>").value;
            }
            if (parseFloat(document.getElementById("<%= txtFuelAdvance.ClientID %>").value) > 0) {
                FuleAmt = document.getElementById("<%= txtFuelAdvance.ClientID %>").value;
            }
            document.getElementById("<%= txtTotalAdvance.ClientID %>").value = (parseFloat(CashAmt) + parseFloat(FuleAmt)).toFixed(2);
        }
    </script>
</asp:Content>
