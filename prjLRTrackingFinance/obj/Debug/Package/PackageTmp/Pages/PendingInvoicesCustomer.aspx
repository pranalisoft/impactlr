﻿<%@ Page Title="Pending For Billing" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="PendingInvoicesCustomer.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.PendingInvoicesCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Pending LRs For Billing Report
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <table class="control_set" width="100%">                   
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label22" runat="server" Text="Branch" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td colspan="3">
                                <ajaxToolkit:ComboBox ID="cmbBranch" runat="server" AutoCompleteMode="SuggestAppend"
                                    Width="250px" MaxLength="200" TabIndex="5" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                    DropDownStyle="DropDown" RenderMode="Block">
                                </ajaxToolkit:ComboBox>                           
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td width="80px">
                            </td>
                            <td colspan="3">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="6" ValidationGroup="save" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                    OnClick="btnExcel_Click"/>
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;
                  <div class="grid_region">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>                            
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="3">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="true"
                                    CssClass="grid" AllowPaging="True" AllowSorting="true" DataKeyNames=""
                                    EmptyDataText="No Records Found." 
                                    OnPageIndexChanging="grdMain_PageIndexChanging">
                                    <RowStyle Height="10px" />                                    
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>