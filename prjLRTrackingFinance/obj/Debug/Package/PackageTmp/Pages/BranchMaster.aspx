﻿<%@ Page Title="Line Of Business Master" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="BranchMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.BranchMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Line Of Business Master
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Line Of Business Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table style="width: 99.5%">
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="Name" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBranchName" runat="server" MaxLength="50" TabIndex="1" Width="400px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtBranchName" SetFocusOnError="True" ValidationGroup="save"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Address Line1" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAddress1" runat="server" Width="400px" TabIndex="2" CssClass="NormalTextBold"
                                            MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Address Line 1'
                                            ControlToValidate="txtAddress1" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text="Address Line2" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAddress2" runat="server" Width="400px" TabIndex="3" CssClass="NormalTextBold"
                                            MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text="Address Line3" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAddress3" runat="server" Width="400px" TabIndex="4" CssClass="NormalTextBold"
                                            MaxLength="100"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="Landline No." CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLandline" runat="server" Width="300px" TabIndex="5" CssClass="NormalTextBold"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Mobile No." CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMobile" runat="server" Width="300px" TabIndex="6" CssClass="NormalTextBold"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="Email ID" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmailID" runat="server" Width="300px" TabIndex="7" CssClass="NormalTextBold"
                                            MaxLength="100"></asp:TextBox>
                                        <asp:RegularExpressionValidator runat="server" ID="regexpEmail" SetFocusOnError="true"
                                            ValidationGroup="save" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter valid email id'
                                            ControlToValidate="txtEmailId" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="GST No." CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCSTNo" runat="server" Width="200px" TabIndex="8" CssClass="NormalTextBold"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="PAN" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVAT" runat="server" Width="200px" TabIndex="9" CssClass="NormalTextBold"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 15px;">
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Unit of Measure" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlUOM" runat="server" TabIndex="10">
                                            <asp:ListItem Value="Veh">Vehicle</asp:ListItem>
                                            <asp:ListItem>Ton</asp:ListItem>
                                            <asp:ListItem Value="Pkg">Package</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                            Width="250px" MaxLength="200" TabIndex="11" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label16" runat="server" Text="Plywood Applicable" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkPly" runat="server" TabIndex="12" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label11" runat="server" Text="LOB Group" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbLOBGroup" runat="server" AutoCompleteMode="SuggestAppend"
                                            Width="250px" MaxLength="200" TabIndex="13" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label13" runat="server" Text="Pending POD" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkPendingPOD" runat="server" TabIndex="14" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Active" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" TabIndex="15" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="16" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="17" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="18" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="5" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="7" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,Name,UOM,BillingCompanySrl,PlyApplicable,Active,AddressLine1,AddressLine2,AddressLine3,Landline,Mobile,EmailId,GSTNo,PAN,LOBGroupId,PendingPODShow"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand">
                                <Columns>
                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Name"
                                        SortExpression="Name" ItemStyle-Width="250px" />
                                    <asp:BoundField DataField="AddressLine1" HeaderText="Address 1" SortExpression="AddressLine1"/>
                                        <asp:BoundField DataField="AddressLine2" HeaderText="Address 2" SortExpression="AddressLine2"/>
                                        <asp:BoundField DataField="AddressLine3" HeaderText="Address 3" SortExpression="AddressLine3"/>
                                    <asp:BoundField DataField="Landline" HeaderText="Landline" SortExpression="Landline"
                                        ItemStyle-Width="200px" />
                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" ItemStyle-Width="200px" />
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
