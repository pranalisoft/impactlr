﻿<%@ Page Title="Update Supplier Advance" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="UpdateSupplierAdvanceMultiple.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateSupplierAdvanceMultiple" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Update Supplier Advance
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="1" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="2" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the Payment date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label1" Text="Supplier" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbSupl" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupl_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbSupl" ErrorMessage="Please select Supplier from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top" style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top" style="width: 120px">
                                                        <asp:Label ID="Label3" Text="Payment Mode" runat="server" AssociatedControlID="cmbPaymentMode" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbPaymentMode" runat="server" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" Width="200px" MaxLength="100"
                                                            TabIndex="3" CssClass="WindowsStyle" RenderMode="Block" OnSelectedIndexChanged="cmbPaymentMode_SelectedIndexChanged"
                                                            AutoPostBack="True">
                                                            <asp:ListItem Text="" Value="-1" />
                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                            <asp:ListItem Value="2">Cheque</asp:ListItem>
                                                            <asp:ListItem Value="3">DD</asp:ListItem>
                                                            <asp:ListItem Value="4">Net Banking</asp:ListItem>
                                                            <asp:ListItem Value="5">Debit Card</asp:ListItem>
                                                            <asp:ListItem Value="6">Credit Card</asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvpaymode" runat="server" ErrorMessage="Please select payment mode from the list"
                                                            ValidationGroup="save" ControlToValidate="cmbPaymentMode" Display="None" SetFocusOnError="true"
                                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvpaymodeCall" runat="server" Enabled="True" TargetControlID="rfvpaymode">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblchqddDate" Text="Cheque Date" runat="server" AssociatedControlID="txtchqdt" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtchqdt" Width="100px" runat="server" TabIndex="4" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtchqdt" PopupButtonID="imgBtnCalcPopupPODate1"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate1" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Height="22px"
                                                            ImageAlign="AbsMiddle" Width="22px" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfvchqDt" runat="server" ErrorMessage="Please select the Cheque/DD date"
                                                            ValidationGroup="save" ControlToValidate="txtchqdt" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvchqDtCall" runat="server" Enabled="True" TargetControlID="rfvchqDt">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trBank" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblBank" Text="Bank Name" runat="server" AssociatedControlID="cmbBank" />
                                                    </td>
                                                    <td>
                                                        <asp:ComboBox ID="cmbBank" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDown"
                                                            AppendDataBoundItems="true" ItemInsertLocation="Append" Width="300px" MaxLength="100"
                                                            TabIndex="6" CssClass="WindowsStyle" RenderMode="Block">
                                                            <asp:ListItem Value=""></asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="CustomBank" runat="server" ControlToValidate="cmbBank" ErrorMessage="Please select bank from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true" Enabled="false"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="CustomBankCall" runat="server" Enabled="True" TargetControlID="CustomBank">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trChqNo" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblChqDDNo" Text="Cheque No" runat="server" AssociatedControlID="TxtChqNo" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TxtChqNo" Width="300px" runat="server" TabIndex="7" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the Cheque/DD/Transaction no."
                                                            ValidationGroup="save" ControlToValidate="TxtChqNo" Display="None" SetFocusOnError="true"
                                                            Enabled="false"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true" TargetControlID="reqFVChqNo">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label5" runat="server" AssociatedControlID="txtAmount" Text="Cash Amount" />
                                                    </td>
                                                    <td style="width: 300px">
                                                        <asp:TextBox ID="txtCashAmount" runat="server" AutoPostBack="True" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="false" OnTextChanged="txtCashAmount_TextChanged" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCashAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtCashAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCashAmount"
                                                            Display="None" ErrorMessage="Please enter valid cash amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,11}|\d{1,8}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <%-- <asp:CompareValidator ID="compCashAmt" runat="server" Enabled="true" ForeColor="Red"
                                                            ControlToValidate="txtCashAmount" ControlToCompare="txtCashAmountBal" Operator="LessThanEqual"
                                                            SetFocusOnError="true" ValidationGroup="save" Type="Double" Display="Dynamic"
                                                            ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle">Cash Amount cannot be greater than Balance'></asp:CompareValidator>--%>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" AssociatedControlID="txtAmount" Text="Fuel Amount" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFuelAmount" runat="server" AutoPostBack="True" MaxLength="100"
                                                            TabIndex="8" Width="100px" Style="text-align: right" CssClass="NormalTextBold"
                                                            Enabled="false" OnTextChanged="txtFuelAmount_TextChanged" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFuelAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" Enabled="True"
                                                            TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtFuelAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFuelAmount"
                                                            Display="None" ErrorMessage="Please enter valid fuel amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,11}|\d{1,8}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <%--<asp:CompareValidator ID="CompFuel" runat="server" Enabled="true" ForeColor="Red"
                                                            ControlToValidate="txtFuelAmount" ControlToCompare="txtFuelAmountBal" Operator="LessThanEqual"
                                                            SetFocusOnError="true" ValidationGroup="save" Type="Double" Display="Dynamic"
                                                            ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle">Fuel Amount cannot be greater than Balance'></asp:CompareValidator>--%>
                                                    </td>
                                                </tr>
                                                <caption>
                                                    <br />
                                                    <tr class="control_row">
                                                        <td>
                                                            <span class="req_mark">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Amount" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="false" CssClass="NormalTextBold"
                                                                Enabled="false" MaxLength="100" Style="text-align: right" TabIndex="8" Width="100px" />
                                                            <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                            <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                            <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                                TargetControlID="txtAmount" ValidChars=".">
                                                            </asp:FilteredTextBoxExtender>
                                                            <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtAmount"
                                                                Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                                ValidationExpression="^(?:\d{1,11}|\d{1,8}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                            <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                                TargetControlID="regExAmount">
                                                            </asp:ValidatorCalloutExtender>
                                                        </td>
                                                    </tr>
                                                </caption>
                                            </table>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table width="100%">
                                                <asp:GridView ID="GrdPaymentStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                    DataKeyNames="" EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                    <Columns>
                                                        <asp:BoundField DataField="BillCnt" HeaderText="Total LR" ItemStyle-Width="150px"
                                                            ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="AdvanceAmt" HeaderText="Advance Amount" ItemStyle-Width="150px"
                                                            ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="PaidAmt" HeaderText="Paid Amount" ItemStyle-Width="150px"
                                                            ItemStyle-HorizontalAlign="Right" />
                                                        <asp:BoundField DataField="Outstanding" HeaderText="OutStanding" ItemStyle-Width="150px"
                                                            ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="header" />
                                                    <RowStyle CssClass="row" />
                                                    <AlternatingRowStyle CssClass="alter_row" />
                                                </asp:GridView>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdPaymentSupl" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="Srl" EmptyDataText="No records found." ShowFooter="True" ShowHeader="true"
                                    Width="100%" TabIndex="100">
                                    <Columns>
                                        <asp:BoundField DataField="LRNo" HeaderText="LR No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="LRDate" HeaderText="Date" ItemStyle-Width="100px" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="CashAmount" HeaderText="Cash Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="FuelAmount" HeaderText="Fuel Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PaidCashAmount" HeaderText="Paid Cash" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PaidFuelAmount" HeaderText="Paid Fuel" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <%--<asp:BoundField DataField="BalAmt" HeaderText="Balance" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:TemplateField HeaderText="Balance Cash">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalCashAmount" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false" Text='<%#Eval("BalCashAmount") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Balance Fuel">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalFuelAmount" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false" Text='<%#Eval("BalFuelAmount") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted Cash">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjCashAmount" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" onchange="calculate();" AutoPostBack="false"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtAdjCashAmount" Enabled="True">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmt" runat="server" ErrorMessage="Please  enter valid cash amount"
                                                    ControlToValidate="txtAdjCashAmount" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="regExAmtCall" runat="server" Enabled="True"
                                                    TargetControlID="regExAmt" PopupPosition="Left">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompQty" runat="server" ErrorMessage="Adjust cash amount cannot be greater than balance cash amount"
                                                    ControlToCompare="txtBalCashAmount" ControlToValidate="txtAdjCashAmount" Display="None"
                                                    SetFocusOnError="true" ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="CompQtyCall" runat="server" Enabled="True"
                                                    TargetControlID="CompQty" PopupPosition="Left">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted Fuel">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjFuelAmount" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" onchange="calculate()" AutoPostBack="false"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterFuelAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtAdjFuelAmount" Enabled="True">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExFuelAmt" runat="server" ErrorMessage="Please  enter valid fuel amount"
                                                    ControlToValidate="txtAdjFuelAmount" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="regExFuelAmtCall" runat="server" Enabled="True"
                                                    TargetControlID="regExFuelAmt" PopupPosition="Left">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompFuelAmt" runat="server" ErrorMessage="Adjust fuel amount cannot be greater than balance fuel amount"
                                                    ControlToCompare="txtBalFuelAmount" ControlToValidate="txtAdjFuelAmount" Display="None"
                                                    SetFocusOnError="true" ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="CompFuelAmtCall" runat="server" Enabled="True"
                                                    TargetControlID="CompFuelAmt" PopupPosition="Left">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </div>
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="10" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="11" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbSupl" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cmbPaymentMode" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });

            $('#<%= txtchqdt.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtchqdt.ClientID %>').val('');
                }
            });

        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function calculate() {
            var gv = document.getElementById("<%= grdPaymentSupl.ClientID %>");
            var tb = gv.getElementsByTagName("input");

            var TotalCash = 0;
            var TotalFuel = 0;
            var Total = 0;
                       
            for (var i = 0; i < tb.length; i++) {
                if (tb[i].type == "text" && tb[i].id.indexOf('txtAdjCashAmount') >= 0) {
                    if (parseFloat(tb[i].value) > 0) {
                        TotalCash = parseFloat(TotalCash) + parseFloat(tb[i].value);

                    }
                }
                if (tb[i].type == "text" && tb[i].id.indexOf('txtAdjFuelAmount') >= 0) {
                    if (parseFloat(tb[i].value) > 0) {
                        TotalFuel = parseFloat(TotalFuel) + parseFloat(tb[i].value);                        
                    }
                }
                Total = parseFloat(TotalFuel) + parseFloat(TotalCash);        
            }

            document.getElementById("<%= txtCashAmount.ClientID %>").value = TotalCash.toFixed(2);
            document.getElementById("<%= txtFuelAmount.ClientID %>").value = TotalFuel.toFixed(2);
            document.getElementById("<%= txtAmount.ClientID %>").value = Total.toFixed(2);
        }      
    </script>
</asp:Content>
