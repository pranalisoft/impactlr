<%@ Page Title="User Master" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="UserMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UserMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
<%--start attach file popup--%>
    <asp:Button ID="btnAttachFile" runat="server" Text="Button" Style="display: none" />
    <asp:Panel ID="pnlAttachFile" runat="server" CssClass="dialog" Style="display: none">
        <div style="background-color: #236099; color: White; padding: 3px; font-size: 14px;
            font-weight: bold">
            Company - Upload Digital Signature
        </div>
        <div>
            <asp:Panel ID="pnlInwardFiles" runat="server">
                <table width="100%" cellspacing="10px" style="margin-top: 10px;">
                    <tr>
                        <td width="80px">
                            <asp:Label ID="Label19" runat="server" Text="Digital Signature Image" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload2" runat="server" Width="280px" Font-Size="13px" TabIndex="0" />&nbsp
                            &nbsp
                            <asp:RequiredFieldValidator ID="ReqfldflCopy" runat="server" ErrorMessage="Please select image To Upload"
                                ControlToValidate="FileUpload2" SetFocusOnError="True" ValidationGroup="saveFile"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnSaveFiles" runat="server" Text="Ok" CssClass="button" OnClick="btnSaveFiles_Click"
                                TabIndex="2" ValidationGroup="saveFile" />
                            &nbsp;&nbsp;<asp:Button ID="btnFileCancel" runat="server" Text="Close" CssClass="button"
                                OnClick="btnFileCancel_Click" TabIndex="3" />&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="popAttachFIles" runat="server" DynamicServicePath=""
        Enabled="True" TargetControlID="btnAttachFile" PopupControlID="pnlAttachFile"
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <%-- end attach file popup--%>
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    User Master
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="User Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        User Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table style="width: 99.5%">
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="Userid" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" TabIndex="1" Width="200px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtUserId" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <%-- <asp:RegularExpressionValidator ID="RegExprtxtUserNm" runat="server" ControlToValidate="txtUserNm"
                                Display="Dynamic" ErrorMessage="Enter Characters Only" ValidationExpression="^[a-zA-Z\s]*$"
                                ValidationGroup="save" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Full Name" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtUserName" runat="server" MaxLength="250" TabIndex="2" Width="200px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtUserName" SetFocusOnError="True" ValidationGroup="save"
                                                Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblEmailId" runat="server" Text="Email Id" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmailId" runat="server" MaxLength="100" Width="200px" TabIndex="3"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="reqfldEmailId"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtEmailId" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat="server" ID="regexpEmail" SetFocusOnError="true"
                                            ValidationGroup="save" ErrorMessage="Invalid EMail Address" Display="Dynamic"
                                            ControlToValidate="txtEmailId" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text="Whatsapp No." CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtWhatsappNo" runat="server" MaxLength="100" Width="200px" TabIndex="4"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtWhatsappNo" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="25" Width="200px" TabIndex="5"
                                            TextMode="Password" AutoPostBack="false" CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                                ID="reqfldPassword" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtPassword" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="User Role" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cmbRoles" runat="server" AppendDataBoundItems="true" TabIndex="6"
                                            Width="300px" CssClass="NormalTextBold">
                                            <asp:ListItem Value="-1">Select Roles</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;<asp:RequiredFieldValidator ID="reqFldddlRoles" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="cmbRoles" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Is Active" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" TabIndex="7" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Digital Signature"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkBtnAttachFiles" runat="server" Font-Bold="true" TabIndex="18"
                                            Font-Size="13px" OnClick="lnkBtnAttachFiles_Click">Update Digital Signature</asp:LinkButton>
                                        <asp:FileUpload ID="flCopy" runat="server" Width="300px" Font-Size="13px" TabIndex="10"
                                            Visible="false" />
                                        <asp:Button ID="btnconfirmFile" runat="server" Text="Show Image" OnClick="btnconfirmFile_Click"
                                            Visible="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" Text="Preview" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" CssClass="pnlimage" Width="100px">
                                            <asp:Image runat="server" ID="imgLogo" Width="100px" Height="100px" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr style="height: 15px;">
                                    <td colspan="2" style="height: 15px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="8" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="9" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="10" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="vertical-align: top; border: 1px solid #3A66AF">
                            <div style="height: 200px; overflow: auto">
                                <asp:GridView ID="grdPlant" runat="server" Width="100%" AutoGenerateColumns="False"
                                    CssClass="grid" DataKeyNames="Srl,Field1" EmptyDataText="No Records Found.">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Field1" HeaderText="Branch" SortExpression="Field1"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="2" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="3" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="4" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,UserId,Userpwd,UserName,Role_Srl,RoleName,Email_Id,isActive,DigitalSignature,WhatsappNo"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand">
                                <Columns>
                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="UserId" HeaderText="User Id"
                                        SortExpression="UserId">
                                        <ItemStyle Width="100px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="UserName" HeaderText="Name" SortExpression="UserName">
                                        <ItemStyle Width="300px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Email_Id" HeaderText="Email Id" SortExpression="Email_Id">
                                        <ItemStyle Width="250px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RoleName" HeaderText="Role" SortExpression="RoleName">
                                        <ItemStyle Width="150px" />
                                    </asp:BoundField>
                                    <asp:CheckBoxField DataField="isActive" HeaderText="Active" SortExpression="isActive"
                                        ItemStyle-Width="28px">
                                        <ItemStyle Width="28px" />
                                    </asp:CheckBoxField>
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
