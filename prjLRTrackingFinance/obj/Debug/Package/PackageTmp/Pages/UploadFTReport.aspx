﻿<%@ Page Title="Upload Freight Tiger Data Dump" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="UploadFTReport.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UploadFTReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                          />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                          />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    LOB Group Master
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Upload Freight Tiger Data Dump
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table class="control_set" style="width: 99.5%">
                                <tr class="control_row">
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr class="control_row">
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="File" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                       <asp:FileUpload ID="flUpload" runat="server" Font-Size="12px" TabIndex="0" />&nbsp
                                    &nbsp
                                    <asp:RequiredFieldValidator ID="ReqfldflCopy" runat="server" ErrorMessage="Please select File To Upload"
                                        ControlToValidate="flUpload" SetFocusOnError="True" ValidationGroup="saveFile"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>  
                                <tr style="height: 15px;">
                                    <td colspan="2" style="height: 15px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="7" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="8" />
                                        <asp:Button ID="btnClear" runat="server"   Text="Clear" CssClass="button"
                                            TabIndex="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
               
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
