﻿<%@ Page Title="Purchase Register" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="SupplierPurchaseRegister.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierPurchaseRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Purchase Register
                        </td>
                    </tr>
                </table>
                <div style="border: solid 1px black;">
                    <table width="100%">
                        <tr>
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                    FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                            </td>
                            <td width="250px">
                                <asp:DropDownList ID="cmbSupplier" runat="server" CssClass="NormalTextBold" TabIndex="5"
                                    Width="250px" AppendDataBoundItems="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" OnClick="btnSearch_Click"
                                    TabIndex="6" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" OnClick="btnExcel_Click"
                                    TabIndex="7" />
                            </td>
                        </tr>
                    </table>
                </div>
                &nbsp;&nbsp;
                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblTotalPacklist" Text="Records : " runat="server" CssClass="NormalTextBold"
                                ForeColor="Maroon"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 3px;" colspan="3">
                            <div class="grid_region" style="height:500px;overflow:auto">
                                <asp:GridView ID="grdMain" runat="server" Width="100%" AutoGenerateColumns="true"
                                    ShowFooter="false" PageSize="5" CssClass="grid" AllowPaging="false" AllowSorting="false"
                                    DataKeyNames="" EmptyDataText="No Records Found.">
                                    <RowStyle Height="10px" />
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                <div>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtFromDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtFromDate.ClientID %>').val('');
                }
            });

            $('#<%= txtToDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtToDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });
        
    </script>
</asp:Content>
