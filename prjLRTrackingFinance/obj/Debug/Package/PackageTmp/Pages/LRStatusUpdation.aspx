﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="LRStatusUpdation.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRStatusUpdation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="Button2" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    Update Status
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="" Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblCh" runat="server" Text="Challan No.">
                            </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblChallanNoRaisedBy" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Owner"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox TabIndex="5" ID="txtOwner" Width="200" CssClass="NormalTextBold" runat="server"
                                MaxLength="50"></asp:TextBox>
                            &nbsp;<asp:RequiredFieldValidator ID="reqFldInvNo" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter new Owner'
                                ControlToValidate="txtOwner" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" ValidationGroup="save" CssClass="button"
                        CommandName="yes" OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="msgpopAuth" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="Button2" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div style="padding: 10px 10px 20px 10px;">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            LR Status Updation
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <table style="padding: 0px 0px 0px 10px; width: 100%;">
                </table>
                <table width="100%">
                    <tr>
                        <td width="60px">
                            <asp:Label ID="Label1" runat="server" CssClass="NormalTextBold" Text="Search"></asp:Label>
                        </td>
                        <td width="80px">
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="20" TabIndex="1" Text="" Width="200px"
                                ToolTip="Enter Sender Name,Inward No. as search text"></asp:TextBox>
                        </td>
                        <td width="80px">
                            <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                        </td>
                        <td width="150px">
                            <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                FirstDayOfWeek="Sunday">
                            </asp:CalendarExtender>
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                        </td>
                        <td width="80px">
                            <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                        </td>
                        <td width="150px">
                            <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                FirstDayOfWeek="Sunday">
                            </asp:CalendarExtender>
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click"
                                TabIndex="6" />
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
                <div class="grid_region">
                    <table width="100%">
                        <tr>
                            <td>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblTotalPacklist" Text="Pending Count : 000" runat="server" CssClass="NormalTextBold"
                                    ForeColor="Maroon"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 3px;" colspan="2">
                                <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                    DataKeyNames="ID, LRNo, LRDate, Consigner, Consignee, FromLoc, ToLoc, VehicleNo, Weight, ChargeableWt, TotPackages, InvoiceNo, InvoiceValue, LoadType, Remarks, CreatedBy, CreatedOn, Status,CurrentRemarks,PlacementID,TripId"
                                    OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                    OnRowCommand="grdPacklistView_RowCommand" AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="LRNo" HeaderText="LR No."
                                            SortExpression="LRNo">
                                            <ItemStyle Width="80px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                            DataFormatString="{0:dd/MM/yyyy}">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner"
                                            ItemStyle-Width="200px">
                                            <ItemStyle Width="160px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Consignee" HeaderText="Consignee" SortExpression="Consignee"
                                            ItemStyle-Width="160px">
                                            <ItemStyle Width="160px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc"
                                            ItemStyle-Width="120px">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc"
                                            ItemStyle-Width="120px">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="120px">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:ButtonField ButtonType="Link" CommandName="Remarks" DataTextField="CurrentRemarks"
                                            HeaderText="Remarks" SortExpression="CurrentRemarks">
                                            <ItemStyle Width="160px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtFromDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtFromDate.ClientID %>').val('');
                }
            });

            $('#<%= txtToDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtToDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });
        
    </script>
</asp:Content>
