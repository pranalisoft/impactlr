﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="MISReport1.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.MISReport1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            MIS Report
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <table class="control_set" width="100%">
                        <tr class="control_row">
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="frmDt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select from date'
                                    ControlToValidate="txtFromDate" SetFocusOnError="True" ValidationGroup="save"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy"
                                    PopupButtonID="ImageButton1" FirstDayOfWeek="Sunday">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="toDt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select to date'
                                    ControlToValidate="txtToDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy"
                                    PopupButtonID="ImageButton2" FirstDayOfWeek="Sunday">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td width="80px">
                            </td>
                            <td colspan="3">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button" Visible="false"
                                    OnClick="btnSearch_Click" TabIndex="6" ValidationGroup="save" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                    OnClick="btnExcel_Click" />
                            </td>
                        </tr>
                    </table>
                    <table style="border: 1px solid black">
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label22" runat="server" Text="Total Invoices"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTotSales" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            --
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Total Sales Amount"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblSalesAmount" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                       
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Total Supplier Bills"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPurchases" runat="server" Text=""></asp:Label>
                            </td>
                             <td>
                            --
                            </td>                       
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Total Purchase Amount"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPurchaseAmount" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                       
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
