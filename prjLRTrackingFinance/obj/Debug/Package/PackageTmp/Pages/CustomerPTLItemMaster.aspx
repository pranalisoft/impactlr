﻿<%@ Page Title="PTL - Item" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="CustomerPTLItemMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.CustomerPTLItemMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Customer Wise Zone Type
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Customer Wise Item Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table style="width: 99.5%">
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td colspan="5">
                                        <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="true" OnSelectedIndexChanged="cmbCustomer_SelectedIndexChanged">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbCustomer" ErrorMessage="Please select Customer from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="From" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbFrom" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="2" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDown" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="From" runat="server" ControlToValidate="cmbFrom" ErrorMessage="Please select/enter From Location from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="FromConsigneeCall" runat="server" Enabled="True"
                                            TargetControlID="From">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label13" runat="server" Text="To" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td colspan="3">
                                        <asp:ComboBox ID="cmbTo" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDown" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="To" runat="server" ControlToValidate="cmbTo" ErrorMessage="Please select/enter To Location from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="ToCall" runat="server" Enabled="True" TargetControlID="To">
                                        </asp:ValidatorCalloutExtender>
                                        <%--<asp:TextBox ID="txtName" runat="server" MaxLength="50" TabIndex="1" Width="400px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtName" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="Zone / Type" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td colspan="5">
                                        <asp:ComboBox ID="cmbZoneType" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="4" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="cmbZoneType"
                                            ErrorMessage="Please select Zone / Type from the list" ClientValidationFunction="ValidatorCombobox"
                                            Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" Enabled="True"
                                            TargetControlID="CustomValidator1">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Item" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtItem" runat="server" MaxLength="50" TabIndex="5" Width="250px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtItem" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" Enabled="True"
                                            TargetControlID="RequiredFieldValidator2">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text="Slab" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtSlab" runat="server" MaxLength="50" TabIndex="6" Width="250px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="Vehicle Type Id" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td colspan="5">
                                        <asp:ComboBox ID="cmbVehicleType" runat="server" AutoCompleteMode="SuggestAppend"
                                            Width="250px" MaxLength="200" TabIndex="7" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 140px">
                                        <asp:Label ID="Label6" runat="server" Text="Rate Per Qty (Selling)" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td style="width: 250px">
                                        <asp:TextBox ID="txtRatePerQtySelling" runat="server" MaxLength="10" TabIndex="8"
                                            Width="100px" CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                            FilterType="Numbers,Custom" TargetControlID="txtRatePerQtySelling" ValidChars=".">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="regExTDSPer" runat="server" ControlToValidate="txtRatePerQtySelling"
                                            Display="None" ErrorMessage="Please enter valid Rate Per Qty (Selling) %" SetFocusOnError="True"
                                            ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" Enabled="True"
                                            TargetControlID="regExTDSPer">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                    <td style="width: 140px">
                                        <asp:Label ID="Label7" runat="server" Text="Rate Per Kg (Selling)" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td style="width: 250px">
                                        <asp:TextBox ID="txtRatePerKgSelling" runat="server" MaxLength="10" TabIndex="9"
                                            Width="100px" CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                            FilterType="Numbers,Custom" TargetControlID="txtRatePerKgSelling" ValidChars=".">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRatePerKgSelling"
                                            Display="None" ErrorMessage="Please enter valid Rate Per Kg (Selling) %" SetFocusOnError="True"
                                            ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" Enabled="True"
                                            TargetControlID="RegularExpressionValidator1">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                    <td style="width: 140px">
                                        <asp:Label ID="lblFixedSelling" runat="server" Text="Fixed Rate Selling"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkFixedSelling" runat="server" TabIndex="9" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Text="Rate Per Qty (Buying)" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRatePerQtyBuying" runat="server" MaxLength="10" TabIndex="10"
                                            Width="100px" CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="True"
                                            FilterType="Numbers,Custom" TargetControlID="txtRatePerQtyBuying" ValidChars=".">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtRatePerQtyBuying"
                                            Display="None" ErrorMessage="Please enter valid Rate Per Qty (Buying) %" SetFocusOnError="True"
                                            ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" Enabled="True"
                                            TargetControlID="RegularExpressionValidator2">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Text="Rate Per Kg (Buying)" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRatePerKgBuying" runat="server" MaxLength="10" TabIndex="11"
                                            Width="100px" CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="True"
                                            FilterType="Numbers,Custom" TargetControlID="txtRatePerKgBuying" ValidChars=".">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtRatePerKgBuying"
                                            Display="None" ErrorMessage="Please enter valid Rate Per Kg (Buying) %" SetFocusOnError="True"
                                            ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" Enabled="True"
                                            TargetControlID="RegularExpressionValidator3">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFixedBuying" runat="server" Text="Fixed Rate Buying"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkFixedBuying" runat="server" TabIndex="11" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label11" runat="server" Text="MOQ" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtMOQ" runat="server" MaxLength="4" TabIndex="12" Width="100px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="True"
                                            FilterType="Numbers" TargetControlID="txtMOQ">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Active" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red"></span>
                                    </td>
                                    <td colspan="5">
                                        <asp:CheckBox ID="chkActive" runat="server" TabIndex="10" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="13" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="14" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="15" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="16" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="17" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="7" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,CustomerId,Customer,Item,ZoneTypeId,ZoneType,Slab,VehicleTypeId,VehicleType,RatePerQtySelling,RatePerKgSelling,RatePerQtyBuying,RatePerKgBuying,MOQ,IsActive,FromLoc,ToLoc,IsFixedRateBuying,IsFixedRateSelling"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FromLoc" HeaderText="From" SortExpression="FromLoc"></asp:BoundField>
                                    <asp:BoundField DataField="ToLoc" HeaderText="To" SortExpression="ToLoc"></asp:BoundField>
                                    <asp:BoundField DataField="VehicleType" HeaderText="Vehicle Type" SortExpression="VehicleType">
                                    </asp:BoundField>
                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Item" HeaderText="Item"
                                        SortExpression="Item" />
                                    <asp:BoundField DataField="ZoneType" HeaderText="Zone/Type" SortExpression="ZoneType">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Slab" HeaderText="Slab" SortExpression="Slab"></asp:BoundField>
                                    <asp:BoundField DataField="RatePerQtySelling" HeaderText="Rate/Qty Selling" SortExpression="RatePerQtySelling"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="RatePerQtyBuying" HeaderText="Rate/Qty Buying" SortExpression="RatePerQtyBuying"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="RatePerKgSelling" HeaderText="Rate/Kg Selling" SortExpression="RatePerKgSelling"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="RatePerKgBuying" HeaderText="Rate/Kg Buying" SortExpression="RatePerKgBuying"
                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="MOQ" HeaderText="MOQ" SortExpression="MOQ" ItemStyle-Width="100px"
                                        ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
