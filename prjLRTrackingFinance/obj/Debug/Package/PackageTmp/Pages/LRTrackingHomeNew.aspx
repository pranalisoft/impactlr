﻿<%@ Page Title="LR Tracking Home" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="LRTrackingHomeNew.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRTrackingHomeNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Include/JS/layout/jquery-1.7.1.js"></script>
    <%-- <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var grdViewFUPHeader = $('#<%=grdViewFUP.ClientID %>').clone(true);
            $(grdViewFUPHeader).find("tr:gt(0)").remove();
            $('#<%=grdViewFUP.ClientID %> tr th').each(function (i) {
                $("th:nth-child(" + (i + 1) + ")", grdViewFUPHeader).css('width', $(this).width().toString() + "px");
            });
            $("#grdViewFUPGHead").append(grdViewFUPHeader);
            $("#grdViewFUPGHead").css('position', 'absolute');
            $("#grdViewFUPGHead").css('top', $('#<%=grdViewFUP.ClientID %>').offset().top);
            //                        $('#<%=grdViewFUP.ClientID %> tr th').remove();           
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Width="810px" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    <asp:Label Text="" ID="lblViewLR" runat="server"></asp:Label>
                </div>
                <div align="center" style="padding: 5px; width: 800px; height: 300px; overflow: auto">
                    <asp:GridView ID="grdViewFUP_LR" runat="server" Width="100%" AutoGenerateColumns="False"
                        AllowPaging="False" EmptyDataText="No Records Found." CssClass="grid" PageSize="10">
                        <Columns>
                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo">
                                <ItemStyle Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="80px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner" />
                            <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc"
                                ItemStyle-Width="120px">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc"
                                ItemStyle-Width="120px">
                                <ItemStyle Width="120px" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="header" />
                        <RowStyle CssClass="row" />
                        <AlternatingRowStyle CssClass="alter_row" />
                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                        <PagerSettings Mode="Numeric" />
                    </asp:GridView>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="button" OnClick="btnOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popFUP_ShowLR" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div style="text-align: center; vertical-align: middle; width: 100%;">
                <center>
                    <div>
                        <table width="100%">
                            <tr class="centerPageHeader">
                                <td class="centerPageHeader" style="height: 25px; font-size: 16px; font-weight: bold;
                                    vertical-align: middle;">
                                    <center>
                                        Transport Management System</center>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: White; height: 2px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table style="text-align: center; vertical-align: middle;">
                        <tr>
                            <td>
                                <%--<img src="/Common/images/OBDTrackerIcon.jpg" height="25px" width="35px" />--%>
                            </td>
                            <td style="font-size: 14px; font-weight: bold;">
                                &nbsp<asp:HyperLink ID="hyplOBDMain" runat="server" NavigateUrl="~/Pages/LRMain.aspx"
                                    ForeColor="#39478E">Goto Main Page</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Button TabIndex="1" ID="btnTVA" runat="server" Text="Sales Target Vs Actual For Month"
                                    CssClass="button" Width="250px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                                <asp:Button TabIndex="2" ID="btnACC" runat="server" Text="POD Overview For Month"
                                    CssClass="button" Width="230px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                                <asp:Button TabIndex="3" ID="btnFUP" runat="server" Text="Pending Freight Updation"
                                    CssClass="button" Width="230px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                                <asp:Button TabIndex="4" ID="btnPOD" runat="server" Text="Pending Post POD,Approval & Bill"
                                    CssClass="button" Width="250px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                                <asp:Button TabIndex="5" ID="btnVSU" runat="server" Text="Pending Vehicle Status Updation"
                                    CssClass="button" Width="250px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                                <asp:Button TabIndex="6" ID="btnEBO" runat="server" Text="Due & Overdue Ewaybills"
                                    CssClass="button" Width="230px" Height="60px" OnClick="btn_Click" Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; border-bottom: 1px solid blue" align="center">
                                <asp:Label ID="lblRecCount" Text="0" runat="server" Font-Bold="True" ForeColor="#39478E"
                                    Font-Size="18px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <div id="grdViewFUPGHead" style="margin-top: -5px">
                                </div>
                                <div id="dvFUP" runat="server" style="height: 500px; overflow: auto">
                                    <asp:GridView ID="grdViewFUP" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId,BranchName,TotalRecords" EmptyDataText="No records found."
                                        Width="50%" ShowHeaderWhenEmpty="True" OnRowCommand="grdViewFUP_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                            <asp:ButtonField ButtonType="Link" CommandName="ShowLR" DataTextField="TotalRecords"
                                                HeaderText="Count" SortExpression="TotalRecords">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                                <div id="dvEWO" runat="server" visible="false" style="height: 500px; overflow: auto">
                                    <asp:GridView ID="grdEwaydBill" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="" EmptyDataText="No records found." Width="100%" ShowHeaderWhenEmpty="True"
                                        OnRowCommand="grdSuplBill_RowCommand" OnRowDataBound="grdEwaydBill_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="LOB" HeaderText="LOB" SortExpression="LOB" />
                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo" />
                                            <asp:BoundField DataField="EwayBillNo" HeaderText="Eway Bill No." SortExpression="LRNo" />
                                            <asp:BoundField DataField="EwayBillDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Eway Bill Date"
                                                SortExpression="LRNo" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                                <div id="dvPOD" runat="server" visible="false" style="height: 500px; overflow: auto">
                                    <asp:GridView ID="grdSuplBill" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId,BranchName,PostPOD,Approval,Bill" EmptyDataText="No records found."
                                        Width="100%" ShowHeaderWhenEmpty="True" OnRowCommand="grdSuplBill_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                            <asp:ButtonField ButtonType="Link" CommandName="PostPOD" DataTextField="PostPOD"
                                                HeaderText="Post POD" SortExpression="PostPOD" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:ButtonField ButtonType="Link" CommandName="Approval" DataTextField="Approval"
                                                HeaderText="Approval" SortExpression="Approval" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                            <asp:ButtonField ButtonType="Link" CommandName="Bill" DataTextField="Bill" HeaderText="Bill"
                                                SortExpression="Bill" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                                <div id="dvVSU" runat="server" visible="false" style="height: 500px; overflow: auto">
                                    <asp:GridView ID="grdViewVSU" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId,BranchName,TotalRecords" EmptyDataText="No records found."
                                        Width="50%" ShowHeaderWhenEmpty="True" OnRowCommand="grdViewVSU_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="Line Of Business" SortExpression="BranchName" />
                                            <asp:ButtonField ButtonType="Link" CommandName="ShowLR" DataTextField="TotalRecords"
                                                HeaderText="Count" SortExpression="TotalRecords">
                                                <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                                <div id="dvTVA" runat="server" visible="false" style="height: 500px; overflow: auto">
                                    <asp:GridView ID="grdViewManager" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId,BranchName" EmptyDataText="No records found." Width="100%"
                                        ShowHeaderWhenEmpty="True" OnRowCommand="grdViewManager_RowCommand" OnRowDataBound="grdViewManager_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="LOB" SortExpression="BranchName" />
                                            <asp:BoundField DataField="TargetSale" HeaderText="Target Sale" SortExpression="TargetSale"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="SalesAsOn" HeaderText="Sales As On" SortExpression="SalesAsOn"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Projection" HeaderText="Projection" SortExpression="Projection"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="ShortFall" HeaderText="Shortfall" SortExpression="ShortFall"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TargetMargin" HeaderText="Target Margin" SortExpression="TargetMargin"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="ActualMargin" HeaderText="Actual Margin" SortExpression="ActualMargin"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="ShortFallMargin" HeaderText="Shortfall Margin" SortExpression="ShortFallMargin"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="PODTarget" HeaderText="POD Target" SortExpression="PODTarget"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="PODActual" HeaderText="Actual" SortExpression="PODActual"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="OverallAchievement" HeaderText="Target Points" SortExpression="OverallAchievement"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="GainPoints" HeaderText="Gain Points" SortExpression="GainPoints"
                                                ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="BurlyWood" ItemStyle-Font-Bold="true"/>
                                            <asp:ButtonField ButtonType="Link" CommandName="PendingPOD" DataTextField="PendingPOD"
                                                HeaderText="Pending PODs" SortExpression="PendingPOD" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                                <div id="dvACC" runat="server" visible="false" style="height: 500px; overflow: auto">
                                    <%--<asp:Label Text="POD Overview For This Month" ID="Label8" runat="server" Font-Bold="True"
                                    ForeColor="#39478E" Font-Size="14px"></asp:Label>--%>
                                    <asp:GridView ID="grdViewAccount" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="BranchId" EmptyDataText="No records found." Width="100%" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:BoundField DataField="BranchName" HeaderText="LOB" SortExpression="BranchName" />
                                            <asp:BoundField DataField="PODScanned" HeaderText="POD Scanned" SortExpression="PODScanned"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="BillGenerated" HeaderText="Bill Generated" SortExpression="BillGenerated"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Pending" HeaderText="Pending" SortExpression="Pending"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TargetPercentage" HeaderText="Target %" SortExpression="TargetPercentage"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="ActualPercentage" HeaderText="Actual %" SortExpression="ActualPercentage"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TotalReceivable" HeaderText="Total Receivable" SortExpression="TotalReceivable"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="DueReceived" HeaderText="Due Received" SortExpression="DueReceived"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TargetRcvable" HeaderText="Target %" SortExpression="TargetRcvable"
                                                ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="ActualPercentageRcvable" HeaderText="Achievement %" SortExpression="ActualPercentageRcvable"
                                                ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
