<%@ Page Title="LR Entry" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="LREntry.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LREntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <asp:Button ID="Button2" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="LR Already Created" Font-Bold="True"
                        Font-Size="10pt"></asp:Label>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblLrConfirmation" runat="server" Text="">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="msgpopAuth" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="Button2" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        LR Entry
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div class="body" style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label runat="server" Text="Placement Id" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" TabIndex="1" CssClass="NormalTextBold" ID="txtPlacementId"
                                                    Width="200px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvPlacementId" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter placement id'
                                                    ControlToValidate="txtPlacementId" SetFocusOnError="True" ValidationGroup="placement"
                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                <%-- <asp:ValidatorCalloutExtender ID="rfvPlacementIdCall" runat="server" Enabled="True"
                                                    TargetControlID="rfvPlacementId">
                                                </asp:ValidatorCalloutExtender>--%>
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" FilterType="Numbers" TargetControlID="txtPlacementId">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                &nbsp;&nbsp;<asp:Button TabIndex="1" ID="btnPlacement" Width="100px" runat="server"
                                                    Text="Ok" CssClass="button" OnClick="btnPlacement_Click" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Text="Already Created LR(s)" CssClass="NormalTextBold"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLRNosForPlacementId" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:HiddenField ID="hdTripId" runat="server" />
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                                <asp:HiddenField ID="txtHiddenExpDate" runat="server" />
                                                <asp:HiddenField ID="txtHiddenCustFreight" runat="server" />
                                                <asp:HiddenField ID="txtHiddenSuplFreight" runat="server" />
                                                <asp:HiddenField ID="txtHiddenCustRate" runat="server" />
                                                <asp:HiddenField ID="txtHiddenSuplRate" runat="server" />
                                                <asp:HiddenField ID="txtHiddenSuplId" runat="server" />
                                                <asp:Label ID="Label29" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbBillingCompany_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="BillingCompany" runat="server" ControlToValidate="cmbBillingCompany"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> "Please select billing Company from the list'
                                                    ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%--<asp:ValidatorCalloutExtender ID="BillingCompanyCall" runat="server" Enabled="True"
                                                    TargetControlID="BillingCompany">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="LR No" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLRNo" runat="server" Width="200px" Enabled="false" TabIndex="2"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="LR Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDate" runat="server" Width="80px" TabIndex="3" CssClass="NormalTextBold"
                                                    AutoPostBack="True" OnTextChanged="txtDate_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                                <asp:DropDownList ID="ddlHH" runat="server" Width="50px" TabIndex="5" CssClass="NormalTextBold">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Label ID="lblSeperator" runat="server" Text=":" BorderStyle="None" BorderWidth="1px"
                                                    Font-Bold="True" Width="29px" Style="text-align: center"></asp:Label>
                                                <asp:DropDownList ID="ddlmm" runat="server" Width="50px" CssClass="NormalTextBold"
                                                    TabIndex="6">
                                                    <asp:ListItem>00</asp:ListItem>
                                                    <asp:ListItem>01</asp:ListItem>
                                                    <asp:ListItem>02</asp:ListItem>
                                                    <asp:ListItem>03</asp:ListItem>
                                                    <asp:ListItem>04</asp:ListItem>
                                                    <asp:ListItem>05</asp:ListItem>
                                                    <asp:ListItem>06</asp:ListItem>
                                                    <asp:ListItem>07</asp:ListItem>
                                                    <asp:ListItem>08</asp:ListItem>
                                                    <asp:ListItem>09</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                    <asp:ListItem>24</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                    <asp:ListItem>26</asp:ListItem>
                                                    <asp:ListItem>27</asp:ListItem>
                                                    <asp:ListItem>28</asp:ListItem>
                                                    <asp:ListItem>29</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>31</asp:ListItem>
                                                    <asp:ListItem>32</asp:ListItem>
                                                    <asp:ListItem>33</asp:ListItem>
                                                    <asp:ListItem>34</asp:ListItem>
                                                    <asp:ListItem>35</asp:ListItem>
                                                    <asp:ListItem>36</asp:ListItem>
                                                    <asp:ListItem>37</asp:ListItem>
                                                    <asp:ListItem>38</asp:ListItem>
                                                    <asp:ListItem>39</asp:ListItem>
                                                    <asp:ListItem>40</asp:ListItem>
                                                    <asp:ListItem>41</asp:ListItem>
                                                    <asp:ListItem>42</asp:ListItem>
                                                    <asp:ListItem>43</asp:ListItem>
                                                    <asp:ListItem>44</asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                    <asp:ListItem>46</asp:ListItem>
                                                    <asp:ListItem>47</asp:ListItem>
                                                    <asp:ListItem>48</asp:ListItem>
                                                    <asp:ListItem>49</asp:ListItem>
                                                    <asp:ListItem>50</asp:ListItem>
                                                    <asp:ListItem>51</asp:ListItem>
                                                    <asp:ListItem>52</asp:ListItem>
                                                    <asp:ListItem>53</asp:ListItem>
                                                    <asp:ListItem>54</asp:ListItem>
                                                    <asp:ListItem>55</asp:ListItem>
                                                    <asp:ListItem>56</asp:ListItem>
                                                    <asp:ListItem>57</asp:ListItem>
                                                    <asp:ListItem>58</asp:ListItem>
                                                    <asp:ListItem>59</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Placement Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVehiclePlacementDate" runat="server" Width="80px" TabIndex="7"
                                                    CssClass="NormalTextBold" Enabled="false"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtVehiclePlacementDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="dtpBtnPlace"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtnPlace" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="8" Width="23px" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtVehiclePlacementDate" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label21" runat="server" Text="Ref No" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRefNo" runat="server" Width="200px" TabIndex="9" CssClass="NormalTextBold"
                                                    MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqFVLRNo" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Ref. No.'
                                                    ControlToValidate="txtRefNo" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label22" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" AutoPostBack="true" TabIndex="10" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" OnSelectedIndexChanged="cmbCustomer_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="Customer" runat="server" ControlToValidate="cmbCustomer"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Customer From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%--<asp:ValidatorCalloutExtender ID="CustomerCall" runat="server" Enabled="True" TargetControlID="Customer">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Consignor" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbConsigner" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="11" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbConsigner_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="Consigner" runat="server" ControlToValidate="cmbConsigner"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Consignor From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%-- <asp:ValidatorCalloutExtender ID="ConsignerCall" runat="server" Enabled="True" TargetControlID="Consigner">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="Consignee" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbConsignee" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="13" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbConsignee_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="Consignee" runat="server" ControlToValidate="cmbConsignee"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Consignee From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%--<asp:ValidatorCalloutExtender ID="ConsigneeCall" runat="server" Enabled="True" TargetControlID="Consignee">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label30" runat="server" Text="Address" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtConsignerAddress" Width="270px" TextMode="MultiLine"
                                                    Rows="3" TabIndex="12"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvconsigneraddress" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the consigner address.'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtConsignerAddress"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label31" runat="server" Text="Address" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtConsigneeAddress" Width="270px" TextMode="MultiLine"
                                                    Rows="3" TabIndex="14"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvConsigneeAddress" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the consignee address.'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtConsigneeAddress"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label41" runat="server" Text="PINCode" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtConsignerPIN" Width="270px" MaxLength="6" TabIndex="12"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the consignor PINCode.'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtConsignerPIN"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" ID="fltpin" TargetControlID="txtConsignerPIN"
                                                    FilterType="Numbers">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label42" runat="server" Text="PINCode" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtConsigneePIN" Width="270px" TabIndex="14"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the consignee PINCode.'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtConsigneePIN"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1"
                                                    TargetControlID="txtConsigneePIN" FilterType="Numbers">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label34" runat="server" Text="Consignor GSTNo" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="14" ID="txtConsignorGSTNo" Width="270" runat="server" MaxLength="50"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label35" runat="server" Text="Consignee GSTNo" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="16" ID="txtConsigneeGSTNo" Width="270" runat="server" MaxLength="50"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label26" runat="server" Text="Consignor Email" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="16" ID="txtConsignorEmail" Width="270" TextMode="MultiLine"
                                                    Rows="2" runat="server" MaxLength="500" Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label15" runat="server" Text="Consignee Email" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="16" ID="txtConsigneeEmail" Width="270" TextMode="MultiLine"
                                                    Rows="2" runat="server" MaxLength="500" Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label43" runat="server" Text="Consignor Contact" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="16" ID="txtConsignorContact" Width="270" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label19" runat="server" Text="Consignee Contact" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="16" ID="txtConsigneeContact" Width="270" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="From" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbFrom" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="17" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbFrom_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="cvFrom" runat="server" ControlToValidate="cmbFrom" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select From Location From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%-- <asp:ValidatorCalloutExtender ID="cvFromCall" runat="server" Enabled="True" TargetControlID="cvFrom">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label9" runat="server" Text="To" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbTo" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="18" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbTo_SelectedIndexChanged">
                                                </asp:ComboBox>
                                                &nbsp;&nbsp;<asp:HiddenField ID="HFExpectedDeliveryDays" runat="server" />
                                                <asp:Label ID="lblExpDeliveryDays" ForeColor="Blue" runat="server" Text="" CssClass="NormalTextBold"></asp:Label>
                                                <asp:CustomValidator ID="cvTo" runat="server" ControlToValidate="cmbTo" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select To Location From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%-- <asp:ValidatorCalloutExtender ID="cvToCall" runat="server" Enabled="True" TargetControlID="cvTo">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label10" runat="server" Text="Actual Weight" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="19" ID="txtWeight" Width="80" runat="server" MaxLength="10"
                                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:Label ID="Label13" runat="server" Text="Ton" CssClass="NormalTextBold" Font-Bold="true"></asp:Label>
                                                &nbsp;<asp:RequiredFieldValidator ID="rfvWeight" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the weight.'
                                                    ControlToValidate="txtWeight" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterWeight" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtWeight" Enabled="True">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Text="Chargeable Weight" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="20" ID="txtChWeight" Width="80" runat="server" MaxLength="10"
                                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox><asp:Label ID="Label27"
                                                        runat="server" Text="Ton" CssClass="NormalTextBold" Font-Bold="true"></asp:Label>
                                                &nbsp;<asp:RequiredFieldValidator ID="rfvChWt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the weight.'
                                                    ControlToValidate="txtChWeight" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterChWt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtChWeight" Enabled="True">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="Total Packages" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="21" ID="txttotPackage" Width="80" runat="server" MaxLength="5"
                                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                                &nbsp;<asp:RequiredFieldValidator ID="rfvtotpkg" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the total packages.'
                                                    ControlToValidate="txttotPackage" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="fltPackages" runat="server" FilterType="Numbers"
                                                    TargetControlID="txttotPackage">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label12" runat="server" Text="Invoice No" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" Width="200px" TabIndex="22" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Invoice No.'
                                                    ControlToValidate="txtInvoiceNo" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label14" runat="server" Text="Invoice Value" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="23" ID="txtInvoiceValue" Width="150" runat="server" MaxLength="10"
                                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the invoice value.'
                                                    ControlToValidate="txtInvoiceValue" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="fltInvoiceValue" runat="server" FilterType="Numbers,Custom"
                                                    TargetControlID="txtInvoiceValue" ValidChars=".">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label36" runat="server" Text="Manual LR No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<%--<span style="color: red">*</span>--%></td>
                                            <td>
                                                <asp:TextBox ID="txtRefNo_Invoice" runat="server" Width="200px" TabIndex="23" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label37" runat="server" Text="Invoice Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInvoiceDate" runat="server" Width="80px" TabIndex="23" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtInvoiceDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="dtpBtnInv"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtnInv" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="23" Width="23px" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtInvoiceDate" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label20" runat="server" Text="Vehicle Type" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbVehicleType" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="24" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDownList" RenderMode="Block">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="cvVehicleType" runat="server" ControlToValidate="cmbVehicleType"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Vehicle Type From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%--<asp:ValidatorCalloutExtender ID="cvVehicleTypeCall" runat="server" Enabled="True"
                                                    TargetControlID="cvVehicleType">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label24" runat="server" Text="Vehicle No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="25" ID="txtVehicleNo" Width="150" runat="server" MaxLength="50"
                                                    CssClass="NormalTextBold" Style="text-transform: uppercase" />
                                                <asp:RequiredFieldValidator ID="rfvVehicleNo" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the vehicle no'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtVehicleNo"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label28" runat="server" Text="Driver's Name & No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="26" ID="txtDriverDetails" Width="150" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>&nbsp;&nbsp;
                                                <asp:RequiredFieldValidator ID="rfvDriverDetails" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the driver name'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtDriverDetails"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:TextBox TabIndex="27" ID="txtDriverNumber" Width="150" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDriverNumber" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the driver number'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtDriverNumber"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="fltDriverNumber" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="," TargetControlID="txtDriverNumber">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label32" runat="server" Text="Vehicle Seal No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="28" ID="txtvehicleSealNo" Width="270" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Packing Type" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbType" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="29" CssClass="WindowsStyle" DropDownStyle="DropDown"
                                                    AppendDataBoundItems="true" RenderMode="Block">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="cvType" runat="server" ControlToValidate="cmbType" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Packing Type From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%-- <asp:ValidatorCalloutExtender ID="cvTypeCall" runat="server" Enabled="True" TargetControlID="cvType">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label25" runat="server" Text="Description" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbDescription" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="30" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                                <asp:CustomValidator ID="cvDescription" runat="server" ControlToValidate="cmbDescription"
                                                    ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please Select Description From The List'
                                                    ClientValidationFunction="ValidatorCombobox" Display="Dynamic" ValidateEmptyText="true"
                                                    ValidationGroup="save" SetFocusOnError="true" ForeColor="Red"></asp:CustomValidator>
                                                <%--<asp:ValidatorCalloutExtender ID="cvDescriptionCall" runat="server" Enabled="True"
                                                    TargetControlID="cvDescription">
                                                </asp:ValidatorCalloutExtender>--%>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label33" runat="server" Text="Freight" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbFreight" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="31" CssClass="WindowsStyle" DropDownStyle="DropDownList"
                                                    RenderMode="Block">
                                                    <asp:ListItem Text="TO BE BILLED" Value="TO BE BILLED">TO BE BILLED</asp:ListItem>
                                                    <asp:ListItem Text="TOPAY" Value="TOPAY">TOPAY</asp:ListItem>
                                                    <asp:ListItem Text="PAID" Value="PAID">PAID</asp:ListItem>
                                                </asp:ComboBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblnoofply" runat="server" Text="No. of PlySheets" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtnoofPly" runat="server" Width="100px" MaxLength="2" TabIndex="34"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender runat="server" TargetControlID="txtnoofPly"
                                                    FilterType="Numbers">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label38" runat="server" Text="EWay Bill No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEwayBillNo" runat="server" Width="250px" MaxLength="500" TabIndex="34"
                                                    TextMode="MultiLine" Rows="3" CssClass="NormalTextBold"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the Eway Bill No'
                                                    Display="Dynamic" SetFocusOnError="true" ValidationGroup="save" ControlToValidate="txtEwayBillNo"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                    TargetControlID="txtEwayBillNo" FilterType="Numbers,Custom" ValidChars=",">
                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator runat="server" ID="regExVehicleNo" ControlToValidate="txtEwayBillNo"
                                                    ValidationExpression="^\d{12}(,\d{12})*$" ValidationGroup="save" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter valid 12 digit Eway Bill Nos'
                                                    Display="Dynamic" SetFocusOnError="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label57" runat="server" Text="Eway Bill Valid Upto" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <%-- <asp:CheckBox runat="server" ID="chkMultipoint" Text="Multipoint" TabIndex="35" AutoPostBack="True"
                                                    OnCheckedChanged="chkMultipoint_CheckedChanged" />--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEwayBillDate" Enabled="true" runat="server" Width="80px" TabIndex="35"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtEwayBillDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="dtpBtnEwayBill"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtnEwayBill" runat="server" Enabled="true" Height="20px"
                                                    ImageAlign="AbsMiddle" ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="8"
                                                    Width="23px" />
                                                <asp:RequiredFieldValidator ID="rfvMainLR" Enabled="true" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter Eway Bill Date'
                                                    ControlToValidate="txtEwayBillDate" SetFocusOnError="True" ValidationGroup="save"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label23" runat="server" Text="Supplier" Visible="false" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbSupplier" runat="server" Visible="false" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="37" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label39" runat="server" Text="Shipping Doc No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="38" ID="txtShippingDocNo" Width="270" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label40" runat="server" Text="Internal Billing No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red"></span>
                                            </td>
                                            <td>
                                                <asp:TextBox TabIndex="39" ID="txtInternalbillingNo" Width="270" runat="server" MaxLength="100"
                                                    Style="text-align: left" CssClass="NormalTextBold"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="40" ID="btnSavePacklist" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" Visible="false" />
                                        <asp:Button TabIndex="41" ID="Button1" runat="server" Text="Save" CssClass="button"
                                            CommandName="SaveP" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="42" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="2" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="4" ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" MaxLength="100" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAddPacklist" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDeletePacklist" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" Visible="false" />&nbsp;
                                            &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="ID,LRNo,LRDate,Consigner,Consignee,FromLoc,ToLoc,VehicleNo,Weight,ChargeableWt, 
                                                TotPackages,InvoiceNo,InvoiceValue,LoadType,Remarks,CreatedBy,CreatedOn,Status,CurrentRemarks,
                                                DriverDetails,DeviceID,DeviceName,SupplierID,SupplierName,SubSupplierID,SubSupplierName,CustomerID, 
                                                CustomerName,RefNo,AdvanceToSupplier,TotalFrieght,IsBilled,VehicleTypeID,VehicleTypeName,SupplierAmount,BillingCompany_Srl,
                                                CName,Rate,ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,PackingType,VehiclePlacementDate,
                                                DriverNumber,ExpectedDeliveryDays,FreightType,ConsignorGSTNo,ConsigneeGSTNo,RefNo_Invoice,InvoiceDate,MultiPoint,MainLRNo,EwayBillNo,ShipmentDocNo,InternalBillingDocNo,PlacementID,
                                                ConsignorEmail,ConsignorTelNo,ConsignorPincode,ConsigneePincode,PlyQty,EwayBillDate"
                                                OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                                OnRowCommand="grdPacklistView_RowCommand" AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                                <Columns>
                                                    <%--<asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkSelectPacklist" /></ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                    </asp:TemplateField>--%>
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="LRNo" HeaderText="LR No."
                                                        SortExpression="LRNo">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                        DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner"
                                                        ItemStyle-Width="200px"></asp:BoundField>
                                                    <asp:BoundField DataField="Consignee" HeaderText="Consignee" SortExpression="Consignee"
                                                        ItemStyle-Width="220px"></asp:BoundField>
                                                    <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc"
                                                        ItemStyle-Width="120px"></asp:BoundField>
                                                    <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc"
                                                        ItemStyle-Width="120px"></asp:BoundField>
                                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="120px">
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CurrentRemarks" HeaderText="Remarks" SortExpression="CurrentRemarks"
                                                        ItemStyle-Width="100px"></asp:BoundField>
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                    </asp:ButtonField>
                                                    <%--  <asp:ButtonField CommandName="ViewReport" Text="View">
                                                        <ItemStyle Width="80px" />
                                                    </asp:ButtonField>--%>
                                                    <asp:ButtonField CommandName="SavePdf" Text="Download">
                                                        <ItemStyle Width="50px" />
                                                    </asp:ButtonField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="DetailID, TransitStatus, TranDateTime, TransitRemarks" EmptyDataText="No records found."
                                                        Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="TransitStatus" HeaderText="Status" SortExpression="TransitStatus">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TranDateTime" HeaderText="Date" SortExpression="TranDateTime"
                                                                ItemStyle-Width="100px" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TransitRemarks" HeaderText="Remarks" SortExpression="TransitRemarks">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAddPacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDeletePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSavePacklist" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="txtChWeight" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">

        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });
        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
