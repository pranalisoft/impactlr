﻿<%@ Page Title="Vehicle Type Wise Ply Qty" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="VehicleTypeWisePlyQty.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.VehicleTypeWisePlyQty" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="btnPopUpYesNo" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlPopupYesNo" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Information
                </div>
                <div align="center" style="padding: 5px">
                    <br />
                    <asp:Label ID="lblError" runat="server" Text="Menus Assigned Successfully." CssClass="NormalTextBold"></asp:Label>
                    <br />
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="OK" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    &nbsp
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender runat="server" ID="popUpYesNo" DynamicServicePath="" Enabled="True"
                TargetControlID="btnPopUpYesNo" PopupControlID="pnlPopupYesNo" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                Vehicle Type Wise Ply Qty
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                    <div class="msg_region">
                        <iControl:MsgPanel ID="MsgPanel" runat="server" />
                        <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                    </div>
                    <div style="padding-left: 15px;">
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Ply Item" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbPlyItem" runat="server" AutoCompleteMode="SuggestAppend"
                                            Width="250px" MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="true" OnSelectedIndexChanged="cmbPlyItem_SelectedIndexChanged1">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="cvcmbPlyItem" runat="server" ControlToValidate="cmbPlyItem"
                                            ErrorMessage="Please select Ply Item from the list" ClientValidationFunction="ValidatorCombobox"
                                            Display="Dynamic" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="cvcmbPlyItemCall" runat="server" Enabled="True"
                                            TargetControlID="cvcmbPlyItem">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdVehicleType" runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False"
                                            DataKeyNames="Sel,Srl,Name" EmptyDataText="No Records Found." BackColor="#DCE8F5"
                                            Font-Size="11px" OnRowDataBound="grdVehicleType_RowDataBound">
                                            <RowStyle Height="10px" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="chkSelect" onclick="checkboxclick(this);" TabIndex="2"/></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Name" HeaderText="Vehicle Type" SortExpression="Name">
                                                    <ItemStyle Width="200px" />
                                                </asp:BoundField>
                                               <asp:TemplateField HeaderText="Qty" ControlStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtQty" CssClass="NormalTextBold" TabIndex="2"
                                                        Style="text-align: right" AutoPostBack="false" Text='<%#Eval("Qty") %>' Enabled="false"/>
                                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilterFrieght" runat="server" FilterType="Numbers" 
                                                        TargetControlID="txtQty" Enabled="True">
                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                                <ControlStyle Width="80px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="header" />
                                            <RowStyle CssClass="row" />
                                            <AlternatingRowStyle CssClass="alter_row" />
                                            <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="4" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="5" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cmbPlyItem" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0 || arguments.Value == -1) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function ValidatorComboboxSender(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }

        function checkboxclick(control) {
            var cntrlId = control.id;
            var Rowid = cntrlId.substring(cntrlId.indexOf('_ctl'), cntrlId.indexOf('_chkSelect'));
            var rowindex = Rowid.substring(4, Rowid.length);

            var gv = document.getElementById("<%= grdVehicleType.ClientID %>");
            var tb = gv.getElementsByTagName("input");
            var frt = 0;
            for (var i = 0; i < tb.length; i++) {

                if (tb[i].type == "text" && tb[i].id.indexOf('_ctl' + rowindex + '_txtQty') >= 0) {
                    tb[i].disabled = control.checked ? false : true;
                     tb[i].focus();                 
                }                
            }            
        }
    </script>
</asp:Content>
