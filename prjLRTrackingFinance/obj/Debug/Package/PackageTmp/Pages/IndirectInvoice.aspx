﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="IndirectInvoice.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.IndirectInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Ply Purchase
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div style="padding-left: 2px; padding-right: 2px">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                                <asp:HiddenField ID="txtRowNumber" runat="server" />
                                            </td>
                                        </tr>                                        
                                    </table>
                                    <table class="control_set" style="width: 100%">
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Invoice Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtDate" runat="server" Width="80px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                    ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                    Width="250px" MaxLength="200" TabIndex="2" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                            <td style="width: 120px">
                                                <asp:Label ID="Label22" runat="server" Text="Supplier" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbSupplier" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="3" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr class="control_row">
                                            <td>
                                                <asp:Label ID="Lm2" runat="server" Text="Invoice No." CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBRefNo" Width="150px" runat="server" TabIndex="4" MaxLength="50" />
                                                <asp:RequiredFieldValidator ID="reqFVRefNo" runat="server" ErrorMessage="Please enter the supplier invoice no"
                                                    ValidationGroup="save" ControlToValidate="txtBRefNo" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:ValidatorCalloutExtender ID="reqFVRefNoCall" runat="server" Enabled="True" TargetControlID="reqFVRefNo">
                                                </asp:ValidatorCalloutExtender>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Date" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtBdate" runat="server" Width="80px" TabIndex="5" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtBdate" TodaysDateFormat="dd/MMM/yyyy" PopupButtonID="dtpBBtn"
                                                    FirstDayOfWeek="Sunday">
                                                </asp:CalendarExtender>
                                                <asp:ImageButton ID="dtpBBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="6" Width="23px" />
                                                <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the supplier invoice date"
                                                    ValidationGroup="save" ControlToValidate="txtBdate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                    TargetControlID="reqFVfrmDate">
                                                </asp:ValidatorCalloutExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td colspan="4">
                                        <hr />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="Item" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:ComboBox ID="cmbItem" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                    MaxLength="200" TabIndex="7" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                    DropDownStyle="DropDown" RenderMode="Block">
                                                </asp:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Text="Qty" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQty" Width="100px" runat="server" TabIndex="8" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label7" runat="server" Text="Rate" CssClass="NormalTextBold"></asp:Label>
                                                &nbsp;<span style="color: red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRate" Width="100px" runat="server" TabIndex="9" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp; &nbsp; &nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:Button TabIndex="10" ID="btnConfirm" runat="server" Text="Confirm" CssClass="button"
                                                    CommandName="Submit" OnCommand="EntryForm_Command" ValidationGroup="confirm" />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:GridView ID="grdItems" runat="server" Style="width: 540px" AutoGenerateColumns="False"
                                        AllowPaging="false" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                        DataKeyNames="PlyItemSrl,Qty,Rate,BasicAmount,NetAmount,HSNId,GST" OnRowCommand="grdItems_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="PlyItemName" HeaderText="Item" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left"/>
                                            <asp:BoundField DataField="HSNCode" HeaderText="HSN" ItemStyle-Width="80px"  HeaderStyle-HorizontalAlign="left"/>
                                            <asp:BoundField DataField="Rate" HeaderText="Rate" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"/>
                                            <asp:BoundField DataField="Qty" HeaderText="Qty" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"/>
                                            <%--<asp:BoundField DataField="BasicAmount" HeaderText="Basic Amount" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right"/>--%>
                                            <%--<asp:BoundField DataField="GST" HeaderText="GST" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right"/>--%>
                                            <asp:BoundField DataField="NetAmount" HeaderText="Amount" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"/>
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                        <PagerSettings Mode="Numeric" />
                                    </asp:GridView>
                                </div>
                                <table class="control_set" style="width: 540px">
                                    <%--<tr class="control_row">
                                        <td>
                                        </td>
                                        <td style="width: 150px">
                                            Basic Amount
                                        </td>
                                        <td style="width: 85px">
                                            <asp:Label runat="server" ID="lblBasicAmount" Font-Bold="true" Style="text-align: right">
                                            </asp:Label>
                                        </td>
                                    </tr>--%>
                                    <tr class="control_row">
                                        <td>
                                        </td>
                                        <td style="width: 150px">
                                            Invoice Amount
                                        </td>
                                        <td style="width: 80px;text-align:right;padding-right:4px">
                                            <asp:Label runat="server" ID="lblInvoiceAmount" Font-Bold="true" Style="text-align: right">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="11" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="12" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px">
                                        <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" CssClass="NormalTextBold"
                                            ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton1"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                                    </td>
                                    <td width="80px">
                                        <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="150px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" CssClass="NormalTextBold"
                                            ContentEditable="False"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                            TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy" PopupButtonID="ImageButton2"
                                            FirstDayOfWeek="Sunday">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                                    </td>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="220px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="5" MaxLength="10" Width="200px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="80px">
                                        <asp:Button TabIndex="6" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="580px">
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="7" ID="btnAdd" runat="server" Text="Add" CssClass="button"
                                                CommandName="Add" OnCommand="EntryForm_Command" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="8" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                                CommandName="Delete" OnCommand="EntryForm_Command" Visible="false" />&nbsp;
                                            &nbsp;
                                            <asp:Button TabIndex="9" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                                OnClick="btnMainPg_Click" Style="width: 120px" />
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdInvoiceHeader" runat="server" Width="580px" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="Srl,SupplierName,BillingCompany_Srl,Supplier_Srl,InvoiceAmount,BuyerRefDate,BuyerRefNo"
                                                OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowCommand="grdPacklistView_RowCommand"
                                                AllowSorting="True" OnSorting="grdPacklistView_Sorting">
                                                <Columns>
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Srl" HeaderText="Invoice No."
                                                        SortExpression="InvRefNo">
                                                        <ItemStyle Width="100px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="CreatedOn" HeaderText="Date" SortExpression="CreatedOn"
                                                        ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}"/>                                                        
                                                    <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                                        ItemStyle-Width="200px"/>                                                       
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount"
                                                        ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right"/>
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="60px"
                                                        ItemStyle-HorizontalAlign="Center"/>
                                                        
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="" EmptyDataText="No records found." Width="100%" PageSize="1000"
                                                        ShowHeaderWhenEmpty="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="PlyItemName" HeaderText="Item" />
                                                            <asp:BoundField DataField="HSNCode" HeaderText="HSN" />
                                                            <asp:BoundField DataField="Rate" HeaderText="Rate" ItemStyle-Width="80px" />
                                                            <asp:BoundField DataField="Qty" HeaderText="Qty" ItemStyle-Width="80px" />
                                                            <asp:BoundField DataField="BasicAmount" HeaderText="Basic Amount" ItemStyle-Width="100px" />
                                                            <asp:BoundField DataField="GST" HeaderText="GST" ItemStyle-Width="80px" />
                                                            <asp:BoundField DataField="NetAmount" HeaderText="Net Amount" ItemStyle-Width="100px" />
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMainPg" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
