﻿<%@ Page Title="Post POD Updation - Edit" Language="C#" MasterPageFile="~/LRSite.Master"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="PostPODUpdationEdit.aspx.cs"
    Inherits="prjLRTrackerFinanceAuto.Pages.PostPODUpdationEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="BtnTest" runat="server" Text="" Style="display: none" />            
            <table width="100%">
                <tr>
                    <td>                       
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Post POD Updation - Edit
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <div class="body" style="padding-left: 2px; padding-right: 2px;margin-top 50px;">
                <div>
                    <asp:MultiView ID="mltVwPacklist" ActiveViewIndex="0" runat="server">
                        <asp:View ID="vwEntry" runat="server">
                            <asp:Panel ID="pnlEntry" runat="server">
                                <div class="entry_form">
                                    <%--<table width="100%">
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="txtHiddenId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                <asp:Label ID="Label13" runat="server" Text="LR Info" ForeColor="Black" Font-Bold="true"
                                                    Font-Size="13px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>--%>
                                    <table class="control_set" style="width: 100%">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                        <asp:HiddenField ID="txtLRId" runat="server" />
                                        <asp:HiddenField ID="txthdPODFileName" runat="server" />
                                            <tr>
                                            <td style="width: 270px;vertical-align:top">
                                                <asp:Panel ID="pnl1" runat="server" CssClass="pnl" Style="height: 430px !important">
                                                    <table class="control_set" style="width: 100%">
                                                        <tr class="control_row">
                                                            <td style="width: 140px">
                                                                <asp:Label ID="Label1" runat="server" Text="LR No" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td style="width: 120px">
                                                                <asp:TextBox ID="txtLRNo" runat="server" Enabled="false" TabIndex="1" Font-Bold="true"
                                                                    Width="100px" CssClass="NormalTextBold"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label10" runat="server" Text="Freight" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtFright" CssClass="NormalTextBold" TabIndex="2"
                                                                    Style="text-align: right" Enabled="false" Width="100px" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label13" runat="server" Text="Advance Paid" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtAdvance" CssClass="NormalTextBold" TabIndex="3"
                                                                    Style="text-align: right" Enabled="false" Width="100px" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label15" runat="server" Text="Balance" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtBalance" CssClass="NormalTextBold" TabIndex="4"
                                                                    Style="text-align: right; border-color: black;" Enabled="false" Width="100px" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label14" runat="server" Text="Detention Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtDetention" CssClass="NormalTextBold" TabIndex="5"
                                                                    Width="100px" Style="text-align: right" onchange="calculate();" />
                                                                <asp:RequiredFieldValidator ID="reqFVLRNo" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtDetention" SetFocusOnError="True" ValidationGroup="save"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterDetention" runat="server" FilterType="Numbers"
                                                                    TargetControlID="txtDetention" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label2" runat="server" Text="Warai Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtWarai" CssClass="NormalTextBold" Style="text-align: right"
                                                                    Width="100px" TabIndex="6" onchange="calculate();" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtWarai" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterWarai" runat="server" FilterType="Numbers"
                                                                    TargetControlID="txtWarai" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label3" runat="server" Text="Two Point Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtTwoPoint" CssClass="NormalTextBold" TabIndex="7"
                                                                    Width="100px" Style="text-align: right" onchange="calculate();" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtTwoPoint" SetFocusOnError="True" ValidationGroup="save"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterTwoPoint" runat="server" FilterType="Numbers"
                                                                    TargetControlID="txtTwoPoint" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label4" runat="server" Text="Other Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtOtherCharges" CssClass="NormalTextBold" Style="text-align: right"
                                                                    Width="100px" TabIndex="8" onchange="calculate();" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtOtherCharges" SetFocusOnError="True" ValidationGroup="save"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterOther" runat="server" FilterType="Numbers"
                                                                    TargetControlID="txtOtherCharges" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label7" runat="server" Text="Narration" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtRemarks" CssClass="NormalTextBold" MaxLength="100"
                                                                    Width="100px" TabIndex="9" Style="text-align: left" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label8" runat="server" Text="Late POD Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtLatePODCharges" CssClass="NormalTextBold" TabIndex="10" Enabled="false"
                                                                    Width="100px" Style="text-align: right" onchange="calculate();" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtLatePODCharges" SetFocusOnError="True" ValidationGroup="save"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                    FilterType="Numbers" TargetControlID="txtLatePODCharges" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label11" runat="server" Text="Total Penalty" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtTotalPenalty" CssClass="NormalTextBold" TabIndex="11"
                                                                    Enabled="false" Width="100px" Style="text-align: right" onchange="calculate();" />
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                    FilterType="Numbers" TargetControlID="txtTotalPenalty" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label5" runat="server" Text="Total Charges" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtTotalCharges" CssClass="NormalTextBold" TabIndex="12"
                                                                    Width="100px" Style="text-align: right" Enabled="false" />
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterTotal" runat="server" FilterType="Numbers,custom"
                                                                    ValidChars="-" TargetControlID="txtTotalCharges" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label17" runat="server" Text="Total Balance Payment" CssClass="NormalTextBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtTotalBalance" CssClass="NormalTextBold" TabIndex="13"
                                                                    Font-Bold="true" Width="100px" Style="text-align: right; border-color: black;"
                                                                    Enabled="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td colspan="2" style="text-align: center">
                                                                <asp:CheckBox runat="server" TabIndex="14" Font-Bold="true" ID="chkAccountsApproval"
                                                                    Text="Operations Approval" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td style="vertical-align: top; width: 430px">
                                                <asp:Panel ID="pnl2" runat="server" CssClass="pnl" Style="height: 305px !important;">
                                                    <table class="control_set" style="width: 100%">
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label29" Text="POD File" runat="server" />
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:LinkButton ID="hplPODFile" runat="server" Text="View File" OnClick="hplPODFile_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label9" runat="server" Text="Penalty Remark" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:ComboBox ID="cmbReason" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                                    MaxLength="200" TabIndex="15" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                                    DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="false">
                                                                </asp:ComboBox>
                                                                <asp:CustomValidator ID="custValidReason" runat="server" ControlToValidate="cmbReason"
                                                                    ErrorMessage="Please select Reason from the list" ClientValidationFunction="ValidatorCombobox"
                                                                    Display="Dynamic" ValidateEmptyText="true" ValidationGroup="confirmPenalty" SetFocusOnError="true"></asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td style="width: 120px">
                                                                <asp:Label ID="Label6" runat="server" Text="Penalty Charges" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;<span style="color: red">*</span>
                                                            </td>
                                                            <td style="width: 250px">
                                                                <asp:TextBox runat="server" ID="txtPenalty" CssClass="NormalTextBold" TabIndex="16"
                                                                    Width="100px" Style="text-align: right" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> value required.'
                                                                    ControlToValidate="txtPenalty" SetFocusOnError="True" ValidationGroup="confirmPenalty"
                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilterPenalty" runat="server" FilterType="Numbers"
                                                                    TargetControlID="txtPenalty" Enabled="True">
                                                                </ajaxToolkit:FilteredTextBoxExtender>
                                                                <asp:RegularExpressionValidator ID="revAvailablePeriod" runat="server" ErrorMessage="Must be greater than 0"
                                                                    ControlToValidate="txtPenalty" ValidationExpression="^[1-9][0-9]*$" SetFocusOnError="true"
                                                                    ValidationGroup="confirmPenalty">
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td style="width: 120px">
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Button runat="server" ID="btnConfirm" Text="Confirm" CssClass="button" Width="70px"
                                                                    TabIndex="17" ValidationGroup="confirmPenalty" OnClick="btnConfirm_Click" />&nbsp;
                                                                <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button" Width="70px"
                                                                    TabIndex="18" OnClick="btnDelete_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td colspan="2">
                                                                <div class="grid_region" style="height: 170px; overflow: auto">
                                                                    <asp:GridView ID="grdItems" runat="server" Width="400px" AutoGenerateColumns="False"
                                                                        CssClass="grid" DataKeyNames="ReasonSrl,Reason,Charges" EmptyDataText="No Records Found.">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                                        AutoPostBack="true" /></HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" Width="28px" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Reason" HeaderText="Reason" SortExpression="Reason"></asp:BoundField>
                                                                            <asp:BoundField DataField="Charges" HeaderText="Charges" SortExpression="Charges">
                                                                                <ItemStyle Width="80px" HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="header" />
                                                                        <RowStyle CssClass="row" />
                                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                                        <PagerSettings Mode="Numeric" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel1" runat="server" CssClass="pnl" Style="height: 121px !important;
                                                    margin-top: 4px !important;">
                                                    <table class="control_set" style="width: 100%">
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label12" runat="server" Text="LR Date" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:Label ID="lblLRDate" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td style="width: 120px">
                                                                <asp:Label ID="Label18" runat="server" Text="Reporting" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td style="width: 120px">
                                                                <asp:TextBox ID="txtReportingDate" runat="server" Width="80px" TabIndex="19" CssClass="NormalTextBold"
                                                                    Enabled="true" AutoPostBack="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                                    ControlToValidate="txtReportingDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <asp:CalendarExtender ID="txtReportingDate_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtReportingDate" TodaysDateFormat="dd/MMM/yyyy"
                                                                    PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                                                </asp:CalendarExtender>
                                                                <asp:ImageButton ID="dtpBtn" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="20" Width="23px" />
                                                            </td>
                                                            <td style="width: 120px">
                                                                <asp:Label ID="Label20" runat="server" Text="Delayed Days" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDelay" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label19" runat="server" Text="Unloading" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                 <asp:TextBox ID="txtUnloadingDate" runat="server" Width="80px" TabIndex="21" CssClass="NormalTextBold"
                                                                    Enabled="true" AutoPostBack="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                                    ControlToValidate="txtUnloadingDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <asp:CalendarExtender ID="txtUnloadingDate_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtUnloadingDate" TodaysDateFormat="dd/MMM/yyyy"
                                                                    PopupButtonID="dtpBtnUnload" FirstDayOfWeek="Sunday">
                                                                </asp:CalendarExtender>
                                                                <asp:ImageButton ID="dtpBtnUnload" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="22" Width="23px" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label21" runat="server" Text="Detain Days" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblDetain" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label32" runat="server" Text="POD Scan" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPODDate" runat="server" Width="80px" TabIndex="23" CssClass="NormalTextBold"
                                                                    Enabled="true" AutoPostBack="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                                    ControlToValidate="txtPODDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                <asp:CalendarExtender ID="txtPODDate_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtPODDate" TodaysDateFormat="dd/MMM/yyyy"
                                                                    PopupButtonID="dtpBtnPOD" FirstDayOfWeek="Sunday">
                                                                </asp:CalendarExtender>
                                                                <asp:ImageButton ID="dtpBtnPOD" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="24" Width="23px" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label33" runat="server" Text="POD Late Days" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblPODLate" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td style="vertical-align: top;">
                                             <asp:Panel ID="Panel2" runat="server" CssClass="pnl" Style="height: 305px !important;">
                                                <div class="grid_top_region" style="margin-left:35px;margin-top:0px;padding-top:5px">
                                                    <asp:Label ID="Label28" Text="0 Freight LR(s)" runat="server" Font-Bold="true" Font-Size="15px" />
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdLrZero" runat="server" Width="410px" AutoGenerateColumns="False"
                                                        CssClass="grid" DataKeyNames="ID,LRNo,LRDate,PODFile,PODDate,PODScanned" EmptyDataText="No Records Found."
                                                        OnRowCommand="grdLrZero_RowCommand" OnRowDataBound="grdLrZero_RowDataBound">
                                                        <Columns>
                                                            <asp:BoundField DataField="LRNo" HeaderText="LRNo" SortExpression="LRNo" ItemStyle-Width="100px" />
                                                            <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                                DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="PODDate" HeaderText="PODDate" SortExpression="PODDate"
                                                                ItemStyle-Width="80px" DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField DataField="PODScanned" HeaderText="PODScanned" SortExpression="PODScanned"
                                                                ItemStyle-Width="50px" />
                                                            <asp:ButtonField CommandName="viewFile" HeaderText="" Text="View File" ItemStyle-Width="100px"
                                                                ItemStyle-HorizontalAlign="Center" />
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                        <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                        <PagerSettings Mode="Numeric" />
                                                    </asp:GridView>
                                                </div>
                                                 </asp:Panel>
                                                <asp:Panel ID="Panel3" runat="server" CssClass="pnl" Style="height: 121px !important;
                                                    margin-top: 4px !important;">
                                                    <table class="control_set" style="width: 100%">
                                                        <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label34" runat="server" Text="Supplier Name" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:Label ID="lblSupplier" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr class="control_row">
                                                            <td style="width: 120px">
                                                                <asp:Label ID="Label35" runat="server" Text="Customer Name" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCustomer" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                           <%-- <td style="width: 120px">
                                                                <asp:Label ID="Label26" runat="server" Text="Delayed Days" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label27" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>--%>
                                                        </tr>
                                                       <%-- <tr class="control_row">
                                                            <td>
                                                                <asp:Label ID="Label28" runat="server" Text="Unloading" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label30" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label31" runat="server" Text="Detain Days" CssClass="NormalTextBold"></asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label32" runat="server" Text="" CssClass="NormalTextBoldForBold"></asp:Label>
                                                            </td>
                                                        </tr>--%>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        &nbsp; &nbsp; &nbsp;&nbsp;
                                        <asp:Button TabIndex="25" ID="btnSavePostPOD" runat="server" Text="Save" CssClass="button"
                                            CommandName="Save" OnCommand="EntryForm_Command" ValidationGroup="save" />
                                        &nbsp;&nbsp;
                                        <asp:Button TabIndex="26" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            CommandName="None" OnCommand="EntryForm_Command" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="vwPacklistView" runat="server">
                            <table width="100%">
                                <tr>
                                    <td width="80px" style="padding-top: 3px">
                                        <asp:Label ID="Label16" Text="Search" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                    <td width="370px">
                                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="1" MaxLength="100" Width="350px"
                                            CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                    <td width="150px"">
                                        <asp:CheckBox runat="server" ID="chkPending" Text="Pending For Approval" />
                                    </td>
                                    <td width="280px">
                                        <asp:Button TabIndex="2" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                            OnClick="btnSearch_Click" />&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="button"
                                            OnClick="ExportToExcel" />
                                    </td>
                                    <td>
                                        <asp:Button TabIndex="3" ID="btnAddPostPOD" runat="server" Text="Add Post POD" CssClass="button"
                                            CommandName="Add" OnCommand="EntryForm_Command" Visible="false" />&nbsp; &nbsp;
                                        <asp:Button TabIndex="4" ID="btnUpdatePostPOD" runat="server" Text="Edit Post POD"
                                            CssClass="button" CommandName="SaveP" OnCommand="EntryForm_Command" Visible="false" />&nbsp;
                                        &nbsp;
                                        <asp:Button TabIndex="5" ID="btnMainPg" runat="server" Text="Goto Main Page" CssClass="button"
                                            OnClick="btnMainPg_Click" Style="width: 120px" />
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblTotalPacklist" Text="Total Records : 000" runat="server" CssClass="NormalTextBold"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div class="grid_region">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-top: 3px;" colspan="2">
                                            <asp:GridView ID="grdPacklistView" runat="server" Width="100%" AutoGenerateColumns="False"
                                                AllowPaging="True" EmptyDataText="No Records Found." CssClass="grid" PageSize="10"
                                                DataKeyNames="ID,LRNo,LRDate,Consigner,Consignee,FromLoc,ToLoc,VehicleNo,Weight,ChargeableWt, 
                                                TotPackages,InvoiceNo,InvoiceValue,LoadType,Remarks,CreatedBy,CreatedOn,Status,CurrentRemarks,
                                                DriverDetails,DeviceID,DeviceName,SupplierID,SupplierName,SubSupplierID,SubSupplierName,CustomerID, 
                                                CustomerName,RefNo,AdvanceToSupplier,TotalFrieght,IsBilled,VehicleTypeID,VehicleTypeName,SupplierAmount,
                                                BillingCompany_Srl,CName,Rate,ConsigneeAddress,ConsigneeEmail,ConsigneeTelNo,ConsignerAddress,VehicleSealNo,
                                                PackingType,VehiclePlacementDate,DriverNumber,ExpectedDeliveryDays,FreightType,ConsignorGSTNo,ConsigneeGSTNo,
                                                RefNo_Invoice,InvoiceDate,CashAdvance,FuelAdvance,EwayBillNo,MultiPoint,MainLRNo,ShipmentDocNo,InternalBillingDocNo,
                                                DetentionCharges,WaraiCharges,TwoPointCharges,OtherCharges,Penalty,PODRemarks,LatePODCharges,TotalCharges,PODID,PODFile,
                                                ReportingDate,UnloadingDate,DelayForDelivery,DetainDays,Approved,PODDate,UnloadingChargesPaid"
                                                OnPageIndexChanging="grdPacklistView_PageIndexChanging" OnRowDataBound="grdPacklistView_RowDataBound"
                                                OnRowCommand="grdPacklistView_RowCommand" AllowSorting="false" OnSorting="grdPacklistView_Sorting">
                                                <Columns>                                                   
                                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="LRNo" HeaderText="LR No."
                                                        SortExpression="LRNo">
                                                        <ItemStyle Width="100px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="RefNo" HeaderText="Ref No" SortExpression="RefNo"  ItemStyle-Width="100px"/>   
                                                    <asp:BoundField DataField="LRDate" HeaderText="Date" SortExpression="LRDate" ItemStyle-Width="80px"
                                                        DataFormatString="{0:dd/MM/yyyy}">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                     <asp:BoundField DataField="ChargeableWt" HeaderText="ChargeableWt" SortExpression="ChargeableWt"
                                                        ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Consigner" HeaderText="Consigner" SortExpression="Consigner"
                                                        ItemStyle-Width="200px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Consignee" HeaderText="Consignee" SortExpression="Consignee"
                                                        ItemStyle-Width="160px">
                                                        <ItemStyle Width="160px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FromLoc" HeaderText="Origin" SortExpression="FromLoc"
                                                        ItemStyle-Width="120px">
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ToLoc" HeaderText="Destination" SortExpression="ToLoc"
                                                        ItemStyle-Width="120px">
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" ItemStyle-Width="120px">
                                                        <ItemStyle Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CurrentRemarks" HeaderText="Remarks" SortExpression="CurrentRemarks" />
                                                    <asp:ButtonField CommandName="Item" HeaderText="" Text="Details" ItemStyle-Width="100px"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                    </asp:ButtonField>
                                                    <asp:TemplateField HeaderText="Approved" ItemStyle-Width="60px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" ID="chkApproved" Checked='<%#bool.Parse(Eval("Approved").ToString()) %>'
                                                                Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="header" />
                                                <RowStyle CssClass="row" />
                                                <AlternatingRowStyle CssClass="alter_row" />
                                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                                <PagerSettings Mode="Numeric" />
                                            </asp:GridView>
                                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                                <div class="grid_top_region">
                                                    <div class="grid_top_region_lft">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="grid_top_region_rght">
                                                        Records :
                                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="grid_top_region" style="border: 1px solid black">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label22" runat="server" Text="Frieght" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblFrieght" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Advance" runat="server" Text="Advance" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblAdvance" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblBalanceCaption" runat="server" Text="Balance" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblBalance" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label23" runat="server" Text="Detention" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblDetention" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label24" runat="server" Text="Warai" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblWarai" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label26" runat="server" Text="Two Point" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblTwopoint" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label240" runat="server" Text="Other" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblOther" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                            </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label27" runat="server" Text="Late POD" Font-Bold="true"></asp:Label>
                                                            </td> 
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblLatePOD" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                            </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label30" runat="server" Text="Penalty" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblPenalty" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                            </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label25" runat="server" Text="Total Charges" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblTotalCharges" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                            </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label31" runat="server" Text="Balance Payment" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td style="text-align:right">
                                                                <asp:Label ID="lblBalancePay" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                        <td>
                                                          <asp:Button ID="btnApprove" Text="Approve" runat="server"  CssClass="button" 
                                                                onclick="btnApprove_Click" />
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="grid_region">
                                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                        DataKeyNames="DetailID, TransitStatus, TranDateTime, TransitRemarks" EmptyDataText="No records found."
                                                        Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                                        <Columns>
                                                            <asp:BoundField DataField="TransitStatus" HeaderText="Status" SortExpression="TransitStatus">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TranDateTime" HeaderText="Date" SortExpression="TranDateTime"
                                                                ItemStyle-Width="100px" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TransitRemarks" HeaderText="Remarks" SortExpression="TransitRemarks">
                                                                <ItemStyle Width="250px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="header" />
                                                        <RowStyle CssClass="row" />
                                                        <AlternatingRowStyle CssClass="alter_row" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function calculate() {
            var TotalAmt = 0;
            var Balance = 0;
            if (parseFloat(document.getElementById("<%= txtBalance.ClientID %>").value) > 0) {
                Balance = parseFloat(document.getElementById("<%= txtBalance.ClientID %>").value);
            }

            if (parseFloat(document.getElementById("<%= txtDetention.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) + parseFloat(document.getElementById("<%= txtDetention.ClientID %>").value);
            }
            if (parseFloat(document.getElementById("<%= txtWarai.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) + parseFloat(document.getElementById("<%= txtWarai.ClientID %>").value);
            }
            if (parseFloat(document.getElementById("<%= txtTwoPoint.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) + parseFloat(document.getElementById("<%= txtTwoPoint.ClientID %>").value);
            }
            if (parseFloat(document.getElementById("<%= txtOtherCharges.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) + parseFloat(document.getElementById("<%= txtOtherCharges.ClientID %>").value);
            }
            if (parseFloat(document.getElementById("<%= txtTotalPenalty.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) - parseFloat(document.getElementById("<%= txtTotalPenalty.ClientID %>").value);
            }
            if (parseFloat(document.getElementById("<%= txtLatePODCharges.ClientID %>").value) > 0) {
                TotalAmt = parseFloat(TotalAmt) - parseFloat(document.getElementById("<%= txtLatePODCharges.ClientID %>").value);
            }

            document.getElementById("<%= txtTotalCharges.ClientID %>").value = parseFloat(TotalAmt);

            if (document.getElementById("<%= txtWarai.ClientID %>").disabled) {
                document.getElementById("<%= txtTotalBalance.ClientID %>").value = parseFloat(TotalAmt) + parseFloat(Balance) - parseFloat(document.getElementById("<%= txtWarai.ClientID %>").value);
            }
            else {
                document.getElementById("<%= txtTotalBalance.ClientID %>").value = parseFloat(TotalAmt) + parseFloat(Balance);
            }
        }    
    </script>
</asp:Content>
