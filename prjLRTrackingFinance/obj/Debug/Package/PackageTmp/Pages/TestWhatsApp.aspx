﻿<%@ Page Title="Send Whatsapp" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="TestWhatsApp.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.TestWhatsApp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
 <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Test Whatsapp Messege
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 90%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label2" Text="Send To" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" Width="176px" runat="server" TabIndex="1" 
                                                            CssClass="NormalTextBold" MaxLength="10"/>                                                        
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td >
                                                        <asp:Label ID="Label1" Text="Supplier" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbSupl" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupl_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>                                                        
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label4" Text="Payment No" runat="server" AssociatedControlID="cmbLR" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:ComboBox ID="cmbLR" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            TabIndex="3" CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>                                                        
                                                    </td>
                                                </tr>                                               
                                            </table>
                                        </td>                                        
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Send" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="4" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="5" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbSupl" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
<script type="text/javascript" language="javascript">   
    function ValidatorCombobox(source, arguments) {
        if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
            arguments.IsValid = false;
        } else {
            arguments.IsValid = true;
        }
    }
    </script>
</asp:Content>
