﻿<%@ Page Title="Unloading Charges Payment" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="UpdateUnloadingChargesPayment.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdateUnloadingChargesPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <table width="100%">
                        <tr class="centerPageHeader">
                            <td class="centerPageHeader">
                                LR Status Updation
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: White; height: 2px;">
                            </td>
                        </tr>
                    </table>
                    <div>
                        <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                            <asp:Label ID="lblMsg" Text="" runat="server" />
                        </asp:Panel>
                    </div>
                    <div style="padding-left: 15px;">
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ComboBox ID="cmbLR" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                            OnSelectedIndexChanged="cmbLR_SelectedIndexChanged" TabIndex="1" AutoPostBack="true"
                                            CssClass="WindowsStyle" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbLR" ErrorMessage="Please select LR from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                            </table>
                            <table style="border: 1px solid blue" cellspacing="10px">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbl001" Text="LR No." runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbl002" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLRNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label3" Text="Date" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label131" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDate" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="Label5" Text="Consigner" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td style="width: 10px">
                                        <asp:Label ID="Label6" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConsigner" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td style="width: 150px">
                                        <asp:Label ID="Label01" Text="Consignee" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td style="width: 10px">
                                        <asp:Label ID="Label141" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConsignee" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" Text="From" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label8" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFrom" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label16" Text="To" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label18" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" Text="Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label10" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label11" Text="Chargeable Weight" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label121" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblChargeableWeight" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label15" Text="Vehicle No" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label17" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblVehicleNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label19" Text="Total Packages" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label20" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPackages" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label132" Text="Invoice No" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label142" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label21" Text="Invoice Value" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label22" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceValue" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label23" Text="Type" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label24" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblType" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label26" Text="Remarks" runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label27" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRemarks" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" Text="Driver's Name & No." runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label13" Text=" : " runat="server" CssClass="NormalTextBoldForBold" />
                                    </td>
                                    <td colspan="4">
                                        <asp:Label ID="lblDriverDetails" Text="" runat="server" CssClass="NormalTextBold" />
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lblDriverMobNo" Text="" runat="server" CssClass="NormalTextBold" />
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Unloading Charges" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox TabIndex="2" ID="txtUnloadingCharges" Width="200" runat="server" MaxLength="100"
                                            CssClass="NormalTextBold" Enabled="false"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator ID="rfvpartDesc" runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                            ControlToValidate="txtUnloadingCharges" SetFocusOnError="True" ValidationGroup="save"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <ajaxToolkit:FilteredTextBoxExtender runat="server" ID="fltUnloading" TargetControlID="txtUnloadingCharges"
                                            FilterType="Custom,Numbers" ValidChars=".">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="3" CssClass="NormalTextBold" />
                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                            Width="22px" TabIndex="4" />&nbsp;
                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the Payment date"
                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                            TargetControlID="reqFVfrmDate">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Button TabIndex="5" ID="btnSave" runat="server" Text="Save" CssClass="button"
                                            OnClick="btnSave_Click" ValidationGroup="save" />
                                        &nbsp &nbsp
                                        <asp:Button TabIndex="6" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                            OnClick="btnCancel_Click" />
                                        &nbsp&nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr style="color: Gray; width: 100%" align="right" />
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="font-size: 12px; font-weight: bold; height: 15px">
                                        Records :
                                        <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                                    </td>
                                </tr>
                            </table>                           
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
