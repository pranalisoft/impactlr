﻿<%@ Page Title="Tally Excel Template" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="TallyExcelGeneration.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.TallyExcelGeneration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Tally Excel Template (Sales)
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <table class="control_set" width="100%">
                        <tr class="control_row">
                            <td width="80px">
                                <asp:Label ID="Label18" Text="From Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td width="150px">
                                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" TabIndex="1" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="frmDt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select from date'
                                    ControlToValidate="txtFromDate" SetFocusOnError="True" ValidationGroup="save"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFromDate" TodaysDateFormat="dd/MM/yyyy"
                                    PopupButtonID="ImageButton1" FirstDayOfWeek="Sunday">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="2" Width="23px" />
                            </td>
                            <td width="80px">
                                <asp:Label ID="Label17" Text="To Date" runat="server" CssClass="NormalTextBold"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtToDate" runat="server" Width="100px" TabIndex="3" CssClass="NormalTextBold"
                                    ContentEditable="False"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="toDt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select to date'
                                    ControlToValidate="txtToDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtToDate" TodaysDateFormat="dd/MM/yyyy"
                                    PopupButtonID="ImageButton2" FirstDayOfWeek="Sunday">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                    ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="4" Width="23px" />
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label22" runat="server" Text="Billing Company" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td colspan="3">
                                <ajaxToolkit:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend"
                                    Width="250px" MaxLength="200" TabIndex="5" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                    DropDownStyle="DropDown" RenderMode="Block">
                                </ajaxToolkit:ComboBox>
                                <asp:CustomValidator ID="Customer" runat="server" ControlToValidate="cmbCustomer"
                                    ErrorMessage="Please select customer from the list" ClientValidationFunction="ValidatorCombobox"
                                    Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td width="80px">
                            </td>
                            <td colspan="3">
                                <asp:Button ID="btnSearch" runat="server" Text="Show" CssClass="button"  
                                    TabIndex="6" ValidationGroup="save" Visible="false" />&nbsp;&nbsp;
                                <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="button" TabIndex="7"
                                    OnClick="btnExcel_Click"  />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
</asp:Content>
