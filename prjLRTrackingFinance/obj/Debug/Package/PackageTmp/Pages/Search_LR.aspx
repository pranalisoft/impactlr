﻿<%@ Page Title="Search LR" Language="C#" AutoEventWireup="true" CodeBehind="Search_LR.aspx.cs"
    Inherits="prjLRTrackerFinanceAuto.Pages.Search_LR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</head>
<body style="width: auto; height: auto; font-family: Segoe UI;">
    <form id="form1" runat="server">
    <div style="height: 25px; width: 98%; margin-left: 12px; margin-top: 12px; background-color: #3A66AF;
        color: white; font-size: 16px; font-weight: bold; text-align: center">
        &nbsp;&nbsp; Track Your Consignment
    </div>
    <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;"
        cellpadding="0" cellspacing="10">
        <tr>
            <td colspan="6" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:Label ID="lblLRLabel" Text="" runat="server" Style="font-size: 16px; font-weight: bold" />
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:Label ID="lblError" Text="Sorry !!! LR No. Not Found" runat="server" Style="font-size: 16px;
                    font-weight: bold; color: Red" />
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center" style="border-top: 2px solid black">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
            </td>
        </tr>
    </table>
    <table id="tblDetails" runat="server" style="width: 98%; margin-left: 12px; margin-bottom: 12px;
        border: 1px solid black;" cellpadding="0" cellspacing="10">
        <tr>
            <td>
                <asp:Label ID="Label1" Text="Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label13" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td colspan="4">
                <asp:Label ID="lblDate" Text="" runat="server" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="Label2" Text="Consigner" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td style="width: 10px">
                <asp:Label ID="Label3" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblConsigner" Text="" runat="server" Style="font-size: 16px;" />
            </td>
            <td style="width: 150px">
                <asp:Label ID="Label12" Text="Consignee" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td style="width: 10px">
                <asp:Label ID="Label14" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblConsignee" Text="" runat="server" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" Text="From" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label5" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblFrom" Text="" runat="server" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label16" Text="To" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label18" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblTo" Text="" runat="server" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" Text="Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label7" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblWeight" Text="" runat="server" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label8" Text="Chargeable Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label9" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblChargeableWeight" Text="" runat="server" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label15" Text="Vehicle No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label17" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblVehicleNo" Text="" runat="server" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label19" Text="Total Packages" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label20" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblPackages" Text="" runat="server" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" Text="Invoice No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label11" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblInvoiceNo" Text="" runat="server" Style="font-size: 16px;" />
            </td>
             <td>
                <asp:Label ID="Label25" Text="POD Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label26" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblPODDate" Text="" runat="server" Style="font-size: 16px;" />
            </td>
           <%-- <td>
                <asp:Label ID="Label21" Text="Invoice Value" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label22" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblInvoiceValue" Text="" runat="server" Style="font-size: 16px;" />
            </td>--%>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label23" Text="Type" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label24" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblType" Text="" runat="server" Style="font-size: 16px;" />
            </td>
             <td>
                <asp:Label ID="Label27" Text="Driver's Name & No." runat="server" Font-Bold="True"
                    Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label28" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="lblDriverDtls" Text="" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
        </tr>
        <tr>
           
           <%-- <td>
                <asp:Label ID="Label29" Text="POD File" runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:Label ID="Label30" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
            </td>
            <td>
                <asp:LinkButton ID="hplPODFile" runat="server" Text="View File" OnClick="hplPODFile_Click"></asp:LinkButton>
            </td>--%>
        </tr>       
        <tr>
            <td colspan="6">
                <asp:ListView ID="lstViewPayments" runat="server">
                    <LayoutTemplate>
                        <table width="60%" border="1" cellspacing="0" cellpadding="2" style="border: 1px solid black;
                            border-collapse: collapse; font-size: 14px; font-family: Segoe UI; margin-top: 10px;
                            margin-bottom: 10px; margin-left: 0px">
                            <tr style="background-color: #3A66AF; color: white">
                                <td style="width: 25px; font-weight: bold; text-align: center; border-collapse: collapse">
                                    #
                                </td>
                                <td style="width: 120px; font-weight: bold; text-align: center; border-collapse: collapse">
                                    Date Time
                                </td>
                                <td style="width: 100px; font-weight: bold; text-align: center; border-collapse: collapse">
                                    Status
                                </td>
                                <td style=" width: 200px; font-weight: bold; text-align: center;  border-collapse: collapse">
                                    Remarks
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center; border-collapse: collapse">
                                <%# Container.DataItemIndex + 1 %>
                            </td>
                            <td style="text-align: center; border-collapse: collapse">
                                <%# Convert.ToDateTime(Eval("TranDateTime")).ToString("dd/MM/yyyy HH:mm")%>
                            </td>
                            <td style="border-collapse: collapse">
                                <%# Eval("TransitStatus")%>
                            </td>
                            <td style="border-collapse: collapse;word-wrap: break-word;">
                                <%# Eval("TransitRemarks")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <%--<p style="font-size: 14px; font-family: Segoe UI; text-align: center">
                            X-------------------- No Records Found --------------------X</p>--%>
                    </EmptyDataTemplate>
                </asp:ListView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
