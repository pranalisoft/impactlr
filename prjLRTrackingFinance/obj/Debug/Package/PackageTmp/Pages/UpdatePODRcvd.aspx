﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="UpdatePODRcvd.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.UpdatePODRcvd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div>
                <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                    <asp:Label ID="lblMsg" Text="" runat="server" />
                </asp:Panel>
            </div>
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <div style="height: 25px; width: 98%; margin-left: 12px; margin-top: 12px; background-color: #3A66AF;
                        color: white; font-size: 16px; font-weight: bold; text-align: center">
                        &nbsp;&nbsp; Update POD Received Details
                    </div>
                    <asp:HiddenField runat="server" ID="hdfld" />
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblLRLabel" Text="Enter LR No" runat="server" Style="font-size: 16px;
                                    font-weight: bold" />
                                &nbsp;
                                <asp:TextBox ID="txtLRNo" runat="server" Width="300px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;
                                <asp:Button TabIndex="2" ID="btnOk" runat="server" Text="Search" CssClass="button"
                                    Style="width: 70px" OnClick="btnOk_Click" />
                                &nbsp;
                                <asp:Button TabIndex="3" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                    Style="width: 70px" OnClick="btnCancel_Click" />
                                &nbsp;
                                <asp:Label ID="lblError" Text="LR No Not Found" runat="server" Visible="false" Style="font-size: 16px;
                                    font-weight: bold; color: Red" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the LR.'
                                    ControlToValidate="txtLRNo" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"
                                    Enabled="true"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%; vertical-align: top">
                                <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;"
                                    cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td style="width: 120px">
                                            <asp:Label ID="Label32" runat="server" Text="POD Required"></asp:Label>
                                        </td>
                                        <td style="width: 60px">
                                            <asp:RadioButton runat="server" ID="rdbYs" Text="Yes" Checked="true" GroupName="PODReq"
                                                TabIndex="4" OnCheckedChanged="rdbYs_CheckedChanged" AutoPostBack="True" />
                                        </td>
                                        <td>
                                            <asp:RadioButton runat="server" ID="rdbNo" Text="No" GroupName="PODReq" TabIndex="5"
                                                OnCheckedChanged="rdbNo_CheckedChanged" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trReason" visible="false">
                                        <td>
                                            <asp:Label ID="Label33" runat="server" Text="Reason" CssClass="NormalTextBold"></asp:Label>
                                            &nbsp;<span style="color: red">*</span>
                                        </td>
                                        <td colspan="2">
                                            <asp:ComboBox ID="cmbReason" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                                MaxLength="200" TabIndex="6" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                                DropDownStyle="DropDownList" RenderMode="Block" AutoPostBack="True" OnSelectedIndexChanged="cmbReason_SelectedIndexChanged">
                                            </asp:ComboBox>
                                            <asp:CustomValidator ID="custValidReason" runat="server" ControlToValidate="cmbReason"
                                                ErrorMessage="Please select Reason from the list" ClientValidationFunction="ValidatorCombobox"
                                                Display="Dynamic" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"
                                                Enabled="false"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="tridemnity" visible="false">
                                        <td>
                                            <asp:Label ID="Label34" runat="server" Text="Idemnity Amt" CssClass="NormalTextBold"></asp:Label>
                                            &nbsp;<span style="color: red">*</span>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox TabIndex="7" ID="txtIdenmityAmt" Width="80" runat="server" MaxLength="10"
                                                Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                            &nbsp;<asp:RequiredFieldValidator ID="rfvIdenmityAmt" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter the Idenmity Amt.'
                                                ControlToValidate="txtIdenmityAmt" SetFocusOnError="True" ValidationGroup="save"
                                                Display="Dynamic" Enabled="false"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilterIdenmityAmt" runat="server" FilterType="Numbers"
                                                TargetControlID="txtIdenmityAmt" Enabled="false">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" Text="POD Date" ID="lblPODDateLabel"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtDate" Enabled="false" runat="server" Width="80px" TabIndex="2"
                                                CssClass="NormalTextBold"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="txtDate" TodaysDateFormat="dd/MMM/yyyy"
                                                PopupButtonID="dtpBtn" FirstDayOfWeek="Sunday">
                                            </asp:CalendarExtender>
                                            <asp:ImageButton ID="dtpBtn" Enabled="false" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="lblFile" Text="File" CssClass="NormalTextBold"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:FileUpload runat="server" ID="flUpload" Width="200px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="70px"
                                                ValidationGroup="save" OnClick="btnSave_Click" />
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;>
                                    <tr runat="server" id="trphcopyyesno">
                                        <td>
                                            <asp:Label runat="server" Text="Physical POD Copy Rcvd"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:RadioButton runat="server" ID="rdbPhCopyNo"  Text="No" AutoPostBack="true" Checked="true" 
                                                GroupName="phcopy" oncheckedchanged="rdbPhCopyNo_CheckedChanged" />
                                            &nbsp;&nbsp;
                                            <asp:RadioButton runat="server" ID="rdbPhCopyYes" AutoPostBack="true" Text="Yes"  
                                                GroupName="phcopy" oncheckedchanged="rdbPhCopyYes_CheckedChanged"/>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trphcopy1">
                                         <td>
                                            <asp:Label ID="Label35" runat="server" Text="Rcvd Date"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtphcopydate" Enabled="false" runat="server" Width="80px" TabIndex="2"
                                                CssClass="NormalTextBold"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvphcopydate" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please select the date'
                                                ControlToValidate="txtDate" SetFocusOnError="True" ValidationGroup="saveph" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="txtphcopydate" TodaysDateFormat="dd/MMM/yyyy"
                                                PopupButtonID="dtpBtn1" FirstDayOfWeek="Sunday">
                                            </asp:CalendarExtender>
                                            <asp:ImageButton ID="dtpBtn1" runat="server" Height="20px" ImageAlign="AbsMiddle"
                                                ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="3" Width="23px" />
                                        </td>
                                    </tr>
                                     <tr runat="server" id="trphcopy2">
                                         <td>
                                            <asp:Label ID="Label36" runat="server" Text="Location"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLocation" runat="server" Width="150px" TabIndex="2"
                                                CssClass="NormalTextBold"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvphcopylocation" runat="server" ErrorMessage='<img alt="error" src="../include/Common/images/warning.gif" height="12" align="middle"> Please enter location'
                                                ControlToValidate="txtLocation" SetFocusOnError="True" ValidationGroup="saveph" Display="Dynamic"></asp:RequiredFieldValidator>
                                            
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Button runat="server" ID="btnUpdatePhCopy" Text="Update" CssClass="button" Width="70px"
                                                ValidationGroup="saveph" onclick="btnUpdatePhCopy_Click"   />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;"
                                    cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" Text="Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label13" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td colspan="4">
                                            <asp:Label ID="lblDate" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:Label ID="Label3" Text="Consigner" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td style="width: 10px">
                                            <asp:Label ID="Label4" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblConsigner" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td style="width: 150px">
                                            <asp:Label ID="Label012" Text="Consignee" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td style="width: 10px">
                                            <asp:Label ID="Label14" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblConsignee" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" Text="From" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFrom" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label16" Text="To" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label18" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTo" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label7" Text="Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label8" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblWeight" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label9" Text="Chargeable Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label10" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblChargeableWeight" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label15" Text="Vehicle No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label17" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVehicleNo" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label19" Text="Total Packages" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label20" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPackages" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label11" Text="Invoice No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label12" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblInvoiceNo" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label21" Text="Invoice Value" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label22" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblInvoiceValue" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label23" Text="Type" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label24" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblType" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label26" Text="Remarks" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label27" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRemarks" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label28" Text="Driver's Name & No." runat="server" Font-Bold="True"
                                                Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label31" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDriverDetails" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" Text="POD Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label25" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPODDate" Text="" runat="server" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label29" Text="POD File" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label30" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="hplPODFile" runat="server" Text="View File" OnClick="hplPODFile_Click"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:ListView ID="lstViewPayments" runat="server">
                                                <LayoutTemplate>
                                                    <table width="100%" border="1" cellspacing="0" cellpadding="2" style="border: 1px solid black;
                                                        border-collapse: collapse; font-size: 14px; font-family: Segoe UI; margin-top: 10px;
                                                        margin-bottom: 10px; margin-left: 0px">
                                                        <tr style="background-color: #3A66AF; color: white">
                                                            <td style="width: 25px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                                #
                                                            </td>
                                                            <td style="width: 150px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                                Date Time
                                                            </td>
                                                            <td style="width: 150px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                                Status
                                                            </td>
                                                            <td style="font-weight: bold; text-align: center; border-collapse: collapse">
                                                                Remarks
                                                            </td>
                                                        </tr>
                                                        <tr id="itemPlaceholder" runat="server" />
                                                    </table>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="text-align: center; border-collapse: collapse">
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </td>
                                                        <td style="text-align: center; border-collapse: collapse">
                                                            <%# Convert.ToDateTime(Eval("TranDateTime")).ToString("dd/MM/yyyy HH:mm")%>
                                                        </td>
                                                        <td style="border-collapse: collapse">
                                                            <%# Eval("TransitStatus")%>
                                                        </td>
                                                        <td style="border-collapse: collapse">
                                                            <%# Eval("TransitRemarks")%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <%--<p style="font-size: 14px; font-family: Segoe UI; text-align: center">
                            X-------------------- No Records Found --------------------X</p>--%>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="rdbYs" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdbNo" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="cmbReason" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
