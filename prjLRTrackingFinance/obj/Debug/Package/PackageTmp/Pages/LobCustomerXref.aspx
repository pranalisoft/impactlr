﻿<%@ Page Title="Lob Customer XRef" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="LobCustomerXref.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LobCustomerXref" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Lob Wise Customer(s)
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Lob Wise Customer(s)
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table class="control_set" style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table style="width: 99.5%">                                
                                <tr class="control_row">
                                    <td style="width:100px">
                                        <asp:Label ID="Label12" runat="server" Text="Lob" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbLob" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="1" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="cmbLobVal" runat="server" ControlToValidate="cmbLob" ErrorMessage="Please select From Lob from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="cmbLobValCall" runat="server" Enabled="True"
                                            TargetControlID="cmbLobVal">
                                        </asp:ValidatorCalloutExtender>
                                    </td>                                    
                                </tr>
                                <tr class="control_row">
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Customer" CssClass="NormalTextBold"></asp:Label>
                                        &nbsp;<span style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:ComboBox ID="cmbCustomer" runat="server" AutoCompleteMode="SuggestAppend" Width="250px"
                                            MaxLength="200" TabIndex="2" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                            DropDownStyle="DropDownList" RenderMode="Block">
                                        </asp:ComboBox>
                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbCustomer" ErrorMessage="Please select Customer from the list"
                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                        </asp:ValidatorCalloutExtender>
                                    </td>
                                </tr>  
                                <tr class="control_row">
                                    <td colspan="6">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="3" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="4" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="6" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="7" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="8" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="70%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,CustomerId,Customer,LobId,Lob"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Lob" HeaderText="Lob" SortExpression="Lob"></asp:BoundField>
                                    <asp:ButtonField ButtonType="Link" HeaderText="Delete" Text="Delete" CommandName="DelRecord"
                                        ItemStyle-ForeColor="#444444" ItemStyle-Width="100px" />
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

