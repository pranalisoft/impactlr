﻿<%@ Page Title="Supplier Payment" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="SupplierPayment.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SupplierPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div class="section">
                <table width="100%">
                    <tr class="centerPageHeader">
                        <td class="centerPageHeader">
                            Supplier Payment
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: White; height: 2px;">
                        </td>
                    </tr>
                </table>
                <div class="msg_region">
                    <iControl:MsgPanel ID="MsgPanel" runat="server" />
                    <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
                </div>
                <div class="body">
                    <asp:MultiView ID="mltViewMaster" ActiveViewIndex="0" runat="server">
                        <asp:View ID="view1" runat="server">
                            <div class="buttons_top">
                                <asp:Button ID="btnAdd" Text="Add" runat="server" CssClass="button" CommandName="Add"
                                    OnCommand="EntryForm_Command" TabIndex="1" />
                                <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="button" CommandName="Delete"
                                    OnCommand="EntryForm_Command" TabIndex="2" />
                            </div>
                            <div class="grid_top_region">
                                <div class="grid_top_region_lft">
                                    <asp:CheckBox ID="chkDateFilter" runat="server" AutoPostBack="true" Text="Date Filter"
                                        TabIndex="3" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtFromDate" Width="100px" runat="server" TabIndex="3" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_From" TargetControlID="txtFromDate" PopupButtonID="imgBtnCalcPopupFrom"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupFrom" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="4" />
                                    &nbsp;&nbsp;
                                    <asp:TextBox ID="txtToDate" Width="100px" runat="server" TabIndex="5" CssClass="NormalTextBold" />
                                    <asp:CalendarExtender ID="Calendar_To" TargetControlID="txtToDate" PopupButtonID="imgBtnCalcPopupTo"
                                        runat="server" Format="dd/MM/yyyy" Enabled="True" DaysModeTitleFormat="MMM yyyy">
                                    </asp:CalendarExtender>
                                    <asp:ImageButton ID="imgBtnCalcPopupTo" runat="server" AlternateText="Popup Button"
                                        ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                        Width="22px" TabIndex="6" />
                                    &nbsp;&nbsp;
                                    <asp:Label runat="server" Text="Search"></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:TextBox runat="server" ID="txtSearch" TabIndex="7" Width="200px"></asp:TextBox>
                                    <asp:Button ID="btnFilter" Text="Show" runat="server" CssClass="button" CommandName="Filter"
                                        OnCommand="Filter_Command" TabIndex="8" ValidationGroup="save" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnClearFilter" Text="Clear Filter" runat="server" CssClass="button"
                                        CommandName="ClearFilter" OnCommand="Filter_Command" TabIndex="9" />
                                </div>
                                <div class="grid_top_region_rght">
                                    Records :
                                    <asp:Label ID="lblRecCount" Text="0" runat="server" />
                                    <%--<table border="0" class="cnt_region">
                                    <tr>
                                        <td>
                                            Records :
                                            <asp:Label ID="lblRecCount" Text="1000" runat="server" />
                                        </td>
                                    </tr>
                                </table>--%>
                                </div>
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdViewIndex" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    AllowPaging="True" DataKeyNames="RefNo,TranDate,SupplierID,SupplierName,TotalAmount,PaymentMode,UserName,BranchId,ChqNo,ChqDate,PayMode,BankName"
                                    OnRowCommand="grdViewIndex_RowCommand" OnRowDataBound="grdViewIndex_RowDataBound"
                                    EmptyDataText="No records found." Width="100%" OnPageIndexChanging="grdViewIndex_PageIndexChanging"
                                    PageSize="15">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                    AutoPostBack="true" TabIndex="5" /></HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" TabIndex="6" /></ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="28px" />
                                            <ItemStyle HorizontalAlign="Center" Width="28px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RefNo" HeaderText="Srl." SortExpression="RefNo" ReadOnly="True"
                                            ItemStyle-Width="80px" />
                                        <%--  <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="RefNo"
                                            HeaderText="No." SortExpression="RefNo" ItemStyle-Width="100px" />--%>
                                        <asp:BoundField DataField="TranDate" HeaderText="Date" SortExpression="TranDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="PayMode" HeaderText="Payment Mode" SortExpression="PayMode"
                                            ReadOnly="True" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="BankName" HeaderText="Bank Name" SortExpression="BankName"
                                            ReadOnly="True" />
                                        <asp:BoundField DataField="Remarks" HeaderText="Type" SortExpression="Remarks" ReadOnly="True" />
                                        <asp:BoundField DataField="ChqNo" HeaderText="Chq. No." SortExpression="ChqNo" ReadOnly="True"
                                            ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="ChqDate" HeaderText="Tran. Date" SortExpression="ChqDate"
                                            DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="80px" />
                                        <asp:BoundField DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                            ReadOnly="True" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                        <asp:BoundField DataField="UserName" HeaderText="Entered By" SortExpression="UserName"
                                            ReadOnly="True" />
                                        <asp:ButtonField CommandName="PayDetails" HeaderText="" Text="Details" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:ButtonField CommandName="Report" HeaderText="" Text="Voucher" ItemStyle-Width="90px"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                    <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                    <PagerSettings Mode="Numeric" />
                                </asp:GridView>
                            </div>
                            <asp:Panel ID="PnlItemDtls" runat="server" Visible="false">
                                <div class="grid_top_region">
                                    <div class="grid_top_region_lft">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblItemDtls" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                        Font-Size="13px" Font-Underline="True"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="grid_top_region_rght">
                                        Records :
                                        <asp:Label ID="lblItemCount" Text="0" runat="server" />
                                    </div>
                                </div>
                                <div class="grid_region">
                                    <asp:GridView ID="grdViewItemDetls" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                        DataKeyNames="LRNo,LRDate,InvoiceNo,InvoiceDate,TotalAmount,TDSAmount,TDSPer"
                                        EmptyDataText="No records found." Width="100%" PageSize="1000" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:BoundField DataField="LRNo" HeaderText="LR No." SortExpression="LRNo" ReadOnly="True"
                                                ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." SortExpression="InvoiceNo"
                                                ReadOnly="True" ItemStyle-Width="80px" />
                                            <asp:BoundField DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate"
                                                DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                            <asp:BoundField DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TDSPer" HeaderText="TDS %" SortExpression="TDSPer" ItemStyle-Width="80px"
                                                ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="TDSAmount" HeaderText="TDS Amount" SortExpression="TDSAmount"
                                                ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                        <HeaderStyle CssClass="header" />
                                        <RowStyle CssClass="row" />
                                        <AlternatingRowStyle CssClass="alter_row" />
                                    </asp:GridView>
                                </div>
                            </asp:Panel>
                        </asp:View>
                        <asp:View ID="viewIndex" runat="server">
                            <div class="entry_form">
                                <asp:HiddenField ID="HFCode" runat="server" />
                                <table class="control_set" style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 60%">
                                            <table class="control_set" style="width: 100%">
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" Text="Date" runat="server" AssociatedControlID="txtDate" />
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="1" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                                            Width="22px" TabIndex="2" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="reqFVfrmDate" runat="server" ErrorMessage="Please select the Payment date"
                                                            ValidationGroup="save" ControlToValidate="txtDate" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVfrmDateCall" runat="server" Enabled="True"
                                                            TargetControlID="reqFVfrmDate">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label4" Text="Billing Company" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top" colspan="5">
                                                        <asp:ComboBox ID="cmbBillingCompany" runat="server" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" AppendDataBoundItems="true"
                                                            Width="300px" MaxLength="100" OnSelectedIndexChanged="cmbBillingCompany_SelectedIndexChanged"
                                                            TabIndex="2" AutoPostBack="true" CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="BillingCompany" runat="server" ControlToValidate="cmbBillingCompany"
                                                            ErrorMessage="Please select Billing Company from the list" ClientValidationFunction="ValidatorCombobox"
                                                            Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="Validatorcalloutextender1" runat="server" Enabled="True"
                                                            TargetControlID="BillingCompany">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Label1" Text="Supplier" runat="server" AssociatedControlID="cmbSupl" />
                                                    </td>
                                                    <td valign="top" colspan="3">
                                                        <asp:ComboBox ID="cmbSupl" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDownList"
                                                            ItemInsertLocation="Append" AppendDataBoundItems="true" Width="300px" MaxLength="100"
                                                            OnSelectedIndexChanged="cmbSupl_SelectedIndexChanged" TabIndex="2" AutoPostBack="true"
                                                            CssClass="WindowsStyle" RenderMode="Block">
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="Supl" runat="server" ControlToValidate="cmbSupl" ErrorMessage="Please select Supplier from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="SuplCall" runat="server" Enabled="True" TargetControlID="Supl">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                    <td valign="top" colspan="2">
                                                        <asp:Label ID="lblPaymentHold" Text="" Font-Bold="true"
                                                            ForeColor="Red" Font-Size="18px" runat="server" AssociatedControlID="cmbSupl"
                                                            Visible="false" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td valign="top" style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td valign="top" style="width: 120px">
                                                        <asp:Label ID="Label3" Text="Payment Mode" runat="server" AssociatedControlID="cmbPaymentMode" />
                                                    </td>
                                                    <td valign="top" colspan="5">
                                                        <asp:ComboBox ID="cmbPaymentMode" runat="server" AutoCompleteMode="SuggestAppend"
                                                            DropDownStyle="DropDownList" ItemInsertLocation="Append" Width="200px" MaxLength="100"
                                                            TabIndex="3" CssClass="WindowsStyle" RenderMode="Block" OnSelectedIndexChanged="cmbPaymentMode_SelectedIndexChanged"
                                                            AutoPostBack="True">
                                                            <asp:ListItem Text="" Value="-1" />
                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                            <asp:ListItem Value="2">Cheque</asp:ListItem>
                                                            <asp:ListItem Value="3">DD</asp:ListItem>
                                                            <asp:ListItem Value="4">Net Banking</asp:ListItem>
                                                            <asp:ListItem Value="5">Debit Card</asp:ListItem>
                                                            <asp:ListItem Value="6">Credit Card</asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:RequiredFieldValidator ID="rfvpaymode" runat="server" ErrorMessage="Please select payment mode from the list"
                                                            ValidationGroup="save" ControlToValidate="cmbPaymentMode" Display="None" SetFocusOnError="true"
                                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvpaymodeCall" runat="server" Enabled="True" TargetControlID="rfvpaymode">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblchqddDate" Text="Cheque Date" runat="server" AssociatedControlID="txtchqdt" />
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:TextBox ID="txtchqdt" Width="100px" runat="server" TabIndex="4" CssClass="NormalTextBold" />
                                                        <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtchqdt" PopupButtonID="imgBtnCalcPopupPODate1"
                                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                                        </asp:CalendarExtender>
                                                        <asp:ImageButton ID="imgBtnCalcPopupPODate1" runat="server" AlternateText="Popup Button"
                                                            ImageUrl="~/Include/Common/images/calendar_img.png" TabIndex="5" Height="22px"
                                                            ImageAlign="AbsMiddle" Width="22px" />&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfvchqDt" runat="server" ErrorMessage="Please select the Cheque/DD date"
                                                            ValidationGroup="save" ControlToValidate="txtchqdt" Display="None" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvchqDtCall" runat="server" Enabled="True" TargetControlID="rfvchqDt">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trBank" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblBank" Text="Bank Name" runat="server" AssociatedControlID="cmbBank" />
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:ComboBox ID="cmbBank" runat="server" AutoCompleteMode="SuggestAppend" DropDownStyle="DropDown"
                                                            AppendDataBoundItems="true" ItemInsertLocation="Append" Width="300px" MaxLength="100"
                                                            TabIndex="6" CssClass="WindowsStyle" RenderMode="Block">
                                                            <asp:ListItem Value=""></asp:ListItem>
                                                        </asp:ComboBox>
                                                        <asp:CustomValidator ID="CustomBank" runat="server" ControlToValidate="cmbBank" ErrorMessage="Please select bank from the list"
                                                            ClientValidationFunction="ValidatorCombobox" Display="None" ValidateEmptyText="true"
                                                            ValidationGroup="save" SetFocusOnError="true" Enabled="false"></asp:CustomValidator>
                                                        <asp:ValidatorCalloutExtender ID="CustomBankCall" runat="server" Enabled="True" TargetControlID="CustomBank">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr id="trChqNo" runat="server" class="control_row">
                                                    <td>
                                                        <span class="req_mark"></span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lblChqDDNo" Text="Cheque No" runat="server" AssociatedControlID="TxtChqNo" />
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:TextBox ID="TxtChqNo" Width="300px" runat="server" TabIndex="7" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="reqFVChqNo" runat="server" ErrorMessage="Please enter the Cheque/DD/Transaction no."
                                                            ValidationGroup="save" ControlToValidate="TxtChqNo" Display="None" SetFocusOnError="true"
                                                            Enabled="false"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="reqFVChqNoCall" runat="server" Enabled="true" TargetControlID="reqFVChqNo">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td style="width: 10px">
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Amount" />
                                                    </td>
                                                    <td style="width: 100px">
                                                        <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="True" MaxLength="100" OnTextChanged="txtAmount_TextChanged"
                                                            TabIndex="8" Width="80px" Style="text-align: right" CssClass="NormalTextBold" />
                                                        <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers,Custom"
                                                            TargetControlID="txtAmount" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="regExAmount" runat="server" ControlToValidate="txtAmount"
                                                            Display="None" ErrorMessage="Please enter valid amount" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,10}|\d{1,6}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="regExAmountCall" runat="server" Enabled="True"
                                                            TargetControlID="regExAmount">
                                                        </asp:ValidatorCalloutExtender>
                                                        <asp:CompareValidator ID="CompAmt" runat="server" ErrorMessage="Amount cannot be greater than Payable Amount"
                                                            ControlToCompare="txtFinalPayable" ControlToValidate="txtAmount" Display="None"
                                                            SetFocusOnError="true" ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                        <asp:ValidatorCalloutExtender ID="CompAmtCall" runat="server" Enabled="True" TargetControlID="CompAmt"
                                                            PopupPosition="Right">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label5" runat="server" AssociatedControlID="txtMDApproveAmount" Text="MD Approve Amount" />
                                                    </td>
                                                    <td style="width: 100px">
                                                        <asp:TextBox ID="txtMDApproveAmount" runat="server" AutoPostBack="false" MaxLength="100"
                                                            Enabled="false" Font-Bold="true" TabIndex="8" Width="80px" Style="text-align: right"
                                                            CssClass="NormalTextBold" />
                                                    </td>
                                                    <td style="width: 200px">
                                                        <asp:Label ID="Label7" runat="server" AssociatedControlID="txtFinalPayable" Text="Payable Amount(10% Tolerance)" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFinalPayable" runat="server" AutoPostBack="false" MaxLength="100"
                                                            Enabled="false" Font-Bold="true" TabIndex="8" Width="80px" Style="text-align: right"
                                                            CssClass="NormalTextBold" />
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                        <span class="req_mark">*</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label6" runat="server" AssociatedControlID="txtTDSPer" Text="TDS %" />
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:TextBox ID="txtTDSPer" runat="server" AutoPostBack="True" MaxLength="100" OnTextChanged="txtAmount_TextChanged"
                                                            TabIndex="8" Width="80px" Style="text-align: right" CssClass="NormalTextBold" />
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                            FilterType="Numbers,Custom" TargetControlID="txtTDSPer" ValidChars=".">
                                                        </asp:FilteredTextBoxExtender>
                                                        <asp:RegularExpressionValidator ID="regExTDSPer" runat="server" ControlToValidate="txtTDSPer"
                                                            Display="None" ErrorMessage="Please enter valid TDS %" SetFocusOnError="True"
                                                            ValidationExpression="^(?:\d{1,2}|\d{1,2}\.\d{1,2})$" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" Enabled="True"
                                                            TargetControlID="regExTDSPer">
                                                        </asp:ValidatorCalloutExtender>
                                                    </td>
                                                </tr>
                                                <tr class="control_row">
                                                    <td>
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label9" runat="server" AssociatedControlID="txtTotalLR" Text="Total Adj. Amount" />
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:TextBox ID="txtTotalLR" runat="server" Enabled="false" MaxLength="10" TabIndex="8"
                                                            Width="100px" Style="text-align: right" CssClass="NormalTextBold" />
                                                    </td>
                                                    <td style="width: 120px">
                                                        <asp:Label ID="Label10" runat="server" AssociatedControlID="txtTotalTDS" Text="Total TDS" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalTDS" runat="server" Enabled="false" MaxLength="10" TabIndex="8"
                                                            Width="100px" Style="text-align: right" CssClass="NormalTextBold" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%" valign="top">
                                            <table id="tblSummary" runat="server" width="100%">
                                                <tr>
                                                    <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                        <asp:Label ID="Label8" runat="server" Text="Outstanding Summary" ForeColor="#3A66AF"
                                                            Font-Bold="true" Font-Size="13px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GrdPaymentStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                            DataKeyNames="" EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                            <Columns>
                                                                <asp:BoundField DataField="BillCnt" HeaderText="Total Bills" ItemStyle-Width="100px"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="BillAmt" HeaderText="Bill Amount" ItemStyle-Width="200px"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="PaidAmt" HeaderText="Paid Amount" ItemStyle-Width="100px"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundField DataField="Outstanding" HeaderText="OutStanding" ItemStyle-Width="100px"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                            </Columns>
                                                            <HeaderStyle CssClass="header" />
                                                            <RowStyle CssClass="row" />
                                                            <AlternatingRowStyle CssClass="alter_row" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: White; height: 20px; border-bottom: 1px solid black">
                                                        <asp:Label ID="Label30" runat="server" Text="Plywood Summary" ForeColor="#3A66AF"
                                                            Font-Bold="true" Font-Size="13px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="grdItemStatus" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                                            DataKeyNames="Srl,Name,IssueQty,IssueAmount,RcptQty,RcptAmount,LostQty,LostAmount,BalQty,BalAmount"
                                                            EmptyDataText="No records found." Width="100%" TabIndex="100">
                                                            <Columns>
                                                                <asp:BoundField DataField="Name" HeaderText="Item" HeaderStyle-HorizontalAlign="Left" />
                                                                <asp:BoundField DataField="IssueQty" HeaderText="Issue" SortExpression="IssueQty"
                                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                                <asp:BoundField DataField="RcptQty" HeaderText="Rcpt" SortExpression="RcptQty" HeaderStyle-HorizontalAlign="Right"
                                                                    ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                                <asp:BoundField DataField="LostQty" HeaderText="Damaged" SortExpression="LostQty"
                                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                                <asp:BoundField DataField="BalQty" HeaderText="Bal.Qty" SortExpression="BalQty" HeaderStyle-HorizontalAlign="Right"
                                                                    ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                                <asp:BoundField DataField="BalAmount" HeaderText="Bal.Amount" SortExpression="BalQty"
                                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" />
                                                            </Columns>
                                                            <HeaderStyle CssClass="header" />
                                                            <RowStyle CssClass="row" />
                                                            <AlternatingRowStyle CssClass="alter_row" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                            <br />
                            <div class="grid_top_region">
                            </div>
                            <div class="grid_region">
                                <asp:GridView ID="grdPaymentSupl" runat="server" CssClass="grid" AutoGenerateColumns="False"
                                    DataKeyNames="Srl,InvoiceNo,InvoiceDate,InvoiceAmount,ReceivedAmount,BalAmt,LRId,LRNo,TotalCharges,PaidAmount,BalanceLRAmount,TDSPer,TDSPaidCount"
                                    EmptyDataText="No records found." Width="100%" TabIndex="100">
                                    <Columns>
                                        <asp:BoundField DataField="LRNo" HeaderText="LR No." ItemStyle-Width="120px" />
                                        <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." ItemStyle-Width="200px" />
                                        <asp:BoundField DataField="InvoiceDate" HeaderText="Date" ItemStyle-Width="100px"
                                            DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="TotalCharges" HeaderText="Amount" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="PaidAmount" HeaderText="Paid" ItemStyle-Width="100px"
                                            ItemStyle-HorizontalAlign="Right" />
                                        <%--<asp:BoundField DataField="BalAmt" HeaderText="Balance" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" />--%>
                                        <asp:TemplateField HeaderText="Balance">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtBalAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="false" Enabled="false" Text='<%#Eval("BalanceLRAmount") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adjusted">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAdjAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    MaxLength="10" Width="98%" AutoPostBack="true" OnTextChanged="txtAdjAmt_TextChanged"
                                                    CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterAmt" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtAdjAmt" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmt" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExAmtCall" runat="server" Enabled="True" TargetControlID="regExAmt"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                                <asp:CompareValidator ID="CompQty" runat="server" ErrorMessage="Adjust amount cannot be greater than balance amount"
                                                    ControlToCompare="txtBalAmt" ControlToValidate="txtAdjAmt" Display="None" SetFocusOnError="true"
                                                    ValidationGroup="save" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                <asp:ValidatorCalloutExtender ID="CompQtyCall" runat="server" Enabled="True" TargetControlID="CompQty"
                                                    PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TDSPer" HeaderText="TDS %" SortExpression="TDSPer" ItemStyle-Width="80px"
                                            ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                        <asp:TemplateField HeaderText="TDS">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTDSAmt" Style="text-align: right" TabIndex="9" runat="server"
                                                    Enabled="false" MaxLength="10" Width="98%" AutoPostBack="false" CssClass="NormalTextBold"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilterAmttds" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtTDSAmt" Enabled="True">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="regExAmtTDS" runat="server" ErrorMessage="Please  enter valid amount"
                                                    ControlToValidate="txtTDSAmt" Display="None" SetFocusOnError="True" ValidationExpression="^(?:\d{1,11}|\d{1,7}\.\d{1,3})$"
                                                    ValidationGroup="save"></asp:RegularExpressionValidator>
                                                <asp:ValidatorCalloutExtender ID="regExAmtTDSCall" runat="server" Enabled="True"
                                                    TargetControlID="regExAmtTDS" PopupPosition="Left">
                                                </asp:ValidatorCalloutExtender>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="header" />
                                    <RowStyle CssClass="row" />
                                    <AlternatingRowStyle CssClass="alter_row" />
                                </asp:GridView>
                            </div>
                            <br />
                            <table class="control_set" style="width: 100%">
                            </table>
                            <div class="buttons_bottom">
                                <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" CommandName="Save"
                                    ValidationGroup="save" OnCommand="EntryForm_Command" TabIndex="10" />
                                <asp:Button ID="btnClearitem" Text="Clear" runat="server" CssClass="button" CommandName="Clear"
                                    OnCommand="EntryForm_Command" TabIndex="11" />
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <asp:Panel ID="pnlNote" CssClass="note_area" runat="server" Visible="false">
                <span class="req_mark">*</span> Indicates Mandatory Field(s).
            </asp:Panel>
            <asp:UpdateProgress ID="upPanelProgress" runat="server" DisplayAfter="100">
                <ProgressTemplate>
                    <div class="DarkenBackground" style="text-align: center">
                        <div style="visibility: visible; width: 300px; position: absolute; top: 250px; left: 0;
                            right: 0; z-index: 20; margin-left: auto; margin-right: auto; margin-top: auto;">
                            <img alt="Loading...." src="../Include/Common/images/ajax-loader.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClearitem" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cmbSupl" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cmbPaymentMode" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderScripts" runat="server">
    <script type="text/javascript" language="javascript">
        function blockkeys() {

            $('#<%= txtDate.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtDate.ClientID %>').val('');
                }
            });

            $('#<%= txtchqdt.ClientID %>').keypress(function (event) {
                if (event.keyCode != 8 && event.keyCode != 9) {
                    event.preventDefault()
                    $('#<%= txtchqdt.ClientID %>').val('');
                }
            });

        }

        $(function () {
            blockkeys();
        });

        function ValidatorCombobox(source, arguments) {
            if (arguments.Value == null || arguments.Value == '' || arguments.Value == 0) {
                arguments.IsValid = false;
            } else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>
