﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true"
    CodeBehind="SearchUtility.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.SearchUtility" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <div style="height: 25px; width: 98%; margin-left: 12px; margin-top: 12px; background-color: #3A66AF;
                        color: white; font-size: 16px; font-weight: bold; text-align: center">
                        &nbsp;&nbsp; Track Your Consignment
                    </div>
                    <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;"
                        cellpadding="0" cellspacing="10">
                        <tr>
                            <td colspan="6" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLRLabel" Text="Enter LR No" runat="server" Style="font-size: 16px;
                                    font-weight: bold" />
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtLRNo" runat="server" Width="300px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;
                                <asp:Button TabIndex="2" ID="btnOk" runat="server" Text="Search" CssClass="button"
                                    ValidationGroup="save" Style="width: 70px" OnClick="btnOk_Click" />
                                &nbsp;
                                <asp:Button TabIndex="3" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                    ValidationGroup="save" Style="width: 70px" OnClick="btnCancel_Click" />
                                &nbsp;
                                <asp:Label ID="lblError" Text="LR No Not Found" runat="server" Style="font-size: 16px;
                                    font-weight: bold; color: Red" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-top: 2px solid black">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" Text="Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label13" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td colspan="4">
                                <asp:Label ID="lblDate" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label3" Text="Consigner" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 10px">
                                <asp:Label ID="Label4" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblConsigner" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 150px">
                                <asp:Label ID="Label012" Text="Consignee" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 10px">
                                <asp:Label ID="Label14" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblConsignee" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" Text="From" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label6" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblFrom" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label16" Text="To" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label18" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblTo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" Text="Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label8" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblWeight" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label9" Text="Chargeable Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label10" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblChargeableWeight" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label15" Text="Vehicle No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label17" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblVehicleNo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label19" Text="Total Packages" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label20" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblPackages" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" Text="Invoice No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label12" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblInvoiceNo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label21" Text="Invoice Value" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label22" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblInvoiceValue" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label23" Text="Type" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label24" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblType" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label26" Text="Remarks" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label27" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblRemarks" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label28" Text="Driver's Name & No." runat="server" Font-Bold="True"
                                    Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label31" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblDriverDtls" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" Text="POD Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label25" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblPODDate" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label29" Text="POD File" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label30" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:LinkButton ID="hplPODFile" runat="server" Text="View File" OnClick="hplPODFile_Click"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:ListView ID="lstViewPayments" runat="server">
                                    <LayoutTemplate>
                                        <table width="100%" border="1" cellspacing="0" cellpadding="2" style="border: 1px solid black;
                                            border-collapse: collapse; font-size: 14px; font-family: Segoe UI; margin-top: 10px;
                                            margin-bottom: 10px; margin-left: 0px">
                                            <tr style="background-color: #3A66AF; color: white">
                                                <td style="width: 25px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                    #
                                                </td>
                                                <td style="width: 250px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                    Date Time
                                                </td>
                                                <td style="width: 400px; font-weight: bold; text-align: center; border-collapse: collapse">
                                                    Status
                                                </td>
                                                <td style="font-weight: bold; text-align: center; border-collapse: collapse">
                                                    Remarks
                                                </td>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center; border-collapse: collapse">
                                                <%# Container.DataItemIndex + 1 %>
                                            </td>
                                            <td style="text-align: center; border-collapse: collapse">
                                                <%# Convert.ToDateTime(Eval("TranDateTime")).ToString("dd/MM/yyyy HH:mm")%>
                                            </td>
                                            <td style="border-collapse: collapse">
                                                <%# Eval("TransitStatus")%>
                                            </td>
                                            <td style="border-collapse: collapse">
                                                <%# Eval("TransitRemarks")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <%--<p style="font-size: 14px; font-family: Segoe UI; text-align: center">
                            X-------------------- No Records Found --------------------X</p>--%>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
