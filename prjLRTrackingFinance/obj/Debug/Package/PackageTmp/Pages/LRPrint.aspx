﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LRPrint.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.LRPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="width: auto; height: auto; font-family: Verdana; font-size: 12px">
    <form id="form1" runat="server">
    <table style="width: 65%;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;
                    border-right: 1px solid black" cellpadding="4" cellspacing="0">
                    <tr>
                        <td rowspan="5">
                            <asp:Image ID="imgLogo" runat="server" Style="height: 100px; width:100px" />
                        </td>
                        <td style="border-right: 1px solid black">
                            <b>
                                <asp:Label ID="lblCompanyName" Text="" runat="server" /></b>
                        </td>
                        <td colspan="2" style="width: 50%; border-bottom: 1px solid black">
                            <asp:Label ID="lbl1" Text="CONSIGNOR COPY/TRANSPORTER COPY/CONSIGNEE COPY" runat="server"
                                Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="lblCompanyAddress1" Text="" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="lblCompanyAddress2" Text="" runat="server" />
                        </td>
                        <td colspan="2">
                            <asp:Label ID="lbl2" Text="At OWNER'S RISK/TRANSIT INSURANCE" runat="server" Font-Bold="true" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="lblCompanyAddress3" Text="" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="lblCompanyPhone" Text="" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <%-- <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border-right: 1px solid black">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border-right: 1px solid black">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="Label9" Text="C. N. Date" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="lblLRDate" Text="" runat="server" />
                        </td>
                        <td style="border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="Label1" Text="From" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-top: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="lblFrom" Text="From" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; border-bottom: 1px solid black; border-right: 1px solid black">
                            <asp:Label ID="Label33" Text="C. N. No. " runat="server" Font-Bold="True" />
                        </td>
                        <td style="width: 300px; border-right: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="lblLRNo" Text="LRNo" runat="server" />
                        </td>
                        <td style="width: 80px; border-bottom: 1px solid black; border-right: 1px solid black">
                            <asp:Label ID="Label12" Text="To" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblTo" Text="To" runat="server" />
                        </td>
                    </tr>
                    <%-- <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border-right: 1px solid black">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black">
                            <asp:Label ID="Label2" Text="Consignor's Name & Address" runat="server" Font-Bold="True" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black">
                            <asp:Label ID="lblConsignorName" Text="ConsignorName" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="4" style="border-right: 1px solid black">
                            <asp:Label ID="lblConsignorAddress" Text="ConsignorAddress" runat="server" />
                        </td>
                        <td style="border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                            <asp:Label ID="Label3" Text="Vehicle No" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-top: 1px solid black; border-bottom: 1px solid black;">
                            <asp:Label ID="lblVehicleNo" Text="VehicleNo" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black; border-bottom: 1px solid black;">
                            <asp:Label ID="Label5" Text="Vehicle Type" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            <asp:Label ID="lblVehicleType" Text="VehicleType" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="border-top: 1px solid black">
                            &nbsp;
                        </td>
                        <td style="border-right: 1px solid black;border-top: 1px solid black">
                            &nbsp;
                        </td>
                        <td style="border-top: 1px solid black">
                            &nbsp;
                        </td>
                        <td style="border-top: 1px solid black">
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black; border-top: 1px solid black">
                            <asp:Label ID="Label4" Text="Consignee's Name & Address" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black">
                            <asp:Label ID="Label7" Text="Freight" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-top: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="Label8" Text="PAID/TOPAY/TO BE BILLED" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: 1px solid black">
                            <asp:Label ID="lblConsigneeName" Text="ConsigneeName" runat="server" />
                        </td>
                        <td style="border-right: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="Label11" Text="GST By" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="Label13" Text="CONSIGNOR/CONSIGNEE" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="4" style="border-right: 1px solid black">
                            <asp:Label ID="lblConsigneeAddress" Text="ConsigneeAddress" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black;border-top: 1px solid black">
                            <asp:Label ID="Label6" Text="Type of Packing" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-top: 1px solid black">
                            <asp:Label ID="lblTypeOfPacking" Text="TypeOfPacking" runat="server" />
                        </td>
                    </tr>
                    <%-- <tr style="border-bottom: 1px solid black">
                        <td colspan="6">
                        </td>
                    </tr>--%>
                    <%--<tr>
                        <td colspan="6">
                            <asp:Label ID="Label19" Text="Previous Policy Details : " runat="server" Style="color: #1199C8;
                                border-bottom: 1px solid #DDD; font-size: 14px; font-weight: bold" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <asp:Label ID="Label20" Text="Tel No." runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="lblConsigneePhone" Text="ConsigneePhone" runat="server" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label22" Text="Driver Name" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            <asp:Label ID="lblDriverName" Text="DriverName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="Label21" Text="Email Id" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-right: 1px solid black; border-bottom: 1px solid black">
                            <asp:Label ID="lblConsigneeEmail" Text="ConsigneeEmail" runat="server" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label23" Text="Driver Mbl No." runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblDriverMobile" Text="DriverMobile" runat="server" />
                        </td>
                    </tr>
                   <%-- <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="border-right: 1px solid black">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label10" Text="No. of Packages" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-right: 1px solid black;border-bottom: 1px solid black">
                            <asp:Label ID="lblNoOfPackages" Text="NoOfPackages" runat="server" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label18" Text="Invoice No." runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblInvoiceNo" Text="InvoiceNo" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label16" Text="Weight as per Invoice" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-right: 1px solid black;border-bottom: 1px solid black">
                            <asp:Label ID="lblChargeableWt" Text="ChargeableWt" runat="server" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label24" Text="Invoice Value" runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblInvoiceValue" Text="InvoiceValue" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="3" style="border-right: 1px solid black">
                            <asp:Label ID="Label26" Text="DESCRIPTION (Said to Contain)" runat="server" Font-Bold="True" />
                        </td>
                        <td rowspan="3" style="border-right: 1px solid black">
                            <asp:Label ID="lblRemarks" Text="Remarks" runat="server" />
                        </td>
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label28" Text="Vehicle Seal No." runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblVehicleSealNo" Text="VehicleSealNo" runat="server" />
                        </td>
                    </tr>
                    <tr>                        
                        <td style="border-bottom: 1px solid black;border-right: 1px solid black">
                            <asp:Label ID="Label32" Text="PAN No." runat="server" Font-Bold="True" />
                        </td>
                        <td style="border-bottom: 1px solid black">
                            <asp:Label ID="lblPAN" Text="PAN" runat="server" />
                        </td>
                    </tr>
                    <tr>                        
                        <td style="border-right: 1px solid black">
                            <asp:Label ID="Label17" Text="GST No." runat="server" Font-Bold="True" />
                        </td>
                        <td>
                            <asp:Label ID="lblGST" Text="GST" runat="server" />
                        </td>
                    </tr>
                    <tr style="border-top: 1px solid black">
                        <td colspan="4" style="text-align: center; border-top: 1px solid black">
                            <asp:Label ID="Label15" Text="SUBJECT TO PUNE JURISDICTION ONLY" runat="server" Style="font-weight: bold" />
                        </td>
                    </tr>
                    <%-- <tr>
                        <td>
                            <asp:Label ID="Label16" Text="Gross Prem." runat="server" Font-Bold="True" />
                        </td>
                        <td>
                            <asp:Label ID="lblGrossPremium" Text="" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="Label17" Text="Net Prem." runat="server" Font-Bold="True" />
                        </td>
                        <td>
                            <asp:Label ID="lblNetPremium" Text="" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label18" Text="Comm. Prem." runat="server" Font-Bold="True" />
                        </td>
                        <td>
                            <asp:Label ID="lblComPremium" Text="" runat="server" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                </table>
                <%--<asp:ListView ID="lstViewPayments" runat="server">
                    <LayoutTemplate>
                        <table width="100%" border="1" cellspacing="0" cellpadding="2" style="border: 1px solid black;
                            border-collapse: collapse; font-size: 9px; font-family: Verdana; margin-top: 10px;
                            margin-bottom: 10px; margin-left: 0px">
                            <tr>
                                <td style="width: 25px; font-weight: bold; text-align: center; border-collapse: collapse">
                                    #
                                </td>
                                <td style="width: 100px; font-weight: bold; text-align: center; border-collapse: collapse">
                                    Date
                                </td>
                                <td style="font-weight: bold; text-align: center; border-collapse: collapse">
                                    Pay Mode
                                </td>
                                <td style="font-weight: bold; text-align: center; border-collapse: collapse">
                                    Payment Details
                                </td>
                                <td style="font-weight: bold; text-align: center; border-collapse: collapse">
                                    Amount
                                </td>
                            </tr>
                            <tr id="itemPlaceholder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center; border-collapse: collapse">
                                <%# Container.DataItemIndex + 1 %>
                            </td>
                            <td style="text-align: center; border-collapse: collapse">
                                <%# Convert.ToDateTime(Eval("TranDate")).ToString("dd/MM/yyyy")%>
                            </td>
                            <td style="border-collapse: collapse">
                                <%# Eval("PayMode")%>
                            </td>
                            <td style="border-collapse: collapse">
                                <%# Eval("PayDetails")%>
                            </td>
                            <td style="border-collapse: collapse">
                                <%# Eval("Amount")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <p style="font-size: 9px; font-family: Verdana; text-align: center">
                            X-------------------- No Records Found --------------------X</p>
                    </EmptyDataTemplate>
                </asp:ListView>--%>
            </td>
        </tr>
        <%--<tr>
        <td>
        &nbsp;
        </td>
        </tr>
         <tr>
        <td>
        &nbsp;
        </td>
        </tr>
         <tr>
        <td>
        &nbsp;
        </td>
        </tr>
         <tr>
        <td>
        &nbsp;
        </td>
        </tr>
        <tr>
        <td>
        </td>
        </tr>
        <tr>
        <td>
        <asp:Label ID="Label3" Text="Recieved By" runat="server" Font-Bold="True" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label10" Text="Recieved On" runat="server" Font-Bold="True" />

        </td>
        </tr>--%>
    </table>
    </form>
</body>
</html>
