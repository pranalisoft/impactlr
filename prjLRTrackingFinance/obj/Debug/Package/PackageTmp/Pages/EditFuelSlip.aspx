﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LRSite.Master" AutoEventWireup="true" CodeBehind="EditFuelSlip.aspx.cs" Inherits="prjLRTrackerFinanceAuto.EditFuelSlip" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <div>
                <asp:Panel ID="pnlMsg" runat="server" CssClass="panelMsg" Visible="false">
                    <asp:Label ID="lblMsg" Text="" runat="server" />
                </asp:Panel>
            </div>
            <asp:MultiView ID="mltViewBUMst" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewEntry" runat="server">
                    <div style="height: 25px; width: 98%; margin-left: 12px; margin-top: 12px; background-color: #3A66AF;
                        color: white; font-size: 16px; font-weight: bold; text-align: center">
                        &nbsp;&nbsp; Edit Fuel Slip
                    </div>
                    <table style="width: 98%; margin-left: 12px; margin-bottom: 12px; border: 1px solid black;"
                        cellpadding="0" cellspacing="10">
                        <tr>
                            <td colspan="6" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="txtHiddenAdvanceper" runat="server" />
                                <asp:HiddenField ID="txtHiddenSupplierAmount" runat="server" />
                                <asp:HiddenField ID="txtHdFuelSlipId" runat="server" />
                                <asp:Label ID="lblLRLabel" Text="Enter LR No" runat="server" Style="font-size: 16px;
                                    font-weight: bold" />
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtLRNo" runat="server" Width="300px" TabIndex="1" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;
                                <asp:Button TabIndex="2" ID="btnOk" runat="server" Text="Search" CssClass="button"
                                    Style="width: 70px" OnClick="btnOk_Click" />
                                &nbsp;
                                <asp:Button TabIndex="3" ID="btnCancel" runat="server" Text="Cancel" CssClass="button"
                                    Style="width: 70px" OnClick="btnCancel_Click" />
                                &nbsp;
                                <asp:Label ID="lblError" Text="" runat="server" Style="font-size: 16px; font-weight: bold;
                                    color: Red" />
                                &nbsp;
                                <asp:LinkButton runat="server" ID="lnkDownload" Text="Download" OnClick="lnkDownload_Click"
                                    Visible="False"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="border-top: 2px solid black">
                                <asp:HiddenField runat="server" ID="hdfld" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" Text="Date" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label13" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td colspan="4">
                                <asp:Label ID="lblDate" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="Label3" Text="Consigner" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 10px">
                                <asp:Label ID="Label4" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblConsigner" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 150px">
                                <asp:Label ID="Label012" Text="Consignee" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td style="width: 10px">
                                <asp:Label ID="Label14" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblConsignee" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" Text="From" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label6" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblFrom" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label16" Text="To" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label18" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblTo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" Text="Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label8" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblWeight" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label9" Text="Chargeable Weight" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label10" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblChargeableWeight" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label15" Text="Vehicle No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label17" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblVehicleNo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label19" Text="Total Packages" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label20" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblPackages" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" Text="Invoice No" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label12" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblInvoiceNo" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label21" Text="Invoice Value" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label22" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblInvoiceValue" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label23" Text="Type" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label24" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblType" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label26" Text="Remarks" runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label27" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblRemarks" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label28" Text="Driver's Name & No." runat="server" Font-Bold="True"
                                    Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="Label31" Text=" : " runat="server" Font-Bold="True" Style="font-size: 16px;" />
                            </td>
                            <td>
                                <asp:Label ID="lblDriverDetails" Text="" runat="server" Style="font-size: 16px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table style="border: 1px solid black; width: 90%">
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="TD Advance" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:TextBox TabIndex="4" ID="txtFuelAdvanceTDA" Enabled="false" Width="90" runat="server"
                                    MaxLength="10" Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;<ajaxToolkit:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender2"
                                    TargetControlID="txtFuelAdvanceTDA" FilterType="Numbers,Custom" ValidChars=".">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label52" runat="server" Text="Petrol Pump" CssClass="NormalTextBold"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:ComboBox ID="cmbPetrolPump" runat="server" AutoCompleteMode="SuggestAppend"
                                    Width="250px" MaxLength="200" TabIndex="4" CssClass="WindowsStyle" AppendDataBoundItems="true"
                                    DropDownStyle="DropDownList" RenderMode="Block">
                                </asp:ComboBox>
                                <asp:CustomValidator ID="PetrolPump" runat="server" ControlToValidate="cmbPetrolPump"
                                    ErrorMessage="Please select petrol pump from the list" ClientValidationFunction="ValidatorCombobox"
                                    Display="None" ValidateEmptyText="true" ValidationGroup="save" SetFocusOnError="true"></asp:CustomValidator>
                                <asp:ValidatorCalloutExtender ID="PetrolPumpCall" runat="server" Enabled="True" TargetControlID="PetrolPump">
                                </asp:ValidatorCalloutExtender>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label29" runat="server" Text="Fuel Advance" CssClass="NormalTextBold"></asp:Label>
                                <span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:TextBox TabIndex="4" ID="txtTotFuelAdvance" Width="90" runat="server" MaxLength="10"
                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;<ajaxToolkit:FilteredTextBoxExtender runat="server" ID="FilteredTextBoxExtender1"
                                    TargetControlID="txtTotFuelAdvance" FilterType="Numbers,Custom" ValidChars=".">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label34" runat="server" Text="Cash" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:TextBox TabIndex="5" ID="txtCashAdvanceFuel" Width="90" runat="server" MaxLength="10"
                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;<ajaxToolkit:FilteredTextBoxExtender runat="server" ID="fltcashadv" TargetControlID="txtCashAdvanceFuel"
                                    FilterType="Numbers,Custom" ValidChars=".">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr class="control_row">
                            <td>
                                <asp:Label ID="Label44" runat="server" Text="Fuel" CssClass="NormalTextBold"></asp:Label>
                                &nbsp;<span style="color: red">*</span>
                            </td>
                            <td>
                                <asp:TextBox TabIndex="6" ID="txtFuelAdvanceFuel" Width="90" runat="server" MaxLength="10"
                                    Style="text-align: right" CssClass="NormalTextBold"></asp:TextBox>
                                &nbsp;<ajaxToolkit:FilteredTextBoxExtender runat="server" ID="fltfueladv" TargetControlID="txtFuelAdvanceFuel"
                                    FilterType="Numbers,Custom" ValidChars=".">
                                </ajaxToolkit:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="btnSave" Text="Save" TabIndex="7" CssClass="button"
                                    Width="70px" ValidationGroup="save" OnClick="btnSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>