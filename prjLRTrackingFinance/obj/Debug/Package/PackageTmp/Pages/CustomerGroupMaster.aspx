﻿<%@ Page Title="Customer Group Master" Language="C#" MasterPageFile="~/LRSite.Master"
    AutoEventWireup="true" CodeBehind="CustomerGroupMaster.aspx.cs" Inherits="prjLRTrackerFinanceAuto.Pages.CustomerGroupMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Include/CSS/layout/masterlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Include/CSS/NewMaster.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CenterContentPlaceHolder" runat="server">
    <asp:UpdatePanel ID="uPanel" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTest" runat="server" Text="Button" Style="display: none" Enabled="false" />
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlAlertBox" runat="server" CssClass="modalPopup" Style="display: none">
                <div style="background-color: #3A66AF; color: White; padding: 3px; font-size: 14px;
                    font-weight: bold">
                    System - Warning
                </div>
                <div align="center" style="padding: 5px">
                    <asp:Label ID="lblError" runat="server" Text="Are you sure you want to delete the record(s)?"
                        Font-Bold="True" Font-Size="10pt"></asp:Label>
                </div>
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="button" CommandName="yes"
                        OnCommand="btnConfirm_Command" />
                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="button" CommandName="no"
                        OnCommand="btnConfirm_Command" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="popDelPacklist" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="Button1" PopupControlID="pnlAlertBox" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <div class="msg_region">
                <iControl:MsgPanel ID="MsgPanel" runat="server" />
                <iControl:MsgPopUp ID="MsgPopUp" runat="server" />
            </div>
            <asp:Button ID="btnInfoPnl" runat="server" Text="Button" Style="display: none" />
            <asp:Panel ID="pnlInformation" runat="server" CssClass="modalPopup" Style="display: none">
                <div class="messageBoxTitle">
                    Vehicle Type Master
                </div>
                <div align="center">
                    <br />
                    <asp:Label ID="lblInfoBox1" runat="server" Text="Branch Created Successfully." CssClass="NormalTextBold"></asp:Label>
                </div>
                <br />
                <div align="right" style="padding: 4px;">
                    <asp:Button ID="btnInfoOk" runat="server" Text="OK" CssClass="button" OnClick="btnInfoOk_Click" />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="pnlInfo_PopUp" runat="server" DynamicServicePath="" Enabled="True"
                TargetControlID="btnInfoPnl" PopupControlID="pnlInformation" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server">
            </asp:ModalPopupExtender>--%>
            <table width="100%">
                <tr class="centerPageHeader">
                    <td class="centerPageHeader">
                        Customer Group Master
                    </td>
                </tr>
                <tr>
                    <td style="background-color: White; height: 2px;">
                    </td>
                </tr>
            </table>
            <div style="padding: 0px 0px 0px 10px">
                <table style="width: 99%">
                    <tr>
                        <td style="width: 60%">
                            <table class="control_set" style="width: 99.5%">
                                <tr class="control_row">
                                    <td colspan="2">
                                        <asp:HiddenField ID="txtHiddenId" runat="server" />
                                    </td>
                                </tr>
                                <tr class="control_row">
                                    <td>
                                        <asp:Label ID="lblusername" runat="server" Text="Name" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="50" TabIndex="1" Width="400px"
                                            CssClass="NormalTextBold"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ErrorMessage='<img alt="error" src="../Include/Common/images/warning.gif" height="12" align="middle"> Value Required'
                                                ControlToValidate="txtName" SetFocusOnError="True" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr class="control_row">
                                    <td>
                                        <asp:Label ID="Label2" Text="Exp. Date" runat="server" AssociatedControlID="txtDate" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDate" Width="100px" runat="server" TabIndex="2" CssClass="NormalTextBold"
                                            Enabled="false" />
                                        <asp:CalendarExtender ID="txtDate_CalendarExtender" TargetControlID="txtDate" PopupButtonID="imgBtnCalcPopupPODate"
                                            runat="server" Format="dd/MM/yyyy" Enabled="True">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="imgBtnCalcPopupPODate" runat="server" AlternateText="Popup Button"
                                            ImageUrl="~/Include/Common/images/calendar_img.png" Height="22px" ImageAlign="AbsMiddle"
                                            Width="22px" TabIndex="3" />&nbsp;
                                    </td>
                                </tr>
                                <tr class="control_row">
                                    <td style="width: 120px">
                                        <asp:Label ID="lbl" runat="server" AssociatedControlID="txtAmount" Text="Exp. Amount" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAmount" runat="server" MaxLength="100" TabIndex="4" Width="80px"
                                            Style="text-align: right" CssClass="NormalTextBold" />
                                        <%-- <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                            Display="None" ErrorMessage="Please enter the Amount" SetFocusOnError="true"
                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                        <asp:ValidatorCalloutExtender ID="rfvAmountCall" runat="server" Enabled="True" TargetControlID="rfvAmount">
                                        </asp:ValidatorCalloutExtender>--%>
                                        <asp:FilteredTextBoxExtender ID="FilterAmount" runat="server" Enabled="True" FilterType="Numbers"
                                            TargetControlID="txtAmount">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr class="control_row">
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Latest Remarks" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                            style="color: red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLatestRemarks" runat="server" MaxLength="200" TabIndex="5" Width="500px"
                                            TextMode="MultiLine" Rows="2" CssClass="NormalTextBold"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblactive" runat="server" Text="Active"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" TabIndex="6" runat="server" />
                                    </td>
                                </tr>
                                <tr style="height: 15px;">
                                    <td colspan="2" style="height: 15px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="button"
                                            ValidationGroup="save" TabIndex="7" />
                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Cancel" CssClass="button"
                                            TabIndex="8" />
                                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button"
                                            TabIndex="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Search" CssClass="NormalTextBold"></asp:Label>&nbsp;<span
                                style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" MaxLength="250" TabIndex="10" Width="300px"
                                CssClass="NormalTextBold"></asp:TextBox>
                            <asp:Button TabIndex="11" ID="btnSearch" runat="server" Text="Search" CssClass="button"
                                OnClick="btnSearch_Click" />
                            <asp:Button TabIndex="12" ID="btnClearSearch" runat="server" Text="Clear Filter" CssClass="button"
                                OnClick="btnClearSearch_Click" />
                            <asp:Button TabIndex="13" ID="btnDelete" runat="server" Text="Delete" CssClass="button"
                                OnClick="btnDelete_Click" />
                        </td>
                        <td align="right" style="font-size: 12px; font-weight: bold; height: 15px">
                            Records :
                            <asp:Label ID="lbltotRecords" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="99%">
                    <tr>
                        <td>
                            <asp:GridView ID="grdMst" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="True" CssClass="grid" DataKeyNames="Srl,Name,UsedCnt,IsActive,LatestRemarks,ExpDate,ExpAmount"
                                EmptyDataText="No Records Found." BackColor="#DCE8F5" Font-Size="10px" OnPageIndexChanging="grdMst_PageIndexChanging"
                                OnRowCommand="grdMst_RowCommand" OnRowDataBound="grdMst_RowDataBound">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkSelect" /></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="28px" />
                                    </asp:TemplateField>
                                    <asp:ButtonField ButtonType="Link" CommandName="Modify" DataTextField="Name" HeaderText="Name"
                                        SortExpression="Name"></asp:ButtonField>
                                    <asp:BoundField DataField="ExpDate" HeaderText="Exp. Date" SortExpression="ExpDate"
                                        DataFormatString="{0:dd/MM/yyyy}" ReadOnly="True" ItemStyle-Width="100px" />
                                    <asp:BoundField DataField="ExpAmount" HeaderText="Exp. Amount" SortExpression="ExpAmount"
                                        ItemStyle-Width="120px" ReadOnly="True" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="LatestRemarks" HeaderText="Remarks" />
                                </Columns>
                                <HeaderStyle CssClass="header" />
                                <RowStyle CssClass="row" />
                                <AlternatingRowStyle CssClass="alter_row" />
                                <PagerStyle CssClass="GridPager" HorizontalAlign="Right" />
                                <PagerSettings Mode="Numeric" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
