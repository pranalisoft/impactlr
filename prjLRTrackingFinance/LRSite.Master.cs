﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using System.Data;
using SqlManager;

namespace LR
{
    public partial class LRSite : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            if (Session["ENAME"] != null)
                lblEUserName.Text = Session["ENAME"].ToString();
            if (Session["PlntName"] != null)
            {
                lblPlant.Text = "Line Of Business ( " + Session["PlntName"].ToString() + ")";
            }
            else
            {
                lblPlant.Text = "";
            }
            if (IsPostBack)
            {
                UpdatePanel upPanel = this.CenterContentPlaceHolder.FindControl("uPanel") as UpdatePanel;

                if (upPanel == null)
                {
                    return;
                }

                if (!Page.IsClientScriptBlockRegistered("highlightfields"))
                {
                    ScriptManager.RegisterStartupScript(upPanel, upPanel.GetType(), "highlightfields", "highlightfields();", true);
                }
            }
        }

        protected void lnkbtnLogout_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "logout")
            {
                Session.Remove("ENAME");
                Session.Remove("ROLEID");
                Session.Remove("EID");
                Session.Abandon();
                Cache.Remove("menuxml");
                Response.Redirect("~\\login.aspx", false);
            }
        }

        protected void lblPlant_Click(object sender, EventArgs e)
        {
            try
            {
                CommonDAO cDaoLoin = new CommonDAO();

                DBLink dbconnector = new DBLink();
                dbconnector.GetConnection();
                long id = long.Parse(Session["EID"].ToString());
                UserDAO userDao = new UserDAO();
                DataTable dtBranch = userDao.GetUserBranches(id);
                cmbPlant.DataSource = null;
                cmbPlant.Items.Clear();
                cmbPlant.Items.Add(new ListItem("", "-1"));
                cmbPlant.DataSource = dtBranch;
                cmbPlant.DataTextField = "Field1";
                cmbPlant.DataValueField = "BranchSrl";
                cmbPlant.DataBind();
                cmbPlant.SelectedIndex = 0;
                lblUserSrl.Text = id.ToString();
                if (dtBranch != null)
                {
                    if (dtBranch.Rows.Count == 0)
                    {
                        //lblMessage.Text = "There is no Line Of Business assigned to this userid.";
                        //lblMessage.Visible = true;
                        return;
                    }
                    else
                    {
                        if (dtBranch.Rows.Count == 1)
                        {
                            Session.Clear();
                            Session["BranchID"] = dtBranch.Rows[0]["BranchSrl"].ToString();
                            Session["IsNotify"] = true;
                            Session["PlntName"] = dtBranch.Rows[0]["Field1"].ToString();
                            Session["Duty"] = dtBranch.Rows[0]["Duty"].ToString();
                            Session["BondDtls"] = dtBranch.Rows[0]["BondDtls"].ToString();
                            Session["ReminderDays"] = dtBranch.Rows[0]["Field3"].ToString();
                            Session["PermInd"] = dtBranch.Rows[0]["PermissionInd"].ToString();
                            Session["PlyApplicable"] = dtBranch.Rows[0]["PlyApplicable"].ToString();
                            Session["BillingCompanySrl"] = dtBranch.Rows[0]["BillingCompanySrl"].ToString();
                            Session["UOM"] = dtBranch.Rows[0]["UOM"].ToString();
                            Response.Redirect("Default.aspx?personid=" + id.ToString(), false);
                        }
                        else
                        {
                            popPlant.Show();
                            //cmbPlant.Focus();                               
                        }
                    }
                }
                else
                {
                    //lblMessage.Text = "There is no Line Of Business assigned to this userid.";
                    //lblMessage.Visible = true;
                    return;
                }
            }
            catch (Exception exp)
            {
                //logger = new LogWriter();
                //logger.WriteLogError("Login", exp);
                Response.Redirect("~/pages/ErrorPage.aspx", false);
            }
        }
        protected void btnConfirm_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName.ToLower().Trim())
            {
                case "yes":
                    popPlant.Show();
                    Session.Clear();
                    Session["BranchID"] = cmbPlant.SelectedValue;
                    Session["PlntName"] = cmbPlant.SelectedItem.Text.Trim();
                    Response.Redirect("~/Default.aspx?personid=" + lblUserSrl.Text, false);
                    CommonDAO cmdao = new CommonDAO();

                    DataTable dtbranch = cmdao.GetBranch(long.Parse(Session["BranchID"].ToString()));
                    Session["Duty"] = dtbranch.Rows[0]["Duty"].ToString();
                    Session["BondDtls"] = dtbranch.Rows[0]["BondDtls"].ToString();
                    Session["ReminderDays"] = dtbranch.Rows[0]["Field3"].ToString();
                    Session["PermInd"] = dtbranch.Rows[0]["PermissionInd"].ToString();
                    Session["PlyApplicable"] = dtbranch.Rows[0]["PlyApplicable"].ToString();
                    Session["BillingCompanySrl"] = dtbranch.Rows[0]["BillingCompanySrl"].ToString();
                    Session["UOM"] = dtbranch.Rows[0]["UOM"].ToString();
                    popPlant.Hide();
                    break;
                case "no":
                    popPlant.Hide();
                    break;
                default:
                    break;
            }
        }
    }
}
