﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessObjects.DAO;
using SqlManager;
using System.Data;
using Logger;
namespace prjLRTrackerFinanceAuto
{
    public partial class _Default : System.Web.UI.Page
    {
        LogWriter logger;  
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                long eid = long.Parse(Request.QueryString["personid"].ToString());
                DataTable dt = new CommonDAO().GetEmpNameRole(eid);
                Session["UserId"] = dt.Rows[0]["UserId"].ToString();
                Session["ROLEID"] = dt.Rows[0]["Role_Srl"].ToString();
                Session["EID"] = eid.ToString();
                Session["ENAME"] = dt.Rows[0]["UserName"].ToString();
                Session["USEREMAIL"] = dt.Rows[0]["Email_Id"].ToString();
                Session["IsNotify"] = false;
                if (Session["ROLEID"].ToString()=="6")
                {
                    Response.Redirect("Pages/LREntry.aspx", false);
                }
                else
                {
                    Response.Redirect("Pages/LRTrackingHomeNew.aspx", false);
                }
                
            }
            catch (Exception exp)
            {
                logger = new LogWriter();
                logger.WriteLogError("Default Page - Load", exp);
                Response.Redirect("~/Pages/ErrorPage.aspx", false);
            }
       
        }

       
    }
}
