﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;
using Logger;

namespace BusinessObjects.DAO
{
    public class InvoiceDAO
    {
        DBLink dbconnector;
        LogWriter logger;
        public InvoiceDAO()
        {
            dbconnector = new DBLink();
        }

        public DataTable FillInvoiceGrid(string searchText, long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;


            return dbconnector.USPGetTable("Invoice_Fill", "SP_Invoice_FillInvoicesHeader", spara);
        }

        public DataTable FillFinalInvoiceGrid_Report(string searchText, long branchId, string FromDate, string ToDate, long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            spara[4] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[4].Value = BillingCompanySrl;

            return dbconnector.USPGetTable("Invoice_Fill", "SP_FinalInvoice_FillInvoicesHeader_Report", spara);
        }

        public DataTable FillInvoiceDetails(long Srl,long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            return dbconnector.USPGetTable("Invoice_Details", "SP_Invoice_FillInvoicesDetails", spara);
        }

        public DataTable FillMainInvoiceDetails(string InvRefNo, long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@InvRefNo", SqlDbType.VarChar);
            spara[0].Value = InvRefNo;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            return dbconnector.USPGetTable("Invoice_Details", "SP_MainInvoice_FillInvoicesDetails", spara);
        }

        //FillProformaInvoiceDetails
        public DataTable FillProformaInvoiceDetails(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("Invoice_Details", "SP_Invoice_FillProformaInvoicesDetails", spara);
        }
        public DataTable FillPendingCustomers(long BranchID,long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;


            return dbconnector.USPGetTable("Invoice_Customers", "SP_Invoice_FillPendingCustomers", spara);
        }

        public DataTable FillPendingVehicles(long BranchID, long BillingCompanySrl,long CustomerID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            spara[2] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[2].Value = CustomerID;

            return dbconnector.USPGetTable("Invoice_Customers", "SP_Invoice_FillPendingVehicles", spara);
        }

        public DataTable FillPendingDestinations(long BranchID, long BillingCompanySrl,long CustomerID,string VehicleNo)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            spara[2] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[2].Value = CustomerID;

            spara[3] = new SqlParameter("@VehicleNo", SqlDbType.NVarChar);
            spara[3].Value = VehicleNo;

            return dbconnector.USPGetTable("Invoice_Customers", "SP_Invoice_FillPendingDestinations", spara);
        }

        public string GetInvoiceNoFromID(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@RefNo", SqlDbType.NVarChar);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 50;
            spara[1].Value = "";

            return dbconnector.ExecuteProcedureWithOutParameterStr("SP_Invoice_GetInvoiceNo", spara, 1);
        }

        public DataTable FillPendingLRS(long BranchID, long CustomerSrl, long BillingCompany, string VehicleNo, string Destination)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[1].Value = CustomerSrl;

            spara[2] = new SqlParameter("@BillingCompany", SqlDbType.BigInt);
            spara[2].Value = BillingCompany;

            spara[3] = new SqlParameter("@Vehicle", SqlDbType.NVarChar);
            spara[3].Value = VehicleNo;

            spara[4] = new SqlParameter("@Destination", SqlDbType.NVarChar);
            spara[4].Value = Destination;

            return dbconnector.USPGetTable("Invoice_LRs", "SP_Invoice_FillPendingLRs", spara);
        }

        public DataTable FillPendingLRSNew(long BranchID, long CustomerSrl, long BillingCompany)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = CustomerSrl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompany;

            spara[2] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[2].Value = BranchID;

            return dbconnector.USPGetTable("Invoice_LRs", "SP_Invoice_FillPendingLRs_New", spara);
        }

        public long InsertInvoice(long Srl, long BillCompanySrl, long CustomerSrl, string InvoiceDate, decimal basicAmount, long BranchId, long EnteredBy, string ItemXML, string PaymentTerms, string Particulars, string AmountInWords, string BuyerRefNo, string BuyerRefDate)
        {

            SqlParameter[] spara = new SqlParameter[15];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillCompanySrl;

            spara[2] = new SqlParameter("@CustomerSrl", SqlDbType.BigInt);
            spara[2].Value = CustomerSrl;

            spara[3] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[3].Value = InvoiceDate;

            spara[4] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[4].Value = basicAmount;

            spara[5] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[5].Value = basicAmount;

            spara[6] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[6].Value = BranchId;

            spara[7] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[7].Value = EnteredBy;

            spara[8] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[8].Value = ItemXML;

            spara[9] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[9].Direction = ParameterDirection.Output;
            spara[9].Value = -1;

            spara[10] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[10].Value = PaymentTerms;

            spara[11] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[11].Value = Particulars;

            spara[12] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[12].Value = AmountInWords;

            spara[13] = new SqlParameter("@BuyerRefNo", SqlDbType.NVarChar);
            spara[13].Value = BuyerRefNo;

            spara[14] = new SqlParameter("@BuyerRefDate", SqlDbType.VarChar);
            if (BuyerRefDate == string.Empty)
                spara[14].Value = DBNull.Value;
            else
                spara[14].Value = BuyerRefDate;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Invoice_InsertInvoice", spara, 9);
        }

        public DataTable FillFinalInvoicePendingLRS(long Srl,long BillCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillCompanySrl;

            return dbconnector.USPGetTable("FinalInvoice_LRs", "SP_FinalInvoice_FillPendingLRs", spara);
        }

        public DataTable FillFinalInvoicePendingInvoices(long branchID, long billingCompanySrl ,long customerID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = branchID;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = billingCompanySrl;

            spara[2] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[2].Value = customerID;


            return dbconnector.USPGetTable("FinalInvoice_LRs", "SP_FinalInvoice_FillPendingInvoices", spara);
        }

        public DataTable FillFinalInvoiceGrid(string searchText, long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;


            return dbconnector.USPGetTable("Invoice_Fill", "SP_FinalInvoice_FillInvoicesHeader", spara);
        }

        public DataTable FillFinalInvoicePendingCustomers(long branchID, long billingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = branchID;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = billingCompanySrl;

            return dbconnector.USPGetTable("FinalInvoice_Cust", "SP_FinalInvoice_FillPendingCustomers", spara);
        }

        public long InsertFinalInvoice(long Srl, decimal basicAmount, long BranchId, long EnteredBy, string ItemXML, string PaymentTerms, string Particulars, string AmountInWords, string BuyerRefNo, string BuyerRefDate, string InvoiceDate, long BillingCompanySrl, decimal BiddingCharges)
        {

            SqlParameter[] spara = new SqlParameter[15];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[1].Value = basicAmount;

            spara[2] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[2].Value = basicAmount;

            spara[3] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[3].Value = BranchId;

            spara[4] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[4].Value = EnteredBy;

            spara[5] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[5].Value = ItemXML;

            spara[6] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[6].Direction = ParameterDirection.Output;
            spara[6].Value = -1;

            spara[7] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[7].Value = PaymentTerms;

            spara[8] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[8].Value = Particulars;

            spara[9] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[9].Value = AmountInWords;

            spara[10] = new SqlParameter("@BuyerRefNo", SqlDbType.NVarChar);
            spara[10].Value = BuyerRefNo;

            spara[11] = new SqlParameter("@BuyerRefDate", SqlDbType.VarChar);
            if (BuyerRefDate == string.Empty)
                spara[11].Value = DBNull.Value;
            else
                spara[11].Value = BuyerRefDate;

            spara[12] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[12].Value = InvoiceDate;

            spara[13] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[13].Value = BillingCompanySrl;

            spara[14] = new SqlParameter("@BiddingCharges", SqlDbType.BigInt);
            spara[14].Value = BiddingCharges;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_FinalInvoice_InsertInvoice", spara, 6);
        }

        public long InsertFinalInvoiceNew(long Srl, decimal basicAmount, long BranchId, long EnteredBy, string ItemXML, string PaymentTerms, string Particulars, string AmountInWords, string BuyerRefNo, string BuyerRefDate, string InvoiceDate, long BillingCompanySrl, decimal BiddingCharges, long CustomerSrl)
        {

            SqlParameter[] spara = new SqlParameter[16];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[1].Value = basicAmount;

            spara[2] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[2].Value = basicAmount;

            spara[3] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[3].Value = BranchId;

            spara[4] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[4].Value = EnteredBy;

            spara[5] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[5].Value = ItemXML;

            spara[6] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[6].Direction = ParameterDirection.Output;
            spara[6].Value = -1;

            spara[7] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[7].Value = PaymentTerms;

            spara[8] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[8].Value = Particulars;

            spara[9] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[9].Value = AmountInWords;

            spara[10] = new SqlParameter("@BuyerRefNo", SqlDbType.NVarChar);
            spara[10].Value = BuyerRefNo;

            spara[11] = new SqlParameter("@BuyerRefDate", SqlDbType.VarChar);
            if (BuyerRefDate == string.Empty)
                spara[11].Value = DBNull.Value;
            else
                spara[11].Value = BuyerRefDate;

            spara[12] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[12].Value = InvoiceDate;

            spara[13] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[13].Value = BillingCompanySrl;

            spara[14] = new SqlParameter("@BiddingCharges", SqlDbType.BigInt);
            spara[14].Value = BiddingCharges;

            spara[15] = new SqlParameter("@CustomerSrl", SqlDbType.BigInt);
            spara[15].Value = CustomerSrl;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Invoice_Insert", spara, 6);
        }

        #region Hamali
        public DataTable FillFinalInvoiceGrid_Hamali(string searchText, long branchId, string FromDate, string ToDate, long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            spara[4] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[4].Value = BillingCompanySrl;

            return dbconnector.USPGetTable("Invoice_Fill", "SP_FinalInvoice_FillInvoicesHeader_Hamali", spara);
        }

        public DataTable FillInvoiceGrid_Hamali(string searchText, long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            return dbconnector.USPGetTable("Invoice_Fill_Hamali", "SP_Invoice_FillInvoicesHeader_Hamali", spara);
        }
        public DataTable FillInvoiceDetails_Hamali(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("Invoice_Details_Hamali", "SP_Invoice_FillInvoicesDetails_Hamali", spara);
        }
        public long InsertInvoice_Hamali(long Srl, long BillCompanySrl, string InvoiceDate, decimal InvoiceAmount, long BranchId, long EnteredBy, string PaymentTerms, string Particulars, string AmountInWords, string ItemXML)
        {

            SqlParameter[] spara = new SqlParameter[11];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillCompanySrl;

            spara[2] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[2].Value = InvoiceDate;

            spara[3] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[3].Value = InvoiceAmount;

            spara[4] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[4].Value = BranchId;

            spara[5] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[5].Value = EnteredBy; 
            
            spara[6] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[6].Value = PaymentTerms;

            spara[7] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[7].Value = Particulars;

            spara[8] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[8].Value = AmountInWords;

            spara[9] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[9].Value = ItemXML;

            spara[10] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[10].Direction = ParameterDirection.Output;
            spara[10].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Invoice_InsertInvoice_Hamali", spara, 10);
        }

        public long UpdateBillSubmissionDate(string MasterInvref, string BillSubmissionDate)
        {

            SqlParameter[] spara = new SqlParameter[3];

            spara[0] = new SqlParameter("@MasterInvref", SqlDbType.NVarChar);
            spara[0].Value = MasterInvref;

            spara[1] = new SqlParameter("@BillSubmissionDate", SqlDbType.NVarChar);
            spara[1].Value = BillSubmissionDate; 
            
            spara[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_FinalInvoice_UpdateBillSubmissionDate", spara, 2);
        }

        #endregion
    }
}
