﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
   public class SupplierDAO
    {
       DBLink dbconnector;

       public SupplierDAO()
        {
            dbconnector = new DBLink();
        }

       public long InsertSupplier(long Srl, string Name, string Address1, string Address2, string Address3, string landline, string mobile, string emailID, string vendor, string CST, string VAT, string TIN, string ServiceTaxno, long creditdays, decimal creditLimit, bool isActive, string GLCode, decimal AdvancePer, bool AgreementDone, string AgreementDate, long CreatedBy, decimal TDSPer, string FirmName, string VendorType, string ContactPersonName, string AadharNo, string PAN, long SearchedBy, long ApprovedBy, string BankName, string BankBranch, string AccountNo, string IFSC,string DestinationXML,string VehicleTypeXML,string ValidTillDate,int SupplierType,bool HoldPayment,string HoldRemarks)
        {
            SqlParameter[] sqlParams = new SqlParameter[40];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@Name", SqlDbType.NVarChar);
            sqlParams[1].Value = Name;

            sqlParams[2] = new SqlParameter("@Address1", SqlDbType.NVarChar);
            sqlParams[2].Value = Address1;

            sqlParams[3] = new SqlParameter("@Address2", SqlDbType.NVarChar);
            sqlParams[3].Value = Address2;

            sqlParams[4] = new SqlParameter("@Address3", SqlDbType.NVarChar);
            sqlParams[4].Value = Address3;

            sqlParams[5] = new SqlParameter("@Landline", SqlDbType.NVarChar);
            sqlParams[5].Value = landline;

            sqlParams[6] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
            sqlParams[6].Value = mobile;

            sqlParams[7] = new SqlParameter("@EmailID", SqlDbType.NVarChar);
            sqlParams[7].Value = emailID;

            sqlParams[8] = new SqlParameter("@VendorCode", SqlDbType.NVarChar);
            sqlParams[8].Value = vendor;

            sqlParams[9] = new SqlParameter("@CSTNo", SqlDbType.NVarChar);
            sqlParams[9].Value = CST;

            sqlParams[10] = new SqlParameter("@VATNo", SqlDbType.NVarChar);
            sqlParams[10].Value = VAT;

            sqlParams[11] = new SqlParameter("@TINNo", SqlDbType.NVarChar);
            sqlParams[11].Value = TIN;

            sqlParams[12] = new SqlParameter("@ServiceTaxNo", SqlDbType.NVarChar);
            sqlParams[12].Value = ServiceTaxno;

            sqlParams[13] = new SqlParameter("@CreditDays", SqlDbType.BigInt);
            sqlParams[13].Value = creditdays;

            sqlParams[14] = new SqlParameter("@CreditLimit", SqlDbType.Decimal);
            sqlParams[14].Value = creditLimit;

            sqlParams[15] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[15].Value = isActive;

            sqlParams[16] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[16].Value = -1;
            sqlParams[16].Direction = ParameterDirection.Output;
            sqlParams[16].Size = 9;

            sqlParams[17] = new SqlParameter("@GLCode", SqlDbType.NVarChar);
            sqlParams[17].Value = GLCode;

            sqlParams[18] = new SqlParameter("@AdvancePer", SqlDbType.Decimal);
            sqlParams[18].Value = AdvancePer;

            sqlParams[19] = new SqlParameter("@AgreementDone", SqlDbType.Bit);
            sqlParams[19].Value = AgreementDone;

            sqlParams[20] = new SqlParameter("@AgreementDate", SqlDbType.VarChar);
           if (AgreementDate==string.Empty)
               sqlParams[20].Value = DBNull.Value;
           else
            sqlParams[20].Value = AgreementDate;

           sqlParams[21] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
           sqlParams[21].Value = CreatedBy;

           sqlParams[22] = new SqlParameter("@TDSPer", SqlDbType.Decimal);
           sqlParams[22].Value = TDSPer;

           sqlParams[23] = new SqlParameter("@FirmName", SqlDbType.NVarChar);
           sqlParams[23].Value = FirmName;

           sqlParams[24] = new SqlParameter("@VendorType", SqlDbType.NVarChar);
           sqlParams[24].Value = VendorType;

           sqlParams[25] = new SqlParameter("@ContactPersonName", SqlDbType.NVarChar);
           sqlParams[25].Value = ContactPersonName;

           sqlParams[26] = new SqlParameter("@AadharNo", SqlDbType.NVarChar);
           sqlParams[26].Value = AadharNo;

           sqlParams[27] = new SqlParameter("@PAN", SqlDbType.NVarChar);
           sqlParams[27].Value = PAN;

           sqlParams[28] = new SqlParameter("@SearchedBy", SqlDbType.BigInt);
           sqlParams[28].Value = SearchedBy;

           sqlParams[29] = new SqlParameter("@ApprovedBy", SqlDbType.BigInt);
           sqlParams[29].Value = ApprovedBy;

           sqlParams[30] = new SqlParameter("@BankName", SqlDbType.NVarChar);
           sqlParams[30].Value = BankName;

           sqlParams[31] = new SqlParameter("@BankBranch", SqlDbType.NVarChar);
           sqlParams[31].Value = BankBranch;

           sqlParams[32] = new SqlParameter("@AccountNo", SqlDbType.NVarChar);
           sqlParams[32].Value = AccountNo;

           sqlParams[33] = new SqlParameter("@IFSC", SqlDbType.NVarChar);
           sqlParams[33].Value = IFSC;

           sqlParams[34] = new SqlParameter("@DestinationXML", SqlDbType.Xml);
           sqlParams[34].Value = DestinationXML;

           sqlParams[35] = new SqlParameter("@VehicleTypeXML", SqlDbType.Xml);
           sqlParams[35].Value = VehicleTypeXML;

           sqlParams[36] = new SqlParameter("@ValidTillDate", SqlDbType.VarChar);
           if (ValidTillDate == string.Empty)
               sqlParams[36].Value = DBNull.Value;
           else
               sqlParams[36].Value = ValidTillDate;

           sqlParams[37] = new SqlParameter("@SupplierType", SqlDbType.Int);
           sqlParams[37].Value = SupplierType;

           sqlParams[38] = new SqlParameter("@HoldPayment", SqlDbType.Bit);
           sqlParams[38].Value = HoldPayment;

           sqlParams[39] = new SqlParameter("@HoldRemarks", SqlDbType.NVarChar);
           sqlParams[39].Value = HoldRemarks;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Insert", sqlParams, 16);
        }

        public DataTable GetSuppliers(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetSupplier", "SP_Supplier_Fill", sqlParams);
        }

        public DataTable GetSuppliersByType(int SupplierType)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SupplierType", SqlDbType.Int);
            sqlParams[0].Value = SupplierType;

            return dbconnector.USPGetTable("GetSupplier", "SP_Supplier_FillByType", sqlParams);
        }

        public bool isSupplierAlreadyExist(string Name, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];          

            sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[0].Value = Name;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_isDuplicate", sqlParams, 2);

            if (cnt>0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public long DeleteSupplier(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Delete", sqlParams, 1);
        }

        public DataTable SupplierDestinations(long SupplierSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SupplierSrl", SqlDbType.VarChar);
            sqlParams[0].Value = SupplierSrl;

            return dbconnector.USPGetTable("GetSupplierDestinations", "SP_Supplier_FillDestinations", sqlParams);
        }

        public DataTable SupplierVehicleType(long SupplierSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SupplierSrl", SqlDbType.VarChar);
            sqlParams[0].Value = SupplierSrl;

            return dbconnector.USPGetTable("GetSupplierVehicleType", "SP_Supplier_FillVehicleType", sqlParams);
        }

        public DataTable GetPlySuppliers()
        { 
            return dbconnector.USPGetTable("GetPlySuppliers", "SP_Supplier_FillPlySuppliers");
        }
    }
}
