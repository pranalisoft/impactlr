﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SqlManager;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Web;
namespace BusinessObjects.DAO
{
   public class DatadumpDAO
    {
       DBLink dbConnnector;
       public DatadumpDAO()
       {
           dbConnnector = new DBLink();
       }

       public void SaveExcelWorkSheetsEscalation(DataSet ds, string XLPath)
       {
           string sheetName = string.Empty;

           using (ExcelPackage objExcelPackage = new ExcelPackage())
           {

               for (int i = 0; i < ds.Tables.Count; i++)
               {
                   sheetName = GetSheetName(i);
                   //Create the worksheet    
                   ExcelWorksheet objWorksheet = objExcelPackage.Workbook.Worksheets.Add(sheetName);
                   //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1    
                   objWorksheet.Cells["A1"].LoadFromDataTable(ds.Tables[i], true);
                   //objWorksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 12));
                   objWorksheet.Cells.AutoFitColumns();

                   using (ExcelRange objRange = objWorksheet.Cells["A1:XFD1"])
                   {
                       objRange.Style.Font.Bold = true;
                       objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                       objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                       //objRange.AutoFilter = true;
                       //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                       //objRange.Style.Fill.BackgroundColor.SetColor(.Aqua);
                   }

               }


               

               //Write it back to the client    
               if (File.Exists(XLPath))
                   File.Delete(XLPath);

               //Create excel file on physical disk    
               FileStream objFileStrm = File.Create(XLPath);
               objFileStrm.Close();

               //Write content to excel file    
               File.WriteAllBytes(XLPath, objExcelPackage.GetAsByteArray());

            

           }

       }

       private string GetSheetName(int i)
       {
           string sheetName = "";
           switch (i)
           {
               default:
                   break;
               case 0:
                   sheetName = "tbl_BillingCompany";
                   break;
               case 1:
                   sheetName = "tbl_Branch";
                   break;
               case 2:
                   sheetName = "tbl_Consignor";
                   break;
               case 3:
                   sheetName = "tbl_Customer";
                   break;
               case 4:
                   sheetName = "tbl_Destination";
                   break;
               case 5:
                   sheetName = "tbl_Device";
                   break;
               case 6:
                   sheetName = "tbl_PackingType";
                   break;
               case 7:
                   sheetName = "tbl_Supplier";
                   break;
               case 8:
                   sheetName = "tbl_User";
                   break;
               case 9:
                   sheetName = "tbl_User_Branch";
                   break;
               case 10:
                   sheetName = "tbl_VehicleType";
                   break;
               case 11:
                   sheetName = "tbl_LRDescription";
                   break;
               case 12:
                   sheetName = "tbl_InvoiceHeader";
                   break;
               case 13:
                   sheetName = "tbl_InvoiceDetails";
                   break;
               case 14:
                   sheetName = "tbl_LRHeader";
                   break;
               case 15:
                   sheetName = "tbl_LRDetails";
                   break;
               case 16:
                   sheetName = "tbl_Payment_Header";
                   break;
               case 17:
                   sheetName = "tbl_Payment_Details";
                   break;
               case 18:
                   sheetName = "tbl_Receipt_Header";
                   break;
               case 19:
                   sheetName = "tbl_Receipt_Details";
                   break;
               case 20:
                   sheetName = "tbl_SuplInvoiceHeader";
                   break;
               case 21:
                   sheetName = "tbl_SuplInvoiceDetails";
                   break;
               case 22:
                   sheetName = "tbl_LRChanges";
                   break;      

           }
           return sheetName;
       }

       public DataSet GetDataDump(DateTime FromDate, DateTime ToDate)
       {

           SqlParameter[] sqlParams = new SqlParameter[2];
           sqlParams[0] = new SqlParameter("@FromDate", SqlDbType.SmallDateTime);
           sqlParams[0].Value = FromDate;

           sqlParams[1] = new SqlParameter("@ToDate", SqlDbType.SmallDateTime);
           sqlParams[1].Value = ToDate;

           return dbConnnector.USPGetSet("dsDataDump", "SP_DataDump", sqlParams);
            
       }

      
    }
}
