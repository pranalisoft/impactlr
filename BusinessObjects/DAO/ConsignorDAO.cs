﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class ConsignorDAO
    {
        DBLink dbconnector;

        public ConsignorDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertConsignor(long Srl, string Name, string Address, bool IsActive, string GSTNo, long CreatedBy,string EmailId,string MobileNo,string PhoneNo,string ConsignorType,string CustomerXML,string Pincode)
        {
            SqlParameter[] sqlParams = new SqlParameter[13];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@Name", SqlDbType.NVarChar);
            sqlParams[1].Value = Name;

            sqlParams[2] = new SqlParameter("@Address", SqlDbType.NVarChar);
            sqlParams[2].Value = Address;

            sqlParams[3] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[3].Value = -1;
            sqlParams[3].Direction = ParameterDirection.Output;
            sqlParams[3].Size = 9;

            sqlParams[4] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[4].Value = IsActive;

            sqlParams[5] = new SqlParameter("@GSTNo", SqlDbType.NVarChar);
            sqlParams[5].Value = GSTNo;

            sqlParams[6] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[6].Value = CreatedBy;

            sqlParams[7] = new SqlParameter("@EmailId", SqlDbType.NVarChar);
            sqlParams[7].Value = EmailId;

            sqlParams[8] = new SqlParameter("@MobileNo", SqlDbType.NVarChar);
            sqlParams[8].Value = MobileNo;

            sqlParams[9] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
            sqlParams[9].Value = PhoneNo;

            sqlParams[10] = new SqlParameter("@ConsignorType", SqlDbType.NVarChar);
            sqlParams[10].Value = ConsignorType;

            sqlParams[11] = new SqlParameter("@CustomerXML", SqlDbType.Xml);
            sqlParams[11].Value = CustomerXML;

            sqlParams[12] = new SqlParameter("@Pincode", SqlDbType.NChar);
            sqlParams[12].Value = Pincode;
   


            return dbconnector.ExecuteProcedureWithOutParameter("SP_Consignor_Insert", sqlParams, 3);
        }

        public DataTable GetConsignors(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetConsignor", "SP_Consignor_Fill", sqlParams);
        }

        public DataTable GetConsignorAddress(string Name)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Name", SqlDbType.NVarChar);
            sqlParams[0].Value = Name;

            return dbconnector.USPGetTable("GetConsignorAddress", "SP_Consignor_GetAddress", sqlParams);
        }

        public DataTable GetConsignorDetailsById(long ID)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParams[0].Value = ID;

            return dbconnector.USPGetTable("GetConsignorDetailsById", "SP_Consignor_GetDetailsById", sqlParams);
        }

        public DataTable GetConsigneeDetails(string Name)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ConsigneeName", SqlDbType.NVarChar);
            sqlParams[0].Value = Name;

            return dbconnector.USPGetTable("consignee", "SP_Consignee_GetDetails",sqlParams);
        }

        public DataTable FillCustomersofConsignors(long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ConsignorId", SqlDbType.NVarChar);
            sqlParams[0].Value = Srl;

            return dbconnector.USPGetTable("ConsignorCust", "Consignor_FillCustomers", sqlParams);
        }

        public bool isConsignorAlreadyExist(string Name, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];

            sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[0].Value = Name;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Consignor_isDuplicate", sqlParams, 2);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long DeleteConsignors(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Consignor_Delete", sqlParams, 1);
        }

        public DataSet FillConsignorConsignee(long CustomerId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@CustomerId", SqlDbType.BigInt);
            sqlParams[0].Value = CustomerId;

            return dbconnector.USPGetSet("dsConsignor", "SP_Consignor_FillFromCustomer", sqlParams);
        }
    }
}
