﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class CustomerPTLItemMasterDAO
    {
        DBLink dbconnector;

        public CustomerPTLItemMasterDAO()
        {
            dbconnector = new DBLink(); 
        }

        public long InsertUpdateCustomerPTLItemMaster(long Srl, long CustomerId, string Item, long ZoneTypeId, string Slab, decimal RatePerQtySelling, decimal RatePerKgSelling, long VehicleTypeId, bool IsActive, long CreatedBy, decimal RatePerQtyBuying, decimal RatePerKgBuying, decimal MOQ, string From, string To, int IsFixedRateSelling, int IsFixedRateBuying)
        {
            SqlParameter[] sqlParams = new SqlParameter[18];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@CustomerId", SqlDbType.BigInt);
            sqlParams[1].Value = CustomerId;

            sqlParams[2] = new SqlParameter("@Item", SqlDbType.NVarChar);
            sqlParams[2].Value = Item;

            sqlParams[3] = new SqlParameter("@ZoneTypeId", SqlDbType.BigInt);
            sqlParams[3].Value = ZoneTypeId;

            sqlParams[4] = new SqlParameter("@Slab", SqlDbType.NVarChar);
            sqlParams[4].Value = Slab;

            sqlParams[5] = new SqlParameter("@RatePerQtySelling", SqlDbType.Decimal);
            sqlParams[5].Value = RatePerQtySelling;

            sqlParams[6] = new SqlParameter("@RatePerKgSelling", SqlDbType.Decimal);
            sqlParams[6].Value = RatePerKgSelling;

            sqlParams[7] = new SqlParameter("@VehicleTypeId", SqlDbType.BigInt);
            if (VehicleTypeId>0)            
                sqlParams[7].Value = VehicleTypeId;
            else
                sqlParams[7].Value = DBNull.Value;

            sqlParams[8] = new SqlParameter("@IsActive", SqlDbType.BigInt);
            sqlParams[8].Value = IsActive;

            sqlParams[9] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[9].Value = CreatedBy;

            sqlParams[10] = new SqlParameter("@RatePerQtyBuying", SqlDbType.Decimal);
            sqlParams[10].Value = RatePerQtyBuying;

            sqlParams[11] = new SqlParameter("@RatePerKgBuying", SqlDbType.Decimal);
            sqlParams[11].Value = RatePerKgBuying;

            sqlParams[12] = new SqlParameter("@MOQ", SqlDbType.Decimal);
            sqlParams[12].Value = MOQ;

            sqlParams[13] = new SqlParameter("@From", SqlDbType.NVarChar);
            if (From=="" || To=="")
                sqlParams[13].Value = DBNull.Value;
            else
                sqlParams[13].Value = From;

            sqlParams[14] = new SqlParameter("@To", SqlDbType.NVarChar);
            if (From == "" || To == "")
                sqlParams[14].Value = DBNull.Value;
            else
                sqlParams[14].Value = To;

            sqlParams[15] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[15].Value = -1;
            sqlParams[15].Direction = ParameterDirection.Output;
            sqlParams[15].Size = 9;

            sqlParams[16] = new SqlParameter("@IsFixedRateSelling", SqlDbType.Int);
            sqlParams[16].Value = IsFixedRateSelling;

            sqlParams[17] = new SqlParameter("@IsFixedRateBuying", SqlDbType.Int);
            sqlParams[17].Value = IsFixedRateBuying;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerPTLItemMasterInsertUpdate", sqlParams, 15);
        }

        public DataTable GetCustomerPTLItemMaster(string SearchText,long CustomerId)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@CustomerId", SqlDbType.BigInt);
            sqlParams[1].Value = CustomerId;

            return dbconnector.USPGetTable("GetCustomer", "SP_CustomerPTLItemMasterView", sqlParams);
        }

        public long CustomerPTLItemMasterDelete(long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerPTLItemMasterDelete", sqlParams, 1);
        }

        public DataTable GetZoneType(long CustomerId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];

            sqlParams[0] = new SqlParameter("@CustomerId", SqlDbType.BigInt);
            sqlParams[0].Value = CustomerId;

            return dbconnector.USPGetTable("GetZoneType", "SP_CustomerPTLItemFillZoneType", sqlParams);
        }
        
    }

}
