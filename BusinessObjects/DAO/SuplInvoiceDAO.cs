﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;
using Logger;
namespace BusinessObjects.DAO
{
   public class SuplInvoiceDAO
    {
         DBLink dbconnector;
        LogWriter logger;
        public SuplInvoiceDAO()
        {
            dbconnector = new DBLink();
        }

        public DataTable FillInvoiceGrid(string searchText, long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            return dbconnector.USPGetTable("Invoice_Fill", "SP_SuplInvoice_FillInvoicesHeader", spara);
        }

        public DataTable FillInvoiceGridPTL(string searchText, long branchId, string FromDate, string ToDate,int IsPTL)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            spara[4] = new SqlParameter("@IsPTL", SqlDbType.Int);
            spara[4].Value = IsPTL;

            return dbconnector.USPGetTable("Invoice_Fill", "SP_SuplInvoice_FillInvoicesHeader", spara);
        }

        public DataTable FillInvoiceDetails(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("Invoice_Details", "SP_SuplInvoice_FillInvoicesDetails", spara);
        }

        public DataTable FillPendingCustomers(long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            return dbconnector.USPGetTable("Invoice_Customers", "SP_SuplInvoice_FillPendingSuppliers", spara);
        }

        public DataTable FillPendingLRS(long BranchID, long CustomerSrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = CustomerSrl;

            return dbconnector.USPGetTable("Invoice_LRs", "SP_SuplInvoice_FillPendingLRs", spara);
        }

        public DataTable FillPendingLRSPTL(long BranchID, long CustomerSrl,int IsPTL)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = CustomerSrl;

            spara[2] = new SqlParameter("@IsPTL", SqlDbType.Int);
            spara[2].Value = IsPTL;

            return dbconnector.USPGetTable("Invoice_LRs", "SP_SuplInvoice_FillPendingLRs", spara);
        }

        public long InsertInvoice(long Srl, long BillCompanySrl, long SuplID, string InvoiceDate, decimal basicAmount, long BranchId, long EnteredBy, string ItemXML, string PaymentTerms, string Particulars, string AmountInWords, string InvoiceNo)
        {

            SqlParameter[] spara = new SqlParameter[14];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillCompanySrl;

            spara[2] = new SqlParameter("@SuplID", SqlDbType.BigInt);
            spara[2].Value = SuplID;

            spara[3] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[3].Value = InvoiceDate;

            spara[4] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[4].Value = basicAmount;

            spara[5] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[5].Value = basicAmount;

            spara[6] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[6].Value = BranchId;

            spara[7] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[7].Value = EnteredBy;

            spara[8] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[8].Value = ItemXML;

            spara[9] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[9].Direction = ParameterDirection.Output;
            spara[9].Value = -1;

            spara[10] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[10].Value = PaymentTerms;

            spara[11] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[11].Value = Particulars;

            spara[12] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[12].Value = AmountInWords;

            spara[13] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            spara[13].Value = InvoiceNo;
        

            return dbconnector.ExecuteProcedureWithOutParameter("SP_SuplInvoice_InsertInvoice", spara, 9);
        }

        public long DeleteInvoices(string Srls)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            spara[0].Value = Srls;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_SuplInvoice_Delete", spara, 1);
        }

        public DataTable FillLRPendingForBill(long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            return dbconnector.USPGetTable("LRPendingForBill", "SP_SuplInvoice_PendingForBill", spara);
        }

        public DataTable FillInvoicePenaltyDetails(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("dtPenalty", "SP_SuplInvoice_FillInvoicesPenaltyDetails", spara);
        }

        public long UpdatetInvoice(long Srl, long BillCompanySrl, long SuplID, string InvoiceDate, decimal basicAmount, long BranchId, long EnteredBy, string ItemXML, string PaymentTerms, string Particulars, string AmountInWords, string InvoiceNo, string PenaltyXML,long ApprovedBy)
        {

            SqlParameter[] spara = new SqlParameter[16];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillCompanySrl;

            spara[2] = new SqlParameter("@SuplID", SqlDbType.BigInt);
            spara[2].Value = SuplID;

            spara[3] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            spara[3].Value = InvoiceDate;

            spara[4] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[4].Value = basicAmount;

            spara[5] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[5].Value = basicAmount;

            spara[6] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[6].Value = BranchId;

            spara[7] = new SqlParameter("@EnteredBy", SqlDbType.BigInt);
            spara[7].Value = EnteredBy;

            spara[8] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[8].Value = ItemXML;

            spara[9] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            spara[9].Direction = ParameterDirection.Output;
            spara[9].Value = -1;

            spara[10] = new SqlParameter("@paymentTerms", SqlDbType.NVarChar);
            spara[10].Value = PaymentTerms;

            spara[11] = new SqlParameter("@Particulars", SqlDbType.NVarChar);
            spara[11].Value = Particulars;

            spara[12] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[12].Value = AmountInWords;

            spara[13] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            spara[13].Value = InvoiceNo;

            spara[14] = new SqlParameter("@PenaltyXML", SqlDbType.Xml);
            spara[14].Value = PenaltyXML;

            spara[15] = new SqlParameter("@ApprovedBy", SqlDbType.BigInt);
            if (ApprovedBy==0)
            {
                spara[15].Value = DBNull.Value;
            }
            else
            {
                spara[15].Value = ApprovedBy;
            }        

            return dbconnector.ExecuteProcedureWithOutParameter("SP_SuplInvoice_Update", spara, 9);
        }

        public long InsertInvoice_Bulk(long EnteredBy)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[0].Value = EnteredBy;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_SuplInvoice_InsertInvoice_Auto", spara, 1);
        }

        public long InsertInvoice_BulkPTL(long EnteredBy,string FromDate,string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[0].Value = EnteredBy;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_SuplInvoice_InsertInvoice_Auto_PTL", spara, 1);
        }

        public DataSet InvoicePrint(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetSet("Invoice_Print", "SP_SuplInvoice_Print", spara);
        }

        public DataTable InvoicePrintDoc(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("Invoice_Print", "SP_SuplInvoice_PrintDoc", spara);
        }

        public DataTable GetSuplMainInvoiceDoc(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@InvoiceSrl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("Vw_SuplInvoiceRpt", "SP_SuplInvoice_Print_MainDoc", spara);
        }
    }
}
