﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
   public class CustomerDAO
    {
         DBLink dbconnector;

        public CustomerDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertCustomer(long Srl, string Name, string Address1, string Address2, string Address3, string landline, string mobile, string emailID, string vendor, string CST, string VAT, string TIN, string ServiceTaxno, long creditdays, decimal creditLimit, bool isActive, string GLCode, long CreatedBy, bool IsphysicalPODReqd,long CustomerGroupId)
        {
            SqlParameter[] sqlParams = new SqlParameter[21];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@Name", SqlDbType.NVarChar);
            sqlParams[1].Value = Name;

            sqlParams[2] = new SqlParameter("@Address1", SqlDbType.NVarChar);
            sqlParams[2].Value = Address1;

            sqlParams[3] = new SqlParameter("@Address2", SqlDbType.NVarChar);
            sqlParams[3].Value = Address2;

            sqlParams[4] = new SqlParameter("@Address3", SqlDbType.NVarChar);
            sqlParams[4].Value = Address3;

            sqlParams[5] = new SqlParameter("@Landline", SqlDbType.NVarChar);
            sqlParams[5].Value = landline;

            sqlParams[6] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
            sqlParams[6].Value = mobile;

            sqlParams[7] = new SqlParameter("@EmailID", SqlDbType.NVarChar);
            sqlParams[7].Value = emailID;

            sqlParams[8] = new SqlParameter("@VendorCode", SqlDbType.NVarChar);
            sqlParams[8].Value = vendor;

            sqlParams[9] = new SqlParameter("@CSTNo", SqlDbType.NVarChar);
            sqlParams[9].Value = CST;

            sqlParams[10] = new SqlParameter("@VATNo", SqlDbType.NVarChar);
            sqlParams[10].Value = VAT;

            sqlParams[11] = new SqlParameter("@TINNo", SqlDbType.NVarChar);
            sqlParams[11].Value = TIN;

            sqlParams[12] = new SqlParameter("@ServiceTaxNo", SqlDbType.NVarChar);
            sqlParams[12].Value = ServiceTaxno;

            sqlParams[13] = new SqlParameter("@CreditDays", SqlDbType.BigInt);
            sqlParams[13].Value = creditdays;

            sqlParams[14] = new SqlParameter("@CreditLimit", SqlDbType.Decimal);
            sqlParams[14].Value = creditLimit;

            sqlParams[15] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[15].Value = isActive;

            sqlParams[16] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[16].Value = -1;
            sqlParams[16].Direction = ParameterDirection.Output;
            sqlParams[16].Size = 9;

            sqlParams[17] = new SqlParameter("@GLCode", SqlDbType.NVarChar);
            sqlParams[17].Value = GLCode;

            sqlParams[18] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[18].Value = CreatedBy;

            sqlParams[19] = new SqlParameter("@PhysicalPODreqd", SqlDbType.Bit);
            sqlParams[19].Value = IsphysicalPODReqd;

            sqlParams[20] = new SqlParameter("@CustomerGroupId", SqlDbType.BigInt);
            sqlParams[20].Value = CustomerGroupId;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Insert", sqlParams, 16);
        }

        public DataTable GetCustomers(string SearchText,string ShowAll)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@ShowAll", SqlDbType.Char);
            sqlParams[1].Value = ShowAll;

            return dbconnector.USPGetTable("GetCustomer", "SP_Customer_Fill", sqlParams);
        }

        public DataTable GetCustomersUserWise(string SearchText, string ShowAll,long UserId)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@ShowAll", SqlDbType.Char);
            sqlParams[1].Value = ShowAll;

            sqlParams[2] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParams[2].Value = UserId;

            return dbconnector.USPGetTable("GetCustomer", "SP_Customer_Fill_UserWise", sqlParams);
        }

        public bool isCustomerAlreadyExist(string Name, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];          

            sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[0].Value = Name;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_isDuplicate", sqlParams, 2);

            if (cnt>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long DeleteCustomer(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Delete", sqlParams, 1);
        }

        public DataTable GetLobWiseCustomer(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetLobWiseCustomer", "SP_Customer_Fill_Lobwise", sqlParams);
        }

        public long InsertCustomerLob(long Srl, long LobId)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@LobId", SqlDbType.BigInt);
            sqlParams[1].Value = LobId;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Lob_Insert", sqlParams, 2);
        }

        public long DeleteCustomerLob(long Srl, long LobId)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@LobId", SqlDbType.BigInt);
            sqlParams[1].Value = LobId;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Lob_Delete", sqlParams, 2);
        }
    }
}
