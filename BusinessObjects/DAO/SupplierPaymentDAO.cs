﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class SupplierPaymentDAO
    {
        DBLink dbconnector;
        public SupplierPaymentDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertPaymentDetails(string refNo, string tranDate, long custId, string remarks, decimal totalAmount, long userId, string paymentXML, string BankName, string PaymentMode, string ChqNo, string ChqDate, string CardNetBankingDetails, string PayType, long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[15];

            spara[0] = new SqlParameter("@RefNo", SqlDbType.VarChar);
            spara[0].Value = refNo;

            spara[1] = new SqlParameter("@TranDate", SqlDbType.VarChar);
            spara[1].Value = tranDate;

            spara[2] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[2].Value = custId;

            spara[3] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[3].Value = remarks;

            spara[4] = new SqlParameter("@TotalAmount", SqlDbType.Decimal);
            spara[4].Value = totalAmount;

            spara[5] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[5].Value = userId;

            spara[6] = new SqlParameter("@PaymentDtlsXML", SqlDbType.Xml);
            spara[6].Value = paymentXML;

            spara[7] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[7].Value = -1;
            spara[7].Direction = ParameterDirection.Output;
            spara[7].Size = 9;

            spara[8] = new SqlParameter("@BankName", SqlDbType.NVarChar);
            spara[8].Value = BankName;

            spara[9] = new SqlParameter("@PaymentMode", SqlDbType.Char);
            spara[9].Value = PaymentMode;

            spara[10] = new SqlParameter("@ChqNo", SqlDbType.VarChar);
            spara[10].Value = ChqNo;

            spara[11] = new SqlParameter("@ChqDate", SqlDbType.VarChar);
            spara[11].Value = ChqDate;

            spara[12] = new SqlParameter("@CardNetBankingDetails", SqlDbType.VarChar);
            spara[12].Value = CardNetBankingDetails;

            spara[13] = new SqlParameter("@PayType", SqlDbType.Char);
            spara[13].Value = PayType;

            spara[14] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[14].Value = BranchID;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Payment_Insert", spara, 7);
        }

        public long InsertPaymentWhatsAppFail(string refNo, string tranDate, string supplier, string mobileNo, decimal amount, string user)
        {
            SqlParameter[] spara = new SqlParameter[7];

            spara[0] = new SqlParameter("@RefNo", SqlDbType.VarChar);
            spara[0].Value = refNo;

            spara[1] = new SqlParameter("@TranDate", SqlDbType.VarChar);
            spara[1].Value = tranDate;

            spara[2] = new SqlParameter("@Supplier", SqlDbType.VarChar);
            spara[2].Value = supplier;

            spara[3] = new SqlParameter("@MobileNo", SqlDbType.VarChar);
            spara[3].Value = mobileNo;

            spara[4] = new SqlParameter("@Amount", SqlDbType.Decimal);
            spara[4].Value = amount;

            spara[5] = new SqlParameter("@User", SqlDbType.VarChar);
            spara[5].Value = user;

            spara[6] = new SqlParameter("@Rcnt", SqlDbType.BigInt);
            spara[6].Value = -1;
            spara[6].Direction = ParameterDirection.Output;
            spara[6].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Payment_InsertWhatsAppFail", spara, 6);
        }

        public long UpdatePaymentHeader(string tranDate, long custId, string remarks, decimal totalAmount, long userId, string BankName, string PaymentMode, string ChqNo, string ChqDate, string CardNetBankingDetails, long Srl, decimal CashAmount, decimal FuelAmount)
        {
            SqlParameter[] spara = new SqlParameter[14];

            spara[0] = new SqlParameter("@TranDate", SqlDbType.VarChar);
            spara[0].Value = tranDate;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = custId;

            spara[2] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[2].Value = remarks;

            spara[3] = new SqlParameter("@TotalAmount", SqlDbType.Decimal);
            spara[3].Value = totalAmount;

            spara[4] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[4].Value = userId;

            spara[5] = new SqlParameter("@BankName", SqlDbType.NVarChar);
            spara[5].Value = BankName;

            spara[6] = new SqlParameter("@PaymentMode", SqlDbType.Char);
            spara[6].Value = PaymentMode;

            spara[7] = new SqlParameter("@ChqNo", SqlDbType.VarChar);
            spara[7].Value = ChqNo;

            spara[8] = new SqlParameter("@ChqDate", SqlDbType.VarChar);
            spara[8].Value = ChqDate;

            spara[9] = new SqlParameter("@CardNetBankingDetails", SqlDbType.VarChar);
            spara[9].Value = CardNetBankingDetails;

            spara[10] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[10].Value = -1;
            spara[10].Direction = ParameterDirection.Output;
            spara[10].Size = 9;

            spara[11] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[11].Value = Srl;

            spara[12] = new SqlParameter("@CashAmount", SqlDbType.Decimal);
            spara[12].Value = CashAmount;

            spara[13] = new SqlParameter("@FuelAmount", SqlDbType.Decimal);
            spara[13].Value = FuelAmount;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Payment_Update", spara, 10);
        }

        public long UpdateUnloadingChargesPayment(long LRId, decimal UnloadingCharges, string PayDate, long CreatedBy)
        {
            SqlParameter[] spara = new SqlParameter[5];

            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = LRId;

            spara[1] = new SqlParameter("@UnloadingCharges", SqlDbType.Decimal);
            spara[1].Value = UnloadingCharges;

            spara[2] = new SqlParameter("@PayDate", SqlDbType.VarChar);
            spara[2].Value = PayDate;

            spara[3] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[3].Value = CreatedBy;

            spara[4] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[4].Value = -1;
            spara[4].Direction = ParameterDirection.Output;
            spara[4].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UpdateUnloadingChargesPayment", spara, 4);
        }

        public DataSet ShowPaymentGrid(long SupplierID, long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierID;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchID;

            return dbconnector.USPGetSet("dtPayment", "SP_Supplier_Payment_View", spara);
        }

        public DataTable ShowPaymentGrid_DebitNote(long SupplierID, long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierID;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchID;

            return dbconnector.USPGetTable("dtPayment", "SP_Supplier_Payment_View_Debit_Note", spara);
        }

        public DataTable ShowPaymentStatus(long SupplierID, long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierID;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchID;

            return dbconnector.USPGetTable("dtPaymentStatus", "SP_Supplier_Payment_ViewBillStatus", spara);
        }

        public DataTable FillSupplierCombo(long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            return dbconnector.USPGetTable("dtCustCombo", "SP_Supplier_Payment_FillSupplier", spara);
        }

        public DataTable FillSupplierComboAdvance(long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            return dbconnector.USPGetTable("dtSuplCombo", "SP_Supplier_Payment_FillSupplier_Advance", spara);
        }

        public DataTable FillLRComboAdvance(long BranchID, long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = SupplierID;

            return dbconnector.USPGetTable("dtSuplCombo", "SP_Supplier_Payment_FillLR_Advance", spara);
        }

        public DataTable FillPaymentForWhatsApp(long BranchID, long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = SupplierID;

            return dbconnector.USPGetTable("dtSuplCombo", "SP_Supplier_Payment_FillWhatsapp", spara);
        }

        public DataTable GetPaymentDetailsForWhatsApp(string RefNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@RefNo", SqlDbType.NVarChar);
            spara[0].Value = RefNo;

            return dbconnector.USPGetTable("dtpayDetails", "SP_Supplier_Payment_GetPaymentDetails", spara);
        }

        public DataTable FillLRAdvance(long BranchID, long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = BranchID;

            spara[1] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[1].Value = SupplierID;

            return dbconnector.USPGetTable("dtLRGrid", "SP_Supplier_Payment_FillLR_Advance_Multiple", spara);
        }

        public DataTable FillPaymentHeader(bool DatefilterYes, string SearchFrom, string SearchTo, long BranchID, string PayType, string SearchText)
        {
            SqlParameter[] spara = new SqlParameter[5];

            spara[0] = new SqlParameter("@SearchFrom", SqlDbType.VarChar);
            if (DatefilterYes)
                spara[0].Value = SearchFrom;
            else
                spara[0].Value = DBNull.Value;

            spara[1] = new SqlParameter("@SearchTo", SqlDbType.VarChar);
            if (DatefilterYes)
                spara[1].Value = SearchTo;
            else
                spara[1].Value = DBNull.Value;

            spara[2] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[2].Value = BranchID;

            spara[3] = new SqlParameter("@PayType", SqlDbType.VarChar);
            spara[3].Value = PayType;

            spara[4] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[4].Value = SearchText;

            return dbconnector.USPGetTable("dtRcptHeader", "SP_Supplier_Payment_HeaderDetails", spara);
        }

        public DataTable FillPaymentHeaderAdvance(long PaymentID)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@PaymentID", SqlDbType.BigInt);
            spara[0].Value = PaymentID;


            return dbconnector.USPGetTable("dtptmHeader", "SP_Supplier_Payment_HeaderDetails_Advance", spara);
        }

        public DataTable FillPaymentInvoiceDetails(string RefNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PaymentID", SqlDbType.NVarChar);
            spara[0].Value = RefNo;

            return dbconnector.USPGetTable("dtRcptInvDtls", "SP_Supplier_Payment_InvoiceDetails", spara);
        }

        public long DeletePayment(string Srls)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            spara[0].Value = Srls;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Payment_Delete", spara, 1);
        }

        public DataTable PendingAdvanceReport()
        {
            //SqlParameter[] spara = new SqlParameter[2];

            //spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            //spara[0].Value = SupplierID;

            //spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            //spara[1].Value = BranchID;

            return dbconnector.USPGetTable("dtPendingAdvance", "SP_Report_Pending_Advance");
        }

        public DataTable ShowPayable(long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierID;

            return dbconnector.USPGetTable("dtPayable", "SP_Report_Supplier_Payable", spara);
        }

        public DataTable GetRefNo(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("dtRefNo", "SP_Supplier_Payment_GetRefNo", spara);
        }

        public DataTable PaymentWhatsAppFailReport(string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            return dbconnector.USPGetTable("dtWhatsAppFailReport", "SP_Supplier_Payment_WhatsAppFailReport", spara);
        }

        public DataTable CheckPaymentHold(long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierID;

            return dbconnector.USPGetTable("dtHoldDetails", "SP_Supplier_CheckPaymentHold", spara);
        }
    }
}
