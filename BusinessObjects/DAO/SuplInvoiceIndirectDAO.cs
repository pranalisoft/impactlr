﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;

namespace BusinessObjects.DAO
{
    public class SuplInvoiceIndirectDAO
    {
        DBLink dbconnector;
        public SuplInvoiceIndirectDAO()
        {
            dbconnector = new DBLink();
        }

        public DataTable ShowHeaderData(string FromDate,string ToDate,string SearchText)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[2].Value = SearchText;

            return dbconnector.USPGetTable("dtIndirectHeader", "SP_IndirectInvoices_ShowHeader", spara);

        }

        public DataTable ShowDetailData(long Srl)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;

            return dbconnector.USPGetTable("dtInvoice", "SP_IndirectInvoices_ShowItemDetails", spara);

        }

        public long InsertUpdateInvoice(long Srl, long BillCompanySrl, long SupplierSrl, decimal basicAmount, decimal InvoiceAmount, long CreatedBy, string ItemXML, string AmountInWords, string BuyerRefNo, string BuyerRefDate)
        {

            SqlParameter[] spara = new SqlParameter[11];

            spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            spara[0].Value = Srl;
 
            spara[1] = new SqlParameter("@SupplierSrl", SqlDbType.BigInt);
            spara[1].Value = SupplierSrl; 

            spara[2] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[2].Value = BillCompanySrl;

            spara[3] = new SqlParameter("@BuyerRefNo", SqlDbType.NVarChar);
            spara[3].Value = BuyerRefNo;

            spara[4] = new SqlParameter("@BuyerRefDate", SqlDbType.NVarChar);
            spara[4].Value = BuyerRefDate;

            spara[5] = new SqlParameter("@BasicAmount", SqlDbType.Decimal);
            spara[5].Value = basicAmount;

            spara[6] = new SqlParameter("@InvoiceAmount", SqlDbType.Decimal);
            spara[6].Value = InvoiceAmount ;

            spara[7] = new SqlParameter("@AmountInWords", SqlDbType.NVarChar);
            spara[7].Value = AmountInWords; 

            spara[8] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[8].Value = CreatedBy;

            spara[9] = new SqlParameter("@InvoiceDtlsXML", SqlDbType.Xml);
            spara[9].Value = ItemXML;

            spara[10] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[10].Direction = ParameterDirection.Output;
            spara[10].Value = -1;

           

            return dbconnector.ExecuteProcedureWithOutParameter("SP_IndirectInvoices_InsertUpdate", spara, 10);
        }

    }
}
