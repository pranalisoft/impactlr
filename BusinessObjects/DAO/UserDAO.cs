﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;
using Logger;

namespace BusinessObjects.DAO
{
    public class UserDAO
    {
        DBLink dbconnector;
        LogWriter logger;

        public UserDAO()
        {
            dbconnector = new DBLink();
        }

        public bool isUserAlreadyExist(string eid,long UserSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@userid", SqlDbType.VarChar);
            sqlParams[0].Value = eid;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            sqlParams[2] = new SqlParameter("@UserSrl", SqlDbType.BigInt);
            sqlParams[2].Value = UserSrl;

            dbconnector.ExecuteProcedure("SP_User_isDuplicate", sqlParams);
            long cnt=long.Parse(sqlParams[1].Value.ToString());
            if (cnt > 0)
                return true;
            else
                return false;
        }

        public long InsertUser(string UserId, string Userpwd, string User_Name, string Email_Id, long Role_Srl, string Plant, long UserSrl, bool isActive, byte[] logo, string WhatsAppNo)
        {
            SqlParameter[] sqlParams = new SqlParameter[11];
            sqlParams[0] = new SqlParameter("@UserId", SqlDbType.VarChar);
            sqlParams[0].Value = UserId;

            sqlParams[1] = new SqlParameter("@Userpwd", SqlDbType.VarChar);
            sqlParams[1].Value = Userpwd;

            sqlParams[2] = new SqlParameter("@User_Name", SqlDbType.VarChar);
            sqlParams[2].Value = User_Name;

            sqlParams[3] = new SqlParameter("@Email_Id", SqlDbType.VarChar);
            sqlParams[3].Value = Email_Id;

            sqlParams[4] = new SqlParameter("@Role_Srl", SqlDbType.BigInt);
            sqlParams[4].Value = Role_Srl;

            sqlParams[5] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[5].Value = -1;
            sqlParams[5].Direction = ParameterDirection.Output;
            sqlParams[5].Size = 9;

            sqlParams[6] = new SqlParameter("@Plant", SqlDbType.Xml);
            sqlParams[6].Value = Plant;

            sqlParams[7] = new SqlParameter("@UserSrl", SqlDbType.BigInt);
            sqlParams[7].Value = UserSrl;

            sqlParams[8] = new SqlParameter("@isActive", SqlDbType.BigInt);
            sqlParams[8].Value = isActive;

            sqlParams[9] = new SqlParameter("@logo", SqlDbType.Image);
            sqlParams[9].Value = logo;

            sqlParams[10] = new SqlParameter("@WhatsAppNo", SqlDbType.VarChar);
            sqlParams[10].Value = WhatsAppNo;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_User_Insert", sqlParams, 5);
        }


        public DataTable GetRole()
        {
            return dbconnector.USPGetTable("RoleData", "SP_Role_Fill");
        }

        public DataTable GetUserData(string ShowAll, string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@ShowAll", SqlDbType.VarChar);
            sqlParams[0].Value = ShowAll;
            sqlParams[1] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[1].Value = SearchText;
            return dbconnector.USPGetTable("UserData", "SP_User_Fill",sqlParams);
        }

        public DataTable GetUserDataByID(long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;
           
            return dbconnector.USPGetTable("UserDataByID", "SP_User_FillByID", sqlParams);
        }

        public DataTable GetuserDefLocBU(long eid)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = eid;

            return dbconnector.USPGetTable("UserDefLoc", "SP_GetUserDefLocBU",sqlParams);
        }

        public DataTable GetUserBranches(long UserSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@UserSrl", SqlDbType.BigInt);
            sqlParams[0].Value = UserSrl;

            return dbconnector.USPGetTable("UserDefLoc", "SP_User_Branch_Fill", sqlParams);
        }
    }
}
