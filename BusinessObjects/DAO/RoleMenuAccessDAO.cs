﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;
namespace BusinessObjects.DAO
{
    public class RoleMenuAccessDAO
    {

        DBLink dbConnector;
        public RoleMenuAccessDAO()
        {
            dbConnector = new DBLink();
        }
        #region Common Methods

        public DataTable GetRoles()
        {

            return dbConnector.USPGetTable("Roles", "SP_Role_Fill");
        }

        public DataTable GetMenuAccessData()
        {
            return dbConnector.USPGetTable("Roles", "SP_Role_MenuAccess");
        }

        public DataTable ShowMenuAssigned(long RoleId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RoleSrl", SqlDbType.BigInt);
            sqlParams[0].Value = RoleId;
            return dbConnector.USPGetTable("Roles", "SP_Role_MenuAccess", sqlParams);
        }

        public long InsertRoleMenuAccess(long roleId, string roleName, string menuXml)
        {
            try
            {
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@rolesrl", SqlDbType.BigInt);
                sqlParams[0].Value = roleId;

                sqlParams[1] = new SqlParameter("@MenuXML", SqlDbType.Xml);
                sqlParams[1].Value = menuXml;

                sqlParams[2] = new SqlParameter("@pcnt", SqlDbType.BigInt);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[2].Size = 9;
                sqlParams[2].Value = -1;

                return dbConnector.ExecuteProcedureWithOutParameter("SP_RoleMenu_Insert", sqlParams, 2);
            }
            catch (Exception)
            {
                return -1;
                throw;
            }
        }

        public long DeletetRoleMenuAccess(long roleId)
        {
            try
            {
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("p_id", SqlDbType.BigInt);
                sqlParams[0].Value = roleId;

                long ins1 = dbConnector.ExecuteProcedure("USP_DELETE_ROLEMENUACCESS", sqlParams);
                return ins1;
            }
            catch (Exception)
            {
                return -1;
                throw;
            }
        }
        #endregion Common Methods
    }
}
