﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class CustomerReceiptDAO
    {
        DBLink dbconnector;
        public CustomerReceiptDAO()
       {
           dbconnector = new DBLink();
       }

        public long InsertPaymentDetails(string refNo, string tranDate, long custId, string remarks, decimal totalAmount, long userId, string paymentXML, string BankName, string PaymentMode, string ChqNo, string ChqDate, string CardNetBankingDetails, string PayType, long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[15];

           spara[0] = new SqlParameter("@RefNo", SqlDbType.VarChar);
           spara[0].Value = refNo;

           spara[1] = new SqlParameter("@TranDate", SqlDbType.VarChar);
           spara[1].Value = tranDate;

           spara[2] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
           spara[2].Value = custId;

           spara[3] = new SqlParameter("@Remarks", SqlDbType.VarChar);
           spara[3].Value = remarks;

           spara[4] = new SqlParameter("@TotalAmount", SqlDbType.Decimal);
           spara[4].Value = totalAmount;


           spara[5] = new SqlParameter("@UserId", SqlDbType.BigInt);
           spara[5].Value = userId;

           spara[6] = new SqlParameter("@PaymentDtlsXML", SqlDbType.Xml);
           spara[6].Value = paymentXML;

           spara[7] = new SqlParameter("@pcnt", SqlDbType.BigInt);
           spara[7].Value = -1;
           spara[7].Direction = ParameterDirection.Output;
           spara[7].Size = 9;

           spara[8] = new SqlParameter("@BankName", SqlDbType.NVarChar);
           spara[8].Value = BankName;

           spara[9] = new SqlParameter("@PaymentMode", SqlDbType.Char);
           spara[9].Value = PaymentMode;

           spara[10] = new SqlParameter("@ChqNo", SqlDbType.VarChar);
           spara[10].Value = ChqNo;

           spara[11] = new SqlParameter("@ChqDate", SqlDbType.VarChar);
           spara[11].Value = ChqDate;

           spara[12] = new SqlParameter("@CardNetBankingDetails", SqlDbType.VarChar);
           spara[12].Value = CardNetBankingDetails;

           spara[13] = new SqlParameter("@PayType", SqlDbType.Char);
           spara[13].Value = PayType;

           spara[14] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[14].Value = BranchID;

           return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Receipt_Insert", spara, 7);
       }

        public DataTable ShowReceiptGrid(long CustomerID, long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[2];

           spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
           spara[0].Value = CustomerID;

           spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[1].Value = BranchID;

           return dbconnector.USPGetTable("dtPayment", "SP_Customer_Receipt_View", spara);
       }

        public DataTable ShowReceiptGridLR(long CustomerID, long BranchID,string InvoiceNo)
        {
            SqlParameter[] spara = new SqlParameter[3];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchID;

            spara[2] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            spara[2].Value = InvoiceNo;

            return dbconnector.USPGetTable("dtPayment", "SP_Customer_Receipt_View_LR", spara);
        }

        public DataTable ShowReceiptStatus(long CustomerID, long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[2];

           spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
           spara[0].Value = CustomerID;

           spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[1].Value = BranchID;

           return dbconnector.USPGetTable("dtPaymentStatus", "SP_Customer_Receipt_ViewBillStatus", spara);
       }

       public DataTable FillCustomerCombo(long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[1];

           spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[0].Value = BranchID;

           return dbconnector.USPGetTable("dtCustCombo", "SP_Customer_Receipt_FillCustomer", spara);
       }

       public DataTable FillReceiptHeader(bool DatefilterYes, string SearchFrom, string SearchTo, long BranchID,string PayType)
       {
           SqlParameter[] spara = new SqlParameter[4];

           spara[0] = new SqlParameter("@SearchFrom", SqlDbType.VarChar);
           if (DatefilterYes)
               spara[0].Value = SearchFrom;
           else
               spara[0].Value = DBNull.Value;

           spara[1] = new SqlParameter("@SearchTo", SqlDbType.VarChar);
           if (DatefilterYes)
               spara[1].Value = SearchTo;
           else
               spara[1].Value = DBNull.Value;

           spara[2] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[2].Value = BranchID;

           spara[3] = new SqlParameter("@PayType", SqlDbType.VarChar);
           spara[3].Value = PayType;           

           return dbconnector.USPGetTable("dtRcptHeader", "SP_Customer_Receipt_HeaderDetails", spara);
       }

       public DataTable FillReceiptInvoiceDetails(long ReceiptID)
       {
           SqlParameter[] spara = new SqlParameter[1];
           spara[0] = new SqlParameter("@ReceiptID", SqlDbType.BigInt);
           spara[0].Value = ReceiptID;

           return dbconnector.USPGetTable("dtRcptInvDtls", "SP_Customer_Receipt_InvoiceDetails_LR", spara);
       }

       public long DeleteReceipt(string Srls)
       {
           SqlParameter[] spara = new SqlParameter[2];
           spara[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
           spara[0].Value = Srls;

           spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
           spara[1].Direction = ParameterDirection.Output;
           spara[1].Size = 9;
           spara[1].Value = -1;

           return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_Receipt_Delete", spara, 1);
       }

       public DataTable FillReceiptOnAccount(long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[1];

           spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[0].Value = BranchID;


           return dbconnector.USPGetTable("dtRcptHeader", "SP_Customer_Receipt_View_OnAccount", spara);
       }

       public DataTable FillReceiptOnAccountInfo(long RcptID)
       {
           SqlParameter[] spara = new SqlParameter[1];

           spara[0] = new SqlParameter("@RcptID", SqlDbType.BigInt);
           spara[0].Value = RcptID;


           return dbconnector.USPGetTable("dtRcptHeader", "SP_Receipt_View_OnAccount_Info", spara);
       }

       public long InsertOnAccountPaymentDetails(long Srl, string PaymentXML, long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[4];

           spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
           spara[0].Value = Srl;

           spara[1] = new SqlParameter("@PaymentDtlsXML", SqlDbType.VarChar);
           spara[1].Value = PaymentXML;

           spara[2] = new SqlParameter("@pcnt", SqlDbType.BigInt);
           spara[2].Value = -1;
           spara[2].Direction = ParameterDirection.Output;
           spara[2].Size = 9;

           spara[3] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[3].Value = BranchID;

          return dbconnector.ExecuteProcedureWithOutParameter("SP_Customer_ReceiptOnAccount_Insert", spara, 2);
       }

       public DataTable CustomerOutstandingReport(long BranchID,long UserId)
       {
           SqlParameter[] spara = new SqlParameter[2];
           spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[0].Value = BranchID;

           spara[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
           spara[1].Value = UserId;

           return dbconnector.USPGetTable("dtOutstanding", "SP_Customer_OutstandingReport", spara);           
       }

       public DataTable CustomerPendingInvoices(long BranchID)
       {
           SqlParameter[] spara = new SqlParameter[1];

           spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
           spara[0].Value = BranchID;

           return dbconnector.USPGetTable("dtPendingBilling", "SP_Customer_PendingBilling", spara);

       }
    }
}
