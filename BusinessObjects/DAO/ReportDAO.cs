﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class ReportDAO
    {
        DBLink dbconnector;

        public ReportDAO()
        {
            dbconnector = new DBLink();
        }

        public DataTable LR_Document(long LRID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRID", SqlDbType.BigInt);
            spara[0].Value = LRID;

            return dbconnector.USPGetTable("VW_LR", "SP_Report_LR_Doc", spara);
        }

        public DataTable LR_DocumentMultiple(string LRIDs)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRIDs", SqlDbType.NVarChar);
            spara[0].Value = LRIDs;

            return dbconnector.USPGetTable("VW_LR", "SP_Report_LR_DocMultiple", spara);
        }

        public DataTable LR_Annexure(long LRID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRID", SqlDbType.BigInt);
            spara[0].Value = LRID;

            return dbconnector.USPGetTable("Vw_LRPTLAnnexure", "SP_Report_LR_DocAnnexure", spara);
        }

        public DataTable LR_AnnexureMultiple(string LRIDs)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRIDs", SqlDbType.NVarChar);
            spara[0].Value = LRIDs;

            return dbconnector.USPGetTable("Vw_LRPTLAnnexure", "SP_Report_LR_DocAnnexureMultiple", spara);
        }

        public DataTable Invoice_Document(string InvRefNo,long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@InvRefNo", SqlDbType.VarChar);
            spara[0].Value = InvRefNo;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;   

            return dbconnector.USPGetTable("VW_Invoice", "SP_Report_Invoice_Doc", spara);
        }

        public DataTable Invoice_Document_Multiple(string InvSrls, long BillingCompanySrl,int InvYear=0)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@InvSrls", SqlDbType.VarChar);
            spara[0].Value = InvSrls;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            spara[2] = new SqlParameter("@Year", SqlDbType.Int);
            spara[2].Value = InvYear;

            return dbconnector.USPGetTable("VW_Invoice", "SP_Report_Invoice_Doc_Multiple", spara);
        }

        public DataTable Customer_Ledger(long CustomerID,string FromDate,string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[1].Value = FromDate;

            spara[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[2].Value = ToDate;

            return dbconnector.USPGetTable("VW_Customer_Ledger", "SP_Report_Customer_Ledger", spara);
        }

        public decimal Customer_Ledger_Opening(long CustomerID, string FromDate)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[1].Value = FromDate;

            spara[2] = new SqlParameter("@Opening", SqlDbType.Decimal);
            spara[2].Value = 0;
            spara[2].Direction = ParameterDirection.Output;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Report_Customer_Ledger_Opening", spara, 2);
        }

        public DataTable DashBoard(string fromdate,string todate,long UserId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = fromdate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = todate;

            spara[2] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[2].Value = UserId;

            return dbconnector.USPGetTable("Dashboard", "SP_Dashboard", spara);
        }

        public DataTable DashBoardPTL(string fromdate, string todate, long BranchId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@fromdate", SqlDbType.VarChar);
            spara[0].Value = fromdate;

            spara[1] = new SqlParameter("@todate", SqlDbType.VarChar);
            spara[1].Value = todate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            return dbconnector.USPGetTable("DashboardPTL", "SP_Dashboard_PTL", spara);
        }

        public DataTable AdvancePaidReport(string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            return dbconnector.USPGetTable("dtAdvancePaid", "SP_Report_Advance_Paid", spara);
        }

        public DataTable BalancePaidReport(string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            return dbconnector.USPGetTable("dtBalancePaid", "SP_Report_Balance_Paid", spara);
        }

        public DataTable GetPaymentVoucherData(string RefNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PaymentRefNo", SqlDbType.VarChar);
            spara[0].Value = RefNo;

            return dbconnector.USPGetTable("VW_Supplier_Payment_Report", "SP_Supplier_Payment_Print", spara);
        }

        public DataSet GetPaymentVoucherEmailDetails(string RefNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PaymentRefNo", SqlDbType.VarChar);
            spara[0].Value = RefNo;

            return dbconnector.USPGetSet("VW_Supplier_Payment_Report", "SP_Supplier_Payment_Email", spara);
        }

        public DataTable GEtPODPerformance(string FromDate, string ToDate, long BranchId, string type)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            spara[3] = new SqlParameter("@type", SqlDbType.VarChar);
            spara[3].Value = type;

            return dbconnector.USPGetTable("dtPerformancelink", "SP_Report_POD_Performance_Link", spara);
        }

        public DataTable GetPODPerformanceSlabWise(string FromDate, string ToDate, long BranchId, string type,long UserId)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            spara[3] = new SqlParameter("@type", SqlDbType.VarChar);
            spara[3].Value = type;

            spara[4] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[4].Value = UserId;

            return dbconnector.USPGetTable("dtPerformancelink", "SP_Report_POD_Performance_SlabWiseExcel", spara);
        }

        public DataTable GetSupplierPayableLink(long SupplierId, long BillingCompanySrl)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@SupplierId", SqlDbType.BigInt);
            spara[0].Value = SupplierId;

            spara[1] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[1].Value = BillingCompanySrl;

            return dbconnector.USPGetTable("dtSupplierPayable", "SP_Supplier_Payable_Fill_Link", spara);
        }

        public DataTable GetPendingDashboard(long UserId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[0].Value = UserId;

            return dbconnector.USPGetTable("dtPendingDashboard", "SP_Report_PendingDashboard",spara);
        }

        public DataTable DashboardManager(long UserId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[0].Value = UserId;

            return dbconnector.USPGetTable("dtDashboardManager", "SP_Dashboard_Manager", spara);
        }

        public DataTable DashboardManagerExcel(long BranchId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            return dbconnector.USPGetTable("dtDashboardManagerExcel", "SP_Dashboard_Manager_PendingPOD", spara);
        }

        public DataTable DashboardAccounts(long UserId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[0].Value = UserId;

            return dbconnector.USPGetTable("dtDashboardAccounts", "SP_Dashboard_Accounts", spara);
        }

        public DataTable DashboardFillAllocatedGrids(long RoleId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@RoleId", SqlDbType.BigInt);
            spara[0].Value = RoleId;

            return dbconnector.USPGetTable("dtDashboardGridsAllocated", "SP_Dashboard_FillGridsAllocated", spara);
        }

        public DataTable GetPendingDashboard_Link(long BranchId, string type)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            spara[1] = new SqlParameter("@type", SqlDbType.VarChar);
            spara[1].Value = type;

            return dbconnector.USPGetTable("dtPendingDashboardlink", "SP_Report_PendingDashboard_Link", spara);
        }

        public DataTable GetDueEwayBillsDashboard(long UserId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[0].Value = UserId;

            return dbconnector.USPGetTable("dtdueEwayBill", "SP_DueEwayBillDashboard", spara);
        }

        public DataTable PTLMIS(string FromDate, string ToDate, long BranchId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            return dbconnector.USPGetTable("dtPTLMIS", "SP_Report_MIS_PTL", spara);
        }

        public DataTable SupplierLRDetails(string FromDate, string Todate, long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@Todate", SqlDbType.VarChar);
            spara[1].Value = Todate;

            spara[2] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[2].Value = SupplierID;

            return dbconnector.USPGetTable("dtSuplLRDetails", "SP_Report_Supplier_LRDetails", spara);
        }

        public DataTable SupplierPurchaseRegister(string FromDate, string Todate, long SupplierID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@Todate", SqlDbType.VarChar);
            spara[1].Value = Todate;

            spara[2] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[2].Value = SupplierID;

            return dbconnector.USPGetTable("dtSuplPurReg", "SP_SuplInvoice_Purchase_RegisterNew", spara);
        }
    }
}
