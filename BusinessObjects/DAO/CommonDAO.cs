﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;
namespace BusinessObjects.DAO
{
    public class CommonDAO
    {
        DBLink dbconnector;

        public CommonDAO()
        {
            dbconnector = new DBLink();
        }
        public DataTable GetEmpNameRole(long Eid)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = Eid;

            return dbconnector.USPGetTable("GetEmpNameRole", "SP_Employee_Role", sqlParams);
        }

        public DataTable FillBankDetails()
        {
            return dbconnector.USPGetTable("dtBankNames", "SP_Bank_Details");
        }

        public DataTable GetMenu(long Eid)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = Eid;

            return dbconnector.USPGetTable("GetMenu", "SP_Employee_GetMenu", sqlParams);
        }

        public DataTable GetChallanDetails(string Search, string FromDate, string ToDate, long plnt)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@Search", SqlDbType.VarChar);
            sqlParams[0].Value = Search;

            sqlParams[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            if (FromDate == "")
                sqlParams[1].Value = DBNull.Value;
            else
                sqlParams[1].Value = FromDate;

            sqlParams[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            if (ToDate == "")
                sqlParams[2].Value = DBNull.Value;
            else
                sqlParams[2].Value = ToDate;

            sqlParams[3] = new SqlParameter("@plnt", SqlDbType.BigInt);
            sqlParams[3].Value = plnt;

            return dbconnector.USPGetTable("ChallanDetails", "SP_Challan_View_Dashboard", sqlParams);
        }       

        public string GetPassword(long eid)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = eid;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.VarChar);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 25;

            dbconnector.ExecuteProcedure("SP_User_GetPassword", sqlParams);
            return sqlParams[1].Value.ToString();
        }

        public void ChangePassword(long eid, string newPass)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = eid;

            sqlParams[1] = new SqlParameter("@NewPwd", SqlDbType.VarChar);
            sqlParams[1].Value = newPass;

            dbconnector.ExecuteProcedure("SP_User_ChangePassword", sqlParams);
        }

        public long ValidateLogin(string password, string userid)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@userid", SqlDbType.VarChar);
            sqlParams[0].Value = userid;

            sqlParams[1] = new SqlParameter("@pwd", SqlDbType.VarChar);
            sqlParams[1].Value = password;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_User_ValidateLogin", sqlParams, 2);
        }

        public DataTable GetBranch(long srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = srl;

            return dbconnector.USPGetTable("GetBranch", "SP_FillBranch", sqlParams);
        }

        public DataTable GetChallanRegister(string returnable, string FromDate, string ToDate, string Supplier,string INPNo,string RaisedBy,string Status, long plnt)
        {
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@Returnable", SqlDbType.Char);
            if (returnable == "")
                returnable = "%";
            sqlParams[0].Value = returnable;
           

            sqlParams[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            sqlParams[1].Value = FromDate;

            sqlParams[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            sqlParams[2].Value = ToDate;

            sqlParams[3] = new SqlParameter("@Supplier", SqlDbType.VarChar);
            sqlParams[3].Value = Supplier;

            sqlParams[4] = new SqlParameter("@INPNo", SqlDbType.VarChar);
            sqlParams[4].Value = INPNo;

            sqlParams[5] = new SqlParameter("@Raisedby", SqlDbType.VarChar);
            sqlParams[5].Value = RaisedBy;

            sqlParams[6] = new SqlParameter("@Status", SqlDbType.VarChar);            
            if (Status == "")
                Status = "%";
            sqlParams[6].Value = Status;

            sqlParams[7] = new SqlParameter("@plnt", SqlDbType.BigInt);
            sqlParams[7].Value = plnt;

            return dbconnector.USPGetTable("ChallanReg", "SP_Challan_View_Register", sqlParams);
        }

        public DataTable GetLRForPostPOD(string SearchText, long BranchID, string AddOrEdit, bool PendingForApproval)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            sqlParams[1].Value = BranchID;

            sqlParams[2] = new SqlParameter("@AddOrEdit", SqlDbType.VarChar);
            sqlParams[2].Value = AddOrEdit;

            sqlParams[3] = new SqlParameter("@PendingForApproval", SqlDbType.Bit);
            sqlParams[3].Value = PendingForApproval;
            
            return dbconnector.USPGetTable("GetPostPODFillLR", "SP_LR_PostPODFillLR", sqlParams);
        }

        public DataTable GetLRForPostPODPTL(string SearchText, long BranchID, string AddOrEdit, bool PendingForApproval)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            sqlParams[1].Value = BranchID;

            sqlParams[2] = new SqlParameter("@AddOrEdit", SqlDbType.VarChar);
            sqlParams[2].Value = AddOrEdit;

            sqlParams[3] = new SqlParameter("@PendingForApproval", SqlDbType.Bit);
            sqlParams[3].Value = PendingForApproval;

            return dbconnector.USPGetTable("GetPostPODFillLRPTL", "SP_LR_PostPODFillLRPTL", sqlParams);
        }

        public DataTable GetPODPenalty(long PostPODID)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@PostPODID", SqlDbType.BigInt);
            sqlParams[0].Value = PostPODID;

            return dbconnector.USPGetTable("dtPenalty", "SP_LR_PostPODGetPenalty", sqlParams);
        }

        public DataTable GetZeroLR(long LRID)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@LRID", SqlDbType.BigInt);
            sqlParams[0].Value = LRID;

            return dbconnector.USPGetTable("dtZeroLR", "SP_LR_PostPODFillLR_Zero", sqlParams);
        }

        public long InsertUpdate_PostPOD(long ID, long LRId, decimal DetentionCharges, decimal WaraiCharges, decimal TwoPointCharges, decimal OtherCharges, decimal Penalty, string Remarks, decimal LatePODCharges, decimal TotalCharges, long CreatedBy, string strItem, bool Approved, string ReportingDate="", string UnloadingDate="", string PODDate="")
        {
            SqlParameter[] sqlParams = new SqlParameter[17];
            sqlParams[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParams[0].Value = ID;

            sqlParams[1] = new SqlParameter("@LRId", SqlDbType.BigInt);
            sqlParams[1].Value = LRId;

            sqlParams[2] = new SqlParameter("@DetentionCharges", SqlDbType.Decimal);
            sqlParams[2].Value = DetentionCharges;

            sqlParams[3] = new SqlParameter("@WaraiCharges", SqlDbType.Decimal);
            sqlParams[3].Value = WaraiCharges;

            sqlParams[4] = new SqlParameter("@TwoPointCharges", SqlDbType.Decimal);
            sqlParams[4].Value = TwoPointCharges;

            sqlParams[5] = new SqlParameter("@OtherCharges", SqlDbType.Decimal);
            sqlParams[5].Value = OtherCharges;

            sqlParams[6] = new SqlParameter("@Penalty", SqlDbType.Decimal);
            sqlParams[6].Value = Penalty;

            sqlParams[7] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            sqlParams[7].Value = Remarks;

            sqlParams[8] = new SqlParameter("@LatePODCharges", SqlDbType.Decimal);
            sqlParams[8].Value = LatePODCharges;

            sqlParams[9] = new SqlParameter("@TotalCharges", SqlDbType.Decimal);
            sqlParams[9].Value = TotalCharges;

            sqlParams[10] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[10].Value = CreatedBy;

            sqlParams[11] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            sqlParams[11].Value = -1;
            sqlParams[11].Direction = ParameterDirection.Output;
            sqlParams[11].Size = 9;

            sqlParams[12] = new SqlParameter("@strItem", SqlDbType.Xml);
            sqlParams[12].Value = strItem;

            sqlParams[13] = new SqlParameter("@Approved", SqlDbType.Bit);
            sqlParams[13].Value = Approved;

            sqlParams[14] = new SqlParameter("@ReportingDate", SqlDbType.VarChar);
            sqlParams[14].Value = ReportingDate;

            sqlParams[15] = new SqlParameter("@UnloadingDate", SqlDbType.VarChar);
            sqlParams[15].Value = UnloadingDate;

            sqlParams[16] = new SqlParameter("@PODDate", SqlDbType.VarChar);
            sqlParams[16].Value = PODDate;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_PostPODUpdation", sqlParams, 11);
        }

        public DataTable GetSupplierPayableApproval(long BillingCompanySrl, string ShowOrGenerate)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[0].Value = BillingCompanySrl;

            sqlParams[1] = new SqlParameter("@ShowOrGenerate", SqlDbType.Char);
            sqlParams[1].Value = ShowOrGenerate;

            return dbconnector.USPGetTable("dtSuplPayableApproval", "SP_Supplier_Payable_Fill", sqlParams);
        }

        public long InsertSupplierPayableApproval(string ItemXML, long BillingCompanySrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[0].Value = BillingCompanySrl;

            sqlParams[1] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            sqlParams[1].Value = ItemXML;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Supplier_Payable_Insert", sqlParams, 2);
        }

        public DataTable GetCustomerReceivable(long BillingCompanySrl,string Date)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[0].Value = BillingCompanySrl;

            sqlParams[1] = new SqlParameter("@Date", SqlDbType.NVarChar);
            sqlParams[1].Value = Date;

            return dbconnector.USPGetTable("dtCustomerReceivable", "SP_Customer_Receivable_Fill", sqlParams);
        }

        public long InsertUpdateScrap(long Srl, long ItemSrl, int Qty, string Remarks, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@ItemSrl", SqlDbType.BigInt);
            sqlParams[1].Value = ItemSrl;

            sqlParams[2] = new SqlParameter("@Qty", SqlDbType.Int);
            sqlParams[2].Value = Qty;

            sqlParams[3] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            sqlParams[3].Value = Remarks;

            sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[4].Value = CreatedBy;

            sqlParams[5] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[5].Value = -1;
            sqlParams[5].Direction = ParameterDirection.Output;
            sqlParams[5].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Scrap_Insert", sqlParams, 5);
        }

        public DataTable FillScrap()
        {
            return dbconnector.USPGetTable("dtScrap", "SP_Scrap_Fill");
        }

        public DataTable FillScrapItem()
        {
            return dbconnector.USPGetTable("dtScrapItem", "SP_Scrap_Fill_Items");
        }

        public long InsertUpdateReturn(long Srl, long SupplierId, long ItemSrl, int Qty, int LostQty, string Remarks, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@SupplierId", SqlDbType.BigInt);
            sqlParams[1].Value = SupplierId;

            sqlParams[2] = new SqlParameter("@ItemSrl", SqlDbType.BigInt);
            sqlParams[2].Value = ItemSrl;

            sqlParams[3] = new SqlParameter("@Qty", SqlDbType.Int);
            sqlParams[3].Value = Qty;

            sqlParams[4] = new SqlParameter("@LostQty", SqlDbType.Int);
            sqlParams[4].Value = LostQty;

            sqlParams[5] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            sqlParams[5].Value = Remarks;

            sqlParams[6] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[6].Value = CreatedBy;

            sqlParams[7] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[7].Value = -1;
            sqlParams[7].Direction = ParameterDirection.Output;
            sqlParams[7].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Return_Insert", sqlParams, 7);
        }

        public DataTable FillReturn()
        {
            return dbconnector.USPGetTable("dtReturn", "SP_Return_Fill");
        }

        public DataTable FillReturnSupplier()
        {
            return dbconnector.USPGetTable("dtReturnSupplier", "SP_Return_Fill_Supplier");
        }

        public DataSet FillReturnItem(long SupplierId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SupplierId", SqlDbType.BigInt);
            sqlParams[0].Value = SupplierId;
            return dbconnector.USPGetSet("dtReturnItem", "SP_Return_Fill_Item", sqlParams);
        }

        public string GetUserRoleMenu(long userid)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@UserID", SqlDbType.BigInt);
            spara[0].Value = userid;

            spara[1] = new SqlParameter("@MenuXML", SqlDbType.Xml);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Value = DBNull.Value;

            int cnt = dbconnector.ExecuteProcedure("GenerateRoleMenu", spara);

            if (spara[1].Value != null || spara[1].Value != DBNull.Value || string.IsNullOrEmpty(spara[1].Value.ToString()))
            {
                return spara[1].Value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        public DataTable FillPlysheetRegister()
        {
            return dbconnector.USPGetTable("dtPlysheetRegister", "SP_Plysheet_Register");
        }

        public long UploadFreightTigerDataDump()
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@RecordCount", SqlDbType.Int);
            sqlParams[0].Value = -1;
            sqlParams[0].Direction = ParameterDirection.Output;
            sqlParams[0].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("usp_FreightTigerDataUpload", sqlParams, 0);
        }

        public DataTable GenerateTallySalesExcel(long BillingCompanySrl, string FromDate, string ToDate)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[0].Value = BillingCompanySrl;

            sqlParams[1] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            sqlParams[1].Value = FromDate;

            sqlParams[2] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            sqlParams[2].Value = ToDate;

            return dbconnector.USPGetTable("Sales", "SP_GenerateTallySalesTemplate", sqlParams);
        }

        public DataTable GenerateTallyPurchaseExcel(long BillingCompanySrl, string FromDate, string ToDate)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[0].Value = BillingCompanySrl;

            sqlParams[1] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            sqlParams[1].Value = FromDate;

            sqlParams[2] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            sqlParams[2].Value = ToDate;

            return dbconnector.USPGetTable("Purchase", "SP_GenerateTallyPurchaseTemplate", sqlParams);
        }

        public void UpdateOtp(long eid, string newOtp)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@eid", SqlDbType.BigInt);
            sqlParams[0].Value = eid;

            sqlParams[1] = new SqlParameter("@otp", SqlDbType.VarChar);
            sqlParams[1].Value = newOtp;

            dbconnector.ExecuteProcedure("SP_User_UpdateOtp", sqlParams);
        }

        public long ValidateLoginwithOtp(string password, string userid, string otp)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@userid", SqlDbType.VarChar);
            sqlParams[0].Value = userid;

            sqlParams[1] = new SqlParameter("@pwd", SqlDbType.VarChar);
            sqlParams[1].Value = password;

            sqlParams[2] = new SqlParameter("@otp", SqlDbType.NVarChar);
            sqlParams[2].Value = otp;

            sqlParams[3] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[3].Value = -1;
            sqlParams[3].Direction = ParameterDirection.Output;
            sqlParams[3].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_User_ValidateLoginWithOTP", sqlParams, 3);
        }

    }
}
