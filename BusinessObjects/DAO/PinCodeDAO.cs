﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using SqlManager;

namespace BusinessObjects.DAO
{
   public class PinCodeDAO
    {
       DBLink dbConnector;
       public PinCodeDAO()
       {
           dbConnector = new DBLink();
       }
       public long InsertUpdatePinCode(long Srl,long PinCode,string Longitude,string Lattitude)
       {
           SqlParameter[] spara = new SqlParameter[5];
           spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
           spara[0].Value = Srl;

           spara[1] = new SqlParameter("@PinCode", SqlDbType.BigInt);
           spara[1].Value = PinCode;

           spara[2] = new SqlParameter("@Longitude", SqlDbType.NVarChar);
           spara[2].Value = Longitude;

           spara[3] = new SqlParameter("@Lattitude", SqlDbType.NVarChar);
           spara[3].Value = Lattitude;

           spara[4] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
           spara[4].Direction = ParameterDirection.Output;
           spara[4].Value = -1;

           return dbConnector.ExecuteProcedureWithOutParameter("SP_PinCode_InsertUpdate", spara, 4);
       }
       public DataTable GetPinCodes(string SearchText)
       {
           SqlParameter[] spara = new SqlParameter[1];
           spara[0] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
           spara[0].Value = SearchText;

           return dbConnector.USPGetTable("dtState", "SP_PinCode_Fill", spara);
       }
       public long DeletePinCode(string Srls)
       {
           SqlParameter[] spara = new SqlParameter[2];
           spara[0] = new SqlParameter("@Srls", SqlDbType.NVarChar);
           spara[0].Value = Srls;

           spara[1] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
           spara[1].Direction = ParameterDirection.Output;
           spara[1].Value = -1;

           return dbConnector.ExecuteProcedureWithOutParameter("SP_PinCode_Delete", spara, 1);
       }
       public bool isPinCodeExist(long Srl, long PinCode)
       {
           SqlParameter[] spara = new SqlParameter[3];
           spara[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
           spara[0].Value = Srl;

           spara[1] = new SqlParameter("@PinCode", SqlDbType.BigInt);
           spara[1].Value = PinCode;

           spara[2] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
           spara[2].Direction = ParameterDirection.Output;
           spara[2].Value = -1;

           return dbConnector.ExecuteProcedureWithOutParameter("SP_PinCode_isDuplicate", spara, 2) > 0;
       }
    }
}
