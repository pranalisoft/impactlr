﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;
using Logger;
namespace BusinessObjects.DAO
{
    public class LRDAO
    {
        DBLink dbconnector;
        LogWriter logger;
        public LRDAO()
        {
            dbconnector = new DBLink();
        }       

        public DataTable FillLRGrid(string searchText, long branchId, string FromDate, string ToDate, bool IsPending)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            spara[4] = new SqlParameter("@IsPending", SqlDbType.Bit);
            spara[4].Value = IsPending;

            return dbconnector.USPGetTable("LR_Fill", "SP_LR_FillGrid", spara);
        }        

        public DataTable FillLRPreviousDetails(long BranchId, long CreatedBy)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            spara[1] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[1].Value = CreatedBy;

            return dbconnector.USPGetTable("LR_FillPreviuosDetails", "SP_LR_PreviuosDetails", spara);
        }

        public long InsertUpdateLRRecord(long ID, string LRNO, DateTime LRDateTime, string Consigner, string Consignee, string Origin, string Destination, string vehicleNo, decimal weight, decimal chargeableweight, long totalPackage, string InvoiceNo, decimal InvoiceValue, string LoadType, string Remarks, long CreatedBy, string Status, long BranchID, string DriverDtls, long DeviceID, string RefNo, long SupplierID, long SubSupplierID, long CustomerID, decimal AdvanceToSupplier, decimal TotalFrieght, long VehicleTypeID, decimal SupplierAmount, long BillingCompany_Srl, decimal Rate, string ConsignorAddress, string ConsigneeAddress, string ConsigneeEmail, string ConsigneeContact, string VehicleSealNo, string PackingType, string VehiclePlacementDate, int ExpectedDeliveryDays, string DriverNumber, string FreightType, string ConsignorGSTNo, string ConsigneeGSTNo, string RefNo_Invoice, string InvoiceDate, decimal CashAdvance, decimal FuelAdvance, string EWayBillNo, decimal AdvancePer, bool Multipoint, string MainLRNo, string ShipmentDocNo, string InternalBillingDocNo, decimal FreightIncentive, decimal SupplierFreightIncentive, long PlacementId, int PetrolPumpId, int PlyQty, string ConsignorEmail, string ConsignorTelNo, string ConsignorPincode, string ConsigneePincode, string EwayBillDate)
        {
            SqlParameter[] spara = new SqlParameter[63];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[1].Value = LRNO;

            spara[2] = new SqlParameter("@LRDateTime", SqlDbType.SmallDateTime);
            spara[2].Value = LRDateTime;

            spara[3] = new SqlParameter("@Consigner", SqlDbType.VarChar);
            spara[3].Value = Consigner;

            spara[4] = new SqlParameter("@Consignee", SqlDbType.VarChar);
            spara[4].Value = Consignee;

            spara[5] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            spara[5].Value = Origin;

            spara[6] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            spara[6].Value = Destination;

            spara[7] = new SqlParameter("@VehicleNo", SqlDbType.VarChar);
            spara[7].Value = vehicleNo;

            spara[8] = new SqlParameter("@Weight", SqlDbType.Decimal);
            spara[8].Value = weight;

            spara[9] = new SqlParameter("@ChargeableWeight", SqlDbType.Decimal);
            spara[9].Value = chargeableweight;

            spara[10] = new SqlParameter("@TotPackages", SqlDbType.BigInt);
            spara[10].Value = totalPackage;

            spara[11] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            spara[11].Value = InvoiceNo;

            spara[12] = new SqlParameter("@InvoiceValue", SqlDbType.Decimal);
            spara[12].Value = InvoiceValue;

            spara[13] = new SqlParameter("@LoadType", SqlDbType.Char);
            spara[13].Value = LoadType;

            spara[14] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[14].Value = Remarks;

            spara[15] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[15].Value = CreatedBy;

            spara[16] = new SqlParameter("@Status", SqlDbType.Char);
            spara[16].Value = Status;

            spara[17] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[17].Value = BranchID;

            spara[18] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[18].Direction = ParameterDirection.Output;
            spara[18].Size = 9;
            spara[18].Value = -1;

            spara[19] = new SqlParameter("@DriverDetails", SqlDbType.VarChar);
            spara[19].Value = DriverDtls;

            spara[20] = new SqlParameter("@DeviceID", SqlDbType.BigInt);
            if (DeviceID > 0)
                spara[20].Value = DeviceID;
            else
                spara[20].Value = DBNull.Value;

            spara[21] = new SqlParameter("@RefNo", SqlDbType.VarChar);
            spara[21].Value = RefNo;

            spara[22] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            if (SupplierID > 0)
                spara[22].Value = SupplierID;
            else
                spara[22].Value = DBNull.Value;

            spara[23] = new SqlParameter("@SubSupplierID", SqlDbType.BigInt);
            if (SubSupplierID > 0)
                spara[23].Value = SubSupplierID;
            else
                spara[23].Value = DBNull.Value;

            spara[24] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            if (CustomerID > 0)
                spara[24].Value = CustomerID;
            else
                spara[24].Value = DBNull.Value;

            spara[25] = new SqlParameter("@AdvanceToSupplier", SqlDbType.Decimal);
            spara[25].Value = AdvanceToSupplier;

            spara[26] = new SqlParameter("@TotalFrieght", SqlDbType.Decimal);
            spara[26].Value = TotalFrieght;

            spara[27] = new SqlParameter("@VehicleTypeID", SqlDbType.Decimal);
            if (VehicleTypeID > 0)
                spara[27].Value = VehicleTypeID;
            else
                spara[27].Value = DBNull.Value;

            spara[28] = new SqlParameter("@SupplierAmount", SqlDbType.Decimal);
            spara[28].Value = SupplierAmount;

            spara[29] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[29].Value = BillingCompany_Srl;

            spara[30] = new SqlParameter("@Rate", SqlDbType.Decimal);
            spara[30].Value = Rate;

            spara[31] = new SqlParameter("@ConsignorAddress", SqlDbType.NVarChar);
            spara[31].Value = ConsignorAddress;

            spara[32] = new SqlParameter("@ConsigneeAddress", SqlDbType.NVarChar);
            spara[32].Value = ConsigneeAddress;

            spara[33] = new SqlParameter("@ConsigneeEmailId", SqlDbType.NVarChar);
            spara[33].Value = ConsigneeEmail;

            spara[34] = new SqlParameter("@ConsigneeContact", SqlDbType.NVarChar);
            spara[34].Value = ConsigneeContact;

            spara[35] = new SqlParameter("@VehicleSealNo", SqlDbType.NVarChar);
            spara[35].Value = VehicleSealNo;

            spara[36] = new SqlParameter("@PackingType", SqlDbType.NVarChar);
            spara[36].Value = PackingType;

            spara[37] = new SqlParameter("@VehiclePlacementDate", SqlDbType.VarChar);
            spara[37].Value = VehiclePlacementDate;

            spara[38] = new SqlParameter("@DriverNumber", SqlDbType.NVarChar);
            spara[38].Value = DriverNumber;

            spara[39] = new SqlParameter("@ExpectedDeliveryDays", SqlDbType.Int);
            spara[39].Value = ExpectedDeliveryDays;

            spara[40] = new SqlParameter("@FreightType", SqlDbType.NVarChar);
            spara[40].Value = FreightType;

            spara[41] = new SqlParameter("@ConsignorGSTNo", SqlDbType.NVarChar);
            spara[41].Value = ConsignorGSTNo;

            spara[42] = new SqlParameter("@ConsigneeGSTNo", SqlDbType.NVarChar);
            spara[42].Value = ConsigneeGSTNo;

            spara[43] = new SqlParameter("@RefNo_Invoice", SqlDbType.NVarChar);
            spara[43].Value = RefNo_Invoice;

            spara[44] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            if (InvoiceDate!=string.Empty)
                spara[44].Value = InvoiceDate;
            else
                spara[44].Value = DBNull.Value;

            spara[45] = new SqlParameter("@CashAdvance", SqlDbType.Decimal);
            spara[45].Value = CashAdvance;

            spara[46] = new SqlParameter("@FuelAdvance", SqlDbType.Decimal);
            spara[46].Value = FuelAdvance;

            spara[47] = new SqlParameter("@EWayBillNo", SqlDbType.NVarChar);
            spara[47].Value = EWayBillNo;

            spara[48] = new SqlParameter("@AdvancePer", SqlDbType.Decimal);
            spara[48].Value = AdvancePer;

            spara[49] = new SqlParameter("@Multipoint", SqlDbType.Bit);
            spara[49].Value = Multipoint;

            spara[50] = new SqlParameter("@MainLRNo", SqlDbType.NVarChar);
            spara[50].Value = MainLRNo;

            spara[51] = new SqlParameter("@ShipmentDocNo", SqlDbType.NVarChar);
            spara[51].Value = ShipmentDocNo;

            spara[52] = new SqlParameter("@InternalBillingDocNo", SqlDbType.NVarChar);
            spara[52].Value = InternalBillingDocNo;

            spara[53] = new SqlParameter("@FreightIncentive", SqlDbType.Decimal);
            spara[53].Value = FreightIncentive;

            spara[54] = new SqlParameter("@SupplierFreightIncentive", SqlDbType.Decimal);
            spara[54].Value = SupplierFreightIncentive;

            spara[55] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[55].Value = PlacementId;

            spara[56] = new SqlParameter("@PetrolPumpId", SqlDbType.Int);
            spara[56].Value = PetrolPumpId;

            spara[57] = new SqlParameter("@PlyQty", SqlDbType.Int);
            spara[57].Value = PlyQty;

            spara[58] = new SqlParameter("@ConsignorEmail", SqlDbType.NVarChar);
            spara[58].Value = ConsignorEmail;

            spara[59] = new SqlParameter("@ConsignorTelNo", SqlDbType.NVarChar);
            spara[59].Value = ConsignorTelNo;

            spara[60] = new SqlParameter("@ConsignorPincode", SqlDbType.NVarChar);
            spara[60].Value = ConsignorPincode;

            spara[61] = new SqlParameter("@ConsigneePincode", SqlDbType.NVarChar);
            spara[61].Value = ConsigneePincode;

            spara[62] = new SqlParameter("@EwayBillDate", SqlDbType.NVarChar);
            if (EwayBillDate == string.Empty)
                spara[62].Value = DBNull.Value;
            else
                spara[62].Value = EwayBillDate;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_InsertUpdate", spara, 18);
        }

        public string GetLRNo(long LRId)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = LRId;

            spara[1] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 20;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameterStr("SP_LR_GetLRNoById", spara, 1);
        }

        public DataTable FillDropDowns(string FillType)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@FillType", SqlDbType.Char);
            spara[0].Value = FillType;

            return dbconnector.USPGetTable("LR_Fillcombo", "SP_LR_FillCombo", spara);
        }

        public DataTable FillLRComboDelete()
        {
            return dbconnector.USPGetTable("LR_FillcomboDelete", "SP_LR_Fill_ForDelete");
        }

        public DataTable FillLRComboUnloadingPayment()
        {
            return dbconnector.USPGetTable("LR_FillcomboUnloadPay", "SP_LR_Fill_ForUnloadingPayment");
        }

        public long LRDelete(long ID,string CancelledRemarks,long CancelledBy)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;
           
            spara[1] = new SqlParameter("@CancelledRemarks", SqlDbType.NVarChar);
            spara[1].Value = CancelledRemarks;

            spara[2] = new SqlParameter("@CancelledBy", SqlDbType.BigInt);
            spara[2].Value = CancelledBy;

            spara[3] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[3].Direction = ParameterDirection.Output;
            spara[3].Size = 9;
            spara[3].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_Delete", spara, 3);
        }

        public long UpdateApproval(long ID)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;            

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UpdatePendingAdvanceApproval", spara, 1);
        }

        public long LRUpdateBranch(long ID,long BranchID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchID;

            spara[2] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Size = 9;
            spara[2].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UpdateBranch", spara, 2);
        }

        public DataTable LRFreightUpdationCount(long BranchId, long UserId)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            spara[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[1].Value = UserId;

            return dbconnector.USPGetTable("FreightUpdationCount", "SP_LR_FreightUpdationCount", spara);
        }

        public DataTable LRFreightUpdationCount_ShowLR(long BranchId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            return dbconnector.USPGetTable("FreightUpdationCount_ShowLR", "SP_LR_FreightUpdationCount_ShowLR", spara);
        }

        public DataTable LRStatusUpdationCount(long BranchId, long UserId)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            spara[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[1].Value = UserId;

            return dbconnector.USPGetTable("StatusUpdationCount", "SP_LR_StatusUpdationCount", spara);
        }

        public DataTable LRStatusUpdationCount_ShowLR(long BranchId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[0].Value = BranchId;

            return dbconnector.USPGetTable("StatusUpdationCount_ShowLR", "SP_LR_StatusUpdationCount_ShowLR", spara);
        }

        public DataTable FillDropDownDevice(long DeviceID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@DeviceID", SqlDbType.BigInt);
            spara[0].Value = DeviceID;

            return dbconnector.USPGetTable("LR_Fillcombo_Device", "SP_LR_FillCombo_Device",spara);
        }

        public long Devise_Used(long DeviceID)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@DeviceID", SqlDbType.BigInt);
            spara[0].Value = DeviceID;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_Device_Used", spara, 1);
        }

        public DataTable FillStatusForLR(long ID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            return dbconnector.USPGetTable("LR_FillStatus", "SP_LR_FillStatus", spara);
        }

        public DataTable GetLRHeaderByNo(string LRNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[0].Value = LRNo;

            return dbconnector.USPGetTable("LRHeader", "SP_LR_HeaderByNo", spara);
        }

        public DataTable GetLRHeaderById(long LRId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = LRId;

            return dbconnector.USPGetTable("LRHeader", "SP_LR_HeaderById", spara);
        }

        public DataTable GetLRDetailsByNo(string LRNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[0].Value = LRNo;

            return dbconnector.USPGetTable("LRDetails", "SP_LR_DetailsByNo", spara);
        }

        public long InsertLRStatus(long ID, DateTime TranDateTime, string Status, string Remarks, long CreatedBy,long DetailID)
        {
            SqlParameter[] spara = new SqlParameter[7];
            spara[0] = new SqlParameter("@HeaderID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@TranDateTime", SqlDbType.SmallDateTime);
            spara[1].Value = TranDateTime;

            spara[2] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[2].Value = Remarks;

            spara[3] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[3].Value = CreatedBy;

            spara[4] = new SqlParameter("@Status", SqlDbType.Char);
            spara[4].Value = Status;

            spara[5] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[5].Direction = ParameterDirection.Output;
            spara[5].Size = 9;
            spara[5].Value = -1;

            spara[6] = new SqlParameter("@DetailID", SqlDbType.BigInt);
            spara[6].Value = DetailID;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LRStatus_Insert", spara, 5);
        }

        public long LRDuplicateCount(string LRNo, long ID)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[0].Value = LRNo;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            spara[2] = new SqlParameter("@LRID", SqlDbType.BigInt);
            spara[2].Value = ID;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_DuplicateCount", spara, 1);

        }

        public DataTable LRDetailReport(long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[3];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = branchId;

            spara[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[1].Value = FromDate;

            spara[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[2].Value = ToDate;


            return dbconnector.USPGetTable("LR_DetailReport", "SP_LR_ReportDetails", spara);
        }

        public DataTable LRSummaryReport(long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[3];

            spara[0] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[0].Value = branchId;

            spara[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[1].Value = FromDate;

            spara[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[2].Value = ToDate;


            return dbconnector.USPGetTable("LR_SummaryReport", "SP_LR_ReportSummary", spara);
        }        

        public DataTable PendingPODReport(long UserId)
        {
            SqlParameter[] spara = new SqlParameter[1];

            spara[0] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[0].Value = UserId;

            return dbconnector.USPGetTable("PendingPODReport", "SP_LR_PendingPOD", spara);
        }

        public long UpdatePODDetails(long ID, string PODDate, long CreatedBy, string PODFile)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@PODDate", SqlDbType.VarChar);
            spara[1].Value = PODDate;

            spara[2] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[2].Value = CreatedBy;

            spara[3] = new SqlParameter("@PODFile", SqlDbType.VarChar);
            if (PODFile == "")
            {
                spara[3].Value = DBNull.Value;
            }
            else
            {
                spara[3].Value = PODFile;
            }

            spara[4] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[4].Direction = ParameterDirection.Output;
            spara[4].Size = 9;
            spara[4].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_POD_Update", spara, 4);
        }

        public DataTable FillPackingType(string SearchText)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = SearchText;

            return dbconnector.USPGetTable("dtPackingType", "SP_PackingType_Fill", spara);
        }

        public DataTable FillLRDescriptionCombo(string SearchText)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = SearchText;

            return dbconnector.USPGetTable("dtLRDescription", "SP_LRDescription_Fill", spara);
        }

        public DataTable GetLRNoByCompName(long BillingCompanySrl, long BranchSrl)
        { 
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[0].Value = BillingCompanySrl;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = BranchSrl;

             return dbconnector.USPGetTable("dtLRNo", "SP_LR_NoByCompany", spara);
        }

        public DataTable LRMISReport(string FromDate, string ToDate, long BranchId,long UserId)
        {
            SqlParameter[] spara = new SqlParameter[4];  
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            spara[3] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[3].Value = UserId;

            return dbconnector.USPGetTable("LR_MIS", "SP_Report_MIS", spara);
        }

        public DataTable LRMISConsolidateReport(string FromDate, string ToDate, long BranchId,bool LRWise)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@BranchId", SqlDbType.BigInt);
            spara[2].Value = BranchId;

            spara[3] = new SqlParameter("@LRWise", SqlDbType.Bit);
            spara[3].Value = LRWise;

            return dbconnector.USPGetTable("LR_MISConsolidate", "SP_Report_MIS_Consolidate", spara);
        }

        public DataTable GetSupplierDetailsById(long SupplierId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            spara[0].Value = SupplierId;

            return dbconnector.USPGetTable("LR_GetSupplierDetailsByID", "SP_GetSupplierDetailsByID", spara);
        }

        public DataTable GetPlacementDetails(long placementId)
        {
             
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;

            return dbconnector.USPGetTable("PlacementDetails", "uspTDA_GetPlacementDetails", spara);
        }

        public DataTable GetPlacementDetailsView(long placementId,string Status)
        {

            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;

            spara[1] = new SqlParameter("@Status", SqlDbType.Char);
            spara[1].Value = Status;

            return dbconnector.USPGetTable("PlacementDetails", "uspTDA_GetPlacementDetails", spara);
        }

        public long GetCntNonZeroLRs(long LRId,long placementId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = LRId;

            spara[1] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[1].Value = placementId;

            spara[2] = new SqlParameter("@Cnt", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Size = 9;
            spara[2].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_CntNonZeroLR", spara, 2);
        }

        public long UpdateFulfillment(long placementId)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;


            spara[1] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_FulfillPlacement", spara, 1);
        }
        public long UpdateLRAmounts(long ID, decimal CashAdvance, decimal FuelAdvance, decimal TotalAdvance, decimal TotalFreight, decimal SupplierFreight, long UpdatedBy,string Remarks)
        {
            SqlParameter[] spara = new SqlParameter[9];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@CashAdvance", SqlDbType.Decimal);
            spara[1].Value = CashAdvance;

            spara[2] = new SqlParameter("@FuelAdvance", SqlDbType.Decimal);
            spara[2].Value = FuelAdvance;

            spara[3] = new SqlParameter("@TotalAdvance", SqlDbType.Decimal);
            spara[3].Value = TotalAdvance;

            spara[4] = new SqlParameter("@TotalFreight", SqlDbType.Decimal);
            spara[4].Value = TotalFreight;

            spara[5] = new SqlParameter("@SupplierFreight", SqlDbType.Decimal);
            spara[5].Value = SupplierFreight;

            spara[6] = new SqlParameter("@UpdatedBy", SqlDbType.BigInt);
            spara[6].Value = UpdatedBy;

            spara[7] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[7].Value = Remarks;

            spara[8] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[8].Direction = ParameterDirection.Output;
            spara[8].Size = 9;
            spara[8].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UpdateAmounts", spara, 8);
        }

        public long UpdateUnloadingCharges(long LRId, decimal UnloadingCharges)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = LRId;

            spara[1] = new SqlParameter("@UnloadingCharges", SqlDbType.Decimal);
            spara[1].Value = UnloadingCharges;

            spara[2] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Size = 9;
            spara[2].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UpdateUnloadingCharges", spara, 2);
        }

        public DataTable GetLRHeaderByNoForUnloading(string LRNo)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[0].Value = LRNo;

            return dbconnector.USPGetTable("LRHeader", "SP_LR_HeaderByNoForUnloading", spara);
        }

        public DataTable GetLRNosForPlacementId(long placementId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;

            return dbconnector.USPGetTable("LRNosForPlacement", "SP_LR_GetLRNosForPlacementId", spara);
        }

        public long InsertFuelSlip(long FuelSlipId,long ID, decimal CashAdvance, decimal FuelAdvance,long CreatedBy,decimal TdFuelAdvance,int PetrolPumpId)
        {
            SqlParameter[] spara = new SqlParameter[8];
            spara[0] = new SqlParameter("@FuelSlipId", SqlDbType.BigInt);
            spara[0].Value = FuelSlipId;

            spara[1] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[1].Value = ID;

            spara[2] = new SqlParameter("@FuelAdvanceCash", SqlDbType.Decimal);
            spara[2].Value = CashAdvance;

            spara[3] = new SqlParameter("@FuelAdvanceFuel", SqlDbType.Decimal);
            spara[3].Value = FuelAdvance;

            spara[4] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[4].Direction = ParameterDirection.Output;
            spara[4].Size = 9;
            spara[4].Value = -1;

            spara[5] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[5].Value = CreatedBy;

            spara[6] = new SqlParameter("@TDFuelAdvance", SqlDbType.Decimal);
            spara[6].Value = TdFuelAdvance;

            spara[7] = new SqlParameter("@PetrolPumpId", SqlDbType.BigInt);
            spara[7].Value = PetrolPumpId;

            

            return dbconnector.ExecuteProcedureWithOutParameter("SP_FuelSlip_Insert", spara, 4);

        }


        public DataTable GetOrgDestCordinates(int OrgPincode, int DestPincode)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@OrgPinCode", SqlDbType.Int);
            spara[0].Value = OrgPincode;

            spara[1] = new SqlParameter("@DestPinCode", SqlDbType.Int);
            spara[1].Value = DestPincode;

            return dbconnector.USPGetTable("dt_getcordinates", "SP_GetCoordinates",spara);
        }

        public long UpdateTripId(long ID, long TripId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@TripId", SqlDbType.BigInt);
            spara[1].Value = TripId;

            spara[2] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Size = 9;
            spara[2].Value = -1;


            return dbconnector.ExecuteProcedureWithOutParameter("SP_UpdateFreightTigerTripIdInLR",spara,2);
        }

        public long UpdateTripIdForNonTrips(long ID, long TripId)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@LRId", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@TripId", SqlDbType.BigInt);
            spara[1].Value = TripId;

            spara[2] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[2].Direction = ParameterDirection.Output;
            spara[2].Size = 9;
            spara[2].Value = -1;


            return dbconnector.ExecuteProcedureWithOutParameter("SP_UpdateTripIdForNonTripIds", spara, 2);
        }

        

        public DataTable GetTripsForUpdation()
        {
            return dbconnector.USPGetTable("dtPendingTripsforUpdation", "SP_GetPendingTripsForUpdation");
        }

        public long UpdateStatusFromFreightTiger(string itemXML,long CreatedBy)
        {
            SqlParameter[] spara = new SqlParameter[3];
            spara[0] = new SqlParameter("@ItemXML", SqlDbType.Xml);
            spara[0].Value = itemXML;

            spara[1] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            spara[2] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[2].Value = CreatedBy;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_UpdateLRStatusFromFreightTiger", spara, 1);
        }

        public DataTable FillLRGridBulkStatusUpdate()
        {
            return dbconnector.USPGetTable("LR_FillBulkStatusUpdate", "SP_LR_FillGrid_ForBulkStatusUpdate");
        }

        public DataTable PODPerformanceReport(string FromDate, string ToDate,string ReportType,long UserId)
        {
            SqlParameter[] spara = new SqlParameter[4];

            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            spara[2] = new SqlParameter("@ReportType", SqlDbType.VarChar);
            spara[2].Value = ReportType;

            spara[3] = new SqlParameter("@UserId", SqlDbType.BigInt);
            spara[3].Value = UserId;

            return dbconnector.USPGetTable("PODPerformanceReport", "SP_Report_POD_Performance", spara);
        }



        public long UpdatePODDetails(long ID, string PODDate, long CreatedBy, string PODFile, int PODReq, int PODReasonId, decimal IdemnityAmt)
        {
            SqlParameter[] spara = new SqlParameter[8];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@PODDate", SqlDbType.VarChar);
            spara[1].Value = PODDate;

            spara[2] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[2].Value = CreatedBy;

            spara[3] = new SqlParameter("@PODFile", SqlDbType.VarChar);
            if (PODFile == "")
            {
                spara[3].Value = DBNull.Value;
            }
            else
            {
                spara[3].Value = PODFile;
            }

            spara[4] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[4].Direction = ParameterDirection.Output;
            spara[4].Size = 9;
            spara[4].Value = -1;

            spara[5] = new SqlParameter("@PODReq", SqlDbType.Int);
            spara[5].Value = PODReq;

            spara[6] = new SqlParameter("@PODReasonId", SqlDbType.Int);
            spara[6].Value = PODReasonId;

            spara[7] = new SqlParameter("@IdemnityAmt", SqlDbType.Decimal);
            spara[7].Value = IdemnityAmt;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_POD_Update", spara, 4);
        }

        public DataSet ShowLREmailDetails(long placementId)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;

            return dbconnector.USPGetSet("dsLrEmail", "SP_LREmailDetails", spara);
        }

        public long UpdateEmailCnt(long placementId)
        {
            SqlParameter[] spara = new SqlParameter[2];
            spara[0] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[0].Value = placementId;

            spara[1] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[1].Direction = ParameterDirection.Output;
            spara[1].Size = 9;
            spara[1].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_UpdateEmailCount", spara, 1);
        }

        public long UpdatePODPhysicalCopyData(string PODDate, long ID, string Location, long CreatedBy)
        {
            SqlParameter[] spara = new SqlParameter[5];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@PODDate", SqlDbType.SmallDateTime);
            spara[1].Value = PODDate;

            spara[2] = new SqlParameter("@Location", SqlDbType.NVarChar);
            spara[2].Value = Location;

            spara[3] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[3].Value = CreatedBy;

            spara[4] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[4].Direction = ParameterDirection.Output;
            spara[4].Size = 9;
            spara[4].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_UpdatePhysicalPOD", spara, 4);
        }

        public DataTable PendingLRforBillingWithPhysicalPOD()
        {
            return dbconnector.USPGetTable("dtpending", "SP_Report_PendingLRforbillingWithPhysicalPOD");
        }

        #region PTL
        public DataTable FillLRGrid(string searchText, long branchId, string FromDate, string ToDate, bool IsPending, bool IsPTL)
        {
            SqlParameter[] spara = new SqlParameter[6];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            spara[4] = new SqlParameter("@IsPending", SqlDbType.Bit);
            spara[4].Value = IsPending;

            spara[5] = new SqlParameter("@IsPTL", SqlDbType.Bit);
            spara[5].Value = IsPTL;

            return dbconnector.USPGetTable("LR_Fill", "SP_LR_FillGrid", spara);
        }

        public DataTable FillLRGridMultiple(string searchText, long branchId, string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            spara[0].Value = searchText;

            spara[1] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[1].Value = branchId;

            spara[2] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[2].Value = FromDate;

            spara[3] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[3].Value = ToDate;

            return dbconnector.USPGetTable("LR_FillPTLMultiple", "SP_LR_FillGridPTLMultiple", spara);
        }

        public DataSet FillStatusAndItemForLR(long ID)
        {
            SqlParameter[] spara = new SqlParameter[1];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            return dbconnector.USPGetSet("DocumentElement", "SP_LR_FillStatusAndItem", spara);
        }

        public DataTable FillItem(long CustomerID, long VehicleTypeID,string FromLoc,string ToLoc)
        {
            SqlParameter[] spara = new SqlParameter[4];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@VehicleTypeID", SqlDbType.BigInt);
            spara[1].Value = VehicleTypeID;

            spara[2] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            if (FromLoc=="")
                spara[2].Value = DBNull.Value;
            else
                spara[2].Value = FromLoc;

            spara[3] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            if (ToLoc == "")
                spara[3].Value = DBNull.Value;
            else
                spara[3].Value = ToLoc;

            return dbconnector.USPGetTable("dtItemdd", "SP_LR_FillItem", spara);
        }

        public DataTable FillItemMultiple(long CustomerID, long VehicleTypeID, string FromLoc, string ToLoc)
        {
            SqlParameter[] spara = new SqlParameter[4];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@VehicleTypeID", SqlDbType.BigInt);
            spara[1].Value = VehicleTypeID;

            spara[2] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            if (FromLoc == "")
                spara[2].Value = DBNull.Value;
            else
                spara[2].Value = FromLoc;

            spara[3] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            if (ToLoc == "")
                spara[3].Value = DBNull.Value;
            else
                spara[3].Value = ToLoc;

            return dbconnector.USPGetTable("dtItemdd", "SP_LR_FillItem_Multiple", spara);
        }

        public DataTable FillItemZoneType(long CustomerID, long VehicleTypeID, string Item, string FromLoc, string ToLoc)
        {
            SqlParameter[] spara = new SqlParameter[5];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@VehicleTypeID", SqlDbType.BigInt);
            spara[1].Value = VehicleTypeID;

            spara[2] = new SqlParameter("@Item", SqlDbType.VarChar);
            spara[2].Value = Item;

            spara[3] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            if (FromLoc == "")
                spara[3].Value = DBNull.Value;
            else
                spara[3].Value = FromLoc;

            spara[4] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            if (ToLoc == "")
                spara[4].Value = DBNull.Value;
            else
                spara[4].Value = ToLoc;

            return dbconnector.USPGetTable("dtItemZTdd", "SP_LR_FillItemZoneType", spara);
        }

        public DataTable FillItemZoneTypeSlab(long CustomerID, long VehicleTypeID, string Item, long ZoneTypeId, string FromLoc, string ToLoc)
        {
            SqlParameter[] spara = new SqlParameter[6];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@VehicleTypeID", SqlDbType.BigInt);
            spara[1].Value = VehicleTypeID;

            spara[2] = new SqlParameter("@Item", SqlDbType.VarChar);
            spara[2].Value = Item;

            spara[3] = new SqlParameter("@ZoneTypeId", SqlDbType.BigInt);
            spara[3].Value = ZoneTypeId;

            spara[4] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            if (FromLoc == "")
                spara[4].Value = DBNull.Value;
            else
                spara[4].Value = FromLoc;

            spara[5] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            if (ToLoc == "")
                spara[5].Value = DBNull.Value;
            else
                spara[5].Value = ToLoc;

            return dbconnector.USPGetTable("dtItemZTSdd", "SP_LR_FillItemZoneTypeSlab", spara);
        }

        public DataTable FillItemDetails(long CustomerID, long VehicleTypeID, string Item, long ZoneTypeId, string Slab, string FromLoc, string ToLoc)
        {
            SqlParameter[] spara = new SqlParameter[7];

            spara[0] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            spara[0].Value = CustomerID;

            spara[1] = new SqlParameter("@VehicleTypeID", SqlDbType.BigInt);
            spara[1].Value = VehicleTypeID;

            spara[2] = new SqlParameter("@Item", SqlDbType.VarChar);
            spara[2].Value = Item;

            spara[3] = new SqlParameter("@ZoneTypeId", SqlDbType.BigInt);
            spara[3].Value = ZoneTypeId;

            spara[4] = new SqlParameter("@Slab", SqlDbType.NVarChar);
            spara[4].Value = Slab;

            spara[5] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            if (FromLoc == "")
                spara[5].Value = DBNull.Value;
            else
                spara[5].Value = FromLoc;

            spara[6] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            if (ToLoc == "")
                spara[6].Value = DBNull.Value;
            else
                spara[6].Value = ToLoc;

            return dbconnector.USPGetTable("dtItemDetails", "SP_LR_FillItemDetails", spara);
        }

        public long InsertUpdateLRRecord(long ID, string LRNO, DateTime LRDateTime, string Consigner, string Consignee, string Origin, string Destination, string vehicleNo, decimal weight, decimal chargeableweight, long totalPackage, string InvoiceNo, decimal InvoiceValue, string LoadType, string Remarks, long CreatedBy, string Status, long BranchID, string DriverDtls, long DeviceID, string RefNo, long SupplierID, long SubSupplierID, long CustomerID, decimal AdvanceToSupplier, decimal TotalFrieght, long VehicleTypeID, decimal SupplierAmount, long BillingCompany_Srl, decimal Rate, string ConsignorAddress, string ConsigneeAddress, string ConsigneeEmail, string ConsigneeContact, string VehicleSealNo, string PackingType, string VehiclePlacementDate, int ExpectedDeliveryDays, string DriverNumber, string FreightType, string ConsignorGSTNo, string ConsigneeGSTNo, string RefNo_Invoice, string InvoiceDate, decimal CashAdvance, decimal FuelAdvance, string EWayBillNo, decimal AdvancePer, bool Multipoint, string MainLRNo, string ShipmentDocNo, string InternalBillingDocNo, decimal FreightIncentive, decimal SupplierFreightIncentive, long PlacementId, int PetrolPumpId, int PlyQty, string ConsignorEmail, string ConsignorTelNo, string ConsignorPincode, string ConsigneePincode, string EwayBillDate,string ItemXml)
        {
            SqlParameter[] spara = new SqlParameter[64];
            spara[0] = new SqlParameter("@ID", SqlDbType.BigInt);
            spara[0].Value = ID;

            spara[1] = new SqlParameter("@LRNo", SqlDbType.VarChar);
            spara[1].Value = LRNO;

            spara[2] = new SqlParameter("@LRDateTime", SqlDbType.SmallDateTime);
            spara[2].Value = LRDateTime;

            spara[3] = new SqlParameter("@Consigner", SqlDbType.VarChar);
            spara[3].Value = Consigner;

            spara[4] = new SqlParameter("@Consignee", SqlDbType.VarChar);
            spara[4].Value = Consignee;

            spara[5] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            spara[5].Value = Origin;

            spara[6] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            spara[6].Value = Destination;

            spara[7] = new SqlParameter("@VehicleNo", SqlDbType.VarChar);
            spara[7].Value = vehicleNo;

            spara[8] = new SqlParameter("@Weight", SqlDbType.Decimal);
            spara[8].Value = weight;

            spara[9] = new SqlParameter("@ChargeableWeight", SqlDbType.Decimal);
            spara[9].Value = chargeableweight;

            spara[10] = new SqlParameter("@TotPackages", SqlDbType.BigInt);
            spara[10].Value = totalPackage;

            spara[11] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            spara[11].Value = InvoiceNo;

            spara[12] = new SqlParameter("@InvoiceValue", SqlDbType.Decimal);
            spara[12].Value = InvoiceValue;

            spara[13] = new SqlParameter("@LoadType", SqlDbType.Char);
            spara[13].Value = LoadType;

            spara[14] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            spara[14].Value = Remarks;

            spara[15] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[15].Value = CreatedBy;

            spara[16] = new SqlParameter("@Status", SqlDbType.Char);
            spara[16].Value = Status;

            spara[17] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[17].Value = BranchID;

            spara[18] = new SqlParameter("@pcnt", SqlDbType.BigInt);
            spara[18].Direction = ParameterDirection.Output;
            spara[18].Size = 9;
            spara[18].Value = -1;

            spara[19] = new SqlParameter("@DriverDetails", SqlDbType.VarChar);
            spara[19].Value = DriverDtls;

            spara[20] = new SqlParameter("@DeviceID", SqlDbType.BigInt);
            if (DeviceID > 0)
                spara[20].Value = DeviceID;
            else
                spara[20].Value = DBNull.Value;

            spara[21] = new SqlParameter("@RefNo", SqlDbType.VarChar);
            spara[21].Value = RefNo;

            spara[22] = new SqlParameter("@SupplierID", SqlDbType.BigInt);
            if (SupplierID > 0)
                spara[22].Value = SupplierID;
            else
                spara[22].Value = DBNull.Value;

            spara[23] = new SqlParameter("@SubSupplierID", SqlDbType.BigInt);
            if (SubSupplierID > 0)
                spara[23].Value = SubSupplierID;
            else
                spara[23].Value = DBNull.Value;

            spara[24] = new SqlParameter("@CustomerID", SqlDbType.BigInt);
            if (CustomerID > 0)
                spara[24].Value = CustomerID;
            else
                spara[24].Value = DBNull.Value;

            spara[25] = new SqlParameter("@AdvanceToSupplier", SqlDbType.Decimal);
            spara[25].Value = AdvanceToSupplier;

            spara[26] = new SqlParameter("@TotalFrieght", SqlDbType.Decimal);
            spara[26].Value = TotalFrieght;

            spara[27] = new SqlParameter("@VehicleTypeID", SqlDbType.Decimal);
            if (VehicleTypeID > 0)
                spara[27].Value = VehicleTypeID;
            else
                spara[27].Value = DBNull.Value;

            spara[28] = new SqlParameter("@SupplierAmount", SqlDbType.Decimal);
            spara[28].Value = SupplierAmount;

            spara[29] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[29].Value = BillingCompany_Srl;

            spara[30] = new SqlParameter("@Rate", SqlDbType.Decimal);
            spara[30].Value = Rate;

            spara[31] = new SqlParameter("@ConsignorAddress", SqlDbType.NVarChar);
            spara[31].Value = ConsignorAddress;

            spara[32] = new SqlParameter("@ConsigneeAddress", SqlDbType.NVarChar);
            spara[32].Value = ConsigneeAddress;

            spara[33] = new SqlParameter("@ConsigneeEmailId", SqlDbType.NVarChar);
            spara[33].Value = ConsigneeEmail;

            spara[34] = new SqlParameter("@ConsigneeContact", SqlDbType.NVarChar);
            spara[34].Value = ConsigneeContact;

            spara[35] = new SqlParameter("@VehicleSealNo", SqlDbType.NVarChar);
            spara[35].Value = VehicleSealNo;

            spara[36] = new SqlParameter("@PackingType", SqlDbType.NVarChar);
            spara[36].Value = PackingType;

            spara[37] = new SqlParameter("@VehiclePlacementDate", SqlDbType.VarChar);
            spara[37].Value = VehiclePlacementDate;

            spara[38] = new SqlParameter("@DriverNumber", SqlDbType.NVarChar);
            spara[38].Value = DriverNumber;

            spara[39] = new SqlParameter("@ExpectedDeliveryDays", SqlDbType.Int);
            spara[39].Value = ExpectedDeliveryDays;

            spara[40] = new SqlParameter("@FreightType", SqlDbType.NVarChar);
            spara[40].Value = FreightType;

            spara[41] = new SqlParameter("@ConsignorGSTNo", SqlDbType.NVarChar);
            spara[41].Value = ConsignorGSTNo;

            spara[42] = new SqlParameter("@ConsigneeGSTNo", SqlDbType.NVarChar);
            spara[42].Value = ConsigneeGSTNo;

            spara[43] = new SqlParameter("@RefNo_Invoice", SqlDbType.NVarChar);
            spara[43].Value = RefNo_Invoice;

            spara[44] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            if (InvoiceDate != string.Empty)
                spara[44].Value = InvoiceDate;
            else
                spara[44].Value = DBNull.Value;

            spara[45] = new SqlParameter("@CashAdvance", SqlDbType.Decimal);
            spara[45].Value = CashAdvance;

            spara[46] = new SqlParameter("@FuelAdvance", SqlDbType.Decimal);
            spara[46].Value = FuelAdvance;

            spara[47] = new SqlParameter("@EWayBillNo", SqlDbType.NVarChar);
            spara[47].Value = EWayBillNo;

            spara[48] = new SqlParameter("@AdvancePer", SqlDbType.Decimal);
            spara[48].Value = AdvancePer;

            spara[49] = new SqlParameter("@Multipoint", SqlDbType.Bit);
            spara[49].Value = Multipoint;

            spara[50] = new SqlParameter("@MainLRNo", SqlDbType.NVarChar);
            spara[50].Value = MainLRNo;

            spara[51] = new SqlParameter("@ShipmentDocNo", SqlDbType.NVarChar);
            spara[51].Value = ShipmentDocNo;

            spara[52] = new SqlParameter("@InternalBillingDocNo", SqlDbType.NVarChar);
            spara[52].Value = InternalBillingDocNo;

            spara[53] = new SqlParameter("@FreightIncentive", SqlDbType.Decimal);
            spara[53].Value = FreightIncentive;

            spara[54] = new SqlParameter("@SupplierFreightIncentive", SqlDbType.Decimal);
            spara[54].Value = SupplierFreightIncentive;

            spara[55] = new SqlParameter("@PlacementId", SqlDbType.BigInt);
            spara[55].Value = PlacementId;

            spara[56] = new SqlParameter("@PetrolPumpId", SqlDbType.Int);
            spara[56].Value = PetrolPumpId;

            spara[57] = new SqlParameter("@PlyQty", SqlDbType.Int);
            spara[57].Value = PlyQty;

            spara[58] = new SqlParameter("@ConsignorEmail", SqlDbType.NVarChar);
            spara[58].Value = ConsignorEmail;

            spara[59] = new SqlParameter("@ConsignorTelNo", SqlDbType.NVarChar);
            spara[59].Value = ConsignorTelNo;

            spara[60] = new SqlParameter("@ConsignorPincode", SqlDbType.NVarChar);
            spara[60].Value = ConsignorPincode;

            spara[61] = new SqlParameter("@ConsigneePincode", SqlDbType.NVarChar);
            spara[61].Value = ConsigneePincode;

            spara[62] = new SqlParameter("@EwayBillDate", SqlDbType.NVarChar);
            if (EwayBillDate == string.Empty)
                spara[62].Value = DBNull.Value;
            else
                spara[62].Value = EwayBillDate;

            spara[63] = new SqlParameter("@ItemXml", SqlDbType.Xml);
            spara[63].Value = ItemXml;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_InsertUpdatePTL", spara, 18);
        }

        public DataTable InsertUpdateLRRecordMultiple(DateTime LRDateTime, long CreatedBy, long BranchID, long BillingCompanySrl, string LRXml, string ItemXml)
        {
            SqlParameter[] spara = new SqlParameter[6];
            spara[0] = new SqlParameter("@LRDateTime", SqlDbType.SmallDateTime);
            spara[0].Value = LRDateTime;

            spara[1] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            spara[1].Value = CreatedBy;

            spara[2] = new SqlParameter("@BranchID", SqlDbType.BigInt);
            spara[2].Value = BranchID;

            spara[3] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            spara[3].Value = BillingCompanySrl;

            spara[4] = new SqlParameter("@LRXml", SqlDbType.Xml);
            spara[4].Value = LRXml;

            spara[5] = new SqlParameter("@ItemXml", SqlDbType.Xml);
            spara[5].Value = ItemXml;

            return dbconnector.USPGetTable("dtLRResult", "SP_LR_InsertUpdatePTLMultiple", spara);
        }
        #endregion

        public DataTable FreightTigerSummary(string FromDate, string ToDate)
        {
            SqlParameter[] spara = new SqlParameter[2];

            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate;

            return dbconnector.USPGetTable("FreightTigerReport", "SP_FreightTigerMISReport", spara);
        }

        public DataTable FreightTigerSummaryLinks(string FromDate, string ToDate,string LinkType )
        {
            SqlParameter[] spara = new SqlParameter[3];

            spara[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            spara[0].Value = FromDate;

            spara[1] = new SqlParameter("@ToDate", SqlDbType.VarChar);
            spara[1].Value = ToDate; 

            spara[2] = new SqlParameter("@LinkType", SqlDbType.Char);
            spara[2].Value = LinkType;

            return dbconnector.USPGetTable("FreightTigerReport", "SP_FreightTigerMIS_Link_New", spara);
        }

        public long UploadEwayFile(long Id, string FileName, long UploadedBy)
        {
            SqlParameter[] spara = new SqlParameter[4];
            spara[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            spara[0].Value = Id;

            spara[1] = new SqlParameter("@FileName", SqlDbType.NVarChar);
            spara[1].Value = FileName;

            spara[2] = new SqlParameter("@UploadedBy", SqlDbType.BigInt);
            spara[2].Value = UploadedBy;

            spara[3] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            spara[3].Direction = ParameterDirection.Output;
            spara[3].Size = 9;
            spara[3].Value = -1;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_LR_UploadEwayBillFile", spara, 3);
        }
    }
}
