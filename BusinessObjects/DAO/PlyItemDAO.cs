﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class PlyItemDAO
    {
        DBLink dbconnector;

        public PlyItemDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertPlyItem(long Srl, string Name, bool IsActive, long CreatedBy,long HSNCodeSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[1].Value = Name;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            sqlParams[3] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[3].Value = IsActive;

            sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[4].Value = CreatedBy;

            sqlParams[5] = new SqlParameter("@HSNCodeSrl", SqlDbType.BigInt);
            sqlParams[5].Value = HSNCodeSrl;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_PlyItem_Insert", sqlParams, 2);
        }

        public DataTable GetPlyItems(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetVehicleType", "SP_PlyItem_Fill", sqlParams);
        }

        public bool isPlyItemAlreadyExist(string BranchName, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];

            sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[0].Value = BranchName;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_PlyItem_isDuplicate", sqlParams, 2);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long DeletePlyItems(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_PlyItem_Delete", sqlParams, 1);
        }

        public DataTable GetVehicleTypeWiseQty(long PlyItemSrl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@PlyItemSrl", SqlDbType.BigInt);
            sqlParams[0].Value = PlyItemSrl;

            return dbconnector.USPGetTable("GetVehicleTypeQty", "SP_PlyItem_GetVehicleTypeQty", sqlParams);
        }

        public long InsertPlyQty(long PlyItemSrl,string xml)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@PlyItemSrl", SqlDbType.BigInt);
            sqlParams[0].Value = PlyItemSrl;

            sqlParams[1] = new SqlParameter("@qtyXml", SqlDbType.Xml);
            sqlParams[1].Value = xml;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_PlyItem_InsertVehicleTypeQty", sqlParams, 2);
        }

        public DataTable GetPlyItemHSNGST(long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            return dbconnector.USPGetTable("GetHSNGST", "SP_PlyItem_GetHSGST", sqlParams);
        }
    }
}
