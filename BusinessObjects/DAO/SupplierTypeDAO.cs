﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;

namespace BusinessObjects.DAO
{
    public class SupplierTypeDAO
    {
        DBLink dbconnector;

       public SupplierTypeDAO()
       {
           dbconnector = new DBLink();
       }

       public long InsertSupplierType(long Srl, string Name, bool IsActive, long CreatedBy,string Ind)
       {
           SqlParameter[] sqlParams = new SqlParameter[6];
           sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
           sqlParams[0].Value = Srl;

           sqlParams[1] = new SqlParameter("@Name", SqlDbType.VarChar);
           sqlParams[1].Value = Name;

           sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
           sqlParams[2].Value = -1;
           sqlParams[2].Direction = ParameterDirection.Output;
           sqlParams[2].Size = 9;

           sqlParams[3] = new SqlParameter("@IsActive", SqlDbType.Bit);
           sqlParams[3].Value = IsActive;

           sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
           sqlParams[4].Value = CreatedBy;

           sqlParams[5] = new SqlParameter("@Ind", SqlDbType.Char);
           sqlParams[5].Value = Ind;

           return dbconnector.ExecuteProcedureWithOutParameter("SP_SupplierType_Insert", sqlParams, 2);
       }

       public DataTable GetSupplierTypes(string SearchText, string ShowAll)
       {
           SqlParameter[] sqlParams = new SqlParameter[2];
           sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
           sqlParams[0].Value = SearchText;

           sqlParams[1] = new SqlParameter("@ShowAll", SqlDbType.Char);
           sqlParams[1].Value = ShowAll;



           return dbconnector.USPGetTable("GetSupplierType", "SP_SupplierType_Fill", sqlParams);
       }

       public bool isSupplierTypeAlreadyExist(string Name, long Srl)
       {
           SqlParameter[] sqlParams = new SqlParameter[3];

           sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
           sqlParams[0].Value = Name;

           sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
           sqlParams[1].Value = Srl;

           sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
           sqlParams[2].Value = -1;
           sqlParams[2].Direction = ParameterDirection.Output;
           sqlParams[2].Size = 9;

           long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_SupplierType_isDuplicate", sqlParams, 2);

           if (cnt > 0)
           {
               return true;
           }
           else
           {
               return false;
           }

       }

       public long DeleteSupplierType(string Srls)
       {
           SqlParameter[] sqlParams = new SqlParameter[2];
           sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
           sqlParams[0].Value = Srls;

           sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
           sqlParams[1].Value = -1;
           sqlParams[1].Direction = ParameterDirection.Output;
           sqlParams[1].Size = 9;

           return dbconnector.ExecuteProcedureWithOutParameter("SP_SupplierType_Delete", sqlParams, 1);

       }
    }
}
