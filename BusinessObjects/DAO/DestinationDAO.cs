﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class DestinationDAO
    {
        DBLink dbconnector;

        public DestinationDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertDestination(long Srl, string FromLoc, string ToLoc, int ExpDeliveryDays, bool IsActive, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[7];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[1].Value = FromLoc;

            sqlParams[2] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[2].Value = ToLoc;

            sqlParams[3] = new SqlParameter("@ExpDeliveryDays", SqlDbType.Int);
            sqlParams[3].Value = ExpDeliveryDays;

            sqlParams[4] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[4].Value = -1;
            sqlParams[4].Direction = ParameterDirection.Output;
            sqlParams[4].Size = 9;

            sqlParams[5] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[5].Value = IsActive;


            sqlParams[6] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[6].Value = CreatedBy;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_Insert", sqlParams, 4);
        }

        public long InsertDestinationPTL(long Srl, string FromLoc, string ToLoc, int ExpDeliveryDays, bool IsActive, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[7];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[1].Value = FromLoc;

            sqlParams[2] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[2].Value = ToLoc;

            sqlParams[3] = new SqlParameter("@ExpDeliveryDays", SqlDbType.Int);
            sqlParams[3].Value = ExpDeliveryDays;

            sqlParams[4] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[4].Value = -1;
            sqlParams[4].Direction = ParameterDirection.Output;
            sqlParams[4].Size = 9;

            sqlParams[5] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[5].Value = IsActive;


            sqlParams[6] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[6].Value = CreatedBy;


            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_Insert_PTL", sqlParams, 4);
        }

        public DataTable GetDestinations(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetDestinations", "SP_Destination_Fill", sqlParams);
        }

        public DataTable GetDestinationsPTL(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetDestinations", "SP_Destination_Fill_PTL", sqlParams);
        }

        public DataTable GetFromOrToLocations(string FromOrTo)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@FromOrTo ", SqlDbType.VarChar);
            sqlParams[0].Value = FromOrTo;

            return dbconnector.USPGetTable("GetFromOrToLocations", "SP_Destination_FillFromOrTo", sqlParams);
        }

        public DataTable GetFromOrToLocationsPTL(string FromOrTo)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@FromOrTo ", SqlDbType.VarChar);
            sqlParams[0].Value = FromOrTo;

            return dbconnector.USPGetTable("GetFromOrToLocations", "SP_Destination_FillFromOrTo_PTL", sqlParams);
        }

        public DataTable GetToLocations(string FromLoc)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@FromLoc ", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            return dbconnector.USPGetTable("GetToLocations", "SP_Destination_FillTo", sqlParams);
        }

        public DataTable GetToLocationsPTL(string FromLoc)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@FromLoc ", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            return dbconnector.USPGetTable("GetToLocations", "SP_Destination_FillTo_PTL", sqlParams);
        }

        public long GetDestinationExpDays(string FromLoc, string ToLoc)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            sqlParams[1] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[1].Value = ToLoc;

            sqlParams[2] = new SqlParameter("@ExpDays", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_GetExpDays", sqlParams, 2);
        }

        public long GetDestinationExpDaysPTL(string FromLoc, string ToLoc)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            sqlParams[1] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[1].Value = ToLoc;

            sqlParams[2] = new SqlParameter("@ExpDays", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_GetExpDays_PTL", sqlParams, 2);
        }

        public bool isDestinationAlreadyExist(string FromLoc, string ToLoc, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            sqlParams[1] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[1].Value = ToLoc;

            sqlParams[2] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[2].Value = Srl;

            sqlParams[3] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[3].Value = -1;
            sqlParams[3].Direction = ParameterDirection.Output;
            sqlParams[3].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_isDuplicate", sqlParams, 3);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isDestinationAlreadyExistPTL(string FromLoc, string ToLoc, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@FromLoc", SqlDbType.VarChar);
            sqlParams[0].Value = FromLoc;

            sqlParams[1] = new SqlParameter("@ToLoc", SqlDbType.VarChar);
            sqlParams[1].Value = ToLoc;

            sqlParams[2] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[2].Value = Srl;

            sqlParams[3] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[3].Value = -1;
            sqlParams[3].Direction = ParameterDirection.Output;
            sqlParams[3].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_isDuplicate_PTL", sqlParams, 3);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long DeleteDestinations(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_Delete", sqlParams, 1);
        }

        public long DeleteDestinationsPTL(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Destination_Delete_PTL", sqlParams, 1);
        }
    }
}
