﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data.SqlClient;
using System.Data;

namespace BusinessObjects.DAO
{
    public class DocumentTypeDAO
    {
        DBLink dbconnector;
        public DocumentTypeDAO()
        {

            dbconnector= new DBLink();

        }

        public long InsertUpdateDocumentType(int DocumentTypeId,string DocumentTypeName, bool IsMandatory, bool Active, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@DocumentTypeId", SqlDbType.Int);
            sqlParams[0].Value = DocumentTypeId;

            sqlParams[1] = new SqlParameter("@DocumentTypeName", SqlDbType.NVarChar);
            sqlParams[1].Value = DocumentTypeName;

            sqlParams[2] = new SqlParameter("@IsMandatory", SqlDbType.Bit);
            sqlParams[2].Value = IsMandatory;

            sqlParams[3] = new SqlParameter("@Active", SqlDbType.Bit);
            sqlParams[3].Value = Active;

            sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            sqlParams[4].Value = CreatedBy;

            sqlParams[5] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[5].Value = -1;
            sqlParams[5].Direction = ParameterDirection.Output;
            sqlParams[5].Size = 9;


            return dbconnector.ExecuteProcedureWithOutParameter("SP_DocumentType_InsertUpdate", sqlParams, 5);

        }

        public DataTable ShowData(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("dtDocumentType", "SP_DocumentTypeName_Show", sqlParams);
        }

        public bool isDocumentTypeExist(int DocumentTypeId, string DocumentTypeName)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];

            sqlParams[0] = new SqlParameter("@DocumentTypeName", SqlDbType.NVarChar);
            sqlParams[0].Value = DocumentTypeName;

            sqlParams[1] = new SqlParameter("@DocumentTypeId", SqlDbType.Int);
            sqlParams[1].Value = DocumentTypeId;

            sqlParams[2] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_DocumentTypeName_isDuplicate", sqlParams, 2);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public long InsertDocuments(long DocumentId,int DocumentTypeId,long SupplierId, string FileName,string Remarks,bool Active,long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@DocumentId", SqlDbType.BigInt);
            sqlParams[0].Value = DocumentId;

            sqlParams[1] = new SqlParameter("@DocumentTypeId", SqlDbType.Int);
            sqlParams[1].Value = DocumentTypeId;

            sqlParams[2] = new SqlParameter("@SupplierId", SqlDbType.BigInt);
            sqlParams[2].Value = SupplierId;

            sqlParams[3] = new SqlParameter("@FileName", SqlDbType.NVarChar);
            sqlParams[3].Value = FileName;

            sqlParams[4] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
            sqlParams[4].Value = Remarks;

            sqlParams[5] = new SqlParameter("@Active", SqlDbType.Bit);
            sqlParams[5].Value = Active;

            sqlParams[6] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            sqlParams[6].Value = CreatedBy;

            sqlParams[7] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[7].Value = -1;
            sqlParams[7].Direction = ParameterDirection.Output;
            sqlParams[7].Size = 9;


            return dbconnector.ExecuteProcedureWithOutParameter("SP_Documents_Insert", sqlParams, 7);

        }

        public long DeleteDocument(long DocumentId,long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];
            sqlParams[0] = new SqlParameter("@DocumentId", SqlDbType.BigInt);
            sqlParams[0].Value = DocumentId;

            sqlParams[1] = new SqlParameter("@CreatedBy", SqlDbType.Int);
            sqlParams[1].Value = CreatedBy;

            sqlParams[2] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Documents_Delete", sqlParams, 2);

        }

        public DataTable ShowSupplierDocs(long SupplierId)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SupplierId", SqlDbType.BigInt);
            sqlParams[0].Value = SupplierId;

            return dbconnector.USPGetTable("dtSupplierDocs", "SP_SupplierDocument_Show", sqlParams);
        }



    }
}
