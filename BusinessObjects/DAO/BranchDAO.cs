﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class BranchDAO
    {
         DBLink dbconnector;

        public BranchDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertBranch(long Srl, string BranchName, string ShortName, string UOM, long CreatedBy, long BillingCompanySrl, bool IsPlyApplicable, bool Active, string Address1, string Address2, string Address3, string landline, string mobile, string emailID, string GSTNo, string PAN, long LOBGroupId, bool PendingPODShow)
        {
            SqlParameter[] sqlParams = new SqlParameter[19];
            sqlParams[0] = new SqlParameter("@BranchSrl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@BranchName", SqlDbType.VarChar);
            sqlParams[1].Value = BranchName;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            sqlParams[3] = new SqlParameter("@ShortName", SqlDbType.VarChar);
            sqlParams[3].Value = ShortName;

            sqlParams[4] = new SqlParameter("@UOM", SqlDbType.NVarChar);
            sqlParams[4].Value = UOM;

            sqlParams[5] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[5].Value = CreatedBy;

            sqlParams[6] = new SqlParameter("@BillingCompanySrl", SqlDbType.BigInt);
            sqlParams[6].Value = BillingCompanySrl;

            sqlParams[7] = new SqlParameter("@IsPlyApplicable", SqlDbType.Bit);
            sqlParams[7].Value = IsPlyApplicable;

            sqlParams[8] = new SqlParameter("@Active", SqlDbType.Bit);
            sqlParams[8].Value = Active;

            sqlParams[9] = new SqlParameter("@Address1", SqlDbType.NVarChar);
            sqlParams[9].Value = Address1;

            sqlParams[10] = new SqlParameter("@Address2", SqlDbType.NVarChar);
            sqlParams[10].Value = Address2;

            sqlParams[11] = new SqlParameter("@Address3", SqlDbType.NVarChar);
            sqlParams[11].Value = Address3;

            sqlParams[12] = new SqlParameter("@Landline", SqlDbType.NVarChar);
            sqlParams[12].Value = landline;

            sqlParams[13] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
            sqlParams[13].Value = mobile;

            sqlParams[14] = new SqlParameter("@EmailID", SqlDbType.NVarChar);
            sqlParams[14].Value = emailID;

            sqlParams[15] = new SqlParameter("@GSTNo", SqlDbType.NVarChar);
            sqlParams[15].Value = GSTNo;

            sqlParams[16] = new SqlParameter("@PAN", SqlDbType.NVarChar);
            sqlParams[16].Value = PAN;

            sqlParams[17] = new SqlParameter("@LOBGroupId", SqlDbType.BigInt);
            sqlParams[17].Value = LOBGroupId;

            sqlParams[18] = new SqlParameter("@PendingPODShow", SqlDbType.Bit);
            sqlParams[18].Value = PendingPODShow;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_Branch_Insert", sqlParams, 2);
        }

        public DataTable GetBranches(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetBranch", "SP_Branch_Fill", sqlParams);
        }

        public DataTable GetBranchesUserWise(string SearchText,long UserId)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParams[1].Value = UserId;

            return dbconnector.USPGetTable("GetBranch", "SP_Branch_Fill_Userwise", sqlParams);
        }

        public DataTable GetBranchesPTL(string SearchText, long UserId)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            sqlParams[1] = new SqlParameter("@UserId", SqlDbType.BigInt);
            sqlParams[1].Value = UserId;

            return dbconnector.USPGetTable("GetBranchPTL", "SP_Branch_Fill_PTL", sqlParams);
        }

        public DataTable GetBranchdetailsBySrl(long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            return dbconnector.USPGetTable("GetBranchDetail", "SP_Branch_Details_By_Srl", sqlParams);
        }

        public bool isBranchAlreadyExist(string BranchName, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];          

            sqlParams[0] = new SqlParameter("@BranchName", SqlDbType.VarChar);
            sqlParams[0].Value = BranchName;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_Branch_isDuplicate", sqlParams, 2);

            if (cnt>0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
