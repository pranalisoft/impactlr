﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlManager;
using System.Data;
using System.Data.SqlClient;


namespace BusinessObjects.DAO
{
    public class CustomerGroupDAO
    {
        DBLink dbconnector;

        public CustomerGroupDAO()
        {
            dbconnector = new DBLink();
        }

        public long InsertCustomerGroup(long Srl, string Name, bool IsActive, long CreatedBy, string LatestRemarks, string ExpDate, decimal ExpAmount)
        {
            SqlParameter[] sqlParams = new SqlParameter[8];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[1].Value = Name;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            sqlParams[3] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[3].Value = IsActive;

            sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[4].Value = CreatedBy;

            sqlParams[5] = new SqlParameter("@LatestRemarks", SqlDbType.NVarChar);
            sqlParams[5].Value = LatestRemarks;

            sqlParams[6] = new SqlParameter("@ExpDate", SqlDbType.NVarChar);
            if (ExpDate == "")
                sqlParams[6].Value = DBNull.Value;
            else
                sqlParams[6].Value = ExpDate;

            sqlParams[7] = new SqlParameter("@ExpAmount", SqlDbType.Decimal);
            sqlParams[7].Value = ExpAmount;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerGroup_Insert", sqlParams, 2);
        }

        public DataTable GetCustomerGroups(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.VarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetCustomerGroup", "SP_CustomerGroup_Fill", sqlParams);
        }

        public bool isCustomerGroupAlreadyExist(string Name, long Srl)
        {
            SqlParameter[] sqlParams = new SqlParameter[3];

            sqlParams[0] = new SqlParameter("@Name", SqlDbType.VarChar);
            sqlParams[0].Value = Name;

            sqlParams[1] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[1].Value = Srl;

            sqlParams[2] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[2].Value = -1;
            sqlParams[2].Direction = ParameterDirection.Output;
            sqlParams[2].Size = 9;

            long cnt = dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerGroup_isDuplicate", sqlParams, 2);

            if (cnt > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public long DeleteCustomerGroup(string Srls)
        {
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srls", SqlDbType.VarChar);
            sqlParams[0].Value = Srls;

            sqlParams[1] = new SqlParameter("@rcnt", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerGroup_Delete", sqlParams, 1);

        }
    }
}
