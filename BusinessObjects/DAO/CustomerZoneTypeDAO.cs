﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using SqlManager;
using System.Data;
using System.Data.SqlClient;

namespace BusinessObjects.DAO
{
    public class CustomerZoneTypeDAO
    {
        DBLink dbconnector;

        public CustomerZoneTypeDAO()
        {
            dbconnector = new DBLink();
        }

         public long InsertUpdateCustomerZoneType(long Srl, long CustomerId,string ZoneType,bool IsActive, long CreatedBy)
        {
            SqlParameter[] sqlParams = new SqlParameter[6];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.BigInt);
            sqlParams[0].Value = Srl;
                        
            sqlParams[1] = new SqlParameter("@CustomerId", SqlDbType.BigInt);
            sqlParams[1].Value = CustomerId;

            sqlParams[2] = new SqlParameter("@ZoneType", SqlDbType.NVarChar);
            sqlParams[2].Value = ZoneType;

            sqlParams[3] = new SqlParameter("@IsActive", SqlDbType.Bit);
            sqlParams[3].Value = IsActive;

            sqlParams[4] = new SqlParameter("@CreatedBy", SqlDbType.BigInt);
            sqlParams[4].Value = CreatedBy;

            sqlParams[5] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[5].Value = -1;
            sqlParams[5].Direction = ParameterDirection.Output;
            sqlParams[5].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerZoneTypeInsertUpdate", sqlParams, 5);
        }

       public DataTable GetCustomerZoneType(string SearchText)
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
            sqlParams[0].Value = SearchText;

            return dbconnector.USPGetTable("GetCustomer", "SP_CustomerZoneTypeView", sqlParams);
        }

        public long CustomerZoneTypeDelete(string Srl)
        { 
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@Srl", SqlDbType.NVarChar);
            sqlParams[0].Value = Srl;

            sqlParams[1] = new SqlParameter("@RecordCount", SqlDbType.BigInt);
            sqlParams[1].Value = -1;
            sqlParams[1].Direction = ParameterDirection.Output;
            sqlParams[1].Size = 9;

            return dbconnector.ExecuteProcedureWithOutParameter("SP_CustomerZoneTypeDelete", sqlParams, 1);
        }        
    }
}