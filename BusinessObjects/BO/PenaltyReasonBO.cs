﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class PenaltyReasonBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }

        PenaltyReasonDAO optDao = new PenaltyReasonDAO();

        public long InsertPenaltyReason()
        {
            return optDao.InsertPenaltyReason(Srl, Name,IsActive,CreatedBy);
        }

        public DataTable GetPenaltyReasondetails()
        {
            return optDao.GetPenaltyReasons(SearchText);
        }

        public bool isPenaltyReasonAlreadyExist()
        {
            return optDao.isPenaltyReasonAlreadyExist(Name, Srl);
        }

        public long DeletePenaltyReason()
        {
            return optDao.DeletePenaltyReasons(Srls);
        }       
    }
}
