﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class SuplInvoiceBO
    {
        public string SearchText { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long Srl { get; set; }
        public long BillingCompanySrl { get; set; }
        public long CustomerSrl { get; set; }
        public string InvoiceDate { get; set; }
        public decimal BasicAmount { get; set; }
        public long BranchID { get; set; }
        public long EnteredBy { get; set; }
        public string ItemXML { get; set; }
        public string PaymentTerms { get; set; }
        public string Particulars { get; set; }
        public string InvoiceNo { get; set; }
        public string AmountInWords { get; set; }
        public string Srls { get; set; }

        public int IsPTL { get; set; }

        public long ApprovedBy { get; set; }

        SuplInvoiceDAO optDao = new SuplInvoiceDAO();
        public DataTable FillInvoiceGrid()
        {
            return optDao.FillInvoiceGrid(SearchText, BranchID, FromDate, ToDate);
        }

        public DataTable FillInvoiceGridPTL()
        {
            return optDao.FillInvoiceGridPTL(SearchText, BranchID, FromDate, ToDate,IsPTL);
        }

        public DataTable FillInvoiceDetails()
        {
            return optDao.FillInvoiceDetails(Srl);
        }

        public DataTable FillPendingCustomers()
        {
            return optDao.FillPendingCustomers(BranchID);
        }

        public DataTable FillPendingLRS()
        {
            return optDao.FillPendingLRS(BranchID, CustomerSrl);
        }

        public DataTable FillPendingLRSPTL()
        {
            return optDao.FillPendingLRSPTL(BranchID, CustomerSrl,IsPTL);
        }

        public long InsertInvoice()
        {
            return optDao.InsertInvoice(Srl, BillingCompanySrl, CustomerSrl, InvoiceDate, BasicAmount, BranchID, EnteredBy, ItemXML, PaymentTerms, Particulars, AmountInWords, InvoiceNo);
        }

        public long DeleteInvoices()
        {
            return optDao.DeleteInvoices(Srls);
        }

        public DataTable FillLRPendingForBill()
        {
            return optDao.FillLRPendingForBill(BranchID);
        }

        public string PenaltyXML { get; set; }

        public DataTable FillInvoicePenaltyDetails()
        {
            return optDao.FillInvoicePenaltyDetails(Srl);
        }

        public long UpdatetInvoice()
        {
            return optDao.UpdatetInvoice(Srl, BillingCompanySrl, CustomerSrl, InvoiceDate, BasicAmount, BranchID, EnteredBy, ItemXML, PaymentTerms, Particulars, AmountInWords, InvoiceNo, PenaltyXML,ApprovedBy);
        }

        public long InsertInvoice_Bulk()
        {
            return optDao.InsertInvoice_Bulk(EnteredBy);
        }

        public long InsertInvoice_BulkPTL()
        {
            return optDao.InsertInvoice_BulkPTL(EnteredBy,FromDate,ToDate);
        }

        public DataSet InvoicePrint()
        {
            return optDao.InvoicePrint(Srl);
        }
        public DataTable InvoicePrintDoc()
        {
            return optDao.InvoicePrintDoc(Srl);
        }

        public DataTable GetSuplMainInvoiceDoc()
        {
            return optDao.GetSuplMainInvoiceDoc(Srl);
        }
    }
}
