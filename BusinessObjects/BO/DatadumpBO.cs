﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessObjects.DAO;
namespace BusinessObjects.BO
{
   public class DatadumpBO
    {
       DatadumpDAO optdao = new DatadumpDAO();
       public DataSet GetDataDump()
       {
           return optdao.GetDataDump(FromDate, ToDate);
       }
       
       public DateTime FromDate { get; set; }
       public DateTime ToDate { get; set; }

       public void SaveExcelWorkSheetsEscalation()
       {
           optdao.SaveExcelWorkSheetsEscalation(ds, XLPath);
       }


       public DataSet ds { get; set; }
       public string XLPath { get; set; }
    }
}
