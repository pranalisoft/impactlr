﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class InvoiceBO
    {
        public string SearchText { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long Srl { get; set; }
        public long BillingCompanySrl { get; set; }
        public long CustomerSrl { get; set; }
        public string InvoiceDate { get; set; }
        public decimal BasicAmount { get; set; }
        public long BranchID { get; set; }
        public long EnteredBy { get; set; }
        public string ItemXML { get; set; }
        public string PaymentTerms { get; set; }
        public string Particulars { get; set; }
        public string BuyerRefNo { get; set; }
        public string BuyerRefDate { get; set; }
        public string AmountInWords { get; set; }
        public string VehicleNo { get; set; }
        public string Destination { get; set; }
        public string InvRefNo { get; set; }
        public decimal BiddingCharges { get; set; }

        InvoiceDAO optDao = new InvoiceDAO();
        public DataTable FillInvoiceGrid()
        {
            return optDao.FillInvoiceGrid(SearchText, BranchID, FromDate, ToDate);
        }

        public DataTable FillFinalInvoiceGrid_Report()
        {
            return optDao.FillFinalInvoiceGrid_Report(SearchText, BranchID, FromDate, ToDate, BillingCompanySrl);
        }

        public DataTable FillInvoiceDetails()
        {
            return optDao.FillInvoiceDetails(Srl,BillingCompanySrl);
        }

        public DataTable FillProformaInvoiceDetails()
        {
            return optDao.FillProformaInvoiceDetails(Srl);
        }

        public DataTable FillPendingCustomers()
        {
            return optDao.FillPendingCustomers(BranchID,BillingCompanySrl);
        }

        public DataTable FillPendingVehicles()
        {
            return optDao.FillPendingVehicles(BranchID, BillingCompanySrl,CustomerSrl);
        }

        public DataTable FillPendingDestinations()
        {
            return optDao.FillPendingDestinations(BranchID, BillingCompanySrl,CustomerSrl,VehicleNo);
        }

        public DataTable FillPendingLRS()
        {
            return optDao.FillPendingLRS(BranchID, CustomerSrl,BillingCompanySrl,VehicleNo,Destination);
        }

        public DataTable FillPendingLRSNew()
        {
            return optDao.FillPendingLRSNew(BranchID, CustomerSrl, BillingCompanySrl);
        }

        public long InsertInvoice()
        {
            return optDao.InsertInvoice(Srl, BillingCompanySrl, CustomerSrl, InvoiceDate, BasicAmount, BranchID, EnteredBy, ItemXML, PaymentTerms, Particulars, AmountInWords, BuyerRefNo, BuyerRefDate);
        }

        public string GetInvoiceNoFromID()
        {
            return optDao.GetInvoiceNoFromID(Srl);
        }

        public DataTable FillFinalInvoicePendingLRS()
        {
            return optDao.FillFinalInvoicePendingLRS(Srl,BillingCompanySrl);
        }

        public DataTable FillFinalInvoicePendingInvoices()
        {
            return optDao.FillFinalInvoicePendingInvoices(BranchID, BillingCompanySrl, CustomerSrl);
        }

        public DataTable FillFinalInvoiceGrid()
        {
            return optDao.FillFinalInvoiceGrid(SearchText,BranchID,FromDate,ToDate);
        }

        public DataTable FillFinalInvoicePendingCustomers()
        {
            return optDao.FillFinalInvoicePendingCustomers(BranchID,BillingCompanySrl);
        }

        public long InsertFinalInvoice()
        {
            return optDao.InsertFinalInvoice(Srl, BasicAmount, BranchID, EnteredBy, ItemXML, PaymentTerms, Particulars, AmountInWords, BuyerRefNo, BuyerRefDate, InvoiceDate, BillingCompanySrl, BiddingCharges);
        }

        public long InsertFinalInvoiceNew()
        {
            return optDao.InsertFinalInvoiceNew(Srl, BasicAmount, BranchID, EnteredBy, ItemXML, PaymentTerms, Particulars, AmountInWords, BuyerRefNo, BuyerRefDate, InvoiceDate, BillingCompanySrl, BiddingCharges, CustomerSrl);
        }

        public DataTable FillMainInvoiceDetails()
        {
            return optDao.FillMainInvoiceDetails(InvRefNo, BillingCompanySrl);
        }

        #region Hamali
        public DataTable FillFinalInvoiceGrid_Hamali()
        {
            return optDao.FillFinalInvoiceGrid_Hamali(SearchText, BranchID, FromDate, ToDate, BillingCompanySrl);
        }
        public DataTable FillInvoiceGrid_Hamali()
        {
            return optDao.FillInvoiceGrid_Hamali(SearchText, BranchID, FromDate, ToDate);
        }
        public DataTable FillInvoiceDetails_Hamali()
        {
            return optDao.FillInvoiceDetails_Hamali(Srl);
        }
        public long InsertInvoice_Hamali()
        {
            return optDao.InsertInvoice_Hamali(Srl, BillingCompanySrl, InvoiceDate, BasicAmount, BranchID, EnteredBy, PaymentTerms, Particulars, AmountInWords, ItemXML);
        }
        public long UpdateBillSubmissionDate()
        {
            return optDao.UpdateBillSubmissionDate(MasterInvref, BillSubmissionDate);
        }
        #endregion

        public string BillSubmissionDate { get; set; }

        public string MasterInvref { get; set; }
    }
}
