﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class PinCodeBO
    {
        public long Srl { get; set; }
        public long PinCode { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }

        PinCodeDAO optDao = new PinCodeDAO();

        public long InsertUpdatePinCode()
        {
            return optDao.InsertUpdatePinCode(Srl, PinCode, Longitude, Lattitude);
        }

        public DataTable GetPinCodedetails()
        {
            return optDao.GetPinCodes(SearchText);
        }
       
        public bool isPinCodeExist()
        {
            return optDao.isPinCodeExist(Srl,PinCode);
        }

        public long DeletePinCode()
        {
            return optDao.DeletePinCode(Srls);
        }
    }
}
