﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class VehicleTypeBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }

        VehicleTypeDAO optDao = new VehicleTypeDAO();

        public long InsertVehicleType()
        {
            return optDao.InsertVehicleType(Srl, Name,IsActive,CreatedBy);
        }

        public DataTable GetVehicleTypedetails()
        {
            return optDao.GetVehicleTypes(SearchText);
        }

        public bool isVehicleTypeAlreadyExist()
        {
            return optDao.isVehicleTypeAlreadyExist(Name, Srl);
        }

        public long DeleteVehicleType()
        {
            return optDao.DeleteVehicleType(Srls);
        }



        public long CreatedBy { get; set; }
    }
}
