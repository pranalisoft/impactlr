﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class CustomerGroupBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string LatestRemarks { get; set; }
        public string ExpDate { get; set; }
        public decimal ExpAmount { get; set; }

        CustomerGroupDAO optDao = new CustomerGroupDAO();

        public long InsertCustomerGroup()
        {
            return optDao.InsertCustomerGroup(Srl, Name,IsActive,CreatedBy,LatestRemarks,ExpDate,ExpAmount);
        }

        public DataTable GetCustomerGroupdetails()
        {
            return optDao.GetCustomerGroups(SearchText);
        }

        public bool isCustomerGroupAlreadyExist()
        {
            return optDao.isCustomerGroupAlreadyExist(Name, Srl);
        }

        public long DeleteCustomerGroup()
        {
            return optDao.DeleteCustomerGroup(Srls);
        }
    }
}
