﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
   public class CustomerBO
    {
        public string SearchText { get; set; }
        public long Srl { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string landline { get; set; }
        public string mobile { get; set; }
        public string emailID { get; set; }
        public string vendor { get; set; }
        public string CST { get; set; }
        public string VAT { get; set; }
        public string TIN { get; set; }
        public string ServiceTaxno { get; set; }
        public long creditdays { get; set; }
        public decimal creditLimit { get; set; }
        public bool isActive { get; set; }
        public string Srls { get; set; }
        public long CustomerGroupId { get; set; }

        public string GLCode { get; set; }
        public long CreatedBy { get; set; }
        public string ShowAll { get; set; }
        public bool IsphysicalPODReqd { get; set; }

        public long LobId { get; set; }

        CustomerDAO optDao = new CustomerDAO();
       
       public long InsertCustomer()
        {
            return optDao.InsertCustomer(Srl,Name,Address1,Address2,Address3,landline,mobile,emailID,vendor,CST,VAT,TIN,ServiceTaxno,creditdays,creditLimit,isActive,GLCode,CreatedBy,IsphysicalPODReqd,CustomerGroupId);
        }

        public DataTable GetCustomerDetails()
        {
            return optDao.GetCustomers(SearchText,ShowAll);
        }

        public DataTable GetCustomerDetailsUserWise()
        {
            return optDao.GetCustomersUserWise(SearchText, ShowAll,CreatedBy);
        }

        public bool isCustomerAlreadyExist()
        {
            return optDao.isCustomerAlreadyExist(Name, Srl);
        }

        public long DeleteCustomer()
        {
            return optDao.DeleteCustomer(Srls);
        }

        public DataTable GetLobWiseCustomer()
        {
            return optDao.GetLobWiseCustomer(SearchText);
        }

        public long InsertCustomerLob()
        {
            return optDao.InsertCustomerLob(Srl, LobId);
        }

        public long DeleteCustomerLob()
        {
            return optDao.DeleteCustomerLob(Srl, LobId);
        }
    }
}
