﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class DocumentTypeBO
    {
        public string SearchText { get; set; }

        public int DocumentTypeId { get; set; }

        public string DocumentTypeName { get; set; }

        public bool IsMandatory { get; set; }

        public bool Active { get; set; }

        public long CreatedBy { get; set; }

        public long DocumentId { get; set; }

        public long SupplierId { get; set; }

        public string FileName { get; set; }

        public string Remarks { get; set; }

        DocumentTypeDAO optDao = new DocumentTypeDAO();

        public long InsertUpdateDocumentType()
        {
            return optDao.InsertUpdateDocumentType(DocumentTypeId, DocumentTypeName,  IsMandatory,  Active,  CreatedBy);
        }

        public DataTable ShowData()
        {
            return optDao.ShowData(SearchText);
        }

        public bool isDocumentTypeExist()
        {
            return optDao.isDocumentTypeExist(DocumentTypeId,  DocumentTypeName);
        }

        public long InsertDocuments()
        {
            return optDao.InsertDocuments(DocumentId,DocumentTypeId,SupplierId, FileName,Remarks,Active,CreatedBy);
        }

        public long DeleteDocument()
        {
            return optDao.DeleteDocument(DocumentId,CreatedBy);
        }

         public DataTable ShowSupplierDocs()
        {
            return optDao.ShowSupplierDocs(SupplierId);
        }
    }
}
