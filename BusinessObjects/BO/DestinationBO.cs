﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class DestinationBO
    {
        public long Srl { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public int ExpDeliveryDays { get; set; }
        public string FromOrTo { get; set; }
        public long CreatedBy { get; set; }

        DestinationDAO optDao = new DestinationDAO();

        public long InsertDestination()
        {
            return optDao.InsertDestination(Srl, FromLoc, ToLoc, ExpDeliveryDays, IsActive, CreatedBy);
        }

        public long InsertDestinationPTL()
        {
            return optDao.InsertDestinationPTL(Srl, FromLoc, ToLoc, ExpDeliveryDays, IsActive, CreatedBy);
        }

        public DataTable GetDestinationdetails()
        {
            return optDao.GetDestinations(SearchText);
        }

        public DataTable GetDestinationdetailsPTL()
        {
            return optDao.GetDestinationsPTL(SearchText);
        }

        public DataTable GetFromOrToLocations()
        {
            return optDao.GetFromOrToLocations(FromOrTo);
        }

        public DataTable GetFromOrToLocationsPTL()
        {
            return optDao.GetFromOrToLocationsPTL(FromOrTo);
        }

        public DataTable GetToLocations()
        {
            return optDao.GetToLocations(FromLoc);
        }

        public DataTable GetToLocationsPTL()
        {
            return optDao.GetToLocationsPTL(FromLoc);
        }

        public long GetDestinationExpDays()
        {
            return optDao.GetDestinationExpDays(FromLoc, ToLoc);
        }

        public long GetDestinationExpDaysPTL()
        {
            return optDao.GetDestinationExpDaysPTL(FromLoc, ToLoc);
        }

        public bool isDestinationAlreadyExist()
        {
            return optDao.isDestinationAlreadyExist(FromLoc, ToLoc, Srl);
        }

        public bool isDestinationAlreadyExistPTL()
        {
            return optDao.isDestinationAlreadyExistPTL(FromLoc, ToLoc, Srl);
        }

        public long DeleteDestination()
        {
            return optDao.DeleteDestinations(Srls);
        }

        public long DeleteDestinationPTL()
        {
            return optDao.DeleteDestinationsPTL(Srls);
        }
    }
}
