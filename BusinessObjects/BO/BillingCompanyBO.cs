﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
   public class BillingCompanyBO
    {
        public string SearchText { get; set; }
        public long Srl { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string landline { get; set; }
        public string mobile { get; set; }
        public string emailID { get; set; }
        public string vendor { get; set; }
        public string CST { get; set; }
        public string VAT { get; set; }
        public string TIN { get; set; }
        public string ServiceTaxno { get; set; }
        public long creditdays { get; set; }
        public decimal creditLimit { get; set; }
        public bool isActive { get; set; }
        public string Srls { get; set; }
        public byte[] logo { get; set; }
        public string shortName { get; set; }
        public string billType { get; set; }

        BillingCompanyDAO optDao = new BillingCompanyDAO();

        public long InsertBillingCompany()
        {
            return optDao.InsertBillingCompany(Srl, Name, Address1, Address2, Address3, landline, mobile, emailID, vendor, CST, VAT, TIN, ServiceTaxno, creditdays, creditLimit, isActive,logo,shortName,billType,CreatedBy);
        }

        public DataTable GetBillingCompanyDetails()
        {
            return optDao.GetBillingCompany(SearchText);
        }

        public bool isBillingCompanyAlreadyExist()
        {
            return optDao.isBillingCompanyAlreadyExist(Name, Srl);
        }


        public long DeleteBillingCompany()
        {
            return optDao.DeleteBillingCompany(Srls);
        }



        public long CreatedBy { get; set; }
    }
}
