﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessObjects.BO
{
   public class Loading
    {
       public string lrnumber { get; set; }
       public string lat { get; set; }
       public string lng { get; set; }
       public string address { get; set; }
       public string area { get; set; }
      
 
    }
}
