﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class CustomerReceiptBO
    {
        CustomerReceiptDAO CustRcptDao = new CustomerReceiptDAO();
        public string refNo { get; set; }
        public string tranDate { get; set; }
        public long CustomerID { get; set; }
        public string remarks { get; set; }
        public decimal totalAmount { get; set; }
        public long userID { get; set; }
        public string paymentXML { get; set; }
        public string BankName { get; set; }
        public string PaymentMode { get; set; }
        public string ChqNo { get; set; }
        public string ChqDate { get; set; }
        public string CardNetBankingDetails { get; set; }
        public string PayType { get; set; }
        public long Srl { get; set; }
        public bool DatefilterYes { get; set; }
        public string SearchFrom { get; set; }
        public string SearchTo { get; set; }
        public long BranchID { get; set; }
        public string InvoiceNos { get; set; }

        public string Srls { get; set; }

        public long InsertPaymentDetails()
        {
            return CustRcptDao.InsertPaymentDetails(refNo, tranDate, CustomerID, remarks, totalAmount, userID, paymentXML, BankName, PaymentMode, ChqNo, ChqDate, CardNetBankingDetails, PayType, BranchID);
        }

        public DataTable ShowReceiptGrid()
        {
            return CustRcptDao.ShowReceiptGrid(CustomerID, BranchID);
        }

        public DataTable ShowReceiptGridLR()
        {
            return CustRcptDao.ShowReceiptGridLR(CustomerID, BranchID,InvoiceNos);
        }

        public DataTable ShowReceiptStatus()
        {
            return CustRcptDao.ShowReceiptStatus(CustomerID, BranchID);
        }

        public DataTable FillCustomerCombo()
        {
            return CustRcptDao.FillCustomerCombo(BranchID);
        }

        public DataTable FillReceiptInvoiceDetails()
        {
            return CustRcptDao.FillReceiptInvoiceDetails(Srl);
        }

        public DataTable FillReceiptHeader()
        {
            return CustRcptDao.FillReceiptHeader(DatefilterYes, SearchFrom, SearchTo, BranchID,PayType);
        }

        public long DeleteReceipt()
        {
            return CustRcptDao.DeleteReceipt(Srls);
        }

        public DataTable FillReceiptOnAccount()
        {
            return CustRcptDao.FillReceiptOnAccount(BranchID);
        }
        public DataTable FillReceiptOnAccountInfo()
        {
            return CustRcptDao.FillReceiptOnAccountInfo(RcptID);
        }

        public long InsertOnAccountPaymentDetails()
        {

            return CustRcptDao.InsertOnAccountPaymentDetails(RcptID, paymentXML,BranchID);
        }

        public DataTable CustomerOutstandingReport()
        {
            return CustRcptDao.CustomerOutstandingReport(BranchID, userID);
        }

        public DataTable CustomerPendingInvoices()
        {
            return CustRcptDao.CustomerPendingInvoices(BranchID);
        }

        public long RcptID { get; set; }
    }
}
