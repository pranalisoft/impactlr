﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class ReportBO
    {
        public long ID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long BillingCompanySrl { get; set; }
        public string InvRefNo { get; set; }

        public string InvSrls { get; set; }
        public int InvYear { get; set; }
        public string RefNo { get; set; }

        public string type { get; set; }
        public long BranchId { get; set; }
        public long SupplierId { get; set; }
        public string IDs { get; set; }

        public long CreatedBy { get; set; }
        public long RoleId { get; set; }

        ReportDAO OptDao = new ReportDAO();

        public DataTable LR_Document()
        {
            return OptDao.LR_Document(ID);
        }

        public DataTable LR_DocumentMultiple()
        {
            return OptDao.LR_DocumentMultiple(IDs);
        }

        public DataTable LR_Annexure()
        {
            return OptDao.LR_Annexure(ID);
        }

        public DataTable LR_AnnexureMultiple()
        {
            return OptDao.LR_AnnexureMultiple(IDs);
        }

        public DataTable Invoice_Doument()
        {
            return OptDao.Invoice_Document(InvRefNo,BillingCompanySrl);
        }

        public DataTable Invoice_Document_Multiple()
        {
            return OptDao.Invoice_Document_Multiple(InvSrls, BillingCompanySrl,InvYear);
        }

        public DataTable Customer_Ledger()
        {
            return OptDao.Customer_Ledger(ID,FromDate,ToDate);
        }

        public decimal Customer_Ledger_Opening()
        {
            return OptDao.Customer_Ledger_Opening(ID, FromDate);
        }

        public DataTable DashBoard()
        {
            return OptDao.DashBoard(FromDate, ToDate,CreatedBy);
        }

        public DataTable DashBoardPTL()
        {
            return OptDao.DashBoardPTL(FromDate, ToDate,BranchId);
        }

        public DataTable AdvancePaidReport()
        {
            return OptDao.AdvancePaidReport(FromDate, ToDate);
        }

        public DataTable BalancePaidReport()
        {
            return OptDao.BalancePaidReport(FromDate, ToDate);
        }        

        public DataTable GetPaymentVoucherData()
        {
            return OptDao.GetPaymentVoucherData(RefNo);
        }

        public DataSet GetPaymentVoucherEmailDetails()
        {
            return OptDao.GetPaymentVoucherEmailDetails(RefNo);
        }

        public DataTable GEtPODPerformance()
        {
            return OptDao.GEtPODPerformance(FromDate, ToDate, BranchId, type);
        }

        public DataTable GetPODPerformanceSlabWise()
        {
            return OptDao.GetPODPerformanceSlabWise(FromDate, ToDate, BranchId, type,CreatedBy);
        }

        public DataTable GetSupplierPayableLink()
        {
            return OptDao.GetSupplierPayableLink(SupplierId, BillingCompanySrl);
        }

        public DataTable GetPendingDashboard()
        {
            return OptDao.GetPendingDashboard(CreatedBy);
        }

        public DataTable DashboardManager()
        {
            return OptDao.DashboardManager(CreatedBy);
        }

        public DataTable DashboardManagerExcel()
        {
            return OptDao.DashboardManagerExcel(BranchId);
        }

        public DataTable DashboardAccounts()
        {
            return OptDao.DashboardAccounts(CreatedBy);
        }

        public DataTable DashboardFillAllocatedGrids()
        {
            return OptDao.DashboardFillAllocatedGrids(RoleId);
        }

        public DataTable GetPendingDashboard_Link()
        {
            return OptDao.GetPendingDashboard_Link(BranchId, type);
        }

        public DataTable GetDueEwayBillsDashboard()
        {
            return OptDao.GetDueEwayBillsDashboard(CreatedBy);
        }

        public DataTable PTLMIS()
        {
            return OptDao.PTLMIS(FromDate, ToDate, BranchId);
        }

        public DataTable SupplierLRDetails()
        {
            return OptDao.SupplierLRDetails(FromDate, ToDate, SupplierId);
        }

        public DataTable SupplierPurchaseRegister()
        {
            return OptDao.SupplierPurchaseRegister(FromDate, ToDate, SupplierId);
        }
    }
}
