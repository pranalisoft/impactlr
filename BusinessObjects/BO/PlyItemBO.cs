﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class PlyItemBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public long HSNCodeSrl { get; set; }

        public string xml { get; set; }

        PlyItemDAO optDao = new PlyItemDAO();

        public long InsertPlyItem()
        {
            return optDao.InsertPlyItem(Srl, Name, IsActive, CreatedBy, HSNCodeSrl);
        }

        public DataTable GetPlyItemdetails()
        {
            return optDao.GetPlyItems(SearchText);
        }

        public bool isPlyItemAlreadyExist()
        {
            return optDao.isPlyItemAlreadyExist(Name, Srl);
        }

        public long DeletePlyItem()
        {
            return optDao.DeletePlyItems(Srls);
        }

        public DataTable GetVehicleTypeWiseQty()
        {
            return optDao.GetVehicleTypeWiseQty(Srl);
        }

        public long InsertPlyQty()
        {
            return optDao.InsertPlyQty(Srl,xml);
        }

        public DataTable GetPlyItemHSNGST()
        {
            return optDao.GetPlyItemHSNGST(Srl);
        }
    }
}
