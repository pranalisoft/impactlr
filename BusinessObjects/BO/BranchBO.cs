﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class BranchBO
    {
        public long Srl { get; set; }
        public string BranchName { get; set; }
        public string SearchText { get; set; }
        public bool CalculateAmount { get; set; }
        public bool IsPlyApplicable { get; set; }
        public bool Active { get; set; }
        public long BillingCompanySrl { get; set; }
        public string UOM { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string landline { get; set; }
        public string mobile { get; set; }
        public string emailID { get; set; }
        public string GSTNo { get; set; }
        public string PAN { get; set; }
        public long LOBGroupId { get; set; }
        public bool PendingPODShow { get; set; }

        BranchDAO optDao = new BranchDAO();
        public long InsertBranch()
        {
            return optDao.InsertBranch(Srl, BranchName, ShortName, UOM, CreatedBy, BillingCompanySrl, IsPlyApplicable, Active, Address1, Address2, Address3, landline, mobile, emailID, GSTNo, PAN,LOBGroupId,PendingPODShow);
        }

        public DataTable GetBranchdetails()
        {
            return optDao.GetBranches(SearchText);
        }

        public DataTable GetBranchesUserWise()
        {
            return optDao.GetBranchesUserWise(SearchText,CreatedBy);
        }

        public DataTable GetBranchdetailsPTL()
        {
            return optDao.GetBranchesPTL(SearchText,CreatedBy);
        }

        public DataTable GetBranchdetailsBySrl()
        {
            return optDao.GetBranchdetailsBySrl(Srl);
        }

        public bool isBranchAlreadyExist()
        {
            return optDao.isBranchAlreadyExist(BranchName, Srl);
        }

        public string ShortName { get; set; }

        public long CreatedBy { get; set; }
    }
}
