﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class PODReasonBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }

        PODReasonDAO optDao = new PODReasonDAO();

        public long InsertPODReason()
        {
            return optDao.InsertPODReason(Srl, Name,IsActive,CreatedBy);
        }

        public DataTable GetPODReasondetails()
        {
            return optDao.GetPODReasons(SearchText);
        }

        public bool isPODReasonAlreadyExist()
        {
            return optDao.isPODReasonAlreadyExist(Name, Srl);
        }

        public long DeletePODReason()
        {
            return optDao.DeletePODReasons(Srls);
        }        
    }
}
