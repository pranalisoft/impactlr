﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class DeviceBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }

        DeviceDAO optDao = new DeviceDAO();

        public long InsertDevice()
        {
            return optDao.InsertDevice(Srl, Name,IsActive,CreatedBy);
        }

        public DataTable GetDevicedetails()
        {
            return optDao.GetDevices(SearchText);
        }

        public bool isDeviceAlreadyExist()
        {
            return optDao.isDeviceAlreadyExist(Name, Srl);
        }

        public long DeleteDevice()
        {
            return optDao.DeleteDevices(Srls);
        }





        public long CreatedBy { get; set; }
    }
}
