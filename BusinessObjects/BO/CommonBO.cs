﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class CommonBO
    {
        CommonDAO cmdao = new CommonDAO();
        public long srl { get; set; }
        public string returnable { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
        public string SupplierName { get; set; }
        public string inpno { get; set; }
        public string raisedby { get; set; }
        public string status   { get; set; }
        public long plnt { get; set; }

        public string SearchText { get; set; }
        public long BranchID { get; set; }
        public string AddOrEdit { get; set; }
        public long LRId { get; set; }
        public decimal DetentionCharges { get; set; }
        public decimal WaraiCharges { get; set; }
        public decimal TwoPointCharges { get; set; }
        public decimal OtherCharges { get; set; }
        public decimal Penalty { get; set; }
        public string Remarks { get; set; }
        public decimal LatePODCharges { get; set; }
        public decimal TotalCharges { get; set; }
        public string strItem { get; set; }
        public long CreatedBy { get; set; }

        public string ReportingDate { get; set; }
        public string UnloadingDate { get; set; }
        public string PODDate { get; set; }

        public DataTable GetBranch()
        {
            return cmdao.GetBranch(srl);
        }

        public DataTable FillBankDetails()
        {
            return cmdao.FillBankDetails();
        }

        public DataTable GetChallanRegister()
        {
            return cmdao.GetChallanRegister(returnable, fromdate, todate, SupplierName, inpno, raisedby, status, plnt);
        }



        public long BillingCompanySrl { get; set; }
        public string ShowOrGenerate { get; set; }


        public DataTable GetLRForPostPOD()
        {
            return cmdao.GetLRForPostPOD(SearchText, BranchID, AddOrEdit,PendingForApproval);
        }

        public DataTable GetLRForPostPODPTL()
        {
            return cmdao.GetLRForPostPODPTL(SearchText, BranchID, AddOrEdit, PendingForApproval);
        }

        public DataTable GetPODPenalty()
        {
            return cmdao.GetPODPenalty(srl);
        }

        public DataTable GetZeroLR()
        {
            return cmdao.GetZeroLR(srl);
        }

        public long InsertUpdate_PostPOD()
        {
            return cmdao.InsertUpdate_PostPOD(srl, LRId, DetentionCharges, WaraiCharges, TwoPointCharges, OtherCharges, Penalty, Remarks, LatePODCharges, TotalCharges, CreatedBy, strItem,Approved,ReportingDate,UnloadingDate,PODDate);
        }

        public DataTable GetSupplierPayableApproval()
        {
            return cmdao.GetSupplierPayableApproval(BillingCompanySrl, ShowOrGenerate);
        }

        public long InsertSupplierPayableApproval()
        {
            return cmdao.InsertSupplierPayableApproval(strItem, BillingCompanySrl);
        }

        public DataTable GetCustomerReceivable()
        {
            return cmdao.GetCustomerReceivable(BillingCompanySrl,fromdate);
        }

        public long InsertUpdateScrap()
        {
            return cmdao.InsertUpdateScrap(srl, ItemSrl, Qty, Remarks, CreatedBy);
        }

        public long UploadFreightTigerDataDump()
        {
            return cmdao.UploadFreightTigerDataDump();
        }


        public DataTable FillScrap()
        {
            return cmdao.FillScrap();
        }

        public DataTable FillScrapItem()
        {
            return cmdao.FillScrapItem();
        }

        public DataTable FillReturn()
        {
            return cmdao.FillReturn();
        }

        public DataTable FillReturnSupplier()
        {
            return cmdao.FillReturnSupplier();
        }

        public DataSet FillReturnItem()
        {
            return cmdao.FillReturnItem(SupplierId);
        }
        public long InsertUpdateReturn()
        {
            return cmdao.InsertUpdateReturn(srl, SupplierId, ItemSrl, Qty, LostQty, Remarks, CreatedBy);
        }

        public DataTable FillPlysheetRegister()
        {
            return cmdao.FillPlysheetRegister();
        }

        public long ItemSrl { get; set; }

        public int Qty { get; set; }

        public long SupplierId { get; set; }

        public int LostQty { get; set; }

        public bool Approved { get; set; }

        public bool PendingForApproval { get; set; }

        public DataTable GenerateTallyPurchaseExcel()
        {
            return cmdao.GenerateTallyPurchaseExcel(BillingCompanySrl, fromdate, todate);
        }

        public DataTable GenerateTallySalesExcel()
        {
            return cmdao.GenerateTallySalesExcel(BillingCompanySrl, fromdate, todate);
        }
    }
}
