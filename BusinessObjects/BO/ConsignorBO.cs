﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class ConsignorBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public string Address { get; set; }
        public string GSTNo { get; set; }

        ConsignorDAO optDao = new ConsignorDAO();

        public long InsertConsignor()
        {
            return optDao.InsertConsignor(Srl, Name, Address, IsActive, GSTNo,CreatedBy,EmailId,MobileNo,PhoneNo,ConsignorType,CustomerXML,Pincode);
        }

        public DataTable GetConsignordetails()
        {
            return optDao.GetConsignors(SearchText);
        }

        public DataTable GetConsignorAddress()
        {
            return optDao.GetConsignorAddress(Name);
        }
        public DataTable GetConsignorDetailsById()
        {
            return optDao.GetConsignorDetailsById(Srl);
        }
        public bool isConsignorAlreadyExist()
        {
            return optDao.isConsignorAlreadyExist(Name, Srl);
        }

        public long DeleteConsignor()
        {
            return optDao.DeleteConsignors(Srls);
        }

        public DataTable GetConsigneeDetails()
        {
            return optDao.GetConsigneeDetails(Name);
        }

        public DataTable FillCustomersofConsignors()
        {
            return optDao.FillCustomersofConsignors(Srl);
        }

        public DataSet FillConsignorConsignee()
        {
            return optDao.FillConsignorConsignee(CustomerId);
        }

        public long CreatedBy { get; set; }

        public string EmailId { get; set; }

        public string MobileNo { get; set; }

        public string PhoneNo { get; set; }

        public string ConsignorType { get; set; }

        public string CustomerXML { get; set; }

        public long CustomerId { get; set; }

        public string Pincode { get; set; }
    }
}
