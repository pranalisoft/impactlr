﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class LRBO
    {
        public string SearchText { get; set; }
        public long BranchID { get; set; }
        public long ID { get; set; }
        public string LRNo { get; set; }
        public string LRDate { get; set; }
        public string Consigner { get; set; }
        public string Consignee { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string VehicleNo { get; set; }
        public decimal Weight { get; set; }
        public decimal ChargeableWt { get; set; }
        public long TotPackage { get; set; }
        public string InvoiceNo { get; set; }
        public decimal InvoiceValue { get; set; }
        public string LoadType { get; set; }
        public string Remarks { get; set; }
        public long CreatedBy { get; set; }
        public string Status { get; set; }
        public string CurrentRemarks { get; set; }
        public string FillType { get; set; }
        public string Fromdate { get; set; }
        public string ToDate { get; set; }
        public bool IsPending { get; set; }
        public DateTime TranDateTime { get; set; }
        public DateTime LRDatetime { get; set; }
        public string PODDate { get; set; }
        public string PODFile { get; set; }
        public string DriverDtls { get; set; }
        public long DeviceID { get; set; }
        public string RefNo { get; set; }
        public long SupplierID { get; set; }
        public long SubSupplierID { get; set; }
        public long CustomerID { get; set; }
        public decimal AdvanceToSupplier { get; set; }
        public decimal TotalFrieght { get; set; }
        public long VehicleTypeID { get; set; }
        public long DetailID { get; set; }
        public decimal SupplierAmount { get; set; }
        public long BillingCompanySrl { get; set; }
        public decimal Rate { get; set; }
        public string ConsignorAddress { get; set; }
        public string ConsigneeAddress { get; set; }
        public string ConsigneeEmail { get; set; }
        public string ConsigneeContact { get; set; }
        public string VehicleSealNo { get; set; }
        public string PackingType { get; set; }
        public string VehiclePlacementDate { get; set; }
        public int ExpectedDeliveryDays { get; set; }
        public string DriverNumber { get; set; }
        public string ConsignorGSTNo { get; set; }
        public string ConsigneeGSTNo { get; set; }
        public string RefNo_Invoice { get; set; }
        public string InvoiceDate { get; set; }
        public bool LRWise { get; set; }
        public decimal CashAdvance { get; set; }
        public decimal FuelAdvance { get; set; }
        public decimal TotalAdvance { get; set; }
        public string FreightType { get; set; }
        public string EWayBillNo { get; set; }
        public decimal AdvancePer { get; set; }
        public decimal Incentive { get; set; }
        public decimal SupplierIncentive { get; set; }

        public string CancelledRemarks { get; set; }
        public bool Multipoint { get; set; }
        public string MainLRNo { get; set; }
        public string ShipmentDocNo { get; set; }
        public string InternalBillingDocNo { get; set; }
        public long placementId { get; set; }
        public int PetrolPumpId { get; set; }
        public int OrgPincode { get; set; }
        public int DestPincode { get; set; }

        public long FuelSlipId { get; set; }

        public string FileName { get; set; }

        LRDAO optdao = new LRDAO();

        public DataTable FillLRGrid()
        {
            return optdao.FillLRGrid(SearchText, BranchID, Fromdate, ToDate, IsPending);
        }

        public DataTable FillLRPreviousDetails()
        {
            return optdao.FillLRPreviousDetails(BranchID, CreatedBy);
        }

        public long InsertUpdateLR()
        {
            return optdao.InsertUpdateLRRecord(ID, LRNo, LRDatetime, Consigner, Consignee, Origin, Destination, VehicleNo, Weight, ChargeableWt, TotPackage, InvoiceNo, InvoiceValue, LoadType, Remarks, CreatedBy, Status, BranchID, DriverDtls, DeviceID, RefNo, SupplierID, SubSupplierID, CustomerID, AdvanceToSupplier, TotalFrieght, VehicleTypeID, SupplierAmount, BillingCompanySrl, Rate, ConsignorAddress, ConsigneeAddress, ConsigneeEmail, ConsigneeContact, VehicleSealNo, PackingType, VehiclePlacementDate, ExpectedDeliveryDays, DriverNumber, FreightType, ConsignorGSTNo, ConsigneeGSTNo, RefNo_Invoice, InvoiceDate, CashAdvance, FuelAdvance, EWayBillNo, AdvancePer, Multipoint, MainLRNo, ShipmentDocNo, InternalBillingDocNo, Incentive, SupplierIncentive, placementId, PetrolPumpId, PlyQty, ConsignorEmail, ConsignorTelNo, ConsignorPincode, ConsigneePincode, EwayBillDate);
        }

        public string GetLRNo()
        {
            return optdao.GetLRNo(ID);
        }

        public DataTable FillLRCombo()
        {
            return optdao.FillDropDowns(FillType);
        }

        public DataTable FillLRComboDelete()
        {
            return optdao.FillLRComboDelete();
        }

        public DataTable FillLRComboUnloadingPayment()
        {
            return optdao.FillLRComboUnloadingPayment();
        }

        public long LRDelete()
        {
            return optdao.LRDelete(ID, CancelledRemarks, CreatedBy);
        }

        public long UpdateApproval()
        {
            return optdao.UpdateApproval(ID);
        }

        public long LRUpdateBranch()
        {
            return optdao.LRUpdateBranch(ID, BranchID);
        }

        public DataTable FillDropDownDevice()
        {
            return optdao.FillDropDownDevice(DeviceID);
        }

        public DataTable FillLRStatus()
        {
            return optdao.FillStatusForLR(ID);
        }

        public long Devise_Used()
        {
            return optdao.Devise_Used(DeviceID);
        }

        public DataTable GetLRDtlsByNo()
        {
            return optdao.GetLRDetailsByNo(LRNo);
        }

        public DataTable GetLRHeaderByNo()
        {
            return optdao.GetLRHeaderByNo(LRNo);
        }

        public DataTable GetLRHeaderById()
        {
            return optdao.GetLRHeaderById(ID);
        }

        public long InsertLRStatus()
        {
            return optdao.InsertLRStatus(ID, TranDateTime, Status, Remarks, CreatedBy, DetailID);
        }

        public DataTable LRFreightUpdationCount()
        {
            return optdao.LRFreightUpdationCount(BranchID,CreatedBy);
        }

        public DataTable LRFreightUpdationCount_ShowLR()
        {
            return optdao.LRFreightUpdationCount_ShowLR(BranchID);
        }

        public DataTable LRStatusUpdationCount()
        {
            return optdao.LRStatusUpdationCount(BranchID, CreatedBy);
        }

        public DataTable LRStatusUpdationCount_ShowLR()
        {
            return optdao.LRStatusUpdationCount_ShowLR(BranchID);
        }

        public long LRDuplicateCount()
        {
            return optdao.LRDuplicateCount(LRNo, ID);
        }

        public DataTable LRDetailReport()
        {
            return optdao.LRDetailReport(BranchID, Fromdate, ToDate);
        }

        public DataTable LRSummaryReport()
        {
            return optdao.LRSummaryReport(BranchID, Fromdate, ToDate);
        }

        public DataTable FreightTigerSummary()
        {
            return optdao.FreightTigerSummary(Fromdate, ToDate);
        }
        public DataTable FreightTigerSummaryLinks()
        {
            return optdao.FreightTigerSummaryLinks(Fromdate, ToDate,LinkType);
        }


        public DataTable PendingPODReport()
        {
            return optdao.PendingPODReport(CreatedBy);
        }

        public DataTable FillPackingType()
        {
            return optdao.FillPackingType(SearchText);
        }

        public DataTable FillLRDescriptionCombo()
        {
            return optdao.FillLRDescriptionCombo(SearchText);
        }

        public DataTable GetLRNoByCompName()
        {
            return optdao.GetLRNoByCompName(BillingCompanySrl, BranchID);
        }

        public DataTable LRMISReport()
        {
            return optdao.LRMISReport(Fromdate, ToDate, BranchID,CreatedBy);
        }

        public DataTable LRMISConsolidateReport()
        {
            return optdao.LRMISConsolidateReport(Fromdate, ToDate, BranchID, LRWise);
        }

        public DataTable GetSupplierDetailsById()
        {
            return optdao.GetSupplierDetailsById(SupplierID);
        }

        public DataTable GetPlacementDetails()
        {
            return optdao.GetPlacementDetails(placementId);
        }

        public DataTable GetPlacementDetailsView()
        {
            return optdao.GetPlacementDetailsView(placementId, Status);
        }

        public long GetCntNonZeroLRs()
        {
            return optdao.GetCntNonZeroLRs(ID, placementId);
        }

        public long UpdateFulfillment()
        {
            return optdao.UpdateFulfillment(placementId);
        }

        public long UpdateLRAmounts()
        {
            return optdao.UpdateLRAmounts(ID, CashAdvance, FuelAdvance, TotalAdvance, TotalFrieght, SupplierAmount, CreatedBy, Remarks);
        }

        public decimal UnloadingCharges { get; set; }

        public long UpdateUnloadingCharges()
        {
            return optdao.UpdateUnloadingCharges(ID, UnloadingCharges);
        }

        public DataTable GetLRHeaderByNoForUnloading()
        {
            return optdao.GetLRHeaderByNoForUnloading(LRNo);
        }

        public DataTable GetLRNosForPlacementId()
        {
            return optdao.GetLRNosForPlacementId(placementId);
        }

        public long InsertFuelSlip()
        {
            return optdao.InsertFuelSlip(FuelSlipId, ID, CashAdvance, FuelAdvance, CreatedBy, TdFuelAdvance, PetrolPumpId);
        }

        public DataTable GetOrgDestCordinates()
        {
            return optdao.GetOrgDestCordinates(OrgPincode, DestPincode);
        }


        public decimal TdFuelAdvance { get; set; }


        public long TripId { get; set; }

        public long UpdateTripId()
        {
            return optdao.UpdateTripId(ID, TripId);
        }

        public long UpdateTripIdForNonTrips()
        {
            return optdao.UpdateTripIdForNonTrips(ID, TripId);
        }

        public DataTable GetTripsForUpdation()
        {
            return optdao.GetTripsForUpdation();
        }

        public string itemXML { get; set; }

        public long UpdateStatusFromFreightTiger()
        {
            return optdao.UpdateStatusFromFreightTiger(itemXML, CreatedBy);
        }

        public DataTable FillLRGridBulkStatusUpdate()
        {
            return optdao.FillLRGridBulkStatusUpdate();
        }

        public int PODReq { get; set; }
        public int PODReasonId { get; set; }
        public decimal IdemnityAmt { get; set; }
        public string ReportType { get; set; }

        public DataTable PODPerformanceReport()
        {
            return optdao.PODPerformanceReport(Fromdate, ToDate,ReportType,CreatedBy);
        }

        public long UpdatePODData()
        {
            return optdao.UpdatePODDetails(ID, PODDate, CreatedBy, PODFile, PODReq, PODReasonId, IdemnityAmt);
        }

        public DataSet ShowLREmailDetails()
        {
            return optdao.ShowLREmailDetails(placementId);
        }

        public long UpdateEmailCnt()
        {
            return optdao.UpdateEmailCnt(placementId);
        }

        public int PlyQty { get; set; }

        public string ConsignorEmail { get; set; }

        public string ConsignorTelNo { get; set; }

        public string ConsignorPincode { get; set; }

        public string ConsigneePincode { get; set; }

        public string EwayBillDate { get; set; }

        public string Location { get; set; }

        public long UpdatePODPhysicalCopyData()
        {
            return optdao.UpdatePODPhysicalCopyData(PODDate, ID, Location, CreatedBy);
        }

        public DataTable PendingLRforBillingWithPhysicalPOD()
        {
            return optdao.PendingLRforBillingWithPhysicalPOD();
        }

        public long UploadEwayFile()
        {
            return optdao.UploadEwayFile(ID, FileName, CreatedBy);
        }

        #region PTL
        public string Item { get; set; }
        public long ZoneTypeID { get; set; }
        public string Slab { get; set; }
        public bool IsPTL { get; set; }
        public string PTLItemXml { get; set; }
        public string PTLLRXml { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }

        public DataTable FillLRGridPTL()
        {
            return optdao.FillLRGrid(SearchText, BranchID, Fromdate, ToDate, IsPending, IsPTL);
        }

        public DataTable FillLRGridPTLMultiple()
        {
            return optdao.FillLRGridMultiple(SearchText, BranchID, Fromdate, ToDate);
        }

        public DataSet FillStatusAndItemForLR()
        {
            return optdao.FillStatusAndItemForLR(ID);
        }

        public DataTable FillItem()
        {
            return optdao.FillItem(CustomerID,VehicleTypeID,FromLoc,ToLoc);
        }

        public DataTable FillItemMultiple()
        {
            return optdao.FillItemMultiple(CustomerID, VehicleTypeID, FromLoc, ToLoc);
        }

        public DataTable FillItemZoneType()
        {
            return optdao.FillItemZoneType(CustomerID, VehicleTypeID, Item, FromLoc, ToLoc);
        }

        public DataTable FillItemZoneTypeSlab()
        {
            return optdao.FillItemZoneTypeSlab(CustomerID, VehicleTypeID, Item, ZoneTypeID, FromLoc, ToLoc);
        }

        public DataTable FillItemDetails()
        {
            return optdao.FillItemDetails(CustomerID, VehicleTypeID, Item, ZoneTypeID, Slab, FromLoc, ToLoc);
        }

        public long InsertUpdateLRPTL()
        {
            return optdao.InsertUpdateLRRecord(ID, LRNo, LRDatetime, Consigner, Consignee, Origin, Destination, VehicleNo, Weight, ChargeableWt, TotPackage, InvoiceNo, InvoiceValue, LoadType, Remarks, CreatedBy, Status, BranchID, DriverDtls, DeviceID, RefNo, SupplierID, SubSupplierID, CustomerID, AdvanceToSupplier, TotalFrieght, VehicleTypeID, SupplierAmount, BillingCompanySrl, Rate, ConsignorAddress, ConsigneeAddress, ConsigneeEmail, ConsigneeContact, VehicleSealNo, PackingType, VehiclePlacementDate, ExpectedDeliveryDays, DriverNumber, FreightType, ConsignorGSTNo, ConsigneeGSTNo, RefNo_Invoice, InvoiceDate, CashAdvance, FuelAdvance, EWayBillNo, AdvancePer, Multipoint, MainLRNo, ShipmentDocNo, InternalBillingDocNo, Incentive, SupplierIncentive, placementId, PetrolPumpId, PlyQty, ConsignorEmail, ConsignorTelNo, ConsignorPincode, ConsigneePincode, EwayBillDate, PTLItemXml);
        }

        public DataTable InsertUpdateLRPTLMultiple()
        {
            return optdao.InsertUpdateLRRecordMultiple(LRDatetime, CreatedBy, BranchID, BillingCompanySrl, PTLLRXml, PTLItemXml);
        }
        #endregion

        public string LinkType { get; set; }
    }
}
