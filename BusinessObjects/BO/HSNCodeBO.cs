﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class HSNCodeBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public decimal GST { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }

        HSNCodeDAO optDao = new HSNCodeDAO();

        public long InsertHSNCode()
        {
            return optDao.InsertHSNCode(Srl, Name, IsActive, CreatedBy, GST);
        }

        public DataTable GetHSNCodedetails()
        {
            return optDao.GetHSNCodes(SearchText);
        }

        public bool isHSNCodeAlreadyExist()
        {
            return optDao.isHSNCodeAlreadyExist(Name, Srl);
        }

        public long DeleteHSNCode()
        {
            return optDao.DeleteHSNCodes(Srls);
        }
    }
}
