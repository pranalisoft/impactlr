﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
  public  class SupplierBO
    {
        public string SearchText { get; set; }
        public long Srl { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string landline { get; set; }
        public string mobile { get; set; }
        public string emailID { get; set; }
        public string vendor { get; set; }
        public string CST { get; set; }
        public string VAT { get; set; }
        public string TIN { get; set; }
        public string ServiceTaxno { get; set; }
        public long creditdays { get; set; }
        public decimal creditLimit { get; set; }
        public bool isActive { get; set; }
        public string Srls { get; set; }
        public string GLCode { get; set; }
        public decimal AdvancePer { get; set; }
        public string AgreementDate { get; set; }
        public bool AgreementDone { get; set; }
        public decimal TDSPer { get; set; }
        public long CreatedBy { get; set; }

        public string FirmName { get; set; }
        public string VendorType { get; set; }
        public string ContactPersonName { get; set; }
        public string AadharNo { get; set; }
        public string PAN { get; set; }
        public long SearchedBy { get; set; }
        public long ApprovedBy { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string AccountNo { get; set; }
        public string IFSC { get; set; }
        public string VehicleTypeXML { get; set; }
        public string DestinationXML { get; set; }
        public string ValidTillDate { get; set; }

        public bool HoldPayment { get; set; }
        public string HoldRemarks { get; set; }

        SupplierDAO optDao = new SupplierDAO();

        public long InsertSupplier()
        {
            return optDao.InsertSupplier(Srl, Name, Address1, Address2, Address3, landline, mobile, emailID, vendor, CST, VAT, TIN, ServiceTaxno, creditdays, creditLimit, isActive, GLCode, AdvancePer, AgreementDone, AgreementDate, CreatedBy, TDSPer, FirmName, VendorType, ContactPersonName, AadharNo, PAN, SearchedBy, ApprovedBy, BankName, BankBranch, AccountNo, IFSC,DestinationXML,VehicleTypeXML,ValidTillDate,SupplierType,HoldPayment,HoldRemarks);
        }

        public DataTable GetSupplierDetails()
        {
            return optDao.GetSuppliers(SearchText);
        }

        public DataTable GetSuppliersByType()
        {
            return optDao.GetSuppliersByType(SupplierType);
        }
      
        public bool isSupplierAlreadyExist()
        {
            return optDao.isSupplierAlreadyExist(Name, Srl);
        }

        public long DeleteSupplier()
        {
            return optDao.DeleteSupplier(Srls);
        }

        public DataTable SupplierDestinations()
        {
            return optDao.SupplierDestinations(Srl);
        }

        public DataTable SupplierVehicleType()
        {
            return optDao.SupplierVehicleType(Srl);
        }



        public int SupplierType { get; set; }

        public DataTable GetPlySuppliers()
        {
            return optDao.GetPlySuppliers();
        }
    }
}
