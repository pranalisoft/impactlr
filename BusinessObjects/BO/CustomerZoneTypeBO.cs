﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
   public class CustomerZoneTypeBO
    {
       CustomerZoneTypeDAO optDAO;

       public long Srl { get; set; }
       public long CustomerId { get; set; }
       public string ZoneType { get; set; }
       public bool IsActive { get; set; }
       public long CreatedBy { get; set; }
       public string SearchText { get; set; }
       public string Srls { get; set; }

       public CustomerZoneTypeBO()
       {
           optDAO = new CustomerZoneTypeDAO();
       }

       public long InsertUpdateCustomerZoneType()
       {
           return optDAO.InsertUpdateCustomerZoneType(Srl, CustomerId, ZoneType, IsActive, CreatedBy);
       }

       public DataTable GetCustomerZoneType()
       {
           return optDAO.GetCustomerZoneType(SearchText);
       }

       public long CustomerZoneTypeDelete()
       {
           return optDAO.CustomerZoneTypeDelete(Srls);
       }

    }
}
