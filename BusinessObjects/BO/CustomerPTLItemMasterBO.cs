﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class CustomerPTLItemMasterBO
    {
        CustomerPTLItemMasterDAO optDAO;

        public long Srl { get; set; }
        public long CustomerId { get; set; }
        public string Item { get; set; }
        public long ZoneTypeId { get; set; }
        public string Slab { get; set; }
        public decimal RatePerQtySelling { get; set; }
        public decimal RatePerKgSelling { get; set; }
        public long VehicleTypeId { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public decimal RatePerQtyBuying { get; set; }
        public decimal RatePerKgBuying { get; set; }
        public string SearchText { get; set; }
        public decimal MOQ { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }
        public int IsFixedRateSelling { get; set; }
        public int IsFixedRateBuying { get; set; }

        public CustomerPTLItemMasterBO()
        {
            optDAO = new CustomerPTLItemMasterDAO();
        }

        public long InsertUpdateCustomerPTLItemMaster()
        {
            return optDAO.InsertUpdateCustomerPTLItemMaster(Srl, CustomerId, Item, ZoneTypeId, Slab, RatePerQtySelling, RatePerKgSelling, VehicleTypeId, IsActive, CreatedBy, RatePerQtyBuying, RatePerKgBuying,MOQ,FromLoc,ToLoc,IsFixedRateSelling,IsFixedRateBuying); 
        }

        public DataTable GetCustomerPTLItemMaster()
        {
            return optDAO.GetCustomerPTLItemMaster(SearchText,CustomerId);
        }

        public long CustomerPTLItemMasterDelete()
        {
            return optDAO.CustomerPTLItemMasterDelete(Srl);
        }

        public DataTable GetZoneType()
        {
            return optDAO.GetZoneType(CustomerId);
        }
    }
}
