﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class SupplierPaymentBO
    {
        SupplierPaymentDAO CustRcptDao = new SupplierPaymentDAO();
        public string refNo { get; set; }
        public string tranDate { get; set; }
        public long SupplierID { get; set; }
        public string remarks { get; set; }
        public decimal totalAmount { get; set; }
        public long userID { get; set; }
        public string paymentXML { get; set; }
        public string BankName { get; set; }
        public string PaymentMode { get; set; }
        public string ChqNo { get; set; }
        public string ChqDate { get; set; }
        public string CardNetBankingDetails { get; set; }
        public string PayType { get; set; }
        public long Srl { get; set; }
        public bool DatefilterYes { get; set; }
        public string SearchFrom { get; set; }
        public string SearchTo { get; set; }
        public long BranchID { get; set; }
        public long RcptID { get; set; }
        public decimal CashAmount { get; set; }
        public decimal FuelAmount { get; set; }
        public string Srls { get; set; }

        public long LRId { get; set; }
        public decimal UnloadingCharges { get; set; }
        public string SearchText { get; set; }

        public string supplier { get; set; }
        public string mobileNo { get; set; }
        public string user { get; set; }

        public long InsertPaymentDetails()
        {
            return CustRcptDao.InsertPaymentDetails(refNo, tranDate, SupplierID, remarks, totalAmount, userID, paymentXML, BankName, PaymentMode, ChqNo, ChqDate, CardNetBankingDetails, PayType, BranchID);
        }

        public long InsertPaymentWhatsAppFail()
        {
            return CustRcptDao.InsertPaymentWhatsAppFail(refNo, tranDate, supplier, mobileNo, totalAmount, user);
        }

        public long UpdatePaymentHeader()
        {
            return CustRcptDao.UpdatePaymentHeader(tranDate, SupplierID, remarks, totalAmount, userID, BankName, PaymentMode, ChqNo, ChqDate, CardNetBankingDetails, Srl,CashAmount,FuelAmount);
        }

        public long UpdateUnloadingChargesPayment()
        {
            return CustRcptDao.UpdateUnloadingChargesPayment(LRId, UnloadingCharges, tranDate, userID);
        }

        public DataSet ShowPaymentGrid()
        {
            return CustRcptDao.ShowPaymentGrid(SupplierID, BranchID);
        }

        public DataTable ShowPaymentGrid_DebitNote()
        {
            return CustRcptDao.ShowPaymentGrid_DebitNote(SupplierID, BranchID);
        }

        public DataTable ShowPaymentStatus()
        {
            return CustRcptDao.ShowPaymentStatus(SupplierID, BranchID);
        }

        public DataTable FillSupplierCombo()
        {
            return CustRcptDao.FillSupplierCombo(BranchID);
        }

        public DataTable FillSupplierComboAdvance()
        {
            return CustRcptDao.FillSupplierComboAdvance(BranchID);
        }

        public DataTable FillLRComboAdvance()
        {
            return CustRcptDao.FillLRComboAdvance(BranchID,SupplierID);
        }

        public DataTable FillPaymentForWhatsApp()
        {
            return CustRcptDao.FillPaymentForWhatsApp(BranchID, SupplierID);
        }

        public DataTable GetPaymentDetailsForWhatsApp()
        {
            return CustRcptDao.GetPaymentDetailsForWhatsApp(refNo);
        }

        public DataTable FillLRAdvance()
        {
            return CustRcptDao.FillLRAdvance(BranchID, SupplierID);
        }

        public DataTable FillPaymentInvoiceDetails()
        {
            return CustRcptDao.FillPaymentInvoiceDetails(refNo);
        }

        public DataTable FillPaymentHeader()
        {
            return CustRcptDao.FillPaymentHeader(DatefilterYes, SearchFrom, SearchTo, BranchID, PayType,SearchText);
        }

        public DataTable FillPaymentHeaderAdvance()
        {
            return CustRcptDao.FillPaymentHeaderAdvance(Srl);
        }

        public long DeletePayment()
        {
            return CustRcptDao.DeletePayment(Srls);
        }

        public DataTable PendingAdvanceReport()
        {
            return CustRcptDao.PendingAdvanceReport();
        }

        public DataTable ShowPayable()
        {
            return CustRcptDao.ShowPayable(SupplierID);
        }

        public DataTable GetRefNo()
        {
            return CustRcptDao.GetRefNo(Srl);
        }

        public DataTable PaymentWhatsAppFailReport()
        {
            return CustRcptDao.PaymentWhatsAppFailReport(SearchFrom, SearchTo);
        }

        public DataTable CheckPaymentHold()
        {
            return CustRcptDao.CheckPaymentHold(SupplierID);
        }
    }
}
