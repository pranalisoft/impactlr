﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class SupplierTypeBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }    
        public  string Ind { get; set; }

        SupplierTypeDAO optDao = new SupplierTypeDAO();

        public long InsertSupplierType()
        {
            return optDao.InsertSupplierType(Srl, Name,IsActive,CreatedBy,Ind);
        }

        public DataTable GetSupplierTypedetails()
        {
            return optDao.GetSupplierTypes(SearchText,ShowAll);
        }

        public bool isSupplierTypeAlreadyExist()
        {
            return optDao.isSupplierTypeAlreadyExist(Name, Srl);
        }

        public long DeleteSupplierType()
        {
            return optDao.DeleteSupplierType(Srls);
        }




        public string ShowAll { get; set; }
    }
}
