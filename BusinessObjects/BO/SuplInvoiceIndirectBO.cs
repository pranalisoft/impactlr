﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessObjects.DAO;
namespace BusinessObjects.BO
{
   public class SuplInvoiceIndirectBO
    {
        public string FromDate { get; set; } 
        public string ToDate { get; set; } 
        public string SearchText { get; set; } 
        public long Srl { get; set; } 
        public long BillCompanySrl { get; set; } 
        public long SupplierSrl { get; set; } 
        public string InvoiceDate { get; set; }  
        public decimal basicAmount { get; set; } 
        public decimal InvoiceAmount { get; set; } 
        public long CreatedBy { get; set; } 
        public string ItemXML { get; set; } 
        public string AmountInWords { get; set; }
        public string BuyerRefNo { get; set; }
        public string BuyerRefDate { get; set; }
       SuplInvoiceIndirectDAO optdao;
       public DataTable ShowHeaderData()
       {
           optdao = new SuplInvoiceIndirectDAO();
           return optdao.ShowHeaderData(FromDate, ToDate, SearchText);
       }

       public DataTable ShowDetailData()
       {
           optdao = new SuplInvoiceIndirectDAO();
           return optdao.ShowDetailData(Srl);
       }
       public long InsertUpdateInvoice()
       {
           optdao = new SuplInvoiceIndirectDAO();
           return optdao.InsertUpdateInvoice(Srl, BillCompanySrl, SupplierSrl, basicAmount, InvoiceAmount, CreatedBy, ItemXML, AmountInWords,BuyerRefNo,BuyerRefDate);
       }




      
    }
}
