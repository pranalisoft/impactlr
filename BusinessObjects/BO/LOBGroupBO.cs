﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessObjects.DAO;
using System.Data;

namespace BusinessObjects.BO
{
    public class LOBGroupBO
    {
        public long Srl { get; set; }
        public string Name { get; set; }
        public string SearchText { get; set; }
        public string Srls { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public string LatestRemarks { get; set; }
        public string ExpDate { get; set; }
        public decimal ExpAmount { get; set; }

        LOBGroupDAO optDao = new LOBGroupDAO();

        public long InsertLOBGroup()
        {
            return optDao.InsertLOBGroup(Srl, Name,IsActive,CreatedBy);
        }

        public DataTable GetLOBGroups()
        {
            return optDao.GetLOBGroups(SearchText);
        }

        public bool IsLOBGroupAlreadyExist()
        {
            return optDao.IsLOBGroupAlreadyExist(Name, Srl);
        }

        public long DeleteLOBGroup()
        {
            return optDao.DeleteLOBGroup(Srls);
        }
    }
}
