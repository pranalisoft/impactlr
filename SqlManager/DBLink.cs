﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Logger;
namespace SqlManager
{

    public class DBLink : IDisposable
    {
        SqlConnection conn;
        SqlTransaction sqlTxn;
        bool useTxn;
        public enum txnsAction { CommitTxn, RollbackTxn, SaveTxn }
        string connectionString = string.Empty;
        string connectionStringTDA = string.Empty;
        string conStrAccess = string.Empty;
        LogWriter logger;

        public DBLink()
        {
            try
            {
                //logger = new LogWriter();
                //connectionString = ConfigurationManager.ConnectionStrings["ConLR"].ConnectionString;
                connectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();                

            }
            catch (ArgumentException ArgExp)
            {
                //logger.WriteLogError("In DBLink Constructor", ArgExp);
            }
        }

        public DBLink(bool useTxn)
        {
            try
            {
                //this.useTxn = useTxn;
                //logger = new LogWriter();
                //connectionString = ConfigurationManager.ConnectionStrings["ConLR"].ConnectionString;
                connectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
            }
            catch (ArgumentException ArgExp)
            {
                logger.WriteLogError("In DBLink Constructor", ArgExp);
            }
        }

        public int TxnAction(txnsAction action)
        {
            try
            {
                if (sqlTxn != null)
                {
                    switch (action)
                    {
                        case txnsAction.CommitTxn:
                            sqlTxn.Commit();
                            return 1;
                        case txnsAction.RollbackTxn:
                            sqlTxn.Rollback();
                            return 1;
                        case txnsAction.SaveTxn:
                            sqlTxn.Save("SavePoint " + DateTime.Now);
                            return 1;
                        default:
                            return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                logger.WriteLogError("In DBLink TxnAction While " + action.ToString(), e);
                return 0;
            }
        }

        // Connection Puller
        public SqlConnection GetConnection()
        {
            try
            {
                if (conn == null)
                {
                    conn = new SqlConnection();
                    //conn.ConnectionString = connectionString;
                }
                conn.ConnectionString = connectionString;
                switch (conn.State)
                {
                    case ConnectionState.Broken:
                        if (conn == null)
                        {
                            conn = new SqlConnection();
                            conn.ConnectionString = connectionString;
                        }
                        conn.Open();
                        return conn;
                    case ConnectionState.Closed:
                        conn = null;
                        conn = new SqlConnection();
                        conn.ConnectionString = connectionString;

                        conn.Open();
                        return conn;
                    case ConnectionState.Connecting:
                        break;
                    case ConnectionState.Executing:
                        break;
                    case ConnectionState.Fetching:
                        break;
                    case ConnectionState.Open:
                        return conn;
                }
            }
            catch (ArgumentException ArgExp)
            {
                logger.WriteLogError("In DBLink GetConnection", ArgExp);
                return null;
            }
            catch (SqlException SqlExp)
            {
                //logger.WriteLogError("In DBLink GetConnection", SqlExp);
                //return null;
            }
            catch (InvalidOperationException InvalidExp)
            {
                //logger.WriteLogError("In DBLink GetConnection", InvalidExp);
                //return null;
            }
            return conn;
        }
       

        // Execute Scalar
        public object GetValue(string query)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    return sqlCommand.ExecuteScalar();
                }
            }
            catch (SqlException SqlExp)
            {
                logger.WriteLogError("In DBLink GetValue", SqlExp);
                return null;
            }

        }

        // Execute Scalar with Parameters
        public object GetValue(string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    //sqlCommand.Parameters.Add(parameters);                    
                    sqlCommand.Parameters.AddRange(parameters);

                    //object value = sqlCommand.ExecuteScalar();
                    object value1 = sqlCommand.ExecuteScalar();
                    sqlCommand.Parameters.Clear();
                    return value1;
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink GetValue", SqlExp);
                    return null;
                }
            }
        }

        public object USPGetValue(string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(parameters);
                    object value1 = sqlCommand.ExecuteScalar();
                    sqlCommand.Parameters.Clear();
                    return value1;
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink GetValue", SqlExp);
                    return null;
                }
            }
        }

        // Execute Scalar with Parameters using Transaction
        public object GetValue(string query, SqlParameter[] parameters, string txnName, IsolationLevel isoLevel)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    //if (sqlTxn == null)
                    //   // sqlTxn = conn.BeginTransaction(isoLevel, txnName);

                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.Parameters.AddRange(parameters);
                    object value = sqlCommand.ExecuteScalar();
                    sqlCommand.Parameters.Clear();
                    return value;
                }
                catch (SqlException SqlExp)
                {
                    TxnAction(txnsAction.RollbackTxn);
                    logger.WriteLogError("In DBLink GetValue", SqlExp);
                    return null;
                }
            }
        }

        // Get Any Query ResultSet in DataTable From Database
        public DataTable GetTable(string dtTabelName, string query)
        {
            using (var conn = GetConnection())
            {
                DataTable dt = new DataTable(dtTabelName);
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dt);
                    return dt;
                }
                catch (InvalidOperationException InvalidExp)
                {
                    logger.WriteLogError("In DBLink GetTable", InvalidExp);
                    return null;
                }
            }
        }

        // Get Any Query ResultSet in DataTable From Database with Parameters
        public DataTable GetTable(string dtTabelName, string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                DataTable dt = new DataTable(dtTabelName);
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    //sqlCommand.Parameters.Add(parameters);
                    sqlCommand.Parameters.AddRange(parameters);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dt);
                    sqlCommand.Parameters.Clear();
                    return dt;
                }
                catch (Exception InvalidExp)
                {
                    //logger.WriteLogError("In DBLink GetTable", InvalidExp);
                    return null;
                }
            }
        }

        public DataTable USPGetTable(string dtTabelName, string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                DataTable dt = new DataTable(dtTabelName);
                try
                {

                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    //sqlCommand.Parameters.Add(parameters);

                    sqlCommand.Parameters.AddRange(parameters);

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dt);
                    sqlCommand.Parameters.Clear();
                    return dt;
                }
                catch (InvalidOperationException InvalidExp)
                {
                    //logger.WriteLogError("In DBLink GetTable", InvalidExp);
                    return null;
                }
            }
        }
       

        public DataTable USPGetTable(string dtTabelName, string query)
        {
            using (var conn = GetConnection())
            {
                DataTable dt = new DataTable(dtTabelName);
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dt);
                    return dt;
                }
                catch (InvalidOperationException InvalidExp)
                {
                    logger.WriteLogError("In DBLink GetTable", InvalidExp);
                    return null;
                }
            }
        }

        public DataSet USPGetSet(string dsSetName, string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                DataSet ds = new DataSet(dsSetName);
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddRange(parameters);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(ds);
                    return ds;
                }
                catch (InvalidOperationException InvalidExp)
                {
                    return null;
                }
            }
        } 
        // Execute Procedure with Parameters
        public int ExecuteProcedure(string procName, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(procName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddRange(parameters);
                    int cnt = sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return cnt;
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteProcedure", SqlExp);
                    return -1;
                }
            }
        }

        // Execute Procedure with Parameters using Transaction
        public int ExecuteProcedure(string procName, SqlParameter[] parameters, string txnName, IsolationLevel isoLevel)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    //if (sqlTxn == null)
                    //sqlTxn = conn.BeginTransaction(isoLevel, txnName);

                    SqlCommand sqlCommand = new SqlCommand(procName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddRange(parameters);
                    int cnt = sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return cnt;
                }
                catch (SqlException SqlExp)
                {
                    //TxnAction(txnsAction.RollbackTxn);
                    //logger.WriteLogError("In DBLink ExecuteProcedure", SqlExp);
                    return -1;
                }
            }
        }

        // Execute Procedure with Parameters and OutParameter
        public long ExecuteProcedureWithOutParameter(string procName, SqlParameter[] parameters, int outParameterIndex)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(procName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddRange(parameters);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return long.Parse(parameters[outParameterIndex].Value.ToString());
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteProcedureWithOutParameter", SqlExp);
                    return -1;
                }
            }
        }

        public string ExecuteProcedureWithOutParameterStr(string procName, SqlParameter[] parameters, int outParameterIndex)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(procName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddRange(parameters);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return parameters[outParameterIndex].Value.ToString();
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteProcedureWithOutParameter", SqlExp);
                    return "";
                }
            }
        }

        // Execute Procedure with Parameters and OutParameter using Transaction
        public long ExecuteProcedureWithOutParameter(string procName, SqlParameter[] parameters, int outParameterIndex, string txnName, IsolationLevel isoLevel)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    //if (sqlTxn == null)
                    //    sqlTxn = conn.BeginTransaction(isoLevel, txnName);

                    SqlCommand sqlCommand = new SqlCommand(procName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.AddRange(parameters);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return (long)parameters[outParameterIndex].Value;
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteProcedureWithOutParameter", SqlExp);
                    return -1;
                }
            }
        }

        public int ExecuteNonQueryOnTable(string query)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    return sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteNonQueryOnTable", SqlExp);
                    return -1;
                }
            }
        }

        // Execute NonQuery On Table with Parameters
        public int ExecuteNonQueryOnTable(string query, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.Parameters.AddRange(parameters);
                    int cnt = sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return cnt;
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink ExecuteNonQueryOnTable", SqlExp);
                    //REFERENCE constraint
                    //if (SqlExp.Number == 547)
                    //    return -(SqlExp.Number);
                    return -1;
                }
            }
        }

        // Execute NonQuery On Table with Parameters using Transaction
        public int ExecuteNonQueryOnTable(string query, SqlParameter[] parameters, string txnName, IsolationLevel isoLevel)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    //if (sqlTxn == null)
                    //    sqlTxn = conn.BeginTransaction(isoLevel, txnName);

                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    sqlCommand.Parameters.Add(parameters);
                    int cnt = sqlCommand.ExecuteNonQuery();
                    sqlCommand.Parameters.Clear();
                    return cnt;
                }
                catch (SqlException SqlExp)
                {
                    TxnAction(txnsAction.RollbackTxn);
                    logger.WriteLogError("In DBLink ExecuteNonQueryOnTable", SqlExp);
                    //REFERENCE constraint
                    if (SqlExp.Number == 547)
                        return -(SqlExp.Number);
                    return -1;
                }
            }
        }

        public string GetNames(string query)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(query, conn);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    StringBuilder names = new StringBuilder();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (names.Length == 0)
                            {
                                names.Append(sqlDataReader[0].ToString());
                            }
                            else
                            {
                                names.Append("," + sqlDataReader[0].ToString());
                            }
                        }
                    }
                    sqlDataReader.Close();
                    sqlCommand.Parameters.Clear();
                    return names.ToString();
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink GetNames", SqlExp);
                    return (-1).ToString();
                }
            }
        }

        public string GetXMLString(string spName, SqlParameter[] parameters)
        {
            using (var conn = GetConnection())
            {
                try
                {
                    SqlCommand sqlCommand = new SqlCommand(spName, conn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = spName;
                    sqlCommand.Parameters.AddRange(parameters);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    StringBuilder xmlString = new StringBuilder();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (xmlString.Length == 0)
                            {
                                xmlString.Append(sqlDataReader[0].ToString());
                            }
                            else
                            {
                                xmlString.Append("," + sqlDataReader[0].ToString());
                            }
                        }
                    }
                    sqlDataReader.Close();
                    sqlCommand.Parameters.Clear();
                    return xmlString.ToString();
                }
                catch (SqlException SqlExp)
                {
                    logger.WriteLogError("In DBLink GetXMLString", SqlExp);
                    return (-1).ToString();
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                conn.Dispose();
                logger.WriteLogInfo("closing...");
            }
        }

        #endregion
    }


}
